pelican.matthewgraybosch.com
############################

This is my attempt at building a respectable-looking website that has all of my old posts using Pelican_. The theme is a custom job called "Oedipus_".

.. _Pelican: https://blog.getpelican.com/
.. _Oedipus: https://github.com/matthewgraybosch/oedipus-pelican-starter
