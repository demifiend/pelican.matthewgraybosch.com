Projects
########

:summary: A list of my major projects.
:slug: projects


These are the projects I work on when I'm not too tired from my day job. You'll often find mention of these in `my blog </blog/archives/>`_.

Starbreaker
===========

All-too-human androids and swashbuckling soprano catgirls expose corruption and fight demons from outer space on a near-future alternate Earth that has risen from global collapse to an interplanetary Renaissance in less than a century as a clandestine war between demons threatens to go hot once more. Morgan, Naomi, and their friends are caught in the middle and they are *not* happy about it.

Unix for Writers
================

One of the first killer apps for the original AT&T Unix was a set of text-processing tools, and modern Unix-like operating systems are still excellent systems for writers willing to master them. Maybe I can help you.

Metal Appreciation
==================

Heavy metal saved my life, and there are a lot of great albums that might otherwise go overlooked or forgotten. I want to give back by highlighting some of them.

The Fifty Page Test
===================

All book reviews are inherently subjective. Rather than review books, the Fifty Page Test is about describing what happens in the first fifty pages of a given novel, and then providing my opinion on whether the rest is worth reading. It's how I've selected books since I was a teenager buying second-hand paperbacks, and it still works for me.

The Rebel Branch Initiate's Guide
=================================

I've been writing spoiler-laden and sassy commentaries on a series of literary fantasy novels by Canadian author Cynthea Masson, with her approval.
