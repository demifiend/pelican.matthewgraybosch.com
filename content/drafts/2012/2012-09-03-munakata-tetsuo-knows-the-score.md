---
layout: post
title: "Munakata Tetsuo Knows the Score"
description: "Now he's seen the payoffs everywhere he looks. Who can he trust when God himself's a crook?"
date: 2012-09-03 19:30:18 -0400
image: /assets/images/original/starbreaker-cast-collage.jpg
imageCredit: "[Harvey Bunda](https://www.harveybunda.com)"
tags:
  - Without Bloodshed
  - quote
  - cynicism
  - corruption
  - Queensryche
  - Operation Mindcrime
  - Revolution Calling
---
Just remember that they sent Morgan Stormrider to whack him for being a traitor.

> "The Shenzhen labor strikes were engineered. Imaginos and the rest of the executive council have their hands in everything: corporations, organized labor, organized religion, and even organized crime. It’s all a revenue stream for those bastards, and while they leave sole proprietors and co-ops alone, businesses are extorted of half their profits. Anybody who doesn’t pay up finds his workforce on strike. If they still haven’t fallen in line after the strike, we get sent in to make sure they pay up — or die."

Now he's seen the payoffs everywhere he looks. Who can he trust when God himself's a crook?

Maybe there's a revolution calling?

{% include youtube.html src="CNdOsL4Xe7Q" %}