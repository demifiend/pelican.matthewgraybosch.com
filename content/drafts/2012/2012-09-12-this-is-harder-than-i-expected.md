---
layout: post
title: "This is Harder Than I Expected"
description: "I thought I knew how the climax *Without Bloodshed* would play out, but getting it right has proven unexpectedly difficult."
date: 2012-09-05 23:55:55 -0400
image: /assets/images/original/starbreaker-cast-collage.jpg
imageCredit: "[Harvey Bunda](https://www.harveybunda.com)"
tags:
  - Without Bloodshed
  - writing
  - climax
  - injury
  - unreliability
  - Chekhov's Gun
  - Finagle's Law
---
Back in 2009, I knew exactly how the climax of Starbreaker would play out, and I made it happen at a rate of a scene per night, at least 1,500 words per scene. I thought I knew how the climax of Without Bloodshed would play out, but it's been almost two weeks since I started the chapter, I'm only halfway done, and the ten speed I used to ride as a teenager is following me around and giving me really bad advice. That's never a good sign, but it's a badass song.

{% include youtube.html src="lNfYvNtQqss" %}

The problem, of course, is that I had written myself into a corner by having Morgan's gun jam on him, and then having him take a bullet to the shoulder. I had already established that he heals faster than most people, and from abuse which would normally result in hospitalization. However, I just had to go and complicate things by having the bullet which struck Morgan fail to penetrate due to his armor (despite having an initial muzzle velocity 25 times that of the speed of sound; Morgan had some badass armor), but blocking a bullet with armor is only half the battle. The rest is dealing with the bullet's kinetic energy, which has to go somewhere.

Unfortunately for Morgan, that means right through his shoulder, shattering the bone, bursting blood vessels to the point that he not only suffers massive bruising, but sees blood seeping from what appears to be unbroken skin, and nerve damage severe enough to be classified as neurotmesis.

This injury will leave Morgan partially disabled; he's left-handed, and tends to use his right for limited applications. He can handle a pistol right handed, and plays the guitar by using his left hand for fingering and his right to strum and pick, but unlike Westley and Inigo Montoya, Morgan Stormrider is left-handed.

Despite what I've established concerning the ability of people like Morgan to rapidly heal, I think it would work to show Morgan's injury linger. He had already subjected his body to considerable strain during the course of the story; one does not achieve that ever-elusive time compression without a reliable power source, and in the absence of training in what passes for sorcery in the Starbreaker setting, Morgan drew power from his own body -- which tends to result in hunger severe enough for Morgan to do what Dennis Leary suggested as part of a joke in *No Cure for Cancer*: "Bring me a live steer. I'll carve off what I want and ride the rest home."

{% include youtube.html src="_Q7kUFS-0XQ" %}

Without food and rest, an asura emulator like Morgan cannot heal properly or quickly from traumatic injuries. The preternatural qualities Morgan counted on thus far have failed him, which attaches a personal cost to his victory. He may never get back the full use of his left arm. He will have to adapt, and his disability will have repercussions throughout the rest of *Starbreaker*.

In the meantime, here's something gorgeous Harvey Bunda created for me.

{% include image.html src="/assets/images/original/starbreaker-cast-thagirion.jpg" alt="Artwork by Harvey Bunda" caption="Cellist Tamara Gellion, aka the ensof Thagirion" %}