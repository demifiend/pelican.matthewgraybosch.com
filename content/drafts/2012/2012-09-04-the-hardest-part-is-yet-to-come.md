---
layout: post
title: "The Hardest Part is Yet To Come"
description: "I'm about to write the climax of *Without Bloodshed*. I hope I don't fuck it up."
date: 2012-09-04 19:15:55 -0400
image: /assets/images/original/starbreaker-cast-collage.jpg
imageCredit: "[Harvey Bunda](https://www.harveybunda.com)"
tags:
  - Without Bloodshed
  - writing
  - climax
  - anxiety
---
Chapter 17 of *Without Bloodshed*, "To Endure the Unendurable", is in the can. I can't believe it took so long, but a shitload of character development caught up with me and said, "No, you can't just have a fuckin' duel between Morgan and Tetsuo. Morgan's convinced he was wrong to kill Tetsuo three years ago in Shenzhen, and Tetsuo is convinced that his boss has gone too far."

Next up, the climax in Chapter 18, "Non Serviam". The hardest part is yet to come. Morgan finally faces Alexander Liebenthal, but not before Tetsuo is made to suffer for his betrayal.

My wife, in the meantime, has made it clear that she doesn't want me to turn *Starbreaker* into *Bleach* or some other shounen nonsense where duels take at least half a dozen episodes, and most of that time is spent showing the protagonist getting curb-stomped before finally getting angry (or getting encouragement from his true love), pulling a miracle out of his ass, and turning the battle around. Not that I was planning to do that, since Morgan Stormrider's character development doesn't even permit him to be the berserker as which I originally imagined him, but a reasonably competent soldier taking reasonable risks.

Having him get shot up isn't an option, in other words. If you ever wonder why major characters don't get killed off, the answer comes in five words: **my wife would kill me**.
