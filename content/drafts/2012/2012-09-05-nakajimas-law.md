---
layout: post
title: "Nakajima's Law"
description: "Armorer Nakajima Chihiro has her own variant of Finagle's Law."
date: 2012-09-05 23:55:55 -0400
image: /assets/images/original/starbreaker-cast-collage.jpg
imageCredit: "[Harvey Bunda](https://www.harveybunda.com)"
tags:
  - Without Bloodshed
  - writing
  - Nakajima Chihiro
  - weapons
  - unreliability
  - Chekhov's Gun
  - Finagle's Law
---
The first scene in Chapter 18 of *Without Bloodshed* is done. The climax begins with Munakata Tetsuo lying on the floor of the Mayor's office in Boston, his asuric body rebuilding itself after taking a point-blank burst from a gauss rifle. Morgan Stormrider has just tried to trank Alexander Liebenthal, only to have his pistol, a prototype based on the real-world Heckler & Koch USP, jam on him.

And I'm at 96,120 words. I'll probably break 110K before Without Bloodshed is done, perhaps even 120K, which is supposed to be the upper limit for novels in most genres for a first-time author. Hopefully I won't go too far above 120K, but if you think that's a lot, you should have seen the 2009 draft of Starbreaker, when the story was a single novel instead of a tetralogy. It weighed in at 289,000 words.

Of course, knowing I'm getting up there in terms of word count didn't stop me from having one of those awkward moments when I realize the protagonist could solve the story's central plot problem -- how to take Alexander Liebenthal alive instead of just killing the son of a bitch -- less than a quarter of the way through the chapter. A writer is supposed to make things harder for his protagonist, and here's Morgan with a pistol capable of firing either .45 ACP or 11.43mm tranquilizer rounds -- and his pistol is loaded with tranks. One shot and the rest of the chapter's plot becomes irrelevant.

Fortunately, semi-automatic pistols can jam -- especially if the lady who designed the prototype warned the protagonist fifteen chapters ago that the weapon was in fact experimental, and that he would be field-testing it. That's exactly what Nakajima Chihiro told Morgan Stormrider towards the end of Chapter 3, giving us a setup for Nakajima's Law:

> If somebody hands you a weapon and tells you it's experimental, it will fail when you need it most.

Yes, it's a cross between [Finagle's Law](https://en.wikipedia.org/wiki/Finagle%27s_law) and [Chekhov's Gun](https://en.wikipedia.org/wiki/Chekhov%27s_gun). Deal with it.
