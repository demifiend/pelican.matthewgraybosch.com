---
title: Fun with Visual Studio 2010
excerpt: "Yes, I'm being sarcastic. There's nothing fun about Visual Studio."
header:
  image: visual-studio-logo.png
  teaser: visual-studio-logo.png
category: "Programming Considered Harmful"
tags:
  - 2010
  - IntelliSense
  - Visual Studio
  - WTF
---
I just love it when I have to close and reopen a solution in Visual Studio 2010 because VS IntelliSense isn't willing to update itself as I add new classes.﻿

(post reclaimed from Google+)

<div class="g-post" data-href="https://plus.google.com/+MatthewGraybosch/posts/MCY3KXxgpsK">
</div>
