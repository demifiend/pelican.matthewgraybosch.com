---
id: 1677
title: My First Google+ Post
excerpt: You think I'm kidding?
header:
  image: google-plus-logo.png
  teaser: google-plus-logo.png
category: "Social Media"
tags:
  - Facebook
  - Google+
  - LinkedIn
  - Twitter
---
<div class="g-post" data-href="https://plus.google.com/+MatthewGraybosch/posts/8JUWwb27DmZ">
</div>

Here's the text, in case Google decides to break G+ embeds.

> Well, I've gotten rid of my Twitter account. Now to get rid of LinkedIn and Facebook&#8230;﻿.
