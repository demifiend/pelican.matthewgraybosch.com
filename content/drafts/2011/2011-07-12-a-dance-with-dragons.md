---
title: A Dance With Dragons
excerpt: "My copy of *A Dance With Dragons* arrived this afternoon. :)"
header:
  image: a-dance-with-dragons.jpg
  teaser: a-dance-with-dragons.jpg
categories:
  - Books
tags:
  - A Song of Ice and Fire
  - A Dance with Dragons
  - George R. R. Martin
  - fantasy
  - historical
  - Westeros
---
Got my copy of _A Dance with Dragons_ this afternoon. How long can I resist the temptation to crack it open? (I was going to italicize, but Google+ stream posts don't support HTML. Then again, neither does Facebook.)﻿
