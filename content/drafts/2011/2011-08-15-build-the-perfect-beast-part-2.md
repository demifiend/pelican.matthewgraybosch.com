---
id: 2354
title: "Starbreaker 2.0: Build the Perfect Beast (Part 2)"
excerpt: Here's the second part of a chapter I had written for a second draft of the original *Starbreaker*.
date: 2011-08-15T19:06:30+00:00
guid: http://www.matthewgraybosch.com/?p=2354
permalink: /2011/08/starbreaker-2.0-build-the-perfect-beast-part-2/
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - Starbreaker
  - second draft
  - AI
  - android
  - asura emulator
  - 200 series
  - Frankenstein
  - Josefine Malmgren
  - Polaris
  - Isaac Magnin
  - science fiction
  - fantasy
  - sci-fi
  - science fantasy
---
  * [Read "Build the Perfect Beast" (Part 1)](/2011/08/build-the-perfect-beast-part-1/)

Polaris sat up and looked around the lab as the incubation pod opened, and saw nobody. Realizing that his eyes remained unfocused, he blinked and recalibrated his vision, only to see a single woman watching with nervous eyes and a grey cat perched on the edge of the pod by his feet. “Hello, kitty. Were you the one who started me up?” he asked as he reached out to pet the cat; according to the knowledgebase that had been built into him, cats liked having their fur stroked.

“That’s right,” the cat said. “I started you up. I also designed your body and wrote your operating system.” As Polaris climbed naked from the pod, the cat slipped inside and rolled onto its back. “I am your creator. Worship me. Gimme a belly-rub!”

“That would be a bad idea,” the woman warned with a tired smile as Polaris reached to obey the cat’s command. “Zero does that to everybody. He’ll let you rub his tummy, and then get his claws and teeth into you.”

“Damn it, doc,” Zero hissed. “I would have had him if you had kept quiet.”

“Had me?” Polaris asked.

“Yeah,” Zero said, curling his tail around his feet as he sat up. His ears twitched. “You see, I didn’t actually create you. I was just testing to see if you were human. You’re gullible enough, so you pass.”

Polaris shook his head, and walked around it to meet the woman who had warned him about giving Zero a belly-rub. “Good morning, Polaris,” she said, offering her hand. “Please don’t mind Zero. He’s the first of his kind, the prototype kitty emulator, and he thinks he owns the place.”

“In other words, Dr. Malmgren, he offers a high-fidelity emulation?” Polaris chuckled. He had suspected, based on a photograph he had on file, that the gaunt, tired woman before him was Dr. Josefine Malmgren, but it had been her voice that made him sure.

“Something like that.” Dr. Malmgren nodded, as she scooped up Zero for a cuddle. The cat wriggled in her arms and pressed the pad of his paw against Dr. Malmgren’s nose. “Get your hands off me, you damn dirty ape. Your ancestors used to venerate mine as gods, and you dare to hug me like a doll?”

“You’re a prototype,” Polaris pointed out. “You don’t have any ancestors.”

“They’re spiritual ancestors,” Zero insisted, glaring at Polaris. “But that’s beside the point. As the first of my kind, I’m entitled to a certain degree of respect, and Dr. Malmgren here isn’t giving me my due.”

Polaris gave this a few seconds’ thought. “If she created your operating system as well as mine, then it would be reasonable to say that she is the one entitled to a certain degree of respect, as she had a hand in our creation. Would it kill you to purr for Dr. Malmgren?”

“Well, he hasn’t coughed up a hairball on me yet,” Dr. Malmgren chuckled as she returned Zero to the floor. “I’ll take what I can get.”

“Is it wise to tempt him like that?” Polaris asked as Zero stalked out of the room with his ears back and his tail swishing in annoyance. He suspected that a cat capable of speech was perfectly capable of entertaining thoughts of revenge, but Josefine seemed to shrug off his concerns. “He’ll get over it,” Dr. Malmgren assured him. “He’s just annoyed that I’m not giving him the attention I usually do.”

“Are you sure?”

“Quite,” Dr. Malmgren nodded, and crossed her arms. “Aren’t you cold?” she asked, causing Polaris to blush beneath her gaze. He shook his head; he was not cold, but he suspected that the literal meaning of Dr. Malmgren’s question had not been the one she intended. “Do you want me to put something on?”

Dr. Malmgren nodded, and turned away from Polaris. She began to look around the lab, and eventually settled on a lab coat that another technician had left draped over his chair. “I’m sorry,” Dr. Malmgren began as she offered the coat. “If I had known that I would be activating you tonight, I would have brought clothes.”

“Are you uncomfortable with my nudity?” Polaris asked, looking down at himself.

“It’s… distracting,” Dr. Malmgren admitted as Polaris took the coat and tried it on. Finding that it was too small for him, he tore off the sleeves and wrapped the coat around his waist to create a makeshift kilt. He took a step forward to see if the wrap would remain secure, only to see it come undone. He caught the coat before it could slide from his body and expose him again, and looked at Dr. Malmgren as he held the coat over his groin. “Do you have a safety pin?” he asked. “Or should I just hold this up?”

“Hold on a minute,” Dr. Malmgren said as she shrugged off her lab coat and laid it aside. Reaching behind her back, she slid her hands under her cardigan and produced a pin. “As long as I keep my cardigan on, nobody will notice how baggy my blouse has become. Here. Take it.”

“Thanks,” Polaris said as he accepted the pin and used it to keep his lab coat wrapped about his waist. “That’s better.”

Dr. Malmgren nodded her agreement. “Well, it’s less distracting. The hardware design team is probably very proud of their work.”

“Why aren’t they here?”

Dr. Malmgren looked away, but Polaris could still see her blushing. “Well… I wasn’t actually supposed to activate you just yet. I’m probably going to lose my job as a result, but I had to know if the Asura OS would work under real-world conditions. You had already lived several lifetimes in simulation, but simulations are limited by the assumptions of their programmers.”

“Is that what I am, then?” Polaris asked. “I’m an Asura? I’m not human?”

“Well, you’re not a member of homo sapiens. You are an artificial intelligence. Were your body composed of mechanical and electronic systems, one might call you an android. Instead —”

“Instead, he’s a replicant,” Zero purred. “More human than human, just like I’m more feline than feline.”

“More human than human?” Polaris asked, not understanding what Zero meant.

“Zero,” Dr. Malmgren chided. “Polaris hasn’t seen that movie.”

“No,” Polaris admitted as he connected to the net and searched out the cat’s allusion. “But I’ve seen things you people wouldn’t believe. Attack ships on fire off the shoulder of Orion. I watched C-beams glitter in the dark near the Tannhauser gate.”

“Oh, great,” Dr. Malmgren sighed as she began to massage her forehead. “Claire is just going to love you to bits.”

“Is that a bad thing?”

“Not if Claire can put you back together again,” Zero joked as he rolled onto his back. “You still owe me a belly-rub.”

Polaris shook his head. “Not happening, kitty. You’re embarrassing Dr. Malmgren.”

“No, he’s just being annoying,” Dr. Malmgren sighed as she picked up the cat and tossed him outside. “I designed his operating system and initial personality, but I never thought he would become so demanding of others’ attention. I think he’s actually acting like a child who has just been told that he has a baby brother.”

“Sibling rivalry between a kitty emulator and an Asura?” Polaris asked, unable to believe the idea. “Are you sure?”

Dr. Malmgren shrugged, and slumped into a chair. “I’m not sure. This is all beyond my experience, to be honest. Now, I can’t explain why you’re called an ‘Asura’, but do you have any other questions?”

Polaris had hundreds of questions, as a matter of fact, but he could tell that Dr. Malmgren was tired. He suspected that she must have been working at an inhuman pace if she had been able awaken him ahead of schedule, and decided to ask only the most important question: “Am I the first of my kind? Will there be others like me?”

“I doubt that Dr. Malmgren had expected you to ask that particular question,” a voice said from the doorway as Dr. Malmgren’s eyes widened in shock. Polaris placed himself between Dr. Malmgren and the new arrival. He recognized the man; his silver hair and cool blue eyes matched a photo he had on file, and he recognized the insouciant tone. “You must be Dr. Isaac Magnin.”

Magnin nodded. “I am, and you are awake early, Polaris.” He glanced at Dr. Malmgren, who reddened beneath Magnin’s gaze.

“Please don’t blame Dr. Malmgren,” Polaris insisted. “I woke up on my own.”

Magnin turned his attention back towards Polaris. “I do not blame Dr. Malmgren, nor do I blame you for lying to protect her. In fact, it was sweet of you to do so.”

Dr. Malmgren gave a relieved sigh as she approached Magnin. “I’m sorry. Dr. Magnin. I know it’s ahead of schedule, but I needed to see how the Asura OS performed under real-world conditions. The simulations were too limited to give me satisfactory data.”

Magnin nodded. “You had said as much in your last progress report. I can’t claim to be surprised that you would do something about it on your own, but it might be better this way.”

“I’m glad you’re not disappointed,” Josefine managed to say through a yawn. Magnin caught her shoulders as she stumbled, and guided her to a cot she had placed in a corner of the lab for catnaps. “You’ve been pushing yourself far too hard, doctor. When you wake up, I expect you to see a physician.”

“I can’t,” Josefine protested as Magnin laid her down and drew blankets over her. “There is still so much work to do.”

“You’ve done enough. Now it is time for you to take some time off. A month with pay, to begin with. If your physician indicates that you need more rest than that, you will have it.”

“You’re being too generous again…” Josefine whispered as she drifted off. Polaris felt the air chill for a moment, as if all of the heat and ambient electricity had been sucked from the room, before Magnin tucked Dr. Malmgren in, rose to his feet, and turned to face Polaris. “Dr. Malmgren never knew that it was my idea to limit the realism of the test simulations,” Magnin chuckled. “Please do not tell her, but I had intended all along for her to activate you on her own.”

Polaris narrowed his eyes; he wanted to blame his imagination, but it seemed as if Magnin’s manner had changed as soon as Dr. Malmgren had fallen asleep. “Why tell me this? And what did you do to Dr. Malmgren as she fell asleep?”

“I told you this in order to win your trust. As for Dr. Malmgren, you must have felt me draw power so that I could ensure that she slept soundly for at least twelve hours.”

“How could you do that?”

Magnin smiled and spread his hands. “Sufficiently advanced technology. I think that explanation will do for now.”

Polaris settled into the chair Josefine had been sitting in, and regarded Magnin with growing distrust as Magnin picked up Zero and began to stroke his fur. “You never answered my first question, or gave Dr. Malmgren the opportunity to do so. Am I the first of my kind? Will there be others like me?”

“You are not the first Asura Emulator,” Magnin admitted, shaking his head. “But that is another secret I have entrusted to you. Others like you walk the earth, but were created with older, less-advanced technology. It is highly probable that you will supersede them.”

“And if I cannot, will I be deactivated in favor of a superior model?” Polaris asked, suspecting that he knew the answer already. A need to prove his strength and thus ensure his survival took form in Polaris’ mind, and he threw himself at Magnin, his hands reaching for the man’s throat. He found himself trapped in mid-jump as the air around him thickened into gel.

“No,” Magnin said. He snapped his fingers, and Polaris found himself scrambling to get his feet underneath him. As he thudded to the floor, he looked up and saw Magnin standing over him, still cradling the unruffled cat. “However, I would suggest that you learn subtlety before raising your hand against your creator in the future. Your logic dictated that I might decide to shut you down, since you were activated before your time. Your inbuilt desire to live drove you to attack in order to preserve yourself. This is understandable, and predictable.”

“You knew what I would do,” Polaris said, “and why.”

Magnin smiled as he lowered the cat to the floor and brushed off his jacket. “I am familiar with your core personality programming. You will act to preserve yourself in a manner dictated by experience, logic and probability.”

“Why did you make me?” Polaris asked. “Will there be others of my kind?”

Magnin smiled, and Polaris saw a glint of cruel amusement in his eyes, “You are newly made, and lack the context necessary to comprehend my reasons. As you learn, you may come to understand the purpose of your existence on your own.”

“And what do you hope to learn from me?” Polaris asked, understanding that he was nothing more than an experiment.

“Telling you would distort the results.” Magnin turned his back in Polaris. “Follow me, please.”

“How did you stop me in mid-air, and then throw me across the room?”

“By the same means I ensured that Dr. Malmgren would get the rest she required,” Magnin said without turning to look at Polaris, “Call it magic — or sufficiently advanced technology. Either will do for now, until you get some context.”
