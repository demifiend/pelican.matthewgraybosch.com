---
title: 'Check out *Iconoclast* by Symphony X (2011)'
excerpt: "It's rare for me to get to enjoy an album on vinyl. *Iconoclast* is a welcome exception."
date: 2011-07-20T03:42:15+00:00
permalink: /2011/07/check-out-iconoclast-by-symphony-x/
header:
  image: symphonyx-iconoclast.jpg
  teaser: symphonyx-iconoclast.jpg
category: Music
tags:
  - Michael Romeo
  - New Jersey
  - progressive metal
  - Russell Allen
  - Symphony X
  - Iconoclast
  - album
  - vinyl
  - 2011
---
I finally got a chance to put on my vinyl copy of [_Iconoclast_ by Symphony X](http://www.symphonyx.com/site/release/iconoclast/). They’re a great band, but not quite the sort of heavy metal that does it for my wife. Fortunately, I can listen to bands she doesn’t like on nights when I get home from work before she does.

It’s unfortunate that more bands don’t offer their music on vinyl, and Symphony X and Nuclear Blast Records deserve respect from all metalheads for being willing to stay true to metal’s analog roots.﻿

So you can have a taste yourself, here's "The End of Innocence".

<iframe width="420" height="315" src="https://www.youtube.com/embed/pbrwowS_0ss" frameborder="0" allowfullscreen></iframe>

Pretty good, right?

## Spotify Embed

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A1iDXSRAQ2LkwBRuRAP5bhL" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Reclaimed content

This was originally a Google+ post. One of my first, in fact.

<div class="g-post" data-href="https://plus.google.com/u/0/+MatthewGraybosch/posts/Ee5mA4S5Smk">
</div>
