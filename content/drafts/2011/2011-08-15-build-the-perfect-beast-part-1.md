---
id: 2349
title: "Starbreaker 2.0: Build the Perfect Beast (Part 1)"
excerpt: Here's the first part of a chapter I had written for a second draft of the original *Starbreaker*.
date: 2011-08-15T19:00:43+00:00
guid: http://www.matthewgraybosch.com/?p=2349
permalink: /2011/08/starbreaker-2.0-build-the-perfect-beast-part-1/
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - Starbreaker
  - second draft
  - AI
  - android
  - asura emulator
  - 200 series
  - Frankenstein
  - Josefine Malmgren
  - Polaris
  - Isaac Magnin
  - science fiction
  - fantasy
  - sci-fi
  - science fantasy
---
“Eh? What?” Josefine Malmgren asked as she rubbed the sleep from her eyes. She squinted at her desktop, looking for her glasses, and found them sitting on top of her terminal’s keyboard.

“I asked if you were going to spend the night here, Dr. Malmgren,” the guard cat said as he climbed into Josefine’s lap.

Josefine lifted the cat from her lap and placed it on the desktop. “What would people think if I had cat hair all over my skirt?”

“You know damned well that I don’t shed,” the cat said with an indignant meow. Josefine scratched the cat behind his ears, knowing that he was right. A few years ago, the Asgard Technological Development Company had experimented in creating artificial cats with human intelligence and speech capabilities for households. Part of the specification required that they be hypo-allergenic and not shed fur all over the place.

The order for the AsgarTech Company’s research and development department to create these ‘kitty emulators’ &#8212; as the R&D department called them in private &#8212; had come from its owner, Isaac Magnin. The head of the AsgarTech Company’s marketing department had pitched a fit, claiming that the market for robotic pets was too small to be worth the AsgarTech Company’s attention. Josefine smiled as she remembered Magnin’s response: “I am your market, and if what I want is unworthy of your attention, then you are welcome to submit your resignation.”

Dr. Malmgren had been hired to write the operating system for the artifical cats, and had consulted with several cat breeders, veterinarians, and specialists in feline behavior while other scientists in the R&D lab sequenced the feline genome in order to create, using nanotechnology, an artificial cat that everybody would mistake for the real thing until it rolled over and said, “Rub my belly.”

“Are you just going to sit there and stare at the screen,” the cat asked as he rolled onto his back and stretched, “Or are you going to make yourself useful?”

Josefine smiled and arched an eyebrow, “You’re awfully demanding for a prototype, Zero.”

Zero yawned and said, in a sing-song tone, “Belly-rub! Belly-rub! Give a cat a belly-rub!”

Josefine hesitated with her hand over the cat, suspecting a trap. Zero had lured workers into rubbing its belly in the past, only to nip their hands.

“Come on,” Zero wheedled, “Admit it. You’re tempted.”

“Oh, all right,” Josefine grumbled as she ran her fingers through the soft gray fur that lined Zero’s belly. Zero began to purr in response. After several minutes of petting, he wriggled out from under Josefine’s hand, leapt to the floor, and trotted out of the office.

The dead mouse he held in his mouth when he returned did not stop him from announcing to Josefine, “I brought you a snack!”

“There goes my appetite,” Josefine thought as she looked away from the ‘snack’ that Zero had laid at her feet. “Thanks, but I think I’ll let you eat it.”

Zero looked up at Josefine after nuzzling the dead mouse. “Are you sure? You haven’t eaten all day.”

“I never developed a taste for mice,” Josefine admitted, “but you’re a good kitty for killing it.”

Zero gave a quick purr and lifted the mouse in its jaws again. “I’ll eat it, then, since you don’t like mice,” the cat said as it trotted out the door.

Josefine sat back in her chair and sighed. Magnin had ended up making a fair amount of money selling the kitty emulators to exterminators, who turned them loose in subway systems and sewers to massacre rats and mice in such vast numbers that the cats themselves declared a cease-fire in order to ensure that this wild hunt would not be their last.

Other cats, sold under the “EmCat” Brand, were put to therapeutic use in hospitals, nursing homes, and hospices where they comforted patients. Others still, like the one that had woken Josefine, found work as night watchmen. They prowled office buildings and factories and reported incidents of theft, sabotage, and espionage to management. All they asked for in exchange was the worship of humans and offerings of meat, catnip, and belly-rubs — just like real cats.

The skeptical head of marketing had changed his tune after the EmCat’s first year in production, the AsgarTech Company recouped its research & development costs, and Josefine’s work on the EmCat operating system had brought her to Isaac Magnin’s personal attention.

Magnin placed Josefine in charge of her own R&D team and instructed her to use what she had learned in the development of the EmCat to aid in the successful development of the ‘Asura’, a biomechanical artifical intelligence that emulated humanity at its best.

“Humanity has always been alone,” Isaac Magnin had told her, “Until Dr. Sakhalin and his team created the Sephiroth, there were no non-human intelligences with which humanity could communicate. But the AIs we’ve created are limited.”

“I’ve never heard an AI complain about limitations,” Josefine objected, “Some say they dream of having human bodies and walking side by side with species that created them, but for the most part, AIs seem content. The EmCats are quite content to be cats.”

“Many probably are content as they are,” Magnin admitted, “But I think that those AIs who want to walk among humanity should be able to do so.”

Josefine looked at the white EmCat that sat atop one of Magnin’s bookshelves, watching her with unblinking eyes that reminded her of a clear winter sky back home in Stockholm. “Do you think that traditional AIs are jealous of the EmCat’s mobility?”

Magnin smiled. “I suspect that to many traditional AIs, we humans are a race of Geppettos.”

“So, you want me to develop a biomechanical body that will allow a traditional AI to transfer itself into the body?”

“Exactly,” Magnin said, “However, the Asura must also be able to learn and grow on its own from a basic personality template.”

“What is the deadline?” Josefine asked.

“There is no deadline, just as there was no deadline for the EmCat experiment.”

“But you started promoting the EmCat as soon as we had a working prototype.”

“Why not?” Magnin asked. “The EmCat was an experiment in miniaturizing the processing hardware, temporary memory, and long-term memory necessary to support human-level AI in a humanoid body. As such, it was a success. The fact that the AsgarTech Company recovered its R&D costs and turned a profit on the EmCat is a bonus.”

“And will you try to profit from the Asuras?” Josefine asked.

“No,” Magnin said, “The Asura, an anthromorphic artifical intelligence, is a long-held dream of mine. Everything else I have done as a businessman I did so that I could have the resources to make this dream of mine real.”

“I think I understand,” Josefine said, blushing at the thought of Magnin opening up to her. “Would you think it odd if I suggested that everybody on the Asura R&D team be given copies of Shelley’s ‘Frankenstein’ and Asimov’s ‘Collected Robot’?”

Magnin took a small package wrapped in tissue paper from his desk and offered it to Josefine with a smile. “Here are your copies.”

The books Magnin had given Josefine still sat by her screen, even though she had read them hundreds of times in the five years she had worked on the Asura project. Frankenstein had served her a reminder of her responsibility towards the intelligence she would help to create, while Asimov’s robot stories served as cautionary tales; they warned of the peril inherent in attempting to predict or control the behavior of machines complex enough to possess intelligence and consciousness by using ‘simple’ rules. Had she been more like her friend Claire, she would have bought new copies as soon as they began to fall apart. Instead, she had rebound them with duct tape rather than throw away gifts from a man who appreciated her talents.

Josefine queried the virtual machine that ran a test build of the Asura operating system and requested a full status report before rising to her feet. She pulled her black wool cardigan tightly around herself. She chilled easily; weeks of long nights fueled by black coffee, small, hurried meals, and nutritional supplements had left her gaunt, and she slept only when her body rebelled against the abuse she heaped upon it. She knew what her physician would suspect anorexia instead of overwork driven by a need to bring the project to its conclusion now that the end was finally within Josefine’s sight, as she had no intention of mentioning the Asura project to anybody outside her department.

“Claire would have kittens if she knew,” Josefine thought as she poured a mug of coffee for herself and stirred in a spoonful of sugar. She gently nudged Zero out of her chair and sat down, sipping her coffee as she read the results of the latest test of the Asura OS. According to the report, the virtual Asura’s responses to all test stimuli fell within acceptable parameters. While she slept, the virtual machine running the Asura OS had lived a human lifetime; the human male personality template installed into the OS had experienced a hundred simulated years of artificial life.

Josefine tossed the printed report into the recycling bin; if anybody else cared to read it, they could pull up a copy from the AsgarTech AIs’ solid-state storage array. She knew that nobody would bother to do so. Even Isaac Magnin knew that running the Asura OS in a simulated environment would only yield a fraction of the data that real-world testing would generate. However, Magnin insisted that it was not yet time to install the Asura OS into the prototype body.

Josefine strode past the empty workstations that filled the Asura R&D lab and stopped in front of incubator that held the prototype Asura’s body. Though its outer shell was made of titanium, and the incubator sent a constant stream of diagnostic information to the R&D lab’s primary AI, it reminded her of a coffin. She suspected that Magnin had a reason for designing the incubator in this manner, without any sort of viewport that would allow her to see the Asura within, but not knowing what the incubator nurtured within itself bothered Josefine. If she dared to discuss her work with Claire, she might have admitted that the Asura incubator gave her the creeps.

While the incubator was troubling in its opacity and resemblance to coffins and sarcophagi, Josefine also found its occupant troubling. Though she had designed its operating system, she had only run the Asura OS on a virtual machine that existed in a simulated environment. That simulated environment’s programming was based on the assumptions of the scientists that had implemented it. How was Josefine to know if the programmers’ assumptions were correct? “And why does Magnin call it an ‘Asura’, anyway?” Josefine thought. “Aren’t asuras demons from Hindu mythology? And why mark it as an AES-200 unit? What does the ‘E’ stand for — Asura Emulator System? Are there AES-100 units that Magnin never told me about? And why call it ‘Polaris’?”

Josefine turned away from the incubator and muttered, “Get a grip, Josse. You’re a scientist, not some demon-ridden medieval peasant.” However, looking over her shoulder at the incubator allowed the shadow of a doubt to creep over Josefine. Magnin had spoken of providing AIs with a humanoid body, but would the Asura look human? She had been given no information about the Asura prototype’s external appearance beyond what was necessary for her to implement the functions needed for an Asura to adjust its own appearance. [Now Josefine has other reasons to be concerned. She has no idea what the Asura Prototype looks like. Does it even look human?]

“To hell with it,” Josefine said aloud as she sat down at her workstation and obtained privileged access, “I’m going to get some real-world data.” She overrode the safety mechanisms that she and Isaac Magnin had put in place and began installing the Asura OS on the Asura prototype that slept within the incubator.

The installation ran as flawlessly as it had in Josefine’s hundreds of virtual machine tests. The operating system accepted the personality template, which was based on observing ten thousand thirteen-year-old boys. Such a personality would mature swiftly, given the Asura’s processing capabilities, but the year an Asura would need to reach an adult mentality would give the Asura time to learn human social and ethical norms, learn to integrate and apply the store of pre-installed knowledge available to it, and obtain education sufficient to allow the Asura to function at an adult human’s level.

Josefine watched the bootstrap diagnostics scroll down her screen as she reached into her bottom drawer to retrieve her taser. Satisfied that its battery was fully charged, she slipped it into her cardigan’s pocket; she would use it to disable Polaris if he turned violent.

The incubator’s lid unlatched as Josefine approached it, and a slim white arm pushed the door open. The Asura sat upright as Zero jumped up and perched on the edge of the incubator to look inside, and Josefine could see that his entire body was white. His skin glittered beneath the bioluminescent rods set into the ceiling, reminding Josefine of freshly fallen, powdery snow. His hair also shimmered, and was the same azure that Claire had outgrown in her first year as Josefine’s roommate at Royal Society Polytechnic in London.

“Nobody will mistake Polaris for a human being,” Josefine realized as he climbed out of the incubator and walked around it to face Josefine. Josefine allowed her eyes to examine Polaris’ lean body before forcing herself to focus on the Asura’s face. She suspected that Claire would appreciate the Asura hardware design team’s attention to detail even more than she herself did.
