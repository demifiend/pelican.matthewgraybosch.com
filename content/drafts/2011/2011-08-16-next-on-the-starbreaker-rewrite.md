---
title: "Next on the Starbreaker Rewrite&#8230;"
excerpt: "I've decided what I'll do next for Starbreaker v2.0 after &quot;Build the Perfect Beast&quot;"
categories:
  - Project News
tags:
  - Asgard
  - plans
  - rewrite
  - Starbreaker
  - second draft
---
I've decided on what I'll do next for Starbreaker. While Morgan's on his way from London to New York via maglev after his breakfast with Naomi, I'm going to shift the action to Asgard, a domed city located near Mt. Erebus in Antarctica. At the center is the AsgarTech Building, headquarters of the Asgard Technological Development Company, where the first Model 200 Asura emulator will be activated before schedule. The next three posts will contain rough material from a draft I started and abandoned in 2010, from a chapter entitled "Build the Perfect Beast".﻿
