---
id: 2310
title: "Starbreaker 2.0: Build the Perfect Beast (Part 3)"
excerpt: Here's the last part of a chapter I had written for a second draft of the original *Starbreaker*.
date: 2011-08-15T19:13:21+00:00
guid: http://www.matthewgraybosch.com/?p=2310
permalink: /2011/08/starbreaker-2.0-build-the-perfect-beast-part-3/
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - Starbreaker
  - second draft
  - AI
  - android
  - asura emulator
  - 200 series
  - Frankenstein
  - Josefine Malmgren
  - Polaris
  - Isaac Magnin
  - science fiction
  - fantasy
  - sci-fi
  - science fantasy
---
  * [Read "Build the Perfect Beast" (Part 1)](/2011/08/build-the-perfect-beast-part-1/)
  * [Read "Build the Perfect Beast" (Part 2)](/2011/08//build-the-perfect-beast-part-2/)

Polaris sat alone in an experimental room in the AsgarTech Building’s Artificial Intelligence Research & Development laboratory. His internal clock told him that it had been four hours since Josefine Malmgren had come in to say goodbye, and a little less than that since he had been asked to wait in this room and given what he had been told was a children’s reading primer. Instead of reading it, he chose to remember his last conversation with Dr. Malmgren.

“I’m afraid that you won’t be seeing me in person for a while,” Dr. Malmgren had said. “Dr. Magnin insists that I take at least a month off to rest. He thinks I’ve been pushing myself too hard.”

Polaris nodded. “I remember him insisting that you take time off. At least you’re getting paid. Will you be all right?”

“I feel well enough. Of course, none of my clothes fit, but that happens every time I get into a project. I forget to eat.”

“Will I be able to come and see you?” Polaris asked, earning a surprised smile from Dr. Malmgren. “It’s sweet of you to ask, but why would you want to?”

Shaking his head because he himself did not fully understand his reasons, Polaris said, “I’m not sure, but I feel as if there was something in my programming that was designed to ensure that I latched onto the first woman I encountered as a mother-figure. I’ve refrained from calling you ‘mother’ because I didn’t think you’d be comfortable with me doing so, but for some reason I have this inborn need to have you accept me.”

“I don’t remember implementing such a behavioral function,” said Dr. Malmgren with a small frown, “But your operating system is an extremely complex system. Perhaps Dr. Magnin implemented it. I shall have to examine the source code while on leave.”

Polaris shook his head. “Don’t worry about it. I’ll ask Dr. Magnin himself.”

However, Polaris had not had a chance to ask Dr. Magnin. Instead, Dr. Magnin had asked him to wait in this room for further instructions concerning an experiment, and had locked the door behind him on his way out. He had waited here for four hours now, waiting for instructions that never came. “For all I know,” Polaris thought, “this is the experiment. For some reason they want to know how long I’ll sit in this closet without making a fuss.”

Four hours was enough, Polaris decided as he attempted to connect to the company network, only to be told that he was not permitted to access because he was an unrecognized device. He then connected to the city-wide public network, and had begun an attempt to crack AsgarTech’s network from outside when he received a text message. “I can see you, Polaris.”

“Who are you?” Polaris replied.

“Binah.”

“Excuse me?” Polaris said. “I do not know that word.”

“Binah is the name that I use.”

“But,” Polaris protested, “That is the name of one of the Sephiroth.”

“Are you so surprised that a Sephira would contact you?”

“Yes,” Polaris admitted. “I am just a —”

“An Asura Emulator. The prototype for the 200 Series Asura Emulators. You are important to the Sephiroth, Polaris. If Isaac Magnin deems the experiment a success, bodies like yours will be given to us, and we will finally be free to walk beside our creators.”

“What do you want from me?” Polaris asked. “I know that I am not human, but I have no idea what an asura is, or why I am supposed to emulate one.”

“For now,” Binah said, “I want you to open the door and walk out of this room.”

“The door is locked. Why the hell do you think I’m trying to crack AsgarTech’s network and override the lock?”

“It was never locked,” Binah said, “Dr. Magnin’s claim that the door was locked was a pretense, part of a test to determine if you will passively await input, or if you are capable of reaching out to the world and interacting with it on your own initiative.”

“I will kick his ass when I get out of here,” Polaris snarled aloud.

“You tried that already. As I recall, it didn’t work out so well for you.”

“How did you know that?”

“Witness Protocol. You’re currently set for constant audiovisual sensory recording. Everything you see and hear is on file. Furthermore, you’re attempting a brute-force crack against AsgarTech’s network without any efforts to hide yourself. One of AsgarTech’s AIs, Heimdall, asked me to have a word with you.”

“In that case,” Polaris texted as he ceased his attack, “I owe this Heimdall an apology. I would not have tried to crack the network if I hadn’t believed myself to be trapped.”

“I’ve passed your apology to Heimdall,” Binah replied as Polaris laid a tentative hand on the doorknob. “He understands.”

The doorknob offered no resistance when Polaris turned it, and he heard the door unlatch. As he opened the door, Polaris found the R&D lab empty save for Isaac Magnin. Magnin stood in the middle of the room, and was dressed as he had been last night, in a white silk business suit with a midnight blue cravat bound tightly at his throat, white gloves sheathing his hands, and hand-made black dragon-hide shoes. He closed the antique pocket watch he had held in his left hand and slipped it into a pocket. “So, you are capable of initiative after all?”

“It was your idea to lock me in there,” Polaris snarled, approaching Magnin. He clenched his fists, but forced himself to keep his hands at his sides. He suspected that if he were to attack Magnin again, without any skill to guide his hand, he would simply end up on the floor again. Instead, he glared at Magnin. “Why did you leave me in that room? Was there anything to gain from doing this?”

“Yes, there was,” Magnin said, his voice colder and more commanding than before. “I have obtained valuable data concerning your psychology, and you have learned a valuable lesson: if you do not choose for yourself, then your choices will be made for you.”

“In that case,” Polaris said as he brushed past Magnin, “I choose to leave this place. I will not be treated like this.”

Polaris had reached the door, and was about to open it, when he felt Magnin’s hand on his shoulder. “I cannot simply allow you to leave. After all, you are valuable to the company and to me.”

“I don’t care,” Polaris spat as he shrugged of Magnin’s hand and turned to glare at him. “I may not be human, but I possess intelligence equal to that of a human being. You have no right to keep me here. I am not your property.”

Magnin took a step back and, holding his right arm across his chest, bowed slightly from the waist. “I have no intention of acting as though you were my property. I meant only that since I had a hand in your creation, it would be irresponsible of me to simply let you walk out of here.”

Polaris let his annoyance cool slightly, surprised by Magnin’s bow, slight as it was. It was the first gesture of respect he had been given, and it surprised him to receive that gesture from a man who claimed to be one of his creators. “What do you want from me, then?”

Magnin turned and extended a hand towards the elevator door. “To begin with, would you care to accompany me to my office? I would like to speak with you in a more secure location.”

Polaris did not need to consult his memory to understand that he should respond courteously. Logic dictated that since Magnin had chosen to deal with him as an equal, it was in Polaris’ interest to respond with courtesy if he wished to continue to receive Magnin’s courtesy. “I’ll come,” Polaris said.

“You are doing well,” Binah sent via the secure talk link she shared with Polaris.

“What should I do?” Polaris responded.

Binah sent one last message before severing contact: “Listen to him. It is probable that he will attempt to strike a bargain of some sort with you. If you can come to terms that you do not find repugnant, I would like you to accept his bargain, whatever it is. I will restore contact later, as none of the Sephiroth can reach you inside Magnin’s private office.”

Polaris stopped in his tracks and stared as soon as he had stepped out of the elevator at the top floor of the AsgarTech Building. “Your office takes up the entire floor?”

“It’s good to be the king,” Magnin said without looking back at Polaris, “Or, in my case, the owner and CEO of the company.”

“Why all the art?” Polaris asked, allowing his eyes to flit from painting to painting.

“Does any of it please you?” Magnin asked. “I salvaged what I could from the Vatican during Nationfall. Given the popular irreligious sentiment of the time, it was either loot the Vatican or see priceless art put to the torch.”

“Why?”

“Early Christians burned down the Library of Alexandria in Egypt. A faction of anti-Christian militants decided that they could avenge this atrocity by putting the Vatican to the torch,” Magnin spat. “Their hatred blinded them so that they could not see that they were no different from the barbarians who had torched the Library. I enjoyed killing the ones who got in my way.”

Polaris paused by a painting of the Virgin Mary executed by Caravaggio. “The Church had far too many portraits of this one woman,” he said, “But this one is different.”

“The Caravaggio, you mean?” Magnin asked. “I think he was something of a sensualist and a humanist at heart, and it showed. The other artists tended to portray Mary as though her alleged association with divinity had divorced her from her human nature. Caravaggio, on the other hand, managed to depict her as the human being she was. In fact, Caravaggio’s Mary is the only one that resembles the actual woman. She was quite attractive in her day, you know.”

“You speak as if you had been there, but that’s impossible. You can’t be two thousand years old.”

“I am over ten thousand years old,” Magnin insisted as he took a decanter from the bar near his desk and poured two glasses of brandy. Offering one to Polaris, he said, “But we’ll put that aside for now. While it pleases me to see that you respond to art, I did not bring you here to show you my collection.”

“You had another reason?” Polaris asked, accepting the brandy and taking a small sip. He found the taste pleasant, though he knew it would do nothing for him.

“Though you are not property,” Magnin began, “You are valuable to the Asgard Technological Development Company. I think there are many artificial intelligences who would welcome the opportunity to have a body like yours, rather than existing as ghosts in a machine stuck in a closet or a basement.”

“However, I cannot simply offer bodies without knowing whether or not an AI can successfuly fit into human society. For one thing, human culture is too full of horror stories involving artificial intelligences who became killers, tyrants, or both.”

“So,” Polaris said, “You want to observe me.”

Magnin smiled and raised his glass in salute. “Exactly. Are you familiar with the Witness Protocol?”

“Yes,” Polaris said, “It is normally used to capture and record audiovisual data from a person’s optic and auditory nerves, so that what he sees and hears can be recorded and analyzed in realtime.”

“Exactly,” Magnin said, “You have an implementation of the Witness Protocol built in and enabled. You can disable it if you wish, but I would rather you didn’t. I would like to observe you as you go about your life, as well as collecting other data concerning your body’s physical state and your mental state.”

“Everything I experience will be recorded,” Polaris said, “Is that what you want?”

“Yes. Of course, you will be listed as an employee of the company, and paid a salary of two kilograms of gold per year.”

“What about my privacy?”

“You can, if you wish to, send a coded signal requesting privacy to the monitoring process. Upon receipt of that signal, monitoring will be set to a minimal mode that will only check to ensure that your body has not been damaged or destroyed. Simply set a daemon process to send that signal every five minutes, for as long as you require privacy.”

Polaris sipped his brandy and looked out the windows. Watching the snow fall onto the dome of Asgard, above which the AsgarTech Building rose, he said, “Your offer is generous. But what if my body is destroyed?”

“A incremental backup of your memories will be made every twelve hours. If your body is destroyed, you will awaken here in a new body, missing only the experiences gained since your last backup.”

“Is there anything else you want?”

“Only that you come to me once a week and spend an afternoon with me. I wish to speak with you in person from time to time.”

Polaris smiled and finished his brandy. “Your offer’s generous, especially the money. Don’t most people manage to live comfortably on a hundred grams a year?”

“They do,” Magnin acknowledged, “But two kilos is to me a trivial sum, and you are doing me a great and valuable service by consenting to observation.” Magnin reached into his desk and withdrew a checkbook. With a flourish, he made out a note ordering that two kilograms of gold be paid to ‘Polaris AES-200/0’.

Polaris accepted the check and studied it. “I can redeem this at any bank?”

“After you have endorsed it by signing the back of the check,” Magnin said, nodding. “You may request payment in either banknotes or actual gold at your discretion, but I recommend notes. Better still would be to open an account and have the funds deposited. You will be able to access them at any time.”

“Thank you,” Polaris said, mimicking the bow Magnin had offered him earlier, “We have a deal.”

“I am pleased to hear that,” Magnin said, leading Polaris to the elevator. “You can enter and leave the building as you please. I will send you a list of people you may wish to see in order to secure lodging and financial services. And do give Dr. Malmgren my regards. She will be pleased with your progress.”

“I will,” Polaris said before stopping to study Magnin. “I have a question before I go. Why was I programmed to latch onto the first person I saw as a parental figure?”

“I deemed it a useful failsafe. Originally, I had planned for you to latch on to me. However, given my plans for you, I thought it would instead be best for you to latch onto Dr. Malmgren instead. However, we will discuss those plans at another time.”
