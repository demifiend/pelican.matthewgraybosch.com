---
title: My Third Wedding Anniversary
date: 2007-10-31T12:24:43+00:00
excerpt: Three years ago, I married Catherine Gatt. I have no regrets.
header:
  image: wedding-matthew-graybosch-catherine-gatt.jpg
  teaser: wedding-matthew-graybosch-catherine-gatt.jpg
categories:
  - Personal
tags:
  - three years
  - third anniversary
  - Matthew loves Catherine
  - wedding anniversary
  - love
  - marriage
  - commitment
  - long-distance relationship
  - K1 visa
  - Australia
---
Today is my third wedding anniversary with Catherine Gatt. Yes, we got married on Halloween. We were a bit pressed for time due to her being in the US on a K1 visa, and we figured, "If we do it on Halloween, we won't have an excuse to forget." We've known each other for twelve years, however; we met on a Yahoo! writers' board in May 2000.

I was living alone in a third-floor walkup in a rough neighborhood, and had dialup net access for about a month. She had made some interesting comments about imagination, and we ended up having an exchange based on her comments. I then asked her, via email, if she'd like to exchange stories.

We continued our conversation, and agreed to meet in July 2002. I flew to Australia to meet her, spent six days together, and did not want to leave. We developed a (rather expensive without VOIP!) habit of nightly phone calls and long chats over IM, until after year I broke down and asked her to marry me.

I repeated the question the next morning, after a night's sleep. We spent a year preparing, and dealing with immigration authorities in both the US and Australia; I was willing to go there, but I had the better job at the time, and she wanted to come to the US. The end of August 2004 saw me waiting outside the international arrivals entrance at Newark Airport with an bouquet and an engagement ring. I proposed to her as soon as she came out.

It hasn't always been easy, and I've probably made it harder than it needed to be, but I have no regrets. She says the same; I ask her every year.

We laugh, however, whenever somebody complains about the difficulties of a "long-distance relationship" where the separation is a town, a state, or even the width of the continent. We kept it up with both the breadth of North America and the entire Pacific Ocean between us for four years.﻿

{% include base_path %}
![Catherine and me on our wedding day.]({{ base_path}}/images/wedding-matthew-graybosch-catherine-gatt.jpg)
