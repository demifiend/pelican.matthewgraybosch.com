---
title: "This Is Why I Don't Teach"
categories:
  - Rants
tags:
  - bullshit
  - bullshit jobs
  - censorship
  - mindcrime
  - precrime
  - teacher
  - teaching
  - thoughtcrime
  - writing while black
---
I can't believe I still have a tongue, considering how often and how hard I have to bite it to keep from laughing at people who ask me why I don't teach. Here's why.

Lemme see if I got this straight: a middle school English teacher named Patrick McLaw published two sci-fi novels involving a school shooting 900 years from now called _The Insurrectionist_ and _Lilith's Heir_ in 2011 and 2013 respectively. All of a sudden, he's deemed a threat to student safety by the Dorcester County Board of Education in Maryland?

I smell a rat. I don't think [The Raw Story](http://www.rawstory.com/rs/2014/09/01/district-suspends-dangerous-teacher-for-writing-scifi-novel-about-school-shooting-in-2902/) is telling the whole story. [The Atlantic](http://www.theatlantic.com/national/archive/2014/09/in-cambridge-md-a-soviet-style-punishment-for-a-novelist/379431/) might not have the whole story, either, but Jeffrey Goldberg's reporting meshes with that of Raw Story.

If anybody ever wondered why I became a programmer and not a teacher, here's your answer. Not only do your constitutional rights become irrelevant in school if you're a <strike>prisoner</strike> student, but you _also_ seem to waive them as a teacher. What kind of country do we live in where you can lose your job and be arrested for writing a goddamn book?

It seems we live in Soviet America, where being a writer who teaches for a living is a great way to get arrested &#8212; especially if you get caught writing while black. Yeah, I know I shouldn't be shocked because this shit has happened to teachers before, and happens entirely too often to students.

I'm just sick of this bullshit. This isn't the America I was taught to believe in. It isn't the America I was taught to love. It isn't the America I asked my wife to come and share with me.

I just don't know how to fix it. It's not like Morgan Stormrider can just take a sword to the people who make America suck. That doesn't work in real life.

Regardless, the smartest thing Mr. McLaw could do right now is drop the price of his ebooks from $14.95 to $3 and milk his persecution at the hands of local authorities for all it's worth. It's what _I_ would do.

You can check out his books using the links below.

  * [The Insurrectionist](http://www.amazon.com/gp/product/B0075CP2SG/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B0075CP2SG&linkCode=as2&tag=a-day-job-and-a-dream-20&linkId=7OPIH73AJIMKJJYP)<img src="http://ir-na.amazon-adsystem.com/e/ir?t=a-day-job-and-a-dream-20&#038;l=as2&#038;o=1&#038;a=B0075CP2SG" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
  * [Lilith's Heir](http://www.amazon.com/gp/product/B00FUL3ZRY/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00FUL3ZRY&linkCode=as2&tag=a-day-job-and-a-dream-20&linkId=HTLSADRSPIWLV5JU)<img src="http://ir-na.amazon-adsystem.com/e/ir?t=a-day-job-and-a-dream-20&#038;l=as2&#038;o=1&#038;a=B00FUL3ZRY" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
