---
id: 249
title: Getting the Hang of Dark Souls II
date: 2014-03-15T21:54:38+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=249
permalink: /2014/03/getting-hang-dark-souls-ii/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
sw_cache_timestamp:
  - 401650
bitly_link_linkedIn:
  - http://bit.ly/1N0qIR2
bitly_link:
  - http://bit.ly/1N0qIR2
bitly_link_twitter:
  - http://bit.ly/1N0qIR2
bitly_link_facebook:
  - http://bit.ly/1N0qIR2
bitly_link_tumblr:
  - http://bit.ly/1N0qIR2
bitly_link_reddit:
  - http://bit.ly/1N0qIR2
bitly_link_stumbleupon:
  - http://bit.ly/1N0qIR2
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Dark Souls II
  - Games
tags:
  - character
  - dark souls ii
  - dark souls ii spoilers
  - gameplay
  - knight
  - like a sir
  - magic
  - melee
  - ranged
  - sorcerer
  - warrior
---
I'm starting to get the hang of _Dark Souls II_ after deleting my original character, with whom I was dumb enough to kill the old Fire Keepers in Things Betwixt, and trying to play as a Sorcerer.

Starting with a Sorcerer worked out OK as I worked through the tutorial areas in Things Betwixt, but once I got out of Majula and into the Forest of Fallen Giants or Heide's Tower of Flame, the situation quickly became untenable.

## Playing as a Sorcerer

After an evening of trying to play as one, I think the Sorcerer is a starting class for more advanced players. You start out with a staff, a dagger, and a Soul Arrow spell good for 30 uses. Once you use up your Soul Arrow spell, you're stuck trying to get behind every enemy to hit with your dagger. Worse, until you level your character you lack the strength and dexterity to handle better weapons or shields.

Without a shield, your only defense is to not get hit. This is harder in _Dark Souls II_ than in the original _Dark Souls_ because of the adjustments From Software made to dodging and rolling mechanics. I'll admit that it's more realistic to still get clipped while trying to roll under a horizontal blow, but it can be frustrating when it _looks_ like the other guy's weapon didn't actually hit your toon.

So, without the means to equip a shield or better weapons than a dagger, the Sorcerer is screwed if his magic runs out. It _will_ run out, and fast, because even the Hollows in the Forest of Fallen Giants need two shots each, unless you use one shot each to soften them up, and finish them off with your dagger. For reasons I explained above, this is a risky proposition. It has its upsides, though: you'll either get better at melee combat, or you'll die.

## Swords Never Run Out of Ammo

Since I kept resorting to melee anyway, both to stretch my stock of Soul Arrow castings and to try to take down one last enemy before retreating to the bonfire, I decided to start over with a class equipped for hand-to-hand combat. The game provides <a title="Which Dark Souls II Class is Best for You?" href="http://www.gamespot.com/articles/which-dark-souls-2-class-is-best-for-you/1100-6418247/" target="_blank">four different starting classes </a>for fighters:

  * Warrior: starts with a Broken Straight Sword, Iron Parma shield, and Leather Armor. Has high Strength.
  * Knight: starts with a Broadsword, and Falconer armor. Has high HP and Adaptability
  * Swordsman: Starts with a Scimitar, Short Sword, and Wanderer armor. Has high Dexterity and respectable Adaptability, but low HP.
  * Bandit: Starts with a Hand Axe, Short Bow, 25 Wood Arrows, and bandit's armor. Has high Dexterity and respectable Strength, but abysmal Intelligence.

_Dark Souls II_ has four physical combat builds geared towards different play styles. The Warrior hits hard once you get a decent weapon, and has the additional defense of a shield, but doesn't have as much HP as a Knight.

The Knight does respectable damage with the Broadsword, and can take a bit more abuse. Getting a shield is easy enough; you'll find one on your way to Heide's Tower of Flame if you pay attention to your surroundings.

The Swordsman is a rather technical fighter, and I haven't tried dual-wielding in _Dark Souls II_. The Short Sword is a decent weapon, and has a good thrust attack. However, I never really used Scimitars and other curved swords in _Dark Souls_ or _Demon's Souls_. If you play a Swordsman, tell me what it's like in the comments._
  
_ 

I'm not at all impressed with the Bandit. The Short Bow is something you can get out of a chest in the Blacksmith's Shop. Just play enough of the Forest of Fallen Giants to reach the second bonfire in the Cardinal's Tower, and talk to the old woman with the huge backback. Buy the key she sells for 1000 souls, and you not only get a Short Bow, but can access the Blacksmith's shop.

## Fighting Like a Sir

I decided to start with a Knight yesterday, and I don't regret my choice. Instead of going straight to the Forest of Fallen Giants, I detoured instead to Heide's Tower of Flame, where I skirted around the big old sentinels and hacked at their backs with my broadsword. They were easy as long as I watched my stamina and didn't get caught by the backswing.

I used the area to level up, and am currently at Soul Level (SL) 35, with 12 vigor, 12 endurance, 10 vitality, 5 attunement, 15 strength and dexterity, 10 adaptability, 3 intelligence, and 6 faith. The attunement, intelligence, and faith stats are going to be a problem if I want to expand my Knight's tactical options with magic, but that's just a matter of leveling up.

I'm doing well with this character thus far. I've killed both the Last Giant and the Dragonrider. I even had a go at the Old Dragonslayer, who closely resembles Ornstein from _Dark Souls_. Almost had the bastard, two. Another blow would have done it.

In addition to my broadsword, which I reinforced to +2, I found the Heide Knight Sword. It does lightning damage, and can also be reinforced with regular titanite just like the broadsword. I also found a Fire Longsword, which I also reinforced to +1.

Some players will insist on building up just one weapon at a time, but weapons seem to wear down faster in _Dark Souls II_ to compensate for the automatic free repair of unbroken equipment when you rest at a bonfire. I like having a spare sword or two.

What about you? Have you been playing _Dark Souls II_? What do you think? What sort of character build are you pursuing? Tell me about it in the comments.