---
title: "*Silent Clarion*, Track 06: &quot;Nocturne in E-Flat Major Op. 9, No. 2&quot; by Frédéric Chopin"
excerpt: "Check out chapter 6 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Naomi just wanted a nice family dinner, not questions about when she'll give her foster parents grandkittens."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Maestro's almost priceless parting gift and the manner of his departure had one salutary effect. It left me too preoccupied to get into the revelous mood best suited to a pub crawl. I followed Jacqueline and the other Adversaries long enough to share a single round before taking my leave and returning home for a long soak in the tub before slipping into bed for an early night.

Lucky for my vacation plans, the next morning brought only a little soreness from my efforts against Maestro. Winston joined me at breakfast again, winding around my legs as he purred, before nesting in the cardigan I meant to wear over my blouse today. He let out the most pathetic little meow as I tried to reclaim it, and rolled over to expose his tummy.

I let him have it after indulging in a belly rub, and wore my favorite leather jacket instead. I found it in a secondhand shop, still supple and gleaming despite its age. It had zippers enough to set off metal detectors, and let me look at myself in the mirror and feel a touch Byronic. Sometimes a girl just wants to be mad, bad, and dangerous to know.

Between the jacket, a burgundy blouse, and jeans with calf-length engineer boots, I walked the line between sassy and practical. All I needed was a sword on my hip and I was ready to hit the street. The Damascus steel rapier Maestro gave me beckoned from the closet, its hilt gleaming, but wearing it in public felt too much like flaunting wealth. I grabbed my trusty Nakajima instead.

My foster parents owned a small farm on land reclaimed by bulldozing a depopulated neighborhood. The foundations had to be ripped out, and the toxins from years of urban construction had to be cleansed from the soil before they could plant their first crops. The farm produced all manner of goodies now, rotating and mixing them to enrich the soil, not to mention raised swine, sheep, chickens, turkeys, and geese.

I took the Tube, and walked the last kilometer rather than bother with a cab. I spotted Nathan first, holding an empty basket as if he were headed to the hen house to collect eggs. A dog I didn't recognize bounded beside him, and a gaggle of geese trailed behind.

He seemed a bit forlorn, but a smile broke through as he spied me. He ran toward me, his dog loping at his side, and threw himself into my arms. "Naomi! You came."

"Of course I did." I clapped Nathan's back. "How have you been? How's Charlotte?"

"Don't tell Mum and Dad." Nathan shrugged. "Charlotte and I are through. She got an offer to go pro in Moscow, and didn't want to do long-distance."

"That's stupid of her. It's not as if she were emigrating to Mars." I was wary as the dog approached me. Some of them react poorly to people with CPMD, and I've been bitten a few times. Fortunately, he started wagging his tail. The geese caught up with us, and began rooting about for bugs and worms. "Want to talk about it?"

"Nah. Maybe it was time. We're both only eighteen, so it's silly to expect a lifetime thing."

Bloody hell, that sounded like my attitude. "It's *never* silly to hope you've made a lasting connection. Just look at our parents' marriage. Three decades last April, and they still can't keep their hands off each other."

"Tell me about it." Nathan began leading me back toward the house. "They're worse than teenagers. I was never like that, and neither were you. The worst part was how embarrassed Mum and Dad would look when we came home from classes."

I giggled. "And they'd always have the same excuse." I imitated my mother's Edinburgh accent. "'So sorry, dears. We lost track of the time.' We're not going to interrupt them, are we?"

"Our brothers are home. Last time I checked, they were watching some godawful cricket match. I doubt we'll walk in on anything." Nathan chuckled, and stopped short as a couple of geese chased each other, flapping their wings and honking. "Not that I can promise an absence of gratuitous displays of affection."

"I think I can deal with Mum grabbing Dad's arse."

My mouth watered at the smell of mutton curry as we approached the house. Our mum met us at the door, and reached up to hug me despite our height difference. "Your father's in the kitchen. You're just in time for supper."

"Something smells tasty. I suppose I should have stopped for a bottle of wine to go with our dinner."

"Don't be silly, Nims," Dad called from behind a steaming tureen of curry. I took it from him and carried it to the table, only to see him return with an equally large pot of fresh, aromatic rice. "You bring a gift when you're a guest. You're family."

"Sorry, Dad." I kissed his cheek as I took the rice from him. "Does anything else need to come out?"

Nathan bore yet another huge pot. "I've got the mutter paneer."

"Nathan said something about Niall and Norman being here, but I didn't quite believe him." My older twin brothers were the sort of vegetarians who abstained from eating meat, but weren't averse to milk products. Last I heard, they had embarked on development for a new team shooter called *Nationfall: Final War*. I found them in the living room watching yet another interminable cricket match. I think it was a team from Mumbai against one from Baghdad, but I couldn't bring myself to care. "Does Mindcrime Interactive know you've buggered off to watch cricket with Dad?"

"Oi, Nims!" Niall lifted a remote and paused the match. Or was it Norman? You'd think I learned to tell them apart by now. I turned to make a tactical withdrawal to the dining room, where they'd refrain from greeting me with their usual bear hugs. When they're that close to me, I can definitely tell them apart. Norman doesn't brush his teeth as often as he should. I feel sorry for the girls he dates. "Where do you think you're going?"

"Supper's ready." They followed me to the dining room, and Niall insisted on holding my chair for me. We ate together as I shared amusing anecdotes from work.

I had a bit of everything, but wanted more because it was all so bloody good. I knew better, however, and settled for longing stares at the remaining food until Mum took pity on me. "Should I pack some for you to take home?"

"Yeah. I'd like that." Seeing that the guys seemed to have had enough, I dabbed at my mouth one last time. "Want me to help clear the table?"

Niall and Norman spoke up. "We've got it. How about a bit of music?"

Knowing my cue when I heard it, I uncovered the keys on the upright piano and sat down to play. Somebody had set out a book of Chopin's etudes, so I turned to the first and tried the first few bars, just to see if the piano was in tune. It was, and I slipped into that flowing state of action without conscious effort I experienced while playing, singing, or sparring.

I had played for an hour when somebody rested a hand on my shoulder. "Nims, did you want some cake? I made a raspberry merlot cake with walnuts and chocolate."

I'd probably regret having some, but that sounded too good to refuse. "I'd love a small slice, Mum. Did you want help?"

"Nathan's helping."

I covered the keys, stood, and stretched as my little brother brought out slices of cake and mugs of hot tea. I sipped mine. The cake proved as delightful as it sounded, and it was hard to justify turning down a second piece.

As if by a pre-arranged signal, my brothers left me alone with our parents. "Is something wrong?"

Dad shook his head. "No, but we were wondering how you were holding up by yourself. Are you lonely?"

"Why would I be? Sure, I had to dump John, but I have good friends at the Phoenix Society. And I can get back into local music and theatre."

Mum glanced at Dad before speaking. "You know, there is this pleasant young man who just completed a nanoengineering degree, and earned a position at the AsgarTech Corporation..."

I shook my head. If he just got out of school, he's probably younger than me. I'd have to train him! "No thanks, Mum. I'm not interested in meeting anybody so soon after..."

"But he has CPMD, just like you." Sophie's eyes glittered with thoughts of grandkittens. "You two could start a family."

"I don't *want* a family." I fired off the words without thinking. The shock in my mother's eyes and the hurt in my father's stopped me from saying anything else. I took a breath. "I'm sorry. That was uncalled for."

My father nodded. "I'm glad you understand that."

"I do. But I need you to understand that while I love you and realize you want me to be happy, you can't help me. You can't make my journey for me."

Sophie dabbed at her eyes. "But you're not giving up on meeting somebody, are you?"

"Of course not." I stood, and caressed the piano. "I want an equal. I want a man who can sing a duet with me, or fight me to a draw. Isn't that you guys have? I just want the same for myself."

---

### This Week's Theme Music

Nocturne in E-Flat Major Op. 9, No. 2 by Frédéric Chopin

{% youtube tV5U8kVYS88 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
