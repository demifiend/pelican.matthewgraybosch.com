---
id: 2441
title: New Beginning
date: 2014-03-04T17:45:31+00:00
excerpt: Spring is a better time for self-reinvention than New Year's Day.
header:
  image: robert-tew-self-respect.gif
  teaser: robert-tew-self-respect.gif
category: Website
tags:
  - changes
  - first-post
  - introspection
  - letting go
  - new beginning
  - self-respect
---
Many people associate New Year's Day with fresh starts and resolutions to do better. I associate it with three more months of winter. I think the spring is a better time to make a new start. The cold loosens its grip, the crocuses fight free of the earth to be first to bloom, and the days warm and lengthen. It's a good time to rebrand and reinvent yourself.

Self-reinvention is what I'll be doing here with my new blog, **A Day Job and a Dream**. If you know me from social media like Google+ or web communities like FARK or Reddit, you're acquainted with the persona I presented in the past. You know what sort of abrasive, opinionated asshole I can be.

Being an asshole doesn't help me sell books. The whole "mercenary programmer and novelist with delusions of erudition" schtick doesn't help me sell books. Griping about the stupidity of preachers and politicians on social media doesn't help me sell books. Picking fights with other people doesn't help me sell books. Blogging as my characters doesn't help, because nobody knows who they are or has a reason to care.

{% include base_path %}
![Robert Tew on Self-respect]({{ base_path}}/images/robert-tew-self-respect.gif){: .align-center}

I saw this Robert Tew quote recently. The quote, and Iron Maiden's song "Wildest Dreams" from their _Dance of Death_ album (2003), resonate with me lately.

<iframe width="420" height="315" src="https://www.youtube.com/embed/fX5aMvCd7JE" frameborder="0" allowfullscreen></iframe>

I'm not happy. I'm not getting what I want from my life, which is to write badass science fantasy, sell it, and make enough money from writing to get out of the software development trade. It's my own damn fault; I've been doing this to myself the whole time.

It's time to stop doing things that don't help me. Being abrasive, opinionated, and pretentious doesn't help, and cultivating that public persona isn't good for me any longer. Maybe it never was, but I just wasn't ready to admit it until now. However, I'm old enough to run for President, and I'm not going to get any younger.

I'm not an intellectual. I'm not a Renaissance man. I'm not special. I bleed the same color as anyone else when I cut myself while shaving. I'm just a working stiff with big dreams. I might be able to help you see that all you need is a day job and a dream.

Sure, I have an ulterior motive. I want you to become a fan. I want you to buy my books, of which _Without Bloodshed_ is but the first. Until I've persuaded you that I deserve your money, I'll settle for you taking time out of your day to follow my new blog.

I want to write more short fiction in addition to working on the next Starbreaker novel, _The Blackened Phoenix_. I'll post the rough versions here, a little at a time, for your entertainment. When I have a good quantity of completed stories, I can polish them up and publish collections.

I'll also blog my progress with _The Blackened Phoenix_ and subsequent books. I'll probably write about bands and albums I particularly like, and whatever video games I happen to be playing at the time. If I read a book that makes a particular impression, or want to help out a fellow indie/small press author, this is where I'll do it. Since I do all my writing on Linux, I'll occasionally post about free/open source software that might be of use to writers.

I'm going to be what I am once you pierce the pretense: an ordinary man with ambitions that most likely exceed his ability. I don't mind. I can work to improve myself. Hopefully my work will help and inspire you.
