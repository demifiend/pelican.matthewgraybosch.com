---
id: 417
title: Writing is How You Hack the Imagination
date: 2014-03-27T15:21:48+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=417
permalink: /2014/03/writing-hacking-imagination/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link_twitter:
  - http://bit.ly/1N0CScF
bitly_link_facebook:
  - http://bit.ly/1N0CScF
bitly_link_linkedIn:
  - http://bit.ly/1N0CScF
sw_cache_timestamp:
  - 401537
bitly_link_googlePlus:
  - http://bit.ly/1N0CScF
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - fiction
  - hacking
  - imagination
  - programming
  - similarities
  - writing
---
****As both a writer and a programmer, I’ve noticed dozens of similarities between the two, though many who do one or the other might insist that writing code and writing text are two different and incompatible processes. I don't buy it. I think fiction writers are programmers of a sort for the following reasons:

  1. **Contradictions are deadly to both fiction and software.** What a programmer calls a crash, a writer calls breaking the suspension of disbelief.
  2. **Prose directs imagination, as code directs computer hardware.** Without a program, a computer is just an elaborate machine for warming cats. Without stories, what is your imagination?
  3. **Both require abstract thought.** When designing a software object, one must build a mental model of what data the object handles, and what operations the object performs on its data before you can code. Likewise with characters. You need to understand how your characters perceive the world around them, think, and act before you can write.
  4. **Perfection is impossible**. No matter how accurate your mental model of an object, there will always be something missing. No matter how detailed your characterization, your characters aren’t wholly _real_.
  5. **Don’t worry about perfection.** No matter how hard you work on your code or your prose, you’ll find opportunities for improvement because your skills improved through use. These opportunities present you with a choice: keep working on the same thing, or _move on to something new_. I find the latter preferable, but I struggle to let go.
  6. **You won’t please everybody****.** Regardless of your effort, somebody will find a defect. There are two kinds of defects in code and prose: those preventing your work from accomplishing its purpose, and the rest. Only the former matter.
  7. **You will embarrass yourself.** Take a look at the first piece of prose or code you ever wrote. If you aren’t shocked by how bad it is, you haven’t improved much. If you _are_ embarrassed by the comparatively poor quality of your earliest work, instead be proud of how you improved.
  8. **Other people will offer unsolicited advice.** If I or somebody else mentions that I’m a writer or a programmer, other people tend to offer advice. This advice varies in quality depending on the ability of the person offering it. Regardless of the other person’s skill, it’s best to thank them immediately, and check the merits of their advice in private.
  9. **You have to concentrate.** The ability to write or code with any skill depends on whether you can <a title="Flow (Psychology) on Wikipedia" href="http://en.wikipedia.org/wiki/Flow_%28psychology%29" target="_blank">get into the zone</a>.
 10. **Concentration is _hard_.** Recent research suggests that <a title="Tough Choices: How Making Decisions Tires Your Brain" href="http://www.scientificamerican.com/article.cfm?id=tough-choices-how-making" target="_blank">simply making decisions</a> can wear people out. When you write fiction, or software, you make decision upon decision, sometimes for hours on end.

Despite the difficulties and occasional frustration I meet as a writer and a programmer, I wouldn’t give up writing except under extreme duress. I’d give up programming if I could make a living writing, but I’d still be programming. Instead of hacking your computer, I’ll **hack your imagination**.