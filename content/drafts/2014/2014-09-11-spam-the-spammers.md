---
title: Spam the Spammers
excerpt: The bastards have it coming. Trust me when I say it's the *least* they deserve for presuming that they're entitled to your attention.
header:
  image: water_flames_fire_elements_fist.jpg
  teaser: water_flames_fire_elements_fist.jpg
categories:
  - Marketer Abuse
tags:
  - fight fire with fire
  - hard sell
  - marketers should kill themselves
  - promoted posts
  - promoted tweets
  - spam
  - spam the spammers
  - turnabout is fair play
  - what would edmond dantes do?
---
Instead of just blocking spammers, I'm going to spam them back and tell them to make themselves useful and buy a copy of _Without Bloodshed_. If they want to intrude upon my attention to sell me shit, then _turnabout is fair play_.

Just ask Banksy.

{% include base_path %}
![Banksy says you don't owe advertisers shit.]({{ base_path}}/images/banksy-advertisers.jpg)

If you're an advertiser or marketer who finds this stance objectionable, then take Bill Hicks' advice and you won't have to worry about me any longer. Just kill yourself. Be an hero.

{% include spotify url="https://open.spotify.com/track/1645lYqLa2HDgsusUKvf7O" %}

I'm sick of seeing sponsored posts from businesses I have no intention of ever patronizing on social media. Twitter does it, Facebook does it, and I'm sure Google+ will eventually do it. Instead of just blocking and reporting, I think it's time to fight back.

If you don't have something of you own to sell, feel free to bombard spam posts on social media with abusive messages. Did you just see a sponsored post from Comcast in your Twitter feed? Send them replies calling them monopolistic scum and accuse them of trying to turn the internet into cable TV v2.0.

Like Banksy said, if they want to intrude upon you to sell you shit or force "brand awareness" down your throat, you don't owe them a demon-ridden thing.

**Spam the spammers.** Because turnabout is fair play, the best way to fight fire is with fire, and it's what Edmond Dantes would do.
