---
title:    Winter Solstice 2014
excerpt:  "The first year since the release of Without Bloodshed has been a good one, and Matthew Graybosch means to celebrate."
cover: solstice2014.jpg
coverwidth: 1280
coverheight: 720
covercredit: Matthew Graybosch
---
The year since the publication of my first novel, *[Without Bloodshed](/stories/without-bloodshed/)*, has been a good one. I've got plenty to celebrate:

 * Respectable sales
 * Several favorable reviews
 * No troll reviews
 * A strong showing at the 2014 World Fantasy Convention
 * Dozens of loyal fans
 * Opportunities to serialize [new work online](/stories/silent-clarion/)

I can't do much to thank everybody who has supported me thus far, but I'd like to offer new fans the opportunity to get into [Starbreaker](/about/) this holiday season without blowing their holiday budgets. If you're an existing fan, this your chance to share the awesome with friends and family.

That's right, folks: **The Kindle edition of *[Without Bloodshed](/stories/without-bloodshed/)* is on sale for $1 worldwide from 21 December 2014 to 1 January 2015!** This is not a joke, people.

Why start today? Simple: the Winter Solstice is a major holiday in the Starbreaker setting. It serves as a secular substitute for Christmas that everybody can celebrate. The sun shines on everybody, regardless of their beliefs.

I've also got a bittersweet treat for readers, a holiday story called ["Of Cats and Cardigans"](/stories/cats-and-cardigans/). Morgan has gifts to celebrate his first Winter Solstice as part of Crowley's Thoth, but a kidnapping threatens to derail his holiday plans.

In addition, the next episode of *[Silent Clarion](/stories/silent-clarion/)* is a special spicy treat for readers who have had their fill of Christmas saccharine. It's best read in bed.

Have a safe and happy holiday! Expect more badass epic science fantasy in 2015. And more kitties, too!
