---
title: Knights of Sidonia
excerpt: "Deep space SF anime goes grimdark."
header:
  image: knights-of-sidonia.jpg
  teaser: knights-of-sidonia.jpg
categories:
  - The Tarnished Screen
tags:
  - aliens
  - anime
  - deep space
  - generation ship
  - Knights of Sidonia
  - Netflix
  - sci-fi
  - third gender
  - war
  - grimdark
  - science fiction
  - review
---
I finished watching _Knights of Sidonia_ on Netflix last night. I'm a bit disappointed that the first series only ran for twelve episodes. You see, Netflix's streaming service has small problem with anime: it doesn't have nearly enough _good_ new anime. Most of what's available on streaming my wife and I have already seen (_Attack on Titan_, _Eden of the East_) &mdash; or series we dismissed as utter shit aimed squarely at the shonen demographic after an episode or two. No way am I touching _Beyblade_ in the absence of extreme duress.

Sidonia is a massive <a title="Wikipedia: Generation Ship" href="http://en.wikipedia.org/wiki/Generation_ship" target="_blank">generation ship</a>, one of a fleet that fled our star system after the advent of a species of powerful, protean aliens called the Gauna. The Gauna can only be killed after their outer shells are sufficiently damaged to expose their cores, which must be pierced by massive lances called Kabizashi wielded by pilots in spaceflight-capable mecha called Gardes. Only twenty-eight Kabizashi remain in Sidonia's arsenal, and each is precious.

The series follows the exploits of a young man named Tanikaze who is enlisted into Sidonia's ongoing war against the Gauna by the ship's immortal Captain after he was caught stealing rice to avoid starvation. Tanikaze is different from the others; he lived in hiding with his grandfather and trained in a Garde simulator based on his grandfather's Garde, the Tsugumori.

Unlike other pilots his age, who need only eat once a week because of genetic modifications enabling photosynthesis (which provides an in-story excuse to show tits and ass). This difference becomes important later on as the series reveals more of Sidonia's history.

Nor is human photosynthesis the only surprise awaiting Tanikaze. Humanity now has an androgynous third gender that is neither male nor female, represented by Izana, who is the first to befriend Tanikaze. I suspect that Izana and others like them are inspired by the people of Gethen in Ursula K. Le Guin's novel, _The Left Hand of Darkness_. The Gethen people spend most of their time as neither male nor female, but take on gendered characteristics only during _kemmer_, the mating period.

Unfortunately, Izana's primary role in first series of _Knights of Sidonia_, is a somewhat inactive one; she isn't shown as a particularly effective Garde pilot, She serves primarily as a friend for Tanikaze, expressing fear for his safety as he is repeatedly flung into battle against the Gauna. The series hints at romantic feelings on her part, but Tanikaze appears oblivious to Izana's regard.

Viewers seeking strong female characters should look elsewhere, primarily at Captain Kobayashi. This frequently masked officer leads the war effort against the Gauna and knows more about Tanikaze than she is willing to reveal at first. Unfortunately, the series is so short and frenetically paced that other potentially interesting characters remain relatively flat.

While I wouldn't characterize _Knights of Sidonia_ as <a title="Wikipedia: Hard Science Fiction" href="http://en.wikipedia.org/wiki/Hard_science_fiction" target="_blank">hard science fiction</a>, the series does exhibit a reasonable level of fidelity to basic physics. For example, the Sidonia coasts through space at a constant speed well below the speed of light, unless executing a burn to change its trajectory. Two such burns occur, one with an acceleration of 5 g-forces, and one with an acceleration of only 2 g-forces. Before both burns, the people living aboard Sidonia are ordered to secure themselves to rails placed throughout the ship using their "gravity belts".

You can view the trailer below. <a title="Xenomorphosis: Knights of Sidonia" href="http://xenomorphosis.com/2013/06/28/xeno-review-knights-of-sidonia-volumes-1-3/" target="_blank">Xenomorphosis.com also reviewed the first three volumes of the manga</a>.

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/htgcz87-Wqk" frameborder="0" allowfullscreen></iframe>
