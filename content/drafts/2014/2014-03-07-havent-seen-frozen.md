---
id: 141
title: 'I haven&#039;t seen Frozen, but&#8230;'
date: 2014-03-07T19:04:51+00:00
categories:
  - /dev/random
tags:
  - DeviantArt
  - Elsa
  - fanart
  - Frozen
  - jiyu-kaze
---
I found this on Google+ this morning. I haven't seen Disney's _Frozen_, but my wife Catherine has &#8212; and she loved it. I think she'll like this bit of fanart by Jiyu-Kaze.

I rather like it myself. This Elsa has a serious "Come at me, bro" expression.<figure style="width: 1618px" class="wp-caption aligncenter">

[<img alt="http://jiyu-kaze.deviantart.com/" src="http://i0.wp.com/fc01.deviantart.net/fs70/f/2014/059/b/5/elsa_by_jiyu_kaze-d78bgxt.jpg?resize=840%2C1298" data-recalc-dims="1" />](http://jiyu-kaze.deviantart.com/)<figcaption class="wp-caption-text">Elsa from Frozen by Jiyu-Kaze (Landy Andria)</figcaption></figure>
