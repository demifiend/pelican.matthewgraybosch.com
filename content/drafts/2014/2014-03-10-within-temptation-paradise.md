---
title: "Within Temptation: &quot;Paradise (What About Us?)&quot;"
excerpt: "Symphonic metal and Fallout cosplay. What's not to like?"
header:
  image: sharja_by_ariella_melina-d6uasq3.jpg
  teaser: sharja_by_ariella_melina-d6uasq3.jpg
  caption: "Artwork by [Ariella-melina @ DeviantArt](http://ariella-melina.deviantart.com/art/Sharja-413689323)"
categories:
  - Music
tags:
  - Fallout
  - GECK
  - paradise
  - post-apocalypse
  - nuclear holocaust
  - symphonic metal
  - Tarja Turunen
  - Sharon Den Adel
  - What About Us?
  - Within Temptation
---
The lyrics to "Paradise (What About Us?)" by [Within Temptation](http://www.within-temptation.com) with [Tarja Turunen](http://www.tarjaturunen.com) do a good job of capturing the zeitgeist I want to depict in my writing. I write about a world that isn't perfect, whose leaders all too often fail or betray those who trust them. Though my characters don't live in a paradise, they rely on each other. They're determined to stand and fight.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Dy6MpsDPKts" frameborder="0" allowfullscreen></iframe>

Of course, it also looks like Sharon Den Adel and Tarja Turunen are having fun in _Fallout_ cosplay, trying to build a GECK (Garden of Eden Creation Kit) out of scavenged parts.

The song comes from Within Temptation's new album, _Hydra_. I recommend it.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A2DxO3UHLw3k6cojObrIARR" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
