---
id: 846
title: 'Saturday Scenes &#8211; 5 July 2014'
excerpt: "Here's something different: a &quot;previously on&quot; summary."
date: 2014-07-05T17:28:28+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
---
Welcome back to Saturday Scenes. I was thinking of using this at the beginning of _The Blackened Phoenix_, to summarize the events of [_Without Bloodshed_](http://www.matthewgraybosch.com/without-bloodshed/ "Without Bloodshed") for new and returning readers. I wanted to provide an Argument similar to the ones Stephen King included at the beginning of most of his _Dark Tower_ novels as a courtesy to readers. Starbreaker might not be a long series, but I intend to make it a complex one.

---

Hi there! If you're reading this, you either read _Without Bloodshed_ already and want to see what happens next, or this is your first [Starbreaker](http://www.matthewgraybosch.com/starbreaker/ "Starbreaker") book and you don't know what the bloody hell is going on. If you're a returning fan, stick around anyway and get your memory refreshed. If you're a Starbreaker virgin, I'll be as gentle as you want me to be.

What? Who am I? I'm Claire Ashecroft, the _real_ hero. You know what Morgan Stormrider and Naomi Bradleigh would have accomplished without me? Jack and shit, and Jack just buggered off to the pub. Let me tell you what really happened.

First, some white-haired prettyboy murdered former Crowley's Thoth violinist Christabel Crowley, which I thought was excessive even though I hated the skinny bitch. Then that gun-running wanker Alexander Liebenthal took over Boston with help from ex-Adversary Munakata Tetsuo and the Fireclowns MC. Meanwhile, Morgan Stormrider's off to Tokyo because Nakajima Chihiro wanted him to investigate the theft of designs for some kind of electrical devil-killer rifle.

In the meantime, I busted my heart-shaped ass trying to help Naomi Bradleigh when I could have been enjoying a cuddle sandwich with a couple of strapping young lads I picked up on a pub crawl the night before. Turns out a dirty cop with a grudge against Morgan thought he could get some payback by sticking Nims in a Faraday cage and bullying her into copping to Christabel's murder. I disabled the Faraday cage and helped Naomi keep calm until Morgan could get to London with Eddie Cohen and Sid Schneider and bust her out.

With the whole motley crew together, we went to Boston to kick Alexander Liebenthal's cancer-ridden ass. At least, that was the plan until Morgan told us he's got orders to do the job without killing everybody. I still don't know why we couldn't have just whacked Liebenthal, other than that the Phoenix Society doesn't pay Morgan to cater to every schmuck who tries suicide-by-cop because they're too stupid to understand that suicide is a do-it-yourself job.

First, we had to hook up with the last of the Boston chapter's Adversaries, led by Sarah Kohlrynn. Then we had to get the Fireclowns out of the way. After that, Liebenthal got his hands on a shitload of money and hired a bigger army. Between that and the inability of a certain white-haired bishounen named Imaginos and his girlfriends to mind their own demon-ridden business, a job that should have taken a couple of hours took most of a bloody week and resulted in Morgan, Naomi, Eddie, and Sarah getting hospitalized.

They got better, though I hear Morgan had to go through some kind of surgical marathon with some wizard of a doctor named Desdinova. I don't trust that guy, &#8216;cos wizards who wear gray never tell the whole story. Fortunately, Morgan's a sensible bloke and doesn't trust him, either.

I didn't get to be there, but at the end, Morgan and Naomi cornered Imaginos at Christabel's funeral. We know he's using the Phoenix Society for his own nasty little agenda, but we can't anything yet. But don't worry. We're going to fix that. Morgan's on his way to get a search warrant that will let him turn Murdoch Defense Industries upside down, which will prove that Murdoch made the devil-killer rifles Liebenthal sold.

Get yourself a case of beer and some snacks. I'll tell you all about it, but it'll take a while. Get comfortable, and tell me when you're ready.
