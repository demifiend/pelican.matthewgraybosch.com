---
title: Discover Media Has No Memory
excerpt: "Memo to marketers: I will not host your shitty advertorials."
header:
  image: picard-facepalm.jpg
  teaser: picard-facepalm.jpg
categories:
  - Marketer Abuse
tags:
  - advertorial
  - Bill Hicks
  - discover media
  - email
  - malware
  - marketers should kill themselves
  - public mockery
  - rant
  - third-party advertising
---
If [Discover Media](http://www.discovermedia.co.uk/) had an organizational memory, they wouldn't have sent me the following email:

> Hi,
>
> I work for a company called Discover Media, acting on behalf of a casino client who like to advertise on your website.
>
> I am interested in publishing a short article (around 300+ words) on your website, to stay live for a 12 month period.
>
> We tailor each article to suit individual websites, and are able to provide the content if necessary and can ensure that it is unique, up to date and relevant to the theme of your website, alternatively you can provide content if you wish.
>
> If you are interested please reply and we can discuss it further.
>
> Best regards,
>
> John

Why? Because I've already been contacted by a "Riley" and a "Josh" from Discover Media, and didn't give _them_ the answer they wanted, either. If these demon-ridden idiots ever bothered to actually _look_ at my site, they'd realize it has bugger-all to do with online casinos and hosts **no third-party advertising**.

That's right, folks. There isn't a single third-party ad on my site, let alone an ["advertorial"](http://blogs.reuters.com/felix-salmon/2009/09/03/the-demise-of-the-advertorial-business/) like this asshole John wants me to post.

Oh, but he's going to pay me? Not fuckin' likely. I've got no reason to trust a company that wants to publish shitty advertorials for online casinos on a sci-fi novelist's website.

I mean, seriously, John from Discover Media, what the blithering fuck is _wrong_ with you and your coworkers? Do you idiots not visit websites before you spam their owners, or do you just get a mailing list, run it through a mail merge, and hope irate webmasters don't burst into your offices ready to chew bubblegum and kick ass?

I already John to fuck off on my reply to his spam email, but consider this an expansion. If you want me to host your shitty ads, fuck off.

All third-party advertising is **malware**. Advertisers have _ruined_ the World Wide Web with their ham-fisted attempts at monetization, and I won't help them do _more_ damage.

## PS

If you're a marketer, please listen to this *before* commenting. Or, do us all a favor and just _kill yourself_ now.

{% include spotify url="https://open.spotify.com/track/1645lYqLa2HDgsusUKvf7O" %}

Make sure you get your whole head in front of the shotgun.

## PPS

No, Nikki Tetrault and Clare Dugmore, I don't mean you. I like you.
