---
title: Happy Birthday, Catherine
date: 2014-06-03T00:01:03+00:00
author: Matthew Graybosch
header:
  image: catherine-smile.jpg
  teaser: catherine-smile.jpg
category: Personal
tags:
  - 10 years
  - Happy Birthday
  - marriage
  - Matthew Loves Catherine
  - my wife is going to kill me
  - romance
  - silliness
---
Happy birthday, Catherine. You're thirty-seven years old today, and I regret not being home to help make your day special. Ten years ago I might have said "screw it" and taken a personal day to fuss over you, but I can't do that with my current job. Instead, I'm sitting with you at a cafe and writing this while you read an article on your tablet, and I'll put it on my blog for the whole world to see.

It isn't quite as romantic as ripping a star from the sky and giving it to you to wear as jewelry, but I hope it will do.

I never thought, when I first interacted with you online fourteen years ago, that I'd joke about falling in love with you, or that the jokes would prove serious. I never thought we'd court each other for four years across ten thousand miles. I never thought you'd greet me with a kiss when we first met in person, or that after a week with you I'd have to force myself to refrain from looking back lest I try in vain to remain at your side without a visa or a job. I never thought I'd marry you, or that we'd make our marriage last a decade.

Regardless, here we are. The life we made isn't everything either of us wanted, but we can fix that. I've got the basics sorted on my end. I still love you. I still enjoy your company. I still value your opinion of my work. I still crave your touch. I still melt beneath your kiss. I still feel safe in your embrace.

I am still yours, if you'll have me. If you've grown tired of me, I'll kiss you goodbye and thank you for the memories. I should thank you for the memories anyway.

I wouldn't be the man I am today without you. You humanized me to a great extent, as Haydée did for Edmond Dantes &#8212; only without the creepy aspects of that particular relationship. You humanized my characters, and made Starbreaker better than it might have been without your help.

I love you, Catherine, and you've only improved with age. I don't know if my regard has clouded my vision, but when I look at you I don't only see you as you are today, but as you were when you first kissed me, and on our wedding night, and whenever we've retreated from the world to indulge in each other.<figure id="attachment_697" style="width: 744px" class="wp-caption aligncenter">

[<img class="size-full wp-image-697" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/06/p1010053.jpg?resize=744%2C992" alt="Me and Catherine Gatt back in the day." data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/06/p1010053.jpg)<figcaption class="wp-caption-text">Me and Catherine Gatt back in the day.</figcaption></figure>

I love you, even when it's hard to love you. You know how I like a good challenge. Happy birthday, Catherine.
