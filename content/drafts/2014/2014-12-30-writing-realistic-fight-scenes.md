---
title:    Writing Realistic Fight Scenes
excerpt:  "When writing a fight scene for Starbreaker, Matthew Graybosch often relies on research in the absence of experience."
---

Here be *Silent Clarion* spoilers. Just giving you fair warning in case you give a damn about that sort of thing.

I'm working on Chapter 26 of *Silent Clarion*, wherein Naomi is pursued by one of Christopher Renfield's men. He's a soldier enhanced by Project Harker, and is so thoroughly traumatized by his experiences with Project Harker and the events of Nationfall that his capacity for rational thought is severely limited.

She's got her sword, but he has a knife, and a companion to fight beside him as they trap her in a forest clearing. Of course, I know bugger-all about knife-fighting because I've never trained with one or been in a knife fight.

That means it's *research time!* I found [a page describing basic knife-fighting techniques taught to US Marines](http://www.msisshinryu.com/articles/safreed/knife_fighting.shtml), and used the information I found to give Naomi a more realistic challenge. As a result, you won't see Naomi's enemies holding their knives in a reverse grip, even if that looks cool in anime. Instead, they grip their knives as if they were holding hammers.

You'd think an Italian sidesword would be an easy win against a knife like the US military's [M9 bayonet](http://en.wikipedia.org/wiki/M9_bayonet), but anything can happen in a fight, especially when you're facing two well-trained enemies who know how to fight as a team.

For readers who don't know what a sidesword is, here's a picture for you. Image by [Danielli Armouries](http://www.danelliarmouries.com) on DeviantArt. Click it to view the page.

[![Italian Side sword by Danelli Armouries](http://fc07.deviantart.net/fs70/i/2013/301/b/5/italian_sidesword_16th_c__1__by_danelli_armouries-d6s4c92.jpg)](http://danelli-armouries.deviantart.com/art/Italian-Sidesword-16th-c-1-410028806)

Pretty, isn't it? A side sword can be hard to distinguish from a rapier, but a rapier is a thrusting weapon with severely limited cutting capability. A side sword has both a sharp edge and a point, making it more versatile. Also, the rapier is a descendant of the side sword, though side swords remained in use throughout most of the rapier's period.

[The ARMA](http://www.thearma.org) has a page [describing various European sword types from the Middle Ages and Renaissance](www.thearma.org/terms4.htm) if you want more information.

And here's a M9 bayonet affixed to a M4 carbine. Looks brutal, doesn't it? Hardly a weapon for a romantic, but the US Armed Forces aren't a place for romantics.

![M9 bayonet affixed to M4 carbine](http://upload.wikimedia.org/wikipedia/commons/1/1b/US-Military-M9-Bayonet.jpg)

And *this* is a longsword similar to the one Morgan Stormrider favors, since it allows him to wield it with both hands, or with one hand while firing an M1911 semiauto with his other hand. Morgan's sword is different in that it has a sharper point for thrusting, as well as a [Japanese-style hilt](http://chryonic.deviantart.com/art/Katana-20746120), which is a personal touch added by the armorer Morgan favors, Nakajima Chihiro (whom readers meet in Chapter 3 of *Without Bloodshed*; she also makes electromagnetic devil-killer rifles). She tried giving a side sword for Naomi a similar hilt, but couldn't get the balance right.  

Image by [Danelli Armouries](http://www.danelliarmouries.com) again.

[![Longsword (1) by Danelli Armouries](http://fc04.deviantart.net/fs70/i/2014/290/d/5/longsword__1__by_danelli_armouries-d836zhg.jpg)](http://danelli-armouries.deviantart.com/art/Longsword-1-489094180)
