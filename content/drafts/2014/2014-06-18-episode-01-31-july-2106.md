---
id: 760
title: 'Episode 01 &#8211; 31 July 2106'
excerpt: Here's a slice-of-life bit showing Morgan and Naomi on their way to the moon for a show.
date: 2014-06-18T06:00:25+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - A Day in the Life
  - "Crowley's Thoth"
  - flash fiction
  - morgan stormrider
  - naomi bradleigh
  - starbreaker
  - Voice of Reason
  - Voices from Home tour
---
I've decided to try something new, a series of flash fiction episodes depicting characters from _Starbreaker_ in ordinary life. I'm going to call it **Starbreaker: A Day in the Life**. Here's a piece featuring Morgan and Naomi.

## Episode 01 &#8211; 31 July 2106

Morgan didn't mind the cramped quarters aboard the _Wyoming Knott_ as much if it meant having Naomi pressed close to him. Her warmth seeped through the protective suits issued to all passengers for the duration of the passage from Armstrong City on Luna back to Earth. He clamped down on the temptation to dip his head and steal a kiss in front of the reporter.

"You two make a good couple." Samuel Terell winked at Morgan, as if sharing an unspoken understanding between men. "What was it like to play together, just the two of you?"

Morgan shivered as Naomi wound her fingers in his hair, caressing his neck. Her voice alone was enough to drive him mad. "It just isn't the same without Christabel, and we miss her, though Erica Court performed admirably in her place."

"Why couldn't Christabel Crowley join you on the Voices From Home tour?"

A glance from Naomi told Morgan it was his turn. "Christabel doesn't handle microgravity very well. Even suborbital flights are difficult for her, and it seemed cruel to subject her to a two-week voyage."

"And you didn't want two weeks with the other lovely diva of Crowley's Thoth?" Terell punctuated this question with a cynical chuckle, and leaned forward. "Just be honest. You two wanted some time alone together."

Naomi's laughter rang throughout the stateroom. "Mr. Terell, even if I wanted to seduce Morgan, we had no privacy. Between you and the rest of the press, other musicians, the roadies, and the ship's crew &#8212; we always had somebody underfoot." She leaned forward, and flashed a saucy smile. "I daresay Morgan will take it out on Christabel once he's gotten used to normal gravity again."

"Not if she sees this and thinks I'm cheating on her."

"Don't worry, &#8216;Bel." Naomi took on a teasing tone. "Morgan behaved himself the whole time. He hasn't let me do anything but ogle him."

Terell offered the mic to Morgan. "How do you like being subject to the female gaze, Adversary?"

Morgan shifted, and wished he had space and privacy to adjust himself. "It's uncomfortable."

"I bet." Terell chuckled. "Let's get back to business. Some in the business find it odd that Voice of Reason won't release recordings of their lunar performances. Care to offer some insight?"

Naomi put aside all coquettishness. "Voice of Reason is a creature of necessity, Mr. Terell. We can't tour as Crowley's Thoth without Christabel, and Christabel isn't up to the rigors of spaceflight."

"So, we took on a new name for this tour, based on Naomi being the voice of reason when Christabel and I argue." Morgan paused a moment to let Terell adjust his grip on the mic. "We didn't record our shows because we wanted to make the experience of seeing us special for those of our Lunatic fans who had the privilege of attending our shows."

"I think we should end on that note." Terell signaled his crew to halt recording. "Ms. Bradleigh, Adversary Stormrider, thanks for your time."

Naomi was first to take Terell's hand. "Of course, Mr. Terell. We wish you and your viewers the best."

Morgan also shook Terell's hand. "I can't speak for the members of Demifiend, Doomed Space Marines, Ginger Tabby Sect, or His Darkest Materials, but I appreciate you and your staff following us on the Voices From Home tour."
