---
id: 125
title: Architect or Gardener? How About Both?
date: 2014-03-06T15:00:45+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=125
permalink: /2014/03/architect-gardener/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapWP:
  - 's:160:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:3:"128";s:5:"pDate";s:19:"2014-07-15 07:00:57";}}";'
bitly_link:
  - http://bit.ly/1Qfe2X8
bitly_link_twitter:
  - http://bit.ly/1Qfe2X8
bitly_link_facebook:
  - http://bit.ly/1Qfe2X8
bitly_link_linkedIn:
  - http://bit.ly/1Qfe2X8
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1Qfe2X8
bitly_link_reddit:
  - http://bit.ly/1Qfe2X8
bitly_link_stumbleupon:
  - http://bit.ly/1Qfe2X8
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - architect
  - character
  - fantasy faction
  - gardener
  - pantser
  - plot
  - plotter
  - programmer
  - stephen deas
  - writing
---
(Artwork: King of Ruins, by <a title="Minnhagen on DeviantArt" href="http://minnhagen.deviantart.com/" target="_blank">Minnhagen</a>)

<a title="Stephen Deas" href="http://www.stephendeas.com/" target="_blank">Stephen Deas</a> writes in his Fantasy Faction article, <a title="Is Fantasy for Gardeners?" href="http://fantasy-faction.com/2014/is-fantasy-for-gardeners" target="_blank">"Is Fantasy for Gardeners?"</a>: "At some convention in the distant past on some panel about something or other, someone once asked me whether I was an architect or a gardener. I had really no idea what they were talking about but replied that, as a physicist and an engineer, I was probably more of an architect with everything laid out just so."

He continues in this vein, explaining that fantasy is mainly for gardeners, and that the architects gravitate to other genres where plot matters more than character. I disagree with his view for two reasons:

  * While fantasy stories _can_ grow, many require a substantial amount of world-building before the story's organic growth becomes possible.
  * I don't buy the notion that plot is more important than character, or vice versa.

When somebody asks me if I'm a gardener or an architect, I say I'm neither. I'm neither a plotter or a pantser. I'm a _programmer_.

I have a rough notion of what I need to do to make the story work out the way I want it. When things don't quite work out, I improvise. And if I get stuck, I don't blame <a title="The Myth of Writer's Block" href="http://litreactor.com/columns/the-myth-of-writers-block" target="_blank">writer's block</a>. I go back through the stuff I already wrote and figure out where I screwed up, because chances are I broke character, broke continuity, or just picked the wrong viewpoint for a scene.

My views might be colored both by my own reading, and by the way I prefer to write. I tend to use multiple viewpoints, and I like to build suspense by showing what the story's antagonist's plan to do in their own words. Instead of being omniscient, I'm more like a fly on the wall, or a guy riding in the back of a character's head.

I don't like false dichotomies. I don't like the notion that as a writer I should focus on character at the expense of plot, or focus on plot to the detriment of character. I would prefer to embroil complex, relatable characters in an intricate, well-crafted plot.

If you'll pardon the <a title="Urban Dictionary: &quot;Van Hagar&quot;" href="http://www.urbandictionary.com/define.php?term=van+hagar" target="_blank">Van Hagar</a> reference, I want the best of both worlds.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>