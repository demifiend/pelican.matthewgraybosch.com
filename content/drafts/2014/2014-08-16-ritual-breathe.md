---
id: 1148
title: 'Ritual: Breathe'
date: 2014-08-16T17:05:02+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1148
permalink: /2014/08/ritual-breathe/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapBG:
  - |
    s:347:"a:1:{i:0;a:8:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:64:"http://matthewgraybosch.blogspot.com/2014/08/ritual-breathe.html";s:5:"pDate";s:19:"2014-08-16 21:05:46";}}";
snapDI:
  - 
snapDL:
  - 's:292:"a:1:{i:0;a:8:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"a6bb57ff7a02ea57ca558a03b6e095bd";s:5:"pDate";s:19:"2014-08-16 21:05:39";}}";'
snapGP:
  - 
snapIP:
  - 
snapLI:
  - 
snapLJ:
  - 
snapPN:
  - 
snapSU:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snapWP:
  - 
snap_isAutoPosted:
  - 1
snapSC:
  - 
bitly_link:
  - http://bit.ly/1LnPVBc
bitly_link_twitter:
  - http://bit.ly/1LnPVBc
bitly_link_facebook:
  - http://bit.ly/1LnPVBc
bitly_link_linkedIn:
  - http://bit.ly/1LnPVBc
sw_cache_timestamp:
  - 401540
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"19c45ac3821c";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:58:"https://medium.com/@MGraybosch/ritual-breathe-19c45ac3821c";}'
categories:
  - Stuff I Found
tags:
  - Breathe
  - costumes
  - electronica
  - fashion
  - fetish
  - Jillian Ann
  - Make Life a Ritual
  - Ritual
---
I'm on indie artist/model Jillian Ann's mailing list because I engaged with her on Google+ back in the day, so I hear about her latest project, a fashion shop called [Make Life a Ritual](http://makelifearitual.com) and her electronica duo RITUAL, which you can hear in Jillian Ann's latest fashion video, Breathe.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

There's no my wife or I would fit into any of this without an eating disorder, but that might be just as well. A lot of the men's fashion reminds me of stuff characters in _The Matrix_ might wear. Not that there's anything wrong with that, but it probably isn't the look for me. I think of myself as more of a jeans &#8216;n leather guy.