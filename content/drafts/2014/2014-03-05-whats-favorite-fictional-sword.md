---
id: 75
title: 'What&#039;s YOUR favorite fictional sword?'
date: 2014-03-05T16:30:14+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=75
permalink: /2014/03/whats-favorite-fictional-sword/
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapWP:
  - 's:159:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:2:"87";s:5:"pDate";s:19:"2014-07-13 18:57:14";}}";'
snapLJ:
  - 's:206:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0lj";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:48:"http://matthewgraybosc.livejournal.com/7445.html";s:5:"pDate";s:19:"2014-07-15 11:18:49";}}";'
snapGP:
  - 's:241:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"103251633033550231172/posts/9uVEa8rbR93";s:5:"pDate";s:19:"2014-07-15 12:02:04";}}";'
bitly_link:
  - http://bit.ly/1PiUpPu
bitly_link_twitter:
  - http://bit.ly/1PiUpPu
bitly_link_facebook:
  - http://bit.ly/1PiUpPu
bitly_link_linkedIn:
  - http://bit.ly/1PiUpPu
sw_cache_timestamp:
  - 401653
bitly_link_tumblr:
  - http://bit.ly/1PiUpPu
bitly_link_reddit:
  - http://bit.ly/1PiUpPu
bitly_link_stumbleupon:
  - http://bit.ly/1PiUpPu
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - elric
  - fantasy
  - fiction
  - fritz leiber
  - gene wolfe
  - graywand
  - kill bill
  - m. john harrison
  - michael-moorcock
  - severian
  - stormbringer
  - swords
  - terminus est
  - the bride
  - viriconium
---
Sure, this <a title="What's Your Favorite Fantasy Sword?" href="http://fantasy-faction.com/2011/what%E2%80%99s-your-favorite-fantasy-sword" target="_blank">Fantasy Faction article on swords in fantasy</a> dates back to 2011, but that doesn't bother me. Not when I still listen to classic Black Sabbath albums like _Paranoid_, which dates back to 1970.

I can't blame the author for not mentioning the [Starbreaker](http://www.matthewgraybosch.com/starbreaker/ "Starbreaker"), since I didn't publish until late last year. I didn't grow up watching _Thundarr the Barbarian_, and I managed to avoid _Thundercats_. I can name several swords I prefer over Squall Leonhart's gunblade, despite being one of the few people who enjoyed _Final Fantasy VIII_._
  
_ 

## Stormbringer

It's at least four feet worth of rune-carved <a title="Lay down your soul to the gods' rock 'n roll!" href="http://en.wikipedia.org/wiki/Black_Metal_(album)" target="_blank">black metal</a>; it eats souls and feeds its wielder power; it can destroy the avatars of gods and demons, and it inspired a badass Blue Oyster Cult song. Aside from Stormbringer's nasty little tendency to munch on Elric's friends and lovers, what's not to like?

Oh, and here's the BOC song I mentioned.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

## Changeling

This crystalline sword from C. J. Cherryh's science fantasy saga chronicling the adventures of Morgaine and her retainer Vanye is even deadlier than Stormbringer. How do you defend yourself against a sword that opens a black hole at its tip whenever it's drawn?

One word: _run_.

## The Lion-mark Katana from _Kill Bill_

I don't think the sword Hattori Hanzo forged for The Bride despite his vow to never again forge a killing blade has a name. It doesn't need one. It's an extension of The Bride's will, and the instrument of her bloody vengeance. That's good enough for me.

## **Terminus** Est

Gene Wolfe's Book of the New Sun (_Shadow of the Torturer, The Claw of the Conciliator, Sword of the Lictor_, and _The Citadel of the Autarch_) can be slow going for readers unfamiliar with his style. However, the executioner's sword with which the journeyman torturer Severian is gifted as he is exiled by his guild is an exquisite instrument. It's all edge and no point, which makes thrusting attacks impossible, but Severian still wields it to deadly effect on his enemies &#8212; and his facial hair.

## The Unnamed Sword of tegeus-Chromis

I'm going to get _really_ obscure here and mention M. John Harrison's _The Pastel City_, the first and least literary of his Viriconium novels and stories. Its melancholy protagonist, Lord tegeus-Chromis, fancies himself a better poet and musician than he is a swordsman, and doesn't bother to name his sword. Considering the skill with which he wields that blade, he must be a damn good musician.

## Graywand

Fritz Leiber's character Fafhrd is as practical a sort as his parter, the Grey Mouser. Instead of having a special sword named Graywand, Fafhrd simply takes any longsword that's handy, calls it Graywand, and starts kicking ass with it.

What about you? This site has a comments section for a reason. If you have a favorite fictional sword, I want to hear about it.