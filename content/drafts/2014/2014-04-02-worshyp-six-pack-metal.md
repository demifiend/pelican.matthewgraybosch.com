---
title: 'The Worshyp'
except: A thrash metal revival from Canada
date: 2014-04-02T19:16:57+00:00
header:
  image: the-worshyp-evil-abounds.jpg
  teaser: the-worshyp-evil-abounds.jpg
categories:
  - Music
tags:
  - bandcamp
  - Evil Abounds
  - free music
  - heavy metal
  - kick ass
  - Kingdom Earth
  - plug
  - The Worshyp
  - thrash metal
---
This post is for all you metalheads. Toronto thrash revivalists [The Worshyp](http://theworshyp.bandcamp.com/) are running a little giveaway on their Bandcamp page. Get six great selections from their albums _Kingdom Earth_ and _Evil Abounds_ for free. Donations are naturally welcome. I'm plugging these guys because I'm acquainted with lead singer Marz Nova via Facebook, and because I'm one of their fans.

I've been a fan of this band since late 2011, and even mentioned these guys in _Without Bloodshed_. _Kingdom Earth_ is as solid a debut as Iron Maiden's self-titled 1980 debut or Metallica's 1983 LP _Kill 'Em All_. Their follow-up, _Evil Abounds_, belongs with classics like Maiden's _The Number of the B__east_, Metallica's _Master of Puppets_, and Megadeth's _Rust in Peace_.You can get their full albums on Bandcamp, or check them out here.

I recommend "My World", "Left to Die", and "Never Afraid" from _Kingdom Earth_

Standout tracks on _Evil Abounds_ include "Diabolic", "WarTorn", "Destroyer of Man", and "Villains" &mdash; which would be a great song for Imaginos.

<iframe width="560" height="315" src="https://www.youtube.com/embed/QZm3LOVBqig" frameborder="0" allowfullscreen></iframe>

---

Go buy yourself some independent heavy metal. You won't regret it, because these guys are fucking awesome. :metal:

## Kingdom Earth

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A65Sn21fWD1EwwTldQYKrkj" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Evil Abounds

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A0iHtNQIqthc1S3AZeaPeLK" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
