---
title: Hello to Making Light Readers
categories:
  - Website
tags:
  - Heinlein
  - Hello
  - Making Light
  - Teresa Nielsen Hayden
  - Visitors
---
It looks like I've received an unexpected influx of visitors from [Teresa Nielsen Hayden](http://en.wikipedia.org/wiki/Teresa_Nielsen_Hayden)&#8216;s blog [Making Light](http://nielsenhayden.com/makinglight/) courtesy of a link to my opinion of a certain subset of SF fandom which [venerates Heinlein to the point of excluding and alienating SF fans](http://www.matthewgraybosch.com/2014/03/12/dont-care-youve-read-heinlein/) unfamiliar with the Dean's work.

Hello! I took the weekend off, but I hope you'll stick around. You haven't seen me in fandom, but that's no reflection on you or on fandom. I'm just not very sociable.

And if you're reading this, Ms. Nielsen Hayden, thanks for the link.
