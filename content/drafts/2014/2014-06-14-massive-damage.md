---
id: 744
title: Massive Damage
date: 2014-06-14T21:39:32+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=744
permalink: /2014/06/massive-damage/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1PiU0wq
bitly_link_twitter:
  - http://bit.ly/1PiU0wq
bitly_link_facebook:
  - http://bit.ly/1PiU0wq
bitly_link_linkedIn:
  - http://bit.ly/1PiU0wq
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1PiU0wq
bitly_link_reddit:
  - http://bit.ly/1PiU0wq
bitly_link_stumbleupon:
  - http://bit.ly/1PiU0wq
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - bad metaphors
  - giant enemy crab
  - is this guy drunk?
  - massive damage
  - orgasms
  - random
  - sex
  - women
format: image
---
I'm sure you've heard that women aren't just machines that will dispense sex if you put in enough kindness tokens.<figure style="width: 1019px" class="wp-caption aligncenter">

[<img src="http://i0.wp.com/fc06.deviantart.net/fs11/i/2006/242/4/b/_Incredible_Giant_Crab_Redux_by_VegasMike.jpg?resize=840%2C544" alt="Hit the Weak Point for Massive Damage" data-recalc-dims="1" />](http://vegasmike.deviantart.com/art/Incredible-Giant-Crab-Redux-38936734)<figcaption class="wp-caption-text">Hit the Weak Point for Massive Damage</figcaption></figure> 

It gets worse, boys. You know how you can target a giant enemy crab's weak point and reliably inflict massive damage? Well, you can't target a woman's erogenous zones and reliably give her massive orgasms.

Nobody can _make_ a woman come. Not even God. The best any of us can do is help out.

Paradoxically, worrying about whether or not a woman has an orgasm during sex can make things harder for her. So can stress, anxiety, depression, polycystic ovary syndrome, being a virgin until marriage, and excessive guilt about sex.

If you want to know more, <a title="Brown University: Female Orgasm" href="http://www.brown.edu/Student_Services/Health_Services/Health_Education/sexual_health/sexuality/female_orgasm.php" target="_blank">Brown University has a fairly informative page on female orgasms</a>.