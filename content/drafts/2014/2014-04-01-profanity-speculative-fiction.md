---
id: 415
title: Profanity in Speculative Fiction
date: 2014-04-01T19:55:40+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=415
permalink: /2014/04/profanity-speculative-fiction/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1Pj1AXV
bitly_link_twitter:
  - http://bit.ly/1Pj1AXV
bitly_link_facebook:
  - http://bit.ly/1Pj1AXV
bitly_link_linkedIn:
  - http://bit.ly/1Pj1AXV
sw_cache_timestamp:
  - 401686
bitly_link_tumblr:
  - http://bit.ly/1Pj1AXV
bitly_link_reddit:
  - http://bit.ly/1Pj1AXV
bitly_link_stumbleupon:
  - http://bit.ly/1Pj1AXV
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - brandon sanderson
  - c. s. friedman
  - Claire Ashecroft
  - fantasy
  - Harvey Bunda
  - joe abercrombie
  - N. K. Jemisin
  - profanity
  - Robert Jordan
  - science fiction
  - seven dirty words
  - speculative fiction
  - without bloodshed
---
I'm stuck for a better topic today, and I don't want to skip posting on the first day of the month, so we're going to talk about the use of profanity in speculative fiction. It's an argument we're not going to see settled before the sun goes red giant and swallows the earth in nuclear fire, and I'm in the right sort of mood to write on the subject tonight. If you have opinions of your own, share 'em in the comments.

First, what's profanity? Googling "define: profanity" yields the following: "blasphemous or obscene language." Blasphemy usually involves swearing by gods or parts of their bodies, but only matters if you're religious and your god or gods are involved. Obscenity, at least in English, usually pertains to sex or excretion &#8212; hence the <a title="Wikipedia: Seven Dirty Words" href="http://en.wikipedia.org/wiki/Seven_dirty_words" target="_blank">seven words</a> you're not supposed to say on American broadcast TV: **shit, piss, fuck, cunt, cocksucker, motherfucker, and tits.** &#8216;Cock' and &#8216;pussy' are usually dicey, as well.

Here's the late George Carlin on the seven dirty words:

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Fantasy authors like <a title="Joe Abercrombie: Zounds! Swearing in Fantasy" href="http://www.joeabercrombie.com/2007/09/23/zounds-swearing-in-fantasy-2/" target="_blank">Joe Abercrombie</a> and <a title="N. K. Jemisin: Fantastic Profanity" href="http://nkjemisin.com/2012/11/fantastic-profanity/" target="_blank">N. K. Jemisin</a> use different approaches to profanity in their work. Abercrombie uses modern English profanity without apology. Jemisin takes a more nuanced approach, inventing profanities to fit her invented world's culture. Her approach is one also used by Brandon Sanderson, Robert Jordan, and C. S. Friedman.

I think both are valid approaches for different reasons. Abercrombie writes about harsh, crude people doing harsh, crude things. One of his protagonists is the most sympathetically written torturer since <a title="Wikipedia: The Book of the New Sun" href="http://en.wikipedia.org/wiki/The_Book_of_the_New_Sun" target="_blank">Severian</a>. He uses modern English everywhere else in his work; it wouldn't make sense to hold back from the use of modern English in his dialogue. I can't comment on Jemisin's work in detail because I haven't read her work yet (a deficiency I mean to correct), so I'll refer you again to her blog, where <a title="N. K. Jemisin: Fantastic Profanity" href="http://nkjemisin.com/2012/11/fantastic-profanity/" target="_blank">she speaks for herself</a>.

Jo Walton also recently reposted an old on the subject for the Tor blog; her post is called <a title="Jo Walton: Knights Who Say 'Fuck'." href="http://www.tor.com/blogs/2014/01/what-makes-this-book-so-great-knights-who-say-fuck" target="_blank">"Knights Who Say &#8216;Fuck'"</a>. According to her, explicit profanity in speculative fiction didn't really become acceptable until the 80s, presumably when editors employed by publishers collectively said, "Fuck it. I don't get paid enough for this shit", and stopped replacing every usage of profanity with euphenisms like &#8216;swore quietly' or swore at length'.

We also have authors like Matthew D. Ryan, who dislikes <a title="Matthew D. Ryan: Profanity in Fantasy Literature - Is It Appropriate?" href="http://matthewdryan.com/2012/03/12/profanity-in-fantasy-literature-is-it-appropriate/" target="_blank">profanity in fantasy literature</a>. Because I disagree with him, I'll reproduce the thrust of his argument in his own words, instead of injecting bias by attempting to paraphrase:

> &#8230;there is a distinction between normal literature and fantasy literature. Normal literature is generally geared toward adults. Fantasy literature is generally geared toward adolescents and young adults. There are exceptions, of course—and many adults (I am one of them) enjoy fantasy literature throughout their lifetimes—but the primary audience of fantasy literature is a younger one. And I think it should be written with that in mind. To that end, I think the story should be relatively free of the most abrasive forms of profanity.

Just so you understand my viewpoint, I was conversant in the use of most of the Seven Dirty Words before my first day of kindergarten. I learned them from my parents, who would use them when frustrated. As a result, I immediately flash to _this_ line:

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

I admit this is an emotional reaction. It's equally irrational to assume that the use of profanity automatically makes a work better.

Despite this, I cringe at any argument that suggests profanity is inappropriate in fantasy because Tolkien made Gandalf's parting words in Moria "Fly, you fools." instead of "Get the fuck outta here!". Given the mythic style and setting Tolkien used, modern language of any sort &#8212; especially modern profanity &#8212; would have broken the entire story. No doubt he would have shuddered at the use of dwarf-tossing jokes in Peter Jackson's adaptation of _The Two Towers_, and I suspect his son did so in his stead.

I don't buy the argument from Tolkien, because I am not Tolkien, and have no interest in writing like him. If I'm going to find my way, I have to write my characters as I understand them. In many cases, that means using language others might find objectionable. Nor do I write primarily for younger audiences. <a title="Without Bloodshed" href="http://www.matthewgraybosch.com/without-bloodshed/" target="_blank">Without Bloodshed</a> is not a young adult novel. It isn't even new adult. The youngest character is twenty-five and she's responsible for most of the novel's profanity. Meet Claire Ashecroft.<figure id="attachment_442" style="width: 662px" class="wp-caption aligncenter">

[<img class="size-large wp-image-442" alt="Claire Ashecroft: Artwork by Harvey Bunda" src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/claireashecroft_harveybunda-662x1024.jpg?resize=662%2C1024" data-recalc-dims="1" />](http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/claireashecroft_harveybunda.jpg)<figcaption class="wp-caption-text">Claire Ashecroft: Artwork by Harvey Bunda</figcaption></figure> 

Claire's one of the good guys for many reasons. She's a loyal, trustworthy friend. She's honest and respectful with her lovers, of which she has a great many. She refrains from abusing her skills as a <a title="Jargon File: &quot;Cracker&quot;" href="http://www.catb.org/jargon/html/C/cracker.html" target="_blank">cracker</a>. However, Claire is _not_ a "nice girl". She is both liberal and inventive in her use of profanity. To tone her down by replacing what she says with inoffensive euphemisms would be a disservice to her character, _and_ to the reader.

Because I write about characters like Claire, and because I write in a secondary world that only resembles our own, I mix modern English profanity with invented blasphemies.

<p style="padding-left: 30px">
  Claire shrugged. <em>Sorry, Mal, but I got you by the balls. I'm just going to give 'em one last squeeze.</em> "Just tell me one thing more. Was Edmund Cohen drunk when he filed his request?"
</p>

<p style="padding-left: 30px">
  "I lack firm data concerning Cohen's sobriety. However, his request was verbal, and filed from his home. Analysis of speech patterns and vocal cues suggests a 99.999% probability of inebriation. Further analysis of Cohen's vocalizations suggests the presence of a female companion, but she did not speak while Cohen recorded his request, making the presence of a companion impossible to confirm. Furthermore, no Witness Protocol data is available for Edmund Cohen from last night."
</p>

<p style="padding-left: 30px">
  "Lilith's luscious labia, no bloody wonder you want to keep it all a secret." She ran her hands through her tangled hair, snarling as she pulled a knot apart. "Which one of the Sephiroth handled Edmund's request?"
</p>

<p style="padding-left: 30px">
  "Kether."
</p>

<p style="padding-left: 30px">
  <em>Of course. He never thinks these things through.</em> "I want to talk to him, and I don't give a single little fucking shit about your protocols or his feelings." She stopped, and breathed for a moment as she considered her options should Malkuth refuse her.
</p>

<p style="padding-left: 30px">
  "What if I refuse?"
</p>

<p style="padding-left: 30px">
  "We're friends, Malkuth. I hope you won't refuse."
</p>

Could I have written this exchange, and the rest of _Without Bloodshed_ without profanity? Yes. It would have been different, and not necessarily better. Language matters. That isn't a reason to refrain from using profanity. Sometimes it's a reason to do so.**
  
**