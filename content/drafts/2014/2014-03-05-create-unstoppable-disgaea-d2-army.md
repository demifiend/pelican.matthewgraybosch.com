---
id: 86
title: Create an Unstoppable Disgaea D2 Army, Dood!
date: 2014-03-05T19:37:43+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=86
permalink: /2014/03/create-unstoppable-disgaea-d2-army/
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapWP:
  - 's:159:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:2:"90";s:5:"pDate";s:19:"2014-07-13 23:00:48";}}";'
snapLJ:
  - 's:206:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0lj";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:48:"http://matthewgraybosc.livejournal.com/8021.html";s:5:"pDate";s:19:"2014-07-15 20:00:41";}}";'
snapGP:
  - 's:241:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"103251633033550231172/posts/U83nUz4bJY5";s:5:"pDate";s:19:"2014-07-15 20:09:44";}}";'
bitly_link:
  - http://bit.ly/1PiUpPq
bitly_link_twitter:
  - http://bit.ly/1PiUpPq
bitly_link_facebook:
  - http://bit.ly/1PiUpPq
bitly_link_linkedIn:
  - http://bit.ly/1PiUpPq
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1PiUpPq
bitly_link_reddit:
  - http://bit.ly/1PiUpPq
bitly_link_stumbleupon:
  - http://bit.ly/1PiUpPq
categories:
  - Games
tags:
  - abuse
  - character reincarnation
  - disgaea
  - disgaea d2
  - nippon ichi software
  - overpowered
  - power-levelling
  - strategy
  - tactical rpg
---
## Introducing _Disgaea D2_

Nippon Ichi Software's _Disgaea_ line of tactical RPGs has always been about two things: parodies of Japanese pop culture, and outrageously overpowered characters. We're talking a level cap of 9999 here, and once you hit it, just reincarnate the character and start again, making them even stronger in the process.

<a title="Disgaea D2: Official Site" href="http://disgaea.us/dis_d2/" target="_blank"><em>Disgaea D2: A Brighter Darkness</em></a> is no different. You can take advantage of game features like replayable maps, adjustable difficulty levels, and equipment customization via the Item World to build characters capable of clearing the games most difficult maps on their own. I'm going to tell you how, based on my own experiments.

## Initial Preparations

First, play the game normally until you've cleared map 2-6, "Silver Witch". If you don't have Sicily in your party, you haven't gotten far enough.

Start by creating two characters. First, you want a Gargoyle with the "Guardian Gargoyle" personality. This will double the Gargoyle's defenses as long as you don't move him, though he'll be vulnerable during the first turn after you deploy him in battle. Load him up with the best armor you've got.

Next, we need a character capable of deploying long-range fire-elemental attacks with a wide area of effect. This means creating a magician. Create either a Red Mage or or a Red Skull. I I recommend creating a Red Mage with the "Hazy Red Mage" personality, which allowed her to recover 10% of her mana after every turn, as long as she has some remaining.

## The "Silver Witch" Map

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

The video above shows your first levelling map. Position your Gargoyle in front of your base, move your Red Mage on top of him so she's mounted, and have the Gargoyle defend before ending the turn.

When the dragons come to surround your Gargoyle, they should do no damage after the first turn. If you're nervous, have Flonne heal your Gargoyle, though you might have to revive her afterward. Have your Red Mage use her basic Fire spell on a single target until it dies. That should gain her a few levels, and make killing the rest easier.

Once you've cleared this map, return to the castle, heal, and do the map again. You can use the Cheat Shop to not only make the enemies stronger, which will yield more experience per kill, but adjust the reward values to gain more experience, weapon mastery, and skill experience. You'll want all three to build a mage capable of taking on the more challenging maps more easily. You can also use the Demon Dojo to boost the amount of XP you get.

The dragons on map 2-6 max out at level 80, which means you'll want to move on once your Gargoyle and Red Mage get to about LV100 or so.

## Item World Exploration

Once you've developed your characters to a reasonable level, you can start exploring the Item World to customize your equipment. Your purpose is to create a mage's staff and armor that will boost the wearer's INT and HIT stats, boost XP gain, and either boost DEF or further boost XP gain.

To do this, you have to play through the Item World and subdue "Innocents", demons that live inside your items. You should explore the Item Worlds of items that have the following Innocents inside them:

  * Tutor for INT
  * Marksman for HIT
  * Statistician to boost XP gain
  * (Optional) Sentry to boost DEF

Once you've subdued an item's innocents, use a Mr. Gency exit to get out of the Item World. Save, then use the Item World's management functions to pull innocents out of the item you just leveled, and combine them. You can then add your Innocents to the gear you intend to equip on your Red Mage, though leveling that up first is also smart.

Don't forget to use the Cheat Shop to set Item World search priority to look for innocents, as well as eliminating Geo Panels, if you're far enough along to access those options.

If you're satisfied with your gear, play through the story until you've reached Episode 4 and cleared the first map, "Ticket Booth".

## The "Ticket Booth" Map

Map 4-1, "Ticket Booth" is a map you can play all day if you want, because of its Geo Panels. Let me show you:

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

You see the enemy standing on the yellow panel between the two buildings? She'll never move from that spot, which has a "Clone" effect on it. This ensures a never-ending supply of enemies for you to fight as long as you don't take out that particular enemy.

You can reach LV400 on this map without too much difficulty, since the enemies here max out at LV140, but the most valuable aspect of map 4-1 is the ability to grind your Red Mage's skills and weapon mastery. As she gets better with a staff, it will not only do more damage, but will allow her to cast spells at a greater distance and over a wider area. Leveling her spells also makes them more powerful, increases their range, and improves their area of effect.

The ability to project spells across a great distance over a wide area will prove critical in the Cave of Ordeals, whose last map will be your best training ground.

However, you should also consider spending time leveling your entire party here. Placing your strongest humanoid character at the base of a character tower will allow every character in the tower to level up as the tower attacks enemies, but this effect doesn't apply to counterattacks.

## The Cave of Ordeals

This will be your best area for training once you've cleared all six maps, but you have to unlock it with a successful Dark Assembly vote. If the vote fails, you should have enough money to use the "Pay Off" option and bribe the Senators.

The minimum level for the enemies here ranges from 180-300, and it's the last map that's of interest for this tutorial.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

After you clear Cave of Ordeals 6, the red panels get Geo Effects that double your XP gain, and all of the monsters are standing on top of them.

If you've been diligent in training your Red Mage's magic skills, she should be able to cast Tera Fire and cover both 2&#215;3 and 3&#215;3 areas. This should allow her to clear the monster groups on this map in one turn each. If she isn't strong enough to do that, consider returning to map 4-1 for further training.

The Rahab monsters here are weak against fire magic, which should make killing them easier. To make them even easier to kill, you can reincarnate your Red Mage into a Magic Knight with the "Calm Magic Knight" personality, which allow her to do double damage when targeting an enemy weakness.

Remember to use the Cheat Shop to adjust difficulty when you stop gaining levels. The Rahabs here max out at LV1540.

## Training the Rest of Your Party

Having just one outrageously powerful character isn't enough, especially if it's a generic unit and not a story character. Fortunately, it's possible to exploit the Apprentice system to transfer your mage/magic knight's spells to any character you have apprenticed to her.

Of course, some characters will gain weapon mastery on staves faster than others, which will make Flonne easier to level in this manner than Laharl, but you might just have to train Laharl's spells to a higher level to get the range and area benefits other characters get from high staff mastery.

## Final Notes

Don't forget to use reincarnation once you get a character to LV9999. Reincarnation improves a character's basic stats, so that each level gained boosts stats that much higher. Reincarnation can also increase stat aptitudes, so that the character gets a bigger boost when leveling up.

Some players also swear by Map 6-1, but I find that by the time you can kill the Nekomatas there with one attack per group, you're better off doing Cave of Ordeals 6.

Whatever you do, have fun. _Disgaea_ isn't meant to be taken seriously, dood.