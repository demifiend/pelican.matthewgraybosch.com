---
title: 'Dire Straits: &quot;Money for Nothing&quot;'
excerpt: I want my... I want my... I want my BSD!
date: 2014-03-10T13:09:05+00:00
header:
  image: dire-straits-money-for-nothing-video.jpg
  teaser: dire-straits-money-for-nothing-video.jpg
categories:
  - Music
tags:
  - 1980s
  - BSD
  - classic
  - Dire Straits
  - Brothers in Arms
  - I want my BSD
  - I don't want your MTV
  - Money for Nothing
  - MTV
---
I turned on the radio for shits and giggles while driving back to the office after lunch, and "Money for Nothing" was on. I haven't heard it in years. Anyone else remember this video?

<iframe width="420" height="315" src="https://www.youtube.com/embed/lAD6Obi7Cag" frameborder="0" allowfullscreen></iframe>

---

Talk about low polygon counts...

I hear Dire Straits changed the lyrics so that they're not using "faggot" when they perform "Money for Nothing" live, which works for me.

Yeah, back in the day you could get away with talking shit like that. Sometimes things _do_ get better.

And if you're a fan of Berkeley-style Unix, "I want my BSD" works just as well. But I haven't wanted your MTV since they killed off *Headbanger's Ball*. :metal:
