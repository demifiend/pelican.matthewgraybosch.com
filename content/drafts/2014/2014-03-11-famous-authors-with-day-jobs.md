---
id: 198
title: Famous Authors with Day Jobs
date: 2014-03-11T16:03:29+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=198
permalink: /2014/03/famous-authors-with-day-jobs/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1L2dIJY
bitly_link_twitter:
  - http://bit.ly/1L2dIJY
bitly_link_facebook:
  - http://bit.ly/1L2dIJY
bitly_link_linkedIn:
  - http://bit.ly/1L2dIJY
sw_cache_timestamp:
  - 401666
bitly_link_tumblr:
  - http://bit.ly/1L2dIJY
bitly_link_reddit:
  - http://bit.ly/1L2dIJY
bitly_link_stumbleupon:
  - http://bit.ly/1L2dIJY
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - anthony trollope
  - day jobs
  - encouragement
  - famous authors
  - follow-up
  - glen cook
  - inspiration
  - joseph heller
  - stephen king
  - t.s. eliot
  - toni morrison
  - william faulkner
---
To follow up yesterday's post encouraging you to <a title="Become a Lunch Break Novelist" href="http://www.matthewgraybosch.com/2014/03/10/become-lunch-break-novelist/" target="_blank">become a lunch break novelist</a>, here are some famous authors with day jobs. I'm not one of them yet, because I haven't made the big time. Maybe you can help me with that, but let's get you on _your_ way first.

Chances are you've heard of some of these writers, but you might have forgotten that they worked for a living before they became famous. Some never lived to profit from their writing and quit their day jobs.

<a title="Is the Key to Becoming a Great Writer Having a Day Job?" href="http://www.slate.com/articles/arts/culturebox/features/2013/daily_rituals/is_the_key_to_becoming_a_great_writer_having_a_day_job.html" target="_blank">Anthony Trollope</a> worked at a post office for 33 years, and would write for three hours every morning before going to work. The crazy bastard managed to belt out two dozen books despite not having modern tech. However, the lack of modern tech meant he had fewer distractions available. If he was on a social network, it probably met in person at a nearby bar.

<a title="Is the Key to Becoming a Great Writer Having a Day Job?" href="http://www.slate.com/articles/arts/culturebox/features/2013/daily_rituals/is_the_key_to_becoming_a_great_writer_having_a_day_job.html" target="_blank">William Faulkner</a> wrote _As I Lay Dying_, giving the <a title="As I Lay Dying" href="http://asilaydying.com/" target="_blank">metalcore band of the same name</a> a literary pedigree, in the afternoon before visiting his mother for coffee on his way to night shifts at a university power plant. He'd catnap on the job, and sleep in the mornings. Nice work if you can get it.

<a title="Is the Key to Becoming a Great Writer Having a Day Job?" href="http://www.slate.com/articles/arts/culturebox/features/2013/daily_rituals/is_the_key_to_becoming_a_great_writer_having_a_day_job.html" target="_blank">Joseph Heller</a> worked in advertising, and wrote _Catch-22_ over the course of eight years at night. Reading this, I can't help but wonder if he was actually writing about military life, or life on Madison Avenue.

<a title="Is the Key to Becoming a Great Writer Having a Day Job?" href="http://www.slate.com/articles/arts/culturebox/features/2013/daily_rituals/is_the_key_to_becoming_a_great_writer_having_a_day_job.html" target="_blank">Toni Morrison</a> makes these other guys look lazy. Not only did she work as an editor, but she taught university literature courses while raising two sons on her own. And I thought _I_ had it tough because I work as a programmer.

<a title="What 20 Of the World's Most Famous Writers Were Doing in Their Twenties " href="http://www.policymic.com/articles/66757/what-20-of-the-world-s-most-famous-writers-were-doing-in-their-twenties" target="_blank">Stephen King</a> worked in a laundry, was a janitor, and taught English while writing Carrie and supporting his wife and kids.

Poet <a title="Before They Were Famous: The Oddest Odd Jobs of 10 Literary Greats" href="http://www.writersdigest.com/editor-blogs/there-are-no-rules/before-they-were-famous-the-oddest-odd-jobs-of-10-literary-greats-2" target="_blank">T. S. Eliot</a> worked for Lloyds Bank of London while composing _The Waste Land._ Having read the poem myself, I can't help but think a progressive metal band should set it to music. "I will show you fear in a handful of dust" is probably the most _metal_ line of poetry since Homer, who opened one of the most metal epics in human history as follows (using Robert Fagles' translation):

> Rage—Goddess, sing the rage of Peleus' son Achilles,
  
> murderous, doomed, that cost the Achaeans countless losses,
  
> hurling down to the House of Death so many sturdy souls,
  
> great fighters' souls, but made their bodies carrion,
  
> feasts for the dogs and birds,
  
> and the will of Zeus was moving towards its end.
  
> Begin, Muse, when the two first broke and clashed,
  
> Agamemnon lord of men and brilliant Achilles.

<a title="Before They Were Famous: The Oddest Odd Jobs of 10 Literary Greats" href="http://www.writersdigest.com/editor-blogs/there-are-no-rules/before-they-were-famous-the-oddest-odd-jobs-of-10-literary-greats-2" target="_blank">Langston Hughes</a>, one of the most prominent poets of the <a title="the Harlem Renaissance" href="http://en.wikipedia.org/wiki/Harlem_Renaissance" target="_blank">Harlem Renaissance</a>, held a busboy's job while writing. He must have chugged a can of Liquid Temerity before approaching poet <a title="Vachel Lindsay" href="http://en.wikipedia.org/wiki/Vachel_Lindsay" target="_blank">Vachel Lindsay</a> with his own work while the latter was eating at the restaurant for which Hughes worked.

<a title="11 Authors Who Kept Their Day Jobs" href="http://www.huffingtonpost.com/2013/09/02/author-jobs_n_3845323.html" target="_blank">Bram Stoker</a> worked as actor Henry Irving's personal assistant while also managing the Lyceum Theatre in London. Though, as the HuffPo article snarkily suggests, the epistolary nature of _Dracula_ no doubt made it necessary for him to keep his day job.

<a title="SF Site: Interview with Glen Cook" href="http://www.sfsite.com/10a/gc209.htm" target="_blank">Navy veteran and military fantasy novelist Glen Cook</a> &#8212; who isn't as famous as he deserves to be for Garrett, PI; The Black Company; the Dread Empire; and The Instrumentalities of the Night (of which the latest, _Working God's Mischief_ is coming real soon now) &#8212; used to work for GM. During the day, he made cars. After work, he wrote badass fantasy and science fiction. Though I haven't read it myself, I've heard _Passage at Arms_ described as the _Das Boot_ of science fiction.

They did it. I'm doing it. You can do it, too, so stop making excuses for yourself. Don't make me come over there with a PA and assault your neighbourhood with <a title="Death: The Sound of Perseverance" href="http://en.wikipedia.org/wiki/The_Sound_of_Perseverance" target="_blank">the sound of perseverance</a>. 🙂