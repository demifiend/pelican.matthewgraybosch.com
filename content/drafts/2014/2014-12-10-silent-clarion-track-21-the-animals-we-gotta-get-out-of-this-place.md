---
title: "*Silent Clarion*, Track 21: &quot;We Gotta Get Out of This Place&quot; by The Animals"
excerpt: "Check out chapter 21 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
If I harbored any lingering doubts concerning Sheriff Robinson as a side effect of either our first meeting or the usability of his volunteers, he dispelled them when told the reports of Fort Clarion's abandonment were greatly exaggerated (to paraphrase a classic). He paled, his jaw clenching as he immediately issued a general order. ｢The base is inhabited. Prepare to evacuate, and await instructions.｣

He didn't wait for my approval before issuing a second order. ｢Tower teams, cover everybody on the ground. Once they're out, I want Tower Alpha to cover Tower Bravo. Adversary Bradleigh and I will cover Tower Alpha.｣

Mike's shotgun snicked shut. ｢Count me in, Adversary.｣

｢Thanks, Mike.｣ It was a sensible plan, despite a fearful chill running down my spine at the thought of being one of the last to get out of Fort Clarion. Robinson was right; a leader should lead the charge from the front, and guard the rear in retreat.

A chorus of rogers pinged our implants, along with a question. ｢Sheriff, this is Bravo Lead. What should we do with the rifle we found?｣

Robinson glanced at me instead of replying. ｢Your orders, Adversary?｣

The smart thing to do would be to take the rifle, rather than leave it for the enemy to use. However, that wasn't SOP for an arms control job. All arms and ordnance were to remain in situ until the disposal team confirmed the inventory and signed off. However, the protocol assumed that installations like Fort Clarion are uninhabited.

Hopefully, I won't regret issuing this order. ｢Bravo Lead, bring the weapon directly to me.｣

｢Roger, ma'am.｣

Mike, the Sheriff, and I formed a triangle around Dr. Petersen to protect the unarmed doctor. I counted every volunteer passing us as they retreated through the ruined gate.

｢This is Alpha Lead. I count ten irregulars out and eighty youth volunteers out. Please confirm, Bravo Lead.｣

Martin replied. ｢Confirmed, Alpha Lead. Beginning our retreat.｣

Tightening my grip on the revolver, I held fast, resisting the urge to hasten the remaining volunteers as they descended the western tower. Ms. Martin stopped as her team passed me, and pressed an unexpectedly heavy rifle case into my hands. ｢Did Brubaker bring you the photograph?｣

｢Yes. Thanks for being discreet.｣

A cloud darkened her expression, suggesting the reality underlying country life in Clarion was anything but idyllic. ｢I hope you fucking crucify the creep. I'll help hold him down if you need backup.｣

A rather vengeful sentiment, but I sympathized. ｢Noted. I suppose you had some trouble of your own?｣

Martin nodded. ｢Yeah. I never found out who, or I would have filed a complaint with the Phoenix Society.｣

｢File a complaint anyway when you get home.｣ But that wasn't what she needed to hear. ｢I'm sorry you were denied justice. If I catch the creep who photographed me, I'll be sure to find out who else they've harmed.｣

｢Thanks, Adversary.｣ Martin brightened as she snapped out a smart salute, hand over heart, before rushing off to catch up with her fireteam.

We escaped Fort Clarion and returned to town without incident. Keeping the rifle, I made tracks for the Town Hall after dismissing Mike, the militia, and the youth volunteers. Cat bounced out of her chair. "How did it go, Adversary Bradleigh?"

"I need to report to Mayor Collins as soon as possible. Can you tell him I'm here?"

"Of course." She shot a glance at Sheriff Robinson. "Will you be reporting as well?"

Robinson nodded. "Yeah. We need Mayor Collins to order a full muster of the Clarion Volunteers. I haven't got the authority to do it myself."

At least nobody got hurt before the Sheriff started taking the mission seriously. He must have caught my expression, for he turned his attention to me. "I misjudged the situation, Adversary. I'm sorry. We'll get you the support you need if I have to march people down there at gunpoint."

"Thank you." Offering my hand to show I accepted his apology, I glanced at the conference room the Mayor set aside for my use. "I need to show you something."

"All right." Robinson followed me inside, and closed the doors behind him. "Is this about that rifle?"

"It may be related." I retrieved the photo Martin had Brubaker bring to me from inside my jacket, and showed it to Robinson. "Sergeant Martin found this with the rifle, but didn't mention it on the air."

Taking the photograph from me, Robinson sat down and studied it in silence. His jaw clenched several times, as if he had something to say but choked it back. "This was taken with a Solaroid Instant, Adversary. Nationfall put the manufacturer out of business. Nobody makes them anymore even after the Phoenix Society placed all pre-Nationfall copyrights and patents in the public domain."

A camera model that hasn't been manufactured since Nationfall would most likely be a rarity by now. I'd be shocked if more than a handful of people in Clarion owned a Solaroid Instant. "Sounds like I should find an avid camera collector and ask them some questions."

Robinson wouldn't speak. He wouldn't meet my eyes. His shame was suggestive, but I had to ask the question. Drawing my sword, I let him have a good long look at the edge as I leaned in to whisper in his ear. "Was the camera yours, Sheriff?"

"I wasn't the photographer, Adversary. I know I rubbed you the wrong way when I asked to search your room, and I made your job harder than it had to be today, but I didn't take that photograph. I'll take any oath you ask of me, and swear it by any power you venerate."

"I don't want your oath, Sheriff. I believe you."

"Why would you believe me?"

"Because if I find out you're lying, your last sight before I carve your eyes from your skull will be that of crows fighting over your tongue. Have I made myself clear, Sheriff?" He nodded, and I sheathed my sword to offer my hand. "But as I said, I believe you. What happened to your Solaroid?"

"Somebody burglarized my house the day you arrived in Clarion. They took the camera, my best hunting bow, my arrows, and some cash. I filed an insurance claim and thought no more about it until I saw that photo."

Burglary in broad daylight? That's pretty damned bold for a town like Clarion. "Is that why you wanted to search my room?"

"Not exactly. Somebody phoned in a tip suggesting I check you out." He offered me the photo. "Here. You'll want this as evidence, right? It looks like somebody wants you whacked. Once we have people at our disposal, I'll organize a guard detail for you."

That's just what I need: sheriff's deputies or irregulars from the Clarion Volunteers up my ass wherever I go. We would just be putting more people at risk. While a competent sniper could take me out directly with a head shot, a lesser marksman might take out the guards to open up a clear shot. Worse, a sniper might ignore me in favor of my protectors to terrorize the populace. "I don't want to panic the residents without cause. A garrisoned fort in the woods is one thing, but a sniper in town is a different matter. Besides, you can escort me back to the Lonely Mountain after our debrief."

"It's the least I can do." Robinson coughed as Cat opened the doors. "Looks like Brian's ready to see us."

Mayor Collins might have been ready for us, but I daresay he seemed edgy. Maybe he was just reluctant to hear our news. "Adversary Bradleigh, I understand you discovered that Fort Clarion is inhabited. By whom?"

I told him everything I knew thus far, rounding up with, "The state in which we found Fort Clarion suggests a level of discipline that precludes the possibility of the fort being a retreat for geeks. I need additional resources to flush out the inhabitants and neutralize them."

"Why couldn't you do the job with the volunteers Sheriff Robinson provided today?"

Robinson spoke up before I could. Probably a good thing; the Mayor was starting to annoy me. "Your Honor, I only provided Adversary Bradleigh twenty irregulars from the Clarion Volunteers. The rest were too young for militia duty."

A peevish tone crept into Collins' voice. "The town has been safe so far. I see no reason that this can't wait until after the harvest. The town can't afford to let the crops wilt in the fields while farmers go gallivanting through the forest playing soldier. Not to mention the upcoming Clarion Rocks festival."

"Fort Clarion's safe as long as you're not a tourist. How many have you managed to lose to the forest over the last decade, Your Honor?"

"You're being paranoid, Adversary. I'll not tolerate any slander concerning Clarion's safety."

Paranoid? Slander? My sword-hand twitched as I choked off the urge to bare steel and cut this gaslighting choad. A slice across his forehead wouldn't kill him. Hell, it might even give him the sort of scar that lends an otherwise unprepossessing man an attractive hint of danger. God knows he could use it, now that I've had a good look at him. "Were I paranoid, Your Honor, I might suspect you of obstructing a Phoenix Society mission. But that's a dreadfully serious charge, and surely you wouldn't be that foolish. *Are* you that foolish?"

Collins rose, his eyes going narrow and piggy. "Who are you to threaten me?"

Shaking my head, I produced my ID. "A sworn Adversary in service to the Phoenix Society, remember? As such, I am authorized to do far worse than threaten you. Sit your arse down and do as you're told, and I'll refrain from giving your deputy mayor an unexpected promotion." A moment's research gave me the information I required. "This is how it's going to be, Your Honor. Since the harvest is indeed important, surely you and your brother won't mind shutting down the Collins Glass Works for the duration of my mission. This will place five hundred irregulars at our disposal, should they all volunteer for militia duty. Since the Phoenix Society pays militia volunteers time and a half, this should give me the forces I require without interfering with the harvest."

"B-b-b-but my brother just landed a huge order! He can't afford to halt production!"

My sword was out in a flash, its tip pressing the end of the Mayor's nose. It was just the thing to clarify his situation. "Your Honor, have I stumbled upon a conflict of interest meriting a forensic accountant's attention, or just an inability to prioritize? Your sole priority should be getting me the required personnel. If your brother needs to hire temporary workers, he is welcome to apply to the Society for compensation."

Leaving the 'or else' part unspoken, I sheathed my blade, turned on my heel, and left the Mayor seething. He can't be so stupid that I need to spell out the rest for him, can he?

---

### This Week's Theme Song

"We Gotta Get Out of This Place" by The Animals

{% youtube wJVpihgwE18 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
