Silent Clarion, Track 01: Nemesea - "No More"
#############################################

:date: 2014-07-23 12:00
:tags: new adult, science fiction, science fantasy, romance, breakup, sci-fi, sci-fi romance, post-apocalyptic, cyberpunk, naomi bradleigh, london, vampires, conspiracy, starbreaker, nemesea, no more, draft, web serial
:category: Starbreaker: Silent Clarion
:slug: silent-clarion-track-01-nemesea-no-more
:summary: In Chapter 1 of Silent Clarion, we can't tell the story of Naomi Bradleigh's post-breakup working vacation from Hell without showing the breakup.

London can be a cold city, and my duties as an Adversary often demand that I face it at her coldest. Not that it bothers me. It only makes my nights hotter by comparison.

I expected to find John asleep after finishing my shower. In his last year of residency at an Ohrmazd Medical Group hospital, by necessity he often dozed after loving me. I wholeheartedly encouraged him to. Tired people err, and in our lines of work errors cost lives.

Instead, I found him stretched across the bed naked and reading a medical journal. I sat on the edge of the bed beside him, and dragged my fingertip down his spine to make him shiver.

He rolled and smiled up at me. "What were you singing in there, Naomi? I heard something about someone named Sophia."

"Just another gnostic metal band I recently discovered. Lucifer Invictus. They did a show with Seiten Taisei last week." Since I finally prevailed upon him to come to my flat after our date, I could show him the record instead of just pulling up a digital recording like I usually did when I wanted to share new music with him. I wasn't about to bring vinyl records to the hotels John usually picked for our trysts he still lived in the family home, since that also required dragging the player along.

We listened together as I dried my hair. As I began to comb it, John took the comb and began working out the tangles for me. He was less patient than I, but would stop and kiss my ears if he heard me growl in pain. He was fond of my ears, which were pointed like a cat's, covered in white fur that blended with my hair, and lay flat against my skull, pointing backward.

When he was done, I pushed him down on his back and settled beside him, my arm draped over his chest. I rested my head on his shoulder and studied him. His face was angular, and his default expression pensive. "Did you have a difficult surgery today?"

John shifted beneath me and pressed his thin lips against mine. Their softness always surprised me. "No. I have today and the next three days off because of the hours I worked over the last month."

He kissed me again, his fingertips tracing random patterns on my skin, but it was too soon for him to take me again. At thirty, he no longer possessed the rampant hunger of men my age. I never minded, though I daresay my foster mother had other things in mind when she taught me to value quality over quantity.

Our affair sparked a little scandal at its start. I have CPMD, congenital pseudofeline morphological disorder. I grew up in a foster family, with no record of my actual parentage - though my unknown parents never renounced their parental rights. Leaving home at fourteen to study music in New York while also attending Adversary Candidate School was simply not done in John's circles. Furthermore, I lived in indentured servitude; the Phoenix Society agreed to finance my education if I agreed to a dual course and a minimum of two years of service as an Adversary once I was finished.

John came from one of the few rich aristocratic families to survive Nationfall. I suspect many of his circle thought me a fortune hunter, though only one dared say so to my face. Were I not an officer of the Phoenix Society, I would have rewarded his cousin's insult by letting him choose the terms of our duel.

Instead of pressing John to talk, I found pleasure in his embrace. I tasted him, and his skin was still sweat-salty from his prior efforts on my behalf. He sighed beneath me. "Do you love me, Naomi?"

Every man I ever dated eventually asked this question, or credited me with making them be the first to profess their love. I enjoy John's company. He's intelligent, serious, and frequently witty. He does useful, meaningful work. I love his hands and mouth on me.

But he's never swept me off my feet as if we were the leads in an epic romance. I met him in the course of my duties, and decided after fifteen minutes' conversation that if he were willing, I would take him for a lover. I began our affair expecting it to run its course.

I kissed him. "I suppose we're due for this conversation after a year together. Is that what's keeping you awake?"

I meant the latter half in jest, but his expression hardened. "I'm serious, Naomi. I need to know how you feel about me."

I countered his question. "Has your family started giving you grief about me again?"

John nodded, and shifted as if he meant to sit up. I stood, poured the last of the champagne, and gave him the glass containing more. He drained it, and sat staring into it for a long moment. "How much do you know about my family?"

I admit it: I used my implant, a Society-issued bit of tech that served primarily to keep tabs on me during the course of my duties using Witness Protocol, to search the network for publicly available information. I never cared about John's family before, because I had no inclination to join it by marrying the man. "You come from the old British peerage, and your family survived Nationfall with its fortune mostly intact. Your father would have held a title of some sort under the old regime, and a seat in the House of Lords."

John nodded. "Did you know this before we got involved?"

"No. Has that cousin of yours been slandering me again?"

"It's not my arsehole cousin, Naomi." John looked away for a moment, as if ashamed. "It's the whole demon-ridden family. I'm the oldest son, and I'm under pressure."

"So, you're thinking about having children?"

"Yeah. And my family seems to have the mother picked out for me. I met her this morning."

I put aside my glass, and slipped into a fresh pair of panties and a camisole. The material brushed against the vestigial nipples on my belly, making me shiver a little. It was another sign of my CPMD.

The men who wanted me most were men uninterested in marriage and parenthood. My exotic appearance drew them, which suited me thus far. Though I'm human, I possess some feline characteristics. My pupils are slit-shaped, instead of round. My fingernails grow into claws if I leave them untrimmed. "John, I know it's outside your specialty, but have you ever heard of couples like us having children?"

He shook his head. "No." He paused, as if to collect his thoughts. "Look, Naomi, I wanted to know how you felt about me so I could figure out how to explain this. I never mentioned children before because I figured our age difference would make our relationship a temporary thing."

"I'm only ten years your junior, and I left home at fourteen."

"I thought you'd get bored with me and meet somebody your age, but you stuck around. And I stuck with you. But I have responsibilities to my family. They need me to marry a young lady from a family with whom we frequently do business. It would unite our holdings and make our business ventures stronger, in addition to continuing our lines into the future."

I closed my eyes for a moment, and strangled the urge to fly to John and beg him to defy his family for my sake. I never wanted a permanent relationship, but I had always been the one to end it. Welcome to how the other half feels, I suppose. "This isn't how I wanted us to part."

John smiled at me. "Who says it has to end?"

"You're going to marry someone with whom you can have children, John. Of course we have to say goodbye."

"Not if you want to be my mistress."

I suppose some people might have jumped at the opportunity to be kept in style by a lover who cherished them enough to willfully transgress the expectations of fidelity society places upon married people. I can't condemn them. Despite this, I would not join their ranks for John's sake. My voice sharpened. "Am I supposed to be flattered?"

"You're angry with me."

"I assume you haven't been with her yet, so you're plotting to cheat on a woman you don't know and haven't even touched."

John must have found something intriguing on my floor, because he had stopped looking at me. "I spent the morning with her before I agreed to marry her. She wasn't as good as you."

"But she's good enough to serve as breeding stock?" I wanted nothing more than just cause to run him through. Learning he cheated on me with his bride-to-be wasn't quite enough. I retrieved the revolver from my nightstand drawer, cocked it, and aimed between his thighs. "Get dressed. Get out of my flat. If you ever speak to me again, you'll be the last of your line."

Once John was gone, I shoved myself into workout clothes and grabbed a practice sword. I ran down to Valkyrie Gym, which was always open, and told the first man I saw to spot me on free weights. I took inordinate pleasure in shooting him down after impressing him with my strength. Once I was done, I went upstairs to the dojo and took on the half-dozen students working on their swordplay.

I didn't come home until I had finished taking out my hurt and humiliation on those poor bastards. I texted my parents to tell them I had dumped John, and curled up on the couch, ashamed of what I had done to purge my anger. I held my sword close the way I used to hug my cuddle toys.

This was hardly the manner in which I wanted to spend my first anniversary. Maybe there will be some justice in the world after all, and John's fiancée will give him crabs. I smiled at the notion, and snuggled into my pillow.

Theme Song
==========

You can listen to this chapter's song on YouTube.

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oaaVz12ncds" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Thanks for reading!
===================

If you enjoyed this chapter, you can `buy the whole novel on Amazon`_.

.. _buy the whole novel on Amazon: https://www.amazon.com/Silent-Clarion-Collection-Matthew-Graybosch-ebook/dp/B01MCWCSPA/
