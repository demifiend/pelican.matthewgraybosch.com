---
title: "*Silent Clarion*, Track 20: &quot;Every Breath You Take&quot; by The Police"
excerpt: "Check out chapter 20 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Peering into Fort Clarion over the fence wasn't the same as stepping inside. My ears strained for the non-existent sounds of the garrisoned military base. But no sergeants barked orders at enlisted men. Instead of the synchronized beat of well-trained soldiers drilling, only the soft padding of the older irregulars behind me broke the afternoon quiet.

I had to strain my ears to hear most of them. They followed with such quiet efficiency, it was hard not to mistake them for professional soldiers.

Those too young for militia service waited outside. Without firearms and proper training, they'd be a liability if we encountered resistance. Probably should have refused to let them come along, but hurting their pride with a rejection would do little for community relations.

Michael Brubaker kept pace to my left, and I had Robinson at the rear, with Petersen between us. No way was I leaving them behind. Passing the gate placed us on Gen. George Prevost Street, near the post exchange and rows of mass-produced single-family houses reserved for civilian contractors. We kept our rifles at the ready, covering every angle as we advanced, but encountered nothing as we reached the PX.

Armed with a satellite map, I had worked out a rough plan on the way here. Recalling that glint from the western watchtower, I slipped behind cover and scanned both towers as the others followed suit. Nothing untoward this time, but a bit of insurance wouldn't hurt. ｢Sheriff Robinson, I need a fireteam with good marksmen in each of those watchtowers. Everybody else should find cover.｣

｢Yes, ma'am. Rodriguez and Martin, assemble fireteams and take those watchtowers. Report any contacts, but do not engage until fired upon. The rest of you find cover like Adversary Bradleigh suggested.｣

Superimposing the fireteam leads' IPs on my map allowed me to track their progress. They advanced steadily from cover to cover until they had reached the towers.

Rodriguez was first to get to his post. ｢Alpha Lead reporting. No contacts. The tower is ours.｣

｢Bravo Lead reporting. No contacts. The tower is ours.｣ Before I could congratulate them on a job well done, Martin continued. ｢We found something of interest. Sending photos.｣

Seconds later, my implant displayed a translucent image of a rifle case leaning against the wall. Zooming in, I read the label: 3rd Infantry Division—Squad Designated Marksman Rifle. It was a Western counterpart to Eddie Cohen's Dragunov. ｢Bravo Lead, is there anything in that case?｣

｢Can't say for sure without touching it, Adversary.｣

｢Open it up.｣

｢Roger. The weapon is present. Its condition indicates recent handling.｣

Son of a bitch! How long did someone watch me before I spotted the glint from that scope. Why didn't they shoot? The knowledge that somebody had me in their sights, but chose to refrain from blowing my head off, left me shuddering despite the sun's warmth. I dared not count on being so lucky again.

｢You okay, Naomi?｣ So, Michael noticed that Martin's report had unnerved me. Better get it together before Robinson or Petersen notice as well.

｢We'll talk about it later.｣ I shifted back to the main channel as I decided what to do about that weapon. I didn't have a safe place to put it at the moment, so there was no point in sending a youth volunteer up to the tower to retrieve it. ｢Bravo Lead, leave that rifle in place for now.｣

｢Roger.｣

｢Sheriff Robinson, issue a general order. Nobody is to touch anything without my order. I want to leave no sign of our presence. Any weapons found should be photographed and left in place. Photos and serial numbers of all weapons should be sent directly to me.｣

｢Directly to you, Adversary? Are you sure?｣

Why was Robinson questioning my orders? Did he hope for a chance to steal ordnance to sell on the black market before I could catalog it for the disposal crew? ｢Quite sure, Sheriff. I want all photos and counts sent directly to me. This is a Phoenix Society operation, and thus all data is my responsibility.｣

｢Yes, ma'am.｣

Robinson issued the orders without further delay, but Dr. Petersen shot me a look. What exactly does that man know? ｢Dr. Petersen, we need to talk about your tenure as CO here at Fort Clarion.｣

He nodded with a small, tight smile. ｢Feel free to schedule a time in advance, so I can have my attorney present.｣

So, the good doctor wants to lawyer up before we have our little chat? That's his right, but now he's really got me curious. ｢Afraid of self-incrimination?｣

｢No, but the Phoenix Society uses a broad definition for war crimes, and recognizes no statute of limitations. A lawyer's presence would be prudent.｣ He paused a moment while searching his pockets. He withdrew a ring of keys and tossed them to me. ｢For what it's worth, Adversary, I agree that the weapons stored here should not end up in civilian hands.｣

I tried the key labeled 'PX.' It worked perfectly, but unlocking the door also turned on the power inside. Lights flared to life, and the automatic doors slid open with a soft whir.

A bubbly young woman's voice chirped from speakers mounted in the ceiling as I stepped inside with my rifle pressed tight against my shoulder. "Welcome to the Fort Clarion Post Exchange! If you're a member of the Commonwealth's armed services, thank you for your courage and dedication. If you're a civilian, please support the troops by purchasing souvenirs of your visit."

No way that can be an AI. The Sephiroth were the first, and they were activated after Nationfall. The greeting must have been a recording controlled by a motion detector. ｢Everybody wait outside for my mark. Tower teams, I want eyes on the PX. Give a shout if you see hostiles.｣

Rodriguez and Martin acknowledged, but Robinson had questions. ｢Sure you don't want backup, Adversary?｣

｢I've got this, Sheriff.｣ The PX was mostly open space, divided by long rows of empty shelves. Securing the building only took a couple of minutes. ｢Clear!｣

Brubaker, Robinson, Petersen, and five of the ten adult irregulars still with me trooped in, setting off back to back greetings until some idiot named Hubertson unplugged the speakers. Rather than let Robinson deal with it, I descended upon him. ｢Plug those speakers back in. I know the recording's obnoxious, but if somebody walks in that door, I want to hear about it.｣

Hubertson protested. ｢But Adversary, it'll play for friendlies, too.｣

｢Do I look like I give a flying fuck? Plug those demon-ridden speakers back in, and get out. You're on sentry duty. Pick a buddy on your way.｣

｢Yes, Adversary.｣ The irregular quickly plugged the speakers in, and left the PX. He didn't look at me while doing so, leaving me wondering if I had been too harsh.

The others spread out, poking around the PX as I inspected the back office. The most interesting things there were an old Underwood PC I was able to boot using HermitCrab, and some old magazines. Most were military-themed and bore titles like *Modern Soldier*, *Mercenary*, and *Tactics Quarterly*.

These hid an issue of a men's magazine called *Tomcat*. The cover model was a pale, blue-eyed, snow-blonde whose face resembled my own. I slipped it into the case containing my loaner laptop. Leading this crew would be hard enough without questions about my ancestry making the rounds along with whatever salacious photos the magazine might contain.

｢Sheriff, it's time to secure the rest of the base. I want two irregulars guarding the PX at all times. Check with Hubertson and confirm he found a partner. Have the rest gather teams of ten from the youth volunteers.｣

｢Yes, ma'am.｣ Why can't I shake the suspicion that Robinson is just waiting for an opportunity to stick a knife in my back? Was it that our first meeting rubbed me the wrong way? Or am I still miffed about only having twenty trained militia members at my disposal?

｢Wait. Have the adult volunteers come to me for keys, so we don't have to kick down doors.｣

Once I handed out keys, pointedly ignoring the pained look on Dr. Petersen's face, I set my sights on the barracks and shot a quick text to Mike, the Sheriff, and Dr. Petersen. ｢Follow me.｣

The barracks interior was no less pristine than the rest of the base. Surfaces that should have gathered decades' worth of dust were clean enough to withstand an officer's white glove. Every footlocker was secured, with two under each bunk. Even the heads sparkled as if scrubbed fresh this morning, and the tang of cleaning chemicals stung my nose.

I found Mike sniffing the air near the door to the mess hall. ｢What's wrong?｣

｢If the base had been abandoned for decades, would we smell food?｣

Stepping into the mess, I tasted the air. Brubaker was right; the scent of recent cooking lingered, mainly roasted meat. Inside the walk-in refrigerator, fresh deer and wild pigs hung by their feet, skinned and ready for the butcher's blade, the last of their blood dripping from their carcasess and seeping into the floor drain. The larder contained fresh vegetables and unopened canned goods with recent packing dates.

Drawing my revolver, I backed out of the kitchen. ｢You're right, Mike. It smells fishy.｣

He didn't answer. Instead, he stared, aghast, at a small rectangular card I didn't recognize until he handed it to me. It was a photograph. A photograph of me undressing in my room at the Lonely Mountain.

Was he embarrassed for my sake, or because he liked the photograph? Somebody had shot the photo from a distance, using a telephoto lens. They caught me while lifting the hem of my camisole, which exposed a pair of my extra nipples, but little else.

It's quite tame compared to the selfies I've sent some of my lovers, but the scene makes the picture. This was the sort of photograph a private investigator might take for a client. Worse, somebody—the photographer, perhaps?—wrote 'primary target' on the back. Somebody wanted me dead. Somebody had a golden opportunity yesterday, and didn't take the shot. Why? ｢Mike, where did you find this?｣

｢Martin from Bravo Tower found it in the rifle case, but didn't want to mention it on the air. She brought it to me a couple of minutes ago.｣ A pause. ｢I shouldn't have looked. I'm sorry.｣

I hope nobody on Bravo Team used their implant to copy the image. To think I was worried about an old girlie magazine! Still, was Brubaker upset for my sake, or embarrassed because the photo aroused him despite knowing better? I patted his shoulder. ｢It's fine if you liked it. Just keep it between us, all right?｣

｢Yes, ma'am.｣

He still hadn't eased up. Maybe I should give him something to get that image out of his head. I checked the image archive on my implant, and found a shot a classmate took of me in my dress uniform. The navy blue jacket and trousers clung to me, my hair streamed behind me in the breeze, and my sword blazed with the setting sun's light. This ought to get him thinking about the future. ｢You can have this instead. It's from my induction as a sworn Adversary.｣

｢I don't think you've aged a day since. How do you expect me to get you out of my head?｣

Flattery will get you nowhere, kid. ｢I don't. I'd rather you remembered me while thinking about your own future. This was the moment when the world opened before me. I was finally an Adversary, ready to uphold liberty and equal justice under law for all by diplomacy or force of arms.｣

He didn't need to hear that the work demanded more of me than youthful idealism, fast talk, and a deft hand with a blade. He needed me to reinforce his belief that he could make a difference if he got through the training and took the oath. He needed the fantasy, but I couldn't guarantee this mission wouldn't shatter it. Not when my own idealism was worn and cracked by the Society's secrecy.

Further study of the photo revealed no other pertinent details. The handwriting on the back was difficult to read, but that didn't tell me much. Not when penmanship and calligraphy are practically lost arts.

All it told me was that somebody wanted me out of the picture, which was reason enough to do a proper job of taking it into evidence. Slipping the tagged and bagged photo into my jacket wasn't exactly standard procedure, but at least it wouldn't get lost.

Sheriff Robinson showed up a second after I finished pulling the zipper back up. He must have seen something in my expression, because he stopped short. ｢Did you find anything, Adversary?｣

Luckily, I had something other than that photo to discuss. ｢Somebody lives here, Sheriff. We don't know who, or how many, but somebody still makes Fort Clarion their home. Alert the irregulars.｣

---

### This Week's Theme Song

"Every Breath You Take" by The Police

{% youtube OMOGaugKpzs %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
