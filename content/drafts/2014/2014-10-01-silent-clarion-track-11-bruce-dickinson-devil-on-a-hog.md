---
title: "*Silent Clarion*, Track 11: &quot;Devil on a Hog&quot; by Bruce Dickinson"
excerpt: "Check out chapter 11 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Why would somebody from the Phoenix Society's executive council warn Naomi away from Clarion?"
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
I did manage to wrangle Spinoza down to six grams before he yielded. Guilt at my harsh treatment nagged at me, but I suppressed it with an effort; he was out to get the best deal he could, just like me. The bike grabbed my heart with the first purr, but I wasn't going to tell him. I needed him to think I was willing to walk out empty-handed.

Dropping a couple hundred milligrams at the accessory shop assuaged what little guilt I felt at ramming such a hard bargain down his throat. After all, I needed money to spare for a helmet, boots, gloves, and a shoulder harness for my sword. By the time I stopped for a rest on I-80 a hundred kilometers west of New York, I felt pretty damn good.

The rest stop was an island of commerce carved out of the forest that had encroached upon the interprovincial highway after Nationfall. I had my choice of fast food at this rest stop between Agni Burger, Eight Immortals Buffet, Apollo Coffee, and Borgia Pizza. Something I always wondered about the latter: if you griped about the service, would your next meal be your last?

A brief aside concerning Nationfall for those who would rather repeat history than study it. Damn near everything fell apart in 2048 after the world's governments, corporations, and organized religions started pushing psychiatric nanotech called The Patch. They said it would fix humanity's problems. They lied, unless a close brush with extinction constitutes a fix.

Mum and Dad don't talk about it, but they survived a nanotech-induced zombie apocalypse as *little kids*. If I wanted to top that kind of badassery, I think I'd have to arrest God for crimes against humanity and drag his arse down to earth to stand trial.

I wasn't hungry, or inclined to epic feats of courage, so I ducked into the ladies. By some miracle of janitorial effort, the bathroom looked clean enough to eat in. For a nominal fee I could have rented a locker, stripped, and had a shower before resuming my journey — assuming I was too strapped to just rent a room for the night. The nearby motel even had a discount on the honeymoon suite.

My implant notified me of an incoming call from Baruch Spinoza on my way out. What could he want? Only one way to find out. I pulled my phone, which connected to my implant and served mainly to prevent people from thinking I was talking to myself, and sat down. "Hello?"

"Hello, Naomi. How d'you like your chopper?"

"I almost feel bad about how I bargained with your grandson."

Spinoza gave a wheezy chuckle. "Don't worry about him. He pocketed five grams off that deal."

Dammit, I should have driven a harder bargain. It might have been rude to laugh at the old man, but hearing Jacob didn't make out as badly as I thought was a relief. "Good for him."

A sigh on the other end. "I guess you won't be meeting him for dinner when you come back to the city."

Me, date Jacob Spinoza? Sure. Right after Hell freezes over. Or the rest of Hell, if Dante wasn't just making it all up. "I think I'll just stop by for my usual before I catch the maglev home."

I stepped outside after he hung up, and found another motorcycle charging in the stall behind mine. Its chrome was dull, the front tire worn, and one of the mirrors remained attached through a combination of desperation and duct tape. The rider was equally disreputable. He squinted at me through a haze as he smoked what had to be the fattest blunt known to man.

The wind shifted as he studied me. Only the rifle barrel peeking over his shoulder kept me from dismissing him as a lecherous old stoner. Any Adversary who paid attention to scuttlebutt would recognize the sleazy-looking old biker carrying a rifle with a slim barrel and slotted flash suppressor. What the hell was Edmund Cohen doing here?

He tapped the ashes from his blunt, careless of where the wind blew them. "I can see why Malkuth wants a hardware upgrade."

"I'm not sure why that's any concern of yours, sir. You haven't even introduced yourself."

Holy shit. He actually looked embarrassed. Holding his blunt out to his side, he bowed from the waist. "Sorry about that, Adversary Bradleigh. I'm Edmund Cohen. Most of what you've heard about me is pure slander, I promise."

"Even the flattering things?" Not that I've heard much to flatter Cohen. His saving grace is his skill with a rifle. He's a good man to have at your back in a firefight or a pub crawl, but don't lend him money or leave him alone with your girlfriend—or your boyfriend.

He flashed a handsome smile that made him resemble an espionage film hero. "Especially those."

My guard was up because I was alone on the road with only a sword for protection, but Cohen's self-deprecating humor eased me a little. I offered my hand. "I'd introduce myself, but Malkuth beat me to it. What else did he tell you?"

"Just enough to pique my interest." Cohen glanced at his dashboard. "I'm heading west as far as the I-81 exit. Mind if I ride with you a bit?"

"No harm in it, I suppose." Glancing over my shoulder as I mounted up, I caught the old lech perving on me just like I suspected he would. Should I blow him a kiss? No, that's too Jacqueline. "Sure you can keep up?"

I left him there, hitting the on ramp at the current recommended speed. Aside from occasional RVs I passed as they trundled along in the right lane, I had the highway to myself. The wind played with my hair, streaming it behind me as it pressed my sunglasses against my face. Though it hurt a little and would surely leave marks, I didn't care. Astride my Conquest, I was young and strong; no power on earth could oppose me.

No power save Edmund Cohen. He finally caught up with me, and requested a secure talk session. Though our engines were all but silent, using our implants was still easier than shouting at each other over the wind. ｢What do you want?｣

｢Malkuth told me you were interested in Clarion. Why?｣

Hmm... He isn't flirting, or pissing about with small talk now that we're alone. Why is that? Only one way to find out. ｢I heard about some unsolved disappearances. I'm curious as to why nobody seems to give a shit.｣

｢You know what curiosity did to the cat, right?｣

｢I understand the cat got better.｣ Time to try a gamble while I had the old man's attention. ｢I also understand you're executive council. Think you can tell me anything about Clarion?｣

｢Sorry, but you're not cleared. In fact, I've got orders to persuade you to spend your vacation somewhere else. Clarion isn't your problem.｣

Catching sight of the road signs ahead, I opened the throttle and left Cohen behind. ｢You're going to miss your exit, Eddie.｣

｢Shit!｣ He swerved to get onto the ramp, and I thought for a moment he might lose control. ｢At least call Saul Rosenbaum at the New York chapter for backup if you find anything!｣

The session cut out. He must have used a near-field connection, implant to implant, instead of the network. Who the hell does Cohen think I am, anyway? Of course I'll call the local office for backup if I find something real in Clarion. I might be too curious for my own good, but I'm not a demon-ridden idiot. Hell, I'll probably call Rosenbaum when I get there as a professional courtesy.

My sunglasses proved a wise purchase as the sun led me westward to an exit to Route 62. Trafficnet advised me to take the exit, and to expect a rougher road than I-80. Rougher was something of an understatement. Route 62 had not yet been modernized, so it was nothing but faded asphalt with freshly painted lines and black patches where maintenance crews filled in potholes.

Horse-pulled buggies filled with Pennsylvania Deutsch families slowed my progress every couple of kilometers. Having never shared a road with carriages before, I decelerated to avoid spooking the animals.

The road emptied once I passed the last farm and drove into an old forest threatening to encroach upon the highway. I finally pulled over to the narrow shoulder and stopped to remove my sunglasses.

A pair of deer began mating in the middle of the road. While I could ride around them, a truck barrelling down the highway might just flatten them. "Oi, Bambi!" The buck turned his head to gaze on me, but maintained his position. "Did you two have to start shagging in the middle of the bloody highway?"

I doubt the doe understood me, but she pulled free of her suitor's embrace. He remained, hard and frustrated, as she bounded off into the woods on the opposite side of the road. I snapped a photo and sent it to Jacqueline with a message: "They grow 'em big over here." She'd get a kick out of that.

The buck stared at me a moment before lowering his head to threaten me with his antlers. Well, I suppose I did cockblock the poor bastard. I turned on my bike's V-Twin emulation and revved the engine. The rumbling growl of a gasoline-powered chopper shattered the silence, startling the buck into bolting after his lost mate.

The forest eventually yielded to more farmland. Instead of slowing to pass buggies, the buggies pulled over to yield to me. No doubt they heard me coming. The Conquest purred beneath me as I rode into Clarion at a bicycle's pace to avoid hitting pedestrians.

One child saw me and began pulling at his mother's arm. "Mommylookit! It's Cecilia Harvey on a hog!"

Oh, dear. Being compared to a videogame character was cute when little Claire did it, but it was getting old fast. Perhaps I need a different hairstyle. The mother turned to pay attention to her son, so I stopped beside her. "Excuse me, ma'am. Is there somewhere I could stay overnight?"

She pointed down the road. "Try The Lonely Mountain." Before I could thank her, she led her son away to continue on her business. So much for country hospitality.

Despite being a small town, Clarion's main street bustled in a manner that made me a little homesick. They had everything here, even a nerd shop called Kaylee's Shiny Games, Hobbies, and Crafts.

The window at Kaylee's displayed a poster for the new edition of *Advanced Catacombs & Chimeras*, a tabletop game I remembered from university. Another window was devoted to coming soon posters for computer games like *Nationfall: Final War* and *True Goddess Metempsychosis III - Call of the Lightbringer*. The latter seemed to involve yet another demonic invasion of Tokyo. I swear, the place must be accursed.

A plump brunette in overalls and a Pulsecannon t-shirt leaned against the entrance while polishing some kind of game miniature that resembled a grotesque porcine creature wielding a minigun in each hand. Her eyes widened as I passed by. "Excuse me! Did you know you look just like —"

I pulled over so I could talk without holding up traffic. "Cecilia Harvey? I get that a lot lately. It must be the hair."

"I was gonna say you look like Lady Frostmane. From the samurai movies by Ryuhei Miyamoto? All you need is a katana and a kimono." My utter ignorance of the work of Ryuhei Miyamoto must have been painted across my face, because she stopped geeking out and approached. "Welcome to Clarion. I'm Kaylee Chambers."

I offered my hand. "Naomi Bradleigh. Can you tell me where to find The Lonely Mountain?"

Clutching her miniature in her polishing hand, she gave mine a hearty shake. "Give me a minute to lock up and I'll show you the way. Nothing like a beer after work, right?"

---

### This Week's Theme Song

"Devil on a Hog" by Bruce Dickinson

{% youtube EHlwGJwCHwY %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
