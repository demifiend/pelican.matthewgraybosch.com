---
title: "*Silent Clarion*, Track 04: &quot;Man Skin Boot&quot; by Lordi"
excerpt: "Check out chapter 4 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Naomi learns she isn't the first Adversary to let her anger show on the job."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
I would never have believed Director Chattan married, let alone a divorcee, if Jacqueline hadn't told me. Not to say he's incapable of attracting a woman or earning her trust, respect, and affection. He cuts a fine figure in uniform, and I'll admit to occasionally and discreetly ogling him while he does physical training with us. He's a capable fencer, and gracious when defeated.

He's also an intelligent and capable commander, dedicated to the whole of the Phoenix Society, and its ideals. He likes to visit the desks of Adversaries working on clerical tasks because they aren't in PT or out in the field, and surprise us with questions on law, procedure, and tactics if he thinks we're taking a break. He calls it MT, mental training.

The obstacle to my belief is his professionalism. When he's on the job, he doesn't talk about anything else. I suspect he brings his work home with him. Would a man who seems to care only about the Phoenix Society's mission put duty aside long enough to remember that he's also a person, with a person's needs for connection and release?

All of which I kept to myself as I stepped into Chattan's office immediately after logging in and checking my mail. He put aside his sandwich, looked up from his book, , and indicated a chair. "Feeling better today, Adversary Bradleigh?"

I sat, and tried not to let my embarrassment burn my face raw. Bad enough I was asking for leave, but I must have interrupted Chattan's lunch. "I'm ready to face the consequences of my actions, should the Society determine I exceeded my authority at MEPOL or violated the suspects' rights."

Chattan snapped his book shut, and put it aside. "Relax. Nobody's going to put you on trial."

"You do set a certain example, Director."

"I suppose I do." Chattan chuckled. "I suspect Adversary Russo mentioned my recent difficulties."

"You mean the divorce? I'm sorry. I don't think any of us had any idea. It's that stoicism of yours."

I didn't realize I had been holding my breath until he finally spoke. "Funny you should mention that. She kept talking about emotional unavailability during the proceedings. My ex."

"I'm not certain that's any of my business, sir." In fact, hearing about it made me uncomfortable. While it humanized him, I was concerned he might inquire into my own recent woes.

"Likewise, your relationship problems are none of mine." Chattan gave a wry grin. "Unless you think they're interfering with your duties."

"I thought I could perform my duties without my emotions getting in the way, and I was wrong."

Chattan leaned forward, as if I said something interesting. "Do you think it was your feelings about your ex that came out at MEPOL?"

"I'm not sure. If I had only been angry with Wallace for his callousness toward the people he swore to serve and protect, or with the constables responsible for the abuse, I think I would have managed to keep my emotions under control."

"Maybe I should tell you a story." Chattan stood, and took an old framed photograph from one of the bookcases behind him. He studied the photo for a couple of minutes before continuing. "I was a kid during Nationfall, and joined the Phoenix Society as soon as I was old enough. I served under a director named Iris Deschat."

I'm sure I'd heard that name before, but couldn't place it. I looked her up. "The Iris Deschat who served as captain of the NACS Thomas Paine during Nationfall? I take it you served in New York when you were younger."

Chattan seemed pleased with my response. "You remind me of her. She was also the sort to keep her emotions to herself, and believed in carrying out our mission in the most dispassionate manner possible."

I now had a suspicion as to where this story was headed, but kept it to myself and let Chattan tell it his way.

"Before I took the oath, I followed Deschat on several missions to get a taste of fieldwork. One of them involved gender discrimination at a corporate software shop. The programmers' union reported unethical hiring practices and a hostile environment. Because the shop couldn't find a sufficient number of women willing to take lower-paying non-development positions, they took to hiring women as developers, but then immediately demoting them to the less desirable roles to meet equal-opportunity requirements."

What the Hell? Had these people not heard of Countess Lovelace? "What function did this corporation's management expect the women they hired to perform?"

"They were supposed to perform menial and administrative tasks for the male programmers to let them focus on code, while dressing like courtesans to boost morale. Naturally, personal appearance played an unnecessarily substantial role in the hiring process."

I tried to imagine being evaluated for a software development position based on my looks, and found the result unpleasant. "What did Deschat do?"

Chattan smirked. "Would you like to see? I wasn't sure I'd get access to the video, but Malkuth thought you might find it instructive."

Instructive? Oh, dear. "Well, if Malkuth thinks so."

"I do." Malkuth appeared on the wall screen. He reminded me of a Manhattan detective from classic movies: streetwise with a tendency to exhibit profane wit whenever the script permitted. The Roman numeral one blazed on his forehead. "You're too uptight, Naomi. Oh, and you can call me Mal. It's French for bad, as in 'bad motherfucker'."

I shook my head. "I know what it means, Malkuth. I am also aware of the word's Latin roots, as well as the cabalistic meaning of your name. You're the lowest of the Sephiroth, closest to Earth."

"Kid, I'm going to have such fun with you."

I winked at him. "Sorry, but you're not my type. Too virtual."

Malkuth smiled. "If you aren't seeing somebody when I've fixed that, how about a date? You'll never settle for only human again."

Chattan sighed. "You're incorrigible, Malkuth. Just play the video."

I've never been asked for a date by an AI before. It was kind of sweet. "If I'm single when you get a hardware upgrade, Mal, you can pencil me in."

Malkuth beamed like a giddy teenager getting his first kiss before the screen faded to a frozen frame of the past labeled with Director Chattan's details in the top right corner. He pressed a key and started playback.

Iris Deschat was shorter than me, and wiry, but her bearing amplified her presence even on video as she spoke. "Mr. Johnson, do you honestly mean to tell me only men can code? You have men reimplementing basic algorithms instead of relying on standard library functions. In the meantime, you relegate qualified women to menial tasks like pouring coffee and answering phones, after fraudulently hiring them for development roles. Even worse, you bound these women to contracts with unconscionable clauses intended to prevent them from seeking more suitable work elsewhere."

"Adversary Deschat, I understand that our work seems simple to a woman of your education. However, I'm sure I could find a position for you to fill."

Her voice became a snarl. "I'd require a magnifying glass for the duties you have in mind."

"You castrating bitch." Johnson swung a meaty fist, only to recoil as if stung. I never saw Deschat draw her sword. Her thrust was too swift to track.

She poked him again. "You have abused your authority as CEO of [bleep!](#). The Universal Declaration of Individual Rights is most explicit concerning discrimination on the basis of external physical characteristics, including those related to a person's biological sex or the gender, with which they identify."

This time, she poked at his groin. "You may not consider sex or gender when hiring, and to hire women as programmers with the intention of putting them to work as secretaries and eye candy constitutes fraud. You are clearly in the wrong. Chattan, arrest this filth and notify him of his rights."

Chattan sounded younger, and less commanding, on video. "Yes, ma'am!"

He stopped the video, and did not speak for several minutes. I broke the silence. "I think she went further than me. I only brandished my sword. I think Deschat may have drawn blood with that last poke."

"Probably, but the pusbag had it coming. Once we got authority to check his Witness Protocol feeds, it turned out he had a habit of demanding sexual favors in exchange for hiring. That wasn't in the original complaint."

I only had one response to that. "Bloody hell."

Chattan nodded. "Damn right. But Malkuth wanted you to see that for a reason. Can you guess why?"

Johnson didn't respect Deschat or her uniform seriously because she was a woman. Those MEPOL constables were contemptuous of me for the same reason. That was the simplest answer, the first to spring to mind. Perhaps it was too simple. "Johnson thought himself master of the universe, and recognized no authority beyond his own. He was a bully. Deschat understood this, used the anger Johnson provoked in her, and made a show of force."

"Word for word, Adversary Bradleigh, that's the very explanation Deschat offered me afterward. I think you did what she did because you understood on a subconscious level that those constables wouldn't respect you otherwise." Chattan leaned over his desk and held my gaze. "We're watchdogs. Sometimes our mere presence is enough to deter wrongdoing. Sometimes we must snarl and bare our teeth. And sometimes we must bite down and savage our enemies. It's up to you to determine how much force is appropriate to each situation, regardless of the rules of engagement. You're the one in the field, Naomi, and you should trust your own judgment more."

"So, my emotions are just another weapon I can place in service to our mission."

"Exactly. Did you have any other questions?"

I collected myself, unsure if this was the right time to ask for a leave, but determined to do it anyway. I needed time away, despite the knowledge that I was right to act as I did at MEPOL. If I'm to show my anger, that anger should stem from the injustice before me, and not from unrelated personal issues. "I need to take some time off. I'm still concerned about letting my personal life leak into my work, and would like to resolve some issues."

Chattan didn't immediately reply, but tapped at his keyboard. "Looks like you have a couple months coming, Adversary, and no outstanding work. Enjoy your time off, and try to keep up with your PT and MT."

A weight lifted from my shoulders. "Thank you, Director. Should I check in weekly?"

"Don't be an idiot. Leave work at work." He stood, and offered his hand - a tacit dismissal.

I shook his hand. "I'm sorry to hear about your divorce."

---

### This Week's Theme Song

"Man Skin Boot" by Lordi, from *Deadache*

{% youtube jwCwJvQTk4I %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
