---
id: 1178
title: This Man Ruined the Xbox 360
date: 2014-08-22T08:00:48+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1178
permalink: /2014/08/this-man-ruined-the-xbox-360/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapGP:
  - 
snapPN:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snap_isAutoPosted:
  - 1
snapBG:
  - |
    s:357:"a:1:{i:0;a:8:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:74:"http://matthewgraybosch.blogspot.com/2014/08/this-man-ruined-xbox-360.html";s:5:"pDate";s:19:"2014-08-22 12:01:38";}}";
snapDI:
  - 
snapDL:
  - 's:292:"a:1:{i:0;a:8:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"e51affa2ec2efd9e915f69289b96fd22";s:5:"pDate";s:19:"2014-08-22 12:01:39";}}";'
snapIP:
  - 
snapLI:
  - 
snapLJ:
  - 
snapSC:
  - 
snapSU:
  - 
snapWP:
  - 
bitly_link:
  - http://bit.ly/1LnPV4e
bitly_link_twitter:
  - http://bit.ly/1LnPV4e
bitly_link_facebook:
  - http://bit.ly/1LnPV4e
bitly_link_linkedIn:
  - http://bit.ly/1LnPV4e
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1LnPV4e
bitly_link_reddit:
  - http://bit.ly/1LnPV4e
bitly_link_stumbleupon:
  - http://bit.ly/1LnPV4e
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Games
  - Rants
tags:
  - advertising
  - games
  - suck
  - UI
  - Xbox 360
  - Xbox One
---
OK, I admit it. The headline is hyperbole. [Allen Murray didn't ruin the Xbox 360](http://blog.theilluminatedsquid.com/post/94215585076/10-years-in-games-sorry-for-all-the-ads) on his own.

However, as the Xbox UI devoted more space to advertisements, I found myself doing more of my gaming on my PlayStation 3. Oddly enough, I bought the Xbox 360 _first_. It was cheaper, and had interesting games like Silicon Knights' _Too Human_ and Mistwalker's _Lost Odyssey_.

Unfortunately, the 360's early promise faded. By the end of 2011 and the release of what Microsoft called the "New Xbox Experience", I was doing all my gaming on my PS3.

What happened? Sony's UI wasn't as obnoxious. I only saw adverts when using the PlayStation Store. Moreover, the PlayStation Network wasn't crippled so that players only got the full benefit of the PSN if they paid a monthly fee. Sony didn't screw around with a points-based payment system. All PSN prices were in US dollars. Finally, the PlayStation Plus program consistently offered better freebies than Xbox Live Gold could manage.

Most importantly, the Xbox 360 lost its edge on the most important front of all: games. By December 2011, Microsoft was hellbent on making the Xbox 360 a replacement for all the crap people connect to their living room TVs, and stopped getting good exclusive games. Any halfway decent game available for the Xbox 360 was _also_ available for the PS3, and if you've got little girl hands like mine, the PS3 controller is more comfortable.

Fast-forward to 2014. I can't remember the last time I turned on my Xbox 360. Sure, I've got unfinished copies of _The Witcher 2_ and _Mass Effect 3_, but that isn't reason enough. The former is now available on Linux, and the latter is available for the PS3 if I can be bothered to play start playing that again.

What about the Xbox One? I have no intention of buying one. All of the games that might justify an Xbox One purchase are also available on the PlayStation 4, such as _The Witcher 3_, _Dragon Age: Inquisition_, and _Final Fantasy XV_. (If anybody from Square-Enix is reading this, the latter better be _amazing_.) Oh, and the PS4 got _Bloodborne_. Suck it, Xbox fans. 🙂

Not that any of this can be pinned on Allen Murray. At most, the ads he helped put on the Xbox 360 UI were a harbinger of what would follow &#8212; but good luck proving that.