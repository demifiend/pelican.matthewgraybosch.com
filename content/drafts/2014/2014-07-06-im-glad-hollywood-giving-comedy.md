---
title: "I'm Glad Hollywood is Giving Up on Comedy"
excerpt: Every movie they make is starting to resemble *Springtime for Hitler*
date: 2014-07-06T23:39:50+00:00
header:
  image: the-producers-2005.jpg
  teaser: the-producers-2005.jpg
categories:
  - The Tarnished Screen
tags:
  - comedy
  - decline
  - hollywood
  - Mel Brooks
  - profit
  - The Producers
  - Springtime for Hitler
  - fail on purpose
  - "too cynical?"
---
I just found [an article in *The Atlantic*](http://www.theatlantic.com/entertainment/archive/2014/07/the-completely-serious-decline-of-the-hollywood-comedy/373914/) suggesting that Hollywood isn't making as many comedies any longer, because comedy doesn't seem to do well outside the United States, or on the home video market.

I'm not surprised. I don't know if I'm just getting old, but I haven't enjoyed most of the comedies Hollywood has offered in the last decade. Hell, the last comedy I actually enjoyed the 2005 production of _The Producers_.

It's odd that the last Mel Brooks movie was a remake of his first. Maybe his sort of humor stopped selling. It seems, however, that the sort of comedy exemplified by Adam Sandler and the cast of movies like _The Hangover_ and _This is the End_ isn't selling as well any longer, either. 

Either way, I'm glad Hollywood seems to be giving up on comedy. They've started to suck at it. But maybe they're doing it on purpose but don't have the balls to film *Springtime for Hitler*, so they can use box office bombs to avoid taxes on blockbuster profits?

Or am I being too cynical again?
