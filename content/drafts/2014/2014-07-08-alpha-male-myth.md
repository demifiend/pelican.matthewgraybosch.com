---
id: 891
title: The Alpha Male Myth
date: 2014-07-08T08:00:42+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=891
permalink: /2014/07/alpha-male-myth/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapDL:
  - 's:179:"a:1:{i:0;a:5:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";}}";'
snapPN:
  - 
snap_isAutoPosted:
  - 1
snapBG:
  - |
    s:387:"a:1:{i:0;a:9:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:59:"%FULLTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:69:"http://matthewgraybosch.blogspot.com/2014/07/the-alpha-male-myth.html";s:5:"pDate";s:19:"2014-07-08 12:03:31";s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";
snapDI:
  - 
snapFB:
  - 
snapSU:
  - 
snapLJ:
  - 's:317:"a:1:{i:0;a:9:{s:4:"doLJ";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:10:"%FULLTEXT%";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:48:"http://matthewgraybosc.livejournal.com/2151.html";s:5:"pDate";s:19:"2014-07-08 12:05:10";s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapSC:
  - 
snapTW:
  - 's:305:"a:1:{i:0;a:10:{s:4:"doTW";s:1:"1";s:9:"timeToRun";s:0:"";s:10:"SNAPformat";s:25:"%SURL% %ANNOUNCE% %HTAGS%";s:8:"attchImg";s:1:"1";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:18:"486481141319229440";s:5:"pDate";s:19:"2014-07-08 12:05:17";}}";'
snapLI:
  - 
snapST:
  - 
snapIP:
  - 
snapWP:
  - 
snapGP:
  - 
bitly_link:
  - http://bit.ly/1MfANGE
bitly_link_twitter:
  - http://bit.ly/1MfANGE
bitly_link_facebook:
  - http://bit.ly/1MfANGE
bitly_link_linkedIn:
  - http://bit.ly/1MfANGE
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1MfANGE
bitly_link_reddit:
  - http://bit.ly/1MfANGE
bitly_link_stumbleupon:
  - http://bit.ly/1MfANGE
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - alpha male
  - dominance
  - gender
  - masculinity
  - performing masculinity
  - prestige
  - psychology
  - research
  - Scott Barry Kaufman
---
I've followed <a title="Scott Barry Kaufman" href="http://scottbarrykaufman.com/" target="_blank">Scott Barry Kaufman</a> on <a title="Scott Barry Kaufman on Google+" href="https://plus.google.com/u/0/104926998204753658078/about" target="_blank">Google+</a> for a couple of years, and haven't regretted it. He deals in psychology, and usually provides interesting articles and research on cognition in general. In particular, he tends to post on creativity and intelligence, and is the author of <a title="Scott Barry Kaufman: Ungifted" href="http://www.amazon.com/Ungifted-Intelligence-Scott-Barry-Kaufman/dp/0465025544" target="_blank"><em>Ungifted: Intelligence Redefined</em></a>.

He recently posted a guest article for <a title="The Art of Manliness" href="http://www.artofmanliness.com" target="_blank"><em>The Art of Manliness</em></a> entitled <a title="Art of Manliness: the Myth of the Alpha Male" href="http://www.artofmanliness.com/2014/07/07/the-myth-of-the-alpha-male/" target="_blank">"The Alpha Male Myth"</a> in which he writes:

> When we impose just two categories of male on the world, we unnecessarily mislead young men into acting in certain predefined ways that aren’t actually conducive to attracting and sustaining healthy and enjoyable relationships with women, or finding success in other areas of life. So it’s really worth examining the link between so-called “alpha” behaviors (such as dominance) and attractiveness, respect, and status.

I find this especially appealing, since I am not the sort of man one would consider naturally dominant, let alone "alpha". I don't want to dominate others, or be dominated by others.<figure style="width: 3256px" class="wp-caption aligncenter">

[<img src="http://i1.wp.com/mentonespecial.files.wordpress.com/2013/01/neither-master-nor-slave.jpg?resize=840%2C431" alt="The Mentone Special: " data-recalc-dims="1" />](http://mentonespecial.wordpress.com/volume-two/)<figcaption class="wp-caption-text">The Mentone Special: "Neither Master Nor Slave"</figcaption></figure> 

Kaufman continues by summarizing a 1999 study by <a title="Do Women Prefer Dominant Men? The Case of the Missing Control Condition" href="http://wwww.scu.edu/cas/psychology/faculty/upload/Burger-Cosby-JRP-1999.pdf" target="_blank">Jerry Burger and Mica Cosby</a> called "Do Women Prefer Dominant Men? The Case of the Missing Control Condition". This study suggests mere _dominance_ isn't as attractive a quality in men or women as previously suggested by other studies lacking a crucial control condition.

Most of this is just setup and context. The real meat of the article comes in Kaufman's exploration of dominance vs. prestige. Check this out:

> In our species, the attainment of social status, and the mating benefits that come along with it, can be accomplished through compassion and cooperation just as much (if not more so) as through aggression and intimidation. Scholars across ethnography, ethology, sociology, and sociolinguistics believe that at least two routes to social status – [dominance and prestige](http://www.hirhome.com/academic/hen%26gil.pdf) – arose in evolutionary history at different times and for different purposes.
> 
> The _dominance_ route is paved with intimidation, threats, and coercion, and is fueled by [hubristic pride](http://www.tandfonline.com/doi/abs/10.1080/15298860802505053). Hubristic pride is associated with arrogance, conceit, anti-social behaviors, unstable relationships, low levels of conscientiousness and high levels of disagreeableness, neuroticism, narcissism, and poor mental health outcomes. Hubristic pride, along with its associated feelings of superiority and arrogance, facilitates dominance by motivating behaviors such as aggression, hostility, and manipulation.
> 
> In contrast, _prestige_ is paved with the emotional rush of accomplishment, confidence, and success, and is fueled by [authentic pride](http://www.tandfonline.com/doi/abs/10.1080/15298860802505053). Authentic pride is associated with pro-social and achievement-oriented behaviors, agreeableness, conscientiousness, satisfying interpersonal relationships, and positive mental health. Critically, authentic pride is associated with _genuine self-esteem_ (considering yourself a person of value, not considering yourself superior to others). Authentic pride, along with its associated feelings of confidence and accomplishment, facilitates behaviors that are associated with attaining prestige. People who are confident, agreeable, hard-working, energetic, kind, empathic, non-dogmatic, and high in genuine self-esteem inspire others and cause others to want to emulate them.

For my part, I've tried to find authentic pride through my work as a programmer and a novelist. Rather than being [a boss or a leader](http://www.inc.com/lee-colan/are-you-a-boss-or-a-leader.html) (which isn't as negative, but still a role with which I'm uncomfortable), I seek self-worth through creativity. It's pretty much my best bet, since I am not necessarily agreeable, energetic, kind, or empathic.

I'm still working on the "considering myself a person of value" part, but that's a story for another day.