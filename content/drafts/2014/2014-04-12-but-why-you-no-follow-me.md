---
title: "&quot;But- Why You No Follow Me?&quot;"
excerpt: "I'm not asking why you don't follow *me*, because I don't care. I'm explaining why I don't follow *you*."
header:
  image: angry-y-u-no.jpg
  teaser: angry-y-u-no.jpg
categories:
  - Social Media
  - Longform
tags:
  - but... why
  - circle
  - empty profiles
  - engagement
  - Facebook
  - fake profiles
  - follow
  - google
  - like
  - social media
  - time
  - trolls
  - Twitter
---
{% include base_path %}

No, this isn't the post where I ask why you don't follow my blog, circle me on Google+, follow me on Twitter, or friend/like me on Facebook. I don't give a damn about that, because it doesn't help me sell books. This is the post where I explain why I don't follow _you_ on social media, and am not likely to do so.

This was something I originally posted on Google+ back in 2013, but [that post](https://plus.google.com/u/0/+MatthewGraybosch/posts/7gyvx2DNcEz) seems to be getting renewed attention lately. I've added a few reasons to this version. See if you can spot 'em.

![Why? Because fuck you is why.]({{ base_path }}/images/angry-y-u-no.jpg)

## Here's Why I Don't Follow You...

I don't normally post on how to do social media, because I don't consider it an interesting topic. However, I've been on G+ a couple of years now, and I know a few things. I also have opinions.

A bunch of new people added me to their circles recently, and are no doubt wondering why I haven't circled them back. Too bad none of them bothered to ask, because that's the first reason I haven't circled them back. I have others, though.

### You don't engage with me...

If you want me to circle you, I need a reason. Resharing my posts while adding interesting commentary is the best way, but simply commenting on my posts will do.

### Your profile is incomplete...

I understand that you may have reasons to not show everything on your profile. Moreover, you have the right to reveal as much or as little as you like. However, when you circle me, the first thing I do is check your profile to see if you're a spammer or a robot.

### Your profile looks fake...

If your profile photo is that of an attractive woman, but your profile says you're male and is otherwise empty, I'm going to assume you're a spammer.

Likewise if you claim to be in the military and your profile is just some folderol about respect being earned. There's no way I've actually got half the officers in the armed forces of the United States as *legitimate* followers on *any* social network, let alone Google+.

If you don't even have a profile pic, and your profile is pretty much empty, then I'm going to assume you're a troll. Why? Prior experience.

### You have no public posts...

If I can't see what you post, how am I supposed to know what sort of content to expect from you once I add you to my circles? How am I supposed to know you're a human being, or at least a meat popsicle?

### Your public posts are all the same stuff, from the same source...

That's a dead give-away that you're probably a spammer, especially if you're posting from a site for which you claim to work. Try to be more subtle, damn you.

### Your public posts don't do it for me...

There isn't much you can do about this, but I might still circle you if you provide interesting engagement.

### You publicly post sexist/racist material...

I might be a white man, but that doesn't mean I want to see that shit. Save it for Stormfront.

### You post NSFW stuff during what I consider working hours...

This is just self-interest. I like pin-ups and cheesecake, especially involving pale brunettes and redheads. However, I don't want to see that stuff on the job if I'm using my smartphone to dip into the social for the equivalent of a cigarette break while running the compiler. Nothing personal.

### You're a brand...

If I wanted my stream cluttered with advertising, I'd be on Facebook. If you're a brand or a business, don't circle me. If you're lucky, I'll just ignore you. I know you want to get your message out, but I don't give a damn about your message.

I'm an author, and I don't post about my books all the time. Why? Because nobody gives a shit.

And if you're a business spamming my Twitter and Facebook feeds with promoted posts, expect me to post replies suggesting that you make yourself useful and buy a copy of my novel. I have no scruples about spamming spammers.

### You post long, talky videos that don't have transcripts...

I never liked listening to lectures in school. I didn't care for lectures from my parents, either. When people talk, they repeat themselves, go off on tangents, and clutter up the lecture with semantically null sounds instead of shutting up for a moment to collect their thoughts and figure out what they're going to say next. It's inefficient, and it's _boring_.

I only tolerate boring shit at my day job because I'm getting paid. I absolutely _hate_ long lecture videos without transcripts, because I can read faster than the guy in the video can talk. If you routinely post this crap, I have three words for you: **transcript or GTFO!**

### You post too much religious crap...

If your beliefs lend meaning to your life, or comfort you, or make you genuinely happy, then that's great. I don't need to hear about it. Seeing it in my feed isn't going to convince me to reconsider my atheism.

### You post nothing but pictures, and you're not on Pinterest...

I know Google+ is trying to eat Pinterest's lunch since they failed miserably at eating Facebook, but keep your picture. I'd rather have a thousand words.

### You post pseudoscience and other arrant bullshit...

Does this even merit explanation? If you honestly think that homeopathy works or that vaccines cause autism, I want as little to do with you as possible. If you think psychics are real, and are sufficiently entertaining, I'll consider making an exception. However, I won't believe in ESP until [somebody takes James Randi's million dollars](http://www.randi.org/site/index.php/1m-challenge.html).

### Some other reason...

If you don't fit the above reasons, and I still haven't circled you, then I might not have noticed you. Try interacting with me. Start by posting comments. 🙂

Also, social networking is pretty much dead. Inadequately regulated capitalism ruined it as it tends to ruin everything else. If you *really* want to connect with me, [try the contact page.](/contact/)
