---

title: 'Winter Solstice Promotion'
excerpt: Starting this Winter Solstice, get Without Bloodshed - Part One of Starbreaker by Matthew Graybosch - on your Kindle for a dollar.
cover: winter-solstice-1280x720.jpg
coverwidth: 1280
coverheight: 720
---

Happy Thanksgiving, everybody! As I've mentioned before, [it's been about a year]({% post_url 2014-11-17-without-bloodshed-anniversary %}) since the publication of *[Without Bloodshed](/stories/without-bloodshed/)*, and I have some cause for gratitude:

 * Respectable sales
 * Several favorable reviews
 * No troll reviews
 * A strong showing at the 2014 World Fantasy Convention
 * Dozens of loyal fans
 * Opportunities to [serialize new work](/stories/silent-clarion/) online

I can't do much to thank you all, but I'd like to provide an opportunity for new readers to get into Starbreaker, or share it with family and friends, without breaking the bank. Therefore, I've asked [Curiosity Quills Press](http://www.curiosityquills.com) to reduce the price for *[Without Bloodshed](/stories/without-bloodshed/)*'s Kindle edition to a dollar this December.

This deal will be available between Winter Solstice and New Year's Day (21 December 2014 - 1 January 2015). The Winter Solstice is an important day in the Starbreaker setting, and acts as a secular substitute for Christmas that anybody can celebrate regardless of their beliefs.

Naturally, the neo-Romantic metal band Crowley's Thoth would play major shows around the Solstice, as illustrated by artist [Harvey Bunda](http://www.harveybunda.com) below.

![Crowley's Thoth Winter Solstice by Harvey Bunda](/images/winter-solstice-1280x720.jpg)

The image above is a 1280x720 image suitable for use as a wallpaper on 720p displays. If you would like to download and use it, you're welcome to do so. Other resolutions are also available.

 * [Crowley's Thoth Winter Solstice: 1280x720](/images/winter-solstice-1280x720.jpg)
 * [Crowley's Thoth Winter Solstice: 1440x900](/images/winter-solstice-1440x900.jpg)
 * [Crowley's Thoth Winter Solstice: 1920x1080](/images/winter-solstice-1920x1080.jpg)
 * [Crowley's Thoth Winter Solstice: 2560x1600](/images/winter-solstice-2560x1600.jpg)
 * [Crowley's Thoth Winter Solstice: 1280x1024](/images/winter-solstice-1280x1024.jpg)
 
Have a safe and happy Thanksgiving and Winter Solstice from Starbreaker and [Curiosity Quills Press](http://www.curiosityquills.com)!