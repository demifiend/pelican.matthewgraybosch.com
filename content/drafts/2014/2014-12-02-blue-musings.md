---
title:    'Blue Musings'
excerpt:  Seeing author Lisa Cohen mention Starbreaker in her Blue Musings newsletter is a huge surprise.
---
I *so* wasn't expecting this. I became a fan of Lisa Cohen after [reading and reviewing *Derelict*](http://www.matthewgraybosch.com/2014/07/02/lj-cohen-derelict-worthy-heinlein/ "Derelict: Worthy of Heinlein"), and follow her [Blue Musings](http://ljcohen.net/mailinglist/mail.cgi/archive/bluemusings/20141202165225/ "Blue Musings by Lisa Cohen") newsletter.

You can imagine my surprise when I cracked open the latest installment, and found the following:

> *Science Fantasy with a helping of heavy metal music and original world building*

> [WITHOUT BLOODSHED](/stories/without-bloodshed/ "Starbreaker: Without Bloodshed") is a complex, rambling read that takes place in modern day Boston (my fair city!) and beyond. It's the kind of genre-busting novel that is rarely published in today's world of tightly defined markets, and that's a shame. Taking place after Nationfall, Without Bloodshed isn't quite post-apocolyptic, but it is post nations, a world where The Phoenix society is the law and Morgan Stormrider is its most successful enforcer. But he is also not quite human, and the parallel stories that twist through this novel showcase a writer with a strong grasp on building myth and driving a high-octane plot. There is murder, heavy metal music, betrayal, and demigods. Oh, and swords, too.

Thanks, Lisa! Allow me to return the favor.

If anybody's reading this, please take a look at the following releases by LJ Cohen. Just click the cover to access each book's Amazon page.

[![Derelict by LJ Cohen](http://ecx.images-amazon.com/images/I/811ECpIbwzL._SL1500_.jpg "Derelict: LJ Cohen")](http://www.amazon.com/Derelict-LJ-Cohen-ebook/dp/B00KNFS8OW/)

[![Future Tense by LJ Cohen](http://ecx.images-amazon.com/images/I/71wvLSCMqGL._SL1280_.jpg "Future Tense: LJ Cohen")](http://www.amazon.com/Future-Tense-LJ-Cohen-ebook/dp/B00I9NHBRU/)

[![The Between by LJ Cohen](http://ecx.images-amazon.com/images/I/81Q2BuMGN3L._SL1500_.jpg "The Between: LJ Cohen")](http://www.amazon.com/Between-LJ-Cohen-ebook/dp/B006UVP724/)

Check these out if you're into hard SF (*Derelict*) or urban fantasy (*Future Tense* and *The Between*).
