---
title: Ryan Toxopeus Reviews Without Bloodshed
categories:
  - Project News
tags:
  - 4 stars
  - everybody loves Claire
  - four stars
  - review
  - without bloodshed
---
Indie heroic fantasy author <a title="Ryan Toxopeus" href="http://ryantoxorants.blogspot.com/" target="_blank">Ryan Toxopeus</a> (author of <a title="A Noble's Quest by Ryan Toxopeus" href="http://www.amazon.com/A-Nobles-Quest-ebook/dp/B0095MG1PM" target="_blank"><em>A Noble's Quest</em></a>, _A Wizard's Gambit_, and _A Hero's Birth_) was kind enough to get himself a copy of my science fantasy monstrosity, <a title="Amazon: Without Bloodshed" href="http://www.amazon.com/Without-Bloodshed-Starbreaker-Matthew-Graybosch-ebook/dp/B00GQ0BJOO" target="_blank"><em>Without Bloodshed</em></a>. He was also kind enough to write a <a title="Ryan Toxopeus on Without Bloodshed" href="http://ryantoxorants.blogspot.com/2014/06/without-bloodshed-review.html" target="_blank">favorable review while raising two excellent points about the story</a>.

He also loves Claire. I suspect _everybody_ loves Claire, but I'm sure somebody will come along and prove me mistaken. In the meantime, Ryan has this to say about the gray hat who steals the show whenever she can:

> The pace is quick, it's full of intrigue, the plot is complex, and Claire might just be my favourite female character of all time. I'm not sure if I should characterize her as a major minor character, or a minor major character, because the cast is quite large.

She's one of the most <a title="TVTropes: Genre Savvy" href="http://tvtropes.org/pmwiki/pmwiki.php/Main/GenreSavvy" target="_blank">genre savvy</a> characters in the cast, and distrusts Isaac Magnin at first sight because <a title="TV Tropes: White Hair, Black Heart" href="http://tvtropes.org/pmwiki/pmwiki.php/Main/WhiteHairBlackHeart" target="_blank">white-haired bishounen</a> often turn out to be the bad guys. Yes, she can be a bit silly.

Maybe I should hire Harvey to do more Claire pinups. 🙂<figure id="attachment_442" style="width: 662px" class="wp-caption aligncenter">

[<img class="size-large wp-image-442" src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/claireashecroft_harveybunda-662x1024.jpg?resize=662%2C1024" alt="Claire Ashecroft, by Harvey Bunda" data-recalc-dims="1" />](http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/claireashecroft_harveybunda.jpg)<figcaption class="wp-caption-text">Claire Ashecroft, by Harvey Bunda</figcaption></figure>
