---
id: 887
title: Sister of the Night, by Selenada
date: 2014-07-07T21:36:35+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=887
permalink: /2014/07/sister-night-selenada/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapDL:
  - 's:179:"a:1:{i:0;a:5:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";}}";'
snapPN:
  - 
snap_isAutoPosted:
  - 1
snapBG:
  - |
    s:395:"a:1:{i:0;a:9:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:59:"%FULLTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:77:"http://matthewgraybosch.blogspot.com/2014/07/sister-of-night-by-selenada.html";s:5:"pDate";s:19:"2014-07-08 04:30:27";s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";
snapDI:
  - 
snapFB:
  - 
snapIP:
  - 
snapLJ:
  - 's:317:"a:1:{i:0;a:9:{s:4:"doLJ";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:10:"%FULLTEXT%";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:48:"http://matthewgraybosc.livejournal.com/1973.html";s:5:"pDate";s:19:"2014-07-08 04:32:00";s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapSU:
  - 
snapLI:
  - 
snapTW:
  - 's:305:"a:1:{i:0;a:10:{s:4:"doTW";s:1:"1";s:9:"timeToRun";s:0:"";s:10:"SNAPformat";s:25:"%SURL% %ANNOUNCE% %HTAGS%";s:8:"attchImg";s:1:"1";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:18:"486367087372165123";s:5:"pDate";s:19:"2014-07-08 04:32:04";}}";'
snapWP:
  - 
snapST:
  - 
snapSC:
  - 
snapGP:
  - 
bitly_link:
  - http://bit.ly/1hrFR2f
bitly_link_twitter:
  - http://bit.ly/1hrFR2f
bitly_link_facebook:
  - http://bit.ly/1hrFR2f
bitly_link_linkedIn:
  - http://bit.ly/1hrFR2f
sw_cache_timestamp:
  - 401681
bitly_link_tumblr:
  - http://bit.ly/1hrFR2f
bitly_link_reddit:
  - http://bit.ly/1hrFR2f
bitly_link_stumbleupon:
  - http://bit.ly/1hrFR2f
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - art
  - deviantart
  - fantasy
  - portrait
  - Selenada
  - sister of the night
format: image
---
Daniel Koeker on Google+ shared this this piece by Selenada @ DeviantArt this morning, and it's gorgeous. You can see more of her artwork at <a title="Selenada at DeviantArt" href="http://selenada.deviantart.com/" target="_blank">http://selenada.deviantart.com/</a>.<figure style="width: 600px" class="wp-caption alignnone">

[<img src="http://i2.wp.com/fc01.deviantart.net/fs71/i/2014/095/a/8/sister_of_the_night_by_selenada-d6ubjwd.jpg?resize=600%2C908" alt="Sister of the Night, by Selenada" data-recalc-dims="1" />](http://selenada.deviantart.com/art/Sister-of-the-Night-413724541)<figcaption class="wp-caption-text">Sister of the Night, by Selenada</figcaption></figure>