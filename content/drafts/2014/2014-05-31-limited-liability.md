---
title: Limited Liability
categories:
  - Project News
tags:
  - chromebook
  - limited liability
  - new story
format: status
---
So, the next anthology from Curiosity Quills Press will probably include a Starbreaker story I wrote last week on my new Chromebook called "Limited Liability".

And if they decide they don't want it, I'll just package it up with "The Milgram Battery", "Tattoo Vampire", and "Of Cats and Cardigans" and put it out for a buck. 🙂

Oh, wait. Did I forget to mention I've got a Chromebook? More on that tomorrow.
