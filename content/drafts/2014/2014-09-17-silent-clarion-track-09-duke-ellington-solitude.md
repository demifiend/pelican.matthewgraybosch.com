---
title: "*Silent Clarion*, Track 09: &quot;Solitude&quot; by Duke Ellington"
excerpt: "Check out chapter 9 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
The journey to New York was hardly as long as Lucy Ashecroft predicted. Which just proved Lucy didn't understand her daughter's fundamental problem. The girl was lonely, and related better to adults than she did to kids her own age.

I could sympathize; I was little different. Neither of us had any notion of how to be little girls, so we tried to fake it while masking our impatience to escape childhood. I found my escape through music. I suspected Claire would find hers through tech.

Beyond having booked passage and a couple of nights lodging in central Manhattan, I had no definite plans for my leave. I figured I'd hit Midtown and find something to do after I checked in and dropped off my bag in my room. However, the events and attractions display in the Hellfire Club's lobby cycled through its programming without catching my interest.

I didn't want to take a bus tour of Manhattan, too familiar with the city from my student days when I split my time between the Juilliard Conservatory at Lincoln Center and Adversary Candidate School at the old Fordham University campus. Broadway offered nothing I hadn't seen back home. My implant's memory still held photos of me and my friends from ACS at the Statue of Liberty. And I felt too restless and energetic to wander the city's museums.

A sign on the hotel bar's door caught my eye: 'Pianist Wanted'. I removed the sign from the door, sat at the bar, and placed it before the bartender. "I play, and I'm available tonight and tomorrow. Who should I contact concerning an audition?"

The bartender studied me a moment. His voice sounded like one made for crooning. "The piano's behind you, miss. Show me what you've got."

I caressed the baby grand's keys before sitting down. It was a pre-Nationfall antique, lovingly maintained and perfectly tuned. The presence of such a venerable instrument in the hotel bar suggested a refined clientèle. I tried some jazz, playing a few standards from memory before beginning to improvise, and continued until I became conscious of the bartender's presence beside me.

He seemed pleased with me. "I'll need you to play from six to ten. A hundred milligrams a night plus tips. Sound good?"

I checked the time. It was one in the afternoon. "Sounds fair. Anything else?"

The bartender nodded. "One more thing. Do you have anything formal to wear?"

That's what I get for letting caprice guide me. "I'll have to buy something, and I suppose you'll want me to leave the sword in my room."

"I'll keep it behind the bar for you. Yell if you need it."

"I can live with that." I took my leave until six, and caught a cab to shop for appropriate wear. An ankle-length black dress with a sweetheart neckline at a boutique called Frigga's Loom caught my eye, and I paid extra to have it fitted and fabricated within the hour.

I made a week's salary that night, and double the next. Word must have spread. The money meant less than the opportunity to perform in front of an audience not comprised of family and friends. Playing for the bar's patrons offered a thrill of power I could enjoy without guilt. Their hushed attention was adoration, their rapt gazes—caresses.

When I was done, I longed for a lover who would adore me with more than his hushed attention and rapt gaze. I found several handsome men among the hotel bar's guests, but I couldn't bring myself to approach any of them and invite them to my room. I wanted more from a man than a night of pleasure.

Instead of chatting, I claimed a stool at the bar and ordered a glass of wine. I listened as a pair of women beside me discussed resettlement efforts.

I'm not sure why people are bothering to fill in the old towns between New York and Pittsburgh instead of spreading out west, but I won't complain.

"Plenty of good farms in between, especially around Clarion. Ever been there?"

"No. You?"

"Last year for the fair. Some of the locals started a rock band, and they were pretty tight. Not sure I'd go back, though."

"How come?"

"A couple of people disappeared while I was there. They were visitors, like me. The locals searched the woods, but eventually shrugged it off and went back to their business. One of 'em turned up a week later, but not the other."

"Sounds creepy. I'm surprised the Phoenix Society hasn't gotten involved."

So was I. I reclaimed my sword from the bartender, and returned to my room for privacy and a change of clothes. After putting away my dress and shoes, I called Malkuth using the screen built into the wall opposite my too-large-for-one bed.

The AI seemed surprised to see me. "Did you miss me, Naomi?"

"Yes, but I didn't call because I was lonely." I suppose I was flirting a bit with Mal, but I doubted it would do any harm.

"Do tell."

"Can you tell me anything about disappearances in a town called Clarion? It's situated between New York and Pittsburgh."

Malkuth's formerly interested expression darkened, but his presence seemed to fade. It was as if somebody had caught his attention. I waited a couple of minutes, and was about to speak before he refocused on me. "I'm sorry, Adversary Bradleigh, but you're not cleared for any information related to the town of Clarion."

"What do you mean, I'm not cleared?" I was more curious than indignant; I had never heard of an Adversary being denied access to information on any grounds other than privacy rights. Talk of clearance smacked of pre-Nationfall espionage dramas.

Malkuth shook his head. "I'm not permitted to explain. Orders from the Executive Council. Sorry, Naomi."

"I understand. Sorry if I caused you any trouble. I just overheard a conversation between a couple of businesswomen at the hotel bar, and got curious." Why would the Executive Council order Malkuth to hide information about Clarion from me?

I took my leave of Malkuth, and decided to nip back down to the hotel bar. The businesswomen I overheard earlier had left, and the bar had emptied out a bit. As I claimed a stool, a young man settled beside me and cleared his throat. "Hello. I saw you play earlier. You're amazing."

I smiled at him. He was a handsome kid, though his manner suggested he was still a bit shy around women. "Thank you."

He glanced behind me. I discreetly followed his gaze to a table crowded with youths egging him on. "They're my friends. I just got my degree, and they dared me to buy you a drink and hit on you."

"Perhaps I should buy you a drink, instead. You seem nervous." I smiled at him, and gently touched his hand. "It's all right. What's your name, anyway?"

"Cliff." He blushed, and looked at the bar. "How did you know?"

"I have brothers." I didn't mention that they told me tales of their own amorous adventures to ensure I was forewarned, and thus forearmed. "Also, I'm an Adversary."

That got Cliff's attention. "No way. You're an incredible musician, and an Adversary?"

His awestruck expression made him seem too young to have just earned his degree. It reminded me of little Claire. I nodded. "Your heart really isn't in this game, is it?"

He shook his head. "I have a girlfriend, but she's visiting her family tonight and my friends thought I could do better if I'd just try." He smiled at me. "Thing is, I don't want to do better. I love Isabel."

I motioned the bartender over. "Have a drink on me, while I deal with your friends."

Before he could object, I advanced upon his friends wearing the sauciest smile I could muster. "I need to borrow Cliff for the night. You lot will just have to manage without him for once."

I returned to the bar with a little swagger, and gently touched Cliff's shoulder before whispering in his ear. "When you're done, come with me. I'll sneak you out, and you can get away from those losers."

---

### This Week's Theme Music

"Solitude" by Duke Ellington

{% youtube OPTjfKTnuCc %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
