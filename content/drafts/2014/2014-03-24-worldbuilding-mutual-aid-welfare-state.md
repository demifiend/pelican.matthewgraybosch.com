---
title: "Worldbuilding: Mutual Aid vs the Welfare State"
categories:
  - Backstage
tags:
  - charity
  - conservatism
  - corporate rule
  - libertarianism
  - mutual aid
  - taxation as theft
  - The Atlantic
  - The Gilded Age
  - welfare state
  - worldbuilding
---
I want to establish from the outset that this post is not a political post, despite being inspired by a piece on <a title="The Atlantic" href="http://www.theatlantic.com" target="_blank">The Atlantic's website</a> by <a title="The Atlantic: Mike Konczal" href="http://www.theatlantic.com/mike-konczal/" target="_blank">Mike Konczal</a> about <a title="The Conservative Myth of a Social Safety Net Built on Charity" href="http://www.theatlantic.com/politics/archive/2014/03/the-conservative-myth-of-a-social-safety-net-built-on-charity/284552/" target="_blank">"The Conservative Myth of a Social Safety Net Built on Charity"</a>. Instead, this is about <a title="Wikipedia: Worldbuilding" href="http://en.wikipedia.org/wiki/Worldbuilding" target="_blank">worldbuilding</a> — the task of creating an imaginary setting for a speculative fiction novel or series. To be specific, this is about the worldbuilding I had to do for <a title="Starbreaker" href="http://www.matthewgraybosch.com/starbreaker/" target="_blank">Starbreaker</a>, beginning in <a title="Without Bloodshed" href="http://www.matthewgraybosch.com/without-bloodshed/" target="_blank"><em>Without Bloodshed</em></a>. I hope my experience might prove useful to other writers. If you're looking for an argument, I suggest the comments section on The Atlantic's article because never mind the bear — I'm loaded for troll.

With that said, let's start with what can happen when a writer does a shoddy job of worldbuilding. If they're writing fantasy, then their setting is probably going to be an ANSI standard medieval European setting filtered through the limited perspective of high school or (if we're especially fortunate) undergraduate history courses. If they're writing science fiction, expect an ISO standard utopia or dystopia depending on the writer's values, general mood, and the demands of the story.

I tried to avoid these traps, but it's not for me to judge whether I've succeeded. Instead, I set out to depict a near-future society recovering from collapse. I wanted to depict a secular, libertarian society in which people live in city-states whose governments are supervised by a NGO called the Phoenix Society. It's not a perfect world, because crime, corruption, and abuses of power remain a problem that the Society's soldiers — the Adversaries — continually combat.

Because the <a title="Starbreaker" href="http://www.matthewgraybosch.com/starbreaker/" target="_blank">Starbreaker</a> setting isn't a perfect world, shit still happens. People still lose jobs, see their businesses fail, or are crippled by sudden expenses far in excess of their savings. While it's not directly relevant to the plot, I still have consider what happens to the less fortunate in my novels' setting. Because the Phoenix Society treats <a title="Wikipedia: Taxation as Theft" href="http://en.wikipedia.org/wiki/Taxation_as_theft" target="_blank">taxation as theft</a>, it does not permit city governments to levy taxes. The inability to tax precludes the establishment of a <a title="Wikipedia: Welfare State" href="http://en.wikipedia.org/wiki/Welfare_state" target="_blank">welfare state</a>. Without a welfare state, the work of helping the poor falls to individual charity or volunteer organizations such as <a title="Wikipedia: Mutual Aid Society" href="http://en.wikipedia.org/wiki/Mutual_aid_society" target="_blank">mutual aid societies</a>.

On its face, this is a simple chain of logic. It falls apart under close examination. First, if the Phoenix Society doesn't permit the City of New York to tax its citizens, how does the city government get funding to maintain infrastructure, run the fire department, and keep cops on the streets? Is the mayor supposed to run a Kickstarter campaign? Does the city get a monopoly on drug sales, gambling, and prostitution? Second, history shows us what happens when the task of helping the poor falls upon individuals and civil society: try as they might, individual philanthropists and charitable organizations can't handle the burden. People suffer, starve and die before their time for no good reason. Some turn to crime. Civil unrest often ensues.

It gets worse, folks. In the classic right-wing libertarian scenario on which I originally based the Starbreaker setting, there's a wall of separation between commerce and state, just as there's one between church and state. This means that under such a system, governments cannot check the abuses of corporations and private businesses. Business can do as it pleases. If you don't want to consider existing American society as an example, I can provide historical examples beginning with the <a title="Wikipedia: The Gilded Age" href="http://en.wikipedia.org/wiki/Gilded_age" target="_blank">Gilded Age</a>.

As a result, I couldn't continue to worldbuild along right-wing libertarian lines. I would have ended up writing a dystopia if I continued to think through the consequences of the world I was trying to create. Instead, I had to take a different approach. Since I started with my villain, Imaginos, I returned to him and began to think through the kind of society _he_ would work to create if he saw an opportunity to hit the reset button on human society and rebuild it in a manner conducive to his own schemes.

{% include base_path %}
![Imaginos from Starbreaker. Artwork by Harvey Bunda]({{ base_path }}/images/imaginos_harveybunda.jpg)

With Imaginos' goals to as my guide, I realized that the first thing he would do is create a society in which at least 90% of the population is free, secure, prosperous, and _happy_. He'd prefer 99.999%, but he's a realist. Happy people don't foment revolutions. Happy people don't overthrow governments. Happy people don't know or care about things like the Asura Emulator Project, or try to tamper with them or stymie them with regulations or bans motivated by religion or bioethics.

To create a secular, libertarian society that works for just about everybody, Imaginos had to create a disparity between the Phoenix Society's ideals and its methods beginning with how it obtains funding, and how it funds municipal governments. Because Imaginos and the other members of the Society's executive council are the sole proprietors or senior partners of businesses operating in a variety of industries, they can provide a substantial portion of the Society's funding out of their revenues.

Because these businesses are sole proprietorships or corporations, they need not pay the substantial fees the Phoenix Society demands in exchange for incorporation. Nor are they bound by the same restrictions the Phoenix Society places on other corporations, such as a lifespan limited to ten years, or the obligation to act as a <a title="Wikipedia: Benefit Corporation" href="http://en.wikipedia.org/wiki/Benefit_corporation" target="_blank">benefit corporation</a>. Most importantly, they need not fork over half of their profits in exchange for the Society tolerating their continued existence.

That's right, folks: the Phoenix Society taxes the living Hell out of corporations. This is a point Alexander Liebenthal mentioned in <a title="Without Bloodshed" href="http://www.matthewgraybosch.com/without-bloodshed/" target="_blank"><em>Without Bloodshed</em></a> as part of his efforts to expose the Society's corruption. The funds extorted from corporations by the Phoenix Society fund municipal governments. The Phoenix Society also acts as a silent backer to every mutual aid society in human society, covering any shortfalls between donations from members and expected expenditures.

By extorting corporations and wealthy sovereign citizens, using the Adversaries to crack down on those who refuse to pay up, the Phoenix Society props up a social welfare system that appears to be based on individual charity and volunteer organizations. In addition, by curtailing the economic power of corporations, the Phoenix Society fosters <a title="Wikipedia: Adam Smith's The Wealth of Nations" href="http://en.wikipedia.org/wiki/The_Wealth_of_Nations" target="_blank">a form of competitive capitalism</a> more conducive to economic independence for individuals and families.

Moreover, the discrepancy between the Phoenix Society's ideals and its methods fuels conflict between several characters, as well as within them. For example, Morgan Stormrider must cope with being a kind of economic hit man. He'll learn that the Phoenix Society's corruption is part of its fundamental design.

If you're creating an imaginary world for your own fiction, it behooves you to pay attention to how government and the economy will work in your setting. You'll find additional opportunities for conflict, which will permit the creation of richer characters and a more complex story.
