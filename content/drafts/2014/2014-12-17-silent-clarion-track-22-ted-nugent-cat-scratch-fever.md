---
title: "*Silent Clarion*, Track 22: &quot;Cat Scratch Fever&quot; by Ted Nugent"
excerpt: "Check out chapter 22 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
"I saw how you handled Mayor Collins." Saul Rosenbaum chewed his cigar a moment before continuing. If he was going to excoriate me at any point during my daily report from my room at the Lonely Mountain, this would be the time. "The word 'imperious' comes to mind."

"You should be used to it, since you served with Iris Deschat."

The old Director puffed his cigar. "Iris was just like that back in the Navy, especially if someone denied her personnel or materiel she needed to carry out her mission."

A proud-looking woman Saul's age glided into the office behind him and rested a hand on his shoulder. She still wore an Adversary's smallsword on her hip. "So, Saul, you finally decided to throw me over for a younger woman?"

"Ma'am, it's not like that." Why did I have to say that? Their relationship was none of my business, was it?

An indulgent smile made her grandmotherly instead of commanding for just a moment. "Don't worry, Adversary Bradleigh. It's how Saul and I flirt. You'll keep that to yourself, of course."

"Of course. Director Rosenbaum, do you have any further questions?"

"No. Be careful out there. We need additional evidence before we can justify sending you reinforcements." The screen in my room went dark as Saul disconnected.

With my report to the New York chapter complete, I was done for the night. The sensible thing would be to get out of these clothes and into bed.

Unfortunately, I was too restless to be sensible. Where were Fort Clarion's inhabitants? Do they hide during the day? Did they hide today to avoid me and my posse? Was Chris Renfield among them? The questions chased their tails as I sat at my desk.

Fuck it. It's not like I don't know the way to Fort Clarion. With my implant to guide me, finding the place in the dark should be no problem.

Equipping myself with my sword and a small emergency kit, I slipped past a small crowd that had gathered to watch a rather spirited Catacombs & Chimeras campaign. Kaylee refereed, gleefully using imagination and dice rolls to challenge the gathered players.

It was a clear, starry night with a waxing gibbous moon to light my way to the Old Fort Woods. My eyes soon adjusted, though I checked the documentation for my implant's low-light enhancement functions anyway. It would be darker in the forest.

Whippoorwills advertised their presence all around me as I found a small clearing. A rabbit stood on its hind feet, staring at me. I ducked as I heard a hoot and a soft flutter behind me, and a great horned owl swooped down over my head. It plucked the rabbit from the ground, its talons digging deep into the fur to secure its grip. Dinner to go.

A coyote howled as I continued through the forest. Bloody shame I lacked sufficient wilderness survival training to determine if the beast was alone and far off! The howl went unanswered, presenting its own problems; a lone coyote might be rabid. However, it wasn't the coyote I needed to fear tonight.

Had he been with me, Mike might have noticed the signs and guided me away from the cougar's den. Instead, I stumbled upon it. Worse, the den contained a litter of spotted kittens waiting for their mother to bring back a kill. Backing away from the den to avoid disturbing it, I marked it on my virtual map and skirted it. Unfortunately, that wasn't the end of the matter. I'm not sure if the cougar that attacked me was the mother of the cubs I had happened upon, but as soon as I lowered my guard, she pounced.

She would have had me if not for sheer dumbass luck. Leaves rustled behind me, and I turned while unclipping my still-sheathed sword from my belt. I saw the cougar then, her sleek body gathered for the spring that would have let her slam into my back and drive me into the dirt while she fastened her jaws on my neck and snapped my spine.

I overrode the training that told me to make myself a smaller target. That training was for human opponents wielding weapons, not for big cats taking a swipe at lone nighttime hikers. Instead, I wanted to make myself as huge and threatening as possible if the information I pulled with my implant was reliable.

Drawing my sword, I held the reinforced sheath in my off-hand. Rather than  shouting, however, I used my training as a dramatic coloratura soprano and projected the full force of my voice into a sustained high F that I hoped would drive off the puma.

No such luck. The damn cat sprang at me. I stilled my voice and spun aside, hitting the cougar with my scabbard. It struck with a sharp crack, and the cat shook her head as she landed and faced me.

"You pussy! Is that all you've got?" This time I shouted, advancing upon it with my arms spread wide. "You already took your best shot, and you blew it. Bugger off!"

The cougar shrank back, snarling, and sprang a second time. I dodged, my sheath hitting home with a thwack. "Bad kitty!"

She sprang again, but instead of leaping for my throat, she swiped at my legs with one of her massive forepaws. And caught me just above the top of my boot, tearing my jeans and ripping my shin. Enraged by the pain, I rained blows on Puss with my sheath. Though I could easily have finished the fight with my sword, I didn't want to kill the beast despite my pain and anger. Predators will prey.

A strike across the cougar's nose drove her back. I hurled my voice at her, shrilling a high, clear tone. Raising my weapon again, I made to advance. She backed away several steps before bolting into the underbrush. I waited a minute, only to tense as someone… clapped.

"I'm not going to pounce on you." Christopher Renfield approached slowly, showing me his empty hands. This let me relax enough to sheathe my sword.

After clipping my sword  on my belt, I took several deep breaths to regain my calm. "You startled me. Who the hell expects applause after fighting off a cougar?"

"Sorry. I was just impressed by how you handled the situation."

At least I was able to defend myself without killing that cat. Not that I'd admit it to Renfield. "Hello again, Sergeant. How did you find me?" And are you a friend or foe?

His teeth flashed in the starlight. "I followed a high note that pierced the night. A woman's song made a battle cry. Was it yours, Naomi?" He had drawn close enough to whisper my name in my ear.

The energy surge that had filled me faded away with the adrenaline. The pain of my wound and relief all but turned my legs to jelly. Was I really wired enough to flee my bed for a midnight walk before? Hard to believe, because right now I wanted nothing more than to clean out my scratch, bind it, and curl up in bed, but I didn't dare let myself pass out here. It wasn't safe. "That was me. Can you give me a little space? The cat got in a good swipe at my legs."

Renfield nodded, and pointed towards a large boulder just the right height to sit on. I recognized it; the trap door to Mike Brubaker's little hideaway was nearby. "Would you like some help?"

My teeth began to chatter as I tried to answer, and couldn't get any words out. Renfield led me to the rock, and sat with me. "Was that your first time fighting, Ms. Bradleigh?"

The question and Renfield's arm around me helped me focus. "Believe it or not, it isn't."

Renfield's arm tightened around my shoulders. "You did well. You're alive, and so's the cougar. But let's get a little fire going."

Some primal instinct of mine agreed that a fire would be nice, but not out here in the open. That sniper was still out there somewhere. "Got a better idea, Sergeant. There should be a trap door nearby. If we can find it and get underground, I can check my leg and be on my way."

Renfield crisscrossed the clearing a few times, stopped, and crouched. The trap door came up with a soft creak. "Is this it?"

"Probably." Fortunately, I wasn't so badly hurt that I couldn't descend a ladder. Finding the switch by touch, I flicked it. Only one small light worked, and even that was almost too dim to be of use. It was just enough for me to tell that it wasn't Mike's basement. His lacked a fireplace. Taking a match from the canister left by the hearth, I struck one and held it under the flue. If it was clear, the smoke would escape. "See any smoke?"

"Yeah." Seconds later he was tromping down the ladder. "Holy shit. Somebody managed to conceal this old chimney stump and make look like a natural rock formation."

"Hopefully just some squatter who's moved on." It seemed likely, given the carefully folded pile of old blankets I found. They didn't smell bad, so I shook one out and felt for creepy-crawlies. When I found none, I spread it out on the floor.

"Well, how about I get some firewood?" Before I could answer, he brushed a soft kiss against my cheek. "We'll talk more in a bit."

As he left to gather brush, I drew my sword just in case. Its edge gleamed in the dim electric light, and it wasn't long before my hand was strong and steady again.

Renfield returned with the deadwood, and put half of it aside. I sheathed my sword so I could help, but he waved me away and arranged a small pile of firewood in the hearth. Once he was satisfied, he lit the kindling and tended the flame until it was greedily lapping at the wood. "That's better."

"Thanks." Now that I could see properly, I took off my boot. I tried rolling up the pant leg, but the cut was too close to my knee. If I had a knife, I could just cut the leg off these jeans, but all I had was my sword. "I'm sorry, Sergeant, but I need to take a look at the wound."

Renfield turned his back. "Just give the word when you're done."

"Thanks." I shimmied out of my jeans and assessed the damage. The scratch was wider than it was deep. Blood seeped from the gouges, and the skin around it was swollen. Good thing for that damn cougar that she has kittens, assuming I had fought off a mother cat.

"How bad is it?"

"It's inflamed, but not deep. Damned sore, though, and the muscle's already a bit stiff." I cleaned the scratch before applying an analgesic salve containing medical nanotech that cleared out pathogens and sealed wounds. "Damn, this stuff works fast."

"What is it?"

"Just a standard-issue first aid gel. I've never had to use it before." Unfortunately, none of the accompanying band-aids were big enough. "Can you help me wrap this up?"

Renfield glanced over his shoulder. "You sure?"

"Quite." I wrapped the gauze around my leg and held it in place once I was done. "I need an extra pair of hands."

Renfield knelt before me, resolutely keeping his eyes on my face. "What do you need me to do?"

"Cut the gauze, and tape it up. I'm not sure I can do it myself without it all unravelling."

Renfield drew a knife and carefully sliced the gauze where indicated. He then taped me up, using just enough to keep my leg wrapped. Once finished, he gazed at me with a small, playful smile curving his lips. "Want me to kiss it better?"

Nice of him to ask first. Remembering how his lips seared mine the first time we met, I reached down and ran my fingers through his crew cut. "You're welcome to try, Sergeant."

---

### This Week's Theme Song

"Cat Scratch Fever" by Ted Nugent

{% youtube fit99l6kHyA %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
