---
title: Blackened Phoenix Progress Report
categories:
  - Project News
tags:
  - accountability
  - #amwriting
  - progress report
  - blackened phoenix
  - word count
---
I really need to be better about reporting my progress on my books as I write them. I'm sure I have a few fans who would be interested in knowing I don't spend all my time outside of work being a Sunbro and playing _Dark Souls II_. 🙂

So, here's the current word count: **17713**, with approximately **800** written today at lunch. One of these days I'll write a shell script that generates time-stamped draft files and compares word counts by day so I can get a delta every time I run it. 🙂

The most recently completed scene is scene one of chapter four, **"Keep Your Enemies Closer"**. Munakata Tetsuo just paid Alexander Liebenthal a visit in his room at the Sonamura Psychiatric Hospital in Honolulu, and persuaded him to shut up and keep his head down.
