---
id: 1323
title: U2 on Your iPhone? DO NOT WANT!
date: 2014-09-15T08:45:47+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1323
permalink: /2014/09/u2-on-your-iphone-do-not-want/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapBG:
  - |
    s:323:"a:1:{i:0;a:7:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:8:"isPosted";s:1:"1";s:4:"pgID";s:67:"http://matthewgraybosch.blogspot.com/2014/09/u2-on-your-iphone.html";s:5:"pDate";s:19:"2014-09-15 08:47:00";}}";
snapDI:
  - 
snapGP:
  - 
snapIP:
  - 
snapLJ:
  - 
snapPN:
  - 
snapSC:
  - 
snapSU:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snapWP:
  - 
snap_isAutoPosted:
  - 1
snapLI:
  - 
snapDL:
  - 's:179:"a:1:{i:0;a:5:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";}}";'
bitly_link_googlePlus:
  - http://bit.ly/1LDPYPN
bitly_link_twitter:
  - http://bit.ly/1LDPYPN
bitly_link_facebook:
  - http://bit.ly/1LDPYPN
bitly_link_linkedIn:
  - http://bit.ly/1LDPYPN
sw_cache_timestamp:
  - 401573
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Rants
tags:
  - Apple
  - captive audience
  - consent
  - do not want
  - iPhone
  - Metallica
  - push marketing
  - U2
---
Seems Apple has decided to stick U2's new album on people's iPhones even if they didn't request a copy on the iTunes store. [Some people are understandably annoyed.](http://www.independent.co.uk/arts-entertainment/music/news/tyler-the-creator-compares-having-the-new-u2-album-automatically-downloaded-on-his-iphone-was-like-waking-up-with-herpes-9732091.html)

I don't blame them. For starters, U2 have a lot in common with Metallica. They're over-privileged short-haired rock stars who haven't released a good album in years. (Sorry, but _Death Magnetic_ doesn't redeem _St. Anger_.)

I'd be miffed as well if Google, LG, or T-Mobile tried a stunt like this.

  1. I don't like U2.
  2. I don't like push advertisements.
  3. I don't like having my storage used without my consent.
  4. I don't like strangers deciding what music I should hear.

Hell, I'd be miffed if Amazon did this to Kindle owners, and I'd go from miffed to Homeric rage in less than 60 seconds if Amazon was putting _my_ books on people's devices without their consent.

**I don't want a captive audience.**

What I don't get is why U2 wants to shove their latest album down the throats of people who have expressed no interest in U2. Did Bono give all his money away and realize he didn't hold any back for hookers and blow? Is this some kind of ego trip?

Regardless, I'm glad I jumped the Apple ship when Steve Jobs kicked the bucket. The company's as fucked as it was the last time Jobs was removed from the CEO's office, but this time the board of directors will need a good necromancer.

Of course, some people like [Bernard Zeul at the _Sydney Morning Herald_](http://www.smh.com.au/entertainment/music/free-u2-album-delete-it-when-you-want-so-why-the-whingeing-20140915-10h1n4.html) don't get it. They don't realize that removing unwanted music is unnecessary work, and that it still shows up in your history where it can be used to embarrass you if you're the sort of person whose friends judge people by their playlists.

(If your friends do this, get better friends. If you do this, fuck off and die.)