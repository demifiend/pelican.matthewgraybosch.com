---
id: 817
title: The Trolls are Slacking
date: 2014-06-30T19:48:12+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=817
permalink: /2014/06/trolls-slacking/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1PiTXAY
bitly_link_twitter:
  - http://bit.ly/1PiTXAY
bitly_link_facebook:
  - http://bit.ly/1PiTXAY
bitly_link_linkedIn:
  - http://bit.ly/1PiTXAY
sw_cache_timestamp:
  - 401682
bitly_link_tumblr:
  - http://bit.ly/1PiTXAY
bitly_link_reddit:
  - http://bit.ly/1PiTXAY
bitly_link_stumbleupon:
  - http://bit.ly/1PiTXAY
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - aspergers
  - deserving targets
  - isla vista
  - lazy
  - mockery
  - praising murderers
  - public scrutiny
  - trolls
---
This wasn't how I intended to close the month, but I finally got the sort of response I _expected_ to receive to my post <a title="He's still a murderous asshole." href="http://www.matthewgraybosch.com/2014/05/26/elliot-rodger-was-not-nice-guy/" target="_blank">on a certain murderous asshole</a>. Considering I posted it on May 26th, that means it took more than a month before a troll showed up. Rather than simply approve his comment, I think the comment I received merits public scrutiny. Besides, I'm still sick and I feel like abusing a deserving target.<figure id="attachment_819" style="width: 912px" class="wp-caption aligncenter">

[<img class="size-full wp-image-819" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/06/troll_face_with_problem_high_quality__by_mioakiyama0112-d654qm5.png?resize=840%2C806" alt="It's fun to troll the trolls." data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/06/troll_face_with_problem_high_quality__by_mioakiyama0112-d654qm5.png)<figcaption class="wp-caption-text">It's fun to troll the trolls.</figcaption></figure> 

I won't reproduce the name or email address of the person responsible, which is the one courtesy I'm prepared to offer. Instead, let's focus on the message.

> How dare you insult this great hero! Women are ALL scum for dating guys who treat them like dirt and then complain about "Nice guys" and male entitlement. He is a hero and will always be a hero and you are simply trying to destroy justice for those with Asperger syndrome. I'm 21 and NEVER had a girlfriend because women are scumbags, FACT! Life is unfair for some people, that's why Rodgers existed. Don't tell me man up because I'm a pussy and proud of it!
> 
> IF ONE GIRL SAID YES, no one would die.
> 
> Oh, and if a girl says "I have a boyfriend", I should shout "You bf is a man, and you hate men you hypocrite!"
> 
> This is all truth. You are wrong and I'm right, please understand that.

Let's get something straight. I dare insult the Isla Vista killer because he's dead, and because he was no hero. He was just a murderer who didn't have the guts to kill with his own hands. He was nothing without his guns, and his rampage could have been stopped if the police that came to his apartment brought a search warrant and turned the place upside down.

I find it hilarious that the author of this comment speaks of justice for those with Aspergers, given that the killer was <a title="The asshole didn't have Aspergers. Deal." href="http://www.latimes.com/local/lanow/la-me-ln-frantic-parents-isla-vista-shootings-20140525-story.html" target="_blank">never diagnosed with an autistic spectrum disorder</a>. If he had a mental problem, as a prescription for the antipsychotic Risperidone with which he did not comply might indicate, <a title="The asshole had anger issues." href="http://www.forbes.com/sites/emilywillingham/2014/05/30/elliot-rodger-didnt-have-autism-he-had-anger/" target="_blank">his biggest problem was most likely his anger</a>.

I've been there myself. I've been angry. I've been lonely. I've been frustrated. I was a virgin until I was 25, and my first was my wife of ten years.

So, when you call women scumbags because they act like the autonomous human beings they are and do what they think is best for them without considering your feelings, you're also calling my wife a scumbag. God might forgive, but I expect better from my fellow man.

Here is the real truth: nobody is entitled to anything in this life. Not love, not sex, not friendship. Nor are you entitled to arbitrarily claim that you are right simply because you disagree with me. The evidence is not on your side.