---
id: 641
title: Josefine Malmgren Gets a Bonus
excerpt: Josefine is a character you'll be seeing a lot of in *Blackened Phoenix*.
date: 2014-05-24T13:02:59+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - Isaac Magnin
  - josefine malmgren
  - office
  - overtime
  - Polaris
  - sabbatical
  - Saturday Scenes
  - Tamara Gellion
  - the blackened phoenix
  - vacation
  - work
---
Here's a **Blackened Phoenix** scene I wrote before my Chromebook arrived.

I'm thinking of splitting it, however, and using the stuff where Dr. Malmgren gets her bonus (and a nice long vacation) in another scene where she becomes suspicious of her patron's generosity.

Artwork's by <span class="proflinkWrapper"><a class="proflink aaTEdf" href="https://plus.google.com/114522547167183191785">Harvey Bunda</a></span>.

[<img class="aligncenter size-large wp-image-642" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/05/grayboschwrites-662x1024.jpg?resize=474%2C733" alt="grayboschwrites" data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/05/grayboschwrites.jpg)

Josefine trembled despite the warmth of Isaac Magnin's office, and pulled her cardigan tight around herself. The knitted garment, once a snug fit, was now well on its way to shapelessness thanks to her nervous tugging. The lush leather seat in which she sat had been made to accommodate a much larger person, and she felt lost in it as she stared across the desk at an empty chair.

To distract herself, she pulled her tablet from her purse and pulled up a copy of the report she sent Magnin after the bank robbery. Despite each page bearing her name at the top, the report's substance seemed alien to her, as if somebody else had written it.

<p style="padding-left: 30px;">
  Polaris' ethical outlook is fundamentally non-human. He was determined from the beginning of the robbery to kill Addison Mortimer and his associates, and repeatedly antagonized them after Charles Kimes threatened to kill everybody in the bank after striking down the first to defy them. Despite his rashness, however, Polaris secured the hostages' safe release.
</p>

<p style="padding-left: 30px;">
  Moreover, his capabilities as the Project Aesir prototype &#8212; or a 200 Series Asura Emulator &#8212; make him extraordinarily dangerous to other people. I was unable to restrain him, or even to see what he was doing, once Mortimer ordered Kimes to punish Polaris for his defiance by harming me.
</p>

<p style="padding-left: 30px;">
  Should he or somebody close to him be threatened with violence, Polaris responds with a vastly disproportionate level of force. He seems intent not merely on eliminating present threats, but seeks to do so in as shocking a manner as possible to deter future threats. He would have killed Addison Mortimer despite having restrained him, if not for Tamara Gellion's intervention.
</p>

<p style="padding-left: 30px;">
  As one of his creators, I think it incumbent upon me to state for the record that Polaris is a danger to those around him. Our reuse of design artifacts and software libraries from the North American Commonwealth's original Asura Emulator Project may prove our greatest mistake, for they give Polaris combat abilities suitable only for total war.
</p>

"It's rather late for revision, Dr. Malmgren." Josefine looked up from her tablet to find Isaac Magnin sitting across the desk from her. He smiled at her, and tapped his own tablet. "I already read your report."

"Do you think I'm overreacting?"

"I might, were I unable to compare the events you described with video from the bank's security cameras." Magnin leaned back in his seat. "You left much unsaid concerning your encounter with Ms. Gellion."

_I saw her stop a bullet mid-flight, but if I say so I can kiss my credibility good-bye._ Josefine forced herself to breathe. "I included only the details I deemed relevant to my concerns about Polaris, Dr. Magnin. My recollection of Tamara Gellion's involvement is unreliable due to the stressful nature of my experience. No doubt she filed her own report."

"Do you suspect AsgarTech of researching force field technology?"

"Tamara Gellion said AsgarTech asked her to negotiate on their behalf. What better opportunity to field-test such tech, if we've been working on it?"

"Excellent point." Magnin rose, and retrieved a decanter and two glasses from a sidebar. "Care for a brandy?"

"I shouldn't drink on the job."

Magnin filled both snifters. "I recall you being on leave."

Despite her protest, Josefine accepted the brandy. Her fingertips brushed against Magnin's gloved hands and sparked a chill throughout her body. Instead of dwelling on the sensation, she sipped the liquor. It slipped over her tongue, warming her throat first before suffusing the rest of her.

Magnin tasted his own. "Ms. Gellion made no mention of her conversation with you in her report. Would you be so kind as to tell me what happened?"

Josefine drank half her brandy while recalling the conversation. "She knows too much. She commanded Polaris to let Addison Mortimer go, and he obeyed. She identified Mortimer as an asura emulator, and was unfazed when I asked her what she knew about the Asura Emulator Project."

"Why did you mention the Project to Ms. Gellion?"

"If Mortimer's one of the original asura emulators built before Nationfall, shouldn't he look much older? She knows more than she's willing to tell me."

Magnin nodded. "How did she phrase her refusal?"

"She said I don't factor into her plans, and suggested I leave Asgard. She recommended Armstrong City for a long vacation." Suspicion arose in Josefine's mind as she recalled Gellion's words. "Tamara Gellion seems too young to have been involved in Nationfall, just like Mortimer. Who is she, and why is she aware of the Asura Emulator Project?" She gave Magnin a hard look. "How do you know her?"

"It's complicated. You need not concern yourself." Magnin emptied his glass, and set it down with the finality of a judge's gavel. "Tell me about Polaris. You spend a great deal of time with him despite being on leave. I want your honest opinion in its entirety, not just what you felt you could justify putting in writing."

Josefine swallowed, and looked away from Magnin. She finished her brandy, and left the glass on his desk.

"Perhaps another drink?"

"No, thank you. One's my limit." She began to study a painting that had caught her attention the first time she visited this office. It depicted a man in a dark frock coat standing atop a crag, staring across a sea of fog with his back to the viewer. His platinum hair streamed behind him. She reached up to touch the canvas, but stopped short of marring a brushstroke with her fingertips. _This is an old painting, perhaps an original. Could this be an ancestor of Dr. Magnin's, or just somebody who resembles him? It's unfortunate that his face is obscured._

She lowered her hand as Magnin joined her. "Is he family, or is it just an uncanny likeness?"

Magnin chuckled. "An ancestor of mine. He's the same Isaac Magnin for whom Mary Shelley inscribed that edition of Frankenstein."

"I fear Polaris." The confession escaped Josefine's lips, naked and unbidden. "And I fear for him. His presence unnerves me. He decided to kill Mortimer and his crew the second they announced their intentions. Nor can I say he was unjust, but his understanding of justice allows no room for mercy."

"He fought like a demon for your sake, Dr. Malmgren."

Josefine stared at her feet, unable to face Magnin and admit he was right. "Your description is too apt for my liking. I suspect a fundamental flaw in Aesir OS, or perhaps in our thinking. We gave him access to all of humanity's knowledge, and assumed he'd learn to be human on his own soon after activation. That's not fair to him. It's like leaving a loaded firearm where a toddler can find it, and blaming the kid if he shoots somebody."

A glass filled less than half-way with orange brandy appeared before Josefine, cradled in Magnin's hand. She accepted it despite her prior objection. "The problem shouldn't be insurmountable. Household AIs possess full self-awareness from the moment of activation, but they learn to interact with people on a human level. We can still reach Polaris."

Josefine recalled a Sumerian epic she had been obliged to study at university as part of her required study of the humanities. The people of Uruk cried out for relief from their king's oppression. The gods heard their prayers and fashioned a man who would become Gilgamesh's companion. Ignorant of his nature, he acted as a beast. "Perhaps we created an Enkidu, rather than a Frankensteinian monster. Unfortunately, I'm not the priestess who can teach our Enkidu how to be a civilized man." _I bet Claire could tame him._

Magnin gave his ancestor's portrait a last glance before turning away. "Is Polaris' unhuman nature your only concern?"

"It's my greatest concern. If he were human but could still fight as he did, I think he'd show greater restraint." Josefine paused for effect. "The Vomisa Principles aren't something we can code into an AI's operating system. Adherence is voluntary."

"It's obvious you've given the matter careful thought despite my instructing you to take time for yourself and rest."

Magnin settled behind his desk, and beckoned Josefine. "Please sit."

"Am I not responsible for Polaris?"

Magnin shook his head. "Let me worry about Polaris. As your patron, allow me to say it's high time you worried about yourself. The Phoenix Society is concerned that I've been exploiting you."

"You've given me no cause to complain about working conditions, Dr. Magnin."

"An Adversary came by and checked the hours all our employees report against the hours you all actually spend in the building." Magnin's expression hardened as he leaned forward. "In your case the discrepancy is visible from space."

_An Adversary actually came here to check? Holy shit._ Because of the stories Claire told, Josefine imagined most Adversaries to be cut from Morgan Stormrider's mold: merciless killers dedicated to upholding the Phoenix Society's ideals. "Are you in trouble? Can't I talk to them and explain?"

Magnin shook his head, but his expression softened. "They wouldn't listen, but you need not concern yourself on my behalf. The matter is easily rectified."

"How?"

"Check your bank account. We reviewed your hours, and paid you time and a half for every hour over thirty you worked in a given week. You should have enough for a nice long vacation. Hell, you could probably retire."

Josefine complied, and used her implant to access her account. Unable to believe the figure quoted, she tried crunching the numbers herself. "Dr. Magnin, this is entirely too much, even at standard overtime rates."

"Oh, did I neglect to mention that you got a raise, retroactive to the beginning of last year?" Magnin chuckled. "That was naughty of me."

"This is more than twice my original salary."

"It's triple, actually. What's wrong, Dr. Malmgren? You don't think you're worth it?"

"Worth it?" _Why do I sound like I just sucked down a lungful of helium?_ "Isaac, you were paying me almost double the industry average for a developer of my qualifications and experience in the first place."

"Do I look like I give a damn about the industry average? Most corporations pay their employees just enough to stave off a strike. I see no reason to emulate their example. Let them emulate mine, instead."

Josefine took a breath, and forced herself to understand Magnin's reasoning. "Thank you. When would you like me to return to work?"

Magnin shrugged. "Next year."

"Next year?"

"Yes, Dr Malmgren, next year. That was part of the deal I made with the Phoenix Society: compensate you for your time, and give you adequate time off with which to rest and recover. For the next year, you're on sabbatical."

_What about all the people who depend on me? That Wei kid in particular is going to be a disaster without an experienced hand to guide him._ "Don't you need me to keep Project Aesir running smoothly?"

"No. You're to put aside your work at AsgarTech for a year. You'll be paid your current salary."

She blinked at this. "You're going to pay me to do nothing?"

"I don't believe you ever learned how to be lazy." An insouciant little smile curved Magnin's lips. "In fact, I'd enjoy seeing what you get up to now that you can do almost anything."
