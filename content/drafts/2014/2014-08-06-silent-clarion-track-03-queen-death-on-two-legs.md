---
title: "*Silent Clarion*, Track 03: &quot;Death on Two Legs (Dedicated To)&quot; by Queen"
excerpt: "Check out chapter 3 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Naomi might have gone a little too far, and her partner Jacqueline is concerned. Time for some girl talk."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Queen
    - Death On Two Legs (Dedicated To)
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
We had no trouble getting seats on the Tube when we finished at MEPOL, which is always a pleasant surprise. It meant Jacqueline and I could sit and relax without worrying about our swords poking or tripping somebody. The train thrummed beneath my feet as it accelerated, and I let my eyes slip shut for a quick nap.

Jacqueline had other ideas. "Don't fall asleep on me, Nims."

"Why not?" I really didn't want to open my eyes. Though today's mission wasn't even close to being my toughest, I was worn out. "We're the only people in this car."

"I wanted to talk with you." She actually looked concerned, which made me nervous. "You looked ready to feed those cops their own bollocks back there. What's up?"

I shook my head. "Don't wanna talk about it."

"Not good enough, Nims." Jacqueline tugged on one of her tight black curls. "We're getting off at the next stop and finding a pub."

"I dumped John last night, Jackie. That's all."

"No, that's not all." This quiet vehemence in her voice surprised me. Jacqueline normally broadcasts her anger for all to hear. Was she clamping down for my sake? "We watch each other's backs because we're both Adversaries. If something's bugging you, and you escalate a tense situation, that could damn well get me hurt. Wouldn't you be concerned if I had been the one to lose my cool?"

She's right, but I still didn't want to talk about it on the Tube. "Do we have to discuss it here?"

"Not at all. Like I said, we'll find a pub."

I made a show of checking the time. "Isn't it a bit early for a pub crawl?"

She shrugged. "Chattan's orders. He saw the feeds. But we're friends, Nims. If he hadn't given the order, I would have dragged you out tonight anyway."

We found a pub called the Rampant Stallion, notable because the sign incorporated both the heraldic sense of the word and the sexual one. Jacqueline and I were the only women there, and the bartender gave us an appraising eye. I wasn't surprised; we're a study in contrasts. "If you had a third Adversary with you, ladies, I'd assume this was a joke."

"No joke." Jacqueline laid down a banknote. "A pint of your best for me, and a glass of your house red for my partner. And put us somewhere quiet and out of the way. Girl talk."

The bartender nodded, and signaled a waiter. "You might prefer a booth in the back, then. Charles will see to your needs."

"This way, ladies." Charles seated us in the back, well away from everyone else. The booth was dark, and lit only by a small wall-mounted lamp. He left for just long enough to bring our drinks. "Would you like something to eat? Today's specials are listed on the front page."

Jacqueline sipped her beer as she flipped through the menu. "Curry sounds good. How about you, Nims?"

I tasted my wine. It was a bit dry, but I liked it that way. "A steak done medium rare, Charles, if that's available?"

"Of course, Adversary." He smiled at Jacqueline before rushing off.

"I think he likes you, Jackie." Not that I blamed him. She's shorter than me, much darker, and a bit curvier. More importantly, her default expression is also friendlier and more open.

Jacqueline barely shrugged. "Too bad for him. I'm taken."

I leaned in, interested. Last week, she was single and just a bit bitter about it. Not that I blamed her. You wouldn't believe how much of a pain it can be to date, as an Adversary, if you aren't willing to shit where you eat by hooking up with another Adversary. It's bad enough that many just give up and get their itch scratched at a brothel. "Found someone new already? "

Jacqueline also leaned closer. "He's the vicar at my church."

"A vicar?" I couldn't resist a little tease. "I wonder what the Bible says about that."

"I have faith that God will forgive a bit of non-marital sex. He's supposed to be good like that." She gave me a funny look, as if she expected me to take offense. "Am I out of line? Adversary's honor, I had no idea you were devout until you invoked the deity at MEPOL."

"I'm not." Instead of elaborating, I started flipping through the wine list. Never mind that the house red was perfectly adequate, it gave me a moment to consider my response. Talking about sport, religion, or politics is a great way to alienate people, so it never hurts to be careful. "I'd rather talk about John than talk about our beliefs, and I really don't want to talk about John."

"What happened? Did you two fight?"

"He wanted to get married."

Jacqueline blinked. "What happened, Nims? Did he propose? Did you turn him down?"

"Death on Two Legs didn't propose to me."

Jacqueline stared at me. "Did you just call John 'Death on Two Legs'?"

"That's his new name. Got a problem with that?"

"I keep forgetting you listen to old music." She smiled, and finished her pint. "Hell no. Tell me the rest."

I decided to let her have it. "I'm not worthy of being Bride-of-Death on Two Legs because I've got CPMD, and can't give his aristo parents grandchildren. No, he just wants to keep me around as his exotic fuck doll for after he's done his duty for his family by knocking up the normal aristo girl they picked to be his bride."

I stared at my wine. No way I was already drunk enough to just let everything out like that. Maybe I'm just too angry to give a shit about how I sound right now. I drank the rest, and wished I had the bottle handy.

"I hope you told him to fuck off."

"I told him to fuck off at gunpoint. What really bugs me is that the prat called my parents afterward, and begged them to get me to take him back. Who the hell does that?"

"Not somebody I'd want in my life." Jacqueline sat back as Charles brought our food and refilled our glasses. She sniffed, and a broad grin spread across her face. "Damn, this smells good."

"Please enjoy, ladies."

I took a bite, and the meat melted in my mouth, leaving a hint of citrus and spices from whatever marinade they used here. It fit perfectly with the wine. So I was hungry. Who knew?

Of course, Jacqueline had to ruin it by spooning some of her curry onto my plate. "Nims, you gotta try this."

I'll put this as delicately as possible: the last time I tried chicken korma, it disagreed so violently with me that we fought to the death. Regardless, I made a valiant effort. It tasted the way loud sex in an inappropriate venue feels, and was redolent of coconut and turmeric. I sliced a bit of steak for Jacqueline. "That was good, but try this."

"Holy mother of fuck, Nims. I'd shag the chef for the recipe. Hell, I'd let him take the back door."

"I doubt even your luscious arse is sufficient payment, Jackie." I gestured with my fork. "I was right to dump John, wasn't I?"

"What the fuck is wrong with you?"

I stared at the remnants of my steak, and idly sliced off a bit without eating it. I let go of the one detail I had held back in my little rant. "It was our anniversary. We-"

Jacqueline cut me off by thumping the table with her fist. "There's no 'we' between you and that limp-dicked waste of ammo. He had his chance and he fucking well blew it."

I looked around, sure we had attracted attention, which was the last thing I needed today. No doubt I caused enough trouble at MEPOL.

"Ow!" I reached down and rubbed my shin, where Jackie had kicked me under the table. I glared at her. "What the hell was that for?"

"Pay attention, Nims. I asked you a question. John didn't have the balls to defy his family for you, and you deserve a guy who would defy God Itself. Now, how do you really feel about him?"

How do I feel about John, knowing what a spineless creep he is? "I fucking despise him. I can't believe I ever let him touch me."

Jacqueline nodded sagely. "It's better to despise your ex than to despise yourself."

"So, what should I do now?"

"You were a demon-ridden idiot for coming in today. I could have handled MEPOL without you."

That stung my pride. "Go to Hell, Jackie. I'm not going to stay home and mope just because Death on Two Legs ruined our anniversary."

"So what are you going to do if you break your leg or get shot? Still going to insist you can do the job?" I kept silent, suspecting a rhetorical question, and Jacqueline continued. "Christ on a motorcycle drinkin' cheap vodka, I thought you were the smart one."

"Oh, all right. I'll just tell Chattan I need a week or two off to cry over Death on Two Legs. That'll work."

Jacqueline shrugged. "Why do you think Chattan took leave a couple of months ago? His wife divorced him out of the blue. Poor bastard came home to an empty flat and a letter with divorce papers on the kitchen counter. She even emptied the fridge."

I stared at my plate, unsure of how to respond.

"Take some leave. You're due for some R&R anyway, or you'll bloody well burn out. In front of a suspect looking for an edge on us, with your luck."

No way to argue with such logic. I finished my steak. "I did promise my mum I'd visit."

---

### This Week's Theme Song

"Death On Two Legs (Dedicated To)" by Queen, from *A Night at the Opera*

{% youtube kqVpk0qxmfA %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
