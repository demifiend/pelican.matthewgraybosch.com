---
id: 188
title: 'Tattoo Vampire &#8211; Part 4'
excerpt: Here's a story I wrote to promote *Without Bloodshed*. It also ties into *Silent Clarion*.
date: 2014-03-11T07:00:12+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - fiction
  - morgan stormrider
  - project harker
  - science fantasy
  - science fiction
  - serial
  - short story
  - starbreaker
  - tattoo vampire
---
Recalling his briefing and Christabel's reaction to news of the mission allowed Morgan to pass the time as he pretended to be unconscious in the back room of Dusk Patrol Tattoo. He sat on a chair with a narrow back as Westenra worked on him. Peering through his eyelashes at the mirrors lining the wall allowed him to see almost everything.

A line led from the needle in his left arm to an old machine that Morgan's basic medical training allowed him to identify as a target-controlled infusion system. The nanotech with which he was injected prior to the mission talked to his implant, sending real-time reports every time the TCI system dosed him with propofol or remifentanil. Before each dose, Westenra put aside the tattooing gun, sliced Morgan's bicep with a scalpel, and lapped at the wound. "What are you, pretty boy, that you heal so quick and clean? Too bad I can't keep you. I might feed on you for ten thousand years."

_Just finish the fucking tattoo so I can arrest you._ _Please._ The tattooing gun buzzed while biting into his skin, and Morgan's jaw ached from gritting his teeth. _Eddie would probably_ _call me_ _a pussy if I told him that the nanotech he gave me worked too damned well._

"There you go, pretty boy." Westenra whispered as the tattoo gun ceased its buzz. Something cool and wet pressed against Morgan's arm, gently caressing his abused skin for a moment. It was soon replaced with something dry and gauzy. _He's applying the dressing._ _Taking a little taste every few minutes won't be enough any longer._

Westenra's tongue darted out to moisten thin lips as he brushed Morgan's hair aside to better expose his neck and shoulder. "Such a lovely youth, with such soft skin. Am I the first to taste you?"

A sigh barely escaped Morgan's lips as Westenra's brushed the juncture of his neck and shoulder. Despite his revulsion at being touched in so intimate a manner by somebody who believed him unconscious and unable to object, his mind called up a fresh memory of Christabel doing the same. Next came a firmer kiss, and the sort of suction Morgan knew would leave bruises. They began to fade as soon as Westenra lifted his lips from Morgan's shoulder, just as they did with Christabel. _At least Christabel won't see them and think I'm cheating on her._

"You seem to like that." Westenra's lips brushed Morgan's ear as he whispered, and sudden pain radiated from Morgan's chest as his quarry found one of his nipples. _He's doing to me everything his victims alleged, and it's all on record through Witness Protocol._ "I was just going to taste you, but you seem to want more of me. First, however, I need to feed on you. It won't hurt at all. I promise."

The need to turn the tables on Westenra mingled with his own fury; despite not being attracted to men in general, Morgan could not keep his body from responding to Westenra's rough fondling – or to the sharp burn of fangs slipping into his flesh to draw his blood.

Westenra's bite was not the paired punctures Morgan expected from the few vampire films he watched. Instead, they tore into him as if Westenra were a vampire bat, dragging deep furrows into his flesh. They welled with his blood and overflowed, allowing Westenra to lap at the wound.

Once Westenra was satisfied, he drew back and rubbed himself through his trousers. Still watching through his eyelashes, Morgan allowed himself a slight smile as Westenra licked his lips and reached for the buckle of Morgan's belt. _Enough._

Morgan sprang to his feet, ignoring the flare of pain in his left arm as he tore free of the TCI system, and grabbed Westenra by the throat. He threw him backward, shattering the mirrors behind Westenra from the impact. "Quincy Westenra, by virtue of my authority as an Adversary sworn to the Phoenix Society, I place you under arrest. You have the right to–"

"I have the right to _what_, Adversary?" Westenra laughed, and retrieved a lever action carbine from behind a cabinet. He snapped off a shot, which punched through Morgan's chest.

Morgan staggered several steps backward, and dropped into a crouch. He bowed his head, ducking the second shot as his hands made contact with the floor. As Westenra worked the lever to load a third shot, Morgan sprang at him, driving his shoulder into the suspect's midsection and knocking him to the floor.

Morgan's momentum left him straddling Westenra. He ripped the carbine from Westenra's hands, and threw it aside before slamming his head into the floor beneath him with a palm strike to the face. Drawing his knife from his boot, he held it against Westenra's throat_._ As the edge drew blood, Morgan's pulse quickened. He pressed harder, rapt as the steel slipped into his prey's flesh, and stopped. _My prey? No. I'm an Adversary, not some feral beast. I have a mission._

Morgan withdrew the knife, holding his other hand against Westenra's wound to slow his bleeding. "You have the right to remain silent, pusbag. You have the right to legal representation. You have the right to communications for the purpose of preparing your defense. You have the right to humane treatment while in custody. Do you understand your rights?"

The answer burned as it slid between his ribs. Morgan rolled off Westenra while pulling the knife from his side. He dropped it and took up his enemy's carbine. The first shot rent Westenra's heart asunder. The second spread his brains across the floor. Morgan dropped the carbine on the carcass of his first kill as an Adversary, and waited to tend his wounds until he was well away from Dusk Patrol Tattoo.
