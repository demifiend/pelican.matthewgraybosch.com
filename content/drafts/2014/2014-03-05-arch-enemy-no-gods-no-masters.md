---
title: "Arch Enemy: &quot;No Gods, No Masters&quot;"
excerpt: The classic anarchist slogan, now a melodic death metal anthem
date: 2014-03-05T11:54:26+00:00
header:
  image: arch-enemy-chaos-legions.jpg
  teaser: arch-enemy-chaos-legions.jpg
categories:
  - Music
tags:
  - anthem
  - Arch Enemy
  - death metal
  - defiance
  - inner struggle
  - Khao Legions
format: video
---
I don't normally listen to death metal, and I promised myself I'd let go of my anger. However, I'm using this Arch Enemy anthem as a battle hymn against the worst aspects of myself.

<iframe width="560" height="315" src="https://www.youtube.com/embed/hBLugrlPZ2U" frameborder="0" allowfullscreen></iframe>
