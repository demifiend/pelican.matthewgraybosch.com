---
title: Our Daily Brands
excerpt: Who needs God when you've got brand loyalty - or fandom? It's not like the Doctor will judge you for your sex life.
header:
  image: communion-bread-wine.jpg
  teaser: communion-bread-wine.jpg
categories:
  - Social Media
tags:
  - brands
  - Capitalism kills God dead
  - fandom
  - holy wars
  - psychology
  - religion
  - research
---
[Pacific Standard](http://www.psmag.com) staff writer Tom Jacobs writes in ["Give Us This Day Our Daily Brands"](http://www.psmag.com/navigation/business-economics/give-us-day-daily-brands-91674/) that:

> Some people’s loyalty to certain brands—think Apple—borders on the religious. If you’ve ever wondered why, consider the idea that religious affiliation and close identification with brands fulfill some of the same basic psychological needs.
>
> That’s the conclusion of a research team led by [Keisha Cutright](https://marketing.wharton.upenn.edu/profile/1669/) of the University of Pennsylvania’s Wharton School. It reports that when brands serve as a powerful source of self-expression, people are less likely to report, or demonstrate, strong religious commitment.
>
> It seems if you’re wearing the right brand of jeans, you have less need for Jesus.
>
> “These findings imply that religiosity is less stable than it seems, and can be shaken by a substitution that may seem superficial to many—brands,” [the researchers write](http://psycnet.apa.org/psycinfo/2014-38362-001/) in the Journal of Experimental Psychology: General.

I think we can extrapolate from these findings. I think fandom can _also_ be a substitute for religious faith, because fandom can _also_ serve as a powerful source of self-expression.

Sure, you can be both a metalhead and a Christian, or a Whovian and a Christian, but why bother? I'm not the only one asking this, if the results of a Google search for ["Heavy Metal is my religion"](https://www.google.com/?q=heavy%20metal%20is%20my%20religion#q=heavy+metal+is+my+religion) is any indication. Also, why venerate a Jewish carpenter who died to save humanity when the Doctor has died to save the entire universe almost a dozen times? (Maybe Jesus is a Time Lord? That explains _everything_.) And what about the [people who write "Jedi" in the religion field on census forms?](http://en.wikipedia.org/wiki/Jedi_census_phenomenon)

I have to wonder, though, if there are Orthodox Jedi who believe Han Solo fired first and Reform Jedi who insist Greedo fired the first shot. It would make sense, given George Lucas' propensity for monkeying around with his films.

I'm not joking, by the way. Spend any time in fandom, and you'll see arguments over such issues debated as if they were as important as whether Jesus is God, or merely the Son of God &#8212; because they _are_ that important to the participants. Lilith's heart-shaped ass, you can even start [a holy war over which Unix text editor is best](http://en.wikipedia.org/wiki/Editor_war).

* * *

<img src="http://i2.wp.com/imgs.xkcd.com/comics/real_programmers.png?w=840" alt="XKCD: &quot;Real Programmers&quot;" data-recalc-dims="1" />

* * *

Now, I understand that many adherents of traditional religions like Judaism, Christianity, and Islam will find these conclusions objectionable. The fact remains that maybe the Beatles were right back in the day. Maybe, for a little while, they _were_ bigger than Jesus &#8212; at least for their most ardent fans.
