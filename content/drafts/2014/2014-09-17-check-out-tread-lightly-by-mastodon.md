---
id: 1354
title: "Check out &quot;Tread Lightly&quot; by Mastodon"
header:
  image: mastodon-once-more-round-the-sun.jpg
  teaser: mastodon-once-more-round-the-sun.jpg
categories:
  - Music
tags:
  - Mastodon
  - new wave of American heavy metal
  - Once More Round the Sun
  - sludge metal
  - Tread Lightly
---
I still prefer [Baroness](http://www.matthewgraybosch.com/2014/07/discovering-baroness/) over Mastodon, but the latter's "Tread Lightly" from _Once More &#8216;Round the Sun_ is a solid bit of songwriting.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A46PDxEshU0SPeKY36Pu3FY" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
