---
id: 1530
title: 'WFC 2014: Summoning My Persona'
date: 2014-11-05T10:12:50+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1530
permalink: /2014/11/wfc-2014-summoning-persona/
bitly_link:
  - http://bit.ly/1PiYzH9
bitly_link_twitter:
  - http://bit.ly/1PiYzH9
bitly_link_facebook:
  - http://bit.ly/1PiYzH9
bitly_link_linkedIn:
  - http://bit.ly/1PiYzH9
sw_cache_timestamp:
  - 401529
snap_MYURL:
  - 
snapEdIT:
  - 1
snapBG:
  - |
    s:202:"a:1:{i:0;a:5:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";}}";
snapDI:
  - 
snapGP:
  - 
snapLI:
  - 
snapPN:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snapWP:
  - 
response_url:
  - 
response_title:
  - 
response_quote:
  - 
snap_isAutoPosted:
  - 1
snapLJ:
  - 
snapSC:
  - 
snapIP:
  - 
snapSU:
  - 
snapDL:
  - 's:292:"a:1:{i:0;a:8:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"c94a2d0f2c1ab36c4044e2c171dd2d37";s:5:"pDate";s:19:"2014-11-05 10:13:38";}}";'
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Personal
  - Public Appearances
tags:
  - anxiety
  - fear
  - jitters
  - negative self-talk
  - nerves
  - nervousness
  - persona
  - wfc 2014
  - World Fantasy Convention
---
I'm almost ready to get in the car and drive down to DC to attend the [2014 World Fantasy Convention](http://www.worldfantasy2014.org). It's my first convention, I'm nervous, and I'm currently dealing with a shitload of negative self-talk.

My old fears are yelling, telling me all of the following:

  1. I don't belong there because I don't write traditional fantasy.
  2. Nobody will give a shit about me, even though I'm on the program with a reading on Friday night.
  3. I'm just some schmuck with a day job with a book published by a small press.
  4. I'll probably fuck up the reading or otherwise make an ass of myself.

I've been tempted to say "fuck it" and not go, but that would mean that I'd have wasted the $500 I spent to get Catherine and me attending memberships. Moreover, it's too late to cancel the hotel reservation.

So, fuck it. Even if everybody else at WFC2014 doesn't want me there, I'm going anyway. I paid, just like them. It doesn't matter if I've only published one book through a small press. It doesn't matter that I'm not naturally sociable.

It most certainly doesn't matter that none of them are likely to know who the hell I am. I'll fix _that_ in due course.

I'm going. Nobody's going to stop me. Not even God himself, and I'll treat him to an old-fashioned New York asskicking if he tries. I'm gonna burn my dread, summon my sociable writer persona, and fly the flag for Starbreaker and [Curiosity Quills Press](http://www.curiosityquills.com).