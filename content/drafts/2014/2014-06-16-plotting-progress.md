---
title: Plotting Progress
categories:
  - Project News
tags:
  - battle
  - big damn cats
  - big damn heroes
  - climax
  - imaginos
  - morgan stormrider
  - outlining
  - pantsing
  - plotting
  - silent clarion
  - starbreaker
  - the blackened phoenix
---
I'm almost done plotting<span class="Apple-converted-space">&nbsp;</span>_The Blackened Phoenix_, which I might have done back in December 2013 if I wasn't a<span class="Apple-converted-space">&nbsp;</span>**demon-ridden idiot**. I thought I could get away with writing the second Starbreaker novel as I did<span class="Apple-converted-space">&nbsp;</span>_Without Bloodshed_. I thought it would be sufficient to put together a rough plan of how the plot should work out, chapter by chapter, and improvise the details.

I was wrong. As a result, between the demands of my day job, a month spent moping while my wife visited Australia, another month or so obsessed with<span class="Apple-converted-space">&nbsp;</span>_Dark Souls II_, and my own inability to figure out the details of the plot as I wrote the book, I largely wasted the last six months.

Sure, I've got about 20,000 words worth of material, and I suspect I'll salvage at least half of it. However, 10,000 words isn't much to show for six months' worth of work. It isn't even close to being enough.

The upside is that I also have a new short story for the 2014 Curiosity Quills charity anthology, and I've plotted out and written the first four chapters of a prequel novel set in Naomi Bradleigh's youth. It doesn't hit enough of the tropes to be a proper New Adult novel, but marketing is all about telling people what they want to hear so they'll buy shit. It's called<span class="Apple-converted-space">&nbsp;</span>_Silent Clarion_, and you'll be hearing more soon.

In the meantime,<span class="Apple-converted-space">&nbsp;</span>_The Blackened Phoenix_<span class="Apple-converted-space">&nbsp;</span>will likely be a longer novel than<span class="Apple-converted-space">&nbsp;</span>_Without Bloodshed_, though I hope the plot will be tighter and less confusing since I'm trying to limit the number of viewpoints I use. Whether I'll succeed is a something you'll have to decide for yourself.

Properly plotting a novel isn't easy for me. I'm used to just sitting down and belting out text. As a result, I've reworked the plot for Morgan Stormrider's climactic fight against Imaginos at the end of<span class="Apple-converted-space">&nbsp;</span>_The Blackened Phoenix_. I originally intended the fight to work like this:

  * Morgan confronts Imaginos and gets his ass kicked.
  * Morgan confronts his shadow aspect, and accepts the abilities he repressed.
  * Morgan uses his reclaimed abilities and kicks Imaginos' ass.

However, I couldn't make it work. I felt as if I was stuck with this idea because it was how I had done the fight back in 2009, and I just wasn't feeling it. My story and characters changed so much in the five years since I finished that first draft of<span class="Apple-converted-space">&nbsp;</span>_Starbreaker_<span class="Apple-converted-space">&nbsp;</span>that sticking with the original idea felt like a betrayal.

Besides, I have a big damn cast that includes a big damn cat. Why not let them be big damn heroes and show up to give Morgan an assist after he's proved how badass he is by fighting Imaginos to a standstill? Why not make Mordred useful (instead of just letting him be the big cuddly team mascot) by showing how rakshasas screw with the ensof?

I couldn't think of a good reason not to, so that's how I spent my lunch break today. Fans of shonen anime like<span class="Apple-converted-space">&nbsp;</span>_Dragon Ball Z_<span class="Apple-converted-space">&nbsp;</span>and<span class="Apple-converted-space">&nbsp;</span>_Bleach_<span class="Apple-converted-space">&nbsp;</span>will feel right at home because the fight sprawls across three chapters. Hopefully, I won't bore everybody else.

I just have two chapters left to plot in detail:

  * Chapter Twenty-Four: The Voice Commanding You
  * Chapter Twenty-Five: All Part of the Plan

Expect Imaginos to come back, if only for some angry wizard sex.
