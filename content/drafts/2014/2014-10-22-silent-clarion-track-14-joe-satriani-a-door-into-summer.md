---
title: "*Silent Clarion*, Track 14: &quot;A Door Into Summer&quot; by Joe Satriani"
excerpt: "Check out chapter 14 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. While hiking with Mike Brubaker, Naomi gets a taste of Clarion's secrets."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Without wine and unrequited lust to cloud my vision, Michael Brubaker reminded me of my brother Nathan. He was reserved without beer and nearby friends to bolster his confidence, so I resorted to leading questions to draw him out. His answers didn't give me much, but seemed to help put him at ease.

We followed the town's namesake river, the Clarion, northeast into the forest. Michael proved voluble once I began asking about unfamiliar plant life. A born woodsman, he seemed pleased to have a companion with whom he could share his knowledge. "Don't you have trees in London?"

Craning my neck, I stared up at the tops of the white pines towering overhead. "Not quite like this. This is a real forest, old and wild."

"Not that old." Michael shook his head. He crouched, and put on a pair of heavy gloves before digging into the soft earth. After a minute's effort, he pulled out a chunk of asphalt. Part of it was still yellow. "A road used to run this way. See those little hills off in the distance? That's actually what's left of a strip mall."

That sounded like arrant bullshit, so I used my implant to pull pre-Nationfall maps off the network and compare them with current GPS data as we continued our hike. He was right about the road, but I remained skeptical about the forest's ability to reclaim developed land in mere decades.

｢Got a minute, Malkuth? I wanted to ask a couple of questions.｣

｢Go ahead. Can't guarantee I'll answer, though, especially if it's about Clarion.｣

Damn clearance again. Oh, well. His evasions might still prove enlightening. What the bloody hell is the Society so paranoid about? ｢I'm curious about the forest northeast of town. I'm hiking along the Clarion River, and my companion showed me that there used to be a road running along our route. I confirmed it using pre-Nationfall maps compared with current GPS data. What happened here?｣

｢Companion? Did you meet someone already? I'm jealous.｣

Was Malkuth playing the human game by flirting, or was he serious? I decided to take him seriously. ｢Don't worry, Mal. He reminds me too much of Nathan. Besides, you'll always be my favorite AI. I haven't forgotten that I promised you a date.｣

｢I'm going to hold you to that, Nims.｣

｢Yeah, you do that, Mal. In the meantime, why not tell me what happened?｣

A pair of large files hit my implant a couple minutes later, labeled "clarion-valley-topo-2048" and "clarion-valley-topo-2049". I opened them, and compared the two topographic maps. ｢There's an *impact crater* a kilometer east of my position. Meteorite?｣

｢Worse. There was a protest there seven days, six hours, and fifty-two minutes before the Commonwealth's final collapse. Some NACAF general decided it was an insurrection and decided to use an experimental space-based weapon codenamed GUNGNIR to suppress it.｣

Gungnir was the spear of Odin, king of the Aesir. The use of such a name to signify a space-based weapon couldn't possibly be coincidental. ｢GUNGNIR was a kinetic strike system, wasn't it?｣

｢Exactly.｣ Not that Malkuth was finished. ｢It gets worse. GUNGNIR is still out there, along with two other systems codenamed GAEBOLG and LONGINUS. The Society has them under control, but can you imagine what might happen if nation-states arose again and started creating more of these systems?｣

Staring skyward, I visualized shafts of tungsten raining down as a hail of javelins. The spear was one of humanity's first weapons. Was it to be our last? Despite the warmth of late summer, I shuddered. ｢Has the Society ever used these weapons?｣

｢I'm sorry, Naomi, but you aren't cleared for that information.｣ Not that I expected an answer, but a simple 'no' would have reassured me.

Uphold individual rights. Root out corruption. Overthrow tyranny. Protect the human race. Impose transparency and accountability on authority. That's the mission, but who the hell is going to impose transparency and accountability on the Phoenix Society, when we don't even know who sits on the Executive Council?

"Hey, are you all right?" Brubaker's voice up ahead dragged me out of my own head.

Not wanting to shout, I dashed upriver to catch up with him. "Sorry. Had an argument with a friend from work."

"A fellow Adversary?"

Venting my frustration on the kid would be counterproductive now that he was finally opening up. "Further up. Know anything about our AIs, the Sephiroth?"

Brubaker shook his head. He held a finger to his lips before pointing toward the river. My reward for following his direction was a view of river otters at play. They splashed about, chasing each other and catching fish without the slightest care in the world. A black bear lumbered out of the underbrush on the opposite bank and waded in, eager for her share of the trout flashing in the sun-dappled water.

We left the animals to their business and continued upriver for a bit before Brubaker spoke again. "How do you argue with an AI?"

"Very carefully." It sounded like a punchline, and it got a smile out of Brubaker, but it's also the truth. "Interactive AIs are better at logic than we are, so you must weigh your words if you want to persuade them to do anything."

We walked a dozen meters before his next question. "What were you trying to get the AI to do?"

"I had questions about what had happened to this place."

"Dr. Petersen told us a meteorite fell nearby and flattened the area. Was he wrong?"

Either that, or lying. "It wasn't a meteorite. The Commonwealth bombarded the area from orbit to suppress some kind of protest."

"But that's insane! Why would a government do that? What were they trying to protect?"

Is he really that naive? "Either their own power, or something nearby that they didn't want falling into the people's hands."

"Such as?"

Damned if I know, so I shrugged. "Maybe the Commonwealth had some kind of military installation nearby."

"It would explain why we call 'em the Old Fort Woods." Brubaker drank from his canteen.

I opened the package of bison jerky I bought in town earlier and offered him a piece. He put the entire piece in his mouth and began chewing, stuffing it into one cheek like a deranged carnivorous squirrel. "Damn, that's good. Did you get this from Three Wolves?"

Rather than talk with my mouth full of jerky, I nodded. The salty spiced meat assaulted me with flavor, so that I was lost in the taste as I chewed. It was tough at first, but quickly became as tender as a good rare steak as I worked moisture back into the meat.

We followed the trail upriver for a couple kilometers in companionable silence before he spoke again. "What's it like being an Adversary?"

I stopped short, unsure of how to answer. Becoming an Adversary allowed me to attend the Juilliard Conservatory in New York without selling myself into more onerous forms of indentured servitude. It also let me uphold worthy ideals and make great friends.

But being stonewalled by Malkuth at every turn when I need him most? Nope. That's hardly something to brag about.

He turned to me, and looked me straight in the eye. "I want to know what I'm getting into before I join."

Stop me if you've heard this story before. A small-town kid dreams of seeing the wider world. He meets a more experienced person he can look up to, somebody who's been around, and starts romanticizing the hell out of her life. "We watch the watchmen. But nobody's watching us but each other. Make a single mistake on duty, and you could end up dead."

Brubaker gave a thoughtful nod. "That's why you carry a sword, right? To protect yourself."

Not to mention a Kalashnikov, but weapons are of no use at a court martial. What are you going to do, run the judge through? Gun down the jury? That sounds like an excellent way to refute a charge. "It helps, but I wasn't talking about being killed by a suspect. Adversaries found guilty of violating individual rights are executed."

"But the death penalty was abolished!" Brubaker lapsed into silence for a bit afterward, and we walked for a while. "Why is capital punishment reserved for Adversaries?"

"We have near-absolute authority while on duty. That authority carries an equal weight of responsibility. It's too heavy a burden for most people."

"You seem to handle it well."

Most of us do, until we can't any longer. It's called burnout. I've seen good Adversaries hand over their pins because they couldn't do it any longer, despite the cognitive-behavioral therapy we get between missions to keep the stress under control. "I've been lucky so far."

I fingered the cameo-style pins in my lapels that identified me as an Adversary. Two rattlesnakes coiled around a sword of justice while holding a set of scales in their jaws represented the principles of our service: liberty, justice, and equality for all.

Why did I care about Michael's decision? Was it because he reminded me of Nathan? "I won't say it's not a privilege to serve, and I'm sure you could do a lot of good, but don't buy into the romance. It's just a different kind of police work."

---

### This Week's Theme Song

"A Door Into Summer" by Joe Satriani

{% youtube 5M4BGpdU3pY %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
