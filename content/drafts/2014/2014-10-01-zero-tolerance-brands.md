---
title: "Zero Tolerance For Brands"
excerpt: "I stopped watching TV because of ads. I'll abandon the web for the same reason if necessary."
header:
  image: banksy-advertisers.jpg
  teaser: banksy-advertisers.jpg
categories:
  - Social Media
tags:
  - advertising
  - attention economy
  - brands
  - ello
  - fuck the attention economy
  - no mercy
  - spam
  - zero tolerance
---
Evo Terra ([@evoterra](http://ello.co/evoterra)) asked me the following about brand messaging earlier.

> I'm curious to know the details how On Air Player was spamming you via Ello. They're getting a lot of pushback, so something is clearly happening. Some questions come to mind, if you don't mind answering.

Hopefully he won't mind if I answer here, where I can more conveniently quote his questions.

> Are they spamming you via comments?

Not yet. However, I can imagine it happening soon, especially if @ello doesn't implement blocking soon. 🙂

> Does the act of "being followed" by a brand that you care nothing for constitute as spam on Ello? (Your opinion needed here)

I don't know how prevalent this behavior was on Facebook because I never got as much engagement as I did on Google+, but on Google+ I couldn't go a day without being circled by business pages with whom I had never engaged once Google started permitting such behavior.

I don't care for it. If I have not engaged with a brand, I do not want to hear from that brand for any reason. If somebody from a brand likes something I post, they should comment as human beings instead of hiding behind their brand accounts.

Furthermore, I have no tolerance whatsoever for accounts who follow thousands of people, have no more than one or two posts, and somehow have double-digit follower counts. I've seen too many such accounts on Google+, and they all turned out to be spambots.

I get that Ello is new and people are excited, but what's the point of indiscriminately following thousands of people when scientists like [Robin Dunbar](http://en.wikipedia.org/wiki/Dunbar's_number) have shown that human beings can't maintain more than 100-250 stable personal relationships?

> Does their insistence on pushing a brand message go contrary to your interpretation of Ello's values, so any brand messaging = spam?

I have no tolerance for advertising or for brands with whom I do not freely choose to engage. So, IMO, all _unsolicited_ brand messaging is spam, and I do not consider membership on a social network to be consent for brand messaging. If I want to hear from a brand, I'll google their website and sign up for their mailing list.

My antipathy toward brands and advertising has nothing to do with Ello's stated values. It is born of a dislike for being manipulated so that strangers can enrich themselves at my expense.

Moreover, I remain doubtful about @ello's commitment to treating users as people instead of products in light of [Aran Balkan's](https://aralbalkan.com/notes/ello-goodbye/) revelation that Ello is currently taking venture capital from [FreshTracks Capital](http://www.sec.gov/Archives/edgar/data/1598000/000159800014000001/xslFormDX01/primary_doc.xml).
