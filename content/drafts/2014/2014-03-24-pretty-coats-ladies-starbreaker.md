---
title: "Pretty Coats for the Ladies of Starbreaker"
excerpt: "Something my wife found."
date: 2014-03-24T22:57:41+00:00
categories:
  - Backstage
tags:
  - coats
  - pretty
  - Starbreaker
  - women
  - fashion
---
My wife Catherine tends to find clothes that would look good on my [Starbreaker](http://www.matthewgraybosch.com/starbreaker/ "Starbreaker") characters, mostly for the ladies. Today she found some nice coats on <a title="Tech Sergeant Jenn" href="http://techsgtjenn.tumblr.com/post/80612312311/thepropheticbird-winter-coats-by-xiaolizi" target="_blank">Tech Sergeant Jenn's tumblr</a>.

![](http://66.media.tumblr.com/c550617379e6563e220cd59de649260d/tumblr_mtyx2yX3kj1qcfto6o1_1280.jpg)

![](http://67.media.tumblr.com/e5fb95719798d3aae376a600159091f5/tumblr_mtyx2yX3kj1qcfto6o2_250.jpg)

![](http://65.media.tumblr.com/c82021e9ad2d148654fb75d94cd7c6a5/tumblr_mtyx2yX3kj1qcfto6o3_1280.jpg)

![](http://65.media.tumblr.com/309cf862c75a34cbd429bb6d32a8d268/tumblr_mtyx2yX3kj1qcfto6o4_1280.jpg)

![](http://66.media.tumblr.com/164bd5761dadc1115c2a906c08f28605/tumblr_mtyx2yX3kj1qcfto6o5_1280.jpg)

![](http://66.media.tumblr.com/f3961ecdceae6c14123fdf5b06a05bc6/tumblr_mtyx2yX3kj1qcfto6o6_1280.jpg)
