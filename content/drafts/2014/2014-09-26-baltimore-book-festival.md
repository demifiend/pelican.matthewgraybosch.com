---

title: 'Baltimore Book Festival'
cover: without-bloodshed-final-cover.jpg
coverwidth: 1800
coverheight: 2706
covercredit: Ricky Gunawan
covercrediturl: http://ricky-gunawan.daportfolio.com/
---

I'll be at [Baltimore Book Festival](https://www.baltimorebookfestival.com/) tomorrow and Sunday if anybody wants to pick up a signed copy of *Without Bloodshed*. 

I'm charging $13.00 per copy, which includes Maryland sales tax. Cash and major credit cards accepted.