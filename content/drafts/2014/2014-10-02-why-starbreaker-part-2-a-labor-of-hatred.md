---
id: 1426
title: 'Why Starbreaker? Part 2: A Labor of Hatred'
excerpt: This is probably the closest I'll come to writing a manifesto until I move to Montana.
date: 2014-10-02T08:00:59+00:00
header:
  image: without-bloodshed-final-cover.jpg
  teaser: without-bloodshed-final-cover.jpg
categories:
  - Personal
tags:
  - autobiography
  - bullying
  - depression
  - existential crisis
  - heavy-metal
  - labor of hatred
  - labor of love
  - motivation
  - poverty
  - rage
  - self-hate
  - suicide
  - Why Starbreaker
  - defiance
  - anger
  - rebellion
---
If _Without Bloodshed_ and the rest of Starbreaker is going to be [a hard sell](http://www.matthewgraybosch.com/2014/09/why-starbreaker-part-1-the-hard-sell/), why do it? Why not write a straight space opera, or a straight thriller, or a straight fantasy in the traditions of J. R. R. Tolkien or George R. R. Martin? Hell, why not adopt a feminine pseudonym and write romance, or even write romance from a male perspective as myself?

Why Starbreaker? **Because fuck you is why.** Yes, I know that statement might offend some readers. I know that letting my true feelings show isn't necessarily good for my "author platform" or my "personal brand". _I don't give a damn._ This is my soapbox, and I'm going to tell the truth as _I_ understand it.

You know how some authors describe their work as a labor of love? Well, Starbreaker is a labor of hatred. It's an act of rebellion. It's me, raising both my middle fingers skyward, and screaming defiance at the whole of Western civilization. It's me, biting the hands that fed me rotten meat and expected me to be thankful for it.

Starbreaker is _my_ story, my obsession, and my reason to go on living when I had nothing and nobody else to sustain me. It's also my tribute to heavy metal, the music that saved my fucking life.

No, I'm not joking. I had an idyllic childhood until I started kindergarten. I got on the bus, and bullying started. It continued throughout grade school. My parents were working-class renters living in a town full of upper-middle-class homeowners. They did their best, and worked their asses off so that my brother and I wouldn't feel like the poorest kids in school, but we _were_ poor compared to other families &#8212; and the other kids made sure I knew it.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A5D2eCwqbHcqOnfHOCM6TnV" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

I didn't wear the right clothes. I didn't wear the right shoes. I didn't have the right backpack. I didn't have the right toys. I didn't watch the right TV shows. I read too much. I was a "faggot" (and later on, a "long-haired faggot"). All of that was considered just cause for singling me out, hurling insults at me, and using me as a punching bag.

I didn't handle it well. I got angry. I screamed. I fought. I lashed out at school authorities, and got kicked out of a school because I slapped the principal's face. I got labelled a "discipline problem" and "emotionally handicapped" despite allegedly having a genius-level IQ, as if testing at 156 at the age of 8 meant anything. I got shunted into special education, which gave kids another excuse to abuse me.

My parents did the best they could, but they couldn't help much. Most of their advice consisted of keeping a "low profile" and trying harder to fit in. This advice might have been worth a damn if I could have moved to another town and gone to another school to make a fresh start, but that would have meant forcing my younger brother to make a fresh start as well, and give up all his friends.

What did the school authorities do? Absolutely nothing. I don't remember much of my childhood, but I remember one incident reasonably well. I get a reminder every time I shave, courtesy of the scar on my forehead.

When I was thirteen, a kid named Reland Boyle decided to throw me head-first into a trophy case. He left me there, half-blind from rage and the blood streaming down my face from the resulting cut on my forehead. The teachers and staff let me stumble to the nurse's office without lifting a finger to help.

The school nurse called my parents, who took me to the emergency room for stitches. I may also have suffered a concussion. This is when I learned that the people in charge don't give a shit about individuals. If you ever wanted to know why I have issues with authority, this is as good a reason as any.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A1bPUK3zBMK73QXmCLzqffn" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

After my injury, I started thinking about killing myself. It wasn't that I _wanted_ to die. I was just tired of life as I understood it. I was tired of going to a school that I had come to see as a battleground. I was already the veteran of a thousand psychic wars by then, and I was _tired_. I had no weapons, no armor, and no support.

It was during this period that my parents got cable TV. I wasn't allowed to watch much, because I was supposed to be studying first for the local geography bee sponsored by National Geographic, then the state geography bee, and finally the National Geography Bee in 1992.

If anybody cares, I made it to the finals &#8212; and threw the contest. I decided that being the winner of the 1992 National Geography Bee, and the bullying I was likely to get as a result, wasn't worth the $25,000 college scholarship that National Geographic offered as first prize.

I happened upon MTV while taking a break from my studies, and they were playing heavy metal videos. (Yes, they used to do that back in the day.) In particular, they were playing the video for Metallica's "The Unforgiven". I was used to paying attention to lyrics because my father's a huge fan of 1970s progressive rock, and "The Unforgiven" seemed to describe my life so far.

The anger and defiance in James Hetfield's voice charged me, and told me, "Yeah, the world sucks. But hold onto your anger and hatred. Face the world with unyielding defiance and you'll at least be able to live and die on your own terms."

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A5SnOyuBtyzufoXBAKOdcxD" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

I drew strength from that Metallica song, and started getting into other heavy metal bands. When Rob Halford of Judas Priest sang, "Keep the world / with all it's sins. / It's not fit / for living in!" in "Beyond the Realms of Death", that _resonated_ with me, but so did anthems like "Blood Red Skies" and "One Shot At Glory". My political consciousness began with Queensryche's epic concept album _Operation: Mindcrime_.

People used to worry about kids killing themselves because of heavy metal. They used to think metal was just noise, and that it had no redeeming value. Some of them offered "Christian rock" as an alternative, not realizing that Black Sabbath invented the genre with songs like "War Pigs", "After Forever", and "Lord of This World". They don't have any idea how many kids listened to heavy metal and found the strength to live in a world that isn't worth saving.

**I am one of them.** I found in metal the strength to stand and fight. Did I find the often-Satanic lyrics appealing? Hell yeah. Satan became for me a Promethean figure, a metaphor for unyielding defiance against overwhelming opposition. However, I am not a Satanist. Anton LaVey's brand of Satanism is just a mashup of Ayn Rand's bullshit and the sort of shock-rock theatrics that made Alice Cooper famous.

I fought my way through high school, and things got a little better for me after I put my foot down and told the principal that I would start killing people for bullying me. It also helped that my parents finally threatened to sue the Sayville Public Schools for negligence by allowing me to be bullied.

Would I have carried out my murderous threats? Would I have done a Columbine _before_ Columbine, thus giving the media a bit of fun by speculating on how _Final Fantasy_ and Iron Maiden drove a nice young white boy to mass murder? No. The moral foundation my parents cared enough to give me, coupled with them being too sensible to keep firearms in the house, are the _only_ reasons we talk about Columbine instead of Sayville.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A4suutZllgY8VfXjgmETNEV" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

But when I saw Columbine happen on TV, when I saw Eric Klebold and Dylan Harris on their pathetic rampage, I recognized myself. I could have been them, under the wrong circumstances. Maybe I would have been more disciplined, more _just_, by striking down only those who had wronged me. However, hat wouldn't have made me better than the angry, broken boys who finally _did_ resort to violence.

The hatred is still there, almost twenty years later. But once I got out of high school, it wasn't enough any longer. Hatred can keep you fighting, if you have enemies to fight. But if you don't have anybody to fight, you end up turning your hatred on yourself.

Rather than do that, I tried to get over it. I went to college like a good little middle-class American. I became infatuated with a girl I met online, and had a fling with her. I didn't handle the breakup well.

(If you're reading this, Naomi Long from Somerset, I'm sorry about everything. I hope you're happy, wherever you are.)

I did all kinds of stupid, irrational shit in college, and I did a lot of thinking. Probably too much, but there wasn't much else to do while riding a train to classes or riding my bike to my part-time job at a supermarket.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A387JMg7bz2JPHhLqXC1RBy" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

I thought about the life I was living, and the life I was likely to live once I finished school and started working for a living as a programmer &#8212; a choice I made because it seemed like a good idea at the time. After all, I graduated just before the first dotcom boom, and software development looked like a solid way to earn a living.

However, just earning a living wasn't enough. I thought at the time that all I could look forward to was a life of days spent working for the Man and being a good little American consumer. Despite my defiance I was still half-convinced that I was nothing but a loser who didn't deserve friends, didn't deserve love, and didn't deserve anything but what I could buy to keep myself comfortably numb.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A5HNCy40Ni5BZJFw1TKzRsC" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

Other kids in my position might have found a sense of purpose and camaraderie in the military, but I knew that was a bad idea. Giving an angry kid with authority issues and a chip on his shoulder the size of Mt. Everest access to military training and ordnance is about as smart as starting a land war in Asia. If I had been religious I might have become a priest, but that's not an option for an atheist like me.

Lonely, cynical, and lacking in purpose is no way to go through life, and at eighteen I didn't have a reason to go on living. If adult life was to be nothing but a few lonely decades spent working for a living and using books, video games, and music to escape my shitty reality, then why not throw myself in front of a train and opt out? Sure, it would hurt my family, but I'd be too dead to give a shit.

Instead of going through with it, I remembered the crappy stories I wrote in high school. A few people had read them, laughed at me, and wondered why I was wasting my time. I even flunked my high school creative writing class for writing "derivative genre schlock". (No, you can't read them, and fuck you for asking.)

The old hatred and the old defiance flared to life in me that night, and I decided that since I was going to waste my time until time wasted me _anyway_, I might as well waste my time writing. _Without Bloodshed_ and the rest of Starbreaker are the result of that decision.
