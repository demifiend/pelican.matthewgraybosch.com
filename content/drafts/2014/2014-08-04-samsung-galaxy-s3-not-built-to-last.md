---
title: "Samsung's Galaxy S3 is Not Built to Last"
categories:
  - Rants
tags:
  - danko jones
  - dead phone
  - g3
  - galaxy
  - imaginos
  - lg
  - luc besson
  - lucy
  - replacement
  - s3
  - s7
  - samsung
  - t-mobile
  - villain song
---
If a car died on me right after I finished paying off, I'd wish ass cancer on its manufacturers. I'll do the same to Samsung and their shitty Galaxy phones in the wake of their releasing the Galaxy S7. The S3 was a piece of shit. I doubt the S7 is any better.

<!--more-->

I used to have a Samsung Galaxy S3. It died on me this weekend. It had a full charge when I went to take my wife Catherine to see _Lucy_ this past Saturday, but when I took it out to switch to airplane mode for the movie, I found it shut down and unable to power on. Putting it back the charger all afternoon and night after we got home didn't help.

I just finished paying the damned thing off, too. So, if anybody at Samsung's Galaxy team is reading this: **fuck you**. Fuck you for creating a phone that dies right after I finish paying it off. While we're at it, fuck you for loading a perfectly good smartphone OS down with a shitty interface (TouchWiz) and lots of shitty proprietary apps that I am not allowed to remove even though it's _my demon-ridden phone_. Fuck you very much, and I hope your SO cheats on you and gives you crabs.

The above was my reaction after realizing my phone was dead, for about five minutes. Then, like a reasonably mature American adult, I decided it was time to take a drive down to T-Mobile and see about a replacement.

Since I was out of warranty and didn't have insurance on the device, it was time to get a new device. Since I wasn't about to get _another_ Samsung, I decided to see what else they had in stock. I settled on a LG G3 handset.

It's a nice phone. The UI's comfortable, though every Android phone vendor should just say "fuck it" and pre-install SwiftKey. All of the Google tools I favor are already installed, which is nice, and the phone also includes a built-in activity tracker. It's kinda handy, but not smart enough to automatically figure out when I switch between running and walking.

The camera's pretty sweet, too. I tried it out on my wife (and her cleavage). No, I'm not sharing.

The LG G3 also includes a micro SD card slot, which is handy for expanding the device's storage. I did notice that one of my phones nuked my card's contents, which left me without any music to play while driving with my wife.

So, I installed the Amazon music app, took advantage of my prime membership, and discovered Danko Jones. Good thing I did, too. It's fun, nasty rock &#8216;n roll.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Now I have another villain song for Imaginos in _Starbreaker_. He thinks bad thoughts, too.

As for _Lucy_: If you liked what director Luc Besson did with _The Fifth Element_, you might enjoy his French existentialist mindfuckery in _Lucy_. Or, you can embrace your inner teenage dudebro and spend 89 minutes ogling Scarlett Johansson while enjoying stylish ultraviolence.
