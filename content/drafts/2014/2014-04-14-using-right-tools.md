---
id: 552
title: Are You Using the Right Tools?
date: 2014-04-14T12:04:37+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=552
permalink: /2014/04/using-right-tools/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1N0CSJr
bitly_link_twitter:
  - http://bit.ly/1N0CSJr
bitly_link_facebook:
  - http://bit.ly/1N0CSJr
bitly_link_linkedIn:
  - http://bit.ly/1N0CSJr
sw_cache_timestamp:
  - 401656
bitly_link_tumblr:
  - http://bit.ly/1N0CSJr
bitly_link_reddit:
  - http://bit.ly/1N0CSJr
bitly_link_stumbleupon:
  - http://bit.ly/1N0CSJr
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - Adobe
  - Brackets
  - docx
  - Don Knuth
  - editor
  - free
  - George R. R. Martin
  - LaTeX
  - Linux
  - Microsoft
  - proprietary
  - TeX
  - tools
  - Ubuntu
  - vendor lock-in
  - Word
  - WordStar
  - writing
---
Are you a writer who thinks about the tools of your trade? Do you compose your drafts using Microsoft Word? If so, why do you do it? Do you use Word because you got a copy with your computer? Do you use Word because you think everybody else does it? Do you use Word because it's the "standard"? Do you actually _like_ using Word, or do you find wrestling with it a distraction from writing?

## Word is a Lousy Composition Tool.

I ask because I don't use Word when composing fiction. If I really wanted to, it's possible to run <a title="How to Install Office on Linux" href="http://www.howtogeek.com/171565/how-to-install-microsoft-office-on-linux/" target="_blank">Word on Linux</a>. While I'm masochistic enough to enjoy <a title="Getting the Hang of Dark Souls II" href="http://www.matthewgraybosch.com/2014/03/15/getting-hang-dark-souls-ii/" target="_blank"><em>Dark Souls</em></a>, my appetite for pain has its limits. I could also use <a title="LibreOffice Official Site" href="https://www.libreoffice.org/" target="_blank">LibreOffice</a> to draft, but <a title="Bartleby the Scrivener" href="http://www.gutenberg.org/cache/epub/11231/pg11231.html" target="_blank">I would prefer not to</a>.

I don't like word processors for composition for the following reasons:

  * Word processors are overkill. When I'm writing, I need to denote chapter headings and scene breaks, and I need to italicize interior dialogue and titles of works mentioned in my text. That's all.
  * Word processors are too complex. They have too many menus, switches, and options, most of which have _nothing whatsoever_ to do with composition.
  * Font, line spacing, margins, etc. are irrelevant concerns when drafting a text. I don't want to have to worry about any of that.
  * If I create a text using Word, I'm stuck using Word. While other apps claim compatibility, such compatibility remains at Microsoft's mercy because Word's formats are proprietary and subject to change.
  * Word processor files are binary data, or compressed XML, instead of plain text. Not only does this make them unnecessarily large, but it precludes running their contents through Unix command line utilities or scripts, which might come in handy.
  * The comments and "track changes" functionality so beloved of editors clutters the text with invisible control characters.
  * Copying and pasting text from a word processor usually copies and pastes the word processor's formatting as well, especially when composing blog posts on WordPress. While WordPress has a "paste as plain text" option, using it is a pain in the ass.
  * Word expects you to put your entire text into a single file. This isn't my preferred workflow.
  * I don't give a damn how my text looks on a printed page. I'm not going to print it fourteen years into the twenty-first century.

So, what do I use instead? Unlike <a title="George R. R. Martin: Gleep!" href="http://grrm.livejournal.com/9841.html" target="_blank">George R. R. Martin</a>, I never learned and <a title="A Song of DOS and WordStar" href="http://nevalalee.wordpress.com/2013/04/16/a-song-of-dos-and-wordstar/" target="_blank">fell in love with WordStar</a>. I learned <a title="WordPerfect 5.1 for DOS Updates" href="http://www.columbia.edu/~em36/wpdos/" target="_blank">WordPerfect 5.1</a> in high school, but didn't have a computer of my own until I graduated, and while it ran <a title="PC-DOS 6.1" href="http://en.wikipedia.org/wiki/PC_DOS#PC_DOS_6.1" target="_blank">PC-DOS 6.1</a> and Windows 3.11, it didn't come with WP 5.1. It didn't come with any tools not included with DOS or Windows. So I used the E editor to write.

## "DOS? That's _So_ Twentieth Century!"

So are Unix and the X Window System, both of which I prefer over DOS, Windows, and post-Leopard versions of OS X. I still prefer to use a <a title="Plain Text for Authors" href="http://www.richarddooling.com/index.php/2012/12/20/plain-text-for-authors-writers/" target="_blank">plain old text editor</a>, like the one shown below.<figure id="attachment_554" style="width: 1366px" class="wp-caption aligncenter">

[<img class=" wp-image-554 " title="Brackets editor on Ubuntu Linux 14.04 (beta)" alt="Brackets editor on Ubuntu Linux 14.04 (beta)" src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/brackets-sprint37-linux-20140412.png?resize=840%2C456" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/brackets-sprint37-linux-20140412.png)<figcaption class="wp-caption-text">Brackets editor on Ubuntu Linux 14.04 (beta)</figcaption></figure> 

I currently favor an editor called <a title="Brackets" href="http://brackets.io/" target="_blank">Brackets</a>, and I use the file system on my computer to organize my work. My personal directory is called _/home/demifiend_. I have a "documents" directory there which contains a "starbreaker" directory. Every **Starbreaker** project I do gets its own directory under _/home/demifiend/documents/starbreaker_.

The screenshot above shows me working in _/home/demifiend/documents/starbreaker/silentclarion_. Brackets lets me open this directory in the sidebar as my project folder, and interact with files there within the editor.

The structure should be self-explanatory. All of the .md files you see are actually plain Unicode text files with a file extension that tells editors they contain Markdown formatting and should use the appropriate syntax highlighting if available. I prefix filenames with a number to force the OS to order my files properly. Each chapter gets its own directory, and each scene in a chapter its own file. Each scene is ordered by number and named for its viewpoint character.

## "How Do You Move/Re-order Scenes?"

On Linux, I can accomplish these tasks with shell commands. For example, let's say I have a chapter with two scenes from Naomi's viewpoint named 1.naomibradleigh.md and 2.naomibradleigh.md. I want to create a new second scene between the existing scenes. I can do this with the following commands.

<pre style="padding-left: 30px;">$ mv 2.naomibradleigh.md 3.naomibradleigh.md
$ touch 2.naomibradleigh.md</pre>

The first command, "mv", means "move". I'm telling the OS to move the specified file to a new location. The second command, "touch", exists primarily to change file access and modification times, a function more useful to sysadmins than writers. However, using the "touch" command on a filename that doesn't exist will create an empty text file with that name. This is handy because as the file will immediately show up in Brackets, allowing me to start writing in it when I'm ready.

These commands won't return any output unless something goes wrong, because part of the Unix way is "no news is good news". However, I can verify that my commands worked using the "ls" command.

## "Is This What You Send Your Publisher?"

Hell no. I'm insane, not stupid. This text-editor workflow, based on using the Unix file system and utilities with Unicode text files and Markdown formatting, is strictly for my own convenience. When I'm ready to submit a draft to my publisher, the first thing I do is compile my draft into a single file. I can do _that_ with a shell command as well.

<pre style="padding-left: 30px;">cat 0.title.md 1.dedication.md chapter*/* &gt; draft.md</pre>

The "cat" command will concatenate the files I specify, putting their contents together and dumping them on my screen. That isn't quite what I want, so I redirect the output to the "draft.md" file using the > character, which will also overwrite the previous contents of draft.md. I can then open my draft in a word processor like LibreOffice Writer and format the text as specified by my publisher's submission guidelines.

## "You Admitted to Using a Word Processor."

Yes, I did. However, I don't use one when drafting a novel, or doing revisions. I only use one when I have to share my work with others. My editors at Curiosity Quills Press aren't programmers, and are used to using Word and similar apps. Because they're trying to help me improve my work and prepare it for publication, I accommodate their workflow.

## "Why Not Use Scrivener?"

I'll give the folks at Literature and Latte their due: <a title="Scrivener for Writers" href="https://www.literatureandlatte.com/scrivener.php" target="_blank">Scrivener</a> is an excellent app if you have a Mac. I don't any longer, and haven't used one since 2012. My current workflow is inspired by Scrivener, as a matter of fact.

However, Scrivener's text composition tools suffer from the same flaw as a traditional word processor: it makes presentation part of the content. I don't want to dick around with fonts, spacing, indentation, etc.

&nbsp;

Incidentally did you know that a Scrivener project is a folder whose name ends in ".scriv"? It contains other folders and individual Rich Text Format files. Try cracking one open, and see for yourself.

## "This is Too Complicated For Me."

If you're used to a "one file per work" process, I can see why you'd feel uncomfortable with splitting your work into multiple files and using directories to keep everything tidy. However, I'm also a programmer. I deal with incredibly complex software projects at my day job, and using a single file for each component with folders to impose structure is the best way we programmers have found to manage the complexity in our work.

It still isn't perfect, but if we put everything in a single file, not only would that file be monstrous, but it would be a colossal pain in the ass to track changes. Have you ever tried navigating a file containing several thousand lines of code? I don't recommend it. Just imagine navigating a file containing several _million_ lines of code, and you'll have a notion of why programmers like to break work down to atomic pieces.

## "But We're Not All Programmers."

Yes, I've noticed. I'm one of the _crazy_ writers. Of course, I'm not sure if being crazy is a prerequisite for working in the software development trade, or an occupational hazard.

You don't have to be a programmer to consider putting aside your word processor in favor of a good text editor, at least while writing and revising drafts you're not ready to send to an editor or publisher. I can give you several good reasons for doing so:

  * Plain text files are small. Really small. With Unicode, you're looking at no more than 4 bytes per character. My current outline for _Silent Clarion_ weighs in at just under 6,000 bytes. It would be several times bigger as a Word document.
  * I can read my files on any computer, in any application. My writing is as close to future-proof as humanly possible.
  * I can display the text any way I like without affecting it.
  * Remember the "Reveal Codes" option in WordPerfect? Writing with Markdown is like having "Reveal Codes" on all the time. I have full control over my text.
  * I've never had a text editor mangle my work. I don't think anybody would say the same about Microsoft Word.
  * I can open multiple files in tabs, and cycle between them using CTRL+Tab.

Now, I'm not suggesting that you immediately switch to using plain text. Give it a shot in your next project, instead. If you don't want to use the Brackets editor, I can recommend the following editors instead.

  * <a title="Notepad++" href="http://notepad-plus-plus.org/" target="_blank">Notepad++</a> (Windows) &#8211; A rock solid editor for Windows. I often use this at my day job.
  * <a title="Sublime Text" href="http://www.sublimetext.com/" target="_blank">Sublime Text</a> (Windows, Mac, Linux) &#8211; Another solid editor with some excellent extensions for Markdown. It's nagware. You can use it for free, but it will occasionally nag you to buy a license for $70.
  * <a title="TextWrangler" href="http://www.barebones.com/products/textwrangler/" target="_blank">TextWrangler</a> (Mac) &#8211; A stripped-down version of BBEdit, but free to download and still quite capable.

If you've using Google Docs because you prefer to work in the cloud, you might want to consider using <a title="StackEdit" href="https://stackedit.io/" target="_blank">StackEdit</a>. It integrates with Google Drive and has a built-in Markdown preview mode. You might also consider using <a title="Draft" href="https://draftin.com/" target="_blank">Draft</a>, which works really well for short pieces. It doesn't really suit my files-and-folders workflow for writing novels, though.

Finally, if you give Markdown a shot and decide it isn't suitable because you do academic, technical, or scientific writing, then you really should consider learning to use <a title="LaTeX Project" href="http://www.latex-project.org/" target="_blank">LaTeX</a>, which is based on computer scientist Don Knuth's TeX typesetting system.

Finally, I'm curious about what non-standard workflows you've adopted, if you also strayed from the "Use Word for everything" paradigm. Tell me about it in the comments section below.