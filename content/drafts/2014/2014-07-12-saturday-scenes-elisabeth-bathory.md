---
id: 970
title: 'Saturday Scenes &#8211; Elisabeth Bathory'
excerpt: She's not bad. She's just written that way. This may be NSFW.
date: 2014-07-12T08:40:04+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - Adramelech
  - Ashtoreth
  - Disciples of the Watch
  - Elisabeth Bathory
  - ensof
  - excerpt
  - NSFW
  - Saturday Scenes
  - without bloodshed
---
I've been busy with the day job and _Silent Clarion_, so here's an excerpt from Chapter Seven of [_Without Bloodshed_](http://www.matthewgraybosch.com/without-bloodshed/ "Without Bloodshed") for this week's **Saturday Scenes**. Elisabeth Bathory is also the ensof Ashtoreth, one of the Disciples of the Watch. She doesn't trust her fellow disciple, Adramelech, but is he really the traitor she thinks him to be? You can see for yourself, though this scene may be borderline NSFW.

---

The warmth of the bath and the fragrance of jasmine conspired to lull Ashtoreth into drowsiness. She floated in the tub, letting its currents caress her. She would rise from the waters soon, and relax while the attendants dried her body and massaged oils into her skin before joining her in bed. For now, she was content to drift.

A cool draft signaled the opening of the bathroom door. One of her attendants, a dusky-skinned young woman who came to the Garden of Earthly Delights to study massage, set an uncertain foot inside. "Countess Bathory, I'm sorry to interrupt, but security notified me of an intruder on the grounds."

"How did he get inside the walls?" Ashtoreth held the castle for centuries, despite the efforts of various Hungarian noblemen and the Roman Catholic Church. Military force failed, so they tried marriage. Marriage likewise failed, so they resorted to blood libel. She remained, with Csejte as her seat.

"He just appeared inside, Countess. He's in the chapel now."

"The chapel?" The scented water lapped at her hips as she found her feet. The intruder must be Adramelech. Who else would dare materialize here uninvited, and in that old chapel? "Hand me a towel, Marjane, and tell my attendants I will not need them."

Once Marjane left, she dressed in a black suit with a pencil skirt, heels and stockings. She opened her lingerie drawer to retrieve an object she kept hidden in the drawer's false bottom. A brown and cream tabby cat curled up in a nest of her panties blinked at her. She spoke softly, and the cat purred as she stroked his fur before lifting him out of the drawer and putting him aside. "Must you be so incorrigible, Smudge?"

The cat wound around her calves, brushing the backs of her knees with his bushy tail as she moved aside undergarments she wore only to entice her lovers. She opened the false bottom and retrieved what appeared to be a glass rod. The last of her lovers to stumble upon it mistook the object for a piece of erotic art, which suited Ashtoreth.

She found it simpler to let somebody think she kept a glass toy in a drawer with a false bottom, than to explain that she kept in reserve a dev'astra, a weapon favored by flowseekers for personal defense. She pressed her thumb against an indentation which served as the weapon's power switch. Colors swirled inside it at her touch, and a magnetically-contained plasma blade radiated from the end she held away from her body. Deactivating the weapon, she blinked away the amber after-image the blade burned into her vision, and pocketed it before leaving her suite.

The intruder remained inside as Ashtoreth approached the chapel, a Gothic affair she reconsecrated as a temple to the senses after reclaiming Csejte during the Enlightenment. Ivy twined its way up the flying buttresses, giving the chapel the appearance of having bloomed from the earth centuries ago. The stained glass windows depicting idealized human figures in erotic poses, instead of austere saints, beckoned to her.

She entered with care, keeping to shadows to avoid revealing herself. She ripped out most of the stone floor a century ago, allowing grass, trees, and flowers to grow from the bare earth beneath. Enough remained to form little foot paths, as well as an aisle leading to an altar used for intimate rites too ancient to be called pagan. Behind the altar stood a pair of heroically proportioned male and female figures, embracing as they shared an apple, sculpted in marble. They loomed over a figure in black kneeling before the altar, and as he rose and turned around she saw a Roman collar at his throat. She recalled her words to Imaginos concerning Adramelech, once a Disciple of the Watch like her: "He is reliable enough, as long as he gets his thirty pieces of silver."

What are you doing here, you traitor? She watched, gripping her weapon, as he began to speak to nobody in particular. "Lord, all is prepared. Your Repentant are armed with weapons no armor can withstand, and armored by their faith in You, their living God."

"You stand first among My Repentant, Adramelech." The voice answering him sounded hollow, as if echoing from a great distance, and was difficult for Ashtoreth to understand. However, she recognized the tone. The enemy she helped bind beneath the Antarctic ice spoke in this manner when exhorting his human thralls to genocide. "Will you lead them against Imaginos and all who serve him when I come to claim my kingdom?"

"I do as Providence wills."

She frowned, considering the portent behind these words. If Adramelech accepted Sabaoth as God, his words would be a gesture of obeisance. Otherwise, they implied the ensof was nothing but a fraud. Do you only pretend treason, Adramelech? Why?

"You might as well come out now, Ash." She startled at Adramelech's use of the name by which he addressed her in bed. Without Sabaoth's presence to inspire caution, she complied. With each step down the aisle she recalled her marriages to a succession of Magyar chieftans and Hungarian nobles. She offered each the same bargain: allow her to rule in their names, sire children on the human concubines and mistresses she shared with them, and she would raise the children as her own. Ferenc Nádasdy betrayed her, ending the tradition.

Adramelech predated the Magyars and the Nádasdys alike. She studied him as she approached, ready to fight with both her dev'astra and her ability to manipulate sensation, her specialty as a flowseeker. "The priest's collar suits you ill, Adramelech."

"I wear this for my sins, not my pleasure." He stiffened beneath her touch as she drew close and caressed his shoulders. Her hands slid into his hair, and her lips stopped just short of kissing his. His breath burned as he lowered his voice. "You should not be so close to me. Am I not a traitor?"

"Are you? What do you hope to accomplish by abandoning our cause for Sabaoth's? Why come here, of all the locations you might use, to open a line of communication into his prison?"

His kiss closed the circuit between their bodies. His hands branded themselves into her back as he pulled her tighter against him. He reduced her to her panties and torsolette too quickly for her to protest. The stone was cold beneath her hips as he knelt before her. His eyes flamed as he gazed at her from between her thighs, his mouth burning with still greater heat through the satin separating their lips. "I told Imaginos I would let the rest of you believe me a Judas, but your doubt is a cross I will bear no longer."
