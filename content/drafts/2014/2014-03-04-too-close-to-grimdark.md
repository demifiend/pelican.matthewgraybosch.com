---
id: 64
title: Too Close to Grimdark for Comfort
excerpt: "This was a scene I took out of *Blackened Phoenix* because it was too dark, and didn't advance the plot."
date: 2014-03-04T23:37:41+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - court martial
  - forced marriage
  - grimdark
  - joe abercrombie
  - massacre
  - naomi bradleigh
  - the blackened phoenix
---
I wrote this on my lunch break for the first scene in Chapter 3 of _The Blackened Phoenix_, and then decided to take it out. Not only was it closer to grimdark than I usually go, since I'm not <a title="Joe Abercrombie's Defense of Grimdark" href="http://www.joeabercrombie.com/2013/02/25/the-value-of-grit/" target="_blank">Joe Abercrombie</a>, but it didn't make sense for Naomi to relate this experience from her past when Morgan Stormrider and the others are about to learn that their <a title="Operations Security" href="http://en.wikipedia.org/wiki/Operations_security" target="_blank">operational security</a> is a joke because of tech built into Morgan's head.

Also, the nature of Naomi's anecdote is just _full_ of triggering content, and I thought it would be better to not include it. I'm just a _bit_ old to be trying to shock people to get attention. However, I'm posting it here because it seems a shame to throw it away. I might get a short story or a novella out of it, if I can refine it and tone it down a bit.

With that said, I should clarify that I have absolutely no problem with Joe Abercrombie or his work. The only _First Law_ book of his I don't have is _Red Country_. Nor do I have a problem with grimdark fantasy. It's just not what _I_ want to write.

Also, my wife would kill me if I put my cast through half the hell Abercrombie, Martin, and the like inflict on _their_ characters.

[<img class="aligncenter size-large wp-image-65" alt="rule" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/rule-1024x178.png?resize=474%2C82" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/rule.png)

<!--StartFragment-->"One of my missions involved a bride farm, where captive women are impregnated, and then subjected to abortions if the fetus turns out to be male. The baby girls are then taken from their mothers, and inspected for defects. If they pass inspection, they grow up indoctrinated to be obedient wives eager to please the husbands to whom they're sold as soon as they turn eighteen."

Nobody asked what happened to the girls who failed inspection. Naomi suspected their fate was obvious from her expression and tone of voice, because she witnessed what befell one such unfortunate herself. "I left, and came back with a dozen other Adversaries, all women. I thought we'd be able to restrain our anger if we worked together. Instead, after we liberated the captives, we put everybody working there to the sword. We burned the bride farm to the ground after salvaging the records and documentation. We hunted down the customers, arrested them, and tried them. None were acquitted. I hear most died in prison, at the hands of their fellow inmates."

Naomi fell silent and stared at her hands, unable to fathom why she let this story escape her lips. She had kept it to herself since her post-mission debriefing, and the subsequent trial by court martial. Judges left pale and trembling by the evidence recorded through Witness Protocol acquitted her and her fellow Adversaries. "I'm sorry. At the time, my only coherent thought was that what I saw was unforgivable."

Morgan's hand grasped hers. "I would have done the same in your position, with pleasure."

She found Morgan's expression on everybody else's faces, as well. "You all agree with Morgan?"

Sid was first to show his assent. "Come on, Nims. You think I'd want something like that to happen to Elly, or my girls?"

Astarte's voice was small, and quiet. "What happened to the women?"

"There were a lot of suicides, despite the Phoenix Society's efforts to give everybody the care they needed to heal." Naomi stared at the floor, recalling the names of those she proved unable to save. "Sometimes I get letters from the others."<!--EndFragment-->
