---
id: 727
title: 'Why Novelists Shouldn&#039;t Be Coders'
date: 2014-06-13T00:13:59+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=727
permalink: /2014/06/why-novelists-shouldnt-be-coders/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link_facebook:
  - http://bit.ly/1N0l5SL
bitly_link_linkedIn:
  - http://bit.ly/1N0l5SL
sw_cache_timestamp:
  - 401642
bitly_link_twitter:
  - http://bit.ly/1N0l5SL
bitly_link:
  - http://bit.ly/1N0l5SL
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - automation
  - coders
  - Emacs
  - novelists
  - Rake
  - Ruby make
  - tinkering
  - tools
  - unix
---
If you ever wanted proof that novelists shouldn't also be coders, check this out. In addition to doing my writing in a plain text editor (Emacs, if anybody cares), and placing components in separate files, I now use <a title="Wikipedia: Rake (Software Tool)" href="http://en.wikipedia.org/wiki/Rake_%28software%29" target="_blank">Rake</a> (Ruby Make) to combine the various files I create while writing into a cohesive whole, and then call <a title="Pandoc" href="http://johnmacfarlane.net/pandoc/" target="_blank">Pandoc</a> to convert plain text into formats convenient to others.

I did this because I fucked up a command and would have lost the text of a 4,500 words story called "Limited Liability" if I hadn't been using <a title="InSync" href="https://www.insynchq.com/" target="_blank">InSync</a> to automatically copy my work between computers using Google Drive. InSync, by the way, is a sweet little tool. Works like a charm, comes with a version that doesn't require a GUI, and the guy acting as my "happiness ambassador" extended my trial period when I mentioned I installed InSync because I bought a Chromebook and installed Linux on it. 🙂

Ruby makefiles are called "Rakefiles", and here's what the one I whipped up for _The Blackened Phoenix_ looks like. If I just call "rake" in the project directory, it'll build the draft. if I call "rake outline", it'll do the outline instead. If I call "rake clean", it'll clean up all the crap.

<pre>desc "Build the draft for THE BLACKENED PHOENIX."
task :default =&gt; [:clean] do
  sh "cat 1.title.md 2.disclaimer.md 3.dedication.md chapter*/*.md 4.acknowledgements.md 5.authorsnote.md &gt; draft.md"
  sh "pandoc draft.md -t docx -o draft.docx"
  sh "pandoc draft.md -t odt -o draft.odt"
  sh "pandoc draft.md -t html -o draft.html"
end

desc "Build the outline for THE BLACKENED PHOENIX."
task :outline =&gt; [:clean] do
  sh "pandoc outline/outline.md -t docx -o outline/outline.docx"
  sh "pandoc outline/outline.md -t odt -o outline/outline.odt"
  sh "pandoc outline/outline.md -t html -o outline/outline.html"
end

desc "Clean up this directory."
task :clean do
  if File.file?('draft.md')
    sh "rm draft.md"
  end
  if File.file?('draft.docx')
    sh "rm draft.docx"
  end
  if File.file?('draft.odt')
    sh "rm draft.odt"
  end
  if File.file?('draft.html')
    sh "rm draft.html"
  end
  if File.file?('outline/outline.docx')
    sh "rm outline/outline.docx"
  end
  if File.file?('outline/outline.odt')
    sh "rm outline/outline.odt"
  end
  if File.file?('outline/outline.html')
    sh "rm outline/outline.html"
  end
end
</pre>

Is this unnecessarily elaborate when most writers make do with Microsoft Word? Probably. However, I've been monkeying around with computers for a living too long, and it's hard to resist tinkering. At least if I automate my build process, I'm less likely to screw up and lose work.