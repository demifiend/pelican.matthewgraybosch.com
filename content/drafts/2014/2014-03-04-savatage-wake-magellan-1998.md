---
title: 'Savatage: *The Wake of Magellan* (1998)'
excerpt: The penultimate album by Savatage is so good it's a shame the band broke up.
date: 2014-03-04T23:15:40+00:00
category: Music
header:
  image: savatage-the-wake-of-magellan.jpg
  teaser: savatage-the-wake-of-magellan.jpg
tags:
  - concept album
  - heavy metal
  - Maersk Dubai
  - progressive metal
  - rock opera
  - Savatage
  - suicide
  - The Wake of Magellan
  - Veronica Guerin
  - Ferdinand Magellan
  - 1998
---
I had [Savatage](http://en.wikipedia.org/wiki/Savatage)'s penultimate album, _The Wake of Magellan_, playing all night while working on this blog. It's a rock opera/concept album that relates the story of a fictitious Spanish sailor who descended from Portuguese explorer [Ferdinand Magellan](http://en.wikipedia.org/wiki/Ferdinand_Magellan), and incorporates real-life events such as the [Maersk Dubai incident](http://en.wikipedia.org/wiki/Maersk_Dubai_incident) and the murder of Irish reporter [Veronica Guerin](http://en.wikipedia.org/wiki/Veronica_Guerin).

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A1N2dPrm6OBhsZkY8GzqMhv" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

There's over an hour of great music, including some complex vocal shenanigans in "The Wake of Magellan and "The Hourglass". The instrumentals "Underture" and "The Storm" are absolute essentials for any metalhead.

This album is so good it's a shame Savatage broke up, leaving only the Trans-Siberian Orchestra behind. And the cover is gorgeous.

{% include base_path %}
![Savatage: The Wake of Magellan (1998)]({{ base_path }}/images/savatage-the-wake-of-magellan.jpg){: .align-center}
