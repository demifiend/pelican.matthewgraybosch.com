---
id: 190
title: 'Tattoo Vampire &#8211; Part 5 (Final)'
excerpt: Here's a story I wrote to promote *Without Bloodshed*. It also ties into *Silent Clarion*.
date: 2014-03-12T07:00:09+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - fiction
  - morgan stormrider
  - project harker
  - science fantasy
  - science fiction
  - serial
  - short story
  - starbreaker
  - tattoo vampire
---
Christabel settled onto the bed behind Morgan, who sat on the edge with his back to her. Reaching out, she brushed her fingertips against his skin. "Where is it?"

"Gone." Morgan glanced over his shoulder.

"Are you still angry with me?"

Morgan shook his head. "You apologized. I forgave you. Isn't that enough?"

"Something's still bothering you." Adjusting her nightgown, Christabel slid closer to Morgan. She brushed aside his hair and began to kiss his shoulder. "And I've been neglecting you lately."

With a shudder, Morgan pulled away and rose from the bed. "It's not your fault, Christabel. It's just emotional fallout from the mission, but please don't kiss me like that for now."

Fixing a concerned expression to her face, she shifted and patted the bed. _He might tell me something I can give Isaac, even if I can't use it against him myself later on._ "Want to sit down and tell me about it?"

Instead of sitting, Morgan rubbed at the spot where she kissed him, the juncture of neck and shoulder that normally made him sigh and melt for her. "I had to gather evidence that Quincy Westenra was drugging and molesting his victims, as well as drinking their blood. I had to make Westenra think that I was drugged, and unable to stop him from doing what he wanted."

"That's disgusting. You let him do that to you?" Christabel had no need to pretend to be aghast at Morgan's words. "Don't tell me you enjoyed it."

"I hated it." Morgan shook his head. He knelt before Christabel and took her hands in his. "It was a violation, plain and simple, and I wanted to kill him. However, I had to give him a chance to surrender."

Christabel nodded. _Morgan isn't telling me everything. I think he liked it,_ _the slut__._ "Did you?"

"Yes. I gave him his chance. He threw it away, and his life in the bargain."

Recoiling from him, Christabel slid over the bed and placed it between them. "You just killed somebody on the job, but you care more about where I kiss you. Are you even human?"

"I'm human enough, Christabel. The fact you thought so little of me and our relationship that you thought you could throw an ultimatum at me still aches."

"What do you want from me?" Christabel forced herself to approach, fully conscious of the jeopardy in which she placed her mission again. "What do I have to do to make this work?"

"If you can't accept all of me, then ignore what makes you uncomfortable. Let Morgan Stormrider, the Adversary, be somebody you don't know. I won't bother you when I'm on the job, no matter how bad it gets. But when I'm not on a mission, let me be your man, and let me play with your band." He turned from her, gathered his clothes, and began to dress. When he finished, he lingered in the doorway and looked over his shoulder. "I have a briefing tomorrow for another mission. You can give me your answer when I get back."

_If I can seduce him now, he'll never leave me again._ The notion spurred Christabel, and she sped across the room to grasp Morgan's collar. Pulling him down, she kissed him with every scrap of art she learned in Isaac Magnin's bed before leading Morgan back to her own. Pushing him down, she draped her body over his and began opening his shirt. "Is this answer enough?"

His answer did not come in words.
