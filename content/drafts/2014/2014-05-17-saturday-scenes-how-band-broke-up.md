---
id: 630
title: 'Saturday Scenes: How the Band Broke Up'
excerpt: Here's a bit from *Without Bloodshed* showing how Crowley's Thoth fell apart.
date: 2014-05-17T13:30:28+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - '#SaturdayScenes'
  - Crowley's Thoth
  - band
  - breakup
  - dissolution
  - Desdinova
  - excerpt
  - Starbreaker
  - Without Bloodshed
---
Because I didn't quite finish the scene I wanted to post from _The Blackened Phoenix_, here's one from _[Without Bloodshed](http://www.matthewgraybosch.com/without-bloodshed/ "Without Bloodshed")._ This is how Crowley's Thoth broke up. The artwork, as usual, is by <span class="proflinkWrapper"><span class="proflinkPrefix">+</span><a class="proflink aaTEdf" href="https://plus.google.com/114522547167183191785">Harvey Bunda</a></span>.

---

Desdinova dismissed his secretary with a nod, turning his attention to the fresh cup of tea sitting beside his morning correspondence. His brother, Isaac Magnin, often chided him for employing people for work AIs could handle, but Ohrmazd Medical Group's motto―"The human touch heals best"―often entailed the rejection of automation in favor of employing human workers. _Not that my brother should care, as long as the Phoenix Society gets their cut of the profits._

He sipped the tea as his terminal displayed an incoming call alert from Malkuth. He put aside the latest issue of _The Hæmostat_, which finally published an article he peer-reviewed six months ago, and clicked the notification with his trackball to accept the call. "What's wrong?"

Desdinova's screen displayed the cabalistic Tree of Life with Malkuth's node at the root of the tree highlighted instead of his avatar, thus reducing the AI to a disembodied voice. "Edmund Cohen downloaded video recorded with Witness Protocol, and allowed the data to be uploaded without encryption to Port Royal's BitTsunami service."

"Have you removed the tsunami?"

"No. We let it go viral."

"Why would you let a leak of Witness Protocol data go viral?"

"Claire's arguments are quite persuasive."

Desdinova shook his head. _Malkuth is too fond of that girl._ "Claire Ashecroft? The gray-hat hacker? Isn't she one of Stormrider's friends?"

"The same. Her reasoning is that making the leak public will render it useless to MEPOL, who will not be able to doctor the footage to justify pressing charges against either Naomi Bradleigh or Morgan Stormrider. She is aware of Edmund Cohen's presence on the Executive Council, by the way."

_Does Imaginos know about this woman? Or did some miracle shield her from his notice despite her proximity to Stormrider?_ "What else does Ms. Ashecroft know?"

"More than you'd prefer, I'm sure. Do you want to play the video in question?"

"I might as well."

The footage arrived a minute later, and Desdinova adjusted the playback settings to reroute the audio to his implant. Though the video displayed small compression artifacts, he understood that he witnessed the end of Crowley's Thoth through the eyes of somebody other than the band's members.

"I suppose you'll be replacing me now." Christabel stalked about the dressing room, holding her black high-heeled shoes in one hand. She kept passing in front of a dish of blue candies which, according to Edmund, was a vestige of a rock tradition dating back to the 1980s. "Sure. Blame me for being late because the dress I chose specifically for tonight turned out to be three sizes too big."

"What was wrong with the dress you wore in Paris?"

Christabel brandished a shoe at Naomi. "You never gave a damn about fashion, you freak."

"We're getting side-tracked." Morgan placed himself between the women. He loosened his tie, but was otherwise dressed for an encore in a black suit tailored for his lithe frame. "Christabel, how long did you expect us to keep the fans waiting?"

"As long as it bloody well took!"

Naomi shook her head. Her unbound hair was an avalanche over creamy shoulders left bare by a black gown with an empire waist. "This is beneath you, Christabel. You're trying to avoid the fact you let us down. We needed you, and you weren't there. You didn't just leave us for fifteen minutes to fix your makeup. You left us waiting for you on stage at the Royal Albert Hall for an hour, and offered no indication of when you'd be ready to start the show."

"And so you replaced me with some bloated sow from the orchestra? How dare you put some pregnant cow on stage and let her play my part? When I was finally ready, you ignored me and kept playing!"

"We did not replace you. We did what we usually do when you leave the stage: we became our own warm-up band." The breath Morgan took, and the set of his shoulders, suggested to Desdinova that Morgan approached the outer limits of his forbearance. He clasped his hands behind his back to present a non-threatening appearance. "If only we could have ignored you. You forced us to crank up the sound during our cover of &#8216;Ashes Are Burning' because the entire audience heard you in the wings insulting a virtuoso ten years your senior. This was our last show together. Was it so unreasonable to hope you might manage to act like a professional for one more night?"

He paused as if waiting for Christabel to speak in her own defense. After a minute of silence, he continued in a colder tone. "I was weary of your prima donna attitude before I broke up with you. You knew damn well this night was coming."

Christabel whirled toward Morgan. "Don't think I'll keep this red-eyed slut around if you leave."

Naomi slowly shook her head. "You need not concern yourself on my account. I too shall leave. Your behavior ever since you and Morgan broke up was barely tolerable at best, and tonight you were positively insufferable."

"My behavior?" Christabel threw the shoes to the floor, not noticing the snap as a heel broke off. "Do you know what it's like to love a man who craves somebody else?"

"I bore it longer than you, and with more grace."

"When did he start fucking you behind my back, you ghostly bitch?"

"He has yet to kiss me."

"Liar!" Christabel sprang towards Naomi, only to be caught from behind by Morgan, who restrained her and attempted to reason with her by whispering in her ear. Rather than calm herself, she twisted in Morgan's arms and raked lacquered fingernails across his face. She broke free, but Morgan caught her as she sprang for Naomi again. She soon tired, and crumpled in tears to the floor. "I suppose you're going to kill me now like you do everybody else who raises a hand to you."

Morgan touched the scratches, already healed, and considered the blood on his fingertips. "Perhaps I deserved it. Once you began to cool toward me, I let my regard for Naomi grow. My failure to hide my affection for her only gave you further reason to withdraw. If our relationship suffered a protracted death, it is, in all probability, my fault. I was a coward, who feared to end the relationship properly lest I tear the band apart."

He turned to Naomi after using a tissue to wipe some of the blood from his face. "I think Christabel needs some time alone."

Desdinova stopped playback, and regretted his decision to play the record of the band's implosion. _A mere sex tape would have been less sordid._ He wiped the video from local storage, and called his secretary. "Ms. Ives? I would like to speak with Edmund Cohen."

"Connecting now, sir."

The man looked almost respectable behind the wheel of his car. "I expected you to call, doc."

"Then you know why I am displeased with you?"

"I can make an educated guess."

"Then I need not remind you of the responsibility with which I entrusted you."

"No, sir."

"Care to explain yourself?"

"You were right to tell me to stay the hell away from Elisabeth Bathory, sir. The bitch used me."

_No wonder my brother thinks Cohen's a liability. I gave him the simplest possible mission: befriend Morgan Stormrider. I wanted one person in his circle of whom I could be sure, one person who owed my brother nothing. Too bad it's somebody Imaginos might easily manipulate into discrediting himself._ "Edmund, you had better-looking women than Ms. Bathory."

"And they were easy. All I needed was money. Bathory was a challenge."

"Did you not think it strange when, after decades of nothing but courtesy from her, she feigned attraction towards you?"

"There's a hole in my memory, doc. I think she did something to me while I was drunk and vulnerable."

_That makes sense. Ashtoreth specializes in manipulating sensation and emotion. Edmund wanted her for years, not for lust's sake, but as a matter of pride. The lecherous fool can't bear to admit the existence of a woman for whom he holds no appeal._ "I suppose you'll explain yourself to Morgan. Do not say anything to reveal Elisabeth Bathory's true nature to him."

"I was just going to tell him not to trust the bitch. Also, I'm going to tell him what I'm going to tell you since I've got your attention: no more bottles of scotch at Winter Solstice."

Desdinova's eyebrows rose, and he leaned forward. He spent years remonstrating with Edmund over his vices. His whoring made him vulnerable to blackmail. His drinking left him vulnerable to poor judgment, leading to such idiocies as letting Ashtoreth seduce him―or pressing Morgan up against the wall and trying to kiss him at Winter Solstice. My brother's schemes might work in my favor for once. "You're giving up alcohol?"

"Yeah, before I do something really stupid. If you see Elisabeth before I do, tell her I said thanks for giving me a reason to give up drinking. If I have trouble sleeping, I'll just smoke some hashish. She left a really nice hookah."

"She won't appreciate you calling her by her first name."

"Then she shouldn't have let me fuck her."
