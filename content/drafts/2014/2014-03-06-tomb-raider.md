---
id: 116
title: Tomb Raider
date: 2014-03-06T10:46:27+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=116
permalink: /2014/03/tomb-raider/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapWP:
  - 's:160:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:3:"116";s:5:"pDate";s:19:"2014-07-14 23:00:48";}}";'
bitly_link_googlePlus:
  - http://bit.ly/1LDQbSV
bitly_link_twitter:
  - http://bit.ly/1LDQbSV
bitly_link_facebook:
  - http://bit.ly/1LDQbSV
bitly_link_linkedIn:
  - http://bit.ly/1LDQbSV
sw_cache_timestamp:
  - 401663
bitly_link_tumblr:
  - http://bit.ly/1LDQbSV
bitly_link_reddit:
  - http://bit.ly/1LDQbSV
bitly_link_stumbleupon:
  - http://bit.ly/1LDQbSV
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - free games
  - freebie
  - Playstation plus
  - square Enix
  - tomb raider
format: video
---
<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>


  
I played the first _Tomb Raider_, back in the day, but never bothered with the others. I didn't bother with this one until it turned up as a freebie for Playstation Plus subscribers.

I got through the opening last night, and played enough to get Lara her bow and some dinner. I think I'll save this for when Catherine gets back. I think she'll love this.

And it will make a nice change of pace from _Dark Souls II._