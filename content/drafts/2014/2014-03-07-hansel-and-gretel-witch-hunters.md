---
title: "Hansel and Gretel: Witch Hunters"
excerpt: "I usually know better than to watch this crap, but it was on Netflix and I was bored."
date: 2014-03-07T20:26:15+00:00
header:
  image: hansel-and-gretel-witch-hunters-poster.jpg
  teaser: hansel-and-gretel-witch-hunters-poster.jpg
categories:
  - The Tarnished Screen
tags:
  - bad movie
  - Gemma Arterton
  - Jeremy Renner
  - Hansel
  - Gretel
  - strong female character
  - witch hunter
---
I just finished watching _Hansel and Gretel: Witch Hunters_. Hey, I'm home alone with my wife in Australia, and the movie was on Netflix. I wasn't impressed, but I wasn't exactly expecting high art.

{% include base_path %}
![Not bad if you just want violence and cleavage]({{ base_path }}/images/hansel-and-gretel-witch-hunters-poster.jpg)

However, I found an article on [Comparative Geeks](http://comparativegeeks.wordpress.com/2013/01/30/a-character-study-of-gretel-from-hansel-and-gretel-witch-hunters) which suggests that Gretel's character is one of the movie's few redeeming features. She's the brains of the operation. She saves herself and helps her brother kill the witch when they were children. She insists on evidence before killing accused witches. She tries to figure out what's really going on, instead of just fragging witches for money.

She doesn't wait for a knight in shining armor to save her ass, and is only outmatched when facing a corrupt sheriff and his men. Alone against four men, she puts up a good fight, gets her ass kicked, and is rescued by a troll she befriends despite her reasonable fear of a monstrous being capable of massacring four men without effort.

The battles leading up to the end of the film were ridiculous. A gatling gun in a quasi-Renaissance setting? Seriously? And what's with the conjoined twin witches? Never mind that most of the witches gathered for the Blood Moon looked like rejects from a _Hellraiser_ casting call, but conjoined twin witches?

Suffice to say, I'm glad Catherine and I didn't pay to see this in the theater, or pay to rent it. Despite Gemma Arterton as Gretel, this movie just wasn't good enough to pay to see.

I'll admit it was marginally better than Universal Studios' [2004 production of _Castlevania: The Movie_](http://www.imdb.com/title/tt0338526/) At least it didn't have some dead woman looking down on our heroes from heaven.
