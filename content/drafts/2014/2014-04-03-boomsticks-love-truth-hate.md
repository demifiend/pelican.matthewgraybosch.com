---
id: 489
title: 'Boomsticks &#124; (Love – Truth) = Hate'
date: 2014-04-03T11:17:33+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=489
permalink: /2014/04/boomsticks-love-truth-hate/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link_linkedIn:
  - http://bit.ly/1Pj1AHA
sw_cache_timestamp:
  - 401678
bitly_link:
  - http://bit.ly/1Pj1AHA
bitly_link_twitter:
  - http://bit.ly/1Pj1AHA
bitly_link_facebook:
  - http://bit.ly/1Pj1AHA
bitly_link_tumblr:
  - http://bit.ly/1Pj1AHA
bitly_link_reddit:
  - http://bit.ly/1Pj1AHA
bitly_link_stumbleupon:
  - http://bit.ly/1Pj1AHA
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - boomstick
  - fantasy
  - 'Love - Truth = Hate'
  - preternatural
  - reply
  - science fiction
  - supernatural
---
Wordy Newb replied to my post about the <a href="http://www.matthewgraybosch.com/2014/03/31/boomstick-test/" title="The Boomstick Test" target="_blank">Boomstick Tes</a>t in <a href='http://loveminustruthishate.net/2014/04/03/boomsticks/' target="_blank">Boomsticks | (Love – Truth) = Hate</a>.

I'm particularly gratified by the following, which comes at the end of his post.

> But let me extend some well-deserved kindness: the post I previously quoted was an excellent read and I highly recommend it to anyone who (like me) has aspirations to write science fiction or fantasy. I simply found the quote in question to be an excellent illustration of the concept that anything apparently magical or supernatural can be cast as scientific (or vice versa).
> 
> He himself made that point, on purpose, and I do not believe I misrepresented him. However, he himself defined the word ‘supernatural’ quite well, and stands above my fictional J. Random Atheist in terms of clarity of thought. I wronged him and those of his caliber by not making that clear.