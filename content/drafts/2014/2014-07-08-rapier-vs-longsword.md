---
id: 923
title: Rapier vs Longsword
date: 2014-07-08T22:04:34+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=923
permalink: /2014/07/rapier-vs-longsword/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapDL:
  - 's:292:"a:1:{i:0;a:8:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"87ece0c795303619b4e2c04647e4ef82";s:5:"pDate";s:19:"2014-07-09 02:04:55";}}";'
snapFB:
  - 
snapIP:
  - 
snapLI:
  - 
snapSC:
  - 
snapSU:
  - 
snapTW:
  - 's:305:"a:1:{i:0;a:10:{s:4:"doTW";s:1:"1";s:9:"timeToRun";s:0:"";s:10:"SNAPformat";s:25:"%SURL% %ANNOUNCE% %HTAGS%";s:8:"attchImg";s:1:"1";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:18:"486692501370052609";s:5:"pDate";s:19:"2014-07-09 02:05:09";}}";'
snap_isAutoPosted:
  - 1
snapDI:
  - 
snapLJ:
  - 's:317:"a:1:{i:0;a:9:{s:4:"doLJ";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:10:"%FULLTEXT%";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:48:"http://matthewgraybosc.livejournal.com/3010.html";s:5:"pDate";s:19:"2014-07-09 02:05:02";s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapST:
  - 
snapWP:
  - 
snapBG:
  - |
    s:387:"a:1:{i:0;a:9:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:59:"%FULLTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:69:"http://matthewgraybosch.blogspot.com/2014/07/rapier-vs-longsword.html";s:5:"pDate";s:19:"2014-07-09 02:05:57";s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";
snapPN:
  - 
snapGP:
  - 
bitly_link_googlePlus:
  - http://bit.ly/1WQEOsM
bitly_link_twitter:
  - http://bit.ly/1WQEOsM
bitly_link_facebook:
  - http://bit.ly/1WQEOsM
bitly_link_linkedIn:
  - http://bit.ly/1WQEOsM
sw_cache_timestamp:
  - 401535
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Stuff I Found
tags:
  - fencing
  - historic European martial arts
  - Longsword
  - rapier
  - side sword
  - sparring
  - sword-fighting
format: video
---
I found a video from the Academy of Historical Fencing demonstrating rapier vs. longsword. This is relevant to my own interests, since I put a fair amount of sword-fighting in _Starbreaker_.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

In particular, Morgan Stormrider wields a longsword against Imaginos, who favors the rapier when using a blade. Naomi Bradleigh, in the meantime, favors the <a title="The ARMA: Sword Forms" href="http://thearma.org/terms4.htm#.U7yjg_5hW2c" target="_blank">side sword</a>.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

And while Imaginos learned his sword-fighting over thousands of years, and Naomi had experienced teachers, Morgan learned much of his technique from Renaissance fighting manuals like this one.<figure style="width: 600px" class="wp-caption aligncenter">

[<img src="http://i0.wp.com/upload.wikimedia.org/wikipedia/commons/2/2b/Cod_Winob_10825.jpg?resize=600%2C469" alt="Two techniques from the longsword section in the Dresden codex" data-recalc-dims="1" />](http://en.wikipedia.org/wiki/Paulus_Hector_Mair)<figcaption class="wp-caption-text">Two techniques from the longsword section in the Dresden codex</figcaption></figure> <figure id="attachment_2904" style="width: 1000px" class="wp-caption aligncenter"><img src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/07/Gerichtskampf_mair.jpg?fit=840%2C632" alt="Depiction of a judicial duel in the Munich codex: Jörg Breu d. Jüngere (died 1547), Paulus Hector Mair (died 1579) - Bayrische Staatsbibliothek Cod. icon. 393" class="size-full wp-image-2904" srcset="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/07/Gerichtskampf_mair.jpg?w=1000 1000w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/07/Gerichtskampf_mair.jpg?resize=300%2C226 300w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/07/Gerichtskampf_mair.jpg?resize=768%2C578 768w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/07/Gerichtskampf_mair.jpg?resize=610%2C459 610w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" data-recalc-dims="1" /><figcaption class="wp-caption-text">Depiction of a judicial duel in the Munich codex: Jörg Breu d. Jüngere (died 1547), Paulus Hector Mair (died 1579) &#8211; Bayrische Staatsbibliothek Cod. icon. 393</figcaption></figure> 

Sometimes it just isn't enough to RTFM.