---
id: 151
title: 'Matthew Cox: Division Zero'
date: 2014-03-07T20:47:57+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=151
permalink: /2014/03/matthew-cox-division-zero/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link_twitter:
  - http://bit.ly/1PiTX3L
bitly_link_facebook:
  - http://bit.ly/1PiTX3L
bitly_link_linkedIn:
  - http://bit.ly/1PiTX3L
sw_cache_timestamp:
  - 401582
bitly_link:
  - http://bit.ly/1PiTX3L
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - division zero
  - matthew cox
  - psionic
  - psychic
  - science fantasy
  - urban fantasy
  - wraiths
---
One of my fans, <a title="author Matthew Cox" href="http://www.matthewcoxbooks.com/wordpress/" target="_blank">Matthew Cox</a>, just released his latest urban science fantasy novel, _Division Zero_. I just bought a copy.<figure style="width: 1003px" class="wp-caption aligncenter">

[<img alt="Matthew Cox: Division Zero" src="http://i2.wp.com/ecx.images-amazon.com/images/I/811NXOv7cXL._SL1500_.jpg?resize=840%2C1256" data-recalc-dims="1" />](http://www.amazon.com/Division-Zero-1-ebook/dp/B00ITWTRZ0/ref=sr_1_1)<figcaption class="wp-caption-text">Matthew Cox: Division Zero</figcaption></figure> 

Most cops get to deal with living criminals, but Agent Kirsten Wren is not most cops.

A gifted psionic with a troubled past, Kirsten possesses a rare combination of abilities that give her a powerful weapon against spirits. In 2418, rampant violence and corporate warfare have left no shortage of angry wraiths in West City. Most exist as little more than fleeting shadows and eerie whispers in the darkness.

Kirsten is shunned by a society that does not understand psionics, feared by those who know what she can do, and alone in a city of millions. Every so often, when a wraith gathers enough strength to become a threat to the living, these same people rely on her to stop it.

Unexplained killings by human-like androids known as dolls leave the Division One police baffled, causing them to punt the case to Division Zero. Kirsten, along with her partner Dorian, wind up in the crosshairs of corporate assassins as they attempt to find out who – or what – is behind the random murders before more people die.

She tries to hold on to the belief that no one is beyond redemption as she pursues a killer desperate to claim at least one more innocent soul – that might just be hers.