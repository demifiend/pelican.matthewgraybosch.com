---
id: 205
title: Interview with Ellie Garratt
date: 2014-03-12T10:25:21+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=205
permalink: /2014/03/interview-ellie-garratt/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1Pj1AY3
bitly_link_twitter:
  - http://bit.ly/1Pj1AY3
bitly_link_facebook:
  - http://bit.ly/1Pj1AY3
bitly_link_linkedIn:
  - http://bit.ly/1Pj1AY3
sw_cache_timestamp:
  - 401572
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - ellie garratt
  - excerpt
  - interview
  - josefine malmgren
  - mutual aid
  - passing time
  - plug
  - science fiction
  - starbreaker
  - taking time
  - the blackened phoenix
---
British SF author <a title="Ellie Garratt on Blogger" href="http://elliegarratt.blogspot.com" target="_blank">Ellie Garratt</a> was kind enough to <a title="An Interview With Romantic Science Fantasy Writer Matthew Graybosch " href="http://elliegarratt.blogspot.com/2014/03/an-interview-with-romantic-science.html" target="_blank">host me for her weekly interview today</a>. In addition to answering her questions, I treated her and her readers to a rough cut of the first scene in my current project, _The Blackened Phoenix_. Pay her a visit if you want to check it out and meet Dr. Josefine Malmgren.

Ellie is also the author of two collections of short stories, _Taking Time_ and _Passing Time_. The covers link to their respective Amazon pages if you want to help her out by buying copies.<figure style="width: 210px" class="wp-caption aligncenter">

[<img alt="Taking Time by Ellie Garratt" src="http://i1.wp.com/3.bp.blogspot.com/-4wtjf7CKACI/UxcI151SmxI/AAAAAAAAGu8/4ZkyIDwiFfM/s1600/Taking%2BTime%2Blarger%2Bsubtitle.jpg?resize=210%2C315" data-recalc-dims="1" />](http://www.amazon.com/Taking-Science-Fiction-Stories-ebook/dp/B00F193EVM/ref=sr_1_2?s=digital-text&ie=UTF8&qid=1378638510&sr=1-2)<figcaption class="wp-caption-text">Taking Time by Ellie Garratt</figcaption></figure> <figure style="width: 210px" class="wp-caption aligncenter">[<img alt="Passing Time by Ellie Garratt" src="http://i0.wp.com/1.bp.blogspot.com/-yfU1jzL0czE/UxcI80Kc35I/AAAAAAAAGvI/2VHZ6WoQU5k/s1600/Passing%2BTime%2Blarger%2Bsubtitle.jpg?resize=210%2C315" data-recalc-dims="1" />](http://www.amazon.com/Passing-Time-Strange-Macabre-ebook/dp/B00EYMEBKU/ref=sr_1_1?s=digital-text&ie=UTF8&qid=1378407522&sr=1-1)<figcaption class="wp-caption-text">Passing Time by Ellie Garratt</figcaption></figure> 

&nbsp;