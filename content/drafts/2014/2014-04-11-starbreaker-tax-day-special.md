---
title: Starbreaker Tax Day Special
header:
  image: without-bloodshed-final-cover.jpg
  teaser: without-bloodshed-final-cover.jpg
categories:
    - Project News
tags:
  - April 15
  - Big Business
  - Big Government
  - deal
  - ebook
  - Fuck the IRS
  - Kindle
  - Kobo
  - libertarian science fiction
  - libertarianism
  - liberty
  - miranda warning
  - Nook
  - sale
  - starbreaker
  - tax day
  - taxes
  - without bloodshed
---
Did you just get shafted by the <a title="US Internal Revenue Disservice" href="http://www.irs.gov" target="_blank">IRS</a>? Do you feel like you're not getting your money's worth as you pay the price of civilization? Do you wonder what a world in which <a title="Reason.com" href="http://www.donotlink.com/gg1" target="_blank">taxation was recognized as robbery</a> might be like? So do I.

But I did more than just daydream about such a world. I brought it to life in <a title="Without Bloodshed" href="http://www.matthewgraybosch.com/without-bloodshed/" target="_blank"><i>Without Bloodshed</span></a>, the first part of a new epic science fiction series from Curiosity Quills Press: <a title="Starbreaker" href="http://www.matthewgraybosch.com/starbreaker/" target="_blank"><b>Starbreaker</b></a>. **This April 15th, electronic copies of _Without Bloodshed_ are on sale for ONE DOLLAR.**

Most of the Earth's nations are gone, replaced by city-states whose governments are financed and supervised by the Phoenix Society and its corps of individual rights defense officers, the Adversaries. Dedicated to promoting liberty and equal justice under law for all by diplomacy and force of arms, the Adversaries crack down on government overreach and corporate abuses without mercy.

Without Big Government or Big Business, people are free to work, compete, and prosper. They look out for each other through voluntary organizations rather than depending on inefficient social programs  to help them. But the Second Renaissance that the survivors of Nationfall sparked has a soft, black underbelly&#8230;

When a dictator’s public allegations make Morgan Stormrider a liability to the Phoenix Society, the Society orders him to prove Alexander Liebenthal a liar — or die in the attempt.<figure id="attachment_48" style="width: 1800px" class="wp-caption aligncenter">

{% include base_path %}
![Without Bloodshed cover. Artwork by Ricky Gunawan]({{ base_path }}/images/without-bloodshed-final-cover.jpg)

## How to Purchase _Without Bloodshed_

  * [Buy Paperback on Amazon](http://mybook.to/pb-withoutbloodshed)
  * [Buy Kindle Version](http://mybook.to/withoutbloodshed)
  * [Buy Paperback at B&N](http://www.barnesandnoble.com/w/without-bloodshed-matthew-graybosch/1117442538?ean=9781620072790)
  * [Buy Nook Version](http://www.barnesandnoble.com/w/without-bloodshed-matthew-graybosch/1117442538?ean=2940148972402)
  * [Buy Kobo Version](http://store.kobobooks.com/en-US/ebook/without-bloodshed)

I shouldn't say "celebrate", but if you've done your part this tax day to finance the governments of the United States, why not treat yourself to some badass libertarian science fiction? Just imagine your favorite politician up against a wall, listening to a Miranda warning.

**Don't forget, the ebooks are on sale this April 15th for ONE DOLLAR.** Even if you end up not liking it, you probably wasted a lot more supporting Big Government (and its subsidies to Big Business).

<figure>
<img alt="Three Adversaries Walk into a Bar" src="http://i1.wp.com/okalrel.org/wp-content/uploads/2014/04/3Advesaries1-300x219.jpg"/><figcaption><a href="http://okalrel.org/without-bloodshed-part-1/">Three Adversaries Walk into a Bar</a></figcaption></figure>

Also, you can find an excerpt on the <a title="Lynda Williams: Reality Skimming" href="http://okalrel.org/without-bloodshed-part-1/" target="_blank">Reality Skimming</a> blog, courtesy of <a title="Explore the Okal Rel Universe" href="http://okalrel.org/explore-the-oru/" target="_blank">Okal Rel Saga</a> author <a title="Lynda Williams" href="http://okalrel.org/lynda-williams/" target="_blank">Lynda Williams</a>.
