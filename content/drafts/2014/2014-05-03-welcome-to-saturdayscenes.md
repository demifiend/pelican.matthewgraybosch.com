---
title: 'Welcome to #SaturdayScenes'
excerpt: I don't post a scene *every* Saturday, but if I do have a scene to post I'll probably post it then.
date: 2014-05-03T10:48:53+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - '#SaturdayScenes'
  - Bank Robbery
  - John Dillinger
  - Josefine Malmgren
  - Polaris
  - Saturday Scenes
  - Starbreaker
  - Starbreaker scenes
  - Tamara Gellion
  - Blackened Phoenix
---
<a title="John Ward on Google+" href="https://plus.google.com/u/0/+JohnWard/posts" target="_blank">John Ward on Google+</a> suggested that writers start a theme day with a common hashtag on social media called <a title="#SaturdayScenes on Google+" href="https://plus.google.com/u/0/explore/SaturdayScenes" target="_blank">#SaturdayScenes</a>. It's already happening, and I'm posting my contribution here as well.

---

Here's something from _The Blackened Phoenix_ featuring a couple of new characters.

TBP picks up roughly where _<a title="Without Bloodshed" href="http://www.matthewgraybosch.com/without-bloodshed/" target="_blank"><span class="proflinkWrapper">Without Bloodshed</span></a>_ left off. Morgan Stormrider is still after evidence he needs to put Isaac Magnin (or Imaginos) away for life, but Magnin's own plans are in motion, and a young scientist will get caught up when she looks for the truth about the patron who financed her education and gave her a chance to shine.

---

"We're permitting you to keep your weapons and valuables as a sign of good faith. Remain calm, stay out of our way, and we'll be gone shortly." One of the gunmen who had burst into the Asgard Mutual Aid Bank five minutes ago paced in front of Josefine, repeating his warning to the staff and customers. He cradled a militia-issue carbine and wore a Bowie knife. "Don't bother calling for help. We disabled external network access."

He stopped in front of Polaris, who crouched beside Josefine with a hand on the hilt of the longsword he insisted on buying at a nearby Nakajima Armaments store earlier this afternoon. He levelled his carbine at the Project Aesir prototype. "Don't even think of playing the hero, kid. First person to bare steel dies. Then we kill everybody else."

Josefine sped Polaris a warning over secure talk. [Don't provoke them. You can't fight them all with a sword.]

[I don't have to fight them. I just have to kill them.]

Josefine cringed as Polaris met the gunman's question with a cold smile. "I'm looking at you, Charles Kimes. I identified you and your associates by your IP addresses the second you walked in. You're going to die on my sword today."

Kimes grinned, and turned his carbine on Josefine. "Hey, cutie. Is this clown showing off to impress you? I don't blame him. I like the shy, mousy type myself. You a librarian or something?"

Josefine drew her cardigan tight around herself. _I must defuse the situation before this Kimes person or his companions get violent, but how?_ Before she found the words, Polaris spoke again. "Leave Dr. Malmgren alone. I exercised restraint thus far because my creator asked it of me. Should you harm us, you will answer to the AsgarTech Corporation."

"If you're so valuable to AsgarTech, where's your security detail?

"I am an asura emulator, the prototype for the 200 Series. The only security I require is my sword."

"Doc, is this guy telling the truth, or is he your patient?"

_He's giving me an easy out. I can claim that Polaris is delusional, but under my care. Kimes and the others might leave us alone. But the prospect of bringing AsgarTech down on them might give them pause._ "He's telling the truth."

Kimes leered at her. "So, you got paid to make your own toyboy? Damn, lady."

The hiss of a sword partially drawn pulled Josefine's attention back to Polaris. "I'm a far cry from the VirtuaDoll you keep under your bed because even Xanadu House refuses your patronage."

"Damn it, Polaris!" Josefine clamped her hand over the his mouth and hoped Kimes wouldn't hurt her to get at the prototype.

Instead of opening fire, Kimes met Polaris' jibe with laughter. "Kid, the reason I'm banned from Xanadu House is that they ain't got anybody capable of taking what I'm packing. You kids sit tight, now."

Kimes padded off, and Polaris sheathed his sword. "This might work out for us, Dr. Malmgren."

"It's more likely to get us killed." Josefine caught Polaris by his collar and pulled him close. "Why did you try to provoke him?"

"I wanted his attention. Kimes and the others came with the intention of robbing a bank, but grabbing enough bullion to make a bank robbery profitable takes time these men lack. By revealing that we are valuable individuals, I've given Kimes cause to suggest to his confederates that they take us hostage and let everybody else go."

Josefine peeked over the countertop. As Polaris predicted, Kimes had gathered the other robbers. They spoke in heated whispers, glancing around to ensure the employees and customers weren't about to rush them.

Polaris smiled at her. "Assuming my call got through and was taken seriously, the police should arrive any moment."

A broadcast message from the APD hit Josefine's implant: "Attention Asgard Mutual Aid Bank employees and customers. Please remain calm. We will negotiate your release."

A robber holding a Kalashnikov with one hand held his fingertips to his ear, as if he were talking to somebody outside. She checked the windows, and saw a patrol car and two vans. A militia squad waited at attention behind two uniformed officers and a stern-looking woman in a black suit. "Suppose they make this robbery a kidnapping and take us? What then?"

"With everybody else gone, I need only draw their fire away from you while I kill them all."

"They'll shoot you. One of them has an AK. All you've got is a sword."

"That's all I need." Polaris smiled as he said this. "What?"

Josefine sighed. "If you get shot up and killed, what do you expect me to tell Dr. Magnin?"

"They won't be able to kill me, and it won't matter if they shoot me. I'll get over it." Before she could stop him, Polaris drew his sword partway and cut deep into his palm. The wound hardly bled before closing. Seconds later, his hand was whole again.

Taking his hand, Josefine tried to trace the cut. "You don't even have a scar."

"Do you understand now, Dr. Malmgren? They can't hurt me."

Josefine glanced at the robbers. They finished their conversation, and split up. The one with the Kalashnikov raised it skyward. "It's your lucky day, so listen up. Everybody who isn't Dr. Malmgren or a prototype asshole emulator is hereby welcome to get the fuck out." The robbers began yanking customers and bank employees to their feet. "Come on, people, we ain't got all day."

Kimes returned as the bank emptied. "On your feet, pretty mouse. You too, droid. We got a police negotiator coming in, and the boss wants to show you off."

Josefine smoothed her clothes and followed Kimes. The leader removed his sunglasses, revealing feline green eyes that seemed to appraise everything before him in utilitarian terms. His glossy black hair and aquiline features reminded her of an Adversary she saw mentioned in the news. He lowered his rifle, and thumbed the safety on. "Do you know why I decided to hold you and your science experiment, Dr. Malmgren?"

She shrugged. "I expect you mean to tell me, whether I can bring myself to care or not. No doubt you expect a ransom from the AsgarTech Corporation."

The boss chuckled. "Kimes told me you were just a little mouse, but I think he needs his eyes checked. You're a kitten: adorable, but still possessed of sharp little teeth and claws."

"Why not give me your name first, or should I keep thinking of you as Kimes' boss?"

"Of course, doctor." The boss gave an elaborate bow from the waist, extending one leg as if he were an old European courtier. "Where are my manners, anyway?"

"You pawned 'em to meet last week's payroll, boss." A hairy hulk of a man cradling what appeared to be an anti-tank rifle spoke up. "Where's that demon-ridden negotiator, anyway?"

"I'm sure he'll be along in due course, Bear." The boss returned his attention to Josefine. "Where were we, Doctor? Ah, yes. Names. I know yours, but you never got mine. Call me Dillinger. John Dillinger."

Polaris snorted from behind Josefine. "Nice alias, but your real name is Addison Mortimer. You're a one-hundred series asura emulator, unit number 328. You and your crew used to do counter-espionage for AsgarTech, but they stiffed you on your last job."

He stumbled past Josefine as Kimes jabbed his rifle's stock between his shoulders. "John, this clown's been mouthing off the whole time. We only really need the mouse, if you want me to do the honors."

Mortimer shook his head. "Don't waste ammunition on him. He'll just get over it."

Josefine stared at Mortimer, one thought foremost in her mind. _If he heard that, he expects Polaris to attack them._

"I could whittle him a bit, instead." Kimes jabbed the air with his knife. "Won't waste any ammo that way."

"Carve on Dr. Malmgren instead. Get Bear to hold her down, and get her boots off." Mortimer patted Josefine's cheek. "You don't type with your toes, do you?"

Steel rang defiance, and the reek of blood filled the air. Before Josefine realized what had happened, Mortimer's crew lay on the ground, clutching at mortal wounds and moaning their last. Mortimer strained against Polaris, fending off his longsword with a combat knife in a reversed grip. The prototype hammered Mortimer with a flurry of cuts that drove the dark-haired man against the wall.

Polaris drew back, and Josefine opened her mouth to command him to desist. Before she found the words, he lunged forward and drove his sword through Mortimer until the guard pressed against his chest, pinning him to the wall.

Josefine stopped short of grabbing Polaris' shoulder, lest he turn on her. "Please stop."

"Not yet." The prototype retrieved Bear's monstrous rifle. "Mortimer isn't dead yet."

_Not dead yet? But Polaris impaled him._ She looked, and gasped at the fury in Mortimer's eyes. He held the sword's cross guard in a white-knuckled grip, his arms and shoulders trembling as he worked against it.

"Turn away, and cover your ears."

Too frightened by the massacre before her to do anything else, Josefine obeyed her creation. She whimpered as the gun roared behind her.

"That is more than sufficient, Polaris."

Josefine lowered her hands, opened her eyes, and turned around. Between Polaris and the still-impaled Mortimer stood the chestnut-haired woman she saw with the police and militia outside. Easily as tall as Polaris despite not wearing heels, she remained unperturbed by the bullet suspended in mid-air before her. It fell, and thumped softly upon the ruined carpet.

_Did she just stop an antitank round in mid-air? That's impossible. Isn't it?_ Rather than dwell upon this phenomenon, Josefine rushed to Polaris' side and placed her hands on his arm. "Please let go of the rifle. Let's not make the situation any worse."

Polaris crouched, lowering the rifle to the floor without taking his eyes off the woman before her. "Who the hell are you?"

"The AsgarTech Corporation asked me to negotiate on their behalf. I'm Tamara Gellion." She stepped aside, clearing the way to Mortimer. "Please reclaim your sword. I require this asura emulator."

"Please wait." Josefine stepped in front of Polaris. "I have so many questions, Ms. Gellion. How did you stop that bullet? Do you know anything about the Asura Emulator Project?"

"Those are excellent questions, Dr. Malmgren, but they shall remain unanswered because you do not factor into my plans. You should be on leave, should you not?" Tamara cupped Josefine's chin and stared into her eyes. "The most sensible thing you can do is book a long vacation, far from here. I recommend Armstrong City. The Earthrises are beautiful this time of year."

Before Josefine could protest, Tamara gripped Polaris' sword by its hilt, and pulled it free as easily as Josefine might pluck a knife from the block in her kitchen. She passed a hand over one side of the blade without touching it, leaving it clean and gleaming. She turned the sword over and did the same for the other side before offering it to Polaris hilt-first.

"Thank you." Polaris accepted the weapon, and sheathed it. "What do you mean to do about Mortimer? He threatened Dr. Malmgren."

"He has suffered enough for his effrontery." She wound her fingers into Mortimer's hair and lifted the groaning bank robber to his feet. "I will ensure he does not trouble either of you. Some of my plans involve him and his kind."

_Who the hell is this woman, and why would she tell me to book a vacation on the Moon?_ Josefine narrowed her eyes. "What sort of plans?"

Tamara only smiled. "Armstrong City, Dr. Malmgren. Do yourself a favor, and book your flight today."
