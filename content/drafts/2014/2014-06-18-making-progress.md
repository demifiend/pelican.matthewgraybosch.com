---
title: Making Progress
categories:
  - Project News
tags:
  - progress
  - silent clarion
  - the blackened phoenix
---
So, I've got the plot for _The Blackened Phoenix_ properly outlined. I'm tempted to share some with my followers to offer a taste of what to expect. Now I can start making some serious progress.

Also, I'm 666 words into Chapter Five of _Silent Clarion_, and I'll probably write the rest tonight. Naomi's getting one last fencing lesson. (Update: Chapter Five is in the can.)

I can't share it, because of contractual obligations. I'm working on serializing the novel through Curiosity Quills Press​. Expect a formal announcement soon.

[<img class="aligncenter size-large wp-image-487" src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/the-blackened-phoenix-temp-cover-682x1024.jpeg?resize=682%2C1024" alt="the-blackened-phoenix-temp-cover" data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/the-blackened-phoenix-temp-cover.jpeg)
