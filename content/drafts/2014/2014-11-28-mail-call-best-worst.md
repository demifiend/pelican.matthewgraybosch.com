---
title:    'Mail Call: Best of the Worst'
excerpt:  I like getting email from readers of the Starbreaker saga, but some of these are ridiculous.
category: "Project Updates"
tags:
  - fan mail
  - trolls
  - public mockery
  - Without Bloodshed
  - Silent Clarion
  - Feminism
---
I like getting email from readers, but I occasionally get emails asking questions so obtuse I can't help but wonder what the hell they read and mistook for my work. I often suspect some of these emails come from trolls, rather than readers. I share many of the most egregious ones with my wife Catherine, because they're funny.

She suggested I also share them with you. I won't share names or email addresses, because they aren't relevant. I'll just quote the messages, and follow up with commentary. Please remember that quoting is not endorsement.

I wish I was making this shit up. Believe me.

## This is Why We Still Need Feminism

> Why are all your female characters sluts?

This is one of those emails I get fairly often, and I can't help but think that the senders didn't actually *read* any of my Starbreaker stories. I have perhaps two characters who could reasonably be called promiscuous:

 * Claire Ashecroft
 * Elisabeth Bathory

Claire makes no apologies about [her lifestyle](http://www.amazon.com/The-Ethical-Slut-Infinite-Possibilities/dp/1890159018), and is an incorrigible flirt, but is scrupulous about respecting other people's boundaries. She prefers to keep things [safe, sane, and consensual](http://en.wikipedia.org/wiki/Safe,_sane_and_consensual).

'Elisabeth Bathory' is an alias for one of the ensof, Ashtoreth, who was often venerated as a fertility goddess. She keeps lovers as a way to stay in touch with the mortal, physical being she used to be before her transformation.

Granted, the other women of Starbreaker don't wait for wedding bells before taking their lovers to bed, but why should they? They're in exclusive relationships with people with whom they share long-standing bonds of affection and mutual respect.

Incidentally, I find it telling that none of the people sending questions like this one seem to mind that Edmund Cohen has an AI whose avatar is modeled on an old porn star, and would rather use prostitutes than get into anything resembling a relationship.

## Fake Geek Girls of Starbreaker

These are fun. Somebody's social circles consist only of people who like mainstream stuff, and their imaginations atrophy until they can't believe that people with non-mainstream interests exist.

> It isn't realistic that Naomi Bradleigh likes heavy metal. All the women I know listen to Taylor Swift and Katy Perry.

Bit of personal history here. The first woman I loved was a metalhead. She gave me my first kisses while playing the *October Rust* album by [Type O Negative](http://www.metal-archives.com/bands/Type_O_Negative/802) &mdash; and told me that [Iron Maiden](http://www.metal-archives.com/bands/Iron_Maiden/25) was "too dramatic". I hope she's happy, wherever she is now. I'd love to give her a copy of *[Without Bloodshed](/stories/without-bloodshed/)*.

I have a [shitload of real-life precedent](http://metalholic.com/metalholics-top-25-women-hard-rock-metal-2013/) for Naomi Bradleigh as a metal singer. Sure, I started with Annie Haslam of the art-rock band Renaissance, but we've also got vocalists like Lzzy Hale, Sharon Den Adel, Tarja Turunen, Lita Ford, Floor Jansen, Charlotte Wessels, Manda Ophuis, and Elize Ryd. Naomi as a keyboardist isn't without precedent either, thanks to [Keiko Kumagai of Ars Nova](http://www.arsnova-prog.com/index.htm).

> Claire Ashecroft isn't a real geek. I don't know any women who screw around with computers, play games, or are involved in fandom.

I'm just a *little* annoyed that people still can't take a woman who displays an interest in computers, gaming, or fandom at face value. Instead, they assume her interest is fake. Never mind that Mary Shelley wrote the first science fiction novel, or that Countess Ada of Lovelace wrote the first computer program.

It's bad enough when this bullshit is aimed at women in real life. It's positively galling when somebody accuses a character I've developed as a person her friends value for her technical expertise of faking it. Do these people not read the shit I write? Or do they work for Mattel?

## Dispatches from Fox News

> Why do characters in Starbreaker celebrate the Winter Solstice? Why did you have to take Christ out of Christmas?

This email makes no sense whatsoever. Christmas is December 25. The Winter Solstice (or the Summer Solstice for readers located south of the equator) is December 21.

Christmas is still a holiday in the Starbreaker setting. It's just a religious holiday celebrated almost exclusively by Christians.

Instead of diluting Christmas and other holy days, the majority of the people living in the Starbreaker setting celebrate the [Solstices](https://en.wikipedia.org/wiki/Solstice) and [Equinoxes](https://en.wikipedia.org/wiki/Equinox). They're astronomical events with no basis in any religious tradition. Everybody can celebrate the Solstice regardless of beliefs, because the sun shines on everybody.

## The One For You and Me

> Morgan Stormrider was born on June 6 at 6 in the morning, right? Does that mean your hero is the Antichrist? How can you do that?

Pretty easily, actually. First, I'm just fucking with you. We've got lots of fantasy novels where the protagonist is *special* because they were born on a propitious day or during some astronomical event nobody realizes is totally natural because there's no such thing as  science.

Here's the deals: Morgan Stormrider is one of six hundred and sixty-six 100 Series Asura Emulators cranked out of an AsgarTech Corporation factory on June 6, 2082 at six in the morning. Any *one* of them could have been the hero of this story. It just so happens that Morgan was the one whose life choices put him at the center of the story.

Prophecy plays no role in Starbreaker, and references to [666](https://en.wikipedia.org/wiki/666_(number)) have more to do with [Iron Maiden](https://en.wikipedia.org/wiki/The_Number_of_the_Beast_%28album%29) than [Christian eschatology](https://en.wikipedia.org/wiki/Christian_eschatology).

<iframe width="420" height="315" src="https://www.youtube.com/embed/WxnN05vOuSM" frameborder="0" allowfullscreen></iframe>

It's only rock 'n roll. Seriously. Just relax and enjoy the show.

## Didn't I explain this? I'm sure I explained this.

> It doesn't make sense that Morgan and the others would use swords when they have pistols and rifles.

Right, and it doesn't make sense for police officers to use [batons](https://en.wikipedia.org/wiki/Baton_%28law_enforcement%29), [pepper spray](https://en.wikipedia.org/wiki/Pepper_spray), or a [taser](https://en.wikipedia.org/wiki/Taser) when they're carrying a pistol. No, cops and Adversaries should use firearms every time, [use of force continuum](https://en.wikipedia.org/wiki/Use_of_force_continuum) be damned.

## I didn't say anything about Windows

> HermitCrab from *Silent Clarion* isn't realistic. You can't boot a functional Windows environment from a USB stick or a SD card. I'm a MCSE, so I know better than you.

First, MCSE stands for all kinds of [unflattering phrases](http://www.bpfh.net/microsoft/what-mcse-stands-for.html). Second, I never said that HermitCrab was Microsoft Windows, or based on Windows. HermitCrab is a bootable Unix operating environment you can run in RAM, without touching storage.

We've had those for a while, starting with the venerable [Knoppix LiveCD](http://www.knopper.net/knoppix-info/index-en.html). I've been fucking around with Linux and Unix since the late 1990s, so I know a few things myself.

## Patience: The Forgotten Virtue

> WTF is wrong with you? I'm up to Chapter 19 of *Silent Clarion* and you still haven't shown us who the bad guys are.

Seriously? Naomi herself doesn't know who the bad guys are yet, at least not for sure. She's just got her suspicions.

When did we stop considering [patience](https://atticannie.wordpress.com/2010/01/31/can-we-save-the-virtue-of-patience/) a virtue?

## At Least He's Honest

> I'm a guy, and I don't think Morgan Stormrider is realistic. Why would he put up with Christabel being a bitch to him for so long if he could dump her, start a new band with Naomi, and bang her instead? That's what I'd do.

This one's a bit complicated. Yes, Morgan could have dumped Christabel, broken up Crowley's Thoth, and possibly started a new band with Naomi Bradleigh. He might even have been able to start a new relationship with Naomi.

But consider the following: if Morgan grew tired of Naomi, what would stop him from treating her as he did Christabel? Morgan knows Naomi isn't an idiot; it's one of the reasons he loves her. So he isn't willing to do anything that would give her cause to doubt him.

Also, Christabel subjected Morgan to psychological abuse throughout their relationship. As a result, Morgan isn't sure he even deserves to be with Naomi because he couldn't make it work with Christabel.

## For the Dog Lovers

> Why are there so many people with cats in Starbreaker? Aren't there any dog owners?

Yes, there *are* dog owners in the Starbreaker setting, but they aren't relevant to the story yet. Sorry.

## Email from the KKK

> Why is the Phoenix Society's most important AI a \<racial slur\>?

Why? Because fuck you is why. How the hell do you manage to type while wearing that white hood, anyway?

## Practice Makes Perfect?

> You should stop writing female characters. You're no good at it.

What? Seriously? I could understand if the characters of *[Without Bloodshed](/stories/without-bloodshed/)* or *[Silent Clarion](/stories/silent-clarion/)* didn't do it for you, but stop writing women? How am I supposed to get better?

## Dude, it was *one* time!

> You write like a faggot, and you look like one too. Faggot.

Dude, it was *one* time. It was dark, I was drunk, and you had been wearing your sister's makeup. You didn't seem to mind at the time.
