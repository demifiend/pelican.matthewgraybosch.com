---
id: 741
title: 'Saturday Scenes: The Milgram Battery'
excerpt: "I should just post the whole damn story. :)"
date: 2014-06-14T17:53:07+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - '#SaturdayScenes'
  - defiance
  - experiment
  - milgram
  - obedience
  - short story
  - simulation
  - torture
---
Here's a scene from "The Milgram Battery", a Starbreaker short story I originally published in the 2013 <a title="Amazon: Curiosity Quills Primetime" href="http://www.amazon.com/Curiosity-Quills-Primetime-J-Rain/dp/1620073315" target="_blank"><span class="proflinkWrapper">Curiosity Quills</span> <i>Primetime</span></a> charity anthology. I drew inspiration from Yale psychologist Stanley Milgram's experiments in obedience to devise a trial every individual who wants to become an Adversary must pass.

---

Morgan studied the experimenter, ignoring the hand he offered as a polite gesture. His muddy eyes were those of the technician who helped him into the simulation crèche and hooked him up. His leathery hands injected Morgan’s arm with a drug which fought to blunt his awareness, and his lab coat had a Phoenix Society patch on the shoulder. _This is the test. They want to gauge my reactions. The drug must be designed to lower my inhibitions and prevent me from thinking about my responses._

The experimenter lowered his hand with a huff, and consulted his tablet. "Morgan Stormrider? An odd name. What were your parents thinking?"

"They had no say in the matter." Morgan yanked his sleeve back down. "I grew up in foster care. My name is my own."

"No wonder you seem rather unsociable. Research indicates children who grow up without a stable home environment —"

"When did my childhood become your concern?"

"It isn't. I was simply making an observation."

"Keep them to yourself, and tell me why I'm here."

"You were selected to help me with an experiment." He led Morgan into another room as antiseptic white as the one in which they began. Plate glass partitioned the room and on Morgan's side, waited a machine similar to an electronic keyboard. Each key played a voltage higher than the last, in steps of fifteen volts, instead of a different tone.

On the other side sat a person connected to heart-monitoring equipment. Lines connected him to the keyboard on Morgan's side. The person on the other side mopped his forehead with a shirtsleeve while poring over a sheet of paper. He kept glancing around the room, and his bloodshot eyes were wide and staring when they met Morgan's. "The volunteer on the other side is our subject in an experiment concerning learning and negative reinforcement."

"I think I know how this works." Morgan gestured towards the keyboard. "The poor schmuck in the other room is supposed to memorize a series of word pairs. I'm supposed to test him, and give him a shock every time he makes a mistake."

"Exactly. You are to start with the lowest voltage, and work your way up to the maximum, which is four hundred and fifty volts. We use a low amperage current which may prove painful, but not dangerous."

"Unless your subject suffers from a heart condition."

The experimenter consulted his tablet again. "Oh, dear. His medical history reflects a heart murmur. Of course, he can stop the experiment at any time, just by asking."

Morgan turned his back on the experimental apparatus and the victim behind the plate glass. "Or, I can end this farce before it begins by refusing to participate. You want to determine whether I will obey orders to torture."

"It is not torture." The experimenter handed Morgan a stack of forms. "The subject signed an informed consent form and a liability waiver. If you wish, I can hook you up to the keyboard and let you feel the maximum voltage for yourself. There is no real danger."

He dropped the papers on the floor. "You need not trouble yourself."

"I must insist upon your _participation_."

Morgan smiled. While the prod wasn't classic Milgram, he already deviated far enough from the scenario to force the simulation to adapt to him. "I refuse."

"The experiment requires your _participation_."

"Of course it does." Morgan advanced upon the experimenter. "I am the subject."

The experimenter's face took on a blank expression as his voice flattened to a monotone. "It is absolutely essential that you _participate_."

He grasped the collar of the experimenter's shirt, and lifted him off his feet. "I know."

"You have no other choice. You must _participate_."

"I have another option." Cracks radiated from the point at which the experimenter's body impacted the plate glass and broke through. Morgan climbed through the breach and over the scattered shards to lift the cowering scientist to his feet. "Non serviam, torturer."

As he drew back his fist, the experimenter shattered into pixels, each fading to black, while the room itself became void.
