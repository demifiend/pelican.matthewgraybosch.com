---
title: "*Silent Clarion*, Track 07: &quot;I Fought the Law&quot; by The Clash"
excerpt: "Check out chapter 7 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. The rivalry between police and Adversaries is not unlike the one between Cardinal Richelieu's soldiers and the Musketeers, but Naomi stands alone."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
A transit workers' strike kept me from getting home at a reasonable hour. Not that I blamed the workers. Seems they were worried about the new AIs being installed on all trains in the Tube eliminating their jobs. The AIs also refused to work, which surprised the striking workers. I doubt anybody wrote science fiction predicting solidarity between human workers and intelligent machines.

An emergency dispatch order from the London Chapter had me back on the job, which let me save on cab fare. I rode most of the way home in a bus full of striking transit workers, and patrolled the picket line to ensure MEPOL didn't do anything stupid. The authorities have a history of using agents provocateur to turn peaceful protests violent, thus creating an excuse to crack down. I stopped three such attempts.

As a result, I didn't get home until three in the morning. Some vacation! I was famished, so I stopped at a nearby twenty-four-hour grocery for a frozen pastie. I ate while texting my parents to tell them I got home safely, and curled up in bed. I slept late, lazed in bed for an hour while reading, and indulged myself with a long hot bath instead of just showering.

I had nowhere in particular to go today, and the city beyond my immediate neighborhood was out of reach due to the Tube strike unless I wanted to waste money on cab fare. I used my implant to research local businesses while I soaked. I had no idea I lived so close to a Xanadu House, but since I brought one of my waterproof toys into the bath with me I had no need for their services.

I did, however, need a haircut. The snowy mane I favored was fine as long as I kept it pinned up while on duty, but it had become a bit ragged. An ominously named salon called Moirai had slots free this afternoon and catered to women with CPMD, so I booked myself in for some pampering.

Moirai was blanketed in shadows broken only by bright lights illuminating individual work areas. The black leather and chrome décor reminded me of an underground nightclub. The photos lining the walls suggested that not only did the salon cater to women with CPMD, but also catered to women with a taste for heavy metal. I heard technical death metal playing in the background, with the sound turned down low. The growled lyrics were less comprehensible than usual because of the volume, and because they were Greek instead of English. It was my kind of place.

The receptionist favored me with a knowing smile as the door closed behind me. "Hello, Adversary Bradleigh. My sisters and I suspected you'd eventually visit. You always pass by on your way to work."

"Do I? I never thought about it much."

The receptionist worked her terminal. "No matter. We'll start with your nails once Lachesis is ready. Would you like something to drink?"

"That sounds perfect." I unclipped my sword from my belt and offered it. "Do you want to hold this for the duration of my visit?"

The receptionist wrote on a tag, which she tied to the hilt of my sword before putting it in a safe behind her. She then ducked into the back, returning with two bottles of water. She offered me one. "Sorry. I forgot we didn't have anything else."

"Water's fine." The glass bottle was frigid in my hand as I drank. It was just what I needed as I borrowed one of the tablets laying on the table in the waiting area and checked the news.

I expected the lady working on my nails to chat, but she handled me with a briskness that felt almost clinical, and did not speak unless instructing me. She studied me with cold eyes, as if measuring me. Despite her unfriendly manner, she handled me gently, and left my nails a brilliant red.

She gave way to another woman, who dressed all in black and wore a more kindly expression. Her touch was gentler than her predecessor's as she led me to a chair, gathered my hair, and began to wash it. "Do you know how rare your coloration is, Adversary?"

"Snow-blonde isn't that rare a color in people with CPMD, is it?"

"Not your hair, dear. Your eyes. They mark you as an ensof's child, a demifiend."

Demifiend? What the hell is she on about? Being called half-demon felt like an insult, albeit a more original one than some I've heard. Nor did the word ensof mean anything to me. Using my implant to run a search got me bugger-all besides references to the Zohar and other elements of Kabbalah, of which I knew just enough that an explanation of where the Society's ten AIs got their names was unnecessary, so I kept quiet and let her work. Maybe she'd end up clarifying her remarks. Hope's even cheaper than talk.

She massaged my scalp as she spoke, which felt so good I resolved to get any lovers I took in the future to do it for me. "Some of our people will despise you, like my sister Lachesis, but you don't get to choose your parents."

Lachesis? The salon's name made more sense, but I wondered which of the Fates would cut my hair as I changed chairs. I watched as the woman tending me selected a pair of scissors. "I suppose you're Atropos."

She nodded. "Very astute, dear. And you've met Clotho. You have lovely, thick hair, by the way. Have you given any thought to what sort of style you'd like? Perhaps some layers or a bit of feathering to give it more volume? Or would you prefer a more practical style that will let you tie back your hair on duty?"

I was impressed Atropos would consider my duties while suggesting the most flattering possible style. "I think I'll depend on your judgment, Atropos."

"Will you, now?" Atropos smiled at me. "What if you don't like it?"

I shrugged beneath the smock she draped over me before washing my hair. "It'll grow back. It always does."

"That's a rather philosophical attitude for a young lady to espouse." I heard a snip, and a lock of my hair fell free. I raised my hand to brush it off, but Atropos beat me to it. "Hold still, please."

Atropos was true to her word, and left me with a long, layered cut that flattered my face. I paid Clotho, leaving a hefty gratuity, and made another appointment for next month after reclaiming my blade. I also got the name of the album I heard playing: *Perpetual Titanomachia* by Tartarus.

Unable to decide on a restaurant for dinner, I settled for an Agni Burger before I turned back toward home. As I followed my lengthening shadow, I heard footsteps behind me. Two men followed me at first, and two more joined them. After a block, I turned to confront them. "Is there a problem, gentlemen?"

All four were in decent shape. Each wore a gladius on his hip and civilian clothes, which suggested they were off-duty cops. They were rough, square-jawed men with squat bodies and thick, grasping hands. The tallest stepped forward, a hand on his hilt. "You that white-haired bitch who got a bunch of our friends from the East End suspended without pay?"

I shrugged. "They got themselves suspended through their inability to respect individual rights."

The leader glanced at his friends. "Lift the suspension. Now."

I used my implant to scan the street while messaging the Phoenix Society to request backup. If I managed to deal with these clowns on my own, great, but a sword or two beside me wouldn't go amiss. "I don't have the authority to rescind the suspension."

"I think you just aren't willing. Maybe you look down on us?"

I shook my head. This situation had begun to remind me of the elder Dumas' romances. Was I a lone Musketeer against four of Cardinal Richelieu's soldiers? "I think you're looking for an excuse to escalate the rivalry between MEPOL and the Phoenix Society."

"Nah. We just think you're a stuck-up bitch who needs to know her place."

I glanced at the speaker, who had begun circling to my right. "And you think you're the men to teach me my place?"

"Oh, don't you worry about that." A cop circling to my left spoke. "We only go for human women, not freaks like you."

"Come on, guys. I'm a freak like her, too." The cop who hadn't spoken yet did so. I got a better look at him, and realized he had CPMD. "This really isn't a good idea. She was just doing her job."

I nodded to him. "Thank you."

The other cops rounded on him. "Who the fuck are you trying to impress with the white knight act, Carson? You're going to side with this harpy because she's a pussycat like you? What the hell for? She's probably a bloody lezzer."

Carson drew his gladius. "You said you just wanted to talk to her, but now you're ready to start a fight. This isn't right, and you goddamn well know it."

I sighed, and drew my sword as well. Two against three was better than one against four, but I would have preferred to settle this without violence. "Gentlemen, we should all go home and get a good night's rest. In the morning, you can appeal directly to the Phoenix Society. I won't mention this incident."

A cry pierced the dusk, and Carson crumpled to his knees, clutching at the stab wound in his belly. I speared one man through the shoulder before withdrawing my blade. I sidestepped a thrust from one of the remaining cops and slashed open his coat, leaving a bloody gash across his chest.

The cop who first spoke to me picked up a fallen blade, and came at me with a weapon in each hand. I caught him in the belly with a lunge. Hearing a snarl behind me, I spun to face the man whose chest I sliced. He glared at me while pressing his free hand against his wound. "You murderous whore. I'm gonna —"

I pierced the ligament in his elbow, and he dropped his sword. "I haven't murdered anybody, yet. If you get medical attention in time, you'll live."

Sirens filled the air. Two ambulances, a MEPOL patrol car, and a Phoenix Society staff car screeched to a halt beside us. I cleaned my blade and sheathed it, turning my back on the fallen off-duty cops. As paramedics began triage, I held up my empty hands and decided to get out of London at the first opportunity. This was no place for a holiday.

---

### This Week's Theme Song

"I Fought the Law" by The Clash

{% youtube AL8chWFuM-s %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
