---
title: Safe in My House on Mars
date: 2014-03-19T13:55:58+00:00
excerpt: I sometimes suspect I might be harming myself by seeking the solitude necessary to write. But it's easier this way.
header:
  image: my_house_on_mars_by_misshalcyon-d5yc9kc.jpg
  teaser: my_house_on_mars_by_misshalcyon-d5yc9kc.jpg
categories:
  - Personal
tags:
  - Ayreon
  - cynicism
  - friendship
  - isolation
  - life in America
  - life on Mars
  - loneliness
  - masculinity
  - My House on Mars
  - social media
  - solitude
---
James Warner over at the <a title="Sundog Lit Blog: Best Worst Year Episode 53; or, Life on Mars" href="http://sundoglitblog.wordpress.com/2014/02/13/best-worst-year-episode-53-or-life-on-mars/" target="_blank">Sundog Lit Blog</a> wrote about growing up, becoming jaded, turning inward, and using a "lone writer" persona to keep the world at arm's length. He wrote about pushing oneself to turn outward and reach out to others again after years of self-imposed isolation. He wrote about life on Mars, and coming back to Earth and being among people again. I see a lot of myself in that post, but it's nice and safe here in my house on Mars.

[!["My House on Mars" by MissHalcyon on DeviantArt](http://pre08.deviantart.net/7978/th/pre/i/2013/076/2/d/my_house_on_mars_by_misshalcyon-d5yc9kc.jpg)](http://misshalcyon.deviantart.com/art/My-house-on-mars-360010092)

I suspect I turned inward and learned to armor my heart in ice and shadows for reasons other than Mr. Warner's. Regardless, I keep the world at a distance. Instead of filling my life with people, I fill my life with things made by people: books, music, art, games, and movies. I know I learned as a child how to make friends, and remembered how to do it in order to befriend my wife, win her over, and become her husband, but it seems too much work to do it with other people.

So, aside from my wife and my acquaintances on social media, I don't have friends. <a title="American Mens' Hidden Crisis: They Need More Friends" href="http://www.salon.com/2013/12/08/american_mens_hidden_crisis_they_need_more_friends/" target="_blank">Recent studies suggest this is typical for American men.</a> For most American men, the need to be manly precludes emotional, non-sexual intimacy between men; they have bros, or buddies, but not friends.

I don't have buddies, either. I tell myself and other people that I don't want any, but I occasionally wonder if I'm just bullshitting myself. I do that, you know. I lie to myself, just like most people do.

A truth (because it would be dishonest to write "_the_ truth") is that I suspect that what we call real friendship, friendship between people who talk face-to-face and do things together without networked computers to mediate their interaction, may be more trouble than it's worth. Real-life friendship means taking the bad with the good. It means caring for people when they're _not_ interesting or charming. It means loving them when they seem to be doing their damnedest to be _unloveable_.

I can do this for my wife Catherine, but I still haven't convinced myself that it's worth doing for other people. I once read of an America where you could spend your entire adult life working for a business, befriend your coworkers, and put down roots. That America doesn't exist any longer. We're a nation of mercenaries, with no job security, and therefore no ties to the towns in which we live, to our coworkers, or to our neighbors.

Friendship, loyalty, ties to home and a community rooted in geography instead of common interests &#8212; these are not virtues in modern America. They don't pay the bills, and if you lose your job and have to find another, they can make doing so harder. We made vices of virtues, and made virtues of vices like insularity, self-involvement, and indifference to others.

I don't know what to do to fix this. I've lived without friends other than my wife for so long that the thought of seeking out other friends without the network seems a pointless distraction from my responsibilities as an American man (keep the capitalist machine running by working for other people and spending money) and my dreams of improving as a writer, publishing more books, and hopefully retiring to write full-time.

Despite this, I remain aware that without _people_, I have no hope of ever quitting my day job. It's _people_ who are going to buy my books, and they won't know me or have a reason to care if I don't reach out to them. But damn me if it isn't hard after years of being content to be alone with my wife, nice and safe in my house on Mars.

Am I the only one who struggles with the suspicion that I might be harming myself by seeking the solitude necessary to write? Am I the only one who suspects that there's something fundamentally wrong with American culture that makes it difficult for adults to form meaningful non-sexual/non-romantic relationships with each other? What do you think?

## References

The whole "My House on Mars" bit comes from Dutch prog metal supergroup Ayreon:

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A6FG3RAB8nK0AF1wEI2w5r1" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

---

Post inspired by: [Sundog Lit Blog: "Best Worst Year: Episode 53 (Or, Life on Mars)"](http://sundoglitblog.wordpress.com/2014/02/13/best-worst-year-episode-53-or-life-on-mars/).
