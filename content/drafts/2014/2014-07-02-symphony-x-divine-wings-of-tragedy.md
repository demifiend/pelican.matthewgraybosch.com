---
title: "Symphony X: &quot;The Divine Wings of Tragedy&quot;"
excerpt: "&quot;...the Prince of Truth, now the Bringer of War...&quot;"
header:
  image: symphony-x-the-divine-wings-of-tragedy.jpg
  teaser: symphony-x-the-divine-wings-of-tragedy.jpg
date: 2014-07-02T09:00:36+00:00
categories:
  - Music
tags:
  - Bach
  - epic
  - Holst
  - Mars
  - Milton
  - Paradise Lost
  - heavy metal
  - progressive rock
  - progressive metal
  - Rebellion
  - Satan
  - Symphony X
  - The Divine Wings of Tragedy
  - Rush
  - Venom
---
Want to know what happens when a progressive metal band from New Jersey combines Bach's [Mass in B minor](https://www.youtube.com/watch?v=7F7TVM8m95Y), Holst's [Mars: the Bringer of War](https://www.youtube.com/watch?v=L0bcRCCg01I), and the character of [Satan from Milton's *Paradise Lost*](http://en.wikipedia.org/wiki/Paradise_Lost#Satan)?

You get something more epic than Rush's ["2112"](https://www.youtube.com/watch?v=AZm1_jtY1SQ) (which is difficult to achieve) or Venom's ["At War with Satan"](https://www.youtube.com/watch?v=DnDvgGYfKaE) (not so difficult, unfortunately). You get **"The Divine Wings of Tragedy"**.

<iframe width="420" height="315" src="https://www.youtube.com/embed/S2CISHqGzCg" frameborder="0" allowfullscreen></iframe>

---

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A1KAS6808SALsQ9fMc8Ygcy" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

---

I can't say the song applies to any particular character in _Starbreaker_, but defiance is one of the series' general themes.
