---
title: Newbie Writers Podcast 132
categories:
  - Project News
tags:
  - A Matter of Vengeance
  - Epic Illustrated
  - imaginos
  - inspiration
  - Marvel
  - Morgan Storm-Mane
  - morgan stormrider
  - Newbie Writers
  - Podcast
  - starbreaker
  - without bloodshed
---
I really need to work on public speaking if I'm going to keep doing podcasts like <a title="the Newbie Writers Podcast" href="http://www.newbiewriters.com/category/newbie-writers-podcast/" target="_blank">Newbie Writers</a>. I have no clue if my appearance is a worthy successor to last week's guest, Mignon Fogarty of Grammar Girl. If I sounded awkward, it's because this is pretty much my first appearance on a podcast.

## Check Out the Podcast on Youtube

Damien already has a rough cut of the podcast on Youtube via Hangouts on Air. Thank goodness it was audio only.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Ye gods, I sound like a pretentious wanker when recorded.

Awkwardness aside, I enjoyed the experience. I hope Damien and Catharine enjoyed my company as well. We talked about the origins of _Starbreaker_, where I get some of my characters' names,  and what it might have been like if Sauron and Lord Foul the Despiser had wives.

You can check it out at <a title="Newbie Writers: Starbreaker with Matthew Graybosch" href="www.newbiewriters.com/2014/05/17/episode-132-starbreaker-with-matthew-graybosch/" target="_blank">newbiewriters.com</a> if you want to download to your favorite device instead of dealing with YouTube.

## Cover Versions

We also talked about the cover for _Without Bloodshed_.<figure id="attachment_48" style="width: 474px" class="wp-caption aligncenter">

[<img class="size-large wp-image-48" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/without-bloodshed-final-cover-681x1024.jpg?resize=474%2C712" alt="Artwork by Ricky Gunawan" data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/without-bloodshed-final-cover.jpg)<figcaption class="wp-caption-text">Artwork by Ricky Gunawan</figcaption></figure>

Of course, I couldn't discuss that actual cover by <a title="Ricky Gunawan" href="http://ricky-gunawan.daportfolio.com/" target="_blank">Ricky Gunawan</a> without also mentioning the cover <a title="Harvey Bunda - Advocate for the Arts" href="http://www.harveybunda.com/" target="_blank">Harvey Bunda</a> submitted. I think it's a badass piece, even if it might not have worked as thumbnail.<figure id="attachment_499" style="width: 474px" class="wp-caption aligncenter">

[<img class="size-large wp-image-499" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/without-bloodshed-alternate-cover-662x1024.png?resize=474%2C733" alt="Morgan Stormrider and Naomi Bradleigh - Alternate Without Bloodshed cover by Harvey Bunda" data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/without-bloodshed-alternate-cover.png)<figcaption class="wp-caption-text">Morgan Stormrider and Naomi Bradleigh &#8211; Alternate Without Bloodshed cover by Harvey Bunda</figcaption></figure>

Damien wanted to know why I haven't gotten the tattoo from the production cover as a back piece. The honest answer is that I'm not sure I can justify the expense. If I had money to spare, I'd rather buy a new laptop instead of getting a tattoo on my back where I won't be able to see it.

## Echoes from the Past

While discussing character names, I mentioned a story by Archie Goodwin and Rich Buckler called "A Matter of Vengeance", which Marvel published in _Epic Illustrated_ — which proved a worthy competitor to _Heavy Metal_ for a few years. I had forgotten that this tale of swash-buckling anthromorphic cat people had been the cover story for the April 1985 issue.<figure style="width: 400px" class="wp-caption aligncenter">

[<img src="http://i2.wp.com/files1.comics.org//img/gcd/covers_by_id/21/w400/21748.jpg?resize=400%2C533" alt="Epic Illustrated, April 1985. Cover art by Stephen Hickman" data-recalc-dims="1" />](http://www.comics.org/issue/39901/)<figcaption class="wp-caption-text">Epic Illustrated, April 1985. Cover art by Stephen Hickman</figcaption></figure>

I'm sure I thought this artwork was the coolest thing ever when I was a kid back in 1985. Looking at it now, I can't help but think that whatever the woman in the foreground is wearing can't _possibly_ be comfortable or practical. I do remember her being the viewpoint character, despite Hickman choosing to show us her ass and some side-boob instead of her face.

It's funny how this imagery mutated and made its way into _Starbreaker_, though. Funny how a cover-story from an 1980s illustrated sci-fi/fantasy magazine burrowed into my subconscious. I suspect most writers could tell similar stories about odd inspirations.
