---
title: "Post-IPO Facebook: Echoing the Dot-Com Boom"
excerpt: "All the shit Facebook did to ruin the user experience? I saw it coming back in 2012. Here's why."
header:
  image: facebook-dislike-button.jpg
  teaser: facebook-dislike-button.jpg
categories:
  - Social Media
  - Longform
tags:
  - double dipping
  - Facebook
  - informed consent
  - IPO
  - Wall St
  - capitalism ruins everything
  - James Barraford
  - Media Tapper
  - Reachocalypse
  - social network
  - advertising
  - sociological experiments
---
Back in 2012 I wrote about Facebook's initial public offering for James Barraford's site, Media Tapper, which is now defunct. Considering recent changes to Facebook like [Reachocalypse](http://memeburn.com/2014/04/reachocalypse-is-facebook-really-trying-to-screw-your-brand-pages-reach/), and their recently revealed shenanigans involving [sociological experiments without informed consent](http://www.theguardian.com/technology/2014/jun/29/facebook-users-emotions-news-feeds), I think I was correct in predicting that going public wouldn't be good for Facebook's users. But keep reading, and see for yourself.

## The Facebook IPO: An Echo of the Dotcom Bubble

Many Media Tapper readers will remember the Dotcom Boom, that brief era at the end of the 1990s when venture capitalists and investors happily threw money at every start-up with a website and an idea, only to see that money disappear when the vast majority of these start-ups proved bereft of a semblance of a viable business model or of any prospect of ever turning a profit. The founders of these start-ups often made money, but their employees found themselves holding worthless stock options.

Despite this, Wall Street investment banks like Goldman Sachs and the now-defunct Lehman Brothers remained happy to underwrite initial public offerings (IPOs) for internet start-ups for a while, turning the Dotcom Boom into a bust which triggered a recession at the opening of the twenty-first century. Why? Did they not know what they were doing to the economy?

Proving that the people underwriting the IPOs of the failed dotcom start-ups knew what they were doing would in all probability require a joint investigation by the Securities and Exchange Commission (SEC) and the Federal Bureau of Investigation (FBI). While it could be argued that such an investigation, as well as prosecutions under applicable Federal law, have been overdue since the crash of 2008, such an argument is best left to a separate article. Instead, let’s assume for the purposes of this article that the Wall Street investment banks enabled the initial public offerings of dozens of start-ups which should have been dismissed as non-starters the simplest possible reason: regardless of the eventual success or failure of the start up, the bank underwriting the IPO profited from its involvement.

## Enter Facebook

From the ashes of the dotcom bust came Facebook, which differentiated itself from competitor MySpace by initially restricting its membership to college students and offering a simple, clean standard interface. This interface proved preferable to older MySpace users as its majority demographic, high school students, took advantage of the ability to customize profiles using HTML and JavaScript to turn MySpace into the second coming of GeoCities. MySpace has since become irrelevant, as founder Tom Anderson sold the site to Rupert Murdoch’s News Corporation, retired, and now hangs out on Google+ from time to time.

With MySpace a bad memory, Facebook competed against Twitter by providing public application programming interfaces (APIs) which allowed developers to create applications and games to extend Facebook and make it more attractive to potential users. As potential users became actual users, Facebook’s owners sought a means to make the social network profitable, and thus attractive to outside investment.

The means to profitability Facebook CEO Mark Zuckerberg chose was the monetization of the data fed into Facebook by its hundreds of millions of users. Everything you tell Facebook will be used to target ‘relevant’ ads to you. This is not a viable business model if one seeks to build a sustainable business with a potential for long-term profit, but it’s perfect for the short-term gains which can come from a successful initial public offering.

## A Twentieth Century Business Model

At this point, an explanation of the defects in Facebook’s business model is in order. The simplest possible explanation is that Facebook’s business model is the same business model used by TV broadcasters like CBS, NBC, ABC, and Fox: the users and viewers are not _customers_, but _products_. Facebook sells its customers’ data and attention to advertisers.

This business model is not sustainable because it depends on the assumption that the users will continue to remain suckers indefinitely. It is intrusive, and can only be pushed so far before provoking the users’ anger and drawing the attention of regulatory agencies. Watchdog groups [in Germany](http://www.pcworld.com/businesscenter/article/251430/facebook_loses_german_privacy_lawsuit_over_friend_finder_personal_data.html) have already criticized Facebook’s practices regarding its attitude towards users’ privacy on numerous occasions.

In fairness to Mr. Zuckerberg, it must be admitted that dependence on advertising continues to sustain broadcast television twelve years into the twenty-first century. However, ad revenue for broadcasters [continues to decline](http://adage.com/article/mediaworks/tv-ad-revenue-decline-year/142244/). This trend will continue for the following reasons:

  * Broadcast TV must compete with cable, movies, video games, books, and other entertainments for attention.
  * People are growing used to alternate means of viewing TV programming, such as viewing on Hulu, Netflix streaming, or simply ignoring copyright and downloading bootleg copies of shows via BitTorrent.
  * Advertisers remain fixated on targeting the 18–35 demographic, an age group which will become less valuable for at the next twenty years at a minimum because many Americans in that range will be reduced to a state of indentured servitude by student loan debt.
  * Satellite providers like Dish Network differentiate themselves from other providers by offering tech to [skip ads](http://www.latimes.com/entertainment/envelope/cotown/la-et-ct-cbs-blasts-dish-20120516,0,2439710.story).

If broadcast TV cannot depend on ad revenue, how can Facebook hope to do so when plugins like [AdBlock Plus](http://adblockplus.org/en/) are available for popular browsers like Google Chrome and Mozilla Firefox, and provide easy and automatic ad blocking across the entire internet? How can Facebook hope to make money by advertising when major corporations like General Motors [pull their ad campaigns](http://www.forbes.com/sites/joannmuller/2012/05/15/gm-says-facebook-ads-dont-work-pulls-10-million-account/)?

Again, fairness to Mr. Zuckerberg requires mentioning that GM is also reconsidering its practice of spending millions of dollars to promote its cars during the [Super Bowl](http://www.forbes.com/sites/michelinemaynard/2012/05/21/for-gm-chrysler-set-the-super-bowl-bar-too-high/), and that one of the reasons GM’s ads on Facebook don’t work is that the same people who frequent Facebook – women and people between the ages of 23 and 35 – don’t buy GM cars, if they buy cars at all.

## Facebook by the Numbers

Bearing in mind the fact that advertising is likely to prove a dead end for Facebook as well as broadcast TV, and that even Google is not wholly dependent on advertising for revenues, it is time to consider the numbers. Unless otherwise noted, all information presented concerning Facebook’s finances will come from its [Form S1 Registration Statement](http://www.sec.gov/Archives/edgar/data/1326801/000119312512034517/d287954ds1.htm). All dollar amounts in millions or billions of dollars will be rounded to the nearest tenth, unless rounding to the nearest integer is more appropriate.

According to this form, Facebook seeks to raise $5 billion. Their [market capitalization](http://www.sec.gov/Archives/edgar/data/1326801/000119312512034517/d287954ds1.htm#toc287954_6) as of filing was $4.9 billion. Their 2011 [revenue](http://www.sec.gov/Archives/edgar/data/1326801/000119312512034517/d287954ds1.htm#toc287954_8) was $3.7 billion, of which 12% came from their partnership with Zynga, for whom Facebook handles in-game purchases for users playing games like _Mafia Wars_ and _Farmville_. Their total costs/expenses for 2011 added up to $2 billion. After other expenses, and income taxes, Facebook turned a billion dollar profit in 2011. While a billion dollars in profit is more than respectable, Facebook cannot guarantee that it will earn similar profits in 2012 and beyond, let alone guarantee that it will earn bigger profits. The corporation acknowledges this fact in the 22-page section on [risk factors](http://www.sec.gov/Archives/edgar/data/1326801/000119312512034517/d287954ds1.htm#toc287954_2).

These numbers should be compared with those of similar companies, such as [Google](http://www.nasdaq.com/symbol/goog/financials) and [Apple](http://www.nasdaq.com/symbol/aapl/financials), neither of which have Facebook’s revenues or profits, despite dealing in tangible goods and services. What these corporations have going for them is a stock price in the hundreds of dollars based in part on the perception that both are solid corporations capable of long-term profitablity. People are likely to continue to buy text ads on Google and phones running the Android operating system. People are likely to continue to buy Macs, iOS devices, and use the iTunes store to buy music and movies.

Can the same be said for Facebook? Advertisers and marketers regard Google+ as a "ghost town" because its users tend not to engage with brands. However, GM's removal of paid ads from Facebook suggests that despite being the reigning champion of social networks outside of China, it too has problems with users’ brand engagement. Therefore, the initial stock price of $38 per share does not make sense. Nor is that price sustainable, as Facebook stock continues to slide downward in its third day of public trading.

## Pump and Dump

If Facebook’s business model is obsolete, and its profits unsustainable in the long term, why did Facebook stock open at $38 per share, instead of at a lower price? Is Facebook really worth over $100 billion?

Explaining Facebook’s inflated valuation and stock price requires an understanding of the underwriting process integral to an initial public offering. When a corporation wishes to go public, it does not sell shares directly to the public. That is not how things are done on Wall Street. Instead, the corporation engages the services of one or more investment banks, and agrees to sell these banks enough shares of the company to raise the amount of money desired. The underwriters then sell the stock they’ve purchased on the stock exchange in order to profit from their involvement in the IPO.

In Facebook’s case, the corporation wanted to raise $5–10 billion, and sold shares to the following investment banks:

  * Morgan Stanley & Co. LLC
  * J.P. Morgan Securities LLC
  * Goldman, Sachs & Co.
  * Merrill Lynch, Pierce, Fenner & Smith Incorporated
  * Barclays Capital Inc.
  * Allen & Company LLC

Each of these names should ring alarm bells. During the 2008 crash, Morgan Stanley borrowed $107.3 billion from the Federal Reserve, and has been involved in several [controversies and lawsuits](http://en.wikipedia.org/wiki/Morgan_Stanley#Controversies_and_lawsuits) involving fraud, sexual harassment, and assorted regulatory infractions. J.P. Morgan was given $30 billion by the Federal Reserve to buy out Bear Stearns at $2/share. Goldman Sachs has been involved in [too many controversies](http://en.wikipedia.org/wiki/Goldman_Sachs#Controversies) to recount in this article, and is the subject of a scathing article by Matt Taibbi in [Rolling Stone](http://www.rollingstone.com/politics/news/the-people-vs-goldman-sachs-20110511). Merrill Lynch was involved in the subprime mortgage crisis, and was purchased by Bank of America in 2009. While Barclays is not directly implicated in the 2008 crash, it did purchase the now-defunct Lehman Brothers, and is embroiled in [controversies](http://en.wikipedia.org/wiki/Barclays#Controversies) of its own, such as its provision of financial support to Zimbabwean tyrant Robert Mugabe’s regime, links to the arms trade, and attempts to avoid paying taxes in its home country, the United Kingdom. A Google search on [boutique investment bank](http://en.wikipedia.org/wiki/Allen_%26_Company) Allen & Company did not turn up any dirt, making it the only underwriter of the six named above to have a semblance of a clean reputation.

Considering the history of five of the six investment banks acting as underwriters in the Facebook initial public offering, and the manner in which underwriting works, the corporation’s irrationally high valuation begins to make sense. The underwriters need to sell their shares in Facebook at a profit in order to justify their involvement in the IPO. If they had attempted to manipulate the expectations of the investing public to inflate the price of the stock at IPO, they could be accused of running a pump-and-dump scam.

Instead, lead underwriters JP Morgan, Morgan Stanley, and Goldman Sachs appear to have [revised their estimates of Facebook’s projected earnings](http://finance.yahoo.com/blogs/daily-ticker/facebook-bankers-secretly-cut-facebook-revenue-estimates-middle-133648905.html) just before the IPO, without releasing that information to the public. Compared to the crimes and misdemeanours in which these investment banks have already been implicated, what’s a bit of [insider trading](http://en.wikipedia.org/wiki/Insider_trading)? After all, the underwriters will have made their profit, even if they had to [buy back shares](http://online.wsj.com/article/SB10001424052702303448404577411903118364314.html) on the first day of trading in order to keep the stock price from dropping below the IPO value, and it’s just those who bought Facebook stock without having access to the revised earnings estimates who got the shaft.

## A Better Tomorrow

As Facebook’s stock price continues to slide downward and investors demand growth and profits, CEO Mark Zuckerberg will face hard decisions. Though Facebook has earned a billion dollars of profit in 2011, most of that profit came from advertising and other means of exploiting users and their data. If Facebook is to provide sustainable growth and profitability, it must find a way to turn its users into loyal customers, instead of exploiting them.

Facebook’s partnership with Zynga, in which it gets a cut of all purchases users make while playing Zynga games on Facebook, is one potential avenue towards a sustainable business model. However, it will not prove sufficient, as Zynga can only make so much money from casual players, and hardcore gamers speak Zynga’s name in tones which begin at contemptuous. While their filings with the SEC suggest that they mean to expand their payment-processing business, as well as open their own App Store to compete with those run by Google and Apple, there is a more straightforward way to turn Facebook users into Facebook customers: charge them.

Facebook currently has almost a billion active users, which Facebook defines as users who have connected to Facebook within the last thirty days. A hundred and fifty million of these users live in the United States alone. Charging these users a nominal monthly fee, such as a dollar or two, can yield monthly revenues of almost a billion dollars per month, assuming that everybody currently using Facebook finds the service valuable enough to pay for it. That itself is a debatable proposition, but a case for paying for Facebook can be made if Facebook protects its paying customers’ privacy from other corporations and from governments, while allowing the users full control over their data and the means to extract it from Facebook.

Will Facebook attempt to turn its users into customers without trying to have it both ways by charging users for access and exploiting their data? Would it even occur to Mark Zuckerberg to consider such a transition? Would he be able to persuade other stakeholders that this transition is both necessary, and beneficial to the company in the long run? If the answers to these questions are ‘no’, then Facebook may share the fate to which it consigned MySpace, and its IPO may be remembered as an echo of the worst excesses of the Dotcom Bubble.
