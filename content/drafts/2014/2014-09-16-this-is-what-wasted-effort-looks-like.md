---
id: 1344
title: This is What Wasted Effort Looks Like
excerpt: I couldn't bear to just throw this away. So you get to read it.
date: 2014-09-16T20:09:20+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - deleted scenes
  - lunch break
  - outtakes
  - perfectionism
  - self-hatred
  - Starbreaker
  - Silent Clarion
  - wasted effort
---
There's nothing like belting out 500 words on your lunch break, reading them over, and realizing they do nothing for the story. You've pretty much wasted your time if you can't find some other use for the text you just crapped out through your hands.

I had one of those sessions this afternoon while starting Chapter Fourteen of _Silent Clarion_. The following isn't good for anything but an outtake, because it doesn't advance the plot.

* * *

Some people just don't travel well. They sleep poorly in unfamiliar places. They return from their journeys wearier and more haggard than when they started. It's as if they need a vacation from their vacation just to rest.

That normally isn't a problem for me. When I'm ready to sleep, I'm out. Ragnarok could happen in my room, and I'd remain blissfully ignorant unless some idiot dragged me into the fight.

The hard part last night was getting ready to sleep. You might have noticed how tightly wound I was after my encounter with Renfield. That sort of unreasoning lust is a new thing for me, and I'm not sure if it's because Renfield and I both have CPMD, or if it was the way he smelled, or just because I was single and away from home.

Regardless of the cause, I was too restless to sleep. Dawn had begun to lighten the sky before I finally gave up and forced myself out of bed. If I couldn't sleep, I could at least start the day properly with some PT and a long hot shower.

A short, slim, almost feminine-looking man tended the bar as I sat down in the common room to read. He waved me over. "Hi. You must be Naomi. Bruce told me about you last night."

He was actually rather pretty, and probably would be even without makeup. I offered my hand. "I hope Bruce didn't say anything unflattering. And you are?"

"Dick Halford. Bruce is my husband. Care for some coffee?"

"Yes, please. Black will do." I took a small sip, wary of a scald, but it was the perfect temperature. "Thanks. Do you have a breakfast menu?"

Dick shook his head. "No, but we do serve breakfast. You can have anything you like as long as you want bacon and eggs."

How fortunate that I do, then! "That sounds perfect. This coffee is wonderful, by the way."

"Thanks! I'll have Bruce put together a plate for you." He returned a minute later with the sort of hearty breakfast Leonidas most likely had in mind when he told his Spartans to eat well because they'd probably dine in Hell.

The eggs were fluffy and just a little buttery, and the peppers and onions gave them just enough kick. The bacon was just crispy enough, with the right amount of fat. By the time I finished, I was thoroughly content despite a bit of bacon remaining on my plate. I tore it in half, meaning to share it with the kittens if I could find them.

"Behind you." Dick's voice was soft, as if he were trying to avoid disturbing somebody.

I turned, and found Dante sneaking toward my plate. Reaching out with a paw, he swiped both scraps of bacon from the plate and began to chow down while emitting little growls resembling &#8216;om nom nom nom'. I scratched behind his little ears as he ate. "You greedy little kit. Now Virgil won't get any."

"Don't worry about Virgil. People food doesn't do it for him." Dick took my plate and handed me a chit to sign so that I could pay for my day's meals at dinner.

* * *

By the way, I envy the people who can say, "Screw it. I'm gonna write 10,000 words today, and I don't care if they're shit. I'll fix it in the next draft." I'm not one of those people, and I hate myself for it. If you're one of those people, I hate you too. I have plenty of hate to spare, and a shortage of deserving targets.

I need to get it right the first time, because the slightest mistake in an early chapter will result in my being utterly blocked later on. You could say it's because I'm a perfectionist who needs therapy. I blame my day job.

Programming is one of those jobs where a [high-functioning perfectionist](http://stevenbenner.com/2010/07/the-5-types-of-programmers/) can be an asset. To use Steven Benner's types, I'm a mix between the Duct-Tape Programmer and the OCD Perfectionist. I'd probably be better off if I was the Half-Assed Programmer.
