---
id: 129
title: Big Kitty Yawn
date: 2014-03-06T20:22:24+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=129
permalink: /2014/03/big-kitty-yawn/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapWP:
  - 's:160:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:3:"131";s:5:"pDate";s:19:"2014-07-15 11:03:40";}}";'
bitly_link:
  - http://bit.ly/1LDQbSR
bitly_link_twitter:
  - http://bit.ly/1LDQbSR
bitly_link_facebook:
  - http://bit.ly/1LDQbSR
bitly_link_linkedIn:
  - http://bit.ly/1LDQbSR
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1LDQbSR
bitly_link_reddit:
  - http://bit.ly/1LDQbSR
bitly_link_stumbleupon:
  - http://bit.ly/1LDQbSR
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - kitty
  - orison
  - sleepy
  - tired
  - words of radiance
  - yawn
format: video
---
I feel like I should write yet another post today, maybe on the difference between fantasy where magic is treated as a preternatural phenomenon and fantasy where magic is treated as a supernatural phenomenon.

Instead, I feel like this Maine Coon kitty.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

I think I'll curl up in bed and finish Daniel Swensen's _Orison_ so I don't have to feel guilty about digging into Brandon Sanderson's _Words of Radiance._

Good night. I guess I managed one last post after all.

&nbsp;