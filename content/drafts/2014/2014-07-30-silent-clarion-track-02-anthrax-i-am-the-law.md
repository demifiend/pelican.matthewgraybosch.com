---
title: "*Silent Clarion*, Track 02: &quot;I Am the Law&quot; by Anthrax"
excerpt: "Check out chapter 2 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. What's the best way to cope with a break-up? How about taking it out on somebody who had it coming anyway?"
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Anthrax
    - I Am The Law
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
I woke up nicely rested. Determined to make a fresh start, I immediately stripped my bed and changed the bedclothes. I opened all of the windows in my flat to exorcise John's scent, then set a small pot of coffee to brewing, and fixed a breakfast of scrambled eggs and bacon.

One of the building's resident cats took advantage of the open windows to come visit. Winston wound about my legs and purred as I ate, hoping for a fatty scrap from my bacon or a bit of egg. I let him lick the plate when I was done, and scratched behind his ears.

I missed having a cat of my own, but my responsibilities precluded pets at the moment. Because I might be given a mission that keeps me from home for days at a time without notice, I felt uncomfortable asking one of my neighbors to watch over a cat for me when I could not be relied upon to reciprocate.

Winston began washing himself, so I retrieved my plate and set about cleaning up after myself. I learned the importance of keeping my kitchen clean the hard way. A long mission could make a science experiment out of a dirty sink.

While I cleaned, I checked the messages on my implant, and deleted one from John without reading it. I then set my implant to prevent all further contact between us. It wasn't personal; it's SOP whenever I break up with somebody.

Some of you might think I'm a complete bitch for severing all contact with former lovers, but I don't give a damn what you think. I'm looking out for myself first, last, and always.

If the message had been an entreaty begging me to take him back, I might weaken and grant his request. Worse, I might drunk-dial the son of a bitch and tell him to tell his wife he needs to work late. Worst of all, we might try to continue as friends.

My friend Jacqueline seems to have mastered the trick of remaining friends with her exes. I should ask her for pointers. God knows I give her plenty of help with her swordplay. Fellow Adversaries and all that.

The most recent message in my queue was hers. She must have sent it while I was eating, but the subject didn't suggest it was particularly urgent. I read my mother's message first. John called my parents and asked them to appeal on his behalf, but they told him — and I quote — "to stop being such a manipulative little prat and fuck off".

I'm not nearly as good a daughter to them as they've been parents to me. They didn't let the fact of my being a foster child stop them from loving me as their own, but the knowledge that they were not my 'true' parents always drove me to keep a certain distance. Regardless, this deserved a proper call, not just a text message.

My mother must have expected me to call early. "Did you want to talk about John?"

"Not really. I wanted to thank you for the way you handled him last night."

"You'll find the right person someday." I rolled my eyes at the sentiment. My parents are romantics, especially Mum. She wants me to have the love she experienced. Maybe I will, someday, but you'll pardon my cynicism if I harbor the suspicion that my parents weren't untouched innocents when they met.

Rather than argue, I changed the subject. "How is everybody? Is Nathan still seeing that rugby player? Charlotte, right?"

"Oh, they're so happy together! Why not come and visit? We haven't seen you since you took the oath." Mum lowered her voice. "Howell worries about you. I keep telling him you'll be fine on your own, but you know how he is."

I couldn't help but laugh a little. Because of a medical condition so rare he's the only living person to exhibit it, my father is the envy of kings and magnates throughout human history. He can only father sons, because he only produces sperm with Y chromosomes. Unfortunately, he wanted a daughter or two. They fostered me, hoping for a princess, and got an Amazon. "I'll come visit. I have time off coming, so I'll ask about using some today."

"Really? You mean it, Naomi?"

I meant it. I could use some time off, and the company would keep me from getting too lonely. "Of course, Mum. I'll call again once I've made the arrangements, but I have to report in soon."

I was already likely to be late, so I skipped my morning shower. I had showered after the gym so I didn't really need it, anyway. Nor did I bother to do anything fancy with my hair. I just braided it into a tight cable while riding the Tube. It showed my ears, but unless I wore sunglasses all the time like a Hollywood Vampire, my kitty eyes would be hard to miss.

Jacqueline was there when I got off at Victoria Station. "Oi! Nims! Didn't you get my message?"

I ran up the stairs to meet her, and accepted a quick hug. "Sorry. I meant to check it after I called my mother, but the time ran away from me."

"No worries. I just wanted to tell you we got a job over in the East End."

Jacqueline and I have been to the East End before. I suggested the most likely recipient of our attentions based on prior experience. "MEPOL?"

"Yeah. Religious discrimination instead of racism this time."

I shook my head, and began to suspect we'd have to do a purge. The Phoenix Society can't tolerate the existence of city police who use their religious beliefs or racial prejudices as an excuse to abuse their authority. "Have we met the accused before?"

"Nah." Jacqueline grabbed a doughnut from a stand as we walked to the train that would take us to the East End. She offered me half, but I politely refused. I'm not diabetic, but having CPMD makes eating sugary treats other than small quantities of fruit a bad idea. I usually spend the day after my birthday sick, because it would break Mum's heart if I told her she can't make one of her cakes for me like she does for my brothers.

Jacqueline continued to talk around a mouthful of doughnut. "MEPOL booted the last set of arseholes. This is a fresh batch. They've got shiny new badges, and they're convinced that since monotheists used to persecute everybody else, and allegedly caused Nationfall to boot, they need to be kept in their place."

"Wonderful." I sighed, disappointed that my first task today would prove so mundane. "I guess nobody thought to mention that turnabout ceases to be fair play once you put on the uniform. What level of force is authorized?" I wore my sword and pistol, but I didn't want to dirty my blade or waste ammo on a few bullies.

"Less-than-lethal, and only in self-defense." Jacqueline chuckled. "Though getting some rebar and going all Vlad Tepes on their asses would certainly send a message."

I imagined a few dozen policemen impaled on four meter lengths of rebar and left for the ravens. For a moment I could see it, as real as day, and I shuddered. "I'm not convinced that's a message we want to send."

We strode into the MEPOL precinct as if we owned the place. Jacqueline hung back a bit, her hand on her sword. The desk sergeant looked up from his terminal, and his face fell. "Fuck me. It's you lot again."

"Did you miss us?" I leaned over his desk. "I'm no happier to be here than you are to see me. I forwarded a list of names to you. Have you gathered them?"

The desk sergeant nodded. "Yes, Adversary, but the Chief Inspector isn't happy."

"Excellent." I smiled, partly at his confused expression. "Misery loves company."

Chief Inspector Wallace reminded me of a weasel, with his long, narrow body and his long, narrow face. He glared at us while straightening his tie. "I can't believe you're bothering with this. They're just demon worshipers."

Oh, lovely. The Chief Inspector is a maltheist who thinks all forms of religious faith are demon worship. While he has a right to hold any ignorant notion he likes, his inability to keep his prejudices to himself while acting in an official capacity makes his opinions our concern. The Society frowns on such bias, so I smiled at Jacqueline. "I think we found the root of the problem. Arrest him."

"Right." Jacqueline drew her pistol just in case Wallace felt like doing something stupid, and recited his rights. We left the desk sergeant with the unenviable task of sticking his former superior in a cell until the Society could send a vehicle to collect him.

We found a dozen constables grumbling in the conference room. One of them made to grab my arse, but I saw it coming and left him with a handful of nothing to fantasize about.

I stared at the men. All of them were pale, and stared back at me with hard, cruel eyes. "I understand you've gotten into your heads that you have the right to harass Christians, Muslims, and other monotheists outside their places of worship for no other reason than that they're devout."

"The hell do you care? They're just—"

"They're human beings, and have the same rights as everybody else." Without realizing it, I drew my sword. Rather than put it away and look stupid, I brandished it. "I swear to God, if I have to come back here because you bigoted sons of syphilitic bitches can't refrain from disgracing your uniforms by harassing people who exercise their rights without violating the rights of others, I will bring enough Adversaries to hold you down while Jacqueline and I tattoo the Universal Declaration of Individual Rights into your backs so you can study it while buggering each other in the locker room."

While I had their attention, I pointed my sword at the constable who tried to grab a piece of me. "Also, the next one of you pigs that tries laying a hand on me is going to lose it. Any questions?"

A man in the back raised his hand. "Isn't it child abuse to take children to religious services? You know, forced indoctrination?"

Jacqueline answered before I could. "Children who think their parents have violated their right to freedom of conscience may contact the Phoenix Society. You're law enforcement officers. Stick to your mission parameters, and leave ours to us."

I lifted an empty cardboard box. "One last thing before you gentlemen leave. Hand over your badges and service gladii. As this is you lot's first offense, it's a two-week unpaid leave. Second one gets you a three-month suspension. The third offense is your last."

I cut off the grumbling. "Another word of complaint and I will consult the Society's legal department about compelling you to spend a week with a devout family, including attending services with them, so you can see for yourselves they're as human as you. Any questions?"

---

### This Week's Theme Song

&quot;I Am the Law&quot; by Anthrax, from *Among the Living*

{% youtube 9rVFi6qkPHE %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
