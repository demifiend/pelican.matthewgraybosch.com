---
id: 2442
title: 'RJ Blain&#039;s Fantasy Book Watch'
date: 2014-03-04T18:15:10+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=7
permalink: /2014/03/rj-blains-fantasy-book-watch/
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapWP:
  - 's:159:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:2:"60";s:5:"pDate";s:19:"2014-07-12 10:51:56";}}";'
snapLJ:
  - 's:206:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0lj";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:48:"http://matthewgraybosc.livejournal.com/4381.html";s:5:"pDate";s:19:"2014-07-12 19:19:13";}}";'
snapGP:
  - 's:388:"a:2:{i:3;a:5:{s:12:"rpstPostIncl";s:7:"nxsi3gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"114546293185560266851/posts/GHiNsnQgpMt";s:5:"pDate";s:19:"2014-07-14 12:01:15";}i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"103251633033550231172/posts/jcoCBZzhA1A";s:5:"pDate";s:19:"2014-07-12 12:00:56";}}";'
bitly_link_googlePlus:
  - http://bit.ly/1N0mbOo
bitly_link_twitter:
  - http://bit.ly/1N0mbOo
bitly_link_facebook:
  - http://bit.ly/1N0mbOo
bitly_link_linkedIn:
  - http://bit.ly/1N0mbOo
sw_cache_timestamp:
  - 401678
bitly_link_tumblr:
  - http://bit.ly/1N0mbOo
bitly_link_reddit:
  - http://bit.ly/1N0mbOo
bitly_link_stumbleupon:
  - http://bit.ly/1N0mbOo
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - book blog
  - fantasy
  - links
format: link
---
A friend of mine, fantasy novelist RJ Blain (author of _The Eye of God_ and _Storm Without End_), has a new fantasy book blog called **Fantasy Book Watch** at <a title="Fantasy Book Watch" href="http://fantasybookwatch.com/" target="_blank">http://fantasybookwatch.com/</a>. She focuses on new fantasy releases by indie authors, small presses, and the corporate giants.