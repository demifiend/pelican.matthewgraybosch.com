---
title: Worlds Worth Fighting For
excerpt: "I'm going to give you the straight dope about why I write science fantasy: I don't like the real world, and I don't know how to fix it."
date: 2014-04-07T13:10:14+00:00
category: Personal
header:
  image: genesis-land-of-confusion.png
  teaser: genesis-land-of-confusion.png
tags:
  - a world worth fighting for
  - a world worth living in
  - big ideas
  - big stakes
  - big themes
  - doing epic shit
  - epic
  - epic fantasy
  - fantasy
  - Genesis
  - individual action
  - individualism
  - Land of Confusion
  - romanticism
  - science fiction
  - speculative fiction
---
Indie author Ryan Toxopeus got roped into a blogging chain in which you're supposed to blog about writing on Monday, and tag some other poor bastard(s) to continue the chain. <a title="Ryan Toxopeus: Why Self-Publish?" href="http://ryantoxorants.blogspot.com/2014/03/why-self-publish.html" target="_blank">Last week, he tagged me.</a> I was thinking of doing one about Romanticism in speculative fiction, but I feared getting sidetracked by the need to explain Romanticism to people who don't geek out over literature and art. I'm not qualified for that, nor do I get paid enough. Instead, I'm going to give you the straight dope about why I write in this genre: I don't like the real world, and I don't know how to fix it. I wanted to share a vision of a world worth living in, a world worth fighting for, with those who would rather work on fixing the real world than imagine a better one.

Some of you might have read the last paragraph and thought, "Hey, that sounds like a Genesis song from the 80s." You're right. I share my father's opinion concerning Genesis, and they were better before they went commercial and Peter Gabriel left the band, but I retain a certain affection for their "Land of Confusion" single. Like Queensryche's 1988 concept album, _Operation Mindcrime_, it still seems relevant.

<iframe width="420" height="315" src="https://www.youtube.com/embed/QHmH1xQ2Pf4" frameborder="0" allowfullscreen></iframe>

---

Misplaced remnants of youthful idealism aside, I have other reasons for writing speculative fiction. I freely admit I could have stripped all the weird stuff out of <a title="Without Bloodshed" href="http://mybook.to/withoutbloodshed" target="_blank"><em>Without Bloodshed</em></a>. Morgan Stormrider might have been a FBI man with a different name. I could have made Naomi Bradleigh a MI-6 agent (and most likely a pale brunette like her namesake). I could have made the Phoenix Society a conventional NGO fronting for a drug cartel, human trafficking, or illegal arms sales. Morgan, Naomi, and their friends could have chosen to work outside the law and outside their respective chains of command to stop the Phoenix Society.

I didn't want to. I didn't want to write a straight thriller. That wasn't _big enough_ for me. I wanted to take a crack at _big ideas_, _big themes_, and _big stakes_. To be crude: I wanted to write an <a title="Wikipedia: Epic (Genre)" href="http://en.wikipedia.org/wiki/Epic_%28genre%29" target="_blank"><em>epic</em></a> without having to dick around with <a title="Wikipedia: Iambic Pentameter" href="http://en.wikipedia.org/wiki/Iambic_pentameter" target="_blank">iambic pentameter</a> and <a title="Wikipedia: Dactylic Hexameter" href="http://en.wikipedia.org/wiki/Dactylic_hexameter" target="_blank">dactylic hexameter</a>.

Furthermore, I wanted to write a _human_ epic, one that pits all of humanity against a threat that forces them to reconsider their understanding of the world and their place in it while focusing on specific characters. I wanted to take a crack at exploring what it means to _be_ human in an age where it seems likely we will create artificial intelligence and artificial life. I wanted my characters to be able to work _within_ their society, instead of stepping outside it and pushing the same old "<a title="Contrary Brin: Suspicion of Authority" href="http://davidbrin.blogspot.com/2005/09/another-pause-this-time-for-soa.html" target="_blank">suspicion of authority</a>" meme.

I had to get out of the real world and create one of my own, one where the threats my characters face are not mere men, or institutions created by human hands, but powers beyond human capability and intellects alien to our own. I wanted to write about androids unaware of their nature fighting demons from outer space. I wanted to write about libertarian knights in black carbon-fiber armor astride nuclear-powered motorcycles.

I want to write stories that are bigger than life and louder than heavy metal. I want to write about a world that doesn't suck,with people who aren't dull non-entities of interest only to their friends and family. This is why I write speculative fiction. Now it's somebody else's turn. <a title="Michael Shean" href="http://michael-shean.com/" target="_blank">Michael Shean</a>, <a title="Lynda Williams: Reality Skimming" href="http://okalrel.org/reality-skimming/" target="_blank">Lynda Williams</a>, and <a title="Charity Bradford" href="http://charitybradford.com/" target="_blank">Charity Bradford</a>: by these names I summon thee!
