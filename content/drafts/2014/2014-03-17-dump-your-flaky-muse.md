---
id: 261
title: Dump Your Flaky Muse
date: 2014-03-17T07:30:16+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=261
permalink: /2014/03/dump-your-flaky-muse/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1N0CScH
bitly_link_twitter:
  - http://bit.ly/1N0CScH
bitly_link_facebook:
  - http://bit.ly/1N0CScH
bitly_link_linkedIn:
  - http://bit.ly/1N0CScH
sw_cache_timestamp:
  - 401677
bitly_link_tumblr:
  - http://bit.ly/1N0CScH
bitly_link_reddit:
  - http://bit.ly/1N0CScH
bitly_link_stumbleupon:
  - http://bit.ly/1N0CScH
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - flaky
  - inspiration
  - muse
  - procrastination
  - undependable
  - "writer's block"
  - writing
---
Are you sitting on your ass and <a title="Become a Lunch Break Novelist" href="http://www.matthewgraybosch.com/2014/03/10/become-lunch-break-novelist/" target="_blank">wasting your lunch breaks</a> because <a title="Stop Waiting for Inspiration" href="http://writetodone.com/why-you-should-stop-waiting-for-inspiration/" target="_blank">your muse hasn't graced you with inspiration</a>? Are you not getting anything done as a result? I'm not surprised. You're not in control of your own writing life, but have externalized your <a title="PsychCentral: Locus of Control" href="http://psychcentral.com/encyclopedia/2009/locus-of-control/" target="_blank">locus of control</a> to an imaginary figure.

Let me ask you something: if you were dating your muse, and they were undependable, would you put up with their flakiness? If you wouldn't put up with a flaky person while dating, then why would you put up with a flaky muse? Dump your flaky muse.

And don't go looking for a new muse. The whole problem is that you've been thinking that inspiration is some kind of gift with which you must be graced.

Waiting for inspiration isn't going to help you. It's probably what got you into the mess you're in to begin with. You have to <a title="Ralph Waldo Emerson: Self-Reliance" href="http://www.emersoncentral.com/selfreliance.htm" target="_blank">help yourself</a>. Inspiration is not pizza. You can't wait for it to be delivered. You have to go out and get it.<figure style="width: 1344px" class="wp-caption aligncenter">

[<img alt="William-Adolphe Bouguereau (1825-1905) - Inspiration (1898)" src="http://i1.wp.com/upload.wikimedia.org/wikipedia/commons/8/84/William-Adolphe_Bouguereau_%281825-1905%29_-_Inspiration_%281898%29.jpg?resize=840%2C1294" data-recalc-dims="1" />](http://en.wikipedia.org/wiki/Artistic_inspiration)<figcaption class="wp-caption-text">William-Adolphe Bouguereau (1825-1905) &#8211; Inspiration (1898)</figcaption></figure> 

## Tactics for Hunting Down Inspiration

If you've always waited for a muse to deliver inspiration, you might not be used to finding it on your own. I can help you with that by sharing some of my own methods.

### Retrace Your Steps

If you can't figure out what to write next, maybe you should reread what you've already written. Not only will you get better acquainted with your work, but you might discover problems you missed earlier, such as plot holes, inconsistent characterization, or viewpoint problems. At the very least, you can root out typos.

### Be a Great Artist and Steal

To be original, you have to know what your predecessors and contemporaries have done. Read a <a title="Without Bloodshed" href="http://www.matthewgraybosch.com/without-bloodshed/" target="_blank">book</a>. See a movie. Watch a good TV series. Play a <a title="Follow Me as I Go Beyond Death" href="http://www.matthewgraybosch.com/2014/03/13/follow-go-beyond-death/" target="_blank">good video game</a>.

Don't just mindlessly consume content. Think about what you're reading, watching, or playing. Unpack the plot. Analyze the characters. Figure out how the story works. Find the archetypes underlying the characters. Then come back to your own work and use what you learned.

### Use Your Words

Do you have a friend or a partner who supports your work? Try talking over your story with them. If they know your story as well as you do, they can offer a different perspective. They might provide insights into possibilities you might otherwise miss.

### To Sleep, Perchance to Dream

When all else fails, it might be time to pack it in and get some sleep. Have you been getting enough? If not, that might be why you're stuck.

In addition to getting much-needed rest, many writers report finding inspiration in dreams they recall after waking. This hasn't happened to me, however, because I almost never recall my dreams. I'm not convinced I _do_ dream, to be honest.

Regardless of my own experiences. you might get lucky and find what you need to continue your story in your dreams. <a title="15 Famous Books Inspired by Dreams" href="http://www.bachelorsdegreeonline.com/blog/2010/15-famous-books-inspired-by-dreams/" target="_blank">It worked for writers like H. P. Lovecraft, Stephen King, Mary Shelley, Samuel Taylor Coleridge, and Stephenie Meyer.</a>

<a href="https://www.artsy.net/artist/francisco-jose-de-goya-y-lucientes-1" target="_blank">After all, the sleep of reason produces monsters.</a><figure style="width: 2100px" class="wp-caption aligncenter">

[<img alt="Francisco de Goya, c. 1799 - The Sleep of Reason Produces Monsters" src="http://i0.wp.com/upload.wikimedia.org/wikipedia/commons/f/f5/Museo_del_Prado_-_Goya_-_Caprichos_-_No._43_-_El_sue%C3%B1o_de_la_razon_produce_monstruos.jpg?resize=840%2C1181" data-recalc-dims="1" />](http://en.wikipedia.org/wiki/The_Sleep_of_Reason_Produces_Monsters)<figcaption class="wp-caption-text">Francisco de Goya, c. 1799 &#8211; The Sleep of Reason Produces Monsters</figcaption></figure>