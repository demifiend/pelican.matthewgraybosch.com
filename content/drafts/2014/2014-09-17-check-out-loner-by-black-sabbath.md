---
title: "Check out &quot;Loner&quot; by Black Sabbath"
date: 2014-09-17T10:34:06+00:00
header:
  image: black-sabbath-loner.jpg
  teaser: black-sabbath-loner.jpg
categories:
  - Music
tags:
  - 13
  - Black Sabbath
  - depression
  - loneliness
  - Loner
  - theme song
---
"Loner" from Black Sabbath's 2013 album _13_ is pretty much my theme song these days. It's not an anthem, just a fairly accurate description of who I am. I'm my own best friend, and my own worst enemy. And I doubt I'll be happy when I'm dead.

Yeah, I know this post is a bit negative. If I can deal with it as a high-functioning depressive, then so can you.

## YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/hV2ideRjDIk" frameborder="0" allowfullscreen></iframe>

## Spotify

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A1N8HVlOdTXICfovqEHsaQR" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
