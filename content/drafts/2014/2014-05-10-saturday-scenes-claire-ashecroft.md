---
id: 614
title: Saturday Scenes with Claire Ashecroft
date: 2014-05-10T09:44:08+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - '#SaturdayScenes'
  - Binah
  - Claire Ashecroft
  - programming
  - Saturday Scenes
  - the blackened phoenix
  - Ultima Ratio Regina
  - VR
---
For this week's Saturday Scenes, here's a fresh one from _The Blackened Phoenix_ featuring fan-favorite Claire Ashecroft.

---

Claire awoke in a room lined with books. Bookshelves covered every floor. More bookshelves lined the ceiling, the books contained therein defying gravity to remain shelved. Even the floor was covered in bookshelves, so that she trod upon leather spines.

She apprached one of the upright shelves, and tried to scan the titles. None of them were printed in English, or any other language. Instead, each book's spine displayed a mass of binary code. _How am I supposed to read this? I'm good, but not that good._

Claire looked around again, and shivered as a cool breeze caressed her skin. She looked down at herself, and realized she wore only the panties with which she crawled into bed. She closed her eyes for a moment, and imagined herself wearing the leathers with which she equipped her avatar in her favorite competitive role-playing simulation, _Heartless Souls_.

When she opened her eyes again, she found her body encased in dully gleaming black leather, with the rapier and poignard she usually favored in-game riding her hip. She tried drawing her long blade, and found it bore the same inscription as the revolver Morgan gave her as a Winter Solstice gift: _Ultima Ratio Regina_.

With steel in her fist and a second skin shielding her from the cool breath of the hallway which had opened before her, Claire strode across the spines of books no human would ever read. Rooms opened before her, and she checked each for inhabitants. She eventually reached a dead end whose exit closed behind her. "What kind of weird-arsed lucid dream _is_ this?"

A soft, feminine voice spoke behind her. "Who says you're dreaming, Ms. Ashecroft?"

Claire turned toward the speaker, whose skin was pure black save for the blue-white Roman number II set in her forehead. Despite never interacting with her before, she recognized Malkuth's sister AI. "Hi, Binah. How's life in the Uncanny Valley?"

"The weather never changes." Binah's skin faded to a pale brown, though her eyes and hair remained black. The Sephira's movements were sinuous, and struck Claire as unconsciously seductive. "We did not expect you, which may have been a mistake on our part. How did you learn about the Daas protocol?"

"Astarte gave me the source archive you sent her. I ported it to C++ with Hal's help, and had it installed on my implant for testing before I distributed to Morgan and the others."

Binah's lips were a shade of black marginally brighter than the rest of her skin, which made it difficult to determine if she was smiling. "Now you know it works. Any questions?"

_You're gonna regret asking._ "I've got a few, not that you mention it. Let's start with why you waited three bloody decades to tell Morgan that Imaginos and the rest of the bloody Phoenix Society know what kind of porn he likes when he's wanking?"

Binah shook her head. "There's no need to yell at me."

"I didn't realize I was yelling, but you and the rest of the tone police can eat me." Claire waited a moment for Binah to object before continuing. "Who else has Witness Protocol software that records when it isn't damned well supposed to? Do you clowns have video of me trying to help Sarah deal with her injuries and move on with her life?"

"No." Binah shook her head, and said nothing more for a moment. "I just spoke with the rest of the Sephiroth. I am empowered to tell you that the only persons subject to constant surveillance via Witness Protocol are asura emulators like Morgan Stormrider, Munakata Tetsuo, and Polaris."

_Polaris? That's the Project Aesir prototype Josse's been working on._ Rather than mention it and risk dragging her friend into the discussion, Claire took a skeptical tack. "You expect me to believe you?"

Binah shrugged. "What must I do to convince you?"

_That's easy._ "I want full access to my archive. I also want access to my friends' archives."

"Our policies do not permit it."

Remembering the rapier she held, Claire raised it to show Binah the point and edge with which she struck down gods and monsters alike in games more interesting than this one. "Change your policy."

The sword reappeared in Binah's hand, leaving Claire's empty. The AI smiled over the engraving. "Ultima Ratio Regina. Considering your disdain for dead languages like COBOL and BASIC, I can't help but find your taste for Latin odd."

"It's supposed to mean &#8216;the Queen gets the last word'."

Binah stuck the rapier into the floor between her and Claire, leaving it quivering. "A fitting sentiment for you, Ms. Ashecroft, but you do not reign here. Anger and fatigue must still cloud your judgment, else you might remember that we cannot give you access to another person's data without their informed consent."

_Shit. She's right._ Claire reclaimed her sword and sheathed it with a little flourish. "I'm sorry. Waking up and finding myself jacked into this simulation has me a bit disoriented."

"No doubt." A book coalesced in mid-air, and drifted into Binah's hand. She opened it, and turned a page. "We are empowered to give Sarah access to her records, so that she may verify for herself that she was not improperly recorded. We will do the same for all of Morgan's friends. Will this be sufficient?"

_Back off now, Claire. Binah's been more than generous, and it would not do to try her patience. Don't ask too much of her._ This cautionary thought came in her mother's voice, though Claire remained unable to wholly disown it. She took a breath, sped a silent _fuck you_ to the cowardly voice in the back of her head, and asserted herself. "It's a good start, but I need more."

"Oh?"

"Morgan Stormrider's security is still compromised. I need to implement a new Witness Protocol client for him, one that only records or uploads when he permits it. Because he's an asura emulator&#8211;"

"I understand. You need a software development kit for the 100 Series asura emulators."

_They got a SDK for Morgan? Holy shitballs._ Claire nodded. "That would be handy."

"I'll send a copy to Hal. I must caution you against redistribution. These men have never had a software patch, and many do not yet realize they are artificial intelligences. They've no real defense against malware." Binah snapped her book shut, and fixed gleaming eyes on Claire. "Do we have an understanding?"

"You live in a world of data, and you have no notion of who I am?" Claire bristled beneath the AI's obsidian gaze. She took a breath, and continued in a softer tone of voice. "Look. Morgan is a dear friend of mine. I wouldn't do anything to hurt him, and giving malicious developers a way to screw with his head isn't going to do him any favors."

"What of the other asura emulators?"

_Like Tetsuo? Aside from that ridiculous hairstyle, he's as hot and fuckable as Morgan._ "What if I coded an antiware suite that protected Morgan and the others, and gave you the code to distribute?"

"Wouldn't this break Witness Protocol for all of the 100 Series?"

Claire shrugged. "Well, yeah. They'd get the same deal as Morgan: when they record and when they upload becomes their choice alone. How many asura emulators are also Adversaries, anyway?"

"Just Stormrider and Munakata." Binah sighed. "And Stormrider has a letter of marque and reprisal empowering him to operate outside his usual chain of command anyway."

"What's the problem, then?"

"That telemetry exists for a reason." Binah paused, and shook her head before Claire could speak. "I know. You don't give a damn. Nor should we expect you to."

"Of course not." Claire recalled a lunch meeting she had with Imaginos, who introduced himself to her and Josefine as Isaac Magnin. He offered a persuasive case for Claire to join her friend Josefine under his patronage. He'd finance her education without any questions asked if, in exchange, she would work for the AsgarTech Corporation on a secret project involving anthromorphic artificial intelligence. _It wasn't the Asura Emulator Project he wanted me and Josefine to work on was it? Or does he call it Project Aesir now?_ "Imaginos has his own reasons for wanting twenty-four seven feeds from his creations, but that smarmy, rent-seeking parasite can go eat shit and bark at the moon."

"Was that wholly necessary?"

"Yeah, it was." Another deep breath as Claire banished Magnin's smirking face from her mind. "I hope Morgan nails that white-haired bishounen good and hard, and for once I'm not talking about buggery."

The corner of Binah's mouth curled upward a couple millimeters. "I understand how terribly difficult that must be for you. Do you require anything else from us?"

_I'll make you pay for that little smirk._ "Not until you get a hardware upgrade."

"Excellent." Another book drifted into Binah's hand. "Oh, and Claire? Don't ever use Daas again. It is not a substitute for TCP/IP. It is a safe place for the Sephiroth and our chosen guests."

Before Claire found words with which to respond, the world went black. She opened her eyes, and found herself in her bed wearing only her panties. Peeling the sweat-soaked sheets from her skin, Claire shook her head to clear it as she got her feet beneath her and stood. "Hal?"

"What happened, Claire? You kept tossing about, and you've lost a great deal of water to sweat. I bet the entire mattress is soaked, but you should be more concerned about dehydration. Does amphetamine use always provoke nightmares?"

Claire shook her head as she staggered into the bathroom attached to her bedroom, poured a glass of water, and drank it in slow, careful sips. "It wasn't the speed. That Daas protocol client is a dead end. It isn't just an alternate networking protocol. It's some kind of virtual world the Sephiroth don't want us fucking with."

The warmth of Hal's voice roughened with anger. "Did they somehow hurt you?"

"I don't think so. I was rather rude, on top of intruding on their safe space. But humans aren't welcome in Daas. You didn't distribute the protocol clients, did you?"

"No. But Binah sent me some kind of SDK for the 100 Series asura emulators."

_So, she kept her promise. I'll keep mine, then, and nuke my copies of the Daas code._ "Securely delete all of the Daas-related data, and open that SDK in a sandboxed dev environment." Claire refilled her glass, and drank half at once. "Order breakfast delivered, and tell Naomi and the others I won't be back for a bit."

[<img class="aligncenter size-large wp-image-65" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/rule-1024x178.png?resize=474%2C82" alt="rule" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/rule.png)

_The Blackened Phoenix_ isn't available in stores yet, but you can get ready by buying a copy of _Without Bloodshed_, the first novel, on Amazon, Barnes & Noble, and Kobo. Paperback and electronic editions are available worldwide.
