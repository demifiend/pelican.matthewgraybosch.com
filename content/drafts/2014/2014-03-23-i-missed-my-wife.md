---
id: 362
title: I Missed My Wife
excerpt: I wasn't used to living without Catherine, and I'm glad she's back.
date: 2014-03-23T17:52:20+00:00
header:
  image: catherine-smile.jpg
  teaser: catherine-smile.jpg
category: Personal
tags:
  - Alexandre Dumas
  - Auguste Maquet
  - Australia
  - dependence
  - immigrant
  - jet lag
  - loneliness
  - loud sex
  - love
  - marriage
  - missing my wife
  - old country
  - reunion
  - sleep
  - solitude
  - vacation
  - waking the neighbors
---
I missed my wife, Catherine. She had been gone for the past three weeks, visiting her family and old friends back in Australia, and just returned last night. We didn't get home until after 3am this morning, and didn't actually go to sleep until around 6am. We were too busy talking about her trip, and celebrating our reunion. I just hope we didn't wake our neighbours.

We had a wonderful lunch together, making Aussie-style cheeseburgers with an egg on top, and I showed her a bit of _Dark Souls_ _II_. However, she's gone back to bed for a bit. I don't blame her; she probably hasn't had a proper sleep since before she got on the plane in Melbourne, and she's clocked over twenty-four hours of travel time. She needs her rest.

This isn't the first time Catherine has travelled without me. She's done so several times, simply because <a title="Forbes: 1 in 4 Americans don't get paid vacation. I'm one of them." href="http://www.forbes.com/sites/tanyamohn/2013/08/13/paid-time-off-forget-about-it-a-report-looks-at-how-the-u-s-compares-to-other-countries/" target="_blank">my day job doesn't offer paid vacation</a>. Since I don't have the time or money to travel with her, I kiss her goodbye, wish her a good trip, and stay home to take care of business and keep an eye on the kitties. There's no point in begrudging her the need to occasionally return home. I might want to do the same if I had emigrated to Australia to marry Catherine, instead of her immigrating to the US.

Regardless, it's hard to have her away for so long after a decade of marriage. I miss hearing her voice. I miss the sight of her. I miss her presence. I miss having her hands in my hair. I miss her taste when she kisses me. I miss the warmth of her breath on my skin. I miss the soft heat of her body against mine when we sleep together. I miss being able to talk about [Starbreaker](http://www.matthewgraybosch.com/starbreaker/ "Starbreaker") with her, and it's harder to write fiction without her.

I'm sure some of you are wondering why I'm pouring out these emotions now that Catherine's home, instead of doing it while she was gone. I'm doing it now because it's safe to do so. If I had given much thought to how I missed my wife while she was away, I would have spent the three weeks of her absence moping, instead of working on _The Blackened Phoenix_, getting started on [_Silent Clarion_](http://www.matthewgraybosch.com/2014/03/18/silent-clarion-chapter-1-1/ "Silent Clarion – Chapter 1.1"), and establishing this blog.

I don't remember being this dependent on others before I married Catherine. I won't say I was stronger, but I was younger. Perhaps my emotional palette was more limited as a youth. I miss my wife when our circumstances make it necessary for her to travel without me, but I can deal with her absence for the duration.

I'll keep working. I'll still be there when she returns. She doesn't have to worry about me screwing around with other people while she's gone, because where else would I find the emotional and intellectual support I get from my wife? If I'm <a title="Wikipedia: Alexandre Dumas" href="http://en.wikipedia.org/wiki/Alexandre_Dumas" target="_blank">Alexandre Dumas</a>, she's my <a title="Wikipedia: August Maquet" href="http://en.wikipedia.org/wiki/Auguste_Maquet" target="_blank">Auguste Maquet</a>. Everything I do would probably be much harder without her.

If you're a writer with a spouse who gives you the same sort of creative and emotional partnership, I'd like to hear about it. More importantly, you should treasure it.
