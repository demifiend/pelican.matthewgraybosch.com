---
title: "*Silent Clarion*, Track 15: &quot;You Drive Me Nervous&quot; by Alice Cooper"
excerpt: "Check out chapter 15 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Arranged marriages, eugenics, and an old army base in the woods: things just keep getting weirder for Naomi."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
We reached the ruins of a hunting cabin an hour after our conversation about life as an Adversary lapsed. Despite the darkening sky and the rumble of thunder, my reluctance to seek shelter there remained. "Michael, are you sure we should be here?"

I certainly wasn't. Half the roof was missing, and the rest was charred. The fireplace was so ill-tended that any attempt to use it would most likely burn down the rest of the cabin. Evidence of young lovers using the place for trysts littered a corner, and I glanced at Brubaker. Was one of these used condoms his? Eww.

"It'll be fine." He opened a trap door, and began climbing down as lightning split the sky with an almost instantaneous roar. "Underground is safer!"

I followed, because he was right. The cellar was cleaner, too. Much cleaner, as if somebody came down here regularly and kept the place tidy. It even had working lights, once Brubaker felt around and found the switch. Since there wasn't a couch or any chairs, I settled onto one of the cushions spread around the room. "How did you know about this place?"

"My friends and I found it a few years ago and fixed it up. We'd come here to get away from everybody else. I changed the lock on the trapdoor so that the kids who come here to screw can't get down here and make a mess of things."

"Niall and Nigel, a couple of my brothers, had something like that for a while. They built a little shack out in the fields. It wasn't as fancy as this." Nor did they have as much porn. At least, I didn't think they did, but I'm not judging. I held up a disc I recognized because Jacqueline gave it to me. Taking a mock-serious tone, I showed Brubaker my find. "Take It Like a Man VII? Is this yours, young man?"

"It's one of ours, yes." Brubaker took the porno from me and stuck it back in the box. He then hid the box, as if that would erase my awareness of its existence. "It's hard to find a safe place to be yourself in a small town, let alone get time alone with someone special."

"You ever bring Jessica Stern down here?"

Brubaker shuddered. "Hell no. We bring the people we are actually interested in. Not the ones our parents pick for us. Here's the thing. We handle marriages here the same way we breed stock. You marry whom you're told to marry. Your own feelings don't matter. So we do our duty for our families and town, and get in some fun in private."

How aristocratic! Flashing immediately to my ex, I remembered that many rural cultures across the world still forced children into unwanted marriages when they thought the Phoenix Society wasn't watching, and let their families murder girls who rebelled. Utter barbarism, if you ask me. Why is it that it's always the quiet little towns that are the most profoundly fucked up? "So, you'll marry Jessica, knock her up a few times, and then have your fun with someone more congenial?"

"You shitting me? I want to get the hell out of Clarion, but I'd never make it in the city with the education I've got. Being an Adversary's my only shot."

He actually sounded angry. Was it over his lot in life, or directed at me for trying to persuade him to abandon his best hope for a better life? "I'm sorry. I didn't realize earlier."

He fumed as he brewed the joe on the stove in the kitchenette. Accepting the proffered mug, I waited for him to say something. "Not your fault. Heck, I'm sure Jessica hinted at her anticipated ownership of my ass."

We drank in silence for a while as I studied Brubaker. No doubt he used the free weights stacked in the corner. Between his physique and his comfort with the forests around Clarion, he'd at least make a good recruit. He was strong and sharp, but so am I. That alone doesn't make me a good Adversary, assuming I am a good Adversary.

An Adversary's post isn't just a job. Police officers sometimes talk about holding the true blue line between law and disorder. Adversaries hold a line of their own: a red line between liberty and tyranny. It's a harder line to hold, because it sometimes means being an advocate for chaos. "Michael, why do you want to take the oath? It isn't just about getting out of Clarion, is it?"

He didn't immediately answer. "Something needs to be done about Clarion's arranged marriages. There's something wrong with them."

"You can refuse, can't you?"

"Only if I leave everything behind. Nobody should have to do that. But it's worse than that." Brubaker glanced around the cellar, as if he feared being overheard. "It all goes back to Dr. Petersen. He says he's only testing children and discouraging matches between people whose genomes are too similar."

That sounded plausible. "But when he only gives you one or two acceptable matches, it looks like eugenics instead of genetic counseling?"

Brubaker nodded. "Yeah. I think he's using us for some kind of breeding experiment, but he isn't telling us anything about it or explaining why. I went to the Sheriff and the Mayor, but they're in on it. They told me the Phoenix Society wouldn't believe me."

Now it begins to make sense. He wants the training and authority to crack the case himself. "You want to expose the truth yourself, if nobody else will?"

"Exactly!" He thrust himself to his feet and paced the room as if galvanized by my question. "Maybe the Phoenix Society won't believe me. But if I'm aware of a problem, isn't it my responsibility to do something about it?"

No counterargument was possible, not when I came to Clarion for the exact same reason. "But there is an Adversary here. I might not be officially on duty, but I might be able to fix that if I can find probable cause for an investigation."

That didn't go over as well as I hoped, judging from the indignation in his voice. "Dammit, what more do you need?"

Ah, the impatience of youth. Never mind I'm only a couple years older than him. "Right. Time for your first lesson. I can't take what you've said to court and prove guilt. It isn't enough. It might not even be admissible as evidence. I need more."

Instead of answering, Brubaker cocked an ear at the ceiling. Grabbing a machete from a footlocker by the ladder, he ascended and stuck his head up out of the trap door. "Get the light. The rain's stopped."

He scanned the woods as I joined him topside. "Follow me. I want to show you something."

He did not speak again for the next hour. Instead, he led me deeper into the forest, following a trail marked years ago. The trees along our path bore old scars from hatchet strokes. We stopped several times so that Brubaker could hack through brambles.

We eventually reached a dead end, a wall of brambles that had woven themselves into cables as thick as my arm rose above our heads in a straight line running from west to east. Following the wall east led to a corner, which we turned before reaching what appeared to be a pair of overgrown gates and a dilapidated guard post.

I caught a glimpse of green paint the creepers didn't wholly obscure, and tore away the vines until I revealed a painted metal sign.

> North American Commonwealth Army  
> **FORT CLARION**  
> Authorized Personnel Only  
> (Secret Clearance or Higher Required)  
> CO: Col. Henrik Petersen

CO most likely meant 'commanding officer.' Was the Col. Henrik Petersen named on the sign the same Colonel Petersen who now served Clarion as a doctor and coroner? The same Dr. Petersen who Brubaker insisted was using the people of Clarion as breeding stock in a eugenics experiment?

The vine-matted gates were topped with razor wire, so climbing it was out. However, the guard station suffered no such limitation. Getting atop one of them would let me peek over the wall. Surely Fort Clarion was a ruin, but I wanted to see for myself. "Michael, can you give me a boost?"

He nodded, and got into position. Springing out of his cupped hand, I scrambled atop the guard hut and got my look over the fence. Within Fort Clarion's perimeter, everything was white-glove perfect. The Prime Minister of the North American Commonwealth could probably bounce quarters off every bed in the barracks. Every vehicle gleamed as if freshly washed and polished, and all of the buildings were newly painted. But there was something missing. "Where are the soldiers?"

"What?"

Of course Brubaker has no idea what I'm on about from below. I laid down on the roof and lowered my hand to help him up. He took a long look, and whistled. "I knew the base was here, but holy shit. It's like there were soldiers here only a few minutes ago. Where did they go?"

A glint from the watchtower caught my eye. Grabbing Michael, I jumped off. We rolled as we landed, and he looked ready to take a swing at me once he recovered. Not that I blamed him. "Sorry. I thought I saw light glinting off a scope."

"Sniper?"

I glanced back at Fort Clarion. "Don't know for sure. Somebody's there, and I'd rather not meet them yet."

---

### This Week's Theme Song

"You Drive Me Nervous" by Alice Cooper, from *Killer*

{% youtube K2K7k5hv9Hc %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
