---
id: 183
title: 'Tattoo Vampire &#8211; Part 3'
excerpt: Here's a story I wrote to promote *Without Bloodshed*. It also ties into *Silent Clarion*.
date: 2014-03-10T16:06:01+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - fiction
  - morgan stormrider
  - project harker
  - science fantasy
  - science fiction
  - serial
  - short story
  - starbreaker
  - tattoo vampire
---
Christabel lowered the violin in the middle of tuning it, and stared at Morgan, unable to believe his words. _Damn it, Isaac, why must you pay me to cheat on you with somebody like Morgan? Why couldn't you ask me to play Mata Hari with some short, fat, balding two pump chump of a businessman with_ _back hair and_ _a tiny dick?_ _This is the sort of man I'd love to seduce, if I didn't already have you._ "Morgan, did you just say you're going to blow off rehearsal tomorrow?"

Morgan shook his head. "If I wanted to blow you off, I wouldn't show up. I'd explain after the fact, if at all. Instead, I'm telling you as close to right away as humanly possible. I came here directly from the briefing."

"What sort of mission is it?" Naomi, the pale bitch Isaac Magnin insisted she bring into the band for keyboards and vocals, always seemed too curious about Morgan's day job. "It must be urgent if they won't let you put it off a day."

"I have to get a tattoo at what must be the sleaziest tattoo parlor in the city. Several customers reported being violated on the premises, and the Adversary sent to investigate was killed." Morgan withdrew a folded paper from his pocket and smoothed it on the table after unfolding it. "Here's the design I'm supposed to get."

"It's gorge–"

"It's horrible." Christabel cut Naomi off, glaring at Morgan as she did so. "You get that, or anything else, inked into your skin and we're bloody well through."

Instead of contrition, or any other reaction that suggested Morgan cared about their relationship and was willing to preserve it at any cost, his only response to the ultimatum Christabel issued was to shrug. He adjusted the strap of his guitar case on his shoulder, and turned to Naomi. "I won't be so uncouth as to ask you out so soon after being dumped by Christabel, but I'd love to meet you if you decide not to stick with Crowley's Thoth. I enjoyed working with you, Ms. Bradleigh."

He walked out, oblivious to Christabel as her fury mounted. She hurled a set of headphones at his head, only to see it bounce off the studio door as it closed behind him. _"Asshole!"_

She turned her gaze on Naomi, her hands curling into fists as a longing to wipe the small, satisfied smile from her rival's face gripped her. "You seem pleased with yourself, Nims. Was this what you wanted?"

"I'd tell you not to be an idiot, but you've already done a thorough job of making a fool of yourself." Naomi conveyed her contempt through words alone; her tone remained conversational. "Since Morgan is probably your first boyfriend, and –"

"And you're a bloody serial monogamist."

"–and I'm older and a bit more experienced, would you like me to explain what went wrong? Or shall I pack up and accept Morgan's offer? After you sang his praises with such enthusiasm only yesterday, I'm tempted to find out for myself."

_Don't you fucking dare._ Christabel kept her first response to herself, realizing despite her anger the extent of her failure. _Damn it. I was just bluffing, but he called me on it and walked out. Isaac will be furious with me if he finds out. I'm supposed to keep Morgan on a string, even though Isaac won't tell me why._ She calmed herself, and took a more respectful tone. "I'm sorry, Naomi. That crack about your own love life was uncalled for. What did I do wrong?"

Naomi shook her head. "Everything. To begin, Morgan wasn't blowing us off when he told us he had to miss rehearsal tomorrow because of his duties. He was dealing fairly with us. I'm surprised you didn't grasp that on your own, but let's continue. Next is your reaction to the tattoo."

"I hate tattoos. I think they mark people as being lower-class."

"Does Morgan know anything about your prejudice? Did the subject ever come up, before? Again, he's telling you up front, so you two can discuss it. Furthermore, even if he absolutely must get it as part of his mission, any reputable dermatologist can provide a prescription for tattoo-removal nanotech. Were you unaware?"

Christabel turned away from Naomi for a moment. _I was__<span style="text-decoration: none;">, but I'm not going to admit it.</span>_ "Are you joking?"

"No." Naomi removed one of her boots to reveal her ankle. "I had some ink right here, but I removed it after making some changes in my life. Now, if Morgan is only getting tattooed for a mission, what's to stop him from removing it afterward? You'd never know, unless he told you."

"But why does he tell me these things?"

Naomi shrugged. "Ever consider the possibility that he respects you, or did until you threatened the relationship in order to force him to let you have everything your way? Didn't anybody ever tell you that you should never do that? Any man possessed of a backbone will dump you on the spot."

"I need to talk to him, don't I. I should apologize." _Not that I need you to tell me that. If Isaac finds out how badly I fucked this job up, he'll be disappointed with me. I can't let that happen._ Christabel began rifling through her bag, desperate to find her handheld. She tried to reach Morgan as soon as her fingertips brushed the device's case, and didn't wait for him to greet her before speaking. "Morgan, I'm sorry. I was out of line earlier."

His response came in plain text. "Never call me on duty."
