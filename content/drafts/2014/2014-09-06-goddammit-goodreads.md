---
title: "Goddammit, Goodreads"
excerpt: "Goodreads knows I'm an author. They have my email. So who do I have to *blackmail* to get an email when somebody reviews my fucking books?"
header:
  image: picard-facepalm.jpg
  teaser: picard-facepalm.jpg
categories:
  - Rants
  - Social Media
tags:
  - black dwarf
  - four stars
  - Goodreads
  - nobody tells me anything
  - notification
  - review
  - spoiler
  - Starbreaker
  - Without Bloodshed
  - Lynda Williams
---
Here's a question for anybody who cares to answer. Are authors allowed to hate Goodreads and think it's a lousy site whose developers should be put to work scrubbing toilets? Because that's how I feel about that site right now.

I'm not miffed because I got a bad review. I knew what I was getting myself into when I decided to publish.

I already have an action plan for bad reviews. It's the same as my strategy for job losses, failed romantic relationships, and especially embarrassing deaths in _Dark Souls II_. It's a simple plan, and it consists of the following:

  * Get drunk.
  * Listen to Queensryche (but only the good shit from the 80s).
  * Get over it.

It's more effective than you think, but make sure you've arranged a designated driver before you get started.

No, I'm annoyed with Goodreads because I got a _good_ review back in July, and never heard about it. Reviews are tied to books, books are tied to author profiles, and my email is in my author profile. So why couldn't Goodreads and their corporate parent Amazon provide an opt-in for receiving incoming review alerts by email? Do they not _think_?

Even more galling is that the review in question was written by [Lynda Williams](https://www.goodreads.com/author/show/644584.Lynda_Williams), who I have listed as a friend on Goodreads. She's a Canadian SF novelist whose long-running [_Okal Rel Saga_](http://en.wikipedia.org/wiki/Okal_Rel_Universe) is reminiscent of Frank Herbert's _Dune_ and Iain M. Banks _The Culture_. She deals with cultural collisions, social change, and interplay between masculinity and femininity between and within individuals. I think it's awesome, and I look forward to the next book, _Unholy Science_.

And she's a Starbreaker fan. [Here is her review](https://www.goodreads.com/review/show/825295653), which I've reproduced almost verbatim below. I added paragraph breaks for readability and cleaned up a couple of typos.

> The first of a series, Graybosch's _Without Bloodshed_ introduces a cast of super-powered enforcers licensed to kill by what emerges as a snakepit of upper ups enmeshed in a latticework of intrigues. The protagonist, Morgan Stormrider, is a hunky killing machine with a homey domestic side.
>
> Cat lovers will enjoy both the actual feline pets and the cat-DNA aspect of Stormrider and fellow adversary Naomi. While it is a little hard to keep up with all the baddies plotting in the background, the friendships and romance sweep along consistently.
>
> The story is seasoned with Dickensian characters in supporting roles. I was particularly fond of the motorcycle gangs like the Fire Clowns. Oh, and there's a rock theme throughout, underpinned by Stormrider and love interests belonging to a band.
>
> The surprise ending is a set up for more to come. As the series is called Starbreaker one presumes the scope of cleaning up a city or two, in the first book, will be leaping to the astronomical.
>
> <cite>Goodreads Review of <strong>Without Bloodshed</strong> by <a href="https://www.goodreads.com/review/show/825295653">Lynda Williams, 1 July 2014</a></cite>

I'm quite pleased with this review, and wish only that Lynda had also submitted it to Amazon for potential purchasers to see. But maybe she did, only to have Amazon refuse it. Amazon's policies concerning book reviews by authors can be a bit complicated.

I don't want to spoil anything for Lynda or anybody else, but you'll find that the stakes rachet up nicely by the time we get into the fourth Starbreaker novel, _A Tyranny of Demons_.

What will Morgan Stormrider do when he learns that revenge against Imaginos requires using the Starbreaker to set off a series of reactions that will reduce the Sun to a [black dwarf](http://en.wikipedia.org/wiki/Black_dwarf) billions of years before its time? Will Morgan's sense of justice let him condemn Earth to freeze in the dark? Will he possess the strength to keep the Starbreaker from carrying out its deicidal mission?

Oh, wait. That hint at how the Starbreaker really works is a fairly significant spoiler, since it also implies a few things about the nature of beings like Imaginos. Oh, well. These little teasers are why people follow me, isn't it?
