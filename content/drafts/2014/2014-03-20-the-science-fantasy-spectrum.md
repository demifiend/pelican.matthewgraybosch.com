---
id: 321
title: The Science Fantasy Spectrum
date: 2014-03-20T12:34:23+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=321
permalink: /2014/03/the-science-fantasy-spectrum/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link_facebook:
  - http://bit.ly/1N0qVU6
bitly_link_linkedIn:
  - http://bit.ly/1N0qVU6
sw_cache_timestamp:
  - 401650
bitly_link:
  - http://bit.ly/1N0qVU6
bitly_link_twitter:
  - http://bit.ly/1N0qVU6
bitly_link_tumblr:
  - http://bit.ly/1N0qVU6
bitly_link_reddit:
  - http://bit.ly/1N0qVU6
bitly_link_stumbleupon:
  - http://bit.ly/1N0qVU6
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - acts of caine
  - akallabeth
  - arthur c. clarke
  - brandon sanderson
  - c. j. cherryh
  - c. s. friedman
  - "clarke's third law"
  - coldfire trilogy
  - doom
  - fantasy
  - final fantasy
  - ghostbusters
  - heroes die
  - j. r. r. tolkien
  - logical positivism
  - matthew stover
  - michael-moorcock
  - mistborn trilogy
  - morgaine
  - neon genesis evangelion
  - numenor
  - phantasy star
  - preternatural
  - rahxephon
  - rationalism
  - roger zelazny
  - science
  - science fantasy
  - science fiction
  - scrapped princess
  - shin megami tensei
  - silmarillion
  - star wars
  - stormlight archive
  - sufficiently advanced technology
  - supernatural
  - the Enlightenment
  - trinity blood
  - vampire hunter d
  - vision of escaflowne
  - warbreaker
  - writing
---
What do you call a fantasy novel written by an atheist? Science fiction. I admit it's a lame joke, but I wished to illustrate without quoting Arthur C. Clarke that fantasy and science fiction can be treated as a spectrum, with high fantasy on one end, hard SF on the other, and <a title="TVTropes: Science Fantasy (Warning: Time Sink!)" href="http://tvtropes.org/pmwiki/pmwiki.php/Main/ScienceFantasy" target="_blank">science fantasy</a> in between.<figure style="width: 1024px" class="wp-caption aligncenter">

[<img alt="Science Fantasy by Sykosan @ DeviantArt" src="http://i1.wp.com/fc06.deviantart.net/fs71/i/2011/081/1/9/science_fantasy_by_sykosan-d3c7fb4.jpg?resize=840%2C628" data-recalc-dims="1" />](http://sykosan.deviantart.com/art/Science-Fantasy-201900352)<figcaption class="wp-caption-text">Science Fantasy by Sykosan @ DeviantArt</figcaption></figure> 

## What is Science Fantasy?

<a title="Wikipedia: Arthur C. Clarke" href="http://en.wikipedia.org/wiki/Arthur_C._Clarke" target="_blank">Arthur C. Clarke</a>, author of _2001: A Space Odyssey_ and many other hard SF classics, postulated three laws of prediction. The third is the one most often quoted, and most relevant to this post:

  1. When a distinguished but elderly scientist states that something is possible, he is almost certainly right. When he states that something is impossible, he is very probably wrong.
  2. The only way of discovering the limits of the possible is to venture a little way past them into the impossible.
  3. Any sufficiently advanced technology is indistinguishable from magic.

The third law is of particular interest to me, because I don't write straight fantasy or straight science fiction. I write a sort of hybrid called <a title="Wikipedia: Science Fantasy" href="http://en.wikipedia.org/wiki/Science_fantasy" target="_blank">science fantasy</a>. I do so because, despite my best efforts, I cannot bring myself to write straight fantasy. I can't just write magic, I have to figure out how the magic works, and somehow tie it to known science. I can't just write about gods; they have to be aliens of some kind. My swords can't just be steel; they have to fabricated using some kind of proprietary alloy incorporating precious metals like palladium.

Blame the influence of writers like Michael Moorcock, Roger Zelazny, and C. J. Cherryh. Blame movies like _Ghostbusters_ and _Star Wars_. Blame video games like _Doom_, _Final Fantasy_, _Phantasy Star_, and _Shin Megami Tensei_. Blame anime like&nbsp;_Vampire Hunter D_, _Neon Genesis Evangelion_,&nbsp;_Scrapped Princess_, _Vision of Escaflowne_, _RahXephon_, or _Trinity Blood_. Or just blame my atheism. Whatever you blame for my inability to do straight fantasy, the result is the same: my worldview has no room for the supernatural, which makes it difficult for me to write fantasy. Nor do I have the STEM background that SF titans like David Brin, Larry Niven, C. J. Cherryh, or Ursula K. Le Guin can boast, so I'd be mad to claim I write hard SF.

## Preternatural vs. Supernatural

But what is fantasy? What is science fiction? What's the difference between the two, since both genres often deal with phenomena not explained by existing science? I think the difference lies in how the characters treat such phenomena. In science fiction, phenomena outside the current scope of human knowledge is <a title="Wikipedia: Preternatural" href="http://en.wikipedia.org/wiki/Preternatural" target="_blank"><em>preternatural</em></a>. In fantasy, such phenomena are <a title="Wikipedia: Supernatural" href="http://en.wikipedia.org/wiki/Supernatural" target="_blank"><em>supernatural</em></a>.

[<img class="size-full wp-image-326" alt="Smudge says, &quot;You're confusing me, hairless ape demon.&quot;" src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/smudge.jpg?resize=840%2C630" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/smudge.jpg)

<dl class="wp-caption aligncenter" id="attachment_326" style="width: 1010px;">
  <dd class="wp-caption-dd">
    Smudge says, "You're confusing me, hairless ape demon."
  </dd>
</dl>

At this point, you might be as confused as Smudge. You might be used to conflating the preternatural with the supernatural. Let's unpack the terms, and see if that helps.

The word "preternatural" has a Latin root, &#8216;preter-&#8216; (or &#8216;praeter-&#8216;), so its original definition is "outside or beyond nature". The medieval Catholic scholar Thomas Aquinas defined the preternatural as "what happens rarely, but nonetheless by the agency of created beings&#8230;Marvels belong, properly speaking, to the realm of the preternatural." He contrasted the preternatural with the supernatural, which was God the creator's exclusive province. Early modern scientists use the term to denote abnormalities and strange phenomena that seem to depart from the expected course of nature.

I use the word "preternatural" to denote phenomena outside the _current_ scope of human knowledge. Such phenomena can still be studied and understood through the use of scientific reasoning. Supernatural phenomena, by contrast, are outside human understanding and will always remain so. If we could understand God, he wouldn't be supernatural, but preternatural &#8212; and eventually just natural.

## The Preternatural in Fantasy

If we use Thomas Aquinas' definition of the preternatural, as explained above, then the use of preternatural phenomena in fantasy goes back at least as far as J. R. R. Tolkien. Some readers may argue that the magic used by characters like Gandalf, Saruman, and Galadriel is supernatural, because there's no explanation as to how it works. It's just there, and it just happens. I see where they're coming from, but these characters aren't gods, or avatars of gods.

As a wizards, Gandalf, Saruman, Radagast, and the two unnamed Blue Wizards constitute the Istari, an order of Maiar &#8212; angelic beings &#8212; sent by the Valar to Middle-Earth with a mission to teach and inspire the mortal races of humans, dwarves, and elves to fight Sauron on their own. Galadriel is of the Noldor tribe of Elves who once lived among the Valar in the Undying Lands before following Feanor back to Middle-Earth to help fulfill his oath of vengeance against Morgoth for stealing the Silmarils. Sauron, Gandalf, Saruman, the Elves, and the Maiar and Valar are all creations of Eru, the One, though these connections aren't necessary clear unless one reads beyond _The Hobbit_ and _The Lord of the Rings_ and starts digging into the _Silmarillion_.

Because they are created beings, their powers are preternatural rather than supernatural. I think Tolkien himself would agree, given his adherence to the Roman Catholic faith. The only way to be sure, however, requires the services of a competent necromancer.

## Beyond Tolkien

The use of preternatural phenomena in fantasy does not end with Tolkien. Because Terry Brooks' _Shannara_ setting is post-apocalyptic, almost everything his characters encounter is more preternatural than supernatural, though they don't necessarily have the science to explain the weird shit that happens around them.

In C. J. Cherryh's Morgaine books (_Gate of Ivrel, Well of Shiuan, Fires of Azeroth,_ and _Exile's Gate_), the protagonist Morgaine seems otherworldly, and her tools and weapons appear to be witchcraft, because we're limited by the perceptions of the viewpoint character Vanye, who simply doesn't have the context to understand the tech on which Morgaine depends in her mission to shut down every last one of the _qhal_ gates.

The human colonists who landed on Erna in the beginning of C. S. Friedman's Coldfire Trilogy didn't understand the _fae_, the force native to Erna that tried to integrate humanity into the biosphere by giving human terrors physical form. They started by treating it as magic and then studying it.

Brandon Sanderson continues in Ms. Friedman's vein. The systems of magic he uses in his _Mistborn_ novels, the standalone _Warbreaker_, and his epic _The Stormlight Archive_ may resemble systems of magic intended for use in tabletop role-playing games, but in the context of his stories they subject magic and similar phenomena to human inquiry. He pulls magic out of the realm of the preternatural, and makes it natural in his settings.

Beginning with _Heroes Die_, Matthew Stover also depicts a world in which what we call magic is just a natural force in his _Acts of Caine_ series. The people of Earth understand that on Overworld, magic is just part of nature, but analysts working for the Studio system underestimate the inhabitants of Overworld, and assume that the presence of magic makes scientific inquiry impossible until a thaumaturge does some basic scientific-method research to create wearable anti-magic clothing that functions on the same principle as a Faraday cage. It just gets crazier from there, by the way, and you should read the books yourself if you've got the stomach for grimdark.

My brand of science fantasy is hardly unique, though I think <a title="Starbreaker" href="http://www.matthewgraybosch.com/starbreaker/" target="_blank">Starbreaker</a> falls between _The Stormlight Archive_ and _The Acts of Caine_ on a spectrum between idealism and cynicism, with Stover's series holding down the cynical end. You can see for yourself if you read <a title="Without Bloodshed" href="http://www.matthewgraybosch.com/without-bloodshed/" target="_blank"><em>Without Bloodshed</em></a>, though the preternatural elements in the first Starbreaker novel are minimal, compared to what I mean to show in _The Blackened Phoenix_, _Proscribed Construct_, and _A Tyranny of Demons_.

## What About the Supernatural in Fantasy?

At this point it's fair to ask if fantasy has any room for genuine supernatural phenomena, or if the post-Enlightenment worldviews influenced by science, logical positivism, and other philosophies make impossible the task of depicting supernatural phenomena in fantasy fiction. I think that the supernatural is only possible for entities which exist _outside_ the universe, who reach inside and monkey around with the world with no regard for how the world normally works.

The only instance of a genuine supernatural phenomenon in fantasy that I can think of also comes from Tolkien's Akallabêth, the last section of the _Silmarillion_ detailing the fall of [Númenor](http://en.wikipedia.org/wiki/N%C3%BAmenor "Númenor"). When the Great Armament led by Ar-Pharazon landed on the shores of Aman, the Valar put aside their guardianship of the world of Arda and said, "Eru, we're gonna let _you_ deal with this." Eru dealt with Ar-Pharazon by burying the Great Armament, placing Aman outside Arda, making Arda spherical when it had once been flat, and sinking [Númenor](http://en.wikipedia.org/wiki/N%C3%BAmenor "Númenor") &#8212; though the Elves could still find their way back to Aman if they set sail from the Grey Havens.

If you know of any other examples of the genuinely supernatural in fantasy, or just want to talk about this article, post comments. Also, please feel free to share this article if you found it interesting.