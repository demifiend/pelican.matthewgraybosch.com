---
id: 410
title: I Watched Frozen Last Night
date: 2014-03-26T20:19:42+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=410
permalink: /2014/03/watched-frozen-last-night/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link_googlePlus:
  - http://bit.ly/1PiSIBM
bitly_link_twitter:
  - http://bit.ly/1PiSIBM
bitly_link_facebook:
  - http://bit.ly/1PiSIBM
bitly_link_linkedIn:
  - http://bit.ly/1PiSIBM
sw_cache_timestamp:
  - 401671
bitly_link_tumblr:
  - http://bit.ly/1PiSIBM
bitly_link_reddit:
  - http://bit.ly/1PiSIBM
bitly_link_stumbleupon:
  - http://bit.ly/1PiSIBM
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - Crossbreed Priscilla
  - dark souls
  - frozen
  - Idina Menzel
  - lore
  - speculation
---
I rented _Frozen_ for my wife to watch last night, and watched it with her. Not a bad flick, but this is the main event. Everything else is just context.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

If you think Idina Menzel was good with "Let It Go", check out "Defying Gravity".

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

I should write more, but I'm too tired after putting in twelve hours at my day job yesterday and ten the day before. Instead, I'm just going to leave you with this. Before Elsa slammed the door in everybody's face, there was another misunderstood young woman who was shut away because others couldn't deal with their fear of her. The cold didn't bother her much, either.<figure id="attachment_411" style="width: 650px" class="wp-caption aligncenter">

[<img class="size-full wp-image-411" alt="Crossbreed Priscilla from Dark Souls" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/darksouls-frozen.png?resize=650%2C962" data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/darksouls-frozen.png)<figcaption class="wp-caption-text">Crossbreed Priscilla from Dark Souls</figcaption></figure> 

The in-game lore for _Dark Souls_ doesn't say much about Priscilla, other than that she's a bastard crossbreed, and an abomination with no place in the world who clutched her doll tightly and was eventually drawn into a cold and lonely painted world. However, she's the one boss who speaks to your character, and will let you go without a fight as long as you refrain from attacking her.

I usually leave her alone; it seems cruel to attack a lonely girl who isn't swinging her scythe at me. There's some more speculation in this video, if you care.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>