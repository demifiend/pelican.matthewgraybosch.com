---
id: 1095
title: Are You a Parent? Your Kid Needs Dark Souls II
date: 2014-08-08T09:00:32+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1095
permalink: /2014/08/are-you-a-parent-your-kid-needs-dark-souls-ii/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapPN:
  - 
snapSU:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snap_isAutoPosted:
  - 1
snapBG:
  - |
    s:373:"a:1:{i:0;a:8:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:90:"http://matthewgraybosch.blogspot.com/2014/08/dark-souls-ii-essential-childhood-gaming.html";s:5:"pDate";s:19:"2014-08-08 13:00:41";}}";
snapDI:
  - 
snapDL:
  - 's:292:"a:1:{i:0;a:8:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"dae4a9db74be2c46986a477ea9d6db98";s:5:"pDate";s:19:"2014-08-08 13:00:43";}}";'
snapWP:
  - 
snapLI:
  - 
snapSC:
  - 
snapIP:
  - 
snapLJ:
  - 
snapGP:
  - 
sw_cache_timestamp:
  - 401667
bitly_link_twitter:
  - http://bit.ly/1N0qIQY
bitly_link_linkedIn:
  - http://bit.ly/1N0qIQY
bitly_link_facebook:
  - http://bit.ly/1N0qIQY
bitly_link:
  - http://bit.ly/1N0qIQY
bitly_link_tumblr:
  - http://bit.ly/1N0qIQY
bitly_link_reddit:
  - http://bit.ly/1N0qIQY
bitly_link_stumbleupon:
  - http://bit.ly/1N0qIQY
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Dark Souls II
  - Games
tags:
  - Art of War
  - Book of Five Rings
  - childhood
  - children
  - dark souls ii
  - essential childhood gaming
  - failure
  - filthy casual
  - git gud
  - humor
  - learning
  - life lessons
  - Miyamoto Musashi
  - parenting
  - Sun Tzu
---
If you're a parent, and you've denied your children the experience of _Dark Souls II_, you have crippled them for life. You've condemned them to the mediocre existence of a filthy casual.

Do you even _consider_ the lessons your children learn from the video games you buy for them? Have you considered the possibility that your children might learn the _wrong_ lessons from their games? These are the lessons your children will miss by not playing _Dark Souls II_:

  * Failure is inevitable.
  * Concentrate on the task at hand.
  * Be patient.
  * Know yourself.
  * Know your enemy.
  * Know your tools.
  * Dodge. Don't block.
  * Face one enemy at a time.
  * Fight dirty.
  * Avoid expectations.
  * Don't be a one-trick pony.
  * Victory isn't the end.

## Reasons _Dark Souls II_ Is Not for Kids

I will acknowledge a single reason to deny your children the frustration and joy of _Dark Souls II_. If you're one of those _responsible_ parents who take the time to read the ESRB warnings for games, and only choose "age appropriate" games for your kids, I understand. [Did this warning for _Dark Souls II_ put you off?](http://www.esrb.org/ratings/synopsis.jsp?Certificate=33288)

> This is a role-playing game in which players assume the role of an undead fighter in the realm of Drangleic. Players traverse dungeon-like settings and battle a variety of fantastical enemies (e.g., ghouls, zombies, skeletons, giant rats) to gain souls. Players use knives, swords, and arrows to defeat enemies. Combat is highlighted by cries of pain and small splashes of blood. Some locations depict instances of blood and gore: a giant snake boss holding its severed head; a giant boss creature composed of hundreds of corpses; dead ogres near a pool of blood; a torture device with streaks of blood. During the course of the game, a boss creature appears partially topless (e.g. hair barely covering breasts). The words "bastard" and "prick" can be heard in the dialogue. 

## The Kids Will Be Fine

If the above made you wary, I would understand. Here's the rub: if you let your kids watch TV news, or surf the web unsupervised, chances are they've seen worse. Much worse.

Instead, think of the valuable moral instruction From Software and Namco/Bandai is helping you provide, especially if you sit with your children as they play &#8212; which you should be doing anyway.

### Failure is inevitable.

_Dark Souls II_ isn't an easy, forgiving game. If you screw up, you get hurt. If you screw up badly enough, you _die_. Just like real life.

Unlike real life, however, you can take a deep breath, think over what went wrong, and try again from the last bonfire at which you rested. Try not to die again before recovering your souls.

### Concentrate on the task at hand.

If you're thinking about anything except the enemy you're currently trying to fight, that enemy is going to kill you. There's no time-out in _Dark Souls II_. Hell, there isn't even a pause button.

### Be patient.

You're going to die in _Dark Souls II_. You're going to die often. You're going to die because you tried to get a quick kill. You're going to die because you dodged the wrong way and rolled off a cliff. You're going to die because somebody invaded your game and was better than you.

You're going to die in real life, too. Accept it, take a deep breath, and remember that your feelings don't matter. Control the things you can control, and let everything else do their worst.

And if you're gonna die, die with your boots on.



### Know yourself.

Understanding the character you've chosen to play is the first step to success in _Dark Souls II_. Your character's capabilities will help determine which tactics are most likely to work in the game. Tactics suited to for a character made to wear heavy armor and wield massive weapons aren't ideal for a character built for speed or a character built to use magic.

Your own capabilities will help determine your options in life, as well. You must decide what skills and abilities you will cultivate. Will you build on existing strengths, or improve on your weaknesses? Do you even know your strengths and weaknesses?

### Know your enemy.

Just as you must know your own character in _Dark Souls II_, you should also understand the enemies you've set out to fight. Every enemy type is different and will fight differently.

Learn the enemy's attack pattern, and you can exploit it. Learn the enemy's weaknesses and you can strike with weapons capable of inflicting more damage faster.

You can't solve a problem you don't understand. You'll just get stuck, flailing away to no avail.

### Know your tools.

You'll likely collect dozens of different weapons in _Dark Souls II_: swords, daggers, hammers, axes, polearms, bows, catalysts, and other, more exotic implements. Each has its own movements, speed, rhythm, and requirements.

Some weapons are versatile, and usable in a broad range of conditions. Others are best used in particular situations. A spear might be effective in a narrow passage where enemies are unlikely to surround you. In a wide open space, however, you might prefer a two-handed sword you can swing in a wide arc.

Remember the wisdom of Sun Tzu, who wrote in _The Art of War_&#8230;

> It is said that if you know your enemies and know yourself, you will not be imperiled in a hundred battles; if you do not know your enemies but do know yourself, you will win one and lose one; if you do not know your enemies nor yourself, you will be imperiled in every single battle. 

### Dodge. Don't block.

Maybe your kids have never been in a fight. Maybe they've never been bullied. If so, good for them. But if they do find themselves in a violent situation, you want them to know how to handle themselves.

While imitating the action in _Dark Souls_ is a bad idea, you can still apply some broad principles. The most important of which is that you should never let the enemy make contact.

Blocking a blow still hurts, and can still damage you. It's better, therefore, to avoid your enemy's attentions altogether. Why block an attack you can dodge?

### Face one enemy at a time.

This really should be self-explanatory. You can't ever count on having allies beside you in a fight. When shit gets real, you're likely to stand alone. You're more likely to survive a one-on-one duel than you are a battle against multiple adversaries &#8212; especially if they've got solid teamwork and a grasp of small-unit tactics.

### Fight dirty.

I know you want your children to learn to be honest and to play fair. They aren't going to be knights of Arthurian romance facing chivalrous adversaries who salute before attacking, and never kick a downed enemy.

The enemies you will face will use every nasty trick available to get the better of you. They'll gang up on you. They'll sneak up on you. They'll play dead and strike from behind.

They'll do anything to avoid a fair fight, and you should emulate their example. If you're in anything resembling a fair fight, rather than a fight where the odds are outrageously in _your_ favor, then you fucked up.

### Avoid expectations.

Here is a principle that applies mainly to fights against other players. Others will invade your game unless you burn a Human Effigy at a bonfire or play offline.

None of these invaders will fight the same way. They may not fight as you expect them to fight based on their armament. They may fight honorably, greeting you with a bow that you might prepare for a duel, or they may strike from ambush.

Appearances deceive. Actions can also deceive. Always be on your guard.

### Don't be a one-trick pony.

It is easy, and therefore tempting, to become attached to a favorite weapon and thus a preferred style of gameplay. I am guilty of this myself, especially when playing against others. I tend to favor one-handed slashing weapons, and I like to remain in constant motion.

According to Japan's sword saint, Miyamoto Musashi, this is a mistake.

> You should not have any special fondness for a particular weapon, or anything else, for that matter. Too much is the same as not enough. Without imitating anyone else, you should have as much weaponry as suits you. 

Don't depend on a single weapon, or a single spell, or a single tactic. To do so is to lose flexibility. Sacrificing flexibility leaves you unable to adapt to changing circumstances.

### Victory isn't the end.

When you finally win through to the end, you won't find an ending. You won't find closure. You won't even find an opportunity to begin your journey anew &#8212; at least not until you hit the bonfire at Majula.

But when you do finally start your journey anew, you'll find that it's the same journey, only harder. Life is also like that. It isn't enough to succeed once. Success' reward is the need to redouble one's efforts and succeed again, and again.

## That Isn't So Bad, Is It?

You might think that _Dark Souls II_ is just a gory, violent action game. You might think it's inappropriate for children. You might be right, but to deny your children the experience is to deny them an opportunity to learn lessons that will serve them well in school, and in adult life.