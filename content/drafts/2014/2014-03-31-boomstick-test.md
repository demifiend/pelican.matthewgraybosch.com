---
id: 422
title: The Boomstick Test
date: 2014-03-31T20:35:43+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=422
permalink: /2014/03/boomstick-test/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
sw_cache_timestamp:
  - 401669
bitly_link_linkedIn:
  - http://bit.ly/1N0kvV9
bitly_link_googlePlus:
  - http://bit.ly/1N0kvV9
bitly_link_twitter:
  - http://bit.ly/1N0kvV9
bitly_link_facebook:
  - http://bit.ly/1N0kvV9
bitly_link_tumblr:
  - http://bit.ly/1N0kvV9
bitly_link_reddit:
  - http://bit.ly/1N0kvV9
bitly_link_stumbleupon:
  - http://bit.ly/1N0kvV9
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - atheism
  - boomstick
  - preternatural
  - reason
  - science
  - scientism
  - supernatural
  - talking purple unicorn
---
For some reason I doubt that <a title="Wordy Newb" href="http://loveminustruthishate.net/author/adfaciem/" target="_blank">Wordy Newb</a> at <a title="(Love - Truth) = Hate" href="http://loveminustruthishate.net" target="_blank">(Love &#8211; Truth) = Hate</a> was being kind when he quoted my post on the <a title="The Science Fantasy Spectrum" href="http://www.matthewgraybosch.com/2014/03/20/the-science-fantasy-spectrum/" target="_blank">preternatural and supernatural in science fiction and fantasy</a>, but I'll take it. I read <a title="The Stargate Fallacy" href="http://loveminustruthishate.net/2014/03/31/the-stargate-fallacy/" target="_blank">"The Stargate Fallacy"</a> a couple of times today, and I'm not quite sure if he didn't get what I was writing, or if I was unclear. Here's the short version of his post: he thinks atheists mentally re-index things he considers supernatural as natural to avoid admitting the existence of the supernatural. Here's my answer, and a Bechdel-style test you can use in your own writing if you care: if your talking purple unicorn can't survive a face-full of boomstick, it's not supernatural.

For those of you who haven't seen _Army of Darkness_, here's the relevant clip.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Now for the long version. First, let's see what <a title="Wordy Newb" href="http://loveminustruthishate.net/author/adfaciem/" target="_blank">Wordy Newb</a> actually says in his post:

> Now, if you ask J [Random Atheist] whether he believes in unicorns, he will tell you he does not.
> 
> If you ask him why, he will tell you he doesn’t believe in supernatural things.
> 
> If you happen upon a unicorn and show it to him, he will first suspect it to be a hoax (for that matter, so would you).
> 
> If the unicorn is confirmed to be real, he will say, “Ah, I see that unicorns are _not_ supernatural after all!”
> 
> This is the heart of the Stargate Fallacy.
> 
> The atheist seems to claim (and indeed believes) that there are two categories of things: natural and supernatural.  One contains stuff that actually exists, and one contains stuff that people made up.  Therefore it is quite sensible to disbelieve in the supernatural.  He claims (and indeed believes) that the categories are quite easy to define, but if you press him, you will find that the definition amounts to this:  ”Things I do not believe to be real.”

Um, no. As a real live all-American atheist, I don't define the supernatural as "things I don't believe to be real". The word "supernatural" already has a perfectly good definition, <a title="The Science Fantasy Spectrum" href="http://www.matthewgraybosch.com/2014/03/20/the-science-fantasy-spectrum/" target="_blank">which I belabored</a> a couple of weeks ago. To recap, preternatural phenomena are unusual and extraordinary, but still within nature, and can be understood by humans using reason and science. Supernatural phenomena are not only _beyond_ human understanding, but they _always will be_.

If you'll pardon the digression, I'll also paraphrase Thomas Aquinas, since I come from a Roman Catholic background rather than a Missouri Synod Lutheran one: weird shit caused by beings created by God is preternatural. Weird shit caused by God Himself is supernatural.

Digressions and pedantry aside, I don't use the word "supernatural" to categorize things I don't believe are real, like talking purple unicorns and honest politicians. Instead, _I reject the very existence of the supernatural_. I don't think there is anything within our universe that we cannot subject to human inquiry and understand through the application of reason and science &#8212; including talking purple unicorns.

Now let's talk about _Stargate_. I know the <a title="Wordy Newb" href="http://loveminustruthishate.net/author/adfaciem/" target="_blank">Wordy Newb</a> is referring to the TV series, but I haven't seen any of that. I'm going to refer to <a title="Wikipedia: Stargate (film)" href="http://en.wikipedia.org/wiki/Stargate_%28film%29" target="_blank">the movie</a> whose worldwide commercial success made the TV spin-offs possible.

The movie's primary conflict involves a being venerated by humans on a planet on the other end of a Stargate unearthed in Egypt as the sun god <a title="Wikipedia: Ra" href="http://en.wikipedia.org/wiki/Ra" target="_blank">Ra</a>. Though the people on the other end of the Stargate think Ra and the soldiers he uses to oppress them are gods, they're just primitive screwheads compared to the explorers from Earth, primarily a US Air Force contingent escorting the protagonist who figured out how to open the Stargate.

The explorers demonstrate to the primitive screwheads that their gods are nothing of the sort by killing them. Ra fares no better, though his end is far more spectacular. It turns out he can't survive having a thermonuclear device initiate in his face. Hence the Boomstick Test: **if a god or any other "supernatural" entity can be defeated using human technology, it isn't supernatural.** It's just weird shit we don't fully understand yet &#8212; like Gozer.<figure style="width: 619px" class="wp-caption aligncenter">

[<img alt="What good is being a goddess if four scruffy New Yorkers can banish your ass?" src="http://i1.wp.com/genealogyreligion.net/wp-content/uploads/2011/05/Gozer-the-Gozerian.jpg?resize=619%2C353" data-recalc-dims="1" />](http://genealogyreligion.net/ghostbusting-with-gozer)<figcaption class="wp-caption-text">What good is being a goddess if four scruffy New Yorkers can banish your ass?</figcaption></figure> 

Featured image credit: [SJ Graphics](http://www.redbubble.com/people/sj-graphics/works/8318401-this-is-my-boomstick "SJ Graphics: This... is my boomstick!").