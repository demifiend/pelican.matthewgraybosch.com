---
id: 1415
title: 'Why Starbreaker? Part 1: The Hard Sell'
excerpt: Why do I do this to myself? I've got reasons. I'll explain some of them here.
date: 2014-09-29T23:03:03+00:00
header:
  image: without-bloodshed-final-cover.jpg
  teaser: without-bloodshed-final-cover.jpg
categories:
  - Personal
tags:
  - Baltimore
  - Baltimore Book Festival
  - EpicBookFest
  - Why Starbreaker
  - motivation
  - pitching
  - promotions
  - sales
  - selling
  - writing
  - hard sell
  - Coheed and Cambria
---
{% include toc %}
{% include base_path %}

Several [Curiosity Quills Press](http://www.curiosityquills.com) authors encouraged me to write about _why_ I'm writing Starbreaker. The shortest, most honest answer I've got are one of the following:

  * "It's complicated."
  * <a href="/2014/10/why-starbreaker-part-2-a-labor-of-hatred/" target="_target">"Why? Because fuck you is why."</a>
  * "I don't know why I bother, either."

I'll cover some of it here, and more in subsequent posts. In the meantime, have some Coheed and Cambria while you read.

<iframe width="560" height="315" src="https://www.youtube.com/embed/P5iA4V4Mlds" frameborder="0" allowfullscreen></iframe>

## #EpicBookFest: In Which Our Intrepid Author Learns Starbreaker is a Hard Sell

I'm going to give you the devil's honest truth up front. My first Starbreaker novel, _Without Bloodshed_, is a hard sell. I was aware of this from the start, but got a taste of how hard a sell it would be back in July when I did a signing at the Barnes & Noble in Altoona, PA.

I got another taste at the 2014 Baltimore Book Festival this weekend (September 27 and 28). On Saturday, I squeezed in with [Krystal Wade](http://www.krystal-wade.com/), [Ryan Hill](http://www.ryanhillwrites.com/), [Elsie Elmore](http://elsieelmore.com/), [J. P. Sloan](http://jp-sloan.com/), and [Michael Shean](http://michael-shean.com/).

![Matthew in Baltimore, selling his shitty book.]({{ base_path }}/images/baltimore-1.jpg)

Krystal was there to sell her _Darkness Falls_ trilogy, a dark YA fantasy similar to _Alice in Wonderland_ about a young woman who enters a world without sunlight and turns out to be a prophesied savior. She also sold _Shattered Secrets_, another YA about a young woman who believes she's ordinary, and learns the hard way that she's anything but. She even had teaser copies of _Charming_ to display, which is Grimm's Cinderella meets _Saw_.

Ryan Hill was there to sell _The Book of Bart_, which is a comic YA novel best described as _21 Jump Street_ meets _Dogma_ &#8212; mainly because we couldn't count on prospective readers being familiar with _Good Omens_ by Terry Prachett and Neil Gaiman. You might find you like the story better than the cover.

Elsie Elmore was there to sell _Broken Forest_ and _The Undead: Playing For Keeps_. The latter concerns a young woman who thought surviving high school was tough until she discovers that not only can she raise the dead, but she's also gotten the Reaper's attention.

J. P. Sloan had local appeal going for him. His latest urban fantasy, _The Curse Merchant_, is set in Baltimore and concerns a magician who must win back his ex's soul from a soul-monger before it hits the open market.

Michael Shean was selling his _Wonderland Cycle_ books, along with the gaiden _Bone Wires_. He traded heavily on associations with Philip K. Dick, _Blade Runner_, and (to a lesser extent) William Gibson's cyberpunk classic _Neuromancer_.

Me? The closest I had to a short pitch was **Androids ignorant of their nature fight demons from outer space.**

If you're reading that and going, "WTF?" or "LOLWUT?", then you have something in common with the vast majority of those at #EpicBookFest who heard that pitch and either walked away or let me and Catherine (my wife) explain in greater detail before deciding they didn't want to buy a copy.

## All Things Considered, I Probably Didn't Do Too Badly

Krystal Wade sold most of her stock, leaving with only two copies of _Wilde's Fire_. I think Ryan Hill and Elsie Elmore sold out, but I'm not quite sure about Ryan. I don't know if J. P. Sloan sold out, but Michael Shean didn't.

Neither did I. Out of a stock of twenty-four copies, I sold six for $13 (tax included) on Saturday, and three for $10 (tax included) on Sunday. I also gave away four copies: one to Krystal Wade for her help and advice (and because she was tickled by the phrase "angry wizard sex") and one to a homeless Vietnam War veteran named C.B. who served in the USAF.

I also gave one to Kaliq Hunter Simms at the [Park School of Baltimore](http://www.parkschool.net) after she was kind enough to get my contact details and Krystal Wade's on Sunday. The Park School had a table next to ours in the same tent, and promoted books by former students such as _I Love You, Beth Cooper_ and _Your Face in Mine_.

I even signed one and stuck it in one of the [Village Learning Place's](http://www.villagelearningplace.org) little free libraries near Pratt St. while Catherine and I looked for somewhere to eat after closing our tent on Sunday.<figure id="attachment_3325" style="width: 840px" class="wp-caption aligncenter">

![Matthew's shitty book in one of Baltimore's Little Free Libraries]({{ base_path }}/images/baltimore-2.jpg)

So, why couldn't I sell more copies? Did I pick the wrong primary genre? Was I wrong to target an adult audience rather than aiming younger? Am I still too obscure? Or is the story just too damn complicated to compare to a couple of popular movies so I can hook potential readers?

I suspect all of these factors contributed to my results this weekend, but let's get something straight. I would have loved to have sold all twenty-four copies of _Without Bloodshed_, but nine isn't terrible &#8212; especially when the Curiosity Quills tent had to compete with a secondhand bookstore in the tent next to ours.

## Is It The Genre, Or Is It Just Me?

Yes, science fiction is a hard sell these days. I might have sold more by aiming at the YA or NA (New Adult) demographic &#8212; but _Silent Clarion_ is coming along nicely. I only published _Without Bloodshed_ last year, and I'm not exactly well-known. I'm not even Internet famous, and I don't have the figure for it.

Finally, _Without Bloodshed_ is an outrageously complicated novel. It's got a murder mystery. It's got conspiracies. It's got an effort to depose and arrest a dictator without killing him or his flunkies. It's got a psychological journey as Morgan Stormrider realizes that who he thought he was doesn't work any longer, and starts the process of figuring out who he really is all over again. It's got a huge cast, with multiple viewpoints. It's got a love story between Morgan Stormrider and Naomi Bradleigh, and _another_ romantic subplot between Isaac Magnin and Tamara Gellion (which results in angry wizard sex).

It's a gonzo sci-fi epic, and it's _only going to get **worse**_ in _The Blackened Phoenix_. I'm not joking. It's as if _2001: A Space Odyssey_ joined _The Hunger Games_ and _Guardians of the Galaxy_ for a pub crawl and picked up _The Avengers_, _The Devil's Advocate_, _Unbreakable_, and the Brosnan/Russo remake of _The Thomas Crown Affair_ along the way. Imagine further that this motley crew of movies got mugged by _Heavy Metal_, _Scott Pilgrim vs The World_, and _Killer Klowns from Outer Space_. Now imagine all of this as a rock opera similar to Queensryche's _Operation: Mindcrime_ (1988).

How the bloody blithering _fuck_ do you explain that to potential readers? I'm _lucky_ to have sold as many copies as I did, and you'd better believe I'm grateful.
