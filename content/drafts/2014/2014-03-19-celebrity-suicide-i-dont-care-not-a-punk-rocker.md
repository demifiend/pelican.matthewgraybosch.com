---
id: 278
title: 'Celebrity Suicide: I Don’t Care &#124; Not a Punk Rocker'
date: 2014-03-19T09:36:39+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=278
permalink: /2014/03/celebrity-suicide-i-dont-care-not-a-punk-rocker/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1FUB3OI
bitly_link_twitter:
  - http://bit.ly/1FUB3OI
bitly_link_facebook:
  - http://bit.ly/1FUB3OI
bitly_link_linkedIn:
  - http://bit.ly/1FUB3OI
sw_cache_timestamp:
  - 401671
bitly_link_tumblr:
  - http://bit.ly/1FUB3OI
bitly_link_reddit:
  - http://bit.ly/1FUB3OI
bitly_link_stumbleupon:
  - http://bit.ly/1FUB3OI
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - "l'wrenn scott"
  - lamestream media
  - news
  - reblog
  - suicide
format: link
---
This is one of those "troll" headlines for SEO purposes, but _Not a Punk Rocker_ has some legitimate points to make concerning the lamestream media's handling of L'Wrenn Scott's suicide. They're focusing on trivia instead of asking the important questions, such as what drives a person to suicide and how to save people who feel like they've no other option.

[Celebrity Suicide: I Don’t Care | Not a Punk Rocker](http://notapunkrocker.wordpress.com/2014/03/18/celebrity-suicide-i-dont-care/).