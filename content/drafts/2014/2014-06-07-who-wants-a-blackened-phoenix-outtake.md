---
title: Who Wants a *Blackened Phoenix* Outtake?
excerpt: "What? You don't want to read an outtake from *Blackened Phoenix*? Too bad. You're getting one anyway."
date: 2014-06-07T16:48:18+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - Last Reverie
  - Last Reverie IV
  - morgan stormrider
  - naomi bradleigh
  - outlining
  - outtake
  - pantsing
  - plot
  - the blackened phoenix
---
Trust me. You're better off this way. It's either an outtake from a novel whose plot I'm re-working so I can tighten it up and start the book with more action &#8212; or a rant about how dividing books into "Young Adult", "New Adult", and "adult" not only gives pretentious, ignorant harpies like Ruth Graham something to bitch about, but it also does a disservice to young readers by giving adults an excuse to restrict their choice of "appropriate" reading material.

You know what? I might do that rant anyway, but not today. In the meantime, have an outtake.

---

Naomi stirred as Morgan's lips brushed her neck. She molded her body against his, drawing his arms tighter around her. The bed was a tight fit with Mordred curled up behind Morgan, pinning the covers, but they managed. The blankets embraced them in a warm cocoon as Morgan caressed her hip.

She turned over, careful to avoid falling out of bed, and pecked Morgan's lips. "Better not get me started again. Aren't Eddie and Sid coming back with the girls?"

"Damn. They're probably here already. I didn't mean to fall asleep on you."

"I think I dozed off first." Naomi slipped out of bed as Morgan tried to stretch without disturbing the cat. She began to dress. "I'm glad you stayed, but may I suggest getting a bigger bed?"

"We could have used your room." Morgan opened a drawer and offered Naomi a fresh pair of socks. "How's your ankle?"

"Better, but don't ask me out dancing tonight." She took the socks with a smile. She studied the bedroom while putting them on, grateful for their thick woolen warmth, and found almost nothing that surprised her after a decade of friendship, a shared battle, and their long-overdue romance. "I wanted you to take me here. I was curious."

Instead of the Crowley's Thoth memorabilia decorating the rest of Morgan's home, Naomi found a framed photograph of the two of them from the _Voices from Home_ charity benefit they played in Armstrong City on Luna as "Voice of Reason" five years ago. Sitting atop his dresser was a figurine of a paladin with flowing white hair. _Is this how Morgan sees me?_ "What's this?"

"A Winter Solstice gift from Claire. It's supposed to be Cecilia Harvey from Last Reverie IV. She's some kind of knight who questioned her queen, lost everything, and set out to redeem herself."

"That sounds familiar. One of my brothers is a fan of old video games, and the Last Reverie series was one of his favorites." _It's been months since I spoke with anybody from my family. I should contact them._

"You should introduce him to Claire."

Naomi considered it a moment. "I'd be surprised if they weren't acquainted. Nathan joined Port Royal when he turned eighteen."

Morgan finished dressing, replacing the shirt Naomi stole for herself with a Nativity in Black t-shirt. "Astarte, are Eddie and Sid back with the ladies? Or do I have time to cook?"

Astarte appeared on the wall screen, her avatar betraying emotions Naomi never expected to see in the AI. _The poor girl seemed frightened, and angry._ "Their maglev just arrived at Grand Central. I arranged for a cab to pick them up, and ordered pizza from Luigi Vampa's, if anybody still wants to eat after I've relayed the news Binah just dumped on me."

"What kind of news?"

Morgan asked a more pertinent question. "Why would Binah contact you? If the Society had orders for me, they'd relay them through Malkuth."

Astarte shook her head. "Binah said she acted on orders from Desdinova. According to him -"

"It's probably the same story he tried to sell me at my follow-up visit. It can wait until the others are here." Annoyance tinged Morgan's voice as his hairbrush caught a tangle "He just wants me to put aside my investigation into Isaac Magnin's involvement with Liebenthal and his misuse of executive council authority. He claims the truth is more than I can handle without preparation."

Naomi took the brush from Morgan. She began to brush his hair, working her way toward his scalp. "If he knows something, he might be a valuable ally. Are you sure it's wise to keep pushing him away?"

"Probably not, but I don't trust him." Morgan leaned into her as she worked her way further up, teasing free the tangle that frustrated him. "You've walked through the evidence with me. It's not unreasonable to suggest that Isaac Magnin runs the executive council. Most of the other members follow his lead. Why not Desdinova as well? Would Magnin tolerate a dissenting voice just because it belongs to family?"

"He might be a double agent, telling Magnin what he wants to hear while reaching out to us."

Naomi looked up, and smiled at Astarte as she used her implant to review the summary of the executive council's voting record. "I'm surprised I didn't think of that first. It explains the groupthink implied by the XC's tendency towards unanimous votes."

Morgan nodded. "That tendency toward groupthink worries me, Nims. It might mean the Society's corruption isn't limited to Magnin."

"Sorry to interrupt, but Eddie and the others are here. So is the delivery from Luigi Vampa."

"Don't worry. I got this." Sid Schneider peered from behind the stacked pizza boxes he bore into the kitchen. "Better help the ladies with their luggage."

Naomi drew Sarah into a friendly hug as she limped into the living room. "You're looking better. How's your therapy?"

Sarah sighed. "Not as well as I'd like. The doctors tell me I'll eventually be able to walk without limping as long as I don't push myself too hard, but they doubt I'll ever be able to run well enough to meet the IRD corps' physical requirements."

"Then we'll have to help you train and prove the bastards wrong." Sid returned to help Morgan and Eddie with the luggage. "They have you doing any strength training yet, Sarah?"

"Not yet." Sarah shook her head. "The therapists say it's too early to bother with weight training."

"I'll show you a few things you can do without weights tomorrow, if you like." Sid lifted a case. "Claire, what the hell did you put in this thing?"

Claire somehow managed to evade everybody else's notice and get the first slice of pizza, which was laden with ground bison and bacon. "It's my industrial strength hair dryer, and I can't live without it."

Sarah shook her head. "She's bullshitting you, Sid. It's a bunch of books and papers she got from MEPOL."

"Evidence from the Crowley case?" Morgan came in, bearing a suitcase in each hand as Sarah mentioned London's police force. Mordred followed, carrying a smaller bag in his jaws. They put their bags down before Mordred greeted Morgan's new guests, while Morgan turned to Naomi. "I asked Chief Inspector Windsor to provide copies of any evidence MEPOL found in Christabel's house."

"Yeah, he and a couple of constables rocked up yesterday morning with a few boxes' worth of letters and books." Claire spoke from one side of her mouth while chewing the crust of her pizza. "They must have fixed her AI and printed out all her files and email. Her diary's there, too. I think Morgan's gonna regret reading that shit."

A text from Claire popped up in Naomi's visual field. [Don't tell Morgan, but I think Christabel's been shagging Isaac Magnin the whole time. When he finds out, there's gonna be a metric shitload of devils to pay.]

"Why would I regret reading Christabel's papers?" Morgan came out of the kitchen with a plate for himself and Naomi after serving everybody else. Naomi cut off the secure talk connection Claire initiated. "Did Christabel write some nastiness about me that she lacked the nerve to say to my face?"

Sarah shook her head, and looked down at her slice of Hawaiian-style pizza. "I shouldn't have said anything. I should have just let Claire have her stupid joke."

To Naomi's relief, Morgan seemed to have his priorities straight. "Don't worry about it. Let's eat first, and see what Astarte wanted to tell us. Christabel and her papers can wait. If she doesn't like it, she can claw her way out of the grave and tell me so."

Naomi startled at Morgan's jest. _Is he trying to prepare himself to read her papers by remembering what an insufferable bitch she was?_

After eating, everybody gathered in the living room. Naomi leaned into Morgan, loving the warmth of his body against hers as he held her while scratching behind Mordred's ears. The cat occupied half the couch while curled up beside them, and his purring conspired with the pizza and her man's casual embrace to make her drowsy. "Astarte, now that everybody's here and fed, can you tell us what's going on before I fall asleep on Morgan?"

Morgan gave her a gentle squeeze. "I wouldn't mind an early night myself. My debriefing was more grueling than usual. The psychologist seems to object to people using Witness Protocol to reconstruct memories they lose because of a head injury."

Sid looked up from his plate. "You had to deal with Dr. Kilgus as well? She gave me the same spiel a couple of years ago."

"Wasn't that after you busted that mutual-aid society executive who embezzled funds and bribed city auditors to turn the other way?"

Sid nodded to Eddie. "Yeah. While I notified him of his rights, his secretary snuck up behind me with a fire extinguisher. I stopped taking solo jobs after that."

Naomi snuggled against Morgan while listening to Sid tell the rest of his story. She perked up again as Eddie spoke to her. "Sorry. Didn't quite catch that."

"Sorry, Nims. I asked if you had any good stories."

Naomi sat up and shrugged. _I've taken my share of knocks, and Morgan's seen my scars, but I've had it easy compared to the others._ "Aside from a vampire who wasn't really a vampire, I can't tell any interesting stories. Sorry."

Eddie nodded. "You look like you just remembered some bad shit. I'm sorry."

Naomi took a breath and composed herself. "I understood the risks when I took the oath. Can we move on?"

Astarte appeared on the screen, still as anxious as earlier. Unable to meet Morgan's eyes, her gaze darted around the room. "The message Binah passed me from Desdinova is that Witness Protocol records everything Morgan sees and hear, even when he isn't on the job. He's been sending constant telemetry since his infancy. Desdinova sent proof in the form of every megabyte of data collected since his activation."

Naomi found expressions of dismay similar to her own on everybody's faces. Morgan turned away from her, as if ashamed. _He would be ashamed, even if he didn't realize Witness Protocol was active whenever he loved me._

Before Naomi found words with which to express her understanding, Morgan turned to Astarte. He ground out the words. "Arrange a meeting with Desdinova, tonight, in Central Park."

She caught Morgan's shoulder as he made to leave the room. "You're going to confront him now, when you sound like you're ready to challenge him to a duel?"

Sid rose and blocked Morgan's path. To Naomi he resembled the greater Ajax staring down Achilles. "I can't stop you from going, but you're going unarmed, and I'm coming with you."

Morgan stared up at Sid until Naomi thought he'd assault his friend. Instead, he backed off and took a breath. "That's sensible, unless you'd rather stay with your family and trust Naomi to keep me in line."

Naomi flexed the ankle she twisted while walking home with Morgan, and decided a late-night walk wouldn't kill her as long as she was careful. "It's OK, Sid. Morgan once compared me to Athena. I wonder if he remembers how Athena dealt with Achilles when he flew off the handle."

"I'd pay to see you grab him by the hair." Claire giggled from behind her laptop. "What should Sarah and I do?"

"Put on your black hats and get me some dirt on Desdinova. If I'm to ally with him, I want to be sure I can trust him not to stick a knife in my back at the first opportunity. Keep whatever you find to yourself until I can disable Witness Protocol."

Naomi stared at Morgan. _Did his anger overrule his principles? Or is he making an exception for executive council members to deal with them in the same Machiavellian style we must expect from them?_ "That's blackmail."

"Yes, it is, but I mean to present Desdinova with a choice if Claire turns up any evidence of wrongdoing on his part. If he helps me, he's safe from me until after Isaac Magnin gets his due. Otherwise, I'll nail him now, to deprive Magnin of an ally."
