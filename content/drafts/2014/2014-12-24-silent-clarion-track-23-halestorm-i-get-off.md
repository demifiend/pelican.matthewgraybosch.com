---
title: "*Silent Clarion*, Track 23: &quot;I Get Off&quot; by Halestorm"
excerpt: "Check out chapter 23 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Naomi knows seducing Renfield is a bad idea, but she's tired of being sensible."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
    - NSFW
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
He didn't kiss me through my bandage. Instead, he took my foot and cradled it, his fingertips gently massaging my instep as he kissed my ankle above the cuff of my little black sock. It was a miniscule kiss, a bare brush of moist lips, but I felt better already.

He took my other foot, but did not massage it. Instead, he kissed my ankle before placing another kiss a little further up. He then trailed warm open-mouthed kisses up my uninjured leg, until he had reached my knee. While caressing my calf, his fingertips strayed and brushed a sensitive area behind my knee that forced a sigh from my lips.

His curled in a roguish smile as I laid back. "Looks like I've found a sweet spot."

There'd be a sweet spot for him if he kept this up, and I hoped he would. My elbows relaxed as I yielded to the pleasure he offered and parted my legs a little. Would he catch my hint at where I wanted that hot mouth of his?

If he did, he was subtle about it. Lifting my leg higher, he kissed his way up my calf until he found the sweet spot behind my knee. He lingered there, treating me to quick flickers of his tongue against my hamstring that made my toes curl and my vagina clench in anticipation.

The responsible, conscientious side of me protested, insisting that I shouldn't be fucking around on the job. She was easy to placate; I wasn't abandoning my reconnaissance, but gathering HUMINT by social engineering—or should it be sexual engineering? But that was a rationalization.

The Devil's honest truth was that I wanted, and felt entitled to, some meaningless rebound sex without interminable dates and whispered endearments. Just this once, I wanted to tell a man I found appealing to drop his pants and make himself useful without having to visit Xanadu House and pay a rent boy for the privilege. The fact that this was profoundly dangerous rebound sex only made it hotter. I was going to use Renfield for my own pleasure, and the only one who could stop me was the man himself.

He hardly seemed inclined to refuse as he kneeled before me and gently draped my legs over his shoulders. Christopher Renfield was obviously a man who understood his place, so I ran a hand through his crew cut while licking my lips. "Higher."

Every brush of his lips against my inner thighs burned. He looked so good down there. The sweet torture of his kisses made me squirm against the rock; I was so close, but not close enough.

That only made things worse as I imagined myself pinned between a hungry Renfield and the granite beneath me, but I didn't care. I wanted myself caught between a cock and a hard place.

He was finally where I craved him as he kissed me lightly through my panties. He pressed a harder kiss right over my clitoris, which was in dire need of attention.

The pressure of his hot mouth made me squeal as I grabbed his head and held him in place. He soon had his hands under me and was massaging my ass while clamping his mouth around my vulva and sucking the tender flesh into his mouth.

Renfield's voice was a rough purr as he slipped his teeth between my tender skin and the sodden fabric of my panties and pulled backward. Realizing what he wanted, I lifted myself to see if he could actually manage to get my knickers off with his teeth. None of my other lovers ever managed it, though I had a couple who mastered the trick of unhooking my bra one-handed. "I think you're ready now."

Damn right I was ready. My pussy was an orchid in full bloom, my nectar flowing freely. Renfield tasted me, drawing his tongue up from my entrance to my clit, which I exposed for him by gently opening myself. I moaned and shivered beneath him, desperate to be consumed.

But that would be a surrender. Instead, I lifted his head from me. My voice was deep and rough with lust. "On your feet, soldier. Get out of that uniform."

Renfield licked his lips, smiled, and thrust my wet knickers into his back pocket. Cheeky bastard. "Yes, ma'am."

He made a striptease of it, unbuttoning his uniform shirt and drawing it open as I slowly circled my clit to keep myself hot. Once he had his shirt off, he dropped to his knees. Disregarding my order, he knelt before me again and kissed my fingertips, his tongue gently lashing my clit before dipping inside me.

He drew a long, shuddering moan from me as I stroked his hair and let him tease me for a bit before pushing him from me again. "That's insubordination. Prepare for inspection, Sergeant."

A faint hint of disappointment sharpened my pleasure when he returned to his feet with a mock salute. He submitted too easily. I wanted someone who might just have the strength to overpower me, the sort of man I've always denied myself.

The firelight dancing over the faint sheen of sweat clinging to his chest and belly made me lick my lips. Every muscle was gently defined, the product of rigorous physical training. I've fantasized about riding a dozen such men, but all of them were Adversaries and thus forbidden fruit.

Renfield should have been forbidden, but tonight he would be mine. Tonight, he would place that hot mouth, those strong, gentle hands, and what was most likely a luscious cock in my service until I had had my fill of him. "Get those pants off already, you bastard. Don't make me come over there and do it for you."

He retrieved something from a pouch on his belt. "Mind holding this for me?"

"Sure." It was a condom. Thank God he thought of it instead of making me bring it up. I narrowed my eyes as he stood on one bare foot to get his other boot off. Damn it all, this rubber expired decades ago. Fortunately, he looked good enough to eat. "Get those trousers off already."

Renfield obeyed, and bent to kiss me as I reached for him. "Closer."

Another step and he was mine. His erection stood straight up, and grand enough to fill me to the brim. It quivered as I stroked his chest and dragged my fingertips down over his abs before caressing his thighs. A little pearl of moisture gathered at his tip, and I tasted it with a slow, lingering kiss. He groaned as my tongue circled him, and I gave his balls a gentle squeeze as I tried taking more of him in my mouth.

His tip was all I could manage without my fangs hurting him. As far as I know, the inability to really go to town on a good-sized cock like his was the sole drawback of having CPMD.

Some women didn't care, leaving guys wondering if one of Dracula's brides had gotten at them, but I settled for kissing and licking and stroking him with my fingertips. He seemed to like what I was doing, because he tried to push more of himself into my mouth while stroking my hair.

Withdrawing, I gave his cock a gentle slap that made him moan. "Control yourself. This isn't all about you."

"Please." His ragged plea was barely audible as he smoothed my hair and stood at ease, his hands clasped behind his back.

If he wants to act the soldier, I'll do him like one. Crooking my finger, I smiled up at him. "Present arms."

He took a half-step forward, putting his weapon just close enough for me to do anything I liked without having to strain. Dipping my head, I kissed one of the hot balls drawn up tight beneath his root and gave a little lick before doing the other. I nipped his groin, letting his cock brush against my cheek as I trailed kisses upward.

Even his nipples were hard, all six of them. I couldn't resist tasting each one as I took him in hand and stroked him. I tasted myself on his lips before whispering in his ear. "Where do you want my mouth? Tell me."

"I'm going to ruin your jacket if you keep jerking me around. You're killing me here, Naomi."

"Am I?" I sat back on my rock and unzipped my jacket. Shrugging it off, I toyed with one of the buttons on my blouse. "Maybe I should take the rest off. And stop playing with that. It belongs to me now."

"Yes, ma'am." He stood at ease again, taking deep breaths as his cock twitched in time with his heartbeat. Was he trying to back away from the edge? His eyes were rapt as I undid every button, and shrugged off my blouse. All that remained was my camisole, for I had removed my bra at the Lonely Mountain.

My nipples strained against the fabric, and I teased him by tweaking them, sending little jolts to my hungry pussy. "Want me to take this off, too?"

Too bad if he didn't. It joined the rest of my clothes, and I sat before him with only the fall of my hair to lend me any semblance of modesty. His leaking cock was slick as I took him between my breasts. "Like that?"

He thrust upward, as if he wanted to fuck my cleavage, but I had a better idea. I dug my fingertips into his tight round arse and worshiped his cock the way he did my pussy. I dragged wet kisses upward from his base before loving his tip with my mouth, staring up at him the whole time.

When he tensed, I dug the nails of one hand into his ass while I grasped his base and his balls. Drinking deep, I took my fill as he threw back his head and cried my name.

Seconds later, his mouth was on mine. He kissed me hard, his tongue slipping deep inside. If anything, he was more ravenous than before as he made love to my breasts before trailing kisses downward. I guided him, my hands stroking his close-cropped hair.

He licked me as he had before, his tongue dipping inside me before drawing my clit into his mouth with a gentle kiss. I climbed higher every time he did it, my breathing ragged as I urged him on. When I finally came, it was with such force that I thought I'd hit escape velocity, my climax launching me screaming into orbit.

Renfield stared up at me, a smug little smile on his lips. "Where's that condom?"

Condom? Dammit, I was almost high enough to throw caution to the wind and tell him to do me bareback. "You can't use that. It's expired."

He held up his first two fingers. The claws CPMD gave him were cut short, but could still hurt me if he was careless. "I can use it to cover my fingers."

I clenched at the thought of his fingers in me as he licked me to another climax, and cast about, my hands seeking the old condom. I happened upon my first aid kit, and felt a packet inside. Pulling it out, I examined it in the firelight.

It was a brand new lubricated condom. Damn, the Phoenix Society thinks of everything when they pack these emergency kits. "How long will it take you to reload that gun of yours?"

Renfield stood, and he was already rampant. "Locked and loaded."

Tearing open the wrapper, I rolled the condom down his shaft. Once he was armored, I pulled him close and guided him.

Renfield and I were doubly joined, sharing our breath as he surged into me, and it was everything I hoped it would be when I commanded him to strip. He used me as hard as I used him, his muscular arms tight around me as I drew my legs up and wrapped them around his waist.

He redoubled his efforts, hammering me with long, hard strokes that left me almost empty before filling me again. Every impact sent a shockwave through my body that lifted me to heights I'm lucky to manage alone, let alone with a partner.

When the explosion finally happened, it left me breathless and unable to do more than whimper. I quaked beneath him, and held him close as my climax provoked his own. My shoulder burned, but it seemed inconsequential compared to the delight spreading outward from my core until it permeated my entire body and left me flushed with hot, boneless pleasure.

---

### This Week's Theme Song

"I Get Off" by Halestorm

{% youtube naIT6XfsjAw %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
