---
id: 209
title: The Web We Want
date: 2014-03-12T11:30:42+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=209
permalink: /2014/03/the-web-we-want/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1L2hcw0
bitly_link_twitter:
  - http://bit.ly/1L2hcw0
bitly_link_facebook:
  - http://bit.ly/1L2hcw0
bitly_link_linkedIn:
  - http://bit.ly/1L2hcw0
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1L2hcw0
bitly_link_reddit:
  - http://bit.ly/1L2hcw0
bitly_link_stumbleupon:
  - http://bit.ly/1L2hcw0
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - 25 year anniversary
  - censorship
  - decentralization
  - freedom
  - mozilla
  - openness
  - tim berners-lee
  - web we want
  - world wide web
---
The <a href="http://arstechnica.com/tech-policy/2014/02/tim-berners-lee-we-need-to-re-decentralize-the-web/" target="_blank">World Wide Web is 25 years old today</a>, and Mozilla is celebrating with a <a title="The Web We Want" href="https://mozilla.makes.org/thimble/web-we-want-quilt_" target="_blank">"Web We Want Quilt"</a>. Because joining other people and doing what they're doing isn't really my style, I'm going to tell you about the web _I_ want here, instead of mucking about with Mozilla's quilt.

The web I want is a web where the only law is **"Do what thou wilt, but harm none."**

The web I want is one where the only role permitted the world's governments and corporations is that of _servants_.

The web I want is a web without gods or masters.

The web I want is a web where the data I create is _my_ property &#8212; and that same property right should apply to everybody else.

The web I want is a web where artificial scarcity and planned obsolescence are not tolerated.

The web I want is a web where vital functions such as domain name resolution is decentralized.

The web I want is a web where censorship for _any_ reason is considered an unforgivable crime against the human race.

The web I want is a web run by free software implementing open standards.

The web I want is a web where _everybody_ can find a safe space and build their own community.

The web I want is not the web that the world's governments and corporations want, _and I don't give a damn_.