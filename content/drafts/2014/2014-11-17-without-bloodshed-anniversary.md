---

title: 'Without Bloodshed: First Anniversary'
cover: without-bloodshed-final-cover.jpg
coverwidth: 1800
coverheight: 2706
covercredit: Ricky Gunawan
covercrediturl: http://ricky-gunawan.daportfolio.com/
excerpt: Today is the first anniversary of the release of Without Bloodshed, part one of Starbreaker by Matthew Graybosch.
---

Today marks the first anniversary of the release of my debut, [*Without Bloodshed*](/stories/without-bloodshed/). It's the first book in the [**Starbreaker**]({{ site.url }}/about/) saga, and represents the end of a long beginning.

I had really hoped to do more to celebrate this occasion. I wanted to do a live Hangout on Air where I could answer reader questions and give a reading from *Without Bloodshed*.

Unfortunately, I'm sick today, and have been sick since I got back from the 2014 World Fantasy Convention. Dammit.