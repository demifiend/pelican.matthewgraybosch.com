---
id: 132
title: Missing Chapters
date: 2014-03-06T20:29:04+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=132
permalink: /2014/03/missing-chapters/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapWP:
  - 's:160:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:3:"140";s:5:"pDate";s:19:"2014-07-15 15:01:22";}}";'
bitly_link:
  - http://bit.ly/1LDPhFZ
bitly_link_twitter:
  - http://bit.ly/1LDPhFZ
bitly_link_facebook:
  - http://bit.ly/1LDPhFZ
bitly_link_linkedIn:
  - http://bit.ly/1LDPhFZ
sw_cache_timestamp:
  - 401653
bitly_link_tumblr:
  - http://bit.ly/1LDPhFZ
bitly_link_reddit:
  - http://bit.ly/1LDPhFZ
bitly_link_stumbleupon:
  - http://bit.ly/1LDPhFZ
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - angry robot
  - anna kashina
  - blades of the old empire
  - fantasy book watch
  - missing chapter
  - snafu
format: link
---
<a title="Publisher Announcement: Error in Blades of the Old Empire by Anna Kashina (Angry Robot)" href="http://fantasybookwatch.com/2014/03/06/publisher-announcement-error-in-blades-of-the-old-empire-by-anna-kashina-angry-robot/" target="_blank">Nice to see Angry Robot taking care of their customers and owning up when they screw up. </a>