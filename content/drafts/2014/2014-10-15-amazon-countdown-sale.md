---

title: 'Amazon Countdown Sale'
cover: without-bloodshed-final-cover.jpg
coverwidth: 1800
coverheight: 2706
covercredit: Ricky Gunawan
covercrediturl: http://ricky-gunawan.daportfolio.com/
excerpt: Are you on the fence about reading Without Bloodshed, part one of the Starbreaker saga by Matthew Graybosch? Maybe a sale will help!
---

On the fence about *Without Bloodshed*, the first Starbreaker novel? Don't want to spend five bucks on the first book in yet another series? No problem. *Without Bloodshed* is **on sale** today and tomorrow for the insane price of $.99! 

That's right, folks. One measly dollar for 320 pages of twisting, turning sci-fi conspiracy thriller goodness inspired by classic heavy metal. Take advantage of the Amazon countdown sale! Be among the first to experience the Starbreaker phenomenon, so you can brag about it to everybody who waited until the series was over to get into it.

[More information about *Without Bloodshed*]({{ site.url }}/stories/without-bloodshed)

[Purchase on Amazon](http://www.amazon.com/Without-Bloodshed-Starbreaker-Matthew-Graybosch-ebook/dp/B00GQ0BJOO)

And if you've already read *Without Bloodshed*, please consider leaving a review. I'll even take bad reviews. Don't worry about breaking my little basaltic heart; I'll just get drunk, listen to Queensryche, and get over it.