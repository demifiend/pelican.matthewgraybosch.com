---
title: A Guide to Starbreaker Fandom
categories:
  - Project News
tags:
  - diversity
  - fake geeks
  - fandom
  - fuck that noise
  - fuck-the-gatekeepers
  - inclusion
  - policing
  - rules
  - starbreaker
---
I was going to take today off, curl up with one of my kitties, and do some reading, but then I found yet another article about [gatekeeping in geekdom](http://aceofgeeks.blogspot.com/2014/05/are-you-ambassador-or-gatekeeper-by_22.html) in my stream. Yeah, I remember the bad old days when participation in obscure or outré fandom as a kid meant growing up as a social outcast. I hear those days aren't quite over yet. I can sympathize a little with the impulse to band together with other people in your fandom and form nerdy little wolf packs that bare their fangs at outsiders.

I can sympathize, but I unwilling to tolerate such idiocy in whatever Starbreaker fandom may develop as I publish more books and expand my audience. While I can probably do [a better job with diversity](http://herocomplex.latimes.com/books/beyond-game-of-thrones-exploring-diversity-in-speculative-fiction/) in my casts of characters, I want Starbreaker fandom to be a safe place for everybody.

I think the following guiding principles are reasonable ones, but you're welcome to suggest others.

## Prerequisites for Starbreaker fandom

  * You need to be alive, because it's kinda difficult to be a fan of anything when you're dead.
  * You need to be able to read English. ([Ask my publisher about translations!](mailto://marketing@curiosityquills.com)).

## Who can participate in Starbreaker fandom?

Did you read my books? Did you like them? Welcome to Starbreaker fandom. Simple, isn't it?

If enough people request it, I'll start selling t-shirts. In the meantime, here's the deal:

  * Nobody gets to tell you how to be a Starbreaker fan. As the author I outrank anybody who thinks otherwise.
  * Nobody gets to call you a fake fan or a poser because you just got into Starbreaker. I don't have time for that hipster shit.
  * Nobody gets to call you a fake fan because you aren't conversant in every little bit of trivia. Even I get some of it wrong, and I damn well wrote it.
  * Nobody gets to tell you you're doing Starbreaker cosplay wrong. I don't care if you're a 4'11" black dude and weigh 300lbs. If you want to rock a white wig and a rapier, and cosplay Naomi Bradleigh, **_more power to you._**
  * Nobody gets to say you don't belong in Starbreaker fandom because you're not a straight white cisgender Christian man. _Everybody_ is welcome, regardless of race/ethnicity/gender/sexuality/religion/class/size/nationality. _Everybody_.
  * You aren't obligated to participate in Starbreaker fandom. If you just want to enjoy the story by yourself, that's also cool.

If anybody attempts to engage in gatekeeping Starbreaker fandom, I want to hear about it. I know a guy who knows a guy who's handy with a Dragunov sniper rifle.
