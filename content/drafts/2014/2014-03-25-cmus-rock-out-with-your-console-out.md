---
id: 397
title: 'cmus: Rock Out with Your Console Out'
date: 2014-03-25T12:18:51+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=397
permalink: /2014/03/cmus-rock-out-with-your-console-out/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
sw_cache_timestamp:
  - 401713
bitly_link:
  - http://bit.ly/1N0o2ml
sw_twitter_username:
  - MGraybosch
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Running Mewnix
tags:
  - aac
  - apps
  - cmus
  - console
  - CrunchBang
  - Debian
  - HOWTO
  - Linux
  - mp3
  - mp4
  - music
  - player
  - Ubuntu
  - unix
  - vorbis
---
You’ve got a problem: you want to play some music on your computer, but you need every megabyte of RAM you can get for _real_ work. iTunes on the Mac, or Linux apps like Rhythmbox, Banshee, or Amarok are just too damn heavy. Maybe it’s time to consider a console music player, by which I mean a player which runs in a terminal, and is controlled by keyboard commands. Over on the [CrunchBang Linux](http://www.crunchbang.org) forums, [MOC](http://moc.daper.net/) is pretty popular. I prefer [C* Music Player](http://cmus.sourceforge.net/ "cmus").

## Installation

Cmus is a console player with multiple views, capable of playing just about any format you’ve got on your drive. You can install it on CrunchBang (and Debian) with the following command: `sudo apt-get install cmus`

## Adding Music

You run cmus by typing its name at a shell prompt. The first time you run cmus, you’ll see an empty tree view. You can fix that with the following command: `:add ~/music`

By typing the above, you instruct cmus to start in your home directory’s music folder, and add any music it finds to the library. Vi/vim fans may find this approach to commands familiar, but cmus also has single-key commands.

## cmus Single Key commands

I’ll list some of the common ones below, but you can find exhaustive documentation by typing “man cmus-tutorial” at your shell prompt. “man cmus” and “man cmus-remote” are also useful.

  * c – Pause/Resume
  * C – Toggle playback after current track. Turning this off will halt playback once the current song is done.
  * r – Toggle “repeat” mode.
  * s – Toggle “shuffle” mode.
  * m – Cycle between playback groups: “All from Library”, “Artist from Library”, “Album from Library”.
  * / – Freeform search (in library view)
  * n – Next search result.
  * y – Add to playlist. (Use in Tree View/Library)
  * e – Add to queue. (Use in Tree View/Library/Playlist)
  * p – Move **up** in Playlist/Queue
  * P – Move **down** in Playlist/Queue
  * D – Remove from Playlist/Queue
  * 1 – Switch to Tree View
  * 2 – Library View
  * 3 – Playlist
  * 4 – Queue
  * 5 – Directory Browser
  * 6 – View Filters
  * 7 – View Settings

## Searching and Filtering

The cmus library is searchable and filterable. The commands for searching in the library view are listed above, and it should be mentioned that searches are temporary. If you find yourself running the same searches, consider the use of _filters_.

### Using Filters

Filters are persistent searches, which cmus saves in the “$HOME/.cmus/autosave” configuration file. cmus provides a few pre-defined filters. If you had a substantial collection of classical music, you could ask cmus to show _only_ classical music in your Tree and Library views with the following command: `:factivate classical`

You might be tempted to remove the classical filter by typing the command above _again_. If you do this, you’ll find that cmus will show you everything _but_ classical — which might be what you had in mind. However, if you want to clear all filters, just type the following: `:factivate` (NB: cmus commands beginning with “:” can be tab-completed. So can filter names.)

### Creating New Filters

If you don’t have any classical music in your collection, then the default filters probably aren’t much use to you. You’ll have to make your own. The basic syntax is as follows: `:fset ==""`

Since the above notation is useful only to programmers, here are some examples:

  * `:fset metal=genre="*Metal*"`
  * `:fset soundtrack=album="*OST*|*Soundtrack*"`
  * `:fset anthrax=artist="Anthrax"`
  * `:fset 1900s=date<2001`

Experiment a bit, and you’ll discover more possibilities.

## What Can Go Wrong?

cmus is like any other software: it probably has bugs. If you changed the tags in your music, cmus won’t update its library on its own. Instead, you have to try using the “:add” command again. If that doesn’t work, you might have to exit cmus, and issue the following command before starting cmus again: `rm ~/.cmus/cache`

Simply adding new acquisitions shouldn’t be as problematic, however.

## Configuring cmus

Also, cmus doesn’t look as nice as my screenshot by default. You have to crack open “~/.cmus/autosave” and look for lines beginning with “set color”. [You can use my config as an example.](https://github.com/demifiend/dotfiles/blob/master/.cmus/autosave)

## Got Questions?

If you have questions, post a comment. I'll do the best I can to answer.