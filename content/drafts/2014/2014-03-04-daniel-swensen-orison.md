---
title: 'Daniel Swensen: Orison'
date: 2014-03-04T22:49:14+00:00
author: Matthew Graybosch
categories:
  - Books
tags:
  - daniel swensen
  - dragons
  - epic fantasy
  - indie
  - magic
  - orison
  - thief
  - wizard
---
I'm halfway through _Orison_ by Daniel Swensen, the first part of his **Lotus Throne** sequence. It's a damn good epic fantasy debut.

And it would be nice if I could spell the man's name right on the first attempt. It's not like it wasn't right on the cover.<figure style="width: 1056px" class="wp-caption aligncenter">

[<img alt="Cover for Orison by Daniel Swensen" src="http://i0.wp.com/ecx.images-amazon.com/images/I/91pZ5hjGDRL._SL1500_.jpg?resize=840%2C1193" data-recalc-dims="1" />](http://www.amazon.com/Orison-Daniel-Swensen-ebook/dp/B00IOQF4AI)<figcaption class="wp-caption-text">Cover for Orison by Daniel Swensen</figcaption></figure>

## About _Orison_

Story lives as a thief in the free city of Calushain, and has a plan to escape to better life. But when her stash of money is stolen by her brother, she finds herself faced with a death sentence from her crime lord boss.

Desperate to pay off her debt, she searches for a score big enough to earn her freedom. Instead, she finds the orison, a magical artifact that could tip the balance of power between the city and the Empire seeking to conquer it.

The power to change the world is now in the hands of a sneak thief — if it doesn’t kill her first.

## About Daniel Swensen

> Born and raised in the hinterlands of Montana, I grew up on a steady diet of Star Wars, Robert E. Howard, and cheap B-flicks. I've written everything from game books to cell phone ads to fiction of all genres. My first love will always be heroic, epic fantasy (or "traditional" fantasy, if you roll like that).
