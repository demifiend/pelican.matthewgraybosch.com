---
title: "The Art of Starbreaker"
excerpt: "Meet some of the major characters of Starbreaker, as depicted by various artists."
header:
  image: starbreaker_-_without_bloodshed_-_harvey_bunda_alternate_cover_cropped.jpg
  teaser:
    starbreaker_-_without_bloodshed_-_harvey_bunda_alternate_cover_cropped.jpg
categories:
  - Backstage
tags:
  - cast
  - characters
  - Cinamonspice
  - Sara McSorley
  - Harvey Bunda
---
{% include base_path %}

A few artists have depicted major characters from Starbreaker, but not so many that I can't gather their work here, unfortunately. If I've missed something, please let me know in the comments.

## Naomi Bradleigh by Cinamonspice @ DeviantArt

This is the first piece of artwork somebody's done for Starbreaker, way back in 2011.

[![Naomi Bradleigh by Cinamonspice]({{ base_path }}/images/starbreaker_by_cinamonspice-d3baj7o.png)](http://cinamonspice.deviantart.com/art/Starbreaker-200365764){: .align-center}

## Original Starbreaker cover by Sara McSorley

This was the first cover ever done for *Starbreaker* in autumn 2011, back before Lisa and Eugene at [Curiosity Quills Press](http://curiosityquills.com) decided it was too big a story to publish as one novel.

![Original Starbreaker cover by Sara McSorley]({{ base_path }}/images/nouveau_cover.jpg){: .align-center}

## Character art by Harvey Bunda

Back in 2012 I had cash to spare because I was extorting my day job for time and a half for every hour of overtime they got out of me (don't just me; this is America and time-and-a-half is every worker's right). So, I decided to work with Filipino artist [Harvey Bunda](http://harveybunda.com) to get some character art done up to promote Starbreaker.

<img class="size-large wp-image-437" alt="Ashtoreth, by Harvey Bunda" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/ashtoreth_harveybunda-714x1024.jpg?resize=714%2C1024" data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/ashtoreth_harveybunda.jpg)<figcaption class="wp-caption-text">Ashtoreth, by Harvey Bunda</figcaption></figure>

**Ashtoreth** doesn't have to remember the days when men knelt before her and called her a goddess, because some still do. Is she an ally to our heroes, or an enemy? Perhaps she has her own agenda.<figure id="attachment_439" style="width: 714px" class="wp-caption aligncenter">

[<img class="size-large wp-image-439" alt="Christabel Crowley, by Harvey Bunda" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/christabelcrowley_harveybunda-714x1024.jpg?resize=714%2C1024" data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/christabelcrowley_harveybunda.jpg)<figcaption class="wp-caption-text">Christabel Crowley, by Harvey Bunda</figcaption></figure>

**Christabel Crowley** blazed as the star violinist of neo-Romantic heavy metal act **Crowley's Thoth** until a brutal murder stilled her song, but is her tale truly over?<figure id="attachment_442" style="width: 662px" class="wp-caption aligncenter">

[<img class="size-large wp-image-442" alt="Claire Ashecroft, by Harvey Bunda" src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/claireashecroft_harveybunda-662x1024.jpg?resize=662%2C1024" data-recalc-dims="1" />](http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/claireashecroft_harveybunda.jpg)<figcaption class="wp-caption-text">Claire Ashecroft, by Harvey Bunda</figcaption></figure>

**Claire Ashecroft** might act like an oversexed otaku, but few can match her ability to sweet-talk an AI, charm her way into a secured location, or wage electronic warfare on her friends' behalf.<figure id="attachment_444" style="width: 454px" class="wp-caption aligncenter">

[<img class="size-large wp-image-444" alt="Desdinova, by Harvey Bunda" src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/desdinova_harveybunda-454x1024.jpg?resize=454%2C1024" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/desdinova_harveybunda.jpg)<figcaption class="wp-caption-text">Desdinova, by Harvey Bunda</figcaption></figure>

**Desdinova**&#8216;s gravely digs may surely prove a sight, but this surgical wizard in grey carries secrets that might shatter the Phoenix Society.<figure id="attachment_445" style="width: 662px" class="wp-caption aligncenter">

[<img class="size-large wp-image-445" alt="Edmund Cohen, by Harvey Bunda" src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/edmundcohen_harveybunda-662x1024.jpg?resize=662%2C1024" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/edmundcohen_harveybunda.jpg)<figcaption class="wp-caption-text">Edmund Cohen, by Harvey Bunda</figcaption></figure>

**Edmund Cohen** is a man of few virtues, among them self-awareness; loyalty to friends who stand by him despite his drinking, drugging, and whoring; and deadly aim with a Dragunov.<figure id="attachment_446" style="width: 662px" class="wp-caption aligncenter">

[<img class="size-large wp-image-446" alt="Imaginos, by Harvey Bunda" src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/imaginos_harveybunda-662x1024.jpg?resize=662%2C1024" data-recalc-dims="1" />](http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/imaginos_harveybunda.jpg)<figcaption class="wp-caption-text">Imaginos, by Harvey Bunda</figcaption></figure>

**Imaginos** became a demon to fight demons. What sort of man becomes what he despises for the good of his people? Is such a man truly a villain? Could a man with thousands of megadeaths to his name be a hero? Either way, he proves you can't trust a white-haired bishounen.<figure id="attachment_447" style="width: 662px" class="wp-caption aligncenter">

[<img class="size-large wp-image-447" alt="Morgan Stormrider, by Harvey Bunda" src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/morganstormrider_harveybunda-662x1024.jpg?resize=662%2C1024" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/morganstormrider_harveybunda.jpg)<figcaption class="wp-caption-text">Morgan Stormrider, by Harvey Bunda</figcaption></figure>

**Morgan Stormrider** never doubted his work as one of the Phoenix Society's Adversaries until a duel with a rival in Shenzhen cracked his faith. He wants nothing more than to put aside his sword and dedicate himself to music, but learning the truth about the Phoenix Society did not set him free.<figure id="attachment_448" style="width: 614px" class="wp-caption aligncenter">

[<img class="size-large wp-image-448" alt="Naomi Bradleigh, by Harvey Bunda" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/naomibradleigh_harveybunda-614x1024.jpg?resize=614%2C1024" data-recalc-dims="1" />](http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/naomibradleigh_harveybunda.jpg)<figcaption class="wp-caption-text">Naomi Bradleigh, by Harvey Bunda</figcaption></figure>

**Naomi Bradleigh** never looked back when she resigned her commission with the Phoenix Society and launched a musical career that led her to form **Crowley's Thoth** with **Morgan Stormrider** and **Christabel Crowley**. When a dirty cop tries to frame Naomi for Crowley's murder, she takes up her sword anew and fights beside Morgan.<figure id="attachment_495" style="width: 662px" class="wp-caption aligncenter">

[<img class="size-large wp-image-495" alt="Thagirion, by Harvey Bunda" src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/thagirion_harveybunda_small-662x1024.jpg?resize=662%2C1024" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/thagirion_harveybunda_small.jpg)<figcaption class="wp-caption-text">Thagirion, by Harvey Bunda</figcaption></figure>

**Thagirion** is the eldest of the Disciples of the Watch, and sworn to keep the Starbreaker from the wrong hands. She is the only one **Imaginos** acknowledges as his equal. What will happen when she decides **Imaginos** can no longer be trusted with the one weapon capable of killing gods?<figure id="attachment_499" style="width: 800px" class="wp-caption aligncenter">

### Alternate cover for *Without Bloodshed*

This is a cover Harvey did for *Without Bloodshed* on spec, but Curiosity Quills decided against using it. I still think it's fucking metal. :metal:

![Alternate cover for *Without Bloodshed* by Harvey Bunda]({{ base_path }}/images/starbreaker_-_without_bloodshed_-_harvey_bunda_alternate_cover.jpg){: .align-center}

![Detail: Naomi Bradleigh]({{ base_path }}/images/wb-alt-cover-harvey-bunda-naomibradleigh.jpg){: .align-center}

![Detail: Morgan Stormrider]({{ base_path }}/images/wb-alt-cover-harvey-bunda-morganstormrider.jpg){: .align-center}
