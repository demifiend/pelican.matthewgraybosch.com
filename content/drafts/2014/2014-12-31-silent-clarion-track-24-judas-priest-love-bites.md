---
title: "*Silent Clarion*, Track 24: &quot;Love Bites&quot; by Judas Priest"
excerpt: "Check out chapter 24 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Blood play isn't one of Naomi's kinks."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
The afterglow faded, but the burn in my shoulder remained. It flared as Renfield's tongue lapped at the wound. Had he bitten me after fucking me to an utterly delicious climax? Normally I like a good love bite, but now he was feeding on me like some kind of vampire bat.

Overcome by loathing, I pushed him off and grabbed my sword. He scrambled to his feet, eyes wide with shock. Holding my blade between us, I pressed my other hand against my torn shoulder. "What the fuck is wrong with you?"

"What? What did I do?"

"Don't play the bloody innocent with me. You bit me." A hickey is one thing, but drawing blood is right out. "Are you telling me you had no idea what you were doing?"

Renfield slowly shook his head. His eyes seemed clearer when he finally looked at me again. "Oh, shit. I'm sorry. I never meant to do that."

Lowering my sword just a little, I noticed he was still hard. Maybe he just liked getting smacked around? "You seemed to have been enjoying yourself."

Flushed with shame, he pulled the used condom from his shaft and tossed it into the fire. "It looks like I owe you an explanation. Can we get dressed first? I'll tell you everything I can."

"Stay there. I'll give you your clothes after I've tended to my shoulder." Fortunately, Renfield had the common courtesy to bite somewhere I could easily reach. That allowed me to dress the wound without his help. Once I was done, I threw his clothes to him and checked the dressing on my leg before pulling my jeans back on. He still had my knickers, but wet panties where the last thing I wanted to put back on.

Once I was dressed and had regained some composure, I joined him by the fire. If he hadn't ruined it, I might have settled beside him and rested my head on his shoulder. Instead, I sat across from him with my sword resting across my lap. "Start talking."

"I probably sounded crazy to you when we first met. I know Nationfall was decades ago, but it's easier to keep up the pretense. If I keep my story straight, my secrets remain safe, and so do my men. If people mistake me for a re-enactor or military nerd, they won't come looking for a forgotten Commonwealth Army unit." Renfield spoke slowly, staring into the fire. His voice was haunted, and he slumped with the weight of decades. Yielding to my lust for him was probably a huge mistake on my part, regardless of whatever info I might gather while he's purry and contemplative. How the hell did I not realize he was old enough to be my great-grandfather? It's not like I'd have taken Edmund Cohen to bed, but at least he has the common decency to look his age. "Do you understand what I'll be doing if I give you the explanation you deserve?"

It was easy to guess, given Renfield's behavior. He was somehow involved with Project Harker, but in what capacity? "You need not betray your country or your men. But I've brought locals to Fort Clarion. You're going to have to join the rest of us in this century."

He spat into the fire. "Moving on might be possible for me, but I'm not sure about my men. We're not what we were when we enlisted. The Commonwealth did something to us and then abandoned us. The government collapsed, the Prime Minister ate his own gun, and nobody thought to release us from service. There was nothing for us outside Fort Clarion, so we clung to our last set of orders: protect the base at any cost."

Was Renfield part of an elite unit? "Can you tell me more about Dusk Patrol? Sounds like your unit had strong espirit de corps."

"We were Third Infantry's all-CMPD platoon, and we were damn-near unstoppable." Even now, decades later, he sat up straight and spoke proudly of his outfit. "You could drop us in the middle of a clusterfuck with nothing but our BDUs, and we'd still get the job done."

Nothing but their combat fatigues? Did the Commonweath brass expect them to loot their weapons, ammo, and rations from the bodies of the first enemies they managed to strangle? I suppose that's one way to give taxpaying citizens a break. "Trying to impress me?"

"You sounded pretty impressed earlier, at least until I bit you and ruined everything."

Good point, but I wasn't going to let him get away with it. I was still angry, and I had yet to find the answers I want. "Don't get cocky. Just get to the point."

"Right. We were some of the best the Army had, especially if you wanted to drop a team behind enemy lines to raise Hell. If you told us to take out a supply depot, we'd not only do it, we'd fuck with the enemy's heads while we were there."

"So, you combined psychological and unconventional warfare?"

"Yeah. One job, we went to West Africa to take out a bunch of nutjobs who had taken to kidnapping schoolgirls and selling them into forced marriages. I'm talking kids no older than thirteen. Another division already rescued the last batch of girls these assholes kidnapped, so we went in to do the local government a favor and make sure the terrorists would never pull a stunt like that again."

"What did Dusk Patrol do?" Whatever it was, I had a sneaking suspicion the Society would call it a war crime.

"We got the leader, brought him to a pig farm, slit his throat, and chucked him in the pen. We recorded the pigs eating him on video, and sent the video to all his cronies with a little note telling them they'd be next if they didn't learn to respect women."

"And the pigs didn't mind engaging in cannibalism?" The question slipped from my lips before I could stop myself, and I immediately regretted it. It was too flippant, and thus unbecoming of an Adversary.

He didn't seem to mind, though. He began to laugh, but suppressed his amusement. "No, the swine didn't seem to mind. But they got pretty fat before people got the message."

Bloody hell. How many militants did Dusk Patrol feed to swine? And what am I to do with such information? Would the Society prosecute him and the surviving members of Dusk Patrol as war criminals? What would be the point? Anybody capable of testifying in court is most likely dead by now. But why isn't Renfield? He doesn't look any older than me, which is impossible. Isn't it? "How do I know you aren't bullshitting me? You don't look like the old man you should be."

He nodded. "Didn't your parents tell you? People with CPMD don't age like regular people once they hit their mid-twenties. At least, I've never seen somebody like me looking old. I'm not sure anybody knows why."

"I was adopted. My parents don't have CPMD."  And if Renfield wasn't bullshitting me, I might have to change my name and pretend I'm my own daughter in another twenty years, like in *Exiled Goddess*. Not that Renfield would have seen that movie. Too recent.

But how exactly would Sophie and Howell react to my perpetually youthful appearance as they continue to age? What about my brothers? Would they eventually treat me differently? It's one thing to read about CPMD on the network, but living with it, or somebody who has it, is probably a different story.

Maybe Renfield is different. He isn't the first carrier I've met, but what would happen if a carrier and a normal person had children? Is that even possible? "But what does that have to do with you lapping at my blood? Unless you mean to tell me you're part of a squad of vampire soldiers like in that old D Corps series, I don't know what you're going on about."

"Sounds like you read a few of those books yourself." He paused a moment as if recollecting. "They were a hit with the squad, but after reading a few, some of the guys decided the next time they had to take out sentries on a raid, they'd make it look like Dracula got 'em. They'd rip out some poor bastard's throat with their teeth instead of using a knife."

"That sounds more like torture than psychological warfare."

My disapproval must have been evident from my tone, because Renfield raised his hands as if to ward off a blow. "I didn't like it either, and neither did the rest of the outfit. We made them cut that shit out, but not before the brass found out."

"What happened? Were you punished?"

Renfield shook his head. "No. The goddamn brass were delighted. After all, a lot of the people we were sent to fight were superstitious. They weren't afraid to fight men, but blood-drinking fiends who strike in the night were a different story."

"But you weren't actually vampires then, were you?" Everything Renfield told me thus far indicated that the vampirism was just an act no different from Jacqueline pretending to kiss me when she wanted to fend off a man's unwanted attention. It didn't explain why he had fed upon me.

"No, but some bright lights in the Army Medical Corps decided to fix that." His tone turned bitter. "We already had sharper teeth and superior night vision because of our condition. Army Medical worked on making fiction reality. They wouldn't tell us what they were doing, or explain the side effects. According to them, we didn't need to know even though they were doing it to us. When some of us refused to participate, they decimated us. One out of every ten men got a bullet to the head, even if they had cooperated with the experiments."

If the Commonwealth Army could treat its men so harshly, what was life like for civilians? Had the Commonwealth become some kind of police state toward the end? Regardless, Renfield's talk of medical experiments rang a cathedral's worth of bells in my head, so I made an educated guess. "Was Dr. Henrik Petersen involved? If my intel is right, he would have held the rank of colonel at the time."

"Petersen? No." Renfield shook his head, and his tone softened. "He wasn't a doctor at the time. Dusk Patrol was his idea, and he looked out for us the best he could. He spoke up for us with the brass, but they wouldn't listen to him. We weren't people to the War Department; we were just weapons to be upgraded."

The fire had begun to fade to embers, and with it, the throb in my torn shoulder, but despite the information he provided, I still had more questions than answers. I put more wood on the fire, and watched as the flames tasted the fresh fuel, little red tongues flicking at the wood before flaring to full brilliance and pushing back some of the shadows cast by his tale.

Whatever the Commonwealth Army Medical Corps did to the men of Dusk Patrol, it must have succeeded. Renfield had tasted me, and no doubt derived some nourishment by doing so. Would he have drained me dry, given the chance? Was that even the central question?

As we sat in silence around the fire, I couldn't help but think that why he bit me wasn't the most important issue. Fort Clarion was. What happened to the men of Dusk Patrol there, and what role did Petersen play in what I was beginning to suspect was a tragedy. What did Fort Clarion have to do with Project Harker? What the hell was Project Harker, anyway? Was it an effort to turn men into weapons, or something more? And was it confined to Fort Clarion? Time for a shot in the dark. "Sergeant Renfield, what can you tell me about Project Harker?"

He narrowed his eyes, glaring at me through the firelight. "You are a goddamn spy. I knew it." The flames concealed his movements until he leaped over them and tackled me, the knife in his hand trembling against my throat. "Who sent you, and how much did they tell you? Start talking!"

---

### This Week's Theme Song

"Love Bites" by Judas Priest, from *Defenders of the Faith*

{% youtube PWFkhLUcvo0 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
