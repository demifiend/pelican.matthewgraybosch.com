---
title: Welcome to My Existential Crisis
date: 2014-03-06T08:36:12+00:00
excerpt: Don't mind the title. I just needed something snappy, and I was listening to Alice Cooper.
header:
  image: author2.png
  teaser: author2.png
categories:
  - Personal
tags:
  - alice-cooper
  - existential crisis
  - existentialism
  - google
  - lisa cohen
  - meaning
  - purpose
  - simone de beauvoir
  - suicide
  - the second sex
  - welcome to my nightmare
---
Don't mind the title. I'm not experiencing an <a title="Existential crisis" href="http://en.wikipedia.org/wiki/Existential_crisis" target="_blank">existential crisis</a> right now, but I wanted a snappy title for this post and I was listening to Alice Cooper's <a title="Welcome to My Nightmare" href="http://en.wikipedia.org/wiki/Welcome_To_My_Nightmare" target="_blank"><em>Welcome to My Nightmare</em></a> album. I think I also used the title as that of a fictional album by a fictional punk band called <a title="Simone de Beauvoir - The Second Sex" href="http://en.wikipedia.org/wiki/The_Second_Sex" target="_blank">The Second Sex</a> in [Starbreaker](http://www.matthewgraybosch.com/starbreaker/ "Starbreaker").

I was going to comment on this Google+ post, but decided to make it a blog post instead.

<!-- Place this tag where you want the widget to render. -->

<div class="g-post" data-href="https://plus.google.com/113113447782383069533/posts/M2aKBonp7wo">
</div>

<!-- Place this tag in your head or just before your close body tag. -->

---

I wasn't a high achiever in school. I never saw the point. I just saw school as a meaningless game I was compelled to play by law, with no consideration given as to what _I_ wanted. Yes, I had a bad attitude.

When I graduated from high school, I looked at my choices, and found none of them palatable. I could go to college and bust my ass so that I could spend my life busting my ass to make rich people even richer. I could join the military, and kill or be killed for rich people and their empire. I could try getting a job without going to college, and make rich people richer for a smaller paycheck. Or I could pretend I still believed in God and enter religious life.

Because I didn't care for any of the options, I began to think life wasn't worth living. However, I wasn't ready to give up, because that would just be handing the bastards who made life in America suck for everybody else a victory.

I don't want to be pretentious and call what I went through an existential crisis, but I realized when I was 18 that I had to determine the meaning and purpose of my life, and nothing I learned at school gave me the tools to do so.

I suspect that this is the same for most young people today. We teach them everything but what really matters. We don't teach them how to find a sense of purpose for themselves, we don't give them the philosophical tools to impose meaning on life, and I think that's why too many young people who seem to have every reason to live end up killing themselves.

Am I completely off base? Tell me what you think in the comments section.
