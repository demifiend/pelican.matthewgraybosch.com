---
title: U2 in *my* iTunes account? DO NOT WANT!
excerpt: I don't need Apple *or* U2 doing me any favors, thank you ever so much.
header:
  image: u2-songs-of-innocence.jpg
  teaser: u2-songs-of-innocence.jpg
categories:
  - Marketer Abuse
  - Music
tags:
  - Apple
  - captive audience
  - consent
  - do not want
  - iPhone
  - Metallica
  - push marketing
  - U2
---
Seems Apple has decided to stick U2's new album on people's iPhones even if they didn't request a copy on the iTunes store. [Some people are understandably annoyed.](http://www.independent.co.uk/arts-entertainment/music/news/tyler-the-creator-compares-having-the-new-u2-album-automatically-downloaded-on-his-iphone-was-like-waking-up-with-herpes-9732091.html)

I don't blame them. For starters, U2 have a lot in common with Metallica. They're over-privileged short-haired rock stars who haven't released a good album in years. (Sorry, but _Death Magnetic_ doesn't redeem _St. Anger_.)

I'd be miffed as well if Google, LG, or T-Mobile tried a stunt like this.

  1. I don't like U2.
  2. I don't like push advertisements.
  3. I don't like having my storage used without my consent.
  4. I don't like strangers deciding what music I should hear.

Hell, I'd be miffed if Amazon did this to Kindle owners, and I'd go from miffed to Homeric rage in less than 60 seconds if Amazon was putting _my_ books on people's devices without their consent.

**I don't want a captive audience.**

What I don't get is why U2 wants to shove their latest album down the throats of people who have expressed no interest in U2. Did Bono give all his money away and realize he didn't hold any back for hookers and blow? Is this some kind of ego trip?

Regardless, I'm glad I jumped the Apple ship when Steve Jobs kicked the bucket. The company's as fucked as it was the last time Jobs was removed from the CEO's office, but this time the board of directors will need a good necromancer.

Of course, some people like [Bernard Zeul at the _Sydney Morning Herald_](http://www.smh.com.au/entertainment/music/free-u2-album-delete-it-when-you-want-so-why-the-whingeing-20140915-10h1n4.html) don't get it. They don't realize that removing unwanted music is unnecessary work, and that it still shows up in your history where it can be used to embarrass you if you're the sort of person whose friends judge people by their playlists.

(If your friends do this, get better friends. If you do this, fuck off and die.)
