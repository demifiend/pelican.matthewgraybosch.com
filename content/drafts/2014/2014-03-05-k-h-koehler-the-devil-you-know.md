---
title: 'K. H. Koehler: The Devil You Know'
categories:
  - Books
tags:
  - angels
  - demons
  - ex-cop
  - indie
  - k. h. koehler
  - murder
  - nick engelbrecht
  - paranormal
  - pennsylvania
  - reissue
  - the devil you know
  - urban fantasy
  - witchcraft
---
I first read _The Devil You Know_ when <a title="K. H. Koehler Books" href="http://khkoehlerbooks.wordpress.com" target="_blank">K. H. Koehler</a> published it through Curiosity Quills Press. Now that she's gone indie again, she's taken the opportunity to re-issue the book with a new, stark, minimalistic cover.

I recommend it to fans of adult supernatural thrillers. It's how <a title="Hellblazer" href="http://en.wikipedia.org/wiki/Hellblazer" target="_blank">Hellblazer</a> _should_ have been Americanized, assuming it was ever necessary to strip John Constantine of his Britishness.

This re-issued edition also includes her novelette _And Death Shall Have No Dominion_.

<figure style="width: 938px" class="wp-caption aligncenter">

[<img alt="The Devil You Know, by K. H. Koehler" src="http://i0.wp.com/ecx.images-amazon.com/images/I/71I3RPFhdML._SL1500_.jpg?resize=840%2C1343" data-recalc-dims="1" />](http://www.amazon.com/The-Devil-Know-Nick-Englebrecht-ebook/dp/B00IQWNB1Y/ref=cm_cr_pr_product_top)<figcaption class="wp-caption-text">The Devil You Know, by K. H. Koehler</figcaption></figure>

**Sympathy for the Devil&#8230;**

Not only does the devil have an only begotten son, but he's currently residing in the rural town of Blackwater in northeast Pennsylvania. Semi-retired from law enforcement, the handsome, if cynical, Nick Englebrecht becomes quickly caught up in a local missing child case that seems mundane on the outside, but when the sheriff requests his help as a psychic detective to help find the missing girl, his off-the-books investigation quickly leads him to some terrible truths about life, love and the universe as we know it. And if that isn't bad enough, the angels have begun an ethnic cleansing of all beings with demonic blood. Of course, Nick is at the top of their to-do list.

## About the Author

K. H. Koehler is the author of various novels and novellas in the genres of horror, SF, dark fantasy, steampunk and young adult. She is an associate editor at KHP Publishers, owner of K.H. Koehler Books and a partner in The Job Octopus, which specializes in editing and cover design. Her books are available at all major ebook distributors, her covers have appeared on many books in many different genres, and her short work has been featured on Horror World, Literary Mayhem, and in the Bram Stoker Award-winning anthology Demons, edited by John Skipp. She lives in the beautiful wilds of Northeast Pennsylvania with two very large and opinionated Rottweilers. She welcomes reviews and fan mail and you can contact her via her website: <a title="K. H. Koehler Books" href="http://khkoehlerbooks.wordpress.com" target="_blank">khkoehlerbooks.wordpress.com</a>
