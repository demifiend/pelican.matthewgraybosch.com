---
title: My Fans on Google+ Love Without Bloodshed
categories:
  - Project News
tags:
  - fans
  - google
  - plugs
  - praise
  - reviews
  - silent clarion
  - without bloodshed
---
I'll admit that my efforts on social media tend to be a bit lackluster, and focus primarily on Google+. However, my efforts paid a small dividend recently in the form of praise by three Plus users: Rowan Cota, Daniel Koeker, and Lisa Cohen. Because I love my fans, I'm going to let them speak for yourselves. You should follow these people, and buy their books if applicable.

(Note: I solicited the authors' consent before embedding their posts.)

## Rowan Cota on _Silent Clarion_

Rowan's an all-around geek, and writes some great material about social justice and intersectional feminism both on Plus and her blog, [Sugar on the Asphalt](http://sweetpavement.wordpress.com/). She had [the following to say about me](https://plus.google.com/100230371328294653162/posts/3M8ejWyfZqd).

> If you haven't read Starbreaker, I recommend it! That said, I'm excited to see this going up! Matthew Graybosch is a good author, with stuff to say. If you're not familiar with his work, it's worth checking out! 🙂

I had no idea she read my fiction, or liked it. I'm serious.

## Daniel Koeker on _Without Bloodshed_

[Daniel Koeker](http://danielkoeker.com/index.html), author of _The Dream Sanctum_, is another Plus user I've followed for a long time. I can't comment on his books, since I haven't read them, but he writes excellent articles on a broad range of subjects. Seems he gets paid for it, too. (I really should find a way into that racket.)

Anyway, [he was kind enough to say the following on Plus](https://plus.google.com/116547530413162810816/posts/hYYEoqschgX).

> If you're an active user of Google+ and an avid reader there's a good chance you've heard of Matthew Graybosch's novel, Without Bloodshed, which I have just (belatedly, and apologetically) finished reading. If not, or if you need any further motivation to get yourself a copy, I highly recommend picking it up. It's a definite 5/5 for me.﻿

Better still, he was _also_ kind enough to [review my novel on Amazon, and gave it five stars](http://www.amazon.com/review/R3KZXL50R65WQQ/ref=cm_cr_pr_perm?ie=UTF8&ASIN=B00GQ0BJOO). He had the following to say:

> In a word, brilliant. In more words, I love the world Graybosch has created as well as the characters in it. This novel is utterly compelling, and though it may sound cliche, most assuredly a page turner. It's fast paced and clever, and overall, an excellent read.
>
> I hold a great appreciation for the wit of the characters and the depth of the plot, including twists up until the very end. It was extremely easy to immerse myself in this book. I regret the time it took me to start reading it, but will correct that mistake upon the arrival of the sequel I so eagerly await.

This is just the sort of thing I like to hear. I love an enthusiastic review, but I'll level with you guys. I'll take every review I can get. I'll even take an honest one-star review, or a funny one from a butthurt parent thinking to give this to their kid only to find it's full of sex, violence, and rock &#8216;n roll.

## Lisa Cohen on _Without Bloodshed_

As happy as I was with the posts by Rowan Cota and Daniel Koeker, Lisa Cohen's post _really_ made my day. [Her most recent book is _Derelict_](http://www.amazon.com/Derelict-LJ-Cohen-ebook/dp/B00KNFS8OW/) a tightly written space opera in the tradition of Heinlein's juveniles. <a href="http://www.matthewgraybosch.com/2014/07/02/lj-cohen-derelict-worthy-heinlein/" title="LJ Cohen’s Derelict: Worthy of Heinlein" target="_blank">I recently reviewed it here</a>, as well as on Amazon, without expecting anything in return.

I was surprised, therefore, [to see this post come up while getting my car inspected](https://plus.google.com/113113447782383069533/posts/KNo478xZ4jw).

> Last night I finished +Matthew Graybosch's WITHOUT BLOODSHED. An ambitious SF novel that I enjoyed quite a lot and would absolutely recommend, both for its intriguing world building and for the sprinkling of nerd-pop references throughout. (They amused me, but weren't at all annoying.)
>
> The story really spans/straddles multiple genres with futuristic/alt history elements and a heavy thriller aspect. It reminded me a litle of the complex thrillers I loved in my 20s and 30s, and most strongly of SHIBUMI for the elements of a singular protagonist working through a world-wide political conspiracy.
>
> My main complaint is that the leaders of the conspiracy have too little &#8216;face time' in the story. Ultimately, I was confused at spots in terms of who was who and what their aims and goals were. I don't want a book to spoonfeed me, and I don't mind when a characters is confused, (part of the joy is watching the character put the puzzle together) but if the story is going to have the antagonists have POV scenes, they should inform the reader more clearly.
>
> Part of the problem was that each of the &#8216;antagonists' (in quotes because they are not necessarily &#8216;bad' guys and not all of them are on the same side) has multiple names and identities and some of the terms they use are never fully explained in the context of the story.
>
> None-the-less, it was a page turner and I absolutely loved the main character, Morgan (and the little homage to Buckaroo Bonzai!) and Naomi is a wonderful, fully realized heroine. The world building is very strong, with small details supporting it well. WITHOUT BLOODSHED isn't a beach read &#8211; with a large cast of characters and a big twisty plot, you have to be an active reader, but the story rewards you with a ton of enjoyment.
>
> So thank you, Matthew, and I am looking forward to the sequel.
>
> (It's not a cliff-hanger ending, thank all the gods of writing, or I'd have been forced to hunt Matthew down and hurt him!)﻿

Don't feel bad if you were confused by Ms. Cohen's to _[Shibumi](http://en.wikipedia.org/wiki/Shibumi_%28novel%29)_. I had to Google it, because I am unfamiliar with [Trevanian](http://www.trevanian.com/)&#8216;s work. It seems _Without Bloodshed_ has a similarly byzantine plot.

Have you read _[Without Bloodshed](http://www.matthewgraybosch.com/without-bloodshed)_ yet? If so, I'd love to see what you think in the comments.
