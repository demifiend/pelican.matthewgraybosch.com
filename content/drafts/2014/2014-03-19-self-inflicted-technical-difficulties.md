---
id: 282
title: Self-Inflicted Technical Difficulties
excerpt: This post is outdated, and retained for historical purposes.
date: 2014-03-19T10:36:14+00:00
header:
  image: waiting_spider_web.jpg
  teaser: waiting_spider_web.jpg
category: Website
tags:
  - feeds
  - self-inflicted
  - technical difficulties
  - theme
  - WordPress
---
Don't panic if you see the site acting a bit wonky. I've run into some self-inflicted technical difficulties after getting into my head that maybe I should use a different theme than the current WordPress default. I'll have the feeds and social media buttons back up shortly.

It's kinda sad that nobody has come up with a better responsive WordPress theme that allows me to pack more content onto the page than Twenty Fourteen.
