---
id: 1289
title: International Literacy Day 2014
date: 2014-09-08T14:32:27+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1289
permalink: /2014/09/international-literacy-day-2014/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapBG:
  - |
    s:364:"a:1:{i:0;a:8:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:81:"http://matthewgraybosch.blogspot.com/2014/09/international-literacy-day-2014.html";s:5:"pDate";s:19:"2014-09-08 18:33:15";}}";
snapDI:
  - 
snapDL:
  - 's:292:"a:1:{i:0;a:8:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"7a656659eab40cf64f70a7ee9bf3e4e5";s:5:"pDate";s:19:"2014-09-08 18:33:16";}}";'
snapGP:
  - 
snapPN:
  - 
snapSC:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snap_isAutoPosted:
  - 1
snapLI:
  - 
snapLJ:
  - 
snapSU:
  - 
snapIP:
  - 
snapWP:
  - 
bitly_link:
  - http://bit.ly/1hrFt3I
bitly_link_twitter:
  - http://bit.ly/1hrFt3I
bitly_link_facebook:
  - http://bit.ly/1hrFt3I
bitly_link_linkedIn:
  - http://bit.ly/1hrFt3I
sw_cache_timestamp:
  - 401669
bitly_link_tumblr:
  - http://bit.ly/1hrFt3I
bitly_link_reddit:
  - http://bit.ly/1hrFt3I
bitly_link_stumbleupon:
  - http://bit.ly/1hrFt3I
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"dc75d6ede75f";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:75:"https://medium.com/@MGraybosch/international-literacy-day-2014-dc75d6ede75f";}'
categories:
  - Rants
tags:
  - culture
  - feminism
  - gender
  - illiteracy
  - International Literacy Day
  - non-literacy
  - public schools
  - rant
  - UNESCO
---
Today is UNESCO World Literacy Day, a day set aside to promote literacy throughout the world. The theme for this year's World Literacy Day seems to be sustainable development. I'm not convinced that sustainable development is more than tangentially related to literacy, but nobody at UNESCO asked _my_ opinion. Which is just as well for UNESCO, because if they _had_ requested my opinion, I'd try to get a consulting fee out of them for principle's sake.

Instead, Nikki Tetreault over at Curiosity Quills Press used the publisher's private Facebook group to ask a bunch of us to record short videos on one of the following topics:

  * Why is literacy important?
  * What can be done to improve literacy in the future?

I didn't notice the request, but since Nikki's a fan I thought I'd blog about the topic on my lunch break instead of working on _Silent Clarion_ today.

## Why Literacy is Important

I'm tempted to dismiss the first question as a no-brainer. I wouldn't be the man I am without literacy. Western civilization as we know it wouldn't exist without literacy. But those are easy answers, and I can do better. You probably can, too.

Let's start with my memory. Mine's pretty good. Yours probably is, too, at least for information you use regularly or consider important for other reasons.

It damned well better be, because you should see what I've got crammed in there:

  * an entire imaginary world with a respectably large cast of characters
  * all of the dates that matter to my wife, starting with when we met and when we got married
  * my wife's favorite foods
  * routes to places I've visited twice or more
  * half the .NET framework
  * what I'm supposed to be doing at my day job right now
  * shit I did at my day job last week/month/year
  * common Unix tools/commands
  * common DOS commands
  * basic syntax for a couple dozen programming languages nobody pays me to use
  * solutions to headaches commonly encountered by Windows developers
  * plots for dozens of novels, narrative poems, movies, plays, rock operas, concept albums, and video games
  * lyrics for hundreds of heavy metal songs
  * an ever-growing list of women I would pleasure if I was single and they were willing
  * every line of dialogue in _Spaceballs_
  * proper firearms handling
  * the way my wife first kissed me, and how gorgeous she looked when I first met her
  * the tenets of major religions like Judaism, Christianity, Islam, Laissez-Faire Capitalism, Socialism, and Discordianism
  * the Zombie Survival Guide
  * the "Seven Dirty Words" routine by George Carlin
  * a working vocabulary that keeps my wife's googlejutsu nice and sharp
  * lots of other shit that I could recall with the appropriate trigger

OK, now I'm just bragging. Sorry about that. But if you think all of that is impressive, think again. That's all bupkis compared to the totality of information currently available to humanity. I'd have to live ten thousand years just to _read_ it all, we'd need billion year lifespans just to transmit it all orally.

We'd have no access to the world's information without literacy, _and no means of preserving it_. You know why we still possess the Iliad and the Odyssey in some form? Because it was written down, copied, translated, and _preserved_. Likewise for the Epic of Gilgamesh, the Aeneid, and the Bible.

Those first few books from the Old Testament? That material came out of the oral tradition, and was no doubt a distorted version of the original stories the Hebrews told each other before they started writing things down.

Literacy is proof that ubuntu ("I am because we are.") isn't some feel-good hippie bullshit white dudes appropriated from African hunter-gathers.

## How Do We Improve Literacy?

I am in no way qualified to answer this question, but I'm going to take a stab at it anyway. This question concerns me because as a novelist, I can't sell to illiterate people or to people who _can_ read, but prefer not to.

Fixing illiteracy is simple: _teach people to read_. The hard part is dealing with people who already _can_ read, but choose not to. Why do literate people not read? If somebody asked me why literate Americans don't read, I have an answer ready. You won't like it.

## I Blame the Public Schools

Here's the point where public school teachers start suggesting that Vlad Tepes would know how to deal with me, and the point where I salute America's public school teachers with an upraised middle finger.

I blame America's public schools for turning out graduates who _can_ read, but don't because **THEY DON'T FUCKING ENJOY IT!** Why the bold font, all caps, and use of the word &#8216;fuck'? Because this pisses me off.

The only reason _I_ retained the habit of reading for pleasure was that I was a "problem child". I was a "discipline problem", according to my teachers. I read what I wanted, _when_ I wanted, and wouldn't take shit from adults who insisted that what I wanted to read wasn't "age appropriate".

Nor did I take any shit from kids my own age who thought I was a faggot because I'd rather read than do whatever they thought was cool, and preferred my books to their company. My curiosity was matched with a defiance worthy of Prometheus and Satan.

My younger brother, like most kids, wasn't a little hellion with no respect for authority, and his reward was to have any pleasure he'd get from reading _stolen_ from him by the adults we trusted to teach him, and from his peers.

So, how do we fix literacy in the future? I have some suggestions.

### Change the way we teach kids to read.

Get 'em hooked on phonics, give 'em a dictionary, teach 'em how to behave in a library, and _turn them loose_. It worked for me.

If the parents complain about kids reading books with sex or violence in them, **TELL THEM TO FUCK OFF AND DIE.**

### Stop forcing students to read irrelevant books.

Nobody identifies with Holden Caulfield any longer. If you want to talk dystopia, start with _The Hunger Games_ before you shove _1984_ down their throats. Orwell won't mind; he's dead.

Stop forcing kids to _read_ Shakespeare. Shakespeare belongs on the stage, not in a fucking classroom. And for the love of Hell, stop treating _Romeo and Juliet_ as a love story. The play only works because both Romeo and Juliet are demon-ridden idiots.

### Fix youth attitudes toward reading, especially among boys.

This, right here, is why men need feminism. As long as boys who read can expect to be called "faggots" or "sissies" for doing so, boys will lag behind girls in reading.

Moreover, most kids just want to fit in. They want to be popular, have friends, and (in high school) date or hook up. If reading isn't conducive to these goals, they aren't going to do it.

Kids aren't stupid. They know damn well where a community's priorities lay when a school district spends more on athletics than on libraries. Get rid of varsity sports and spend the money on libraries. Let the jocks play ball on their own time and at their own expense, because government spending on high school sports is _socialism_.

## Suggestions?

If you have any other suggestions for how to fix literacy in the US, feel free to share 'em. This is a matter of self-interest for me, because illiterate people and people who hate to read aren't going to help me quit my day job.