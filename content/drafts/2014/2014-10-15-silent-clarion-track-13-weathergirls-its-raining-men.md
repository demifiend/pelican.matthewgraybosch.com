---
title: "*Silent Clarion*, Track 13: &quot;It's Raining Men&quot; by The Weathergirls"
excerpt: "Check out chapter 13 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Naomi meets three men who will make her vacation a little *too* interesting..."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Shielded scarlet streetlamps lit my way back to the Lonely Mountain. I passed locals out for a stroll, their conversations dueling with the songs of the crickets and cicadas that still owned the night because of the false summer. Their melodies serenaded me as I walked past the darkened street windows, so unlike my neighborhood in London.

A fleeting shadow accompanied by a soft rustle of cloth caught my eye in an alley, and I stopped to check it out. I crept toward it with my hand on my sword's hilt, ready to draw. Before me stood a man in dark camouflage fatigues, his feline eyes a feral yellow in the gloom. His left shoulder bore sergeant's stripes, and the name badge pinned to his chest identified him as 'C. Renfield.'

Renfield studied me for a moment before speaking. "Do you have any idea what the moonlight does to your hair?"

As gambits go, that wasn't half bad. It lacked the simplicity of '"Hello, I'm so-and-so,'" but it wasn't nearly as lame as, '"Does God realize you snuck out of Heaven?'" It was almost poetic, which surprised me.

"Maybe you should tell me." Not that I planned to drag him back to the Lonely Mountain, but he did have a sexy voice and the uniform looked good on him. But why would somebody wear a Commonwealth Army uniform decades after the NAC's dissolution?

"Surely I'm not the first to notice the moon lends you an ethereal aspect?" He offered his hand. "Their loss, and hopefully my gain. I'm Sergeant Christopher Renfield, NACA. You can't be from around here with that British accent."

Dismissing his remark about my voice as a slip of the tongue, I shook his hand. It gave me an excuse to check him out. His gaze held an intensity I found a little unnerving. I prefer longer hair on a man, but his body was made for rough handling, and he had a mouth on him I could definitely put to use. Just thinking about him kissing his way up the backs of my legs made me shiver. "Naomi Bradleigh."

He also had a nice smile, which I captured for future reference. "So, what brings you to the Commonwealth from Britain?"

Britain again? What the hell? I'm a Londoner, not British. The United Queensreach died in Nationfall, like the North American Commonwealth. Turns out there wouldn't always be an England after all, but her people kept calm and carried on. Why does he think those nations still exist? "I'm not sure I understand, Sergeant."

"Word from the brass says the British might invade. Are you with them?"

Oh, bloody hell. He must be some kind of war re-enactor who's still in character. "I'm not with anybody tonight, Sergeant. If you find me during the day, we might arrange a meeting. Sound good?"

"I'd love that, ma'am, but I don't know when I'll get the order to deploy." He closed the distance between us, and slipped an arm around me before I could think to withdraw. His lips were warm and soft upon mine, and lingered long enough to make me want more. "Sorry, ma'am. I should have asked first."

Damn right. He really shouldn't have teased me like that. To teach him a lesson, I caught him by the collar, pressed him against the wall, and stole a deeper kiss to show him what he had gotten himself into. I held him there long enough for his hands to find their way to my arse before pushing myself away.

Taking a few steps back gave me a good view of what I had done to him. A purr leaked into my voice. "I'd better go before I take advantage of you, Sergeant."

"What if I want you to take advantage of me?" Renfield's voice was low and rough as he pulled me against him with a hand in my hair and the other still gripping my ass. He was ready to take me in the alley, and all I had to do was tell him to go for it.

He stared into my eyes for a moment before lowering his head. His lips brushed my throat, followed by a gentle graze of teeth that threatened to obliterate all rational thought not pursuant to the goal of getting this unknown soldier to press me up against the wall and fuck me senseless.

Who the hell was this guy? What the hell was wrong with me? Was I just rebounding, or was it because we're both CPMD+ and that triggered some kind of animalistic, pheromonal chemistry between us? Tempting as he was, I didn't want to think of myself as being that easy. I've never been so hot for a man that I couldn't be bothered to consider the consequences, and it scared me a little.

Forcing him off me with a shove that drove him across the alley, I drew my sword. The weight of steel in my hand cooled my ardor and helped me focus. "I'm serious, Christopher. I hardly know you, so regardless of how much we both want it, I'm not ready to play with you tonight."

Keeping my blade between us, I withdrew from the alley and ran most of the way back to The Lonely Mountain. I paused only when I realized that returning to the pub with a naked sword while looking disheveled and panting was likely to cause a disturbance. I stopped a block away, glancing behind me to confirm nobody was following me, and sheathed my blade. After composing myself using a shop window as a mirror, I walked the rest of the way.

Kaylee was still there, with dual shoulder-mounted kitties. The little black kittens that had draped themselves across the hound now perched on her. Given that kitten claws can be needle-sharp, I doubted she was comfortable. "Looks like you made some friends. Bet the dog's grateful."

One of the kittens leaped from her shoulder, and didn't quite make it to mine. Not only was Kaylee down to a single weapon of magical kitty sweetness, but now I had a little purr baby clinging to the sleeve of my jacket, clawing his way up, and leaving marks. Dammit.

"No shit." Now that Kaylee had an arm free, she exploited her situation and grabbed her beer. "Bruce found 'em in the barn and named 'em Dante and Virgil. Dante's the one climbing you."

Actually, he was now perched on my shoulder and playing with a zipper. I scratched behind his little ears and tried not to let his purring distract me. "Anything interesting happen while I was out?"

"Depends on your definition. See the kid in the red flannel shirt?" She pointed out a local youth at the pool table.

"Not bad. What's his story?"

"His name's Mike Brubaker. His parents run the dairy farm you probably passed on your way in. Some of the younger girls think he's gay."

I shrugged. "Is he?"

"Hell, no." Kaylee flashed a wicked smile, but didn't elaborate. "He just ignores girls his age. You're more his type, but I don't think he's yours. Speaking of which, how'd you like Doc Petersen?"

"I agreed to meet him for coffee tomorrow afternoon." Something's off about Clarion's general practitioner. I'm sure of it, but I kept that to myself. Kaylee's willingness to dish could easily work against me, and I didn't want to either victimize a blameless man or tip off a guilty one. "He's a bit too old for me, so get your mind out of the gutter."

"Then who's the lucky guy?" Agitated by Kaylee's constant gesturing, Virgil also jumped ship. He sat in front of me, and voiced a pathetic little mew before scampering up to complete my furry arsenal. "You were flushed and practically panting when you came back, so there has to be somebody."

Was I really that obvious? Doesn't matter. Here comes the Brubaker boy. He flashed a shy smile. "I have to admit, Adversary Bradleigh, I'm jealous of the kittens."

Now that was good for a laugh. But who told him I serve? Kaylee? And who else did she tell? "I think you'd be more comfortable sitting in my lap. You must be Michael Brubaker. Kaylee told me about you."

He reddened a bit, and glanced her way. She smiled behind her glass. "Should I ask how much she told you?"

"She implied you appreciate experience." The glass in his hand held water with a wedge of lemon. Was Michael too young to drink, or merely abstemious?

Brubaker shook his head and sat at our table without asking for permission. "I heard you're new in town. I'd love to show you around."

Say this much for the kid: he's got balls. Letting him escort me would afford me a view of Clarion I might not get on my own. I offered my hand, careful not to dislodge my purring guardians. "I think I'd enjoy that. But call me Naomi. I'm not on the job."

Michael managed to shake my hand without turning too unnerving a shade of crimson. "Is eleven in the morning good?"

He fled as soon as I agreed to the time. Kaylee managed to wait until the door slammed shut behind him before squealing. "Awwww! He's finally growing up. I think you're the first woman he's ever asked out."

Oh, bugger. What have I gotten myself into? "What about you?"

"Pfft. You shittin' me? I seduced his sweet cornfed ass." She held up her empty glass, a silent imperious demand. "It was last year at the harvest festival. He managed to ask me to dance, but went mute afterward." She leaned in, adopting a conspiratorial whisper. "I can point out stallions who would be jealous of him."

I shook my head, trying to banish the image her words conjured. How was it that wherever I go, I find a Jacqueline ― or a girl well on her way to becoming a Jacqueline? Was it an archetype I attract in the same manner that I seem to attract cats? And who was this blonde girl stalking toward me as if she were ready to throw a gauntlet at my feet?

She stared at me in a frankly appraising manner that I suspected I should find grossly offensive. "Michael is mine. Our parents arranged everything. He just won't accept it."

I shrugged, not particularly interested in disputing her claim, though Michael would be justified in filing a complaint with the Phoenix Society if it were true that his parents arranged a marriage for him without his consent. "You're welcome to him, if you can get his attention. But you might start by brushing up on your manners. I'm Naomi Bradleigh, and who might you be?"

"Jessica Stern. We don't appreciate out-of-town sluts poaching our men here."

I shook my head and struggled not to laugh. Unable to hold my silence any longer, I turned to Kaylee. "I think I know why Michael ignores girls his age."

"I'm right here."

"Still? Exactly!" I gave an exaggerated sigh.

The shocked, disgusted expression on Jessica's face suggested she finally grasped my meaning. Poor Mike. She spun on her heel and stalked away, shoving past one of Mike's lingering friends.

Kaylee burst out laughing. "The look on that little bitch's face was so fuckin' priceless."

The clearing of a masculine throat caught our attention. Halford placed a fresh beer and a glass of red before us. A tall, uniformed man waited behind him. He approached once Halford left, and flashed a badge. "Snow-blonde, scarlet eyes, and an Italian-style sword. You must be Adversary Bradleigh."

Dammit, who's telling everybody I'm an Adversary? First Kaylee told the kid, and now the cops know I'm in town. Word really does get around fast.

Regardless, he was handsome if you liked 'em rugged, with a gravelly voice to match. His uniform looked as good on him as fatigues looked on Renfield. I wouldn't have minded seeing those hazel eyes staring up at me from between my thighs, but I don't get involved with cops. It's a conflict of interest. "And you must be Sheriff Robinson. Who told you I serve?"

"Don't worry about that. I only want to ascertain your intent."

I narrowed my eyes. "Is this the welcome all visitors get? It's not exactly good for tourism."

"Don't get cute with me, Ms. Bradleigh."

Oh, so he wants to be the big dog and mark his territory? Which reminds me, I really should call Rosenbaum and introduce myself. Though I'm sure Cohen or Malkuth have already told him that there's a very naughty kitty hunting in his garden. "I wanted some quiet time away from the city, and I heard some things about Clarion that piqued my curiosity."

Robinson took a moment to mull this over. "And I've heard some things about you that piqued my curiosity. I'd like to have a look at your room."

Not bloody likely. I haven't even been to my room, or done more than pocket my key, but I wasn't about to let Robinson indulge in a fishing expedition. "I do not consent to a search, Sheriff. Do you have a warrant?"

"No." Robinson glanced at my sword, as if it meant something to him. "Do us all a favor and try not to give me cause to get one."

He turned around, treating Kaylee and me to a nice long view of his ass as he walked away. It really was too bad he's a cop. No doubt Kaylee felt the same, judging by her sigh. "Too bad he's married. I'd let him do a cavity search."

"Dammit, Kaylee, I didn't need that mental image." I took her beer away, ignoring her protest. "I think it's time we got you home."

---

### This Week's Theme Song

"It's Raining Men" by The Weathergirls

{% youtube l5aZJBLAu1E %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
