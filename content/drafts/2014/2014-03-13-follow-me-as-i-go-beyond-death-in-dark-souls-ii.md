---
id: 228
title: Follow Me as I Go Beyond Death in Dark Souls II
date: 2014-03-13T20:22:10+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=228
permalink: /2014/03/follow-me-as-i-go-beyond-death-in-dark-souls-ii/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1N0qJ7k
bitly_link_twitter:
  - http://bit.ly/1N0qJ7k
bitly_link_facebook:
  - http://bit.ly/1N0qJ7k
bitly_link_linkedIn:
  - http://bit.ly/1N0qJ7k
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1N0qJ7k
bitly_link_reddit:
  - http://bit.ly/1N0qJ7k
bitly_link_stumbleupon:
  - http://bit.ly/1N0qJ7k
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Dark Souls II
  - Games
tags:
  - bandai namco
  - dark souls ii
  - dark souls ii spoilers
  - from software
  - gameblogging
  - new game
  - spoilers
  - the first hours
  - unboxing
  - YASD
---
I have my copy of _Dark Souls II_ for the PS3. I had to drive down to the post office before it closed to get it, but that's OK. That's what I get for not having stuff shipped to my office. Now that I've got the game up and running, I want to invite you to join me as I go beyond death, and into the ruined kingdom of Drangleic. You can also fight beside me (or against me) on the PlayStation Network. My handle is **EddieVanHelsing**.

Expect spoilers, and prepare to die. Often.

## Starting Up

Immediately after starting the game, my console greeted me with a prompt to download and install the v1.01 patch. I guess From Software found bugs already. 🙂

After installing the 125MB patch, the game took a few minutes to install system data to my console's hard drive. Upon completion, I was asked to read and agree to Bandai Namco's EULA, which contains the usual most-likely-unenforceable bullshit.

Upon finally starting the game, I'm asked to set some options, such as brightness, presence of subtitles (which should always be on by default, IMO, and whether I want to see blood, just a little blood, or no blood at all. The blood option is most likely to be a handy one for parents concerned about the ESRB's blood/gore warning in the game's Teen rating.

The piano music playing while I type this is beautiful, by the way. Its bleak ambience sets the stage for what's sure to come.

Clicking next takes me to a second settings page, where I can set options like auto-targeting and whether to automatically target the nearest enemy after killing the currently locked enemy. I'll have to actually play the game to see if these options help or hinder, but I assume I can adjust these settings later on. There's a switch to automatically adjust the camera when it touches a wall. Not sure about that one, either.

The game also has options for voice chat and cross-region matching; you can restrict or allow either, and both are restricted by default. I think I'll leave those alone; trying to play with Japanese players is likely to be a lagfest, and one of the things I liked about the original _Dark Souls_ was that online play didn't have voice chat &#8212; which meant no Giantdads yelling "faggot" as they invaded my game.

## Things Betwixt

After an eerie opening cinematic in which the Accursed makes his way to the entrance to Drangleic, which is narrated by a rheumy-eyed crone who seems to know more than she's willing to reveal, my hooded character awakens in an area called **Things Betwixt**. The grass rustles and bones crunch and tumble as I run through.

Oh, and don't try punching the monkey things. They'll gang up on you and kill you. That's my first death already.

And I just died again by jumping off a waterfall. That was dumb of me.

After a little more exploration in which I pick up a rusted coin and a small, silky smooth stone, I come to an old cottage. It looks like I've no choice but to stick my head inside. An old woman in red, with two equally aged companions and a younger one, greet me. They mock me, saying I will end up just like the other soul-eating Hollows they've met, before asking my name.

## Character Creation

It's time to create my character. My choices in starting class are as follows:

  * Warrior: "High strength and dexterity. Skilled with weapons."
  * Knight: "High HP and adaptibility. Tough to take down."
  * Swordsman: "Fights gracefully with strong weapons in both hands."
  * Bandit: "High dexterity, skilled with bow; fights well at various ranges."
  * Cleric: "High faith and miracles guide the way."
  * Sorcerer: "Casts sorceries with high intelligence and attunement."
  * Explorer: "Not very powerful, but has many items."
  * Deprived: "Has nothing to fight with, except life-affirming flesh."

I suppose the Deprived is the go-to class for people who want to build a character from scratch, since he has 6 points in all stats and starts at Soul Level 1.

Character creation is reasonably complex, but the layout's a bit wonky. You can choose your starting class and gift without even customizing your character's appearance. You can play as either a male or female, and have a variety of builds to choose from. You can also choose whether to have a muscular physique, or a softer one. Facial customization gets two screens, one labelled "Face" for basic settings, and one labelled "Advanced".

I choose to keep things simple and create a Warrior with the "Healing Wares" gift, and made her look somewhat like my wife: a tall, sturdy brunette with long hair. Hey, if I'm going to have this character, I might as well have one I like.

## Things Betwixt &#8211; Reprise

It turns out the three old women and their young companion have a story. The young woman is named Milibeth. The old women were once Fire Keepers. There's a fourth, but nobody knows where she is. You can kill Milibeth and the elderly Fire Keepers if you want to be an asshole. You won't get any souls, but they drop Human Effigies, which allow you to restore humanity from a hollowed state and have full HP. Of course, if you're going to do this, go after Milibeth first. She swings a mean ladle.

Regardless, take the back door, and look behind the wagon to find a corpse with treasure. He's got a Soul of a Lost Undead and a Torch. Nearby is your first Bonfire. Tag it, and rest.

Afterward, you can enter caves with mist-covered entrances for tutorials. This will get you some souls, and additional items. I noticed that my starting sword not only wears down pretty fast, but that I can see my weapon's durability under its icon on the HUD. That's pretty handy. Moreover, weapon durability recovers when you rest at a bonfire.

Just be careful not to screw around with the cyclops on the beach near the rowboat. It has a friend. And I was wrong. It's not a rowboat. It's a sarcophagus. Getting into it changed my character's gender.

I decided to leave the cyclopes alone until I get my hands on a bow and arrows. Plinking from a distance is more my style than melee, but swords never run out of ammo &#8212; and I wanted a class that started with a decent shield.

Instead, I made my way out to a gorgeous coastline called Majula, where I found another bonfire and a lady called the Emerald Herald. I can speak to her to level up, it seems&#8230;

* * *

Attack of the Fanboy has a guide to <a title="Attack of the Fanboy: Getting Started in Dark Souls II" href="http://attackofthefanboy.com/guides/guide-started-dark-souls-2/" target="_blank">getting started in <em>Dark Souls II</em></a>, for players who want a bit of guidance. A wiki is also available on <a title="Dark Souls 2 Wiki" href="http://darksouls2.wikidot.com/" target="_blank">wikidot.com</a>, but try to figure things out on your own first. I will.