---
title: Accidental Worldbuilding
excerpt: I don't set out to worldbuild. In fact, I think worldbuilding for its own sake is nothing but wank, and only worldbuild when doing character development.
categories:
  - Backstage
  - How Not to Write
tags:
  - bare stage
  - IRC
  - just-in-time
  - secure shell
  - silent clarion
  - starbreaker
  - talk
  - unix
  - worldbuilding
---

J. R. R. Tolkien would turn over in his grave at the notion of a writer not building out his entire world in advance, and I'm sure that if Ursula K. Le Guin gave a damn, she might find a few pointed things of her own to say about accidental worldbuilding.

<!--more-->

Not that Le Guin needs to tell me I'm doing it wrong. That's what the Internet is for. Not that I get paid enough to give a damn what they think.

{% include base_path %}
![Meme: "You're Doing it Wrong"]({{ base_path }}/images/youre-doing-it-wrong.jpg.jpg)

## Writing Like a Programmer

I favor revealing just enough about the setting to make the story work, and not worrying about the rest until you actually need it. An analogous principle called [YAGNI (You Aren't Gonna Need It)](http://en.wikipedia.org/wiki/You_aren't_gonna_need_it) exists in the [extreme programming](http://en.wikipedia.org/wiki/Extreme_programming) methodology. Why code functionality not currently specified by the requirements? Why labor over a setting you aren't going to use? Instead, write your code and build your world in an _extensible_ fashion that permits you to add things later without screwing up what you've already done.

It's storytime, so buckle up. There I was at the bagel shop near my day job, banging away at Chapter 14 of _Silent Clarion_, when I realized I needed to explain why state forests like Cook Forest reclaimed so much of Pennsylvania so soon after Nationfall. Only a couple of farms separated the town of Clarion from the forest that crept southwest along the Clarion River.

Now, this forest actually has what's left of a road underneath; one of the locals, Michael Brubaker, digs through the humus and soil and retrieve a chunk of asphalt to show Naomi. He then points to some little hills in the distance and says they used to be a strip mall.

Now, Naomi might be snow-blonde (she's the lady with the sword on the motorcycle), but she isn't a schmuck. She knows that since less than 50 years have passed since Nationfall, evidence of human habitation should be more visible than it is. She does a bit of searching, and asks her buddy Malkuth (one of many friendly AIs in the Starbreaker 'verse) what the hell happened.

## So, What Happened?

You really want to know? OK. I'll show you.

> A pair of large files hit my implant a couple minutes later, labeled "clarion-valley-topo-2048.svg" and "clarion-valley-topo-2049.svg". As I opened them, my implant superimposed them over my vision as transparencies while warning me against physical activity. A quick comparison showed a drastic difference between the two topographic maps. &#65378;There's an _impact crater_ a kilometer east of my position. Meteorite?&#65379;
>
> &#65378;Worse. There was a protest there just before everything fell apart. Some NACAF general decided it was an insurrection and decided to use an experimental space-based weapon codenamed [GUNGNIR](http://en.wikipedia.org/wiki/Gungnir) to suppress it.&#65379;
>
> Gungnir was the spear of Odin, king of the Aesir. Various Norse myths claimed it was so exquisitely balanced that it could strike down any enemy, regardless of its wielder's capabilities. The use of such a name to signify a space-based weapon couldn't possibly be coincidental. &#65378;GUNGNIR was a [kinetic strike](http://en.wikipedia.org/wiki/Kinetic_bombardment) system, wasn't it?&#65379;
>
> &#65378;Exactly.&#65379;
>
> &#65378;Holy living fuck.&#65379; It was one of Jacqueline's favorite expressions. Damned if I know why, but it fit.

## Got Unix?

This conversation took place between Naomi and the AI Malkuth using [secure](http://www.openssh.com/) [talk](http://en.wikipedia.org/wiki/Talk_(software)), a text-based system. Think of it as SMS with encryption so badass the US would fall apart before the NSA cracked it. I also have a secure version of [IRC](http://en.wikipedia.org/wiki/Internet_Relay_Chat) called "Secure Relay Chat".

These tools allow fast, text-based communication between characters with implanted computers in the Starbreaker setting. However, a character could be using secure talk while also having a face-to-face conversation. If I use standard English quotation marks for both, it might prove difficult to distinguish between a voice conversation and network chatter.

## Matters of Convention

You may be confused by the brackets and wondering why I don't just quotation marks. and distinguish between vocalization and texting in narrative. I think it would bog down the story. Of course, my refusal to do this creates new problems.

In _Without Bloodshed_, the publisher decided to italicize all network chatter. This decision imposed on readers the challenge of figuring out from the context what was a character's thoughts, and what was something they were saying on the network.

So, I'm using corner brackets as "quotation marks" for text-based comms, since they're used for that purpose in languages like Chinese and Japanese. It's either that, or persuade the publisher to use monospaced fonts for text-based dialogue.'

Which do you think is more readable? This?

> Claire pushed back her chair, glaring at the "You died like a punk." notification taunting her. &#65378;You backstabbing little shit. I'm gonna get you next time.&#65379;

Or this.

> Josefine chuckled at Claire's frustrated trash talk as she made her toon do a victory dance over the virtual carcass of her latest kill. `You're welcome to try, but maybe you shouldn't be so obvious.`

Don't forget that the colored text isn't going to render on all devices. Nor will it render properly on paper. :cat:

## What About GUNGNIR?

Oh, right. GUNGNIR and systems created by rival nations like GAEBOLG and LONGINUS were something I pulled out of my ass to explain widespread destruction conducive to rapid reforestation in the wake of global social collapse. Nukes or conventional ordnance like the [GBU-43 Massive Ordnance Air Blast](http://en.wikipedia.org/wiki/GBU-43/B_Massive_Ordnance_Air_Blast) would have worked just as well, but dropping shit from space sounds cooler &#8212; as long as it doesn't actually _happen_.

This is just-in-time worldbuilding. It's worldbuilding done as needed, within the confines of the existing continuity. Reforestation in less than fifty years might be possible without the help of a character resembling [Brig. Gen. Jack D. Ripper](http://www.imdb.com/character/ch0003297/quotes), but the existence of orbital doomsday weapons named after legendary spears gives me something more than an explanation for [Cook Forest's](http://www.dcnr.state.pa.us/stateparks/findapark/cookforest/) rapid expansion.

You see, those weapons are still out there, orbiting Naomi's version of planet Earth. Who has the keys?

> Not that Malkuth was finished. &#65378;It gets worse. GUNGNIR is still out there, along with two other systems codenamed GAEBOLG and LONGINUS. The Society has them under control, but could you imagine what might happen if nation-states arose again and started creating more of these systems?&#65379;
>
> Staring skyward, I tried to imagine shafts of tungsten raining down as a hail of javelins. The spear was one of humanity's first weapons. Was it to be our last? Despite the warm of late summer, I shuddered. &#65378;Has the Society ever used these weapons?&#65379;
>
> &#65378;I'm sorry, Naomi, but you aren't cleared for that information.&#65379;

That's right. The Phoenix Society, that delightful implementation of one world government masquerading as a NGO, has the means to subject opposition to orbital bombardment. Worse, a loyal Adversary like Naomi isn't even allowed to know if the organization she serves has ever used these weapons.

Trust me when I say that if the Phoenix Society hasn't used a weapon like GUNGNIR, GAEBOLG, or LONGINUS to enforce it's rule, it's used the _threat_ of such weapons. And it also gives me something the Phoenix Society can try using against Sabaoth in the last main-sequence Starbreaker book, _A Tyranny of Demons_.

It won't work, of course, because then Morgan Stormrider wouldn't have to use the Starbreaker. We can't have _that_. It would be too easy.
