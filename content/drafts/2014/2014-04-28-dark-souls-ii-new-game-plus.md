---
id: 588
title: 'Dark Souls II: New Game Plus and Beyond'
date: 2014-04-28T00:59:00+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=588
permalink: /2014/04/dark-souls-ii-new-game-plus/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1PiSIBJ
bitly_link_twitter:
  - http://bit.ly/1PiSIBJ
bitly_link_facebook:
  - http://bit.ly/1PiSIBJ
bitly_link_linkedIn:
  - http://bit.ly/1PiSIBJ
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1PiSIBJ
bitly_link_reddit:
  - http://bit.ly/1PiSIBJ
bitly_link_stumbleupon:
  - http://bit.ly/1PiSIBJ
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Dark Souls II
  - Games
tags:
  - Builds
  - courtesy
  - dark souls
  - Dual Wielding
  - etiquette
  - Longsword
  - multiplayer
  - New Game Plus
  - NG+
  - online
  - PVE
  - PVP
  - sportsmanship
  - Tactics
  - Weapons
---
A small confession is in order: I rarely play a game twice. Once I know how a game's story works out, I'm done with it. I don't care about getting every achievement. I don't bother with speed runs or low-level challenges or any of the usual methods gamers use to get their money's worth out of a game. I usually don't bother with online multiplayer, either. However, _Dark Souls II_ is different. After beating the game, I mucked about in post-game mode for perhaps half an hour before bringing the game full circle and starting my character's second journey to Drangleic, aka NG+.

Since the Windows port of _Dark Souls II_ just came out, this might be a good time to share some of what I learned from my journeys through Drangleic. It's a long, strange trip, and you'll want all the help you can get. Everything from here is based on personal experience and opinion. Your mileage may vary.

## What to Expect from New Game Plus

Expect stronger enemies, with stronger attacks and stronger defenses, that are harder to kill. Hopefully you upgraded your favorite equipment. Expect _more_ enemies, in the form of computer-controlled unnamed Black Phantoms. Expect to find equipment not previously available your first time through the game, such as Gower's Ring of Protection, and a variety of +2 rings which improve upon the originals. Expect to get invaded more often. Expect familiar enemies to exhibit unfamiliar attack patterns. Expect to die often.

## Fashion Souls

If you thoroughly explored Drangleic in your first playthrough, now's the time to mix and match armor sets for better fashion. Here's how my toon looks.<figure id="attachment_3370" style="width: 840px" class="wp-caption aligncenter">

<a href="http://www.matthewgraybosch.com/2014/04/dark-souls-ii-new-game-plus/1-my-toon039s-all-dolled-up-for-dragonbro-duels-on-the-ikb/" rel="attachment wp-att-3370"><img src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/1-My-toon039s-all-dolled-up-for-dragonbro-duels-on-the-IKB-1024x576.jpg?fit=840%2C473" alt="Fashion Souls: Dolled up for Dragonbro duels on the Iron Keep Bridge" class="size-large wp-image-3370" srcset="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/1-My-toon039s-all-dolled-up-for-dragonbro-duels-on-the-IKB.jpg?resize=1024%2C576 1024w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/1-My-toon039s-all-dolled-up-for-dragonbro-duels-on-the-IKB.jpg?resize=300%2C169 300w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/1-My-toon039s-all-dolled-up-for-dragonbro-duels-on-the-IKB.jpg?resize=768%2C432 768w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/1-My-toon039s-all-dolled-up-for-dragonbro-duels-on-the-IKB.jpg?resize=610%2C343 610w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/1-My-toon039s-all-dolled-up-for-dragonbro-duels-on-the-IKB.jpg?resize=1200%2C675 1200w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" data-recalc-dims="1" /></a><figcaption class="wp-caption-text">Fashion Souls: Dolled up for Dragonbro duels on the Iron Keep Bridge</figcaption></figure> <figure id="attachment_3371" style="width: 840px" class="wp-caption aligncenter"><a href="http://www.matthewgraybosch.com/2014/04/dark-souls-ii-new-game-plus/2-closeup/" rel="attachment wp-att-3371"><img src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/2-Closeup-1024x576.jpg?fit=840%2C473" alt="Fashion Souls: Closeup of the toon my wife made for me." class="size-large wp-image-3371" srcset="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/2-Closeup.jpg?resize=1024%2C576 1024w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/2-Closeup.jpg?resize=300%2C169 300w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/2-Closeup.jpg?resize=768%2C432 768w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/2-Closeup.jpg?resize=610%2C343 610w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/04/2-Closeup.jpg?resize=1200%2C675 1200w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" data-recalc-dims="1" /></a><figcaption class="wp-caption-text">Fashion Souls: Closeup of the toon my wife made for me.</figcaption></figure> 

## Level Up Whenever Possible

In _Demon's Souls_ and the first _Dark Souls_, players were matched for cooperative play and PvP by their Soul Level. As a result, the player community eventually decided to standardize on a maximum level for competitive play that was far short of the level needed to raise every attribute to 99. In the previous games, the top suggested level for competitive play was between 120 and 150.

This isn't very relevant any longer with the Soul Memory system. In _Dark Souls II_, the game keeps track of every soul you earn, regardless of what you do with those souls. Whether you use them to level up, buy equipment, repair or upgrade equipment, or lose them by dying, your Soul Memory is the primary stat used for online matchmaking.

Added to Soul Memory is a bit of segregation between characters in NG (New Game), and characters in NG+ and beyond. Player characters in NG won't play with characters in NG+. Moreover, invasions are more common in NG+.

With this in mind, I recommend levelling up whenever you aren't reinforcing your favorite weapons and armor. Since the souls you gain will be used for matchmaking rather than your level, there's no reason to stick to an arbitrary maximum level unless you want to challenge yourself by creating an effective character with limited stats, since you get one point to put into your attributes every level.

## Suck it Up, Soldier!

Unlike the previous games, _Dark Souls II_ players are always vulnerable to invasions by other players. While invasions are more common in NG+ and beyond, they can still happen during your first campaign, especially if you venture into the following areas:

  * Belfry Luna
  * Belfry Sol
  * The Grave of Saints
  * The Doors of Pharros

These areas are guarded by in-game player factions dedicated to harassing trespassers. The Belfries are guarded by the Bell Keeper covenant. The Grave of Saints and the Doors of Pharros are guarded by servants of the Rat King. The former will invade your game. The latter will automatically draw you into _their_ games, where they've prepared all manner of traps for you.

In addition, players have made the bridge near the first bonfire in the Old Iron Keep an unofficial dueling ground &#8212; and I've been invaded there as well.

Once you leave Majula, you're always vulnerable to invasion. You have a few options at this point:

  * Play offline, which also prevents you from summoning allies to help with boss fights.
  * Let invaders slaughter you, and then clutter the internet with your complaints.
  * Learn to fight back, and treat every defeat as a learning experience.

Now, I'll admit that I used to be a big pussy, and would play in Soul Form or Hollow Form to avoid being invaded. Since that isn't an option in _Dark Souls II_, I decided to make the best of it. While I'm not an elite player, I've won as many fights as I've lost.

## Honor Among the Undead

While you might not speak or message your fellow players, online multiplayer in _Dark Souls II_ is still a social experience. I think good manners and a sense of sportsmanship are worthwhile, even if you've become the hunter instead of the hunted by invading other players instead of waiting to be invaded &#8212; or by taking advantage of red summoning signs or dragon's eye symbols to invite other players to duel with you.

When I invade other players, I use the game's gesturing system to greet them with a bow. I am a guest in their game, though perhaps an uninvited one, and a little courtesy never killed anybody.

If an invader bows to me, I'll also bow to acknowledge my guest's good manners, though many players will argue that an invaded player has no obligation to be courteous, and that invaded players are within their rights to attack invaders on sight. I understand their viewpoint, but that's not how I prefer to play. I can't expect others to be good guests if I am a poor host.

I don't get upset if a host tries to heal during a fight, though I'll do my best to take advantage of my opponent's momentary vulnerability. If you try to heal, expect me to come at you hard and fast. I will do my best to nullify your attempt at recovery. However, I won't use magic to heal myself if I'm the invader, since losing the fight won't harm me.

Finally, I don't mock players I've defeated with rude gestures, though _Dark Souls II_ offers several for that purpose. If it was a close fight, with both of us down to less than ten percent of our hit points, mockery would no doubt sour the memory for the other player. And if I managed to win without taking a hit, why rub it in? No doubt the other guy's embarrassed enough already.

However, I'm not above messaging a player who gave me a hell of a good fight to thank them, and to congratulate them if they won.  It doesn't do me any harm to be a good sport, especially since the guy I just fought might be the guy I end up summoning for help with a boss fight later on.

## Roll With the Punches

In _Demon's Souls_ and _Dark Souls_, I depended mainly on my shield for defense. Instead of trying to dodge blows, I'd block or try to parry them. That's not how I roll in _Dark Souls II_.

Instead, I'm doing everything I can to optimize my character for evasion, starting with her stats, which are as follows:

<p style="padding-left: 30px;">
  Level: <strong>209</strong><br /> Vigor: <strong>30</strong><br /> Endurance: <strong>22</strong><br /> Vitality: <strong>30</strong><br /> Attunement: <strong>20</strong><br /> Strength: <strong>40</strong><br /> Dexterity: <strong>40</strong><br /> Adaptibility: <strong>40</strong><br /> Intelligence: <strong>20</strong><br /> Faith: <strong>20</strong>
</p>

These stats give my character a respectable amount of HP in case I do take a hit or two. She also has a fair amount of stamina, which permits her to execute longer combos, or fight according to the principles espoused by Muhammed Ali: **Float like a butterfly. Sting like a bee.**

High Vitality lets my character equip heavier gear, and more of it. It also means that the lightweight equipment loadouts I favor use a smaller percentage of her equipment burden. I tend to do most of my fighting with a Fire Longsword and a Hunter's Black Bow in my character's right hand, and a Royal Kite Shield in her left. I tend not to equip a helmet, and for armor I favor the Wanderer's Coat, Jester's Gloves, and Black Leather Boots. This setup doesn't use more than 30% of my character's maximum burden, which means her stamina regenerates fast (especially with the Cloranthy Ring equipped), she can roll far and fast, and she can dodge damn near anything as long as I don't get sloppy.

Having 40 Adaptability gives my character an agility rating of 110, which is ten short of the maximum. While the efficacy of high Adaptability remains subject to dispute among players, I find it useful.

## It's Not the Size that Counts

It's tempting to equip the heaviest, nastiest looking weapon you currently have in your inventory. Many players do just fine that way, and I've gotten my ass kicked a few times by players who not only favor swords big enough to make Cloud Strife from _Final Fantasy VII_ and Guts from _Berserk_ not look like <a title="Urban Dictionary: Size Queen" href="http://www.urbandictionary.com/define.php?term=size%20queen" target="_blank">size queens</a>, but know how to use them.

That's not my style, though you'll rarely see me rocking a rapier. Instead, I _still_ favor the <a title="Dark Souls II Wiki: Fire Longsword" href="http://darksouls2.wiki.fextralife.com/Fire+Longsword#.U13bWaaRAjg" target="_blank">Fire Longsword</a> players can find in the Forest of Fallen Giants, in a cave guarded by a big friggin' salamander. Despite it being a weapon you can get, near the beginning of the game, it's still a viable weapon in NG+ whether you use it one-handed, two-handed, or dual-wield it.

Its two-handed light attack is a quick slash that you can easily combo. Its two-handed strong attack is a thrust attack. Moreover, you can roll toward an enemy, hit the light attack, and come out of your roll doing a thrust attack. In addition, you can follow a backstep with a light attack to make your character lunge forward and thrust.

I personally like to roll toward spellcasters in PVP, nail them with a thrust followed by a quick cut, and then roll away before they can land a counter-attack. This tactic also works against melee fighters, but you have to stay sharp.

## Some Last Scraps of Advice

This post rambles a bit, for which I apologize. If you'll indulge me a bit longer, I have some last bits of advice. During your first time through _Dark Souls II_, you may have happened upon a Ring of Life Protection. I advise you to keep it equipped at all times, and try to always have at least 3000 souls handy to cover the repair cost should you die. Wearing this ring will keep you from going Hollow if you die, and keep you from losing all your souls &#8212; which is handy if you've been saving up a couple hundred thousand so you can level up a few times at once. It'll make New Game Plus much easier, and take the sting out of losing should you get smacked around by an invader.

<div>
  If you're a PS3 player, and you see a summoning sign for a player named <strong>EddieVanHelsing</strong>, that's me. If I haven't joined a PVP covenant so I can stay sharp, I play as a Sunbro (Heirs of the Sun covenant). I'll often hang out by a boss fight to praise the Sun with some jolly cooperation. Call me in if you see my sign and need a hand. And if you've fought beside me (or against me), say hello in the comments section. Most of all, have fun. It's only a game.
</div>