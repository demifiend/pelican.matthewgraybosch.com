---

title: 'Is World Domination At Hand?'
cover: without-bloodshed-final-cover.jpg
coverwidth: 1800
coverheight: 2706
covercredit: Ricky Gunawan
covercrediturl: http://ricky-gunawan.daportfolio.com/
---

Not likely, but after giving in and pulling up the Amazon page for *Without Bloodshed*, I found a little treat. See for yourselves.

I own cyberpunk for both print and Kindle, and I may also claim print SF romance as my dominion. At least, for now...

See the stats for yourself.

![Amazon stats for 10/16/2014](/images/wb-amazon-stats-20141015.png)