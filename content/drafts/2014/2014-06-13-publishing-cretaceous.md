---
id: 735
title: Publishing in the Cretaceous
date: 2014-06-13T23:42:13+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=735
permalink: /2014/06/publishing-cretaceous/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link_linkedIn:
  - http://bit.ly/1N0jdcG
bitly_link_facebook:
  - http://bit.ly/1N0jdcG
bitly_link:
  - http://bit.ly/1N0jdcG
bitly_link_twitter:
  - http://bit.ly/1N0jdcG
sw_cache_timestamp:
  - 401642
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - Amazon
  - asteroid
  - cartel
  - cretaceous period
  - dinosaurs
  - extinction event
  - Hachette
  - publishing
---
As an indie novelist publishing through a [small press](http://www.curiosityquills.com), Amazon is one of my primary markets. The ongoing dispute between Amazon and Hachette is of some interest to me. I'm tempted to suggest that the current publishing environment is one akin to Earth near the end of the Cretaceous period.<figure style="width: 800px" class="wp-caption aligncenter">

[<img src="http://i1.wp.com/upload.wikimedia.org/wikipedia/commons/8/8c/Chicxulub_impact_-_artist_impression.jpg?resize=800%2C630&#038;ssl=1" alt="This painting by Donald E. Davis depicts an asteroid slamming into tropical, shallow seas of the sulfur-rich Yucatan Peninsula in what is today southeast Mexico. The aftermath of this immense asteroid collision, which occurred approximately 65 million years ago, is believed to have caused the extinction of the dinosaurs and many other species on Earth. The impact spewed hundreds of billions of tons of sulfur into the atmosphere, producing a worldwide blackout and freezing temperatures which persisted for at least a decade. Shown in this painting are pterodactyls, flying reptiles with wingspans of up to 50 feet, gliding above low tropical clouds. Courtesy of NASA." data-recalc-dims="1" />](https://en.wikipedia.org/wiki/Cretaceous%E2%80%93Tertiary_extinction_event)<figcaption class="wp-caption-text">This painting by Donald E. Davis depicts an asteroid slamming into tropical, shallow seas of the sulfur-rich Yucatan Peninsula in what is today southeast Mexico. The aftermath of this immense asteroid collision, which occurred approximately 65 million years ago, is believed to have caused the extinction of the dinosaurs and many other species on Earth. The impact spewed hundreds of billions of tons of sulfur into the atmosphere, producing a worldwide blackout and freezing temperatures which persisted for at least a decade. Shown in this painting are pterodactyls, flying reptiles with wingspans of up to 50 feet, gliding above low tropical clouds. Courtesy of NASA.</figcaption></figure> 

I have a reason for using this metaphor. The Big Five publishers are dinosaurs. Authors are mammals. I think the publishers are afraid that Amazon is the asteroid that triggered the [Cretaceous-Paleogene extinction event](https://en.wikipedia.org/wiki/Cretaceous%E2%80%93Tertiary_extinction_event).

Not that anybody at Hachette would admit it. I don't think any of the Big Five want to admit that they're so dependent on Amazon they and the authors bound to them by contract need Amazon more than Amazon needs them.

Instead, we get traditionally published authors like James Patterson [claiming that Amazon is hurting literature](http://www.usatoday.com/story/life/books/2014/05/29/james-patterson-amazon-hachette-bookexpo-america/9724969/). He argues that "Amazon wants to control book buying, book selling and even book publishing", and that Amazon "sounds like the beginning of a monopoly". I find this argument hilarious because [the Big Five act as a cartel](http://www.theguardian.com/commentisfree/2014/jun/04/war-on-amazon-publishing-writers) whose anti-competitive practices (such as their attempts to prop up eBook prices with Apple) were sufficiently egregious to warrant [antitrust prosecution by the US government](http://www.bloomberg.com/news/2014-05-30/apple-must-face-e-book-price-fixing-trial-before-appeal.html).

## But Amazon is Killing Literature.

I must confess to finding Mr. Patterson's doomsaying especially amusing. At risk of indulging in an _ad hominem_ attack, I can't help but suspect that a virulently racist, libelous screed accusing Alexandre Dumas, _pere_ of running a novel factory might more properly be applied to Mr. Patterson. I don't think he cares about literature, but I'm not saying this because I dislike his novels.

Since I've only read two of his earlier works (_Along Came a Spider_ and _Kiss the Girls_), I can't reasonably judge his skill as an author. Instead, I think he speaks so passionately in traditional publishing's defense because he's a human being, and like most human beings who know what it's like to not be broke, he doesn't want to screw up an arrangement that has thus far proved beneficial to him.

I don't blame Patterson. He's a writer like me. Instead, I blame the publishers. Amazon CEO Jeff Bezos is famous for saying, ["Your margin is my opportunity."](http://www.businessweek.com/articles/2013-01-08/amazons-jeff-bezos-doesnt-care-about-profit-margins) I think the Big Five inflate their margins by keeping their payouts to authors artificially low. As soon as one publisher tries to offer its authors a better deal, the others crack down. This, by the way, is classic cartel behavior.

The Big Five are little different from the RIAA. They treat their stars — writers like James Patterson and Neil Gaiman — like royalty just as record labels pamper their top-selling acts. However, the bestsellers still get only a fraction of the revenue their sales generate. The rest remains in the publishers' hands. What's left after paying wages and salaries is profit.

Bestselling authors can play Cassandra if they want, and prophesy the death of literature if the publishers' profits are not preserved — by act of Congress if necessary — but it won't help. If the Big Five fall, American literature will survive. Hell, I spent almost _twenty years_ writing with no prospect of making a cent of my efforts.

## But Amazon is Killing Bookstores.

No. Amazon is killing retail in general. Amazon's kicking around [Barnes & Noble](http://www.forbes.com/sites/lauraheller/2012/10/02/barnes-noble-vs-amazon-book-wars-get-more-interesting/). Amazon is killing [Best Buy](http://www.cringely.com/2013/05/02/amazon-com-isnt-killing-best-buy-blame-best-buy-it/), though one could reasonably argue that Best Buy is so poorly managed the chain is already a dead man walking — and deserves it.

The standard pro-retail argument against Amazon revolves around sales taxes, and Amazon's refusal for most of its existence to collect sales taxes in states where it does not maintain a physical presence. I'm not an attorney specializing in tax law, so I can't comment on the legitimacy of Amazon's stance.

However, if Amazon continues its expansion and succeeds in making same-day delivery a reality, it will require physical presences everywhere in the United States. As a result, they've taken to collecting sales taxes.

All of this is irrelevant. Amazon might be killing big retail chains, and good riddance to the lot of them, but [independent bookstores are doing just fine](http://money.msn.com/now/post--independent-bookstores-start-a-new-chapter-growth). The big chains sell popular books, which Amazon can do better and cheaper. They're impersonal. The independent bookstores can and do offer a different experience, and perhaps a superior one for those who still cherish physical books.

## But Amazon is Screwing Writers.

I'm not sure why I should even bother with _this_ argument. I am acquainted with several dozen independent authors on Google+ who have managed to earn a living as independent novelists. My own books are selling, albeit not as well as I'd like, as are most other titles from Curiosity Quills Press.

But even if Amazon is screwing writers, take a look at their [royalty rates](https://kdp.amazon.com/help?topicId=A29FL26OKE7R7B). Are you prepared to argue that the Big Five offer a better deal for writers who can't sell books by name recognition alone? Are you prepared to [dispute the data](http://www.hughhowey.com/authorearnings-com/)?

Even though I only get half of what Amazon and other retailers pay because I publish through [Curiosity Quills](http://www.curiosityquills.com), that's _still_ a better deal than the Big Five would offer, especially after the agent takes their cut. While my publisher doesn't have the promotional resources available to the Big Five, they believe in my work. They were willing to take a chance on a metalhead's science fantasy epic, something traditional publishers might have rejected because it didn't fit neatly into a single genre.

## Whose Side Am I On?

No doubt you're wondering about which side I'm on. The answer is simple: **I'm on my side**. I'm out for myself, first and last and always. I think I can do better in an environment where writers can work with small presses and get personalized attention or attempt to reach readers by dealing directly with retail.

The Big Five are just another cartel of corporate rent-seekers. The value-adding functions they claim to provide can also be obtained by working with reputable freelancers. Writers don't need them. Readers don't need them. We're just used to them.