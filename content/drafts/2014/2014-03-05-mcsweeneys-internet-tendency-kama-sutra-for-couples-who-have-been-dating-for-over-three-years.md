---
id: 69
title: 'McSweeney’s Internet Tendency: Kama Sutra for Couples Who Have Been Dating for Over Three Years.'
date: 2014-03-05T09:42:13+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=69
permalink: /2014/03/mcsweeneys-internet-tendency-kama-sutra-for-couples-who-have-been-dating-for-over-three-years/
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapWP:
  - 's:159:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:2:"79";s:5:"pDate";s:19:"2014-07-13 06:57:54";}}";'
snapLJ:
  - 's:206:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0lj";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:48:"http://matthewgraybosc.livejournal.com/6072.html";s:5:"pDate";s:19:"2014-07-14 11:49:31";}}";'
snapGP:
  - 's:388:"a:2:{i:3;a:5:{s:12:"rpstPostIncl";s:7:"nxsi3gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"114546293185560266851/posts/axUbgLgq23t";s:5:"pDate";s:19:"2014-07-15 19:57:42";}i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"103251633033550231172/posts/GkHBLVstwvB";s:5:"pDate";s:19:"2014-07-14 19:57:01";}}";'
bitly_link:
  - http://bit.ly/1PiUpPB
bitly_link_twitter:
  - http://bit.ly/1PiUpPB
bitly_link_facebook:
  - http://bit.ly/1PiUpPB
sw_cache_timestamp:
  - 401650
bitly_link_linkedIn:
  - http://bit.ly/1PiUpPB
bitly_link_tumblr:
  - http://bit.ly/1PiUpPB
bitly_link_reddit:
  - http://bit.ly/1PiUpPB
bitly_link_stumbleupon:
  - http://bit.ly/1PiUpPB
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - dating
  - humor
  - long term relationship
  - marriage
  - Netflix
  - sex
  - Skyrim
format: link
---
I've been married long enough to be wary of these sorts of things happening with my wife and me. Keeping a relationship sexy is work, but it's easier when everyone involved pitches in.

[http://www.mcsweeneys.net/articles/](http://www.mcsweeneys.net/articles/kama-sutra-for-couples-who-have-been-dating-for-over-three-years)[kama-sutra-for-couples-who-have-been-dating-for-over-three-years](http://www.mcsweeneys.net/articles/kama-sutra-for-couples-who-have-been-dating-for-over-three-years)