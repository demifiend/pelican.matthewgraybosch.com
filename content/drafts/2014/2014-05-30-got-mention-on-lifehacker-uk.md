---
id: 664
title: Got a Mention on Lifehacker UK
date: 2014-05-30T19:25:50+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=664
permalink: /2014/05/got-mention-on-lifehacker-uk/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1LnPJ54
bitly_link_twitter:
  - http://bit.ly/1LnPJ54
bitly_link_facebook:
  - http://bit.ly/1LnPJ54
bitly_link_linkedIn:
  - http://bit.ly/1LnPJ54
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1LnPJ54
bitly_link_reddit:
  - http://bit.ly/1LnPJ54
bitly_link_stumbleupon:
  - http://bit.ly/1LnPJ54
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
format: aside
---
I'll take my <a title="10 Great Things You Can Do on Work Breaks" href="http://www.lifehacker.co.uk/2014/05/28/10-great-things-can-work-breaks" target="_blank">mentions</a> where I can get them. Rather than make a big deal, I'll just embed my earlier Google+ post. Tomorrow, expect Chapter 2 of Silent Clarion.

<!-- Place this tag in your head or just before your close body tag. -->


  


<!-- Place this tag where you want the widget to render. -->

<div class="g-post" data-href="https://plus.google.com/103251633033550231172/posts/D5KNvjHGHYd">
</div>