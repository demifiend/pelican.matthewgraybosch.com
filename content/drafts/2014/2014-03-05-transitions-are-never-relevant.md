---
id: 101
title: Transitions are Never Relevant
date: 2014-03-05T22:06:45+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=101
permalink: /2014/03/transitions-are-never-relevant/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapWP:
  - 's:159:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:2:"95";s:5:"pDate";s:19:"2014-07-14 07:00:33";}}";'
bitly_link:
  - http://bit.ly/1PiUpzc
bitly_link_twitter:
  - http://bit.ly/1PiUpzc
bitly_link_facebook:
  - http://bit.ly/1PiUpzc
bitly_link_linkedIn:
  - http://bit.ly/1PiUpzc
sw_cache_timestamp:
  - 401575
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - flirtation
  - naomi bradleigh
  - relevance
  - technique
  - the blackened phoenix
  - transition
  - "writer's craft"
---
Here's something else I'm removing from _The Blackened Phoenix_. It's a transition scene that doesn't really go anywhere. It's just Morgan and Naomi walking home and flirting. It's cute, but doesn't drive the plot. Worse, it gets in the way of me moving moving Astarte's scene to the beginning of Chapter 2, allowing her to wonder where the hell Morgan is _before_ I show what he's doing.

Scenes like this are the reason writers learn never to write transitions. They're just bloat.

[<img class="aligncenter size-large wp-image-65" alt="rule" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/rule-1024x178.png?resize=474%2C82" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/rule.png)Naomi regretted turning down Morgan's offer of a cab, which he repeated after the incident at Mr. Mouzone's hot dog stand, by the time they finally managed the trek up Broadway to 96th Street. The physical training that kept her in shape for the stage did not save her feet from growing sore in boots too new for her to have broken in. Worse, she twisted her ankle a block ago while skidding on a patch of ice made possible by a failed sidewalk heating coil. Morgan caught her, but in his haste he pressed her too tightly to him, making her rib injury hurt. She gritted her teeth as they turned the corner and began walking the last few blocks up 96th Street to Morgan's brownstone, where she would ask him to free her from her boots – and perhaps wash and massage her feet.

Morgan seemed to sense her fatigue, for he slowed a bit. "I asked Astarte to start up the hearth. I'll move a chair and get a bucket of hot water so you can soak your feet. Do you want me to call in a doctor to check your ankle?"

"Damn. You saw that?"

"I caught you while you stumbled, and you've been wincing the way you do when your rib's giving you trouble."

Naomi nodded, and forced herself to expand her ribcage with a deep breath despite the pain. "It's giving me trouble right now. I'm sure you didn't mean it, but you were a bit rough."

"Damn it." Morgan hung his head for a moment. "I'm sorry, Nims. I–"

Naomi understood Morgan's concern without needing an explanation. Every winter, at least one unfortunate pedestrian slid on ice or packed snow where a heating coil had failed, fallen, and suffered a concussion after their head struck the pavement. Seasonal public service announcements regularly advised people to watch their step, and to avoid sections of sidewalk or pavement that looked icy or were covered in snow. "You didn't want me to fall."

Morgan nodded, his expression still concerned, and Naomi decided a bit of flirtation might ease the mood. "What if I told you I like it rough?"

Morgan did not respond until they had reached his brownstone a couple blocks down the street. Instead of opening the door, he pressed Naomi against it and kissed her breathless before whispering in her ear. "Is that what you had in mind?"

Naomi found the doorknob, turned it, and let herself in while pulling Morgan inside by his collar. "That will do for a start."