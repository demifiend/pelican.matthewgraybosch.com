---
id: 303
title: 'Go home, Gmail. You&#039;re drunk.'
date: 2014-03-19T20:57:41+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=303
permalink: /2014/03/go-home-gmail-youre-drunk/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1WRdlXO
bitly_link_twitter:
  - http://bit.ly/1WRdlXO
bitly_link_facebook:
  - http://bit.ly/1WRdlXO
bitly_link_linkedIn:
  - http://bit.ly/1WRdlXO
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1WRdlXO
bitly_link_reddit:
  - http://bit.ly/1WRdlXO
bitly_link_stumbleupon:
  - http://bit.ly/1WRdlXO
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - 419
  - con
  - confidence game
  - fraud
  - i go chop your dollar
  - inheritance
  - nigerian
  - scam
  - suckers
---
I'm not actually unhappy with Gmail. I'm just surprised to see a Nigerian scam email make it to my inbox. Gmail is usually pretty good about catching this crap.

> from: Lisa Shane, M.D. LShane@memorialcare.org
> 
> subject: !!!!
> 
> <div dir="ltr">
>   <div dir="ltr">
>     <span style="color: #000000; font-family: Tahoma; font-size: small;">I have an Inheritance for you contact me now: <a href="mailto:atika@abdul-aziz.onlinesltd.com" target="_blank">atika@abdul-aziz.onlinesltd.com</a></span>
>   </div>
> </div>
> 
> * * *
> 
> **<span style="text-decoration: underline;"><span style="color: #444444; font-family: Verdana; font-size: small;">NOTICE: This email may contain PRIVILEGED and CONFIDENTIAL information</span></span>** <span style="color: #444444; font-family: Verdana; font-size: small;">and is intended only for the use of the specific individual(s) to which it is addressed. It may contain Protected Health Information that is privileged and confidential. Protected Health Information may be used or disclosed in accordance with law and you may be subject to penalties under law for improper use or further disclosure of the Protected Health Information in this email. If you are not an intended recipient of this email, you are hereby notified that any unauthorized use, dissemination or copying of this email or the information contained in it or attached to it is strictly prohibited. If you have received this email in error, please delete it and immediately notify the person named above by reply email. Thank you.</span>

Whoever this person really is, they must be lonely. Why not send them a message? Just don't let them bullshit you. There's no inheritance waiting for you. There's just some guy waiting for a chance to go chop your dollar. Don't be a schmuck.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Guys like this are waiting to laugh at you.