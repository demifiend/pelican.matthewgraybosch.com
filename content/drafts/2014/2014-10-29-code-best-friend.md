---
title: Code For Your Best Friend
excerpt: He knows where you live, and he just might also be a violent psychopath.
header:
  image: code-938776_1280.png
  teaser: code-938776_1280.png
categories:
  - Programming Considered Harmful
tags:
  - code
  - "code doesn't"
  - comments lie
  - design
  - House MD
  - maintainability
  - programming
  - refactoring
---
I recall a programming proverb dating from the mid-1990s that says, “Always code as if the guy who ends up maintaining your code is a violent psychopath who knows where you live.” A quick Google search suggests it arose in the Perl community, and attributes it to various sources.

It’s good advice, but not exactly something you’d expect to see on a motivational poster in an American workplace unless you work in a shop where management regularly jokes about improving worker morale by beating the crap out of them.

Instead, I would suggest that you design and implement your code as if the poor schmuck who will inherit your code after you leave is your best friend. Would you want your friend to chew up a bottle of aspirin a week as they try to make sense of your code? Would you want your friend to come to hate you for the suffering you inflicted upon them?

It isn’t even that hard to write code that another developer can take over without resorting to self-medication to kill the pain. We’ve known most of the principles for at least a decade:

## Indent your code properly.

This should be self-explanatory, and your preferred editor/IDE probably provides tools to help. Either [pick a style](https://en.wikipedia.org/wiki/Indent_style), or stick to the style already in use at your shop.

## Use meaningful variable names.

Something like `string driverFirstName` or `driverFirstName = ''` is self-explanatory, but I’ve seen lazy programmers use names like `drvfstnm` or `DRIVER_FN`. And the latter, if you’re used to C and languages derived from C, is easy to mistake for a constant if you don’t check the declaration.

## Use meaningful method names.

It’s the same principle as with variable names. A new developer is going to have an easier time understanding what a method named `UpdateDriversLicenseAddress()` does than if the method is called `UpdDLAddr()`.

## Don’t abbreviate.

As soon as you start using abbreviations, you force the next developer to spend time figuring out what the damn abbreviations mean. This breaks the developer’s flow and hampers productivity.

## Keep your methods short.

Anything method longer than thirty lines is probably trying to do too much, and is a good candidate for refactoring.

## Use the standard library instead of reinventing the wheel.

Admittedly, “standard library” is a C-specific term, but chances are your preferred programming language has something similar with a different name. Everything you implement yourself is more work for the next person. It's also more for _you_ to test and debug, and you probably don't get paid enough for that shit.

## Don’t repeat yourself.

The same functionality shouldn’t be implemented in two modules, because it will have to be updated in two modules if requirements change. It will also have to be tested twice. If you’re tempted to copy and paste code because you have a case that needs slightly different functionality, try inheriting from the base code.

## What about comments?

You may have noticed that I didn’t say anything about commenting your code. You can comment your code if you like, or if your shop’s coding standards specify comments when declaring classes and methods to identify them and state their purpose.

But I won’t read them. I don’t trust comments in code. To paraphrase Dr. House: Comments lie. Code doesn’t. I know from experience, because I’ve been the “next guy” — and I’m _nobody’s_ friend.
