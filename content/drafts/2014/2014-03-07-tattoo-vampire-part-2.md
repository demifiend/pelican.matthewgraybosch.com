---
id: 123
title: 'Tattoo Vampire &#8211; Part 2'
excerpt: Here's a story I wrote to promote *Without Bloodshed*. It also ties into *Silent Clarion*.
date: 2014-03-07T09:00:10+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - fiction
  - morgan stormrider
  - project harker
  - science fantasy
  - science fiction
  - serial
  - short story
  - starbreaker
  - tattoo vampire
---
"Dusk Patrol Tattoo? You want me to investigate a tattoo parlor? Why tell me this here, instead of calling me in?" Morgan glared at Saul Rosenbaum, who came to his studio apartment in the Bronx. While Morgan forced the place into a semblance of respectability, it was still a cheap apartment in an impoverished neighborhood. "I haven't even invited Christabel here."

Saul glanced around, taking out a cigar and a lighter. "I wanted to see what sort of place you chose for yourself instead of making do with a bunk at headquarters."

Morgan opened the windows; between the lingering odor of fresh paint and the tobacco in Saul's cigar, the apartment was beginning to stink. "Put that away. I don't want you smoking it in my home."

Saul shrugged, and put the cigar in his mouth without lighting it. "Not exactly hospitable of you, Adversary, but I suppose I should have asked."

"You're right on both counts, but let's return to the subject. Why are you here?"

"You said you wanted a mission. For your sins, you're getting one. Hell, I even brought it to you like room service, 'cause everyone gets what they want."

Morgan suppressed a smile. "I watched that movie, Saul. Don't tell me somebody named Kurtz is running this place."

"That's taking the joke too far. The proprietor is Quincy Westenra. We've got a long string of complaints about the guy, most of them unsubstantiated. People say he tranks them, and then molests them while they're out. We've got victims of both genders, so I wouldn't make any assumptions concerning his preferences. We got serious when a victim turned up HIV positive after getting a tat from this guy."

"Is he deliberately spreading the disease?"

"Not that we know of, but I'm not here because some sucker picked the wrong tattoo parlor and had to get HIV purged from her system. We sent in another Adversary, and Westenra made him disappear."

Morgan nodded. "Are we still giving the bastard due process, or extreme prejudice?"

Saul spread his hands. "That's up to him. I'm going to hand you off to Edmund Cohen now. He's going to tell you how we want the mission to go down."

With this, Saul left to make way for Edmund Cohen. Cohen was taller than Rosenbaum, and slimmer; instead of a cigar, he carried a faint hint of hashish with him, as if he had smoked some at breakfast. "Ready for your first real mission, kid?"

Morgan fought the urge to bridle at his former instructor's words. "I took the oath and wear the pins, sir. I am an Adversary, and my mission to Ursa Styrns was a real mission."

Cohen shrugged off Morgan's protest. He produced a cigarette case and opened it one-handed. "Have a joint and loosen up."

"No thanks."

"More for me, then." Cohen stood by an open window, lit up, and took a long toke before continuing. "Sure, the Ursa Styrns job was a legit mission. Somebody has to remind those people that there's a world beyond Wall Street that doesn't take kindly to attempts to fuck with the market. However, that's something any Adversary can handle – just like most of the jobs you've had the last three months."

"Did I not handle it?" Morgan doubted the answer would please him.

Cohen took another toke, imitating a dragon. "You handled it fine, but the XC thinks you're wasted on that kind of work. This tattoo job is better suited to your specialized talents. Quincy Westenra killed the last Adversary we sent to bust him. We found the poor bastard in a dumpster on the other side of Queens, and the coroner marked the cause of death as exsanguination."

"I think you should stop smoking that shit before you tell me Westenra's a vampire."

"Look on the bright side. You probably won't need a stake and garlic for this job." Cohen called up a set of files labeled Project Harker, and displayed them on Morgan's secondhand wall screen. "Just before Nationfall, the North American Commonwealth's military figured out that they had a platoon of soldiers with CPMD, and decided to experiment on the poor bastards. They wanted to enhance these soldiers' unconventional warfare capabilities. One experiment involved modifying them to subsist on human blood when no other food was available. The Commonwealth Army organized the survivors into a special forces unit code-named Dusk Patrol."

"And he calls his shop Dusk Patrol Tattoo? I suppose he had trouble letting the past go."

"Ask him yourself. We're sending you after him because we need somebody who can face Westenra on equal terms. The abilities and adaptability you displayed in training suggest you're the right Adversary for the job."

Morgan nodded. "Have other Dusk Patrol survivors turned up?"

"One turned up at Fort Clarion a few years ago, and murdered two young men before the Adversary sent to investigate killed him. Her report's in the dossier, but the names are redacted."

"Are there any other reasons to send me that might impact the mission?"

Cohen lit another joint before producing a folded sheet of paper and passing it to Morgan. "As a matter of fact, there is: you aren't marked as one of us. Most Adversaries get that tattoo the day they're sworn in."

Morgan shrugged. "So, that's my cover. I'm getting a tattoo to celebrate becoming an Adversary?"

"Hell, no." Cohen took a toke before continuing, "You have no idea this design is used by Adversaries. You're just some metalhead in civvies who thinks it looks cool. We're going to shoot you up with some nanotech that will neutralize the general anesthetic Westenra's likely to use while pretending to give you a local. When you think he's at his most vulnerable, spring the trap."

"What level of force am I authorized to use, should Westenra resist?"

Cohen shrugged, and glanced around the room. "For my part, I don't care if you shove five meters of rebar up his arse and plant the other end in front of his shop. However, the Society doesn't want Adversaries emulating pre-Renaissance Wallachian warlords when dealing with idiots who murder Adversaries. It's bad PR."

Morgan unfolded the paper, and studied the tattoo design. A parody of the caduceus, the design replaced the staff with an ornate sword, and made the twin serpents a pair of diamondback rattlesnakes. From the sword's point radiated ten roses. _Tartarus take Christabel if she objects, but I'm getting this tattoo. However, I won't be able to keep my sword handy once I strip down to get inked. An ankle holster might work, but my service pistol's too much gun to hide in my boot._ He looked up at Cohen. "I can start once my equipment requisition goes through."

"Don't you have gear?"

"Westenra will figure out something's off if I wear my usual weapons to his shop. And I'll need to tell Christabel and Naomi I won't be available to jam tomorrow."

Cohen shook his head. "You've got two girlfriends now? Is Naomi hot?"

Morgan rolled his eyes at Cohen's remark. Unable to trust himself to refrain from admitting that the tall, pale keyboardist Christabel recently brought aboard was not only attractive, but had graced his dreams for the last five years, he allowed himself only a short question. "Are we done here?"
