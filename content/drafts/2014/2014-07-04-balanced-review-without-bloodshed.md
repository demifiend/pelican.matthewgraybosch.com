---
title: A Balanced Review for Without Bloodshed
categories:
  - Project News
tags:
  - Amazon
  - balanced
  - disclaimer
  - isis
  - review
  - three stars
  - without bloodshed
---
I just got my first three-star review for [_Without Bloodshed_](http://www.matthewgraybosch.com/books/without-bloodshed/) on Amazon, from a reader called Isis. I'm satisfied with it, as it seems a balanced review. She had the following to say about my book's disclaimer:

> > First and foremost, I must share just how much I love the book's Disclaimer. It is chock full of sarcasm, letting the reader know just what they are getting themselves into. In my opinion the odds of the book being well worth reading go up dramatically when the Disclaimer states, “If you find any allegory or applicability, please consult a qualified professional for psychiatric evaluation and treatment.”

If it helps, the following is the disclaimer you'll find in all Starbreaker books:

> The following is a work of fiction. Any resemblance or similarities between the characters in this novel to living or dead persons in this world or any parallel world within the known multiverse is either a coincidence; an allusion to real, alternate, invented, or occult history; or a parody. If you find any allegory or applicability, please consult a qualified professional for psychiatric evaluation and treatment.

She made an observation common to other reviews: I've got characters, man, lots of characters. They can be hard to keep straight, but I've introduced most of the primary cast already. However, you'll meet Dr. Josefine Malmgren in _Blackened Phoenix_, as well as the 200 Series asura emulator prototype, Polaris. And I've got more new characters to introduce in _Heartless Savior_ and the final book, _A Tyranny of Angels_.

[You can read Isis' full review on Amazon.](http://www.amazon.com/Without-Bloodshed-Starbreaker-Matthew-Graybosch-ebook/product-reviews/B00GQ0BJOO/)
