---
title: "*Silent Clarion*, Track 10: &quot;Hell Bent For Leather&quot; by Judas Priest"
excerpt: "Check out chapter 10 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Some people buy t-shirts when visiting New York City. Naomi bought a motorcycle."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
The bartender looked so heartbroken by my departure this morning that I took pity on him and promised to stop for a repeat engagement before returning to London. No doubt Jacqueline would insist he had fallen for me, but I suspect he was infatuated with the metric shitload of money I helped him make.

I hit the streets wondering what I should do with my share of the windfall. Investment was right out. I already do that with a chunk of my Adversary's salary before I pay the bills, and I'll be damned if I'll do boring shit with money I earned on vacation.

What I wanted was something fun, something I could keep to conjure the memories I made with it. Maybe... oh, take a trip to Clarion and poke around, my curiosity well and truly piqued by last night's conversation with Malkuth? Hopping a train to Pittsburgh would be simple enough, but a ticket stub isn't my idea of a good souvenir. I'd paste it into my scrapbook and forget it until I was ready to bore my grandchildren.

A gang of bikers on restored gasoline-powered choppers rumbled to a stop at my street corner. Their rides' idling growl muffled their laugher and conversation. On closer inspection, the group looked a little too clean cut. Instead of an outlaw biker gang, they were just a crew of weekenders trading business suits for leathers. A rider who just needed a woman half his age riding pillion to complete his midlife crisis looked at me and called out, "Hey, sexy! Wanna climb aboard and have the ride of your life?"

The rider's catcall helped me reach a decision. It was time to fulfill a childhood dream and get a horse of my own. An iron horse. I waved at him. "Thanks, mate, but I think I'll get my own ride. Know a good dealer?"

He didn't stick around long enough for me to finish my question, but peeled out with his crew the second the light changed. Bollocks to him, then. If he was that impatient, I doubt he could have given me a halfway decent ride anyway.

Not that I needed him. A cab advertising a Conquest Motorcycles dealer in Hell's Kitchen drove past. Capturing the address with my implant, I found the shortest route from my location in the Upper West Side and set out on foot. It wasn't far, and I did promise Director Chattan I'd keep up with my PT.

I stopped for coffee and a bagel at a dim delicatessen called Maimonides' Deli, which was often full of old gentlemen arguing over chess in half a dozen languages other than English. I used to stop here every morning before classes, and I remembered the clerk. His namesake was a famous philosopher. "Hello, Mr. Spinoza. It's been a while."

Spinoza's dentures flashed as he smiled. "Medium black coffee, and a toasted everything bagel with plain cream cheese. Aren't you late for class, Ms. Bradleigh?"

I laughed as I paid him. "I graduated a couple of years ago, and was assigned to the London chapter."

"Ah! I remember now. You made a point of stopping in to tell me. Are you happy?" He handed me my coffee and bagel.

Rather than answer immediately, I tried my bagel. It was as good as I remembered, the crunch of rich dough topped with sea salt, poppy and sesame seeds, and roasted onion and garlic contrasting with the slightly salty-sweet cream cheese. The coffee was perfect, and blacker than Sabbath. "I'm content for now. Did you know I haven't been able to find a decent bagel anywhere in London?"

Mr. Spinoza chuckled. "You should come back to New York, then. I could introduce you to my grandson. He sells motorcycles. He makes good money, and you could focus on your music."

His suggestion was such an old-fashioned sentiment for the end of the twenty-first century that it seemed almost ridiculous, but he meant well. "Does he sell Conquests here in Hell's Kitchen, by any chance? I'm on my way to buy a cycle and ride west."

"I'll tell him to expect you. I'm sure Jacob will deal with you personally, instead of leaving you to some clerk who will give you the hard sell on a bike that isn't perfect for you."

Some new customers walked in, so I stepped aside to give them access to the counter. "I'd appreciate that, Mr. Spinoza. It was good to see you again."

"Have a good day, Naomi."

The rest of my walk was slow and pleasant as I ate my breakfast. Before I knew it, I had arrived at Spinoza Motors with my half-finished coffee still in hand. A man resembling Mr. Spinoza finished his conversation with one of his sales staff before coming to greet me. "You must be Naomi Bradleigh. Papa Baruch didn't tell me you'd be gorgeous. I'm Jacob Spinoza."

"What did he tell you, Mr. Spinoza?"

"Just that I was to treat you right." Spinoza chuckled as he opened the door to his office and beckoned me inside. "Said he didn't want me delegating you to one of my staff lest they try to sell you on a Vestal."

My imagination drew a blank as I tried to visualize myself riding a Vestal. No doubt I'd look prim and proper riding such a dainty little scooter, but it wouldn't be me. Rather than follow him into his office, I cut to the chase. "I appreciate your personal attention. Can you show me your Conquests?"

"A Conquest?" Spinoza studied me for a moment. "Yeah, I can see it. You know what? I've got a model that might be perfect for you out back." He let the office door snick shut behind him and led me to a rear exit near the garage's waiting area.

At least twenty Conquest Type C bikes leaned on the kickstands, parked side-by-side. All but one was black, and indistinguishable from the model shown in all of Conquest's advertising. Conquest Motorcycles only made one type of motorcycle, and you could have it in any color you liked as long as you liked black.

The lone exception stood apart from the others. It sat lower, to caress the road. The suspension looked capable of providing a smooth ride across lunar regolith. Part of the frame had been cut away to accommodate bigger batteries and a more powerful motor. Crimson paint and polished chrome flashed in the sun, challenging me to mount up. "It's gorgeous. May I try it out?"

Jacob produced a key fob and tossed it to me. "Of course. Mind if I ride pillion?"

After we returned from our test ride, I flashed my best stage smile at Jacob while caressing the leather seat. "Tell me more."

He cleared his throat. "The NDA won't let me name names, but this was a custom job for a certain rock musician. He paid half as a deposit, but died in a helicopter crash a couple weeks ago. His estate wouldn't pay the rest or accept delivery."

I could guess at who Jacob meant and its implication on the price expected, but it wasn't germane to the discussion. The relevant fact was that Jacob Spinoza had a custom job he wanted to move, and he would use the implication of star power to jack the price up. "How much did he owe?"

"He owed fifty grams."

Fifty? *Fifty!* Fifty grams when I got my coffee and bagel for two point five milligrams in the middle of fucking Manhattan?! There was no way in any of the hells imagined by humanity I was going to pay such an exorbitant sum. I could buy three bog-standard Conquest Type Cs with money to spare for lunch, tolls, and a down payment on a three-bedroom house outside London for the price this slick bastard was trying to extort.

I closed the distance between us, and picked a bit of lint from his jacket. "I hope you can offer me a better deal than that, Mr. Spinoza. I'm willing to bet you turned a modest profit already from the deposit."

Jacob shook his head. "I'm sorry, Ms. Bradleigh, but I'm still five grams in the hole. The battery and engine are also custom work. You can cross five hundred kilometers in two hours before you need to recharge. You can go even further if you don't go above a hundred and twenty an hour."

"Ten grams sounds reasonable. Half of that is profit for you, and triple your markup on a plain Type C."

Jacob mastered himself quickly, but I still caught the 'How dare she insult me like that?' expression in the way his eyes tightened for just a moment. I smiled at him and sweetened the deal. "Ten grams in cash. And I still need to buy a helmet."

Jacob shook his head. "I need at least fifteen."

"The hell you do." I stepped away from the chopper, my hand resting on the hilt of my sword. "Seven and a half."

"That's less than your original offer!" Jacob was rather cute when flustered.

"I can go lower if you continue to annoy me." I circled the bike, taking a closer look. "Whether you turn a profit on this deal is no concern of mine, especially since you might be bullshitting me. The recently deceased unnameable celebrity whose estate won't take delivery is an old con."

"Grandpa told me you were this sweet, innocent girl. You're staring me down like you're ready to pull your sword on me."

He was still flustered, and still cute. But if he's going to drag the kindly old man into this, it's time for the claws. "So, you thought you could take advantage of me? Listen, asshole, I don't care if your grandfather is God. Six grams is my final offer."

---

### This Week's Theme Song

"Hell Bent For Leather" by Judas Priest

{% youtube OwUpV1_tteY %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
