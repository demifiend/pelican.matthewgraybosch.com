---
title: "Archive Binging on RWBY"
excerpt: "RWBY is an easy series to binge on, because the individual episodes are so short."
header:
  image: rwby-2013-teaser.jpg
  teaser: rwby-2013-teaser.jpg
categories:
  - The Tarnished Screen
tags:
  - action
  - anime
  - comedy
  - low budget
  - Monty Oum
  - Rooster Teeth
  - RWBY
  - web animation
---
I remember hearing about [Monty Oum's](https://twitter.com/montyoum) web animation series [RWBY](http://en.wikipedia.org/wiki/RWBY) back in 2012, and I might even have seen some of the trailers. I didn't keep up with it because I tend not to watch anything on YouTube other than rock videos.

I pulled RWBY up on a whim after seeing how short every episode was. Catherine and I watched the first volume this morning.

## RWBY: Volume 1

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/-sGiE10zNQM?list=PLnzPXnz47JdEtI2EOVcSe69GzAYRwpXA5" frameborder="0" allowfullscreen></iframe>
---

You can see for yourself that the animation is a bit rough, but not terrible. Catherine remarked that it reminded her of the _Persona_ games ATLUS released for the PlayStation2 console. It gets better in the second series, and there's a third released after Monty Oum's death.

## RWBY: Volume 2

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/PzPZ6joXq5Y?list=PLPAzAH6uCHzo5uEKul75QFYaiI5XMRHY4" frameborder="0" allowfullscreen></iframe>

---

I'm still not convinced Cinder is bad. I think she's just drawn that way. :wink:

## RWBY: Volume 3

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/946xgoU4fkQ?list=PLa1n-u-69G0zZWTFSN-QsfZIaCnLK-frd" frameborder="0" allowfullscreen></iframe>

---

Not sure this season's finished, so I'm letting it wait until Rooster Teeth releases episodes from Volume 4.

## Yeah, but is it any good?

The story is pretty standard for this sort of animation: teenagers from diverse backgrounds gather at a school for young preternatural warriors. They face ordeals together, and appear to be working to uncover a conspiracy that the Proper Authorities&#0153; don't seem to be prepared to tackle.

RWBY offers plenty of slapstick, and some genuinely stunning fight scenes, while also hinting at a potential for deep narrative. The music kicks ass, and there are plenty of visual references for those paying attention. A villain from the first episode, for example, has a character design straight out of Stanley Kubrick's production of _A Clockwork Orange_.

Nor do Oum and his team at Roosterteeth scrimp on giving minor characters an opportunity to shine. In particular, Nora is hilarious. She reminds me of a more innocent Claire Ashecroft from _Without Bloodshed_.

Best of all (or worst of all), RWBY is short and sweet. The entire first series clocks in at around a couple of hours. Despite this, one still gets the sense that the Rooster Teeth crew worked through a coherent narrative arc while setting the stage for more shenanigans in the second series.

Of course, those intent on doing so will find reasons to dislike RWBY. I made the mistake of looking at YouTube comments and noted one viewer asking why if Ruby and Yang are sisters, they don't have the same family names. Also, the voice acting isn't perfect, and neither is the animation.

Regardless, I recommend at least giving RWBY a shot. The series is really starting to come together.

## Credits

Featured artwork by [Monty Oum](http://montyoum.deviantart.com/art/RWBY-336841243). <http://montyoum.deviantart.com/art/RWBY-336841243>
