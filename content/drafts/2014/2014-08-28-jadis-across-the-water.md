---
title: 'Jadis: Across the Water'
excerpt: This is a modern progressive rock band from the UK that doesn't get enough respect.
date: 2014-08-28T16:58:03+00:00
permalink: /2014/08/jadis-across-the-water/
header:
  image: jadis-across-the-water-1994.jpg
  teaser: jadis-across-the-water-1994.jpg
categories:
  - Music
tags:
  - Across the Water
  - Jadis
  - music
  - progressive rock
  - writing
---
I've been grooving to this album by UK progressive rock band Jadis, led by Gary Chandler on guitar. Check out the good stuff.

<iframe width="560" height="315" src="https://www.youtube.com/embed/fhjIud6QMCo" frameborder="0" allowfullscreen></iframe>

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL425610FA7862861E" frameborder="0" allowfullscreen></iframe>

---

This is actually good writing music. Following the changes in the melody gets me into a flow, which helps the words come.
