---
title: 'Alice Cooper: &quot;Welcome to my Nightmare&quot;'
excerpt: Alice Cooper + Muppets = Awesome
date: 2014-03-06T09:59:15+00:00
header:
  image: alice-cooper-muppet-show.jpg
  teaser: alice-cooper-muppet-show.jpg
categories:
  - Music
tags:
  - alice-cooper
  - classic rock
  - old school
  - shock rock
  - Welcome to my Nightmare
  - Muppets
  - Jim Henson
  - The Muppet Show
---
This is the title track your the album I mentioned in my last post, for those who didn't get exposed to good rock as kids. 🙂

Yes, Alice Cooper did the Muppet Show.

<iframe width="420" height="315" src="https://www.youtube.com/embed/iz0lXNNkqac" frameborder="0" allowfullscreen></iframe>
