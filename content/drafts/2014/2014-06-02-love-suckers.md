---
id: 690
title: Love is for Suckers
date: 2014-06-02T08:00:29+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=690
permalink: /2014/06/love-suckers/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1N0l3Kx
bitly_link_twitter:
  - http://bit.ly/1N0l3Kx
bitly_link_facebook:
  - http://bit.ly/1N0l3Kx
bitly_link_linkedIn:
  - http://bit.ly/1N0l3Kx
sw_cache_timestamp:
  - 401557
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - courtship
  - feminism
  - gender
  - love
  - masculism
  - patriarchy
---
That's right. You heard me. If you think I'm trolling, think again. I mean it, at least when it comes to the way American culture tells us love should work. Love is for suckers, and we're _all_ suckers because that's how the Big Lie works. If you hear it often enough, you start thinking it's true.

Stop me if you've heard this story before.

Man meets woman. Man falls in love. Woman isn't impressed. Man persists, and does outrageous things to prove himself worthy of woman's love. He kills dragons, conquers nations, makes momentous scientific discoveries, creates artistic masterpieces, or just shows up at her house with a boombox playing her favorite song. Woman decides he's worthy after all, or settles because she's afraid he might simply force himself on her or kill her, and they live unhappily ever after.<figure style="width: 2230px" class="wp-caption aligncenter">

[<img src="http://i2.wp.com/api.ning.com/files/xufWM9U-8fhYHZq8n*jbcKgWkfUSF13A98zpBwWioMbprGEukEEopUeKdwTsQE46jeVnU6GYC1T3024TYzqpWySdYKP3L78k/FrankDickseeLaBelleDameSansMerci.jpg?resize=840%2C614" alt="Our myths concerning love predate Disney." data-recalc-dims="1" />](http://www.paganspace.net/photo/dicksee-la-belle-dame-sans)<figcaption class="wp-caption-text">Frank Dicksee's "Belle Dame Sans Merci". 1902</figcaption></figure> 

This is the script we're supposed to follow in Western society. You see it all the time in literature. It's the story Hollywood sells us a dozen times a year with different titles and casts. It's the lie Disney feeds your kids. It feeds us all the following pernicious assumptions:

  * Sex is love. Love is sex. To separate the two is a sin.
  * Men are supposed to want sex with women.
  * Women are supposed to want love from men.
  * Men have to prove themselves _worthy_ of sex.
  * Women are the gatekeepers of sexual pleasure.
  * Sex is a reward to be earned, or a prize to be won.
  * A man's worth is measured by how many women he's had.
  * A woman's worth is measured by how many men lust after her in vain.

These assumptions play a huge role in foul up relations between heterosexual cis men and women, to say nothing of how they complicate life for <a title="Queer Dictionary: QUILTBAG" href="http://queerdictionary.tumblr.com/post/3899608042/quiltbag" target="_blank">QUILTBAG people</a>. I can't prove it, but they may even be the foundation of the patriarchy.

Because of this toxic script, and the toxic gender roles our society imposes, men think that to get the human touch, connection, affection, and esteem _that we all need_ to thrive as fully human beings, they must seek out sex with women. Worse, men feel obligated to seek out sex with "hotties": women who most closely conform to our society's ideals of feminine beauty.

Men beat themselves into the ground working in service to a capitalist system because that's what our society tells us men should do to prove themselves worthy of sex with beautiful women. We bust our humps, buy products we don't necessarily need and don't particularly want, and remake ourselves to fit our culture's ideal of a "real man" who's worthy of sex with a "hot" woman.

And then the woman we've fixated upon has the temerity to exercise autonomy by rejecting us anyway, despite all we've done to prove ourselves worthy. Such effrontery must surely be punished! Right?

Wrong. We blame the women who hurt us because of the expectations we imposed on her, instead of blaming the expections themselves or the expectations we impose on ourselves and our fellow men.

The existing gender roles are crap. As a man, I think we men deserve better. I think women deserve better, too. We shouldn't have to contort ourselves to fit into a "man box" or a "woman box", or starve ourselves while running outselves ragged to fit some artificial ideal of beauty that exists mainly for the fashion industry's convenience.

We deserve a society where sex isn't a prize, but another way for people to connect to each other if they want to. We deserve a society where we can work to make _our_ lives better, instead of enriching already rich people by buying garbage because some advertiser tells us it will make us worthy of the affection, esteem, or connection we seek from others.

I think women have been fighting for this society for over a century, but feminism isn't enough. Women can't win this battle on their own, otherwise they would have. Nor are male "allies" sufficient.

We men need to create a masculist movement that _complements_ feminism and draws upon the lessons feminists already learned in the course of their struggle. We need a masculism and a feminism that create a humanistic whole. The existing Men's Rights movement, the seduction community, and the anti-PUA groups aren't going to help us.

Instead of helping us direct our justified anger in a constructive manner, they encourage us to police each other's masculinity while hating women for exhibiting the very femininity we seek in them because we don't feel we have the right to recognize it in ourselves. They still serve the patriarchy, though they don't realize it.

The first step is to stop thinking you have to prove yourself worthy of love. The second step is to stop thinking women owe you sex if you do all the right things. The third is to stop thinking you must do certain things or act a certain way to be a "real man" or a "real woman".

And stop selling this bullshit to your kids. They deserve better.

The people who tell you how to live, how to act, how to feel, how you should look and what you should do to be worthy of another person's love and respect are only human. They are no more than your equals. They bleed the same red you do, and just as easily. They will someday die, just like you.

Live for yourself, not for them or their expectations. Stop being a sucker.