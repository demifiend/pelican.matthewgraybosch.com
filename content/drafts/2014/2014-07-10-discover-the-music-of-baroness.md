---
title: Discover the Music of Baroness
excerpt: They're better than Mastodon, but don't get as much respect.
header:
  image: baroness-upset-photo.jpg
  teaser: baroness-upset-photo.jpg
  caption: "Photo from [Upset Magazine](http://www.upsetmagazine.com/features/baroness-purple-reign/)"
gallery:
  - url: baroness-first-ep.jpg
    image_path: baroness-first-ep.jpg
    alt: "Baroness: First (EP)"
  - url: baroness-second-ep.jpg
    image_path: baroness-second-ep.jpg
    alt: "Baroness: Second (EP)"
  - url: baroness-unpersons-grey-sigh.jpg
    image_path: baroness-unpersons-grey-sigh.jpg
    alt: "Baroness/Unpersons: A Grey Sigh in a Flower Husk"
  - url: baroness-red-album.jpg
    image_path: baroness-red-album.jpg
    alt: "Baroness: Red Album"
  - url: baroness-blue-record.jpg
    image_path: baroness-blue-record.jpg
    alt: "Baroness: Blue Record"
  - url: baroness-yellow.jpg
    image_path: baroness-yellow.jpg
    alt: "Baroness: Yellow & Green (Yellow)"
  - url: baroness-green.jpg
    image_path: baroness-green.jpg
    alt: "Baroness: Yellow & Green (Green)"
  - url: baroness-purple.jpg
    image_path: baroness-purple.jpg
    alt: "Baroness: Purple"
date: 2014-07-10T22:49:49+00:00
categories:
  - Music
tags:
  - Amazon Prime Music
  - art-nouveau
  - Baroness
  - discovery
  - groove
  - heavy metal
  - old school
  - progressive rock
  - southern rock
  - stoner metal
---
I finally noticed that Amazon had gotten into the streaming music game with Prime Music. Since I already pay these guys eighty bucks a year for Amazon Prime, I figured I'd give 'em a shot. I installed the Amazon Music app on my phone, and got on their website to [check out their heavy metal selection](http://www.amazon.com/s/ref=sr_nr_n_11?rh=n%3A163856011%2Cp_85%3A8755839011%2Cp_n_feature_browse-bin%3A625150011%2Cn%3A!624868011%2Cn%3A625011011&bbn=624868011&ie=UTF8&qid=1405044481&rnid=624868011). I found several old favorites, like _Blizzard of Ozz_ and _Powerslave_, but I also found some new bands to try. One of them is [an act called Baroness](http://yourbaroness.com).

I admit it, I added their albums to my library on the strength of their covers. Just look at them.

{% include gallery caption="all covers by guitarist/vocalist [John Dyer Daizley](http://aperfectmonster.com/)..." %}

These are some gorgeous covers, reminiscent of both 1970s rock and metal and [art nouveau](http://en.wikipedia.org/wiki/Art_Nouveau). The music seems reminiscent of Lynyrd Skynyrd, the Marshall Tucker Band, and the Allman Brothers, which shouldn't surprise me since Baroness hails from Savannah, Georgia.

Their _Red Album_ and _Blue Record_ also reminds me of The Sword's first albums, _Age of Winters_ and _Gods of the Earth_. There's plenty of progressive material in here as well. I'm not sure what to make of _Yellow and Green_ yet, but there's a serious groove to be found in "Take My Bones Away".

I regret not discovering Baroness sooner, but I've said the same about just about every band I like, from Coheed and Cambria all the way back to the Blue Oyster Cult and Black Sabbath. It's not that I want the hipster cred. Trust me when I say don't have the figure for skinny jeans.

Actually, Baroness kinda reminds me a bit of the BOC. I think it's the guitar work, and the emphasis on tight drumming and bass. Why not give 'em a listen using the Spotify widgets below, and decide for yourself? Just keep in mind that their material from _Yellow and Green_ is different from their older albums. Good stuff, either way.

## Baroness & Unpersons: _A Gray Sigh in a Flower Husk_ (2007)

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A0gSIjRqY5h16h7BnRqjxRC" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Baroness: _Red Album_ (2007)

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A7HjDc1R38sIpwbKHOrbBNR" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Baroness: _Blue Record_ (2009)

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A7540tn9HKPt2PbPPyR5dRP" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Baroness: _Yellow & Green_ (2012)

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A4mSoz87AFyUIcZlCmwbI8s" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Baroness: _Purple_ (2015)

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A25FRZSpdZCczTURlAcMm6r" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
