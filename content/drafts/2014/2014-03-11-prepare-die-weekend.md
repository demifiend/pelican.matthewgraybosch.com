---
id: 195
title: Prepare to Die This Weekend
date: 2014-03-11T08:20:47+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=195
permalink: /2014/03/prepare-die-weekend/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link_linkedIn:
  - http://bit.ly/1PiRkz7
sw_cache_timestamp:
  - 401650
bitly_link_facebook:
  - http://bit.ly/1PiRkz7
bitly_link:
  - http://bit.ly/1PiRkz7
bitly_link_twitter:
  - http://bit.ly/1PiRkz7
bitly_link_tumblr:
  - http://bit.ly/1PiRkz7
bitly_link_reddit:
  - http://bit.ly/1PiRkz7
bitly_link_stumbleupon:
  - http://bit.ly/1PiRkz7
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - 300
  - dark souls
  - dark souls ii
  - jethro tull
  - locomotive
  - prepare to die
  - tonight we dine in hell
---
Check out this trailer for _Dark Souls II_, which hits the fan this weekend &#8212; though Amazon already shipped my copy, and I'm supposed to get it this Thursday.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

You hear that music? That's Jethro fucking Tull. It fits for some reason, but it really reminds me of this fan-made clip.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Anybody else prepared to die this weekend? I am. And guess where I'm gonna be eating.

[<img class="aligncenter size-full wp-image-196" alt="dark-souls-dine-in-hell" src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/dark-souls-dine-in-hell.png?resize=600%2C338" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/dark-souls-dine-in-hell.png)