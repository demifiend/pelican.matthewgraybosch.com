---
id: 1367
title: Molly Crabapple Has Guts
date: 2014-09-18T17:01:44+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1367
permalink: /2014/09/molly-crabapple-has-guts/
snapLI:
  - 
snapPN:
  - 
snapDL:
  - 's:265:"a:1:{i:0;a:7:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"013e455227e71f3459beabc795cdd7c3";s:5:"pDate";s:19:"2014-09-18 17:02:34";}}";'
snapWP:
  - 
snapIP:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snapSC:
  - 
snap_MYURL:
  - 
snapEdIT:
  - 1
snapDI:
  - 
snapSU:
  - 
snapBG:
  - |
    s:357:"a:1:{i:0;a:8:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:74:"http://matthewgraybosch.blogspot.com/2014/09/molly-crabapple-has-guts.html";s:5:"pDate";s:19:"2014-09-18 17:02:17";}}";
snapLJ:
  - 
snapGP:
  - 
snap_isAutoPosted:
  - 1
bitly_link:
  - http://bit.ly/1PjLAVF
bitly_link_twitter:
  - http://bit.ly/1PjLAVF
bitly_link_facebook:
  - http://bit.ly/1PjLAVF
bitly_link_linkedIn:
  - http://bit.ly/1PjLAVF
sw_cache_timestamp:
  - 401681
bitly_link_tumblr:
  - http://bit.ly/1PjLAVF
bitly_link_reddit:
  - http://bit.ly/1PjLAVF
bitly_link_stumbleupon:
  - http://bit.ly/1PjLAVF
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"f603e50516bd";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:68:"https://medium.com/@MGraybosch/molly-crabapple-has-guts-f603e50516bd";}'
categories:
  - Stuff I Found
tags:
  - America
  - art
  - interview
  - molly crabapple
  - money
  - Pacific Standard
---
I'm serious. Molly Crabapple actually makes her living off her art. I still have a fucking day job like a goddamn chickenshit loser. This is what she has to say about the business end of art for [Pacific Standard](http://www.psmag.com/navigation/business-economics/make-living-molly-crabapple-90799/).

> I’ve noticed that the people who obfuscate the most about the financial realities of art have another form of income that they don’t want to talk about. They have a spouse who is making a lot of money or they come from a wealthy family. That’s why they are able to disregard financial reality. They don’t have the same pressures that other people do.
> 
> Or maybe they just come from an awesome European country that has an actual safety net for citizens and provides government funding for the arts. I wish our country was like that. I wish there were grants for artists. I wish we lived in a country that had a universal basic income, or even just where everyone was insured, so that risking dying of untreated illnesses wasn’t the cost of pursuing your dreams outside of a day job.
> 
> But we don’t. **We live in a brutal, cutthroat country. Money, unfortunately, determines so much of one’s health, safety, freedom.** I think it’s important to acknowledge the realities of our lives. 

She's right. We live in a brutal, cutthroat shithole of a country that takes everything it can from individuals &#8212; and only gives back when forced to do so.

I can't pretend that money doesn't matter for reasons other than those Ms. Crabapple offers. I don't have a rich mommy and daddy. I didn't marry money. I don't live in a civilized country. As a man, I have no choice but to work for a living since I missed my chance to make easy money as a rent boy.