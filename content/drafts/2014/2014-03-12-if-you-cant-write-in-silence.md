---
id: 213
title: 'If You Can&#039;t Write in Silence, Write to Music'
date: 2014-03-12T15:59:50+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=213
permalink: /2014/03/if-you-cant-write-in-silence/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1N0vspC
bitly_link_twitter:
  - http://bit.ly/1N0vspC
bitly_link_facebook:
  - http://bit.ly/1N0vspC
bitly_link_linkedIn:
  - http://bit.ly/1N0vspC
sw_cache_timestamp:
  - 401648
bitly_link_tumblr:
  - http://bit.ly/1N0vspC
bitly_link_reddit:
  - http://bit.ly/1N0vspC
bitly_link_stumbleupon:
  - http://bit.ly/1N0vspC
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - album
  - concentration
  - delain
  - flow
  - flow state
  - galneryus
  - headphones
  - heavy-metal
  - inspiration
  - japanese power metal
  - joe satriani
  - liquid tension experiment
  - mindfulness
  - music
  - playlist
  - screenshot
  - silence
  - Syu
  - technical death metal
  - writing
---
<a title="Deconstructing Yourself: Concentration and Silence" href="http://deconstructingyourself.com/concentration-and-silence.html" target="_blank">Michael Taft wrote in 2011 about the pernicious effects of excessive background noise on the human mind.</a> In addition to linking to a [British Medical Bulletin](http://bmb.oxfordjournals.org/content/68/1/243.full) article concerning noise pollution, Mr. Taft advocates seeking silence and making it part of one's meditative practice to aid mindfulness. This is reasonable advice for writers, but not necessarily realistic if you work for a living, have familial responsibilities, and therefore cannot count on having quiet time in which to write.

My alternative is to learn how to write with music playing. My experience is that the right music helps me get into a flow state and focus on writing. The right lyrics can offer inspiring imagery. There's nothing like technical death metal to silence the inner editor. However, since you're likely to be writing outside your home and around other people <a title="Become a Lunch Break Novelist" href="http://www.matthewgraybosch.com/2014/03/10/become-lunch-break-novelist/" target="_blank">on your lunch break</a> if you have a <a title="Famous Authors with Day Jobs" href="http://www.matthewgraybosch.com/2014/03/11/famous-authors-with-day-jobs/" target="_blank">day job</a>, it is unwise to listen to any sort of music without a good set of headphones.

So, got a good pair of headphones? Excellent. Now you need some music and a way to play it. If you're one of those pen-and-paper writers and have a smartphone, chances are you've got enough storage to put _some_ music on your phone. The iPhone has the iTunes app by default, and Android phones usually have a crappy music player app developed by the device manufacturer, but you're better off ignoring that in favor of Google Play, which will play music files loaded on your device. I'm sure Windows Phone also has a music player app of some kind, but Microsoft doesn't pay me to care about their mobile devices.

If you're using a laptop, there's no reason to bring your phone when you go and write; I've never seen a laptop that didn't have a headphone jack. Since I run Linux, I favor an app called <a title="Quod Libet: a Music Player/Tagger app for Linux" href="https://code.google.com/p/quodlibet/" target="_blank">Quod Libet</a> because it has a mode that lists albums on the left and tracks from the selected album on the right. It looks like this:<figure id="attachment_214" style="width: 474px" class="wp-caption aligncenter">

[<img class=" wp-image-214" title="Quod Libet on Ubuntu 14.04 (beta), playing Delain" alt="Quod Libet on Ubuntu 14.04 (beta), playing Delain" src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/quodlibet-1024x535.jpeg?resize=474%2C247" data-recalc-dims="1" />](http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/quodlibet.jpeg)<figcaption class="wp-caption-text">Quod Libet on Ubuntu 14.04 (beta), playing music by Delain</figcaption></figure> 

Now that you have headphones and a means to play music, the last step is to select some good writing music. It would be presumptuous of me to give you specific advice on _this_ score, because tastes vary. My taste for female-fronted symphonic metal bands like Delain isn't universal.

However, I might be able to offer some general suggestions. First, I recommend music with complex melodies, harmonies, and rhythms. Second, don't just listen to one artist/band or one album. Compile a collection of artists/bands and a wide variety of albums to suit different moods.

For example, I made a recent habit of listening to music by a Japanese power metal band called <a title="Galneryus on Wikipedia" href="http://en.wikipedia.org/wiki/Galneryus" target="_blank">Galneryus</a> while writing, particularly their first and third albums, respectively titled _The Flag of Punishment_ and _Beyond the End of Despair_. <a title="Galneryus guitarist Syu on Wikipedia" href="http://en.wikipedia.org/wiki/Syu" target="_blank">Syu</a> plays a mean guitar, and I can ignore the lyrics. Sometimes I'll put on a female-fronted symphonic metal band like Delain or Within Temptation, or some instrumental rock like Joe Satriani or Liquid Tension Experiment. It's all about complex melody, harmony, and rhythm. It's all about the flow.

If you can find a quiet place in which to write, and get into a flow state without music, the science to date suggests that's the ideal. However, I live and write in the real world, and I'd get nothing done if I only wrote under ideal conditions. A good pair of headphones and a few gigabytes of heavy metal is my compromise. What's yours?