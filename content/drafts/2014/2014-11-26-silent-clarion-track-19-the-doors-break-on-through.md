---
title: "*Silent Clarion*, Track 19: &quot;Break On Through&quot; by The Doors"
excerpt: "Check out chapter 19 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Delightful. Absolutely brilliant. I instructed Sheriff Robinson to get me a hundred militia volunteers, and what does he do? He turns my mission into the bloody Children's Crusade. Perhaps one in five was armed and in uniform. The rest had machetes or hatchets on their hips. One beefy youth with spectacles shouldered a sledgehammer.

None of them looked a day over eighteen, which would still be three years too young to serve militia duty. The minimum age is twenty-one to prevent younger people from being brainwashed into blindly obeying orders. Not that Sheriff Robinson seemed to give a damn. If anything, he had puffed himself up like some loathsome, vaguely humanoid toad.

"Sheriff, I need a word with you inside." Time to deflate his ego. "What were my instructions?"

His eyes got shifty, as if we were playing poker and I caught him with an ace up his sleeve. "You wanted a hundred volunteers. Here are a hundred volunteers."

"How many of them are actually old enough to serve militia duty?"

Now he looked away, and backed up a step. "Twenty of them. The rest are here with their parents' permission."

That explains why the aforementioned rest was unarmed and out of uniform. "Then what the hell are those kids doing here? My orders specified the use of local militia because we're dealing with military ordnance. I need people who can be trusted to follow orders and safely handle weapons."

Robinson indicated the people outside with a sweep of his arm. "Adversary, I understand you're from the city where people only handle swords unless they're training for militia duty. Christ, the goddamn Phoenix Society even makes the police carry swords."

Does the good Sheriff resent being forced to trade in his pistol for a gladius? Rather than hunt down a suitably tiny violin, I let him have his say. "But out here we grow up with guns. Most of those kids first learned to handle BB guns when they were six. They'll be fine, and we shouldn't need more than twenty militiamen against whatever old men still lurk at the fort."

"Hold on a moment." Tempting as it is to arrest Robinson on the spot, do I even have just cause for doing so? Holding my fingertips to my ear so Robinson would understand, I fired up secure talk. ｢Malkuth, it's Naomi. You watching my feed?｣

｢I'm monitoring Robinson's as well. He isn't quite in violation of either the letter or the law or its spirit, but he's dancing on thin ice. Don't trust him.｣

｢Oh, I won't. Instead, I'll give him all the rope he wants. Let's see if he hangs himself.｣ Dropping out of secure talk, I cleared my throat to get Robinson's attention. My conversation with Malkuth probably took all of two seconds, but the Sheriff was already bored. "Sheriff, I will hold you personally responsible for the safety of the kids you insist on bringing along with us. If one of them so much as stubs their toe on a tree root, I will place you under arrest on an abuse-of-power charge."

Robinson stared at me a moment. "Don't you think that's a bit excessive, Adversary?"

"Compared to summary execution? Not really." Arresting Robinson would be a dicey situation. Would it turn the militia against me? Twenty against one isn't a fight I'm likely to win. A hundred to one if the younger kids get involved is even nastier. "I'd rather we just got this done so I can leave town and be nothing to you but another bad memory."

"You'd have plenty of company." Robinson opened the door for me, allowing me to rejoin the crew outside as he commanded their attention. "Sorry to keep you guys waiting. Here's the deal. Adversary Naomi Bradleigh needs our help tearing apart an old military installation in the woods. She's in command, but will relay instructions through me."

Stepping forward, I let the men get a good look at me. "Thank you, Sheriff Robinson. Gentlemen, I'm Adversary Bradleigh. Our mission is an arms control operation at Fort Clarion, in the Old Fort Woods northeast of here."

They started looking at each other and muttering. Guess they had no clue. Drawing my sword got their attention. "Michael Brubaker will guide us there. Once we arrive, I will provide further instructions. Now, I want the adult volunteers from the Clarion Volunteers to step forward."

They complied, and saluted in so smart a fashion, I was honor-bound to return the gesture. "You will each be responsible for four of the younger volunteers. How many of you have implants?"

All twenty hands went up. That certainly simplifies matters. After I obtained their IP addresses and Robinson's, I connected them all to SRC. "Sheriff Robinson will relay my orders over secure relay chat. Ignore any order that did not first come from me." I glanced at a militiaman in the middle, whose nametag read 'Yoder.'. "Do you have a question, Mr. Yoder?"

"Ma'am, how should we relay your instructions to the volunteers you've tasked us with supervising? Should we also run our own secure relay chats while monitoring yours?"

"That's an excellent idea, but first ― is there anybody here who doesn't have an implant? Raise your hands if that's the case." Nobody fessed up to not being properly equipped. "Perfect. I will expect the adult volunteers to do as Mr. Yoder suggested. Any other questions?"

A smirking kid raised his hand. "Are you a vampire?"

Seriously? Not that a show of anger would help; this schmuck's only trying to look cool. Do guys ever get tired of trying to prove their masculinity? "Were you hoping I'd sneak into your bedroom at night and enslave you with my kiss?" I delivered the question in the most stereotypically seductive tone I could manage while keeping a straight face, before going full ball-breaking drill instructor on him. "In case you've forgotten, I already own your ass for the duration of this job―assuming you've got the nerve to stick around."

The kid flushed, but stood his ground. Good. "Sorry, ma'am."

"Accepted. Now, does anybody have any questions germane to the mission?" Nobody did, which meant we could finally get down to business. We'd need two hours to get to Fort Clarion, which didn't leave much daylight for actual work in the middle of October. It would be dark by six, and it was already two. We'd be marching back in the dark. Dammit.

Before I could issue marching orders, Dr. Petersen ran up to us clutching a black bag. Pretty spry for an old guy. "Excellent, Adversary. I had hoped I wouldn't be too late. No doubt you'll want a physician around in case anybody gets hurt."

Actually, a physician would be handy. I can provide first aid, but a doctor capable of working in battlefield conditions if everything goes pear-shaped could save more lives than I might alone. Too bad it had to be Dr. Petersen. Instead of telling him to go make some house calls, I decided to keep him in sight and added his IP address to my secure relay chat. "Thank you, doctor. Please stay with me and Mr. Brubaker."

"Of course, Adversary." Not that I liked having him close to me, either, but if I ordered him to march in front of me, I'd tell everybody I don't trust their family doctor. Just gotta keep giving him rope and hope he doesn't take the lot of us with him when he hangs himself.

｢Brubaker, Petersen, and I will take point. Sheriff Robinson, please take the rear and give a shout if anybody falls behind.｣ After issuing the general order, I texted Mike as we began marching. He kept up easily with my stride. ｢Can you lead us directly to Fort Clarion, and quickly?｣

Mike nodded. ｢No problem. Follow me.｣

We did. The militia and youth volunteers kept up an excellent pace, and we reached the Fort Clarion perimeter in a little over an hour. While we marched, I got everybody's names and assured them they'd be in good hands under me.

｢So, how do we get in, Adversary?｣ One of the militia volunteers, Schmidt, stared at the tangle of vines and foliage that had so choked the gates, that entry seemed all but impossible.

I yanked the machete from his belt and started slashing at the creepers, ripping away what I had cut loose and throwing it over my shoulder. After several minutes' work, I revealed a few links of rusted chain. ｢Sheriff, have the men take turns at this, two at a time. Give each other plenty of room so nobody gets hurt.｣

We had the gates cleared within minutes. All that kept us out now was a rusted chain bound by a corroded padlock. I turned to the beefy kid with the sledgehammer. ｢Zimmer, you're up. Think you can break the lock?｣

｢I might be better off attacking the chain, ma'am. Can you pull it tight for me?｣ Zimmer rolled his shoulders, hefted his sledge, and brought it down with a grunt. Ten kilos of blunt steel whistled past me and tore through the chain as if it were taffeta. I pulled it free, and managed to push the gate half a meter inward.

｢Hinges are probably rusted to hell and back, ma'am. Gimme a minute.｣ Zimmer shattered the lower hinge first, then jumped skyward to reach the other as if slam-dunking a basketball. Showoff.

Volunteers rushed forward to lift the gate out of the way, leaning it against the fence. Fort Clarion was now open. Who would greet us inside?

---

### This Week's Theme Song

"Break On Through" by The Doors

{% youtube CqLdDKy2XUQ %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
