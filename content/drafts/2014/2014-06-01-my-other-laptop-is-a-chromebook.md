---
id: 682
title: My Other Laptop is a Chromebook
date: 2014-06-01T16:59:36+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=682
permalink: /2014/06/my-other-laptop-is-a-chromebook/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1N0l3KB
bitly_link_twitter:
  - http://bit.ly/1N0l3KB
bitly_link_facebook:
  - http://bit.ly/1N0l3KB
bitly_link_linkedIn:
  - http://bit.ly/1N0l3KB
sw_cache_timestamp:
  - 401653
bitly_link_tumblr:
  - http://bit.ly/1N0l3KB
bitly_link_reddit:
  - http://bit.ly/1N0l3KB
bitly_link_stumbleupon:
  - http://bit.ly/1N0l3KB
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - acer
  - c720
  - chromebook
  - chrubuntu
  - customization
  - dual boot
  - Linux
  - writing
---
I never thought I'd find a Chromebook useful. That's probably the manner in which a great many Chromebook conversion stories start, especially those written by professional tech bloggers. Nobody's paying me to write yet another "How I Learned to Stop Worrying and Love the Cloud" testimonial, so I'll tell you a more interesting tale. I'll tell you how I made my Chromebook a useful writing tool.

## What's So Bad About the Chromebook?

Absolutely nothing. I have no objection to the Chromebook on its own merits. They're low-price, low-spec computers pre-installed with Google's ChromeOS, which is a Linux-based environment that does just enough to provide a graphical user interface and run the Chrome browser.

The hardware is all solid-state. It's all optimized to provide just enough processing power while running cooler than a conventional laptop. As a result, you can get about eight hours of use out of a new Chromebook on battery power with a full charge.

You're supposed to be able to do almost everything you'd normally would do with a computer inside Chrome, and trust Google to store your data. They even give you 100GB of cloud storage for free if you buy one.

## Why Not Just Use a Chromebook?

Despite the advantages I outlined above, I remained skeptical about using a Chromebook myself. I have my reasons, but I'll keep it short.

I'm a longtime Unix fan who learned on a SPARCstation in college back in 1998. I started using Linux in 1999. I like having full control over _my_ computer. I don't want a computer whose functionality is limited to what some corporation thinks I need.

I'm a writer who finds it hard to concentrate on writing when I'm using a machine connected to the network. Instead of depending on self-discipline or getting evaluated for ADHD, I prefer to just turn off my machine's wifi adapter.

I'm a long-haired metalhead with a multi-gigabyte collection of legally acquired music larger than the 20GB Google lets people upload to Google Music. Listening to music from the cloud while writing without wifi requires the use of a second device.

## My Hardware, My Rules

I remained skeptical of the Chromebook for the reasons described above. It didn't occur to me until recently to consider modifying a Chromebook by replacing the default ChromeOS operating software with one better suited to my requirements.

I reconsidered my stance after growing dissatisfied with my two-year-old System76 Performance Pangolin laptop as a writing machine. There's nothing wrong with the hardware itself; it performs well with the current version of Ubuntu. I wrote, revised, and published _Without Bloodshed_ with it.

My problem was that it was too easy to find something to do with that machine other than writing, but I still needed it for all the stuff I do aside from writing. I decided my best bet was to get a cheap little laptop, install Linux, and make a badass little novelist's laptop out of it. <a title="BBC - Game of Thrones author George RR Martin: 'Why I still use DOS'" href="http://www.bbc.com/news/technology-27407502" target="_blank">It's more portable than George R. R. Martin's DOS box, and doesn't require obsolete software.</a>

The hard part was finding a cheap little laptop. I considered buying a used Dell or a used Lenovo, but the best prices I could find were around $400, and I could buy a Chromebook at Costco for that kind of money. So, I thought, "Why not find out if Ubuntu runs on Chromebooks?"

## It's Alive. IT'S ALIVE!

You're probably not interested in all the nerdy shenanigans involved in getting Ubuntu running on my Chromebook. Instead, here's a photo of my Chromebook running Ubuntu next to my Sytem76 Pangolin laptop. Virgil helped me with the installation.<figure id="attachment_684" style="width: 720px" class="wp-caption aligncenter">

[<img class="wp-image-684 size-large" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/06/20140524_121644-1024x768.jpg?resize=720%2C540" alt="Virgil's helping with my Chromebook. Isn't he a good kitty?" data-recalc-dims="1" />](http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/06/20140524_121644.jpg)<figcaption class="wp-caption-text">Virgil's helping with my Chromebook. Isn't he a good kitty?</figcaption></figure> 

The Pangolin's my primary laptop, and I use the Chromebook as a dedicated writing machine. I've taken to using Emacs for writing, and the Chromebook has a "music player daemon" that will play selected songs for me without distracting me. I sync files between the two using InSync and Google Drive instead of Dropbox.

Best of all, my wife thinks it's a cute little machine instead of thinking I've gone batshit crazy for wanting a laptop that does nothing but run Emacs and a music player daemon. Unfortunately for her, she tends to hit the power button when she wants the backspace key.

Oh, and why Emacs? I already know the basics. It took me about twenty minutes to get it customized the way I like it. I can run the editor full-screen, and make the editor's "mode line" show the current time and battery status.<figure id="attachment_685" style="width: 720px" class="wp-caption aligncenter">

[<img class="wp-image-685 size-large" src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/06/Screenshot-from-2014-06-01-125414-1024x575.png?resize=720%2C404" alt="Emacs 24 on a Chromebook running Ubuntu" data-recalc-dims="1" />](http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/06/Screenshot-from-2014-06-01-125414.png)<figcaption class="wp-caption-text">Emacs 24 on a Chromebook running Ubuntu</figcaption></figure> 

I've already written a new Starbreaker short called "Limited Liability" using this Chromebook, as well as the second chapter of _Silent Clarion_. I even wrote this post on it.

## Linux on Chromebook Resources

Rather than re-hash the recipes I followed to get Linux working on my Chromebook, I'm just going to link to the ones I used and some alternatives. Have fun, and tell me about your Chromebooking adventures in the comments or on social media using the hashtag <a title="Chromebooking" href="https://twitter.com/search?q=chromebooking&src=typd" target="_blank">#chromebooking</a>.

  * <a title="ChrUbuntu for New Chromebooks: Now with more Ubuntu" href="http://chromeos-cr48.blogspot.com/2013/10/chrubuntu-for-new-chromebooks-now-with.html" target="_blank">Installing Linux on an Acer C720 using Chrubuntu</a>
  * <a title="ChrUbuntu 12.04. Now with double the bits!" href="http://chromeos-cr48.blogspot.com/2012/04/chrubuntu-1204-now-with-double-bits.html" target="_blank">Installing Linux on older Chromebooks using Chrubuntu</a>
  * <a title="HOWTO: Bodhi Linux on the Acer C720 Chromebook" href="http://jeffhoogland.blogspot.com/2014/01/howto-bodhi-linux-on-acer-c720.html" target="_blank">Installing Bodhi Linux on an Acer C720 Chromebook</a>
  * <a title="List of fixes for Xubuntu 13.10 on the Acer C720" href="http://redd.it/1rsxkd" target="_blank">A helpful Reddit thread full of Chrubuntu fixes/tweaks</a>
  * <a title="CrunchBang Linux on the Acer C720 Chromebook" href="https://github.com/liangcj/AcerC720CrunchBang" target="_blank">Installing Crunchbang Linux on an Acer C720 Chromebook</a>