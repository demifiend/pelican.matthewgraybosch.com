---
title: Quickie Interview with Tony Healey
categories:
  - Project News
tags:
  - interview
  - quickie
  - Tony Healey
format: link
---
British indie novelist <a title="Tony Healey" href="http://tonyhealey.com/" target="_blank">Tony Healey</a> just put up a quickie interview with me on his site at <a title="Interview with Tony Healey" href="http://www.tonyhealey.com/2014/05/17/5-minute-interview-matthew-graybosch/" target="_blank">http://www.tonyhealey.com/2014/05/17/5-minute-interview-matthew-graybosch/</a>. Check it out. 🙂
