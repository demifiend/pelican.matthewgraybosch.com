---
id: 2443
title: If you use WordPress, you should read this.
excerpt: This is an outdated post concerning Google Authorship and WordPress.
date: 2014-03-04T19:35:04+00:00
header:
  image: waiting_spider_web.jpg
  teaser: waiting_spider_web.jpg
category: Website
tags:
  - authorship
  - google
  - links
  - seo
  - WordPress
---
I was able to set up Google Authorship using this article and Yoast's WordPress SEO plugin. If you run your own WordPress blog, check this out.

<a title="How To Set Up Google Authorship" href="http://www.craigfifield.com/authorship-wordpress-seo.htm" target="_blank">http://www.craigfifield.com/authorship-wordpress-seo.htm</a>
