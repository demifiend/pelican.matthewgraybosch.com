---
title: "Coming Soon: Silent Clarion (Before Starbreaker)"
header:
    image: silent-clarion-final-cover.jpg
    teaser: silent-clarion-final-cover.jpg
categories:
  - Project News
excerpt: "I'm pleased to announce the serialization of Silent Clarion, a new adult sci-fi thriller set before the events of Starbreaker by Matthew Graybosch."
---

I know you've seen bits and pieces of _Silent Clarion_ before, a little side project featuring Naomi Bradleigh from _[Without Bloodshed](http://www.matthewgraybosch.com/without-bloodshed)_. I'm pleased to announce that _Silent Clarion_ is no longer a mere side project, but a prequel to **[Starbreaker](http://www.matthewgraybosch.com/starbreaker)** coming soon from [Curiosity Quills Press](http://curiosityquills.com/silent-clarion-serial-announcement/).

As the first **Before Starbreaker** novel, _Silent Clarion_ will run weekly on the [Curiosity Quills Press website](http://www.curiosityquills.com) beginning this Wednesday, 23 July 2014. Once the novel is complete, _Silent Clarion_ will be available in paperback and electronic formats from your favorite retailers.

## About _Silent Clarion_

_Silent Clarion_ is a new adult science fiction thriller set before the events of the **[Starbreaker](http://www.matthewgraybosch.com/starbreaker)** novels. Meet Naomi Bradleigh as an Adversary, seventeen years before _[Without Bloodshed](http://www.matthewgraybosch.com/without-bloodshed)_.

![Artwork by Polina Sapershteyn.](/images/silent-clarion-final-cover.jpg)

My curiosity might get me killed. I thought I needed a vacation from my duties as an Adversary in service to the Phoenix Society. After learning about unexplained disappearances in a little town called Clarion, I couldn't stop myself from checking it out.

Now I must protect a witness to two murders without any protection but my sword. I must identify a murderer who strikes from the shadows. I must expose secrets the Phoenix Society is hellbent on keeping buried.

I have no support but an ally I dare not trust. If I cannot break the silence hiding what happened in Clarion's past, I have no future. I must discover the truth about Project Harker. Failure is not an option.

## About the Author

Matthew Graybosch started writing after realizing he’d never be a world-class musician. He spent my entire adult life writing and rewriting **[Starbreaker](http://www.matthewgraybosch.com/starbreaker)**, an epic hybrid of science fiction and fantasy with romantic elements inspired by classic heavy metal by Judas Priest, the Blue Öyster Cult, Iron Maiden, and Queensrÿche. _Silent Clarion_ is his second novel, unless he manages to finish _The Blackened Phoenix_ first.

In addition to his work as an author, he is also a self-taught software developer. He lives in Central Pennsylvania with his wife and two cats who act like dogs. You can circle him on [Google+](http://plus.google.com/+MatthewGraybosch), like him on [Facebook](https://www.facebook.com/matthewgrayboschnovelist), and follow him on [Twitter](https://twitter.com/MGraybosch) and [Goodreads](https://www.goodreads.com/author/show/7144002.Matthew_Graybosch).
