---
id: 71
title: 'Let&#039;s Get Personal. How Authors Should Open Up Online.'
date: 2014-03-05T11:22:42+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=71
permalink: /2014/03/lets-get-personal-how-authors-should-open-up-online/
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapWP:
  - 's:159:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:2:"81";s:5:"pDate";s:19:"2014-07-13 10:53:02";}}";'
snapLJ:
  - 's:206:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0lj";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:48:"http://matthewgraybosc.livejournal.com/6537.html";s:5:"pDate";s:19:"2014-07-14 19:15:54";}}";'
snapGP:
  - 's:388:"a:2:{i:3;a:5:{s:12:"rpstPostIncl";s:7:"nxsi3gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"114546293185560266851/posts/AHGxKWDgqSy";s:5:"pDate";s:19:"2014-07-15 19:59:19";}i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"103251633033550231172/posts/heQHfPW5JAR";s:5:"pDate";s:19:"2014-07-14 19:57:41";}}";'
sw_cache_timestamp:
  - 401563
bitly_link_linkedIn:
  - http://bit.ly/1PiUrXP
bitly_link:
  - http://bit.ly/1PiUrXP
bitly_link_twitter:
  - http://bit.ly/1PiUrXP
bitly_link_facebook:
  - http://bit.ly/1PiUrXP
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - being yourself
  - opening up
  - sharing
format: link
---
I'm not the only one trying to open up instead of hiding behind a limited persona. YA novelist Diana Urban is working to do the same, without the advantages of male privilege.

<http://diana-urban.com/how-authors-should-open-up-online>