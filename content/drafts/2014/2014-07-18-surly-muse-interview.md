---
title: Surly Muse Interview
categories:
  - Project News
tags:
  - craft
  - interview
  - life
  - silent clarion
  - starbreaker
  - Surly Muse
  - the blackened phoenix
  - without bloodshed
  - work
  - writers
---
[Daniel Swenson at Surly Muse](http://surlymuse.com/), author of _[Orison](http://surlymuse.com/orison-release-lessons-first-novels/)_, was [kind enough to interview me for his blog](http://surlymuse.com/surly-questions-matthew-graybosch/). Here are but two of the questions I answered for him while also teasing _The Blackened Phoenix_ and my upcoming Curiosity Quills serial, _[Silent Clarion](http://curiosityquills.com/silent-clarion-serial-announcement/)_. You can read the rest of the interview on Surly Muse.

## When do you know a book is done?

I know a book is done when not only am I thoroughly sick of it, but so is my wife. Trust me, Ragnarok could come and go, and I could _still_ find aspects of _[Without Bloodshed](http://www.matthewgraybosch.com/without-bloodshed/)_ that could use improvement.

Maybe I have too many viewpoint characters. Maybe there's still some wooden dialogue. Maybe I didn't explain something as well as I could. Maybe I over-explained something else, and left too little to the reader's imagination. It's always something, but if I don't draw a line and say, "Fuck it. I'm done.", I'll never move on to the next book.

## What's the best writing advice you ever received?

"Don't quit your day job." It's also the worst writing advice I've ever received, but more on that in a minute.

Having a day job allowed me to focus on writing for myself, without _any_ concern whatsoever for marketability. Readers want boy wizards because _Harry Potter and the Magical McGuffin_ is hot? Don't care. Readers want soulful teenage vampires because _Twilight_ sells like hotcakes? Not my problem, Jack.

I don't have to chase trends to pay the bills. Instead, I can focus on my craft. With **[Starbreaker](http://www.matthewgraybosch.com/starbreaker/)**, I can take a shot at starting a _new_ trend.

The downside is that I don't get to have a "writing day" without cutting into my weekends. Instead, I have to steal what time I can for writing while also spending at least eight hours a day making somebody else richer in exchange for wages. I work at least two full-time jobs. Three, if you count being a halfway-decent husband.

If I had kids instead of cats, I'd be utterly screwed.

## Want More?

[Read the rest of my Surly Muse interview at http://surlymuse.com/surly-questions-matthew-graybosch/](http://surlymuse.com/surly-questions-matthew-graybosch/)
