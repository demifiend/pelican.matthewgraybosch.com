---
id: 813
title: 'Saturday Scenes &#8211; 28 June 2014'
excerpt: "I'm not up to writing something original this week for **Saturday Scenes**, through I've been busy with _Silent Clarion_."
date: 2014-06-28T11:37:17+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - '#SaturdayScenes'
  - Even Gods May Stand Defenseless
  - morgan stormrider
  - Nakajima Chihiro
  - without bloodshed
---
I think I'm coming down with a summer cold. So, here's an excerpt from Chapter 3 of _Without Bloodshed_, where I first introduce protagonist Morgan Stormrider from his own viewpoint. This the first scene of Chapter Three, Even Gods May Stand Defenseless. In the meantime, I'm going to go curl up and watch _Death Note_.

---

Morgan Stormrider stood before the receptionist's desk at the center of the steel and granite lobby of the Nakajima Armaments Company's Tokyo office, and waited to be acknowledged. He ignored the dull whole-body ache induced by the multiple G-force accelerations of his suborbital flight from London to Tokyo. While waiting, he speculated on what business might prove urgent enough for the company's owner, Nakajima Chihiro, to arrange a flight for him instead of awaiting his arrival via transoceanic maglev. Whatever the reason, he doubted the matter was so urgent he could afford to arrive without a gift, however small. The larger of the two pastel-wrapped boxes of chocolates from Maia Chocolaterie in the Ginza district would prove sufficient to satisfy the demands of courtesy.

"Adversary Stormrider?" The receptionist glanced up from her terminal and adjusted her wire-rimmed spectacles as an amber-eyed woman in a black dress stepped out of the elevator leading to Nakajima's office. She addressed him in English as she left her desk. "Ms. Nakajima awaits you in her office. Shall I escort you?"

Morgan remembered the way, but also that escorting guests was part of Ms. Yamagishi's responsibilities as the receptionist; without her cooperation, nobody met Nakajima after business hours. He bowed, offered the smaller box, and addressed her in Japanese. "Thank you for allowing me to meet Ms. Nakajima so late at night, Ms. Yamagishi. Please accept this gift."

Ms. Yamagishi bowed as she accepted the box, and placed it aside before resuming in Japanese. "Thank you, Adversary. Please follow me." She led him to the elevator, and pressed the call button for him. Her slight nod indicated he should enter first.

Glass cases lined the walls of the hallway on the top floor as Ms. Yamagishi led him from the elevator. They held production models of Nakajima Armaments' current offerings: swords, daggers, and knives; staves of various lengths; rifles, shotguns, revolvers, and pistols; and their latest functional fashions: outerwear and footwear nobody would believe was armored until the wearer emerged all but unscathed from a duel which should have been their death. Few customers merited the privilege of viewing the impromptu museum, and Morgan regretted his inability to justify taking time to appreciate Nakajima's work. Rather than waste his guide's time examining each case, Morgan matched her brisk pace and followed her into Nakajima's office.

He stood silent behind the receptionist as she waited for Nakajima to acknowledge her, thus confirming a hierarchy. The guest waits for the receptionist. The receptionist, in turn, waits for her employer. Nakajima glanced up from her terminal, just as Ms. Yamagishi did a few minutes earlier, stood, and gave the slightest of bows before addressing her subordinate in Japanese. "Thank you for bringing Adversary Stormrider. You may leave for the evening if you wish. I'll show him out when we are finished."

Ms. Yamagishi's bow was deeper than her employer's, further indicating Nakajima's social superiority to her. "Thank you, Ms. Nakajima. I will be back tomorrow at the usual time." She bowed to Morgan before leaving, showing the same deference she offered her boss.

Nakajima turned towards the windows, staring into the Tokyo neon for a minute before glancing over her shoulder at Morgan. "Thank you for coming, Adversary, and for remembering me and my family at Winter Solstice. I apologize for insisting upon a suborbital flight, but I needed to speak with you as soon as possible."

Morgan presented his gift with a deeper bow than he offered Ms. Yamagishi. "The matter must be of dire importance for you to insist on a personal meeting at your own expense, Ms. Nakajima."

She accepted the gift with a bow acknowledging Morgan as an equal, and put it aside. "You embarrassed my receptionist. She keeps forgetting you're fluent."

Yamagishi must have used secure talk. "Please offer her my apologies tomorrow. I intended no discomfort."

Nakajima nodded, and turned from Morgan to prepare tea. He considered checking in with Astarte for a moment before deciding it would be rude, especially if his AI assistant had business which required his full attention for more than a minute. Instead, he turned his attention to Nakajima's reasons for requesting his presence in Tokyo. She means to ask a favor of me. She either needs a hell of a lot of money, or is going to ask me to do something which might cause a great deal of trouble.

She waited for Morgan to taste the tea she prepared before speaking. "Ms. Yamagishi's embarrassment reminded me. We tend to accommodate foreign visitors, rather than expect them to learn our language and observe our etiquette. But you accept no accommodation."

"I often visit Japan while touring with Crowley's Thoth, Ms. Nakajima. While the Empress' edict against foreigners prevents us from playing anywhere in the country but Tokyo, our Japanese fans are very accommodating hosts. As a guest in Japan, I feel obligated to learn the language and customs."

"Are you self-taught, as you are with the sword?"

"A friend of mine, Naomi Bradleigh, also spent a fair amount of time in Tokyo and became fluent herself. She helped me learn."

"I remember Ms. Bradleigh." Nakajima smiled as she sipped her tea. "Did you know she owns the last sword my mother made before she retired to fight her cancer? It's a beautiful Italianate side-sword."

"She never mentioned her sword's history."

"And you're too much of a gentleman to ask a lady about her past. Such discretion is not only admirable, but a quality in which I find myself in dire need."

Morgan sipped his tea in silence. In any other city, and with most other people, he would find a courteous way to direct the conversation to business. With his superiors in the Phoenix Society, or his fellow Adversaries, he would tell them to stop stroking his ego. In Tokyo, an indirect approach was indicated, and the green tea Nakajima prepared was worth savoring after his flight. "Ms. Nakajima, you were kind enough to handle my travel arrangements. May I order dinner for the two of us? We need not leave the office."
