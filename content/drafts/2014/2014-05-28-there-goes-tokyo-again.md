---
id: 661
title: There Goes Tokyo Again
date: 2014-05-28T00:16:44+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=661
permalink: /2014/05/there-goes-tokyo-again/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1LnPWVS
bitly_link_twitter:
  - http://bit.ly/1LnPWVS
bitly_link_facebook:
  - http://bit.ly/1LnPWVS
bitly_link_linkedIn:
  - http://bit.ly/1LnPWVS
sw_cache_timestamp:
  - 401685
bitly_link_tumblr:
  - http://bit.ly/1LnPWVS
bitly_link_reddit:
  - http://bit.ly/1LnPWVS
bitly_link_stumbleupon:
  - http://bit.ly/1LnPWVS
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - Demi-Fiend
  - Nocturne
  - post-apocalypse
  - PSN
  - Re-release
  - RPG
  - shin megami tensei
  - Tokyo
---
Never mind Godzilla. Here's the Demi-Fiend. If you want to see Tokyo go straight to Hell, I've got three words for you: **Shin Megami Tensei**. Actually, make that four, because I can't believe I missed Atlus' re-issue of <a title="ATLUS - Shin Megami Tensei: Nocturne" href="http://www.atlus.com/smt/main.html" target="_blank"><em>Shin Megami Tensei: Nocturne</em></a> on the PlayStation Network earlier this month.<figure style="width: 1199px" class="wp-caption aligncenter">

[<img src="http://i2.wp.com/www.gpgr.net/wp-content/uploads/2013/06/SMT_Nocturne.jpg?resize=840%2C840" alt="Artwork by Kazuma Kaneko" data-recalc-dims="1" />](http://www.gpgr.net/ps2/classic-rpg-corner-shin-megami-tensei-nocturne/)<figcaption class="wp-caption-text">Artwork by Kazuma Kaneko</figcaption></figure> 

Here's the deal. Did you have a PS2 back in the day? Did you enjoy those _Persona_ games? How about the Raidou Kuzunoha _Devil Summoner_ games? Hell, what about _Digital Devil Saga: Avatar Tuner_? Well, before Atlus could do any of those, they did Shin Megami Tensei III (Nocturne) &#8212; and reused the engine. 🙂

Now, ten years after its original release, the beast is back. A ritual called The Conception destroys the world as we know it, and the ruins of Tokyo become a Vortex World wrapped around the light of creation, Kagutsuchi. Kagutsuchi will empower the one who stands above all others to create a new world, but before that can happen, the few remaining humans must formulate a Reason &#8212; an ideal that will guide creation.

As the <a href="http://megamitensei.wikia.com/wiki/Demi-fiend" target="_blank">Demi-Fiend</a> empowered through Lucifer's intervention, you're granted a choice: support another survivor's Reason, work to restore the pre-existing world, abort creation &#8212; or rebel against the Great Will that permits the cycle of death and rebirth throughout existence.

But let's not get ahead of ourselves. Immediately after the Conception and Lucifer's intervention in our silent protagonist's favor, you're given full control of the character in an abandoned hospital in Shinjuku. You're weak, and alone, and must perforce seek allies to survive.

As a demonic being with a human heart and intellect, you can communicate with other demons without the aid of the Devil Summoning Program that shows up in other _Shin Megami Tensei_ games. You must cajole, seduce, persuade, and outright bribe demons before they'll join you.

The art and environments are bleak, minimalistic, and frequently beautiful as a result. Each demon has a distinctive style courtesy of Kazuma Kaneko's artwork, and the soundtrack by Shoji Meguro suits the game's bleak graphics and sombre, mysterious story. There's no voice acting to distract from the proceedings, which I consider a plus, though younger gamers who grew up on voice-acted games may find the lack jarring.

_Shin Megami Tensei: Nocturne_ is not recommended for young players. Though the violence is minimal, and sexual content is limited to partial nudity in demon designs (like the Bondage Angel), the occult imagery and themes may prove problematic for some players or their parents.