---
id: 170
title: Become a Lunch Break Novelist
date: 2014-03-10T07:00:57+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=170
permalink: /2014/03/become-lunch-break-novelist/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1LDPidb
bitly_link_twitter:
  - http://bit.ly/1LDPidb
bitly_link_facebook:
  - http://bit.ly/1LDPidb
bitly_link_linkedIn:
  - http://bit.ly/1LDPidb
sw_cache_timestamp:
  - 401616
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - eating at work
  - finding time
  - getting away
  - lunch break novelist
  - work/life balance
---
I'm a lunch break novelist. It's how I wrote my first novel, <a title="Without Bloodshed" href="http://www.matthewgraybosch.com/without-bloodshed/" target="_blank"><em>Without Bloodshed:</em></a> one lunch break at a time. I did the same with the original draft of <a title="Starbreaker" href="http://www.matthewgraybosch.com/starbreaker/" target="_blank"><em>Starbreaker</em></a>, too. You too can write a novel on your lunch break. I'm going to tell you how while piercing the common excuses.

## But I Have to Eat at My Desk, and Work

I know times are tough, and working people are under pressure to put in more time on the job and do more while they're there. Sometimes that pressure leads you to eat lunch at your desk while you work. You owe it to yourself to fight back. You deserve a break from your work. You deserve some time to yourself during the day. <a title="The Atlantic: Only Work Through Lunch if it's Your Choice" href="http://www.theatlantic.com/health/archive/2013/10/study-only-work-through-lunch-if-its-your-choice/280208/" target="_blank">You'll feel less tired if you don't work at your desk because it's expected of you.</a>

Remember that you don't _have_ to do anything you don't want to do. Your choices might come with consequences, but you are still free to do what _you_ want to do if you're willing to pay the price demanded.

It might very well be that nobody cares if you bugger off for an hour. Your obligation to work through lunch may prove self-imposed.

Get out of the office if you think you can't take a break to write while eating at your desk. If your office building has a cafeteria, that will do. Otherwise, get out of the building altogether and find a pizza parlor with seating, or a Starbucks, or a Subway. Hell, even a McDonalds will do if you can't find anything better nearby.

Regardless of where you go, the idea is to go somewhere you can get to in 5-10 minutes. If you can bring your own food, great. If not, try to buy something that isn't too expensive and isn't likely to result in your doctor having kittens. The ideal venue that isn't the office break room is somewhere where you can just buy a bottle of water and sit down for 45 minutes, like Starbucks.

## But 45 Minutes Isn't Enough

You might think 45 minutes isn't enough to get anywhere with your novel. I know better. Remember that I wrote two novels on my lunch breaks, one of which I published last November. The key is to _focus_.

If you're like epic fantasy <a title="R. J. Blain" href="http://www.rjblain.com/" target="_blank">novelist R.J. Blain</a> and can write with pen and paper, bring a notebook and some pens and leave all the electronics at the office. Otherwise, bring a laptop of your own and turn off its wifi if your lunchtime hangout spot offers free wireless access.

If you can get your focus going in 10 to 20 minutes, that gives you 25-35 minutes with which to do some writing before you have to go back to the office.

## I Only Managed 500 Words

Some writers say that word counts don't matter, especially if you use them as an excuse to guilt-trip yourself. I disagree. It's handy to set goals, especially if you can consistently achieve them. 500 words a day doesn't seem like much, especially compared to people who manage thousands of words a day,

You should remember that many of those who manage thousands of words a day _are full-time writers_. They're not like you. They only have one job. Most of them are _lazy_. You don't think so? Just ask the Hedleys.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

500 words is fine. If you manage 500 words a day, every day, then you'll write a 100,000 word novel in 200 days. And you can speed this up by writing _after_ work and after you've take care of any other responsibilities you can't ignore or delegate.

If you need somebody to keep you on track, find an online writing group like the <a title="Writers' Discussion Group on Google+" href="https://plus.google.com/communities/106134988944938026164" target="_blank">Google+ Writers' Discussion Group</a>. You can also use sites like <a title="750words.com" href="http://750words.com/" target="_blank">750words</a> to keep yourself honest and set daily targets. Read articles like <a title="Fantasy Faction: 10 Ways to Up Your Word Count" href="http://fantasy-faction.com/2014/10-ways-to-up-your-word-count" target="_blank">"10 Ways to Up Your Word Count" on Fantasy Faction</a> for additional productivity tips, and apply them.

## But My First Draft Sucks

Everybody's first draft sucks. The only difference is in the degree to which your first draft sucks. Yours might suck like an Electrolux, like a Michael Bay action movie, or like a supermassive black hole in the middle of a spiral galaxy. It doesn't matter. Your first draft is going to suck, no matter what. Accept it.

Once you've finished your first draft, don't worry about how much it sucks. Put the draft away and go celebrate. Get drunk. Eat foods you normally deny yourself because they make you fat. Have loud nasty sex with someone you care about (or by yourself). Take a vacation. Be happy. Be proud. You _earned_ it by finishing a novel.

Give it a month, and then start revising. If you can spare the expense, hiring a competent independent developmental editor may be worth your while, especially if you want to publish your novel yourself instead of trying to persuade some corporate publisher to publish your book for you in exchange for a royalty of less than 10% and an advance best expressed as an <a title="Math is Fun: Irrational Numbers" href="http://www.mathsisfun.com/irrational-numbers.html" target="_blank">irrational number</a> between fuck-all and jack shit.

## I'm Ready to Publish. Now What?

First, congratulations on finishing your first novel and getting it ready to publish. You've worked hard to get this far, but it only gets worse from here. You see, not only do you have to promote your novel (which is beyond this post's scope), but now it's time to write _another_ novel.

You should find your second novel easier than the first, since you've developed work habits you can continue to use. Just don't fall prey to imposter syndrome. No matter what that hateful little voice in the back of you head says, you _are_ a writer.

So get your ass out of that office and go write, my fellow lunch break novelist.

(Photo Credit: <a title="Darryl Mouzone on Google+" href="https://plus.google.com/+DarrylStormofDeathShieldcrusher/about" target="_blank">Darryl Mouzone</a>)