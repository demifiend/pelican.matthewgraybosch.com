---
title: "Screw Digital Sharecropping. I'm Getting a POSSE."
excerpt: "What I create belongs to *me*, not Google or Facebook. It should be on my website first, and then syndicated."
header:
  image: Greene_Co_Ga1941_Delano.jpg
  teaser: Greene_Co_Ga1941_Delano.jpg
  caption: "Photo by [Jack Delano](https://en.wikipedia.org/wiki/Sharecropping#/media/File:Greene_Co_Ga1941_Delano.jpg) for the FSA"
categories:
  - Social Media
tags:
  - backfeed
  - digital sharecropping
  - IndieWeb
  - IndieWebCamp
  - posse
  - social media
  - social network
  - syndication
  - ownership
  - walled gardens
  - silo
  - pay the goddamn writers
---
I'm going to level with you guys. I dislike social media and the modern Web. I think both are too subject to corporate control, and too dependent on silos and walled gardens. I loathe [Facebook](https://www.facebook.com/matthewgrayboschnovelist), think [Twitter ](https://twitter.com/MGraybosch) is nothing but the men's room wall of the internet, consider [LinkedIn](https://www.linkedin.com/in/matthew-graybosch-29a9a4116) worse than useless, don't really get [Tumblr](http://starbreakernovelist.tumblr.com/), and have come to find [Google+](https://plus.google.com/+MatthewGraybosch/) a disappointment. I am convinced that [digital sharecropping is for suckers](http://blog.codinghorror.com/are-you-a-digital-sharecropper/). I've decided, therefore, to get myself a POSSE.

Don't worry. I'm not going to ride around on a Harley while brandishing a Kalashnikov and terrorizing the populace. To start, my wife would kill me if I got a motorcycle. Also, she'd freak out if I had an AK in the house. She's an Aussie, and not fond of firearms. But first, let's talk about what's wrong with social media in general. I can summarize it in two words: **digital sharecropping.**

As far as Google, Facebook, Reddit, and other internet corporations are concerned, *this* is what you are if you use their websites.

{% include base_path %}
![Sharecroppers]({{ base_path }}/images/Greene_Co_Ga1941_Delano.jpg)

You don't own what you create on their websites, and you sure as shit aren't guaranteed a profit from the value you contribute.

## What is Digital Sharecropping?

I think [Nicholas Carr]("http://www.nicholascarr.com/) was the first to use the term, but can't prove it. He used the term in a [2006 entry on his Rough Type](http://www.roughtype.com/?p=634) blog, and had the following to say:

> What’s being concentrated, in other words, is not content but the economic value of content. MySpace, Facebook, and many other businesses have realized that they can give away the tools of production but maintain ownership over the resulting products. One of the fundamental economic characteristics of Web 2.0 is the distribution of production into the hands of the many and the concentration of the economic rewards into the hands of the few. **It’s a sharecropping system, but the sharecroppers are generally happy because their interest lies in self-expression or socializing, not in making money, and, besides, the economic value of each of their individual contributions is trivial.** It’s only by aggregating those contributions on a massive scale – on a web scale – that the business becomes lucrative. To put it a different way, the sharecroppers operate happily in an attention economy while their overseers operate happily in a cash economy. In this view, the attention economy does not operate separately from the cash economy; it’s simply a means of creating cheap inputs for the cash economy.

The bolded section in the quote above is my emphasis. Here's how social media works: it's a popularity contest in which you get more followers and kudos if you contribute more content that gets more attention. You get praise, but praise doesn't pay the rent. The actual money your work generates goes to whoever owns the social network you use, and you don't get a single red cent.

## What's Wrong With Digital Sharecropping?

Who owns what you post on the Internet? If you can't say, "I own my creations", then you're getting screwed. That's the Devil's honest truth. If anybody should be making money from the content you create, even if it's just ad revenue, you should be first in line. Not Facebook. Not Google. **You.**

Don't try to tell me that they're doing you a favor by providing the "means of production". _You_ are the means of production. Social media is just the means of distribution. Without you to create content, Facebook, Google, etc. has nothing to distribute and therefore nothing to sell.

## But How Will I Get Exposure?

Here's the dilemma we face, whether we just want to have fun online or hope to make a living from the creative work we do online. Most of the people live in the social media silos created by Facebook, Google, Twitter, etc. If we refuse to deal with the silo people, we lose any hope of reaching an audience. We own our content, but have no means of distribution. This is the problem [#IndieWebCamp](http://indiewebcamp.com/) set out to tackle.

## Joining the IndieWeb

The IndieWeb movement behind IndieWebCamp proposes a  new way. Rather than simply retreating into our own personal walled gardens, those involved in IndieWeb want to own their own content while still syndicating to the major social networks.

This principle is called [POSSE: Publish (on your) Own Site, Syndicate Elsewhere](http://indiewebcamp.com/POSSE). If your site is properly built, you can post on your site, syndicate outward to other sites, and then pull comments from other sites _back_ to your own site.

The tools required to make a website IndieWeb compatible are still a bit complicated. For the most part, IndieWeb participants are more concerned with design principles than with implementation at this point, though tools for making a self-hosted WordPress installation IndieWeb compatible are available as plugins. [You can find a list here.](http://indiewebcamp.com/WordPress)

## Why Does IndieWeb Matter?

As I wrote earlier: do you want to own your content, or do you want Google or Facebook or some other corporation to own it? If you're an author, everything you post online constitutes a portion of your body of work. Your work should belong to _you_, and nobody else.
