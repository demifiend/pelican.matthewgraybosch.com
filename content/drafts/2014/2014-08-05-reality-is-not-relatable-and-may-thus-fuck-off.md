---
id: 1123
title: Reality Is Not Relatable, and May Thus Fuck Off
date: 2014-08-05T22:05:35+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1123
permalink: /2014/08/reality-is-not-relatable-and-may-thus-fuck-off/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapPN:
  - 
snapSU:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snap_isAutoPosted:
  - 1
snapSC:
  - 
snapDI:
  - 
snapIP:
  - 
snapLJ:
  - 
snapDL:
  - 's:292:"a:1:{i:0;a:8:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"a0d9c03fda343ef04d742c3b65b70410";s:5:"pDate";s:19:"2014-08-06 02:06:49";}}";'
snapGP:
  - 
snapLI:
  - 
snapBG:
  - |
    s:332:"a:1:{i:0;a:7:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:8:"isPosted";s:1:"1";s:4:"pgID";s:76:"http://matthewgraybosch.blogspot.com/2014/08/reality-is-not-relatable_5.html";s:5:"pDate";s:19:"2014-08-06 02:07:05";}}";
snapWP:
  - 
bitly_link:
  - http://bit.ly/1PjLAoG
bitly_link_twitter:
  - http://bit.ly/1PjLAoG
bitly_link_facebook:
  - http://bit.ly/1PjLAoG
bitly_link_linkedIn:
  - http://bit.ly/1PjLAoG
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1PjLAoG
bitly_link_reddit:
  - http://bit.ly/1PjLAoG
bitly_link_stumbleupon:
  - http://bit.ly/1PjLAoG
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"9928aa4f1586";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:90:"https://medium.com/@MGraybosch/reality-is-not-relatable-and-may-thus-fuck-off-9928aa4f1586";}'
categories:
  - How Not to Write
tags:
  - art
  - fuck reality
  - realism
  - reality is overrated
  - relatability
  - romanticism
  - Shakespeare
  - tell reality to go fuck itself
---
So, Ira Glass of _This American Life_ made a bit of a flap with some recent tweets about his inability to relate to Shakespeare.

<blockquote class="twitter-tweet" lang="en">
  <p>
    Same thing with the great Mark Rylance shows this yr: fantastic acting, surprisingly funny, but Shakespeare is not relatable, unemotional.
  </p>
  
  <p>
    &mdash; Ira Glass (@iraglass) <a href="https://twitter.com/iraglass/statuses/493610573725057024">July 28, 2014</a>
  </p>
</blockquote>



Glass and others argue Shakespeare isn't relatable. I think reality isn't relatable, and neither is most realistic literary fiction. Reality, and realistic literary fiction, may thus fuck off and die.

I read an even-handed [Washington Post](http://www.washingtonpost.com/news/act-four/wp/2014/07/28/ira-glass-and-what-we-get-wrong-when-we-talk-about-shakespeare/) op-ed by [Alyssa Rosenberg](https://twitter.com/AlyssaRosenberg) about this, but that's just an op-ed piece. I wouldn't have bothered hitting "New Post" for that.

I don't think anybody actually cares that I don't think much of most of Shakespeare's plays. I hated reading them in high school. I didn't need the teacher to belabor the point that Romeo and Juliet were idiots for making such a production about their romance instead of just marrying the partners chosen for them while enjoying an affair on the down low.

However, I enjoyed seeing some of them performed live. Hell, a good film production also works. I don't need Patrick Stewart to be "relatable" in that Scottish play.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Nor do I need Helen Mirren to be "relatable" as Prospera.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

My requirements for enjoying a story are twofold. I need time enough to throw aside the shackles of reality, flex the demonic sinews of my imagination, and hoist skyward my disbelief long enough to enjoy the story. I also need a _reasonable_ expectation that my effort at getting into the story will pay off.

I avoid realistic literary fiction, such as the works of Jonathan Franzen and Jennifer Weiner, because I don't can't bring that expectation to such fiction. I'm not interesting in reading about real life. It's bad enough I have to live it.

According to a recent [Pacific Standard](http://www.psmag.com/navigation/books-and-culture/relatable-indeed-fictional-tales-moving-predict-87534/) article, some studies support my attitude. I'll just quote the salient point below:

> "Although forecasters predicted less intense emotional reactions when reading about a distant (fictional) event than when reading about a proximal (real) event, experiencers actually reported equally intense emotional reactions when they believed the story was fictional as when they believed it was real." 

If you aren't sure what this means for you as a writer, I'll break it down for you. Realism and verisimilitude are overrated. Tell an interesting story, avoid boring your reader, and everything else will fall into place.

#### Image Credit

Featured image by [MagaLennha@DeviantArt](http://magalennha.deviantart.com/art/Art-is-how-you-tell-reality-to-go-fuck-itself-353698792).