---
title: "Sorry, Marketers. There Is No Society."
excerpt: "Society, like God and the Market, is an imaginary concept. Stop reifying shit, people."
header:
  image: cat-headphones.jpg
  teaser: cat-headphones.jpg
  caption: "Pic unrelated. I just think it's cute."
categories:
  - Marketer Abuse
tags:
  - advertising
  - attention economy
  - marketing
  - mass market
  - niche
  - no such thing as society
---
Marketing "guru" Seth Godin waxes nostalgic for the "good old days" when everyone who mattered read the same newspapers, watched the same TV shows, listened to the same music, et cetera and ad fucking nauseum. He writes in [What Everyone Reads](http://sethgodin.typepad.com/seths_blog/2014/09/what-everyone-reads.html):

> Everyone used to read the morning paper because everyone did. Everyone like us, anyway. The people in our group, the informed ones. We all read the same paper.
>
> Everyone used to read the selection of the book of the month club, because everyone did.
>
> And everyone used to watch the same TV shows too. It was part of being not only informed, but in sync.

He worries that Society is falling apart. I'm sitting here with hot popcorn and a cold beer, enjoying the show. How often do you get to see an empire fall apart right in front of you? Not often, so you might as well enjoy it.

When the commoditized American mass culture finally dies, I will not praise it. I will bury it. And then I will piss on its grave. It's the only libation our hegemonic, sterile culture will ever deserve.

Of course, Godin doesn't share my perspective.

> Society without a cultural, intellectual core feels awfully different than the society that we're walking away from.

## What is Society, Anyway?

I don't think there's any such thing as Society. Not in the sense that the almost 350 million people living in the United States constitute anything resembling a cohesive society. (Why do you _think_ Washington is so thoroughly fucked?) If such a mass society ever existed, the Web killed it.

We didn't just _walk_ away from that old mass society Seth Godin laments. We _fled_ that society, riding hellbent for leather, because it is inimical to any reasonable conception of a good life.

Why should anybody want to live in a society where a small, exclusive, and all-but-untouchable elite decide not only our laws and the way our economy functions, but what books we'll read, what dramas we'll watch, what music we'll listen to, and what clothes we'll wear?

I could write a dystopian novel about such a world, and what it's like to grow up knowing that that world has no place for you, but nobody would recognize it as science fiction. They'd mistake it for literary fiction.

## Brave New Worlds

The cultural, intellectual core of the old American society was a black hole. That black hole is eating itself, spewing out the rhetorical equivalent of Hawking radiation all the while. The sooner it finally disappears, the better off we'll all be.

I'm not afraid of a world without gatekeepers, trendsetters, and tastemakers. Their continued existence won't help me reach an audience, because I write for a niche audience.

Instead of a single American society where everybody is as close to being the same as corporate American can manage, let a million American societies bloom. Make your own society. Let other people make their own societies. The points at which our societies intersect are the points at which we will revitalize our cultures.
