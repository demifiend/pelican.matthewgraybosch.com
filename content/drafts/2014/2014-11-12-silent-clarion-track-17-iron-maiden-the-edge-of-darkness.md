---
title: "*Silent Clarion*, Track 17: &quot;The Edge of Darkness&quot; by Iron Maiden"
excerpt: "Check out chapter 17 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Naomi got her orders like room service, 'cause everyone gets what they want."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Warning Mike about the need for operational security was easy, albeit a day late and a milligram short. Now was time to update Rosenbaum before somebody who doesn't have my best interests in mind clues him in. Good thing I got the introductions out of the way last night.

｢Hello again, Naomi. How's Clarion?｣Saul's reply was instantaneous, thanks to the implant-to-implant connection.

Implanted computers are standard issue for everybody on the Phoenix Society payroll, not to mention anybody who holds anything resembling a position of authority. They jack into your optic and auditory nerves to gather Witness Protocol data. Because of this, and their ability to run a wide variety of software targeted at POSIX systems, they're invaluable in the field.

Bloody good thing, too, because nobody will sell you a firearm unless you've got a working implant. But back to business before Saul thinks I've lost my connection.

｢I met a nice boy who wants to become an Adversary. He's got the drive, and he's pretty sharp.｣ That much was true: Mike Brubaker would probably be an asset to the IRD corps.

｢Send him my details and tell him to call me if he's serious about joining up. Pittsburgh will need its own chapter in a few years. Anything else?｣

｢I found something of interest while hiking in the woods northeast of Clarion. An old North American Commonwealth Army installation in unusually good condition. I think the commanding officer is still alive, and practicing medicine in town. He slipped me a note warning me away.｣

｢I see you're making friends on vacation. I thought you promised to behave yourself.｣ He continued before I could make a crack about how I never promised to be well behaved. ｢If you were composing an explanation, belay it.｣

The naval terminology brought a smile. ｢Aye, sir.｣

｢And belay the aye-ayes, Adversary. This isn't the Navy.｣

Saul got down to business. Good thing he couldn't see me smiling over secure talk. ｢I'll find a justification for letting you investigate further, but you're back on the job for the duration. I hope you brought your pins.｣

As a matter of fact, I did. It always pays to be prepared. Though I wouldn't be fully armed. ｢I brought my uniform, but no weapons other than my sword.｣

｢I'll take that into account. Goodnight, Naomi.｣

｢Goodnight.｣ But it wasn't. Sleep took its sweet time claiming me, despite my efforts to hasten its coming. Vigorous exercise in the cool night air, warm chamomile tea, and a steamy shower spent imagining a certain soldier's hands exploring my body left me in a state so common among Adversaries we have an acronym for it. I was TBW: tired but wired.

By four in the morning, I had had enough of lying in bed doing mindfulness meditation and hoping for a sleep that stood me up like a man too spineless to say no when I asked him for a date. It was too early for breakfast, but sitting in the beer garden with a book was a welcome change, and gave me an excuse to put myself together. A bit of fresh air would help me stay awake.

A motorcycle's rumble died off, and its rider vaulted over the fence instead of using the front gate like a reasonable person. Not that Edmund Cohen struck me as the reasonable sort. He sauntered over to my table, spun a chair around, and straddled it. "Well, Nims, you certainly clean up nicely."

"I should hope so." He should have seen me at that hotel bar in Manhattan, though I doubt my gown would prove suitable for kicking ass and taking names. "Saul Rosenbaum told me I could expect orders."

Edmund chuckled as he withdrew a miniature digital voice recorder from his coat and slid it across the table. "He said you wanted a mission, and for your sins, you're getting one. Need headphones?"

"Got some." I pulled a set of earbuds out of my pocket and plugged them into the device's output jack, carefully tucked the buds into my ears, and pressed play.

A voice I had never heard before greeted me. "Good morning, Adversary Bradleigh. Before we begin, please note the serial number of this communication. You may send it to Malkuth to verify this message's authenticity."

He rattled off a string of hexadecimal numbers I recognized as a cryptographic key. Using my implant, I stored the key for authentication, and listened to the rest. "Adversary Bradleigh, your mission is to muster Clarion's militia and lead it to Fort Clarion. Upon arrival at Fort Clarion, you are to gain entry by any means necessary, and search the premises. Compile a complete inventory of all weapons and equipment, and forward said inventory to Saul Rosenbaum at the New York chapter. Await a Phoenix Society ordnance disposal team, and ensure that no equipment leaves the base."

That seemed simple enough, like a textbook arms control job. Never had to do one myself, but I remembered the training drills from ACS. I let the tape continue playing, but heard nothing but the hiss of blank tape. Before I could press stop, the voice returned. "Naomi, the following is unofficial. Malkuth will not authenticate it, because he is unaware of this addendum. Fort Clarion isn't just an old Commonwealth Army installation. It was also the site of a series of human enhancement experiments codenamed 'Project Harker.' I cannot tell you more without tipping off my colleagues on the executive council. You will have to find the rest on your own, using evidence procured on site. Keep this to yourself, and wipe the device after listening."

Obeying the recording's final instruction, I returned the blank device to Edmund. "Who gave you these orders?"

He shook his head. "I can't tell you. Got orders of my own. I'll say this much: he won't tell you everything, but what he does tell you is true."

Could it be Cohen himself? The voice sounded a bit like him, but it's possible to fake such recordings. And what exactly is Project Harker?

Was the Commonwealth Army trying to make vampire soldiers? Is that yummy Sergeant Renfield somehow involved? After all, Harker and Renfield are both names from the Stoker novel. No sense dwelling on it, or asking Edmund. He wouldn't tell me anything. "All right. That should do. It's only an arms control job, after all. Should be so uneventful I'll regret finding the bloody place."

"That's the spirit." Edmund chuckled. "Now, do you need any equipment? I brought an AK and a M1911, but you'll have to assemble the Kalashnikov."

As tempting as the prospect of having some firepower sounded, I decided against it. "Thanks, but it would look better if I requisitioned gear from the militia armory. Some people in town know I'm an Adversary on vacation. If I suddenly show up at Town Hall to muster the militia with an AK, people might wonder if the vacation was an insertion cover."

"That's smart. I like it." Edmund rose, indicating that the conversation was over. "Be careful, Naomi. You'll have Witness Protocol running in ten minutes, but don't count on backup even if we do see you're in trouble."

No backup? No problem. I've got this. "Thanks for the warning. Sure you don't want to stay for breakfast? The bacon here is to die for."

"Better not." Edmund glanced at the street. "If I stick around any longer, I might find myself involved. That would be bad for both of us. I know too much, and can't safely reveal any of it."

If he knows so much, then was the masked voice on the recorder his? Or is he working for whoever made the recording? "I understand. Thanks, Eddie. Have a safe ride."

"You too, Nims." Nims, eh? So, him calling me that earlier wasn't a one-off. Seems he's been chatting with Malkuth.

Once Edmund was safely away, I checked the time: 6:31. Dammit. Town Hall won't be open until nine. Time enough for breakfast, but why eat alone? ｢Mike, you awake? Come to the Lonely Mountain. Breakfast's on me.｣

Ten minutes later, he arrived. "You look different with your hair up. Is this about the fort?"

"Yeah." If I had been thinking, I could have gotten the latitude and longitude at Fort Clarion, and used GPS to pinpoint it on a map. But a direct road to the installation in usable condition is too much to hope for, so precise coordinates and satellite navigation might not be useful. "You can lead us there again if necessary, right?"

"Sure, but who's us?" He looked around to see if I had anybody with me. I didn't yet, but that would soon change.

"You, me, and the Clarion Volunteers." The surprised expression on Brubaker's face was good for a smile. "I'm back on the job, Mike, and you're my star witness. Let's get some chow, and then we'll go see Mayor Collins about calling up the militia."

Cat must have seen my pins before she recognized my face, because she scrambled to her feet and stood at attention as if I were some potentate, and not somebody who had listened to her flirt with her husband over the phone the day before. "Good morning, Adversary. How may I assist you?"

"Relax, Cat. It's just me, Naomi. I was here yesterday asking about the town's history."

"I know, but you weren't on the job then. You obviously are, now. Do you need the Sheriff? What did Michael do?"

"Mike's fine. He's working with me. I need to see Mayor Collins about raising the militia. It's official business. Here's the authentication key for my orders." I rattled off the hex string four digits at a time, so Cat could key them into her terminal for confirmation.

Cat nodded. "Just a moment, please." Picking up an old-fashioned black telephone that looked solid enough to make a decent weapon, she dialed an extension. "Mayor Collins, Adversary Naomi Bradleigh needs to see you on official business. Yes, Your Honor. I'll send her and Mr. Brubaker right up."

---

### This Week's Theme Song

"The Edge of Darkness" by Iron Maiden, from *The X Factor*

{% youtube t9WU57ovl5g %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
