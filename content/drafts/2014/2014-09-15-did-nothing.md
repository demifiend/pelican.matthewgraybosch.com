---
id: 1330
title: I Did Nothing
date: 2014-09-15T09:42:43+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1330
permalink: /2014/09/did-nothing/
snap_MYURL:
  - 
snapEdIT:
  - 1
snapGP:
  - 
snapLI:
  - 
snapPN:
  - 
snapTR:
  - 's:246:"a:1:{i:0;a:9:{s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:0:"";s:12:"apTRPostType";s:1:"I";s:10:"SNAPformat";s:20:"<p>Source: %URL%</p>";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doTR";i:0;}}";'
snap_isAutoPosted:
  - 1
snapDL:
  - 's:292:"a:1:{i:0;a:8:{s:4:"doDL";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:10:"SNAPformat";s:35:"Source: <a href="%URL%">%TITLE%</a>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:32:"ee5607d15ea851dfe9c11a9f417fa40a";s:5:"pDate";s:19:"2014-09-15 09:43:19";}}";'
snapBG:
  - |
    s:346:"a:1:{i:0;a:8:{s:4:"doBG";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPTformat";s:7:"%TITLE%";s:10:"SNAPformat";s:58:"%RAWTEXT% <p>Original post:<a href='%URL%'>%TITLE%</a></p>";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:63:"http://matthewgraybosch.blogspot.com/2014/09/i-did-nothing.html";s:5:"pDate";s:19:"2014-09-15 09:43:21";}}";
snapSC:
  - 
snapWP:
  - 
snapLJ:
  - 
snapIP:
  - 
snapSU:
  - 
snapDI:
  - 
bitly_link:
  - http://bit.ly/1MivuLc
bitly_link_twitter:
  - http://bit.ly/1MivuLc
bitly_link_facebook:
  - http://bit.ly/1MivuLc
bitly_link_linkedIn:
  - http://bit.ly/1MivuLc
sw_cache_timestamp:
  - 401567
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"8ec98302c441";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:57:"https://medium.com/@MGraybosch/i-did-nothing-8ec98302c441";}'
categories:
  - Personal
tags:
  - I did nothing
  - Office Space
  - slacking
format: video
---
I spent my weekend either curled up with my wife or playing a copy of _The Legend of Zelda: Twilight Princess_ that I had forgotten I had since 2008. Why didn't I do something "productive"? Why didn't I write? Because fuck you is why.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

That's right. I did nothing, and it was everything I thought it would be.