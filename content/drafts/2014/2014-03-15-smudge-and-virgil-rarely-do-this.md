---
id: 257
title: Smudge and Virgil Rarely Do This
date: 2014-03-15T22:29:50+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=257
permalink: /2014/03/smudge-and-virgil-rarely-do-this/
snapGP:
  - 's:94:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0gp";}}";'
snapWP:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0wp";}}";'
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
bitly_link:
  - http://bit.ly/1LnJaiH
bitly_link_twitter:
  - http://bit.ly/1LnJaiH
bitly_link_facebook:
  - http://bit.ly/1LnJaiH
bitly_link_linkedIn:
  - http://bit.ly/1LnJaiH
sw_cache_timestamp:
  - 401650
bitly_link_tumblr:
  - http://bit.ly/1LnJaiH
bitly_link_reddit:
  - http://bit.ly/1LnJaiH
bitly_link_stumbleupon:
  - http://bit.ly/1LnJaiH
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - blanket
  - Cat
  - cats
  - Caturday
  - couch
  - Smudge
  - Virgil
format: image
---
[<img class="alignnone size-full" title="20140315_222545.jpg" alt="image" src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/wpid-20140315_222545.jpg?w=840" data-recalc-dims="1" />](http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2014/03/wpid-20140315_222545.jpg)

It's pretty damn rare for Virgil to lay down with Smudge. They'll play together and groom each other, but sharing the couch? Definitely a rare occurrence.

By the way, Virgil's the black cat. He used to have a brother named Dante, but Dante died young of bone cancer. So after a while we adopted Smudge.