---
id: 98
title: SF Signal interviews Elspeth Cooper
date: 2014-03-05T20:43:59+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=98
permalink: /2014/03/sf-signal-interviews-elspeth-cooper/
snapBG:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0bg";}}";'
snapLJ:
  - 's:50:"a:1:{i:0;a:1:{s:12:"rpstPostIncl";s:7:"nxsi0lj";}}";'
snapWP:
  - 's:159:"a:1:{i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0wp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:2:"92";s:5:"pDate";s:19:"2014-07-14 02:58:09";}}";'
snapGP:
  - 's:241:"a:2:{i:3;a:1:{s:12:"rpstPostIncl";s:7:"nxsi3gp";}i:0;a:5:{s:12:"rpstPostIncl";s:7:"nxsi0gp";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:39:"103251633033550231172/posts/NXkEmmKbn1A";s:5:"pDate";s:19:"2014-07-15 20:12:49";}}";'
bitly_link:
  - http://bit.ly/1PiUrXJ
bitly_link_twitter:
  - http://bit.ly/1PiUrXJ
bitly_link_facebook:
  - http://bit.ly/1PiUrXJ
bitly_link_linkedIn:
  - http://bit.ly/1PiUrXJ
sw_cache_timestamp:
  - 401636
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Uncategorized
tags:
  - Elspeth Cooper
  - epic fantasy
  - fantasy
  - interview
  - SF signal
format: link
---
I found this interview with _Songs of the Earth_ author Elspeth Cooper.

<a title="SF Signal interviews Elspeth Cooper" href="http://www.sfsignal.com/archives/2014/03/an-interview-with-fantasy-author-elspeth-cooper/" target="_blank">http://www.sfsignal.com/archives/2014/03/an-interview-with-fantasy-author-elspeth-cooper/</a>