---
title: "*Silent Clarion*, Track 18: &quot;Locked and Loaded&quot; by Jackyl"
excerpt: "Check out chapter 18 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
I thought at first Cat misdirected me to an attorney's office instead of the Mayor's. Either that, or an accountant's. Law books and budget ledgers filled the polished cherry bookshelves lining the walls. The desk and the three chairs set before it were plain, but the gleam of leather and well-oiled cherry hinted at quality.

The nameplate on the desk read "Mayor B. Collins," and the memory of two miserable winter weeks watching old television serials while fighting the flu led me to wonder if the 'B' stood for Barnabas. Given that I had already run into a Renfield, and that Fort Clarion was the site of Project Harker, having a mayor named after a vampire seemed fitting.

Not that I'd ever ask. He would probably think I was taking the piss, even if he got the allusion. Besides, a quick network search showed his name was Brian. Dammit.

"Morning, Adversary Bradleigh. What can I do for you?" Mayor Collins was a bit shorter than me, and stout, but his grip as he shook my hand suggested he took admirable care of himself. This was a good sign; a man who doesn't care for himself can't be expected to care for others. "Please take a seat. Would you like some coffee? How about you, young man?"

Coffee? Hell yeah, and yes please. "Some coffee would be wonderful. Thanks." Mike concurred. "Thanks, Your Honor. I could use a cup."

Once we had our mugs, Mayor Collins settled into his chair and tapped at his keyboard. "Adversary Bradleigh, it seems you've been tasked with investigating an old military installation nearby and compiling a complete inventory preparatory to cleanup by an ordnance disposal team. Is this the case?"

"Yes, Sir. It should also mention that I will require the aid of the local militia." Collins nodded, sipping his coffee. "Are you aware that we're in the middle of the harvest season, Adversary? You'll be asking men and women to put aside pressing work."

No shit, Sherlock. Not that I'd say anything of the sort. It would be unprofessional. But I can bloody well think it. "I understand your concern. Instead of calling up the entire militia, can you put out a call for volunteers? Naturally, the Phoenix Society will compensate people for their time and effort."

Collins relaxed, leaning back in his seat as he steepled his fingers. "That's fair. To be honest, Sheriff Robinson led me to believe you weren't the sort who was capable of being reasonable."

The Sheriff's been talking about me, has he? Maybe I should have been gentler with him. I might have said I don't invite strange men to my room until I've had a couple of drinks with them first. Not that he'd have bothered with an invitation, but my phrasing would have conveyed sufficient reproof. "He wanted to search my room and belongings without a warrant, and I refused him."

"I don't blame you. He used to be a cop before Nationfall. I don't think he ever got used to people asserting their rights. And he still gripes about having to carry a sword instead of a gun."

So, Robinson's another old-timer. How much does he know about Fort Clarion and Project Harker? "Am I correct in assuming that Sheriff Robinson normally leads the Clarion Volunteers?"

"Hole in one, Adversary. Gotta tell you, he's not going to enjoy being sidelined."

Now, why would I complicate my life by monkeying with the existing command structure? It would be more sensible to have Sheriff Robinson work under me. Any annoyance he suffers from answering to me would be bacon on my pizza. Mmm, bacon. "I've no intention of sidelining him. Would you kindly invite him up?"

Collins nodded and picked up a phone. "Robinson, it's Collins. Come up to my office. I met that young woman you told me about."

We drank our coffee in silence until Sheriff Robinson arrived. He studied me as Collins explained my mission and my request for militia assistance. After Collins finished, Robinson studied me a bit longer. "So, you want to take over my militia and go tramping through that old army base?"

Did Sheriff Robinson just imply that he knew about Fort Clarion? I'll have to feel him out later. First, diplomacy. Let's try an open hand instead of a closed fist. "First, I'd like to apologize for my brusqueness at our first meeting. I had not realized you had served as long as you have."

He glanced at Collins. "So, he told you? Did he mention I used to work narcotics?"

Well, that explains a lot. History shows that the institution of prohibition always leads to police trampling individual rights in their search for contraband. "No, he didn't."

Robinson nodded, and poured himself a cup of coffee. "Tell me something, Adversary. Have you ever led men before? Got any command experience?"

Oh, I'm used to having men under me, but that's not what he had in mind. "No, Sheriff. For that reason, I've no intention of supplanting you as captain of the Clarion Volunteers. I will tell you what I need the militia to do, and you may issue the appropriate orders. Is that suitable?"

"That suits me fine, ma'am." Robinson's attitude shifted, and became more respectful. "How many people do you need?"

Good question. If I bring too many, they'll get in each other's way and make the job harder. If I bring too few, the job won't get done before Ragnarok. "Let's start with a hundred. Try to preserve the existing chain of command."

"Yes, ma'am. Take my IP address so we can use secure talk."

So, the old dog learned some modern tricks. That'll simplify matters. "Thanks, Sheriff. Can you have the volunteers ready by thirteen hundred hours?"

"No problem. I'll have 'em mustered. Anything else?"

I patted the sword on my hip. "I'll need to borrow a rifle from the town arsenal. A pistol as well, if you can spare one."

Robinson nodded, and finished his coffee. "You think we're likely to run into trouble out there?"

Recalling the glint off what might have been a scope high up in one of Fort Clarion's watchtowers, I shrugged. "I'd rather have a rifle I don't need than need a rifle I don't have."

The Mayor spoke up. "Adversary Bradleigh, you've been to Fort Clarion. Did you see anything the Sheriff should know about? What about you, Mike?"

"We haven't actually been inside, sir." Mike glanced at me, and I nodded. "The fence is completely overgrown. We climbed up to the guardhouse's roof and looked over the top. It doesn't look abandoned on the inside."

"Meaning?"

I took over. "I think the installation may still be garrisoned, Sheriff. I don't know who's manning the base, but they appear sufficiently disciplined to keep the installation in perfect order."

"But the North American Commonwealth fell apart decades ago. If Fort Clarion has any soldiers left, they're probably old men. How did they even survive off-grid this long? With us remaining ignorant of their existence until now?" All excellent questions, for which I lacked answers. I'll have to remedy that, and soon, not to mention for a few of my own. What if Christopher Renfield is involved? His uniform was period-accurate, and our whole conversation was weird until he kissed me.

Sheriff shook his head at the Mayor's remark, and gave a disgusted snort. "Oh, come on, Brian. You know damn well this town is so infested with geeks and nerds, we ought to be looking to attract tech startups instead of farmers. I wouldn't be surprised if a bunch of basement-dwellers found the base and fixed it up so they could have a realistic setting for when they play soldier in the woods."

This guy was starting to annoy me. Will he give me cause to arrest him before I'm done? I rather hope he does. "Do you really believe that to be the case, Sheriff, or are you just looking for an excuse to take my mission less seriously than you might otherwise?"

"Adversary, I'll get you your weapons and instruct the men not to fire unless fired upon. Is that satisfactory?"

"Perfectly so, Sheriff." I returned his salute, and turned to Mayor Collins once the Sheriff left. He still looked concerned, no doubt for any townsfolk who might volunteer. "As you said, Your Honor, if Fort Clarion is still occupied, it may be by men too old to fight―or wargamers. It shouldn't come to violence."

"I hope you're right, Adversary." He checked his watch. "I have a meeting in five minutes. Would you like to use one of the conference rooms? No doubt you'll want to set up a proper headquarters instead of working out of your room at the Lonely Mountain."

Does Mayor Collins think I'm going to stay in town, drinking coffee and buffing my nails while the men tramp through the woods? That's so not my style. Adversaries lead from the front. Still, a war room might be handy if I have the only key. "Thank you, Your Honor."

"Good luck. Cat will take care of anything you need." He led us outside, where Cat was waiting. We followed her down to a conference room on the first floor, and there on the table was a gun case. A note rested atop it:

*Let me know if this isn't enough gun. -R*

Unlatching the case, I lifted out a handsome Westchester lever-action rifle with a scope. The walnut stock was engraved with the image of a river and the name of the local militia: the Clarion Volunteers. The steel gleamed as I opened the weapon to determine if it was loaded. It wasn't, so I did the honors from a thoughtfully included box of 7.62x51mmR ammo and chambered a round. The action was silky-smooth, indicating that whoever last carried this rifle took proper care of it.

Returning it to the case, I checked out the revolver and its ammunition. Granted, it wouldn't have the same range or accuracy, but we're trained to fight with a sword in one hand and a pistol in the other.

The revolver was a double-action model, and its empty cylinder held six 11.43mm rounds. I wouldn't be able to fire as quickly as I might with a semiautomatic, and I'd need to take cover to reload, but that's what I get for only bringing my sword. I felt like a nineteenth century cavalry officer as I strapped on my gun belt.

"Michael, you aren't old enough to serve in the militia, are you?" He shook his head. "Nah. You gotta be twenty-one, but I've got a shotgun."

"Go get it, just in case. You might want to bring a mixed load of slugs and buckshot."

"Right." Michael left, and I considered calling Cat. While I could use my implant to call up maps and compile the inventory instead of cluttering the room, the powerful little computer in my head was a strictly private resource. Moreover, if I rigged up a computer properly, I'd be able to check for tampering or attempts to falsify data.

Unlike her namesake, Cat turned up promptly when called. No need to rattle a bag of kitty treats. Or was she the curious sort, and had been listening nearby? "What's up, Naomi?"

"Can you please supply me with maps of the area? Also, I'd like a laptop if Town Hall has a spare."

Cat nodded. "Maps are easy, and my husband will bring a loaner from his shop."

A long-haired, bespectacled man in jeans and a 'Keep Firing, Assholes' t-shirt arrived five minutes later. He was kinda cute, if you like your men cuddly. "Are you Adversary Bradleigh?"

"Yes. Are you Cat's husband?"

"Yeah." He plugged in the laptop and opened it, but didn't power it up. "This laptop's diskless. I understand Adversaries carry devices they can plug into any machine to boot up a secure workstation."

That was certainly the case. I removed one of my pins and ran a fingertip over the back in a predefined pattern, as if I were trying to solve a demonic puzzle box. The pin's back opened, allowing me to remove a tiny memory card. The memory card held a bootable secured Unix variant called HermitCrab that interfaced with my implant for storage. No installation necessary, and far more convenient than doing everything in my head. Naturally, Adversaries got training in this environment for use in computer forensics. "Good thinking. How did you know?"

Cat's husband shrugged. "I helped out with the hardware detection modules."

"Well, thank you. This will be a huge help." The ability to remove my card whenever I wasn't using this laptop would frustrate snoops. Furthermore, I might find computers at Fort Clarion. If I can power them up, I might be able to salvage data from their storage drives.

Michael returned with an antique single-barrel breech-loading shotgun, as I finished confirming the laptop worked. And I thought I was packing old-school heat. He snapped the gun shut after loading a buckshot round about two seconds before Sheriff Robinson opened the door.

"I've got the men assembled outside, Adversary." He held the door for me and gestured toward a crowd assembled outside. "We're ready."

Time to inspect the troops. "Thank you, Sheriff."

---

### This Week's Theme Song

"Locked and Loaded" by Jackyl

{% youtube B2ON0MSuCuo %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
