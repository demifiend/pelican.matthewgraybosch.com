---
id: 119
title: 'Tattoo Vampire &#8211; Part 1'
excerpt: Here's a story I wrote to promote *Without Bloodshed*. It also ties into *Silent Clarion*.
date: 2014-03-06T12:00:33+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - fiction
  - Morgan Stormrider
  - Project Harker
  - science fantasy
  - science fiction
  - serial
  - short story
  - starbreaker
  - tattoo vampire
---
Morgan Stormrider carried with him bittersweet memories of the Queens neighborhood in which Dusk Patrol Tattoo was located. Situated at the edge of Queens, where the city of New York ended and Long Island began, the Nassau's Edge slum constituted a rectangle, three blocks by six, of three story houses whose basements had been converted into shops. The residents refused to leave New York altogether, but were either unable to afford a more fashionable part of the city or unwilling to pay for the privilege of living in one.

The house whose basement Dusk Patrol Tattoo occupied blighted the street. A sane property owner would have razed it to the ground, rather than waste time and money fighting structural decay and black mold. A reasonably paranoid landlord might buy the adjacent houses as well, and raze them on general principles. However, nobody owned the building. Nobody was willing to buy it. It was thus fair game for squatters under city law, though most squatters lacked the audacity to open a business in a building under adverse possession.

_I shouldn't be surprised by the proprietor's temerity._ Morgan smiled as he checked the weapons hidden in his boots. His orders were explicit, and required that he disguise his true nature until it was time to make the arrest – or the kill. _He had the nerve to kill the last Adversary to come for him, so running the sleaziest tattoo parlor in New York while squatting is chump change by comparison._

The sign behind the door claimed that Dusk Patrol Tattoo was open for business, so Morgan pulled the handle and slipped inside. Dim lights mounted in the ceiling flickered, as if fighting a losing battle against the gloom pervading the front of the shop. The proprietor, identified in Morgan's orders as Quincy Westenra, looked up from a battered book and pointed at a wall of faded designs printed on rice paper. "You got a design in mind, kid? If not, take a look at the shop specials. I've got some stuff you've never seen before, and I can do everything in one session, without any pain – I guarantee it."

Morgan nodded, and made a show of examining the shop's designs. He found nothing a cleaner, better lit establishment in a more respectable location could not also offer. One poster advertised a variety of Chinese and Japanese ideograms with translations whose accuracy Morgan distrusted. Another offered romanticized &#8216;tribal' designs. A third offered a variety of nude female figures mounted on great cats and other more improbable beasts. The last offered fantastical designs sufficiently commonplace to comply with an ISO standard.

Pretending to be disappointed, he approached the counter while withdrawing a folded paper from his pocket. "Do you do custom work?"

"Costs extra." Westenra eyed Morgan with an interest that struck him as predatory. "Especially for rough sketches."

"My girlfriend found this on the network. She thought it would be fuckin' metal. I don't think you'll have any trouble with it." Morgan unfolded the paper and showed the proprietor, hoping the sight of the design, a tattoo many of Morgan's fellow Adversaries bore with pride, would rattle him.

Westenra hesitated a moment, and named a price. "Half up front. Half after." He shoved a form across the counter as Morgan reached for his wallet. "Read that and sign first. Gotta have informed consent, since I use anesthesia."

Morgan nodded as he skimmed the form, which specified the use of local anesthetics where the tattoo would be pricked into the skin. He signed on the dotted line and placed a small sheaf of banknotes atop the form, with the pen as a minimal paperweight. "Shall we begin?"
