---
id: 1143
title: 'Saturday Scenes: Without Bloodshed, Chapter 10'
excerpt: More from *Without Bloodshed*, because I'm lazy and nobody cares.
date: 2014-08-16T12:30:08+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - Abram Mellech
  - Adramelech
  - Chapter Ten
  - epic
  - excerpt
  - Munakata Tetsuo
  - read for free
  - read online
  - Saturday Scenes
  - sci-fi
  - science fantasy
  - science fiction
  - thriller
  - torrent
  - without bloodshed
---
Hey, everybody. It's time for Saturday Scenes again. I know I skipped last week, but that was due to personal difficulties. You see, I've spent the last few years dancing along the edge of [burnout](http://nymag.com/news/features/24757/). It's the price I pay for being both a novelist _and_ a programmer.

But you don't care about any of that. You want Saturday Scenes. I haven't had time to come up with something new for you, so here's a scene from Chapter Ten of [_Without Bloodshed_](http://mybook.to/without-bloodshed).

---

Abram Mellech took a hand off the wheel of his restored black Castille to pull at the collar chafing his throat. Everything seemed so simple. Of all the Disciples of the Watch, he was the one most reluctant to indulge in the pleasures of the flesh. This made him perfect for his mission. He adopted the guise of an ascetic, and became Sabaoth's right hand. As a prophet, he gathered and equipped an army willing to kill and die for their faith. When the time came, however, the Repentant in Christ would fight against the demon who dared call himself God.

He looked forward to reclaiming his true self and being Adramelech again. No longer a priest, but a deva who found demonic power and became an emanation of the tree of death. First, he would achieve a flow state, rip the damnable collar from his neck, and brush its ashes from his hands. He already missed Bathory's taste, and the heat of her kiss, despite only meeting her a few days ago. He would seek her next, and—should she permit it―take his fill.

A motorcyclist cut Abram off, forcing him to hit the brakes. He recognized the biker as a Sun Jester; the patch sewn on his jacket was distinguishable from that of the Fireclowns only by color. All he needed was to concentrate and draw power, and he could blow the bike and its rider off the road. Some of the Fireclowns might even thank him for eliminating one of their rivals. He shook his head and pulled at his collar again. "Grant me patience, Lord." _Now I pray to one of my equals, and an enemy besides. The things I do to stay in character._

He managed to park underneath Boston's City Hall without further incident, and retrieved an attaché case containing payment for a final shipment of weapons from the trunk. One of Liebenthal's mercenaries, a heavily tattooed man with a thick red beard, greeted him and verified his identity before escorting him upstairs. He was muscular enough to handle one of the huge vintage motorcycles he and his fellow Fireclowns insisted upon riding, despite the age evident in his weathered face. Abram had no doubt he would handle the rifle slung across his back or the knives in his belt with similar ease. He shot Abram a sidelong glance as the elevator doors closed. "You want to be careful around Yojimbo."

Abram nodded, recognizing Munakata's nickname. "I usually am. Is there any particular reason for concern?"

"Yeah. The client sent us to the Phoenix Society chapter to bag some hostages. It didn't work out."

"What happened?"

"We got our asses kicked, and Liebenthal's pet samurai is still pissy because a woman saved us."

The elevator stopped, and opened its doors. As Abram stepped out, the biker caught his shoulder. "Hey, Rev. I've got a couple of minutes before I need to resume my post downstairs. You want to duck into one of these offices to hear my confession and let me do penance?"

Abram gently brushed off the man's hand. "Sorry. I took a vow of chastity. Christ's peace be upon you."

The staccato of rapid footsteps preceded the metallic hiss of a sword clearing its scabbard. The blade rent air, and hissed again as its owner sheathed it. The pattern's repetition led Abram to a long conference room in which all of the furniture stood stacked against the walls. Munakata's head snapped toward Abram to reveal a visage indistinguishable from Morgan Stormrider's save for a hairstyle out of fashion since the Meiji Restoration. He dressed in black, with the exception of a shirt matching his green eyes. His jacket hung from a coat rack near the door. Abram examined it, and noted the label. "Caderousse & Sons of Marseilles? I'm surprised you'd spare the expense."

Sheathing his katana, Munakata found a chair for Abram. "I'm a sellsword, not a barbarian." His eyes fixed on a crow perched outside the windows. "Are you here for the last shipment?"

"Among other things. What happened last night?"

"Liebenthal thought there might be remnants of the Phoenix Society's presence in Boston: support staff, Adversaries just returned from assignments, and the like. He ordered me to lead a detachment of Fireclowns and do a sweep. We found a couple of Morgan Stormrider's friends, Sid Schneider and Edmund Cohen." Munakata spat the names. "They drew their weapons first. I came within millimeters of tearing out Schneider's throat, but the son of a bitch must have been sparring with Stormrider."

_Witness Protocol tells a different story, but I gain nothing from pressing the matter._ "I understand you had a guardian angel. Who was she?"

"Doubtless you're better acquainted with her than I." Munakata paused, considering the sheathed blade in his hands. When he spoke again, it was almost to himself, as an afterthought. "Schneider seemed aware of the asura emulators' existence. He knew how to put me down permanently. Why did he hold back?"

Here was a question Abram could answer. Despite playing the traitor, he retained access to the Sephiroth. They answered to Imaginos, and were in on the plot. "Stormrider's orders specified no fatalities."

"His orders usually do." Munakata's smile held a cynical edge. "Imaginos can't just tell the man he needs so-and-so rubbed out. The Phoenix Society isn't that sort of organization. Regardless, if he's done with you, you get a choice: Die facing an Adversary, or die with a shank in the back."

Abram's tone lacked its usual gentle warmth, which he used to soothe those burdened by guilt, and ease their consciences for a reasonable fee. "According to Scripture, those who take the sword die by the sword."

"Better to die by the sword than on the cross. Where's Stormrider?"

"He recently arrived in Boston, and brought a companion."

"Who?"

"Naomi Bradleigh. Imaginos expects you to leave his daughter alone."

Munakata turned his back on Abram, who followed his gaze out the window. A pack of bikers riding two abreast approached City Hall. "If I refrain from drawing my blade against his daughter, it will be because she did not first threaten me."

"You showed no such scruples when you cut down Rutherford, Collins, and Gabriel. Your technique left them no time to draw weapons."

"They took bribes from Liebenthal, and are therefore a disgrace to the IRD corps." Munakata looked away. "If I let them live, the Phoenix Society would send somebody other than Stormrider, which does not suit my employer's purpose. As an ex-Adversary, my sword was the only due process they deserved."

"I understand you have no reason to care, but allow me to finish my message. Imaginos hopes you'll rouse Stormrider's curiosity concerning his nature."

"If he wants to learn about CPMD, he can use the network. Why concern yourself?"

"The souls of all Christ's children are my concern. I can ease the weight of your sins."

"My sins made me what I am. I will keep them, and you can keep your Christ." Munakata rose, and grabbed his jacket. He tucked it and his katana under his right arm. "I need some fresh air. Mr. Liebenthal is in his office. He's expecting you."

* * *

If you enjoyed this, get your copy of the complete book using the sample below.
