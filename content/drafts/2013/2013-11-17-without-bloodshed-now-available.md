---
title: 'Without Bloodshed is Now Available'
excerpt: "After years of effort, I am finally privileged to announce the release of *Without Bloodshed*, part one of the Starbreaker saga by Matthew Graybosch."
header:
  image: without-bloodshed-final-cover.jpg
  teaser: without-bloodshed-final-cover.jpg
categories:
    - Project News
tags:
  - Without Bloodshed
  - Starbreaker
  - release
  - announcement
  - book on sale
  - purchase
  - buy this book
---

After years of work and dozens of abandoned drafts in which I refined my technique, my first novel is finally published. [Curiosity Quills Press](http://www.curiosityquills.com) and I are proud to announce the release of *Without Bloodshed*, the first part of [**Starbreaker**](http://www.starbreakerseries.com/about/). The electronic edition is available on Amazon.com using the universal link below:

[http://mybook.to/withoutbloodshed](http://mybook.to/withoutbloodshed)

![Without Bloodshed: Cover by Ricky Gunawan](/images/without-bloodshed-final-cover.jpg)

Morgan Stormrider must put down a coup d'etat in Boston without killing those responsible. The coup's ringleader, Alexander Liebenthal, accused the Phoenix Society of using Adversaries like Morgan Stormrider as assassins. If Morgan cannot put the lie to Liebenthal's allegations, the Phoenix Society will make scapegoats of him and his friends to preserve its own legitimacy.

Please share on social media, check out *Without Bloodshed* on Amazon, and consider buying a copy. If enough people do so, I can quit my day job and focus on writing the sequel, *The Blackened Phoenix*, as well as *Silent Clarion* &mdash; a tale from Starbreaker heroine Naomi Bradleigh's past.
