---
title: "Dialogue Tags Considered Harmful"
excerpt: "If you absolutely *must* use dialogue tags, then for the love of Hell don't add adverbs to them."
date: 2013-03-18T00:12:05+00:00
guid: http://www.matthewgraybosch.com/?p=2524
permalink: /2013/03/dialogue-tags-considered-harmful/
header:
  teaser: dialogue-tags-1024x402.png
  image: dialogue-tags-1024x402.png
categories:
  - How Not to Write
tags:
  - considered harmful
  - descriptive beats
  - dialogue tags
  - adverbs
  - Melanie Card
  - style
  - writing
---
While reading drafts for _Without Bloodshed_, my wife Catherine remarked on how I never use dialogue tags like "said" anymore. She thought it was a mistake, and that I should add them back in. I agreed to, but only if she thought I still needed them after I had finished the draft.

When she asked me why, I explained that I tried writing one scene using just descriptive action beats instead of dialogue tags as an exercise. I liked it, and decided to run with it. I'm not the only one. [Melanie Card explains the technique in "Dialogue Tags vs Descriptive Beats"](http://melaniecard.com/for-writers/dialogue-tags-vs-descriptive-beats/).

> Descriptive beat: a sentence before, after, or breaking up dialogue that describes a character’s response or action.
> 
> Eg:
> 
> She fluffed her hair. “I’m ready for my close up.”
> 
> “Coffee?” He held out a mug.
> 
> **These examples are pretty basic, but you can effectively eliminate all or most dialogue tags by working these descriptive beats among your dialogue.**
> 
> A word of warning about descriptive beats: pick quality descriptions, ones that reveal a character’s personality or motivation or adds to the setting or feel of the story. Too many meaningful glances, or smiles, or nods can make the descriptions feel repetitive and unoriginal.
> 
> Now that you know about dialogue tags versus descriptive beats, let’s take a quick look at punctuation (another area easy to fix that will strengthen your writing).
> 
> Dialogue tag: “Pass the peas,” he said. (note the comma inside the quotation marks)
> 
> Descriptive Beat: “Pass the peas.” He reached for the pot. (note the period inside the quotations)
> 
> It’s as simple as paying attention to what you’re writing. Ask yourself this: is this a way of speaking? If yes, then punctuate with a comma. If no, then use a period. 

Ms. Card's post explains that you can use descriptive beats instead of dialogue tags. I'm going to go farther, and suggest that dialogue tags should be considered harmful, because it's too easy to tack on adverbs.

If you do something like this&#8230;

> "Meow," said Smudge. "Feed me already." 

It's easy to go on and do _this_&#8230;

> "Meow," said Smudge plaintively. "Feed me already." 

The &#8216;plaintively' is the adverb, and whether you like Hemingway's spartan style or not, the use of adverbs when describing dialogue is long out of fashion.
