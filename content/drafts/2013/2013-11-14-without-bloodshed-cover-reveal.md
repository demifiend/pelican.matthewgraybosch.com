---
title: Without Bloodshed Cover Reveal
header:
  image: without-bloodshed-final-cover.jpg
  teaser: without-bloodshed-final-cover.jpg
categories:
    - Project News
tags:
  - Cover Reveal
  - Google
  - Hangout
  - Without Bloodshed
  - Starbreaker
---
Please join me as I reveal the new cover for my first novel, _Without Bloodshed_ (Part One of Starbreaker), available in electronic and paperback formats on **Sunday, 17 November 2013**.

Tell your friends. In addition to revealing the cover, I'll be reading excerpts from my novel if I can get hangouts working on my computer, and answering any questions you might have about +Starbreaker &#8212; as long as it doesn't involve spoilers.

{% include base_path %}
![Without Bloodshed cover. Artwork by Ricky Gunawan]({{ base_path }}/images/without-bloodshed-final-cover.jpg)

It's gonna be a black Sabbath. Join me, or carry your regret to your grave!

---

[This post is reclaimed from Google+.](https://plus.google.com/events/cf3gib7va4cevubejaq8ir7ui0k)
