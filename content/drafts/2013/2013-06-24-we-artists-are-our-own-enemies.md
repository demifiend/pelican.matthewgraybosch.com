---
id: 2247
title: We Artists are Our Own Enemies
excerpt: Like other workers, artists are too busy competing for corporate America's favor to band together and *take* their rightful due from the rich.
date: 2013-06-24T06:41:26+00:00
guid: http://www.matthewgraybosch.com/?p=2247
permalink: /2013/06/we-artists-are-our-own-enemies/
header:
  image: solidarity_by_matzek.png
  teaser: solidarity_by_matzek.png
categories:
  - /dev/random
tags:
  - capitalism
  - competition
  - independence
  - solidarity
  - Karen Michaelson
  - sabotage
  - War on the Humanities
---
Author and attorney Karen Michalson released the third of her essays covering [_The War on the Humanities_](http://www.karenmichalson.com/the-war-on-the-humanities-has-three-fronts-part-3-artists-themselves/) recently. In this installment, she takes on the artists themselves, accusing them of sabotaging each other, and thus themselves, in their desperation to be promoted by corporate America. Such promotion, Michalson argues, has become a proxy for aesthetic merit and artistic legitimacy.

If you're an artist of any sort, you've probably heard this song before. It goes like this: "If a corporation isn't selling your stuff, you can't possibly be very good." I gave up music because of this, because I knew I'd never be able to get signed as a rock violinist.

It's easier for musicians to buck this tradition, despite corporations encroaching on indie rock. Writers are just beginning to establish their own indie space, and the fight for legitimacy still rages.

However, I may be fortunate. The writers with whom I'm acquainted on G+ still grasp the concept of mutual aid, and if we compete, it's to create better work, not to have the most followers or sell the most books.

Despite this, Ms. Michalson makes a salient point: the emphasis on copies sold and the primacy of good marketing over good art makes it difficult for introverted artists or artists who put their work first to get the audiences they deserve.

I'm not going to complain, despite being one of those artists who would rather focus on his work than on his promotional efforts. I knew from the start I'd have to pay my dues if I wanted to make it big. I just expected my dues to take the form of time spent refining my craft and learning to write a genuinely good science fantasy novel. If I also have to learn to market my work, so be it.

I don't want your pity. I want you to buy my book, and then I want your friends to buy my book.

And I want you to stop stabbing yourself and your fellow workers and artists in the back and stand together.

{% include base_path %}
![Solidarty by Matzek @ DeviantArt]({{ base_path}}/images/solidarity_by_matzek.png)

[Image by Matzek @ DeviantArt](http://matzek.deviantart.com/art/Solidarity-175732848)

## Karen Michelson's _War on the Humanities_

  1. [The War on the Humanities Has Three Fronts (Part 1: The Right Wing)](http://www.karenmichalson.com/the-war-on-the-humanities-has-three-fronts-part-1-the-right-wing/)
  2. [The War on the Humanities has Three Fronts (Part 2: Higher Education)](http://www.karenmichalson.com/the-war-on-the-humanities-has-three-fronts-part-2-higher-education/)
  3. [The War on the Humanities has Three Fronts (Part 3: Artists Themselves)](http://www.karenmichalson.com/the-war-on-the-humanities-has-three-fronts-part-3-artists-themselves/)