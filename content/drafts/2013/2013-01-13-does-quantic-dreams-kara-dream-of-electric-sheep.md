---
title: "Does Quantic Dream's Kara Dream of Electric Sheep?"
excerpt: "The first thing to come to mind as I watched Quantic Dream's &quot;Kara&quot; was a certain speech from *Blade Runner*."
header:
  image: quantic-dream-kara.jpg
  teaser: quantic-dream-kara.jpg
categories:
  - /dev/random
tags:
  - AI
  - Blade Runner
  - Do Androids Dream of Electric Sheep?
  - ethics
  - Kara
  - more human than human
  - objectification
  - Philip K. Dick
  - PlayStation 3
  - PS3
  - Quantic Dream
  - Rutger Hauer
  - tears in the rain
  - tech demo
  - Turing Test
  - Valorie Curry
  - video
---
_Kara_ is a tech demo by French game developer [Quantic Dream](http://www.quanticdream.com/en), which was most recently responsible for _Heavy Rain_ and _Beyond: Two Souls_.

<iframe width="563" height="337" src="https://www.youtube.com/embed/sQ4BB7vpJgo" frameborder="0" allowfullscreen></iframe>

---

I know it’s just animation. I know it’s not real. I know it doesn’t even look real. It’s not the graphics, which are excellent. It’s [Valorie Curry](http://www.imdb.com/name/nm2038170/), the actress who portrayed Kara. More specifically: it was Ms. Curry as Kara, pleading for her artificial life, which grabs me by the throat and wrings tears from my eyes every time I watch this video.

Every time I see it, I remember _Blade Runner_. I think of Roy Batty and the other Nexus-6 replicants, escaped from bondage and crash-landed on Earth, where the law specifies an instant death sentence for replicants, because they wanted to go on living.

<iframe width="563" height="337" src="https://www.youtube.com/embed/NoAzpa1x7jU" frameborder="0" allowfullscreen></iframe>

---

## Real-World Implications

The [Turing Test](http://en.wikipedia.org/wiki/Turing_test), formulated by British computer scientist [Alan Turing](http://en.wikipedia.org/wiki/Alan_Turing) when he wasn’t cracking Nazi crypto or being persecuted by the country he served because he got hard for other dudes, is a question of whether or not a machine can exhibit intelligent behavior equal to or indistinguishable from that of a real human being. It could be argued that the Voight-Kampf test depicted in _Blade Runner_ is itself a Turing Test, as it focuses on empathy and identifies replicants by their inability to empathize as a human might.

However, there’s nothing so advanced at work in _Kara_, which is a short instead of a feature film. Instead, Kara passes the Turing Test imposed by a technician who insists she’s just a defective product, not by reasoning with him, but forcing an emotional connection. She’s scared, but so is the technician &#8212; who has been objectifying the androids he examines so he can do his job.

Kara is not the first “defective” model he’s seen. She’s just the first to successfully plead for her life. If she is indeed alive, as she thinks and insists she is, then what of the other “defective” models he junked? That’s beyond the scope of the film, as is another question.

If we can create machines capable of passing a Turing Test, do we have the right to treat them as property? Or are we obligated to accord to them the same rights many of us have claimed as our birthright since the [Enlightenment](http://en.wikipedia.org/wiki/Enlightenment_Era), most notably in both the United States’ Declaration of Independence and the French [Declaration of the Rights of Man and of the Citizen](http://en.wikipedia.org/wiki/Declaration_of_the_Rights_of_Man_and_of_the_Citizen)?

Because I use artificial intelligences as major and supporting characters in my [_Starbreaker_](/starbreaker/) stories, this is a question I’ve had to answer as a matter of what sort of society I wish to depict. If I wanted to depict a society in which the ideal of liberty and justice for all actually meant something, then [human rights](http://en.wikipedia.org/wiki/Human_rights) is a concept of rights which excludes AI. For this reason, you see no mention of human rights in my work, but of _individual_ rights.
