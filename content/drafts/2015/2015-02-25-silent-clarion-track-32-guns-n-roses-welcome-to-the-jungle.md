---
title: "*Silent Clarion*, Track 32: &quot;Welcome to the Jungle&quot; by Guns 'n Roses"
excerpt: "Check out chapter 32 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
The meaning of Iris' text message didn't become clear until the next morning, when I found Edmund Cohen devouring a hearty breakfast in the common room of The Lonely Mountain. A huge package wrapped like a birthday present rested on the table beside him. Dante and Virgil were also on the table, purr-begging as usual. The old soldier must have been a soft touch, because they had him feeding them scraps of steak out of his weathered hand between mouthfuls.

"Those kittens are going to get fat if people keep giving them scraps." Sitting down with Edmund, I waved to Bruce behind the bar.

Cohen chuckled. "I guess you've been feeding these little buggers, too. It was either this, or have them climbing on me. Last thing I need right now is dual shoulder-mounted kitties. I'm too ticklish to deal with kitten whiskers in my ears."

That would have been a sight to see. "Whose birthday is it?"

"Actually, Nims, that's for you. It's your unbirthday."

"My what?" My mouth outpaced my brain, which had to dredge up bits of Lewis Carroll before Cohen's explanation made the slightest bit of sense. The Alice stories had been among my favorites until I learned that their author had a thing for young girls. After that, the duology held a rather creepy undertone for me. I held up a hand and smiled as Eddie opened his mouth to explain. "It's all right. I get it now. I just haven't had my morning coffee yet."

Whatever my unbirthday present was, it was bloody heavy. Wishing the damned thing had handles, I lifted the package from Eddie's table to another so I could open it without getting in his way. "What the hell is in this thing?"

"An industrial-strength hair dryer. Saul and Iris told me you couldn't live without it."

Saul and Iris, eh? Giving Cohen the finger, I untied the bow and let it fall to the floor for the kittens to enjoy. My removal of the gaily colored wrapping paper revealed a second layer of sturdier brown paper, which I also ripped away.

The kittens were underfoot when I was done, playing in the shredded wrapping paper while I stared at a dull gray steel case stamped with the hiragana I recognized as the official brand of the Nakajima Armaments Company of Osaka. Cohen stood beside me as my hands hesitated on the latches. "Open it."

The case wasn't a simple box, but more closely resembled a giant's bento stuffed with delightful treats. One layer contained armor. I don't just mean the armored coat most people wear as a compromise between effective defense and aesthetic appeal, the sort I usually go with on the job so that I don't look like I'm ready to get medieval on a suspect.

This was the real deal, the sort of armor an Adversary might wear when standing alone against a riot. The padded carbon-fiber weave covering my entire body from the neck down would protect me from teeth, claws, blades, and small-diameter bullets. The ceramic plates forming the outer layer would most likely keep everything short of an anti-tank round from getting through, though the blunt-force trauma would be a bitch.

Best of all, it was black with red accents. Not that the coloring made the armor more functional, but it showed Nakajima's usual attention to every conceivable detail. The styling lent the armor an aura of classy menace that might allow its wearer to intimidate others and win fights before they began, or prevent them altogether. I'd think twice before drawing a weapon when facing an opponent wearing this.

I tried on one of the gauntlets, admiring the intricate network of ceramic scales protecting the back of my hand and fingers. The helmet was a high-tech affair. The visor was completely opaque, but the helmet used hidden cameras that interfaced with my implant to display my surroundings in realtime with low-light and infrared modes. Joan of Arc and Tomoe Gozen would have loved this stuff.

Not that I told Eddie that, because I wasn't sure if he had heard of either of these warrior women. Instead, I gave him a quick hug and kissed his cheek. "Thanks, Eddie. It feels like Winter Solstice."

Eddie shrugged, but the color rising in his face suggested my gesture affected him despite his casual manner. "No worries. I owed Saul and Iris a favor. Nakajima Kaoru owed me another."

"Nakajima Kaoru herself prepared this gear for me? Holy shit."

That got a laugh out of Eddie. "Wait till you see the weapons."

"Bloody hell." Once I cracked open the other half of the case, I understood his meaning. It contained four semiautomatic pistols, for starters. Two was reasonable. In the heat of battle a New York reload was sometimes faster than swapping magazines. Four pistols was Hong Kong gangster movie territory.

In addition to the pistols, I found a rifle. It wasn't a standard-issue Kalashnikov, but more closely resembled the sort of carbine Sheriff Robinson might have carried as an MP in the Commonweath Army. Nakajima had equipped it with all the trimmings: an electronic scope, tactical grip, laser sight, flashlight, suppressor, and a grenade launcher. "What the hell does Nakajima expect me to do with a grenade launcher?"

Eddie shrugged. "What do you usually do with a grenade launcher? Blow shit up."

"Ever hear of a rhetorical question?" Yes, that was just a bit bitchy. Like I said, I hadn't had my morning coffee yet. "This is just a lot more heat than I usually pack on the job. I normally get by with my sword and a single pistol."

"Swords?" Now Eddie wore the smile of a man suppressing laughter at another's expense. Maybe a sword was old-fashioned compared to this pimped-out tactical carbine, but I couldn't let all this firepower seduce me. Most of the time, goddammit, a sword is enough. "There's a couple of those in there, too."

"But I already have a perfectly good sword." Despite this, I put down the rifle. There were indeed two swords among the treasures arrayed before me, the paired katana and wakizashi of a samurai. These would probably prove more effective against a Dusk Patrol soldier. If the blades were sharp enough, and I strong enough, I could lop off limbs. Had Nakajima known my enemies could shrug off a thrust from my sidesword?

Cohen watched with a concerned expression as I lifted the katana to feel its weight. Drawing the blade partway, I gazed entranced at the waves captured within the steel. "You could take somebody's head off with that."

"No shit, Sherlock." I sheathed the sword and put it down. "Please tell me this equipment is only a loan. There's no way I can keep all this gear. It's too valuable."

"Actually, you can, but Nakajima doesn't expect you to. She said something about how you blush too easily."

Though I wanted to protest, the truth was that I had blushed when Nakajima Kaoru had insisted on giving me her personal attention when I was buying my current sword, and had found a sidesword that was almost perfect except for the balance. Her offer to hand-forge a blade to suit me instead of customizing a production model hadn't fazed me, but I had been shocked by her answer to the inevitable question of price. "No extra charge, because you're an Adversary and I respect your cause. I think you'll understand when I tell you I hope for a long and mutually beneficial relationship."

Even though I understood her, I couldn't hide my embarrassment. I had only recently taken my oath, and was using the last of my savings to buy better gear than the standard-issue Murdoch junk. I was practically a nobody, and here was the founder of a well-respected arms manufacturer treating me like I was the demon-ridden Empress of Japan and offering to forge a sword for me with her own hands. "I do, and your generosity astounds me."

And here was Nakajima Kaoru's astonishing generosity again. Even if she was doing it to repay a debt to Edmund Cohen, it was hard to believe I deserved this gear as a loan, let alone as a gift to keep. She did indeed respect the cause. And because I respected her, I had to return it. It wouldn't make me invincible, but it would confer power few civilians could hope to match.

Simply having this equipment available had a profound psychological effect on me. "Thanks, Eddie. If you speak to Ms. Nakajima, please convey my thanks. I'll return this gear personally once I've completed my mission."

Eddie gave me a cockeyed grin. "Afraid it'll go to your head?"

"I think it already has. I feel like I could march into Fort Clarion alone and round up every remaining member of Dusk Patrol. The only thing stopping me is my ignorance of the way underground."

"Adversary Bradleigh!" Mayor Collins' voice was especially strident this morning, and it pierced my eardrums.

Turning away from my borrowed arsenal, I faced the Mayor with my sweetest smile and a tone guaranteed to induce adult onset diabetes. "What seems to be the problem, Your Honor?"

"What the fuck were you doing last night? Playing with yourself?"

"That's a rather intimate question, sir, so I'll pretend for your sake it was rhetorical. To answer your first question, I reported to my superiors, listened to Charn while they performed in the common room, and then went to bed."

"Well, I hope you enjoyed the show while another of my citizens died alone in the forest last night. Maybe your oath to uphold individual rights doesn't matter as much as you pretend it does!"

"The Universal Declaration of Individual Rights hasn't been revised to include the right to be a demon-ridden idiot. Considering the number of missing tourists you've failed to report to the Phoenix Society, I should think the people of Clarion would be well aware that the woods are dangerous for lone individuals."

Eddie shook his head at Mayor Collins' display. "Mr. Collins, are you sure you want to talk about accountability given your current position?"

Collins rounded on Eddie, his fists raised as if he were about to deck him. I almost hoped he'd try, if only to see how hard Eddie would kick his arse. "And who the fuck are you?"

"Edmund Cohen, Phoenix Society Executive Council. Any further questions, mate?"

Eddie's on the Executive Council? A thousand questions sprang to mind, but Bruce Halford joined us before I could finish prioritizing them. He pointed a shotgun at Mayor Collins. "Excuse me, but why are you abusing my guests?"

Collins rounded on Bruce, heedless of the weapon aimed to blow his guts out. "Mind your own business, Halford."

Bruce pumped the shotgun, racking a round into the chamber. "I am. Show me a warrant, buy something, or get the fuck out of my inn."

---

### This Week's Theme Song

"Welcome to the Jungle" by Guns 'n Roses, from *Appetite for Destruction*

{% youtube o1tj2zJ2Wvg %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
