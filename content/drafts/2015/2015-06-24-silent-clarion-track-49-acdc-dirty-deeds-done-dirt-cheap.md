---
title: "*Silent Clarion*, Track 49: &quot;Dirty Deeds Done Dirt Cheap&quot; by AC/DC"
excerpt: "Check out chapter 49 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
When I first paired my Conquest with my implant, it was so the bike could send information like current speed, charge remaining, engine temperature, and tire pressure directly to my implant's visual overlay instead of making me glance at little dials. It was a standard feature, and I thought nothing of it until my implant displayed a notification telling me that my bike was five kilometers away, and asking if I wanted to call it. As a lark, I chose the "call bike" option. A few minutes later, my Conquest trundled riderless up Main Street and stopped in front of me.

Mike stared at my ride, no doubt as surprised as I. "Naomi, did you know your ride had autopilot?"

"No. That's what I get for not reading the manual." And if Jacob Spinoza had told me this bike could steer itself, I might not have haggled so hard.

Not that reading the documentation stopped us from going off-road, with Mike clinging to my waist for dear life. He pressed his face into my shoulder, which in other circumstances might have been cute. No doubt a collaboration between marketing and legal came up with the warning that Conquest motorcycles weren't designed for such use, for the bike performed almost as well off-road as it did on the highway. It just didn't look as nice spattered with mud. Nor did I, for that matter.

Of course, we left a nice clear trail for anybody who wanted to follow us. It had started to rain while we were inside Gibson Hacker Supply, and the ground quickly softened enough that a bike with two riders left tracks screaming *Naomi went this way*. However, getting there on foot in this weather would have taken much longer.

I'll admit it: my plan's sanity was questionable. Here I was, returning to Dusk Patrol's home base, where several of their number no doubt still slept. The only upside was that at Fort Clarion I might manage to avoid further civilian casualties.

Further civilian casualties other than Mike, that is, who seemed capable of fighting beside me. At least, he kept himself together when we faced Mayor Collins, and seemed to know his away around the designated marksman rifle he filched from the armory. My superiors would bitch about the discrepancy between my report and what their arms control crew actually found.

Fuck 'em. If we're going to make a stand here, we might as well be properly equipped. I was about to emulate Mike's example and grab a carbine with an grenade launcher mounted under the barrel when I saw him pick up an object marked 'Front Toward Enemy'. "Is this what I think it is? And why are you grinning like that?"

Why? Because I just had a deliciously evil idea. If I really wanted to fuck with Dusk Patrol, what better way than to use their own weapons to deny them access to their arsenal? The Network of Things had infiltrated the military, resulting in mines a soldier could detonate with a smartphone, or rig to detonate should anybody else approach.

The last thing anybody dumb enough to get too close would here before the bang would be a keening scream. "I had forgotten they had Mandrakes here. Let's set some by the entrance. And see if there's any Semtex."

"You're going to rig the place to blow if somebody else comes in here?"

"Oh yeah. So grab plenty of ammo; we won't be able to come back for more." While Mike scurried off, I set my traps. With a bunch of shriekers slaved to each other, the first schmuck to set foot in here would get his legs blown out from under him and blow up every bit of ammo and explosives the building. I was sure to catch hell from the Phoenix Society for this bit of dirty pool, but right now I had more pressing concerns.

Besides, all's fair in love and war.

Since I had a couple of shriekers left, I took them with me. I could rig them in front of the entrance to the watchtower. They'd hurt somebody, and give Mike and me warning we were about to have company. Speaking of Mike, I should probably see if he's actually any good with that rifle. "How accurate are you with that weapon?"

Mike shrugged, and pointed skyward, where a flock of Canada geese called out to one another with harsh cries as they flew south. A lone straggler trailed behind. "See that goose flying alone?"

Oh, no. "Must you?"

"Any idiot can nail a stationary target. And this one is probably old or sick. See how erratically he flies?" As Mike pointed out the goose's flight, it plummeted from the sky as if dying in mid-flight. Mike led the bird for a moment, and fired. The report echoed through the fort with a sharp crack, and a second later I saw the poor goose burst into a spray of gore and feathers. Mike lowered the rifle. "Any questions?"

"You realize I'm not happy you killed a bird just to prove yourself to me? Even if he was going to die anyway."

Mike nodded. "Yeah, but try visiting Clarion in the spring when they're underfoot, crapping everywhere, and attacking people who get too close. You'll be ready to bag your limit within a week."

"So, you hunt them?"

Mike shrugged. I hefted the backpack full of supplies I looted from the mess hall. "Come on. Do we need anything else before we hole up? I really don't want to push my luck any further without cause".

"Don't think so. We're armed, we've got some food and water, and you grabbed a laptop from Gibson Hacker. Right?"

Recalling the banknotes worth 250mg of gold I left on the counter, I nodded. "Yeah. Nothing fancy, but it'll run HermitCrab and let me talk to Tetragrammaton over secure shell unless somebody cuts the power to both machines."

"So, where do we make our stand?"

"The western tower." I turned and began walking toward it, and Mike had my back. At least in the tower we'd have the advantage of elevation. Even if they surrounded us, we would see them coming from all sides and snipe at them as they approached.

The stair leading up was also defensible. Even if I hadn't placed my last two Mandrakes at the entrance, it was a tight space. Between some flashbangs appropriated from the armory and the late Matt Tricklebank's shotgun, we should be well placed to hold our own.

The top of the watchtower had a Tesla point. At least I wouldn't have to worry about the battery running out. Not that I expected to need ten hours, but sometimes it's the little things that make the difference. While HermitCrab ran though its startup scripts, I took in Fort Clarion from above.

While the immediate vicinity of the towers themselves was quite open, the barracks and other buildings provided more cover than I would have liked for attackers. There was no way we could secure the individual buildings, let alone ensure they remained so. Hopefully Dusk Patrol didn't have high-power rifles stashed underground, or something ridiculous like a recoilless rifle or a rocket launcher.

With that cheery thought, I picked up my laptop and connected to Tetragrammaton via secure shell. Fortunately, I got a strong signal high up above the base, and the damage Mayor Collins had done to Tetragrammaton's console and chassis hadn't brought the computer down. The credentials Malkuth provided earlier gave me the keys to the kingdom. "Mike, I just found your porn stash. You naughty boy."

"That's just for show, in case my parents ever got into my account and went poking around. Didn't you ever have a fake diary for your parents to find?"

"I didn't have the sort of relationship with my parents that made such subterfuge necessary. Also, I left home to study in New York when I was fourteen."

"Damn." Mike shook his head, and scanned the base through his scope. "I wish I had had the nerve to do that."

"You had the nerve to fight beside me."

"Thanks." Narrowing his eyes, he placed his finger on the trigger, only to remove it a second later. "Just a deer. You find anything yet?"

"I'm in the directory for Petersen Family Medicine right now. If I don't find anything here, I can also hit his personal account and see if he has anything more interesting than porn there." Hoping that the grep tool would prove that sometimes old tricks were the best kind, I searched for 'project harker' from the command line.

And I got nothing. At least, nothing in the account for Petersen Family Medicine. Same with 'Dusk Patrol', 'Harker', and 'Renfield'. Just for chuckles, I tried my own name. Nothing but my records for when I got patched up. Nothing terribly interesting there, either.

Time to hit his personal account. Running the same commands I used earlier, I hit paydirt. The output just kept scrolling until I aborted the search. A couple seconds of work, and I tailored and tailored my command to just list the directories containing files that mentioned Project Harker. There was only one, named "harker".

That's where I hit the jackpot. It was all there: requirements, specifications, protocols, security recommendations, and gigabytes of email. Now to find out what role Petersen really played in all this. Was he Frankenstein, or the unwilling bystander and advocate his men believed him to be? Searching in the email directory for 'proposal' and filtering the output by a search for 'mutiny', I found the following:

> from: Col. Henrik Petersen  
> to: Dr. Ian Malkin  
> date: 6 May 2046  
> subject: Risk Management
>  
> Dr. Malkin:
>  
> I have just finished reading your proposal.
>   
> While I agree that the men and women of Dusk Patrol would make excellent candidates for your experiments, we must tread carefully. They have rights under the law, and many of them are aware of this fact. They must be carefully managed lest they mutiny, or speak to the media.
>  
> Therefore, it is my suggestion that the authorization for this project come not from me, but from a general in my chain of command. This will allow me to credibly act the part of advocate for my men, protesting on their behalf against illegal human experimentation.  
>
> If I am overruled, but continue to play the part of advocate, the soldiers will grudgingly allow themselves to be subjected to your experiments. Lt. Collins will maintain discipline among the men and punish dissent.
>   
> ---  
> Col. Henrik Petersen
> North American Commonwealth Army
> Fort Clarion, Mid-Atlantic Province

The reply was succinct, and if Project Harker hadn't been Ian Malkin's baby, I might even have liked the man:

> from: Dr. Ian Malkin  
> to: Col. Henrik Petersen  
> date: 6 May 2046  
> subject: RE: Risk Management
>  
> Your logic is sound, albeit repugnant even to me.
>  
> ---
> Dr. Ian Malkin, CEO
> AsgarTech Corporation

I looked up in time to see Mike stand up and stretch. He glanced at me. "Find anything?"

"Only stuff I don't dare share with Renfield or the others if I want to get Petersen and Robinson in front of a jury. But they're not the only people who need a bit of due process. Ever hear of somebody named Ian Malkin?"

"Nope." Mike shrugged. "You're rummaging in Petersen's directory, right? Is there anything about breeding experiments or genetics?"

"Let's have a look." 'Breeding' or 'genetic' might be too specific a term, though, so I started with 'population'. A smart move on my part, because this came up.

> from: Col. Henrik Petersen  
> to: Dr. Ian Malkin  
> date: 3 June 2049  
> subject: Further Research
>  
> Dr. Malkin:
>  
> It is clear that the North American Commonwealth will soon collapse, especially since I faced no official reprisals for my use of the GUNGNIR platform against civilian protesters gathering outside the base. Everybody has more pressing concerns than a bunch of whining hippies who think we're trying to create a battalion of super-soldiers.
>  
> However, I think it's too soon to conclude Project Harker. The research to date has turned up several interesting questions requiring further investigation.
>
> 1. We don't know what long-term effects the Renfield Protocol will have on those it didn't kill outright. A longitudinal study is indicated.
> 2. We have not attempted to replicate the research with other populations. Therefore, we don't know why the Renfield Protocol works on some CPMD+ individuals but not others.
> 3. We don't have a control group. The most likely candidates would be a population of CPMD- individuals.
>
> You seem to know people involved with this new NGO, the Phoenix Society. Would any of them prove sufficiently sympathetic with our mutual aims to help repopulate the town of Clarion? With sympathetic allies in local government, I can pose as a local physician and continue your work in secret.
>
> In a chaotic world, the Society may need to rule rather than serve. It cannot do so without soldiers capable of enforcing its dictates. By continuing the work begun with Project Harker, we can *provide* these soldiers.
>   
> ---  
> Col. Henrik Petersen
> North American Commonwealth Army
> Fort Clarion, Mid-Atlantic Province

By the time I had finished reading this, I was shaking. Not only were the suspicions Mike shared with me true, but the truth carried implications that affected *me*. Did the Phoenix Society use technology developed by Project Harker on Adversaries like me? Had *I* been subjected to the Renfield Protocol? And who was Ian Malkin?

That last question I might be able to answer on my own. Running a search on the name, I found a hit in the metadata for an image file. As I opened it, the screen filled with a high-resolution color photograph of a handsome blue-eyed man wearing a white double-breasted suit with a blue cravat. He was snow-blonde, like me, and the expression of secretive amusement with the world that the photographer captured was one I remembered from my sparring sessions with Maestro. Looks like we might cross swords again.

---

### This Week's Theme Song

"Dirty Deeds Done Dirt Cheap" by AC/DC

{% youtube whQQpwwvSh4 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
