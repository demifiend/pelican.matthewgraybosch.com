---
title: "*Silent Clarion*, Track 33: &quot;Etude #3 In E, Op. 10/3, Tristesse&quot; by Frédéric Chopin"
excerpt: "Check out chapter 33 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
It wasn't until after I had secured Nakajima's gift and washed down a hurried breakfast with a mug of black coffee that I contacted Sheriff Robinson and got his location. Though I was tempted to put on Nakajima's armor, I refrained. Wearing all of that when I wasn't heading to a situation where overwhelming force was an absolute certainty would only make me look overdressed.

An Adversary's authority didn't come from her pins, or even from the organization backing her. It came from her willingness to use minimal force. It wasn't tactical superiority that mattered, but moral.

Besides, if Sheriff Robinson was right about Christopher Renfield often appearing in town, I didn't want him to see me rigged up like some kind of urban samurai. Would he report it to his comrades? It didn't seem likely, but I wasn't willing to risk it. Most of a week had passed since our coupling, and I had no idea how he fared against his fellow soldiers. Was he aware of the latest murder; or worse, involved?

Hopefully, he was able to get them back under control. Fighting two of them at night had been bad enough. In a situation where they need not remain unnoticed, they'd come at me with everything they had. It's what I would do in their situation.

I was still trying to get into their heads based on what little I knew when I almost walked into a tree. Fortunately, Robinson stopped me. "Not quite awake yet, Adversary?"

"I am *now*." No doubt my embarrassment showed. "Do you have an ID on this morning's victim?"

"Clarence Foster, eighteen years old." Robinson led me to the body.

It had been stripped, and subjected to the same treatment as Scott Wilson. Taking a pair of rubber gloves, I crouched for a closer look. Getting his legs spread was hard work, Foster having been dead long enough for rigor mortis to set in. "Same stab wound at the juncture of thigh and groin, Sheriff. Did anybody check under his fingernails?"

Deputy Colby shrugged beneath her forensic backpack. "No joy. You think he tried to fight back?"

"Unless he was prevented from doing so." Robinson crouched by Foster's body, measuring the space between furrows on each bite except the one on his penis.

I held my hand out for the tape measure, since Robinson seemed unnecessarily squeamish about touching dead men's genitals. Colby flashed a conspiratorial grin at me while sending a text. ｢Such a tragic waste. This young man had so much to offer.｣

Naturally, I ignored Colby's crude attempt at gallows humor. "Fifteen millimeters, Sheriff. I think a smaller woman than me was responsible. Or a smaller man. I doubt we have any business inferring gender on the basis of bite marks."

"I'll be sure to relay that to the Mayor." Robinson opened up a body bag and shook it out, getting it ready for its occupant. He gave me a pointed look. "He still thinks you're our best bet for a suspect."

That didn't merit more than a shrug from me. "And I still think he's a schmuck." At least Collins hadn't resorted to rounding up every resident with CPMD. Not that I'd be the first to voice such thoughts. Somebody might mistake it for a good idea. "Given the attack I fought off, and Scott Wilson's murder soon after, the facts suggest our most likely suspects are under Fort Clarion."

Pointing at the body, I brought everybody's attention back to the matter before us. "How did they manage to overpower Foster without him taking any defensive wounds, or getting one of his killers' tissue under his fingernails?"

Robinson shrugged, but Colby seemed to honestly consider the question. "Maybe they knocked him out first. Simplest way would be a blow to the head, especially if you intend to kill the guy anyway, but I didn't see any sign of a concussion so far."

Her meaning was pretty obvious, so I grabbed Foster's ankles. "OK. Let's get him turned over."

Taking the dead youth's shoulders, Colby helped me roll the corpse over. Lividity had well and truly set in, and with all his blood pooling in tissue that had until now been closest to the ground, I suspected it would prove more difficult to determine why Foster hadn't been able to resist his attackers. We'd be better off getting him to a lab, but I wanted to try ruling out the obvious explanation first.

Starting with his head, I slowly worked my fingers through his hair, feeling along his scalp for bumps or cuts indicative of a head injury. As Deputy Colby had said, the easiest way to knock somebody out if you didn't care whether they ever regained consciousness was to hit them upside the head. It wasn't long before I found an injury site.

Moving Foster's hair aside, I found a strange wound. It contained several depressions arranged in a small circle, as if somebody had *stamped* this man's head with an injurious level of force. Confused, I looked up at Robinson and Colby. "I've never seen a wound like this. Have either of you?"

Colby shrugged. "Some kind of stamp, maybe?"

"No. Not a stamp." Dismay and anger warred in Robinson's expression as he leaned in to examine the wound for himself. "This is an arrow wound."

An arrow wound? But there's no penetration. "Did the archer use a blunt arrow?"

Robinson nodded. "I know this mark. It's made by a specific brand of arrowhead designed to stun or kill small game without penetration."

That reminded me of the reason Sheriff Robinson had originally gotten on my case. Somebody burglarized his house and stole his camera and his bow-hunting gear. "Are you sure somebody isn't trying to frame you, Sheriff? Somebody used your camera to photograph me, and now they used one of your arrows to knock this poor bastard out."

He actually *glared* at me for a second, as if I had accused him outright. "I don't like the idea, Adversary, but you're right. It's a possibility I ought to consider."

"Especially since the theft happened before I even arrived in Clarion, let alone started poking around the forest." That was an unsettling thought. "What if this killing isn't about my investigation at all? Maybe the perp had already planned to whack Foster and Wilson."

Rather than dismiss me, Robinson narrowed his eyes and began to pace. "That would leave us without a motive, Adversary. If we assume these killings are tied to Fort Clarion, we have a suspect pool. We can investigate Dr. Petersen and his associates, or go after the Dusk Patrol survivors. Take that away, and we're stuck starting from zero."

Now, where did I put that little violin? "You have my sympathies, Sheriff, but you saw the head injury inflicted on Charles Foster. What if a similar injury had been inflicted on Scott Wilson to prevent him from resisting his attackers? Given that you go bow-hunting with Dr. Petersen and Mayor Collins, they may well be possible suspects."

"You're not suggesting we check Wilson's head, are you?" Colby stared aghast at me. "We'd have to exhume him. His parents will shit themselves."

"What are they going to do, object on religious grounds? I'd like to see them try."

Robinson shook his head. "Yes, goddammit. They will object on religious grounds. Hell, Naomi, I'll be lucky if the magistrate doesn't tell me to go fuck myself. They go to the same church."

All right, that complicates matters somewhat. "Does it have to be a local judge signing the warrant?"

"I suppose I could go to Pittsburgh, but the Wilsons would still have the right to contest the warrant with Judge Ellsworth. We need to appeal to a higher authority." Judging from the exceedingly meaningful look Robinson and Colby aimed at me, that meant going to the Phoenix Society.

Which in turn meant a metric shitload of work for me, since getting a warrant that would trump any objections the Wilsons were likely to raise would demand that I offer a convincing case that the exhumation of Scott Wilson serves a compelling public interest overriding his parents' rights to the free exercise of their religious beliefs.

It took me four hours of the sort of creative thinking I haven't had to exercise since ACS, but I wrangled an exhumation warrant from the Phoenix Society. It seems they're happy to use me to investigate murders and the like while I'm in the area as long as I don't mention Project Harker. That's fine by me; I've already accepted that I'm on my own if I want to figure out what sort of shenanigans the Commonwealth Army got up to at the expense of its soldiers.

Unfortunately, Wilson's parents were there when we arrived at his grave to exhume him. Sheriff Robinson glanced at the grieving parents before raising his arm to stop me. "Is there any chance you can let me talk to them?"

It took a second to check the regs. I had to present the warrant, but they didn't say anything about letting local authorities act as an intermediary between me and the next of kin if any of them were willing to do so. "Go ahead, but I have to serve the warrant."

Robinson and the other deputies seemed grateful as he led the way to the parents, who knelt before their son's grave praying. He waited until they appeared to have finished the latest round. "Mr. and Mrs. Wilson?"

They rose, startled despite the Sheriff's gentle tone. They were my parents' age, though grief had aged them. It was Mrs. Wilson who spoke, though she looked at me first. "Is something wrong, Sheriff?"

The Sheriff took a deep breath. "I'm sorry, but the young snow-blonde with the sword is an Adversary. She's helping me investigate Scott's murder."

Mrs. Wilson nodded. "We'd be happy to answer any questions you or the young lady might have." She turned to me. "What's your name, Adversary?"

"Naomi Bradleigh, ma'am." I offered my hand, and she took it. "Sheriff Robinson, if you would?"

Robinson sighed. "Ma'am, we're not here to question you or Mr. Wilson. We found a wound on another victim that we suspect may also have been inflicted on your son, a wound that wasn't found during the autopsy."

Mr. Wilson's eyes flashed at the mention of Wilson's autopsy, and he began muttering. Though I had agreed to let Sheriff Robinson do the talking, I felt duty-bound to draw the father out. "Sir, can you please speak up? Is something bothering you?"

"'Twasn't right, cutting open our boy after he was dead. 'Tisn't God's will."

God talk already? That was quick. Quicker than I'd like. This can't possibly end well.

---

### This Week's Theme Music

Etude #3 In E, Op. 10/3, "Tristesse" by Frédéric Chopin

{% youtube mpiJbQvBP8A %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
