---
title: "*Nightmare Sequencer*, Track 01 &mdash; Ayreon: &quot;The Dream Sequencer&quot;"
excerpt: One of these days I have to write Claire's first big adventure.
date: 2015-10-17T14:03:25+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - Ayreon
  - Claire Ashecroft
  - computer guided lucid dreaming
  - lucid dreaming
  - novel
  - outtakes
  - prequel
  - Saturday Scenes
  - starbreaker
  - The Dream Sequencer
  - Starbreaker
  - Nightmare Sequencer
---
The first chapter of _Nightmare Sequencer_, a Starbreaker novel, is inspired by Ayreon's "The Dream Sequencer". This is a rough cut, so please comment.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A2c2mOsmEPcAsgwE6WQ9Je7" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

It was hard not to suspect something was up when the technician at InceptActive stopped me from using my usual creche for today’s test session. This was supposed to be a bloody literal dream job for a girl my age: test a new massively multiplayer RPG that used computer-guided lucid dreaming instead of virtual reality or the classic player-in-chair-staring-at-screen paradigm.

Even better, all the NPC enemies are AI-controlled, and the AI can tailor the game to challenge you in real time. Not to mention the network features were incredibly flexible. One person I invited into my game as an ally turned on me halfway through. An enemy who had invaded my game took issue with this, and fought beside me against my betrayer.

The reality wasn’t quite as attractive. It still wasn’t bad, since we got bragging rights, great pay, and the kind of wild parties normally confined to a university setting. Since more than one CGLD session per day was a bad idea because the brain needs at least one free REM cycle to handle background processing and cleanup, we only worked three hours a day.

IncestActive — what we play-testers called our employer in the privacy of our own heads because it was a cheap laugh — naturally found a way around the limit. They had to, since wanted as much play-testing done in as short a timeframe as possible with a bare-bones budget. Rather than leave the seventy-two creches allocated for play-testing, we worked in shifts and hot-bunked.

Each of them was named for one of the demons in the Ars Goetia, and were assigned by lottery. Valefor was my favorite. It seemed that the various couches had specific AIs dedicated to them, and I could always count on Valefor to provide fast-paced scenarios with frenetic action punctuated by weird plot twists.

Of course, I couldn’t always get the couch I wanted. Last week, I got stuck with one whose most recent occupant seemed to have been in dire need of medical attention. Poor bastard.

Thank Eris and all her twisted sisters I only had to deal with the lingering remnants of his fartulence, but the only explanation I could imagine was that Satan himself crawled up his arse and died, only to have half of Hell join him. As it was, I had to play while under the effects of a non-stop poisoned status that constantly sapped my hit points. Talk about real life intruding upon the game; it’s usually the other way around.

Knowing this, the presence of a tech at my assigned creche meant some serious shit had just happened. Furthermore, Forneus was already empty, which was unusual. I usually liked to arrive early, and say hello to my fellow testers as they came back to reality. You wouldn’t believe how many hook-ups I’ve gotten from other testers that way.

Fortunately, I knew Javier Martinez y Castillo. Nice guy, if you didn’t mind him staring at your arse. I certainly didn’t. “Why so serious, Javier?”

Poor bastard actually _crossed_ himself. Had no idea he was Catholic. Not that there’s anything wrong with that. “Something’s wrong with this rig, kid. Last kid to use it died in it. EMTs must wheeled him out the door just before you arrived.” He glanced around, as if afraid management might be listening. “Only reason I said anything is that I know you like to arrive early and chat up the other testers. We’re gonna get you a different one.”

“Bollocks to that, Javier. I was assigned to Forneus today, so that’s the one I’ll use.”

I was already sitting inside when he grabbed my arm. “Claire, come out of there. What if the last kid’s dream killed him? If you use the same rig, you might dream his dream.”

“And what if the last kid faked his medical records to hide a condition that made CGLD gaming dangerous for him?” That made him think, but he wasn’t convinced. “Look, mate, if I worried about what might go wrong, I’d still be a virgin. Strap me in and fire it up.”

“Your funeral.” Despite the gruff tone, he was gentle as he helped me get settled. Before closing the lid over me, he took my hand and gently squeezed it. “I’m gonna be right here the whole time. If something looks wonky, I’m gonna pull you out.”

“Thanks, Javier.” It was sweet of him to offer that last bit of contact before closing me off from the rest of the world. I’ve never told him how much it meant to me, or how nervous getting into one of these oneirogenic couches made me. He never knew that I was much younger, I used to have nightmares about being buried alive. Clawing at my coffin’s lid until my fingertips were worn down to blood-stained bone, my eyes bulging out and staring into nothing, my skin a cyanotic blue: that was the one way I _didn’t_ want to go.

Taking a deep breath, I raised a hand and held it in front of the vent that constantly pumped in fresh air. Not that the pump was necessary, but feeling the cool flow against my fingers made things easier.

The dark creche lid above me began to swirl and scintillate in hypnotic patterns reminiscent of a starry sky above an empty field that caught my attention and relaxed me. My eyes slipped closed as tiny speakers set into the creche played soft nighttime sounds similar to what our prehistoric ancestors must have heard back in Africa. The crackling of the campfire felt so warm, so real, that if I just turned over and opened my eyes I might actually get to watch the embers fade into hot coals slowly cooling.

“So, the stories were true.” A quiet voice caught my attention, and I sat up to see a cloaked and hooded figure tending to a fire between us. As the fire fed upon the freshly added fuel, its warm glow revealed the walls of a ruined temple, its frescos faded by age and the depredations of sunlight and rain pouring through a roof that might as well not be there. “It seemed too good to be true, that prayer and fire in this forsaken place might summon one of the Warp Riders from beyond our world. Brave soul who faces death unafraid, by what name might we call you?”

“You can call me Claire, right after you pull that stick out of your arse and remember how to talk like a normal human being.”

My summoner raised slim hands dripping with jewels and pushed back her hood. “That is hardly the sort of speech we expected from one of the legendary Warp Riders, the heroes of our eldest legends.”

“Good, because I ain’t one of ‘em. But since I’m here, why don’t you tell me why you need a bloody hero in the first place.”

“Crown Prince Astaroth has risen from Hell, and claims dominion over the world and all born in it. You are the only one who can defy her. ”

Wait a minute. This sounds like a standard dark fantasy RPG setup, but the logic’s already getting wonky. If Astaroth held dominion over this world, isn’t summoning somebody capable of kicking her ass an act of defiance? “This is a trap, isn’t it?”

The summoner began to laugh, and hid her mirth behind her hand. “Was I that obvious? No matter. _Day of wrath, day of burning, my enemy’s soul to ash—_”

Turns out I was armed after all. Had me a lion-marked katana, a Hattori Hanzo Special. Lucky me! I drew and slashed in the same motion, and the look on my summoner’s face as her head flew was bloody priceless. “Here’s a clue, fuckwit: if you’re gonna burninate me with magic, don’t announce it with a chant.”

I expected gravity to do its thing and pull my first enemy’s severed head to the stone floor. I was about to give it a good swift kick when it rose to face me. “Your effrontery will not go unpunished, Claire. I will follow you throughout your futile struggle, even if you should reach my master’s throne. I will exult as you are put to the sword for your defiance, and take your body as my own.”

Damn, she got ugly fast. Wait. It was _he_ now. And since he never bothered to offer a name, I’d have to come up with something. He reminded me of an old band war pig mascot with his teeth and tusks; or was that just because of the t-shirt I had picked to wear today? “Oi, Snaggletooth.”

“What?”

“If you wanted my body that badly you could have just asked. I’ll try just about _anything_ once.” Though with _that_ face, I’d have to be pretty desperate for novelty. “Hell, I might find a use for you even without your body.”

Snaggletooth danced away as I reached for one of his tusks. Pretty agile for a floating demon’s head. Guess he wasn’t into me after all. Sucks when guys play hard to get, doesn’t it? “Tell you what, Snaggy. Soon as we find a town I’ll buy you something to show there aren’t any hard feelings.”

Snaggletooth pouted, but floated obediently behind me. “It better not be a fucking hat.”
