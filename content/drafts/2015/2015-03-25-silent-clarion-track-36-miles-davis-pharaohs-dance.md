---
title: "*Silent Clarion*, Track 36: &quot;Pharaoh's Dance&quot; by Miles Davis"
excerpt: "Check out chapter 36 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Kaylee raised a full glass in greeting as I wove through the mass of patrons who had taken advantage of the fact that tonight's musical entertainment was a jazz combo instead of a heavy rock band by pairing off and dancing. Her beer overflowed as she waved the glass, and doused Michael, who had been nursing his own drink next to her and angrily tapping on a tablet. He muttered something that sounded a bit like, "Goddammit, Kaylee. Sit down."

She didn't hear him, but instead waved more enthusiastically. "Hey, Naomi! What took you so long?"

"Sheriff Robinson detained me."

"Oh yeah?" Kaylee thumped her pint down on the table, splashing what remained. "Whafuck? This was full a second ago."

"Yeah, and now I'm wearing it." Michael ran his hand through his wet hair. "You can have mine. I already smell like a brewery." He pushed his tablet across the table to me before rising. "Can you keep an eye on this, Adversary?"

"Of course. Will you be back?"

He shrugged. "I'm just ducking into the men's room to clean up."

Kaylee leaned toward me as Michael circumnavigated the dancers. "Have you seen the body on that kid? You really should get yourself a piece of that before you leave."

"I can't if I'm going to recommend him when he applies to become an Adversary. Maybe if I were recruiting for Xanadu House."

"I bet you've never even been to a Xanadu House. At least, not as a customer. You're too prim and proper."

Me, prim and proper? Try telling that to Christopher Renfield. "You got me. I was investigating allegations of wage theft."

"Always on the job, eh?" Kaylee tried Michael's unwanted beer, and grimaced at the taste. "What the hell is this crap?"

"Not your usual, eh?" Though I was tempted to wake up Michael's tablet and poke around, it wasn't mine and I didn't have probable cause, let alone a warrant. "Did you know Scott Wilson or Charles Foster?"

"Scott and Charlie?" She wasn't quite slurring her words, but it was close. "Yeah, I knew those boys. They were nice boys. You would've liked 'em."

"Do I dare ask how?" Considering that Kaylee was happy to brag about Michael's prowess, I half-expected her to count the victims among her conquests.

"Nothing sordid, I promise. They often joined my weekly Catacombs & Chimeras campaign sessions. And… I think they were lovers, but they were discreet about it. You don't think that was why they were killed, do you?"

Though I made a note to check for anti-queer sentiment among the locals, I didn't think it likely. Not when I saw queer couples slow-dancing and letting their hands wander with the same disregard as straight couples  . If hate-motivated violence was prevalent here, they'd probably be more circumspect. Besides, why would their sexuality have made them more likely to catch Dusk Patrol's attention? "It would be premature to assign a motive to suspects I haven't identified yet. Any info that you and Michael could share about Wilson and Foster might help."

"Mike could tell you more. They were friends. Them and some other guys. Like Ernest Yoder. He's a couple years older than the others, and lives alone on the edge of town. He's kinda reclusive, and I don't think anybody's seen him since the day you arrived."

I took a deep breath. "Yoder's missing?"

Kaylee shrugged, and glanced around as if she wanted to be sure nobody was eavesdropping. She leaned forward until her lips all but brushed my ear. The smell of beer nauseated me, and I messaged Halford for a pot of coffee. "I doubt anybody knows for sure. Here's the thing, Naomi. His parents died when he was ten, after which he bounced from one foster family in town to another like a hot potato until he was eighteen . Mr. Yoder was a wife-beater, and he went too far. I was with the militia, but we got there too late to stop him. He's been pretty much on his own ever since."

"At least you were able to save Ernest. Did his father murder Mrs. Yoder?"

Her expression had become grim as she recounted the story. "We thought he had. She looked beaten to death when we got to their barn, but when he hit Ernest, she got up. If it wasn't so damn miraculous, you would have thought she was a zombie from the damage he had done to her."

"What happened next?" It was still unclear what bearing this story had on Ernest Yoder's apparent disappearance the day of my arrival in town, but now I was curious.

Kaylee tried Brubaker's warm beer again, but it had not improved in her estimation. "We figured she'd try to shield her son with her body, but she didn't."

"Did she attack her husband?"

"Attack him?" Kaylee shook her head as if my words were a woefully inadequate guess at what happened next. "This ain't no shit, Naomi. She took a ten kilo sledgehammer, screamed like some kind of samurai banshee, and pulped his fucking head with one strike."

Oh, come on. You don't see such feats happen in real life. Hell, it's rare enough to see them in fiction that isn't outright fantasy. Though I wanted to dismiss Kaylee's anecdote, the implication that she served in the militia and her usage of the phrase *this ain't no shit* gave me pause. When an Adversary says that, it's tantamount to an oath sworn by the river Styx. It's practically sacred. "One blow. She killed him with one blow, in her condition?"

"I saw it with my own eyes. It's recorded. It fuckin' happened. She dropped him with one swing, and then kept swinging until she dropped dead. Dr. Peterson said she might have lived if she had stayed down, but seeing Mr. Yoder hit Ernest must have triggered some kind of berserker rage. We used a hose to get what was left of him out of the floor."

"Poor kid. I suppose he's fucked in the head." Which is a horrible thing for me to say, but I couldn't stop myself.

"No shit." Kaylee drank half the vile brew before her with a grimace.

I took the glass from Kaylee. "Tell me what happened to Ernest Yoder."

"I didn't let him see his mother keep pounding on the old man, but the whole town heard her screaming. I think he's been to a hundred different psychotherapists, and none of them could help him." She stopped as Halford came by with mugs and a fresh pot of coffee. "Thanks, Bruce."

"You're welcome, but don't bogart the pot. Save some for Adversary Bradleigh." He winked at me before returning to the bar.

Instead of continuing her story, Kaylee nursed her coffee. It wasn't until she had finished the cup before she spoke again. "Ernest is afraid of women, which is why none of the families in town kept him for long. He's afraid all women have the hidden capacity for violence that his mother displayed in his defense. And he's afraid of himself, that he's just like his father. So he lives alone, only comes out at night, and then only rarely."

Now I get why nobody worried overmuch about Ernest Yoder. He's the town hikikomori, the reclusive loner who spends most of his life holed up in his home. "Hasn't anybody thought to check up on him?"

"I've wanted to, but I haven't been able to persuade Sheriff Robinson that it was worthwhile to get an entry warrant. After all, he's an adult, and he's done this before." Kaylee shrugged, and flashed a wistful smile. "When the last True Goddess Metempsychosis game came out, he picked up his copy after hours. I didn't see him again for three months."

"What about electronic communications?" Reclusive people might avoid face-to-face social interactions while engaging in a rich and varied social life mediated by the network. "Do you know if he frequents any particular network forums?"

"Other than the town bulletin board?" Kaylee shrugged. "I don't frequent it myself. Cat's husband runs it; he's the town guru."

That sounded about right, since he claimed to have contributed patches to HermitCrab. I got up after finishing my coffee. "Then I might have to consult him."

But before I spoke with Cat's husband, I should check up on Brubaker. Where the hell is he? Does the men's room have a queue? Also, maybe I ought to mention Yoder to Robinson. He just might know something.

"You're going?" Kaylee swayed as she stood, which worried me. She was probably drunker than she realized. "Oh, man. Those three shots of whiskey were definitely a mistake."

God, she was as bad as Jacqueline. "Can you get home?"

"Uhm… maybe?"

She stumbled while taking a step, but I got to her in time to keep her from falling. "Come on, you. You're coming upstairs with me."

"But I'm not into girls."

She was definitely as bad as Jacqueline. I should introduce them. They could go on pub crawls together, and I could stay home and practice my piano for once. "Neither am I. But you can have a nap while I poke around the town BBS."

Once I got Kaylee settled, I poured myself a mug of coffee from a pot Bruce had been kind enough to send up to my room and fired up HermitCrab on my loaner laptop. Once I was on the network, I found Clarion's town forum and browsed the topic list. Nothing in particular stood out, so I tried searching for 'Fort Clarion'. I found a couple of threads with a fair amount of chatter, but they had been posted after the first day I led volunteers there to catalog the base's equipment, and everybody participating used their real names.

A search on 'Ernest Yoder' didn't turn up anything useful. The town forum was a virtual bathroom wall where nobody had anything good to say about anybody else, but at least the Mayor's smear campaign against me was entertaining. Whoever did PR for Mayor Collins needs a nice fat raise for making that clown into a martyr facing crucifixion by the high-handed Phoenix Society and their sluttish agent, yours truly.

The threads buzzing with ever-more-improbable conspiracy theories concerning the current deaths and recent tourist disappearances were also amusing. Some of them suggested that the victims had been brought into Fort Clarion for use as sacrifices in some kind of Black Mass, which struck a bit too close to my own suspicions for comfort.

And I'd have to save copies of the pornographic fanfics featuring me as some kind of vampire dominatrix. Each proved more devoid of literary merit than the last, but Jacqueline and I could read them aloud for a laugh while drinking to excess.

If this forum had any useful evidence, I wouldn't find it by using the standard interface. I opened HermitCrab's relational database query tool, pointed it at the forum's location, and aimed it at the standard TCP ports for database servers. It found one, and automatically set about cracking the admin password.

The connection cut out, and my laptop crashed. After restarting the machine and logging back in, an incoming mail notification popped up with a subject line consisting of two words: "Bad Kitty". It was a message from the town's admin, consisting of a single sentence: "Next time you want to poke around my server, get a goddamn warrant."

So, Cat's hubby wants me to get a warrant? He'd better be careful what he wishes for, because he might just get it. Since Kaylee's hogging the damn bed, I might as well fill out the forms now.

---

### This Week's Theme Music

"Pharaoh's Dance" by Miles Davis, from *Bitches Brew*

{% youtube ycSAGSO1AI0 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
