---
title: "I Know I'm Fat. Did You Know You're an Asshole?"
categories:
  - Rants
tags:
  - "Live and Let Die"
  - bullshit
  - diet
  - exercise
  - fat acceptance
  - fat-shaming
  - get the fuck out of my way
  - "I know I'm fat"
  - leave me the fuck alone
  - nutrition
  - obesity
  - research
  - shut the fuck up
  - unsolicited advice
  - weight loss
  - yo-yo dieting
---
That's right. I know I'm fat. I've been fat my entire life. Barring _radical_ changes in my lifestyle that I lack both the money and the inclination to make, I will most likely die fat. I might even die of heart disease if the cancer plaguing my father's side of the family doesn't get me first.

The _real_ question is, do you know you're an _asshole_ for bringing up the subject? No, you don’t need to get your eyes checked, but I’ll explain it for you just to make sure you get my point. But let’s make one thing crystal fucking clear. This isn’t about _fat acceptance_. 

I don’t want your _acceptance_. I want your _silence_. Just shut the fuck up, get the fuck out of my way, and leave me the fuck alone. Like the song goes, just **live and let die**.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Got it so far? Moving on, then. There is a limited set of circumstances in which my weight is any of your business:

  1. You're my spouse, and my weight negatively impacts our sex life.
  2. You're my doctor.
  3. You're a nurse or physician's assistant working for my doctor.
  4. You work for the DMV and renewing my driver's license.
  5. You need to calculate what else you can pack into the payload of the rocket lifting my ass into orbit (in which case my mass is more relevant than my weight).
  6. You're the goddamn Batman.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

If you ask a large person about their weight or comment on it for _any_ other reason, you're an asshole. That's all, folks. Case closed. End of fucking story.

Don't tell us you care; we don't believe you. Don't tell us you're trying to help; we didn't ask for your help. Don't give us any shit about health insurance premiums; we pay them too. Don't say "I'm just saying"; instead, just say _nothing_.

But maybe you don't know _why_ you're an asshole for commenting on another person's weight, which is most likely none of your goddamn business. Normally, I wouldn't bother to explain myself. I would settle for staring at you until you back off while fantasizing about attending your funeral &mdash; which would be a closed-casket affair.

However, I'm feeling generous today. I also promised a young lady at my publisher that I'd blog more, so I'll enlighten you.

## We Know We're Fat

In case you haven't noticed, we live inside our bodies. We are well aware of how much we weigh and how much space we take up. We do not need, and certainly don't appreciate, constant reminders.

Quite frankly, your comments, opinions, and "helpful" advice are little different from the insults you used to hurl at fat kids when _you_ were a kid.

You aren't telling us anything we don't already know, unless we previously labored under the mistaken assumption that you are a civilized, humane person.

## We've Tried to Lose Weight

Do you have any idea how much the [diet food industry](http://www.theguardian.com/lifeandstyle/2013/aug/07/fat-profits-food-industry-obesity) makes at the expense of people like us? That industry's annual revenue, thanks to the insecurities and self-loathing inspired by assholes like you, is best measured in _metric fucktons_.

Any heavy person in the United States can tell you stories of the weight loss methods they've tried, many of which were based on current research, only to lose a few pounds despite months of effort. Simply eating less and moving more doesn't work, and more complicated methods [turn out to be bullshit](http://www.businessweek.com/debateroom/archives/2008/03/the_diet_industry_a_big_fat_lie.html).

We try. We fail. We try again, hoping this time we'll get it right. It's not as easy as you think, even if it _was_ easy for you.

## It Isn't About Motivation

You think being heavy is _fun_? Here's a clue, you ignorant shitcunt: **it isn't**. Even if we didn't have to deal with the inability of others to mind their own business and keep their judgmental comments and unsolicited advice to themselves, it's still not easy.

In case you haven't noticed, the world isn't made for _us_. We don't _fit_. At least, not easily. We pay more for clothes, have a more limited selection (which is especially pronounced for women), and many of us would _happily_ pay for two seats on an airplane rather than deal with you.

We don't choose to be fat. It isn't _fun_. Some of us put on a good front and try to make the best of it, but it isn't as easy as you think it is. We don't just gaily waddle through life oblivious to our size and shape. We are reminded on a daily basis, if we're lucky. Otherwise, we're reminded several times an hour.

## Helping Us Isn't as Easy as You Think

Even if we wanted to change our bodies, either because we don't _want_ to be heavy, or because we want to appeal to others, it's a non-trivial process. It's a long, arduous journey that we have to make _on our own_.

Unless you're a physician who specializes in human nutrition and human metabolism, and keep up with the latest research, you aren't qualified to help us. Your best bet is to stay out of our way, but if you simply _can't_ mind your own business, keep reading.

## What You _Can_ Do to Help

To start with, you can stop being a judgmental, meddlesome asshole. That alone would be a _huge_ help. No more jokes, no more questions about our weight, no more reminders, and no more unsolicited advice &mdash; _especially_ if you're not exactly Hollywood pretty yourself.

And, no, you don't get to make fat jokes just because _I_ joke about being fat. I can joke about being fat for the same reason black comedians and rappers can use the &#8216;n' word; they're reclaiming a word used to demean them.

So, yeah. I'm fat. I'm fucking _huge_. I ate the last idiot to give me shit about it and washed it down with a _tanker_ of Diet Coke. I can make jokes like that about myself. _You_ don't have that privilege.

You think I should eat less? _Fine._ You do the cooking, then, and serve what you think are appropriate portions. Once we get used to eating smaller portions, we can take it from there.

Also, don't push me to eat when I tell you I'm not hungry, and give me shit about how it will "fuck up my metabolism". Unless you're a scientist specializing in human metabolism, you aren't qualified to make such statements.

Oh, and instead of _telling_ me to exercise? How about inviting me to join you, and keeping in mind that I might not be able to keep up with you at your best. Maybe invite me to the gym, spot me on weights, and speak up if you see other people being dicks about the fat guy who can't lift as much as the lifelong bodybuilders.

Speaking of bodybuilders: don't assume that because we're fat we don't exercise at all, but just sit on our asses all day eating Cheesy Poofs. It's entirely possible to build muscle without losing all the excess bodyfat.

Case in point: the guy in the picture up top is Wilson Fisk from the Netflix production of Marvel's _Daredevil_. He's my new favorite Marvel villain. Why? Because he's a fat bastard who doesn't take any shit from _anybody_. You fuck with him or somebody he cares about? He will fuck you just as hard.

Feel free to share your experiences with being large/heavy in the comments below. If you're here to troll, however, keep in mind that I ban first and ask questions later. Much later. As in the first Tuesday after Ragnarok.
