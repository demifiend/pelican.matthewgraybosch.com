---
title: "*Silent Clarion*, Track 55: &quot;The Dark Eternal Night&quot; by Dream Theater"
excerpt: "Check out chapter 55 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Renfield must have heard our pursuers as well, for his expression hardened. He glanced at Mike, who was checking his rifle with a frown that suggested deep concentration. Grabbing Mike's shoulder, he gave the younger man a hard shake. "Hey, kid. We gotta move."

"I'm ready." Mike stood and slung his rifle over his shoulder. "Naomi, you'd better take point so those shriekers don't go off in our faces."

"Right."

"You deployed Mandrake mines in the tower?" Renfield trotted beside me as we headed for a stairway to the surface. The way he winced as he mentioned the devices suggested a bad experience with them in the past. "Christ, I hate those fucking things."

"Trouble deactivating one without the phone used to prime them?"

"Yeah. Lost my legs when the fucker went off."

"Now you're just taking the piss." He had to be having a bit of fun at my expense, given that I had gotten my hands all over his naked body. If his legs were artificial, then they're a bloody work of art. "You honestly expect me to believe you had your legs blown off?"

Renfield shrugged mid-stride. "One of the few upsides to Project Harker. I grew 'em back. Took me most of a year, and you wouldn't believe how bad the itching was. Nurses had to cover 'em in gauze to keep me from clawing the new skin to ribbons."

My turn to wince. "That must have been rough."

"That wasn't the worst. By the time it was over, I had a hell of a Demerol jones. Not much else to do when your locked in infirmary like a goddamn science project."

Though I should have saved my breath for running, I couldn't resist a final question. "How did you get over the Demerol?"

Because he ran beside me, I couldn't tell if he had shrugged. "Once I was judged fit for duty, it was easier. Life wasn't just about getting my next fix anymore, and after a while I stopped needing that shit."

That got me wondering as we reached the surface, and carefully closed the door behind us to avoid unnecessary noise. How may other addicts and obsessives ended up where they were because they had nothing else in their lives that mattered to them? Such questions were the province of scientists, so I put it aside and loaded the app I used to control the Mandrake mines. A quick status check showed that all remained active. ｢Stay behind me. My implant will be broadcasting to the mines I armed, telling them not to go off but —｣

｢But if we get ahead of you — boom.｣ Renfield chuckled, and glanced at Mike. ｢Better watch your ass, kid. You get your legs blown off, they're gone forever.｣

A soldier on patrol spotted us halfway to the watchtower. Raising his rifle, he narrowed his eyes but did not immediately open fire. "That you, Renfield?"

Renfield stepped forward. "You can stand down, Specialist Catherman. They're friendlies."

"You sure about the broad? Didn't she take out —"

"Yeah, but it was self-defense. Jackson and Munoz went after her without orders."

"Our standing orders —"

"Our standing orders do not apply when a ranking officer is in the field, Specialist. Remember?"

Specialist Catherman lowered his weapon. "So, what's the plan?"

Renfield glanced at me. "Sergeant Robinson or one of the men following him killed Colonel Petersen. We're fighting them. If you don't want to fight beside us, I understand, but if you get in the way we'll take you down. Tell anybody else who hasn't picked a side."

Catherman nodded, pulled a walkie-talkie from his belt, and spoke a rapid stream of Navajo. Too bad I could only recognize the language, and not understand it. Guess I should have gone for that code-breaking training after all.

He waited a moment for a response, before meeting Renfield's waiting gaze. "We ain't gonna fight for your girlfriend, boss, but if any of Robinson's butt buddies come after us, we'll make 'em pay."

Stepping forward, I offered Specialist Catherman my hand. "Thank you. In exchange, I would like to promise that I won't strike killing blows against anybody but Robinson."

That surprised him. "Going right after the boss, huh? That's how we prefer to do things. Why dick around with pawns —"

"When you can go straight for the king?" I finished the question, which I remembered from training. No doubt somebody in the Training Corps had once served in the Commonwealth military's special forces. Drawing my sword to show I meant business, I let Catherman take a good look at the blade. "I do intend to go directly after the Sheriff, but anybody who gets in the way is going to get hurt. How badly depends on how badly they piss me off."

Catherman nodded, his eyes fixed on the gleaming edge. The wicked smile of a man looking forward to payback bared his teeth. "Sarge, I'll be in the East Tower enjoying the show."

Renfield pointed toward the tower, and spoke in a near-whisper. "We could make a straight run, but that might not be a good idea."

His meaning was obvious. The buildings we would pass on the way could conceal several ambushes. Despite the risk of giving Robinson's men a chance to catch up, the safest way forward was to approach each corner and check. Though I wanted to run to the next intersection, I advanced a slow, silent step at a time while drawing my pistol and stopped short of the corner to listen. Nothing. ｢I'll peek around the corner once you guys join me.｣

They crossed the block at the same careful pace I used, and crouched behind me. Leading with my pistol, I leaned out to get a look down the street intersecting our road. Nothing, but it didn't pay to underestimate them. ｢Let me cross first, and see if I can draw them out.｣

This time I made no effort at stealth. Instead, I dashed across the street heedless of my footsteps. If they were there, I wanted to draw them out. Stopping at the other side, I peered around the other corner. Still nothing. ｢Clear.｣

Mike looked impatient as he texted. ｢Don't you think you're being a bit paranoid, Naomi?｣

｢Yeah, but there's plenty of time to yell, "Come get some," after I've gotten you and Renfield up that tower.｣

Renfield smiled, and gave Mike a gentle punch in the shoulder. "You could do worse than that for a girlfriend, kid."

｢Did I miss something?｣

Mike blushed. ｢Nothing.｣

Nice to see the guy still has a sense of humor. ｢Come on.｣

We continued our advance, for all it felt as though the entire night was slipping away in the silence of our slow, stealthy progress. It was almost nine when we finally reached the tower. Another nine hours to go before my backup was scheduled to show, and I was already tired. I was going to need a vacation from my vacation.

The Mandrake mines settled into dormancy as I used my implant to disarm them. Holding open the door, I beckoned them inside. ｢Up you go, gentlemen. You're going to cover me, and I'll keep their attention where it belongs.｣

Renfield raised an eyebrow. ｢On your tight ass?｣

No way in hell I'd dignify that with a response, though I daresay Jacqueline would have had fun with such a line. ｢Just focus, Sergeant. Up you go. We've a long night ahead of us.｣

Sauce for the goose being sauce for the gander, I made sure to ogle him as he followed Mike up the stairs, and waited a decent interval before re-arming the shriekers. ｢Tower entrance secure. You should hear some fireworks if anybody tries to come up after you.｣

Mike texted a couple minutes later. ｢We're in position. Not seeing any hostiles, though.｣

Shit. Where were those bastards? Robinson and his followers dogged our heels as we ran down the tunnel from Clarion, but now they're nowhere to be found? That doesn't make any sense. ｢Gevurah, are you there?｣

The AI named after a node of the cabalistic Tree of Life representing severity was nowhere near as friendly as Malkuth. ｢State your business, Adversary Bradleigh.｣

｢Malkuth said you could put a satellite over Fort Clarion and feed me info on enemy movements. How long would that take?｣

｢There is already a satellite overhead. I should be able to access its targeting systems.｣

Targeting systems? That was ominous. ｢Do I even want to know what sort of military satellite is overhead right now?｣

Malkuth would have indulged in a bit of sarcasm and told me that I'd sleep better if I didn't know. Binah would have gone literary and called my question one of those Lovecraftian situations where ignorance is sanity. Gevurah gave it to me straight. ｢The GUNGNIR platform is currently in a geosynchronous orbit over Fort Clarion and the vicinity.｣

｢GUNGNIR? Are you shitting me? Whose bright idea was that?｣

｢You are not cleared for that information.｣

My reply hit the network before I could think twice and stop myself from sending it. ｢Am I cleared to put a boot up your arse?｣

｢What exactly does Malkuth see in you? It can't be your winning personality.｣

Oh, I'll give him winning personality once I'm done here. ｢Sorry, Gevurah, but I've been hearing that a lot lately and it's getting a bit tiresome. Next time I ask a question, could you find some other way to deny me?｣

｢No.｣

Well, it was worth a try. ｢Fine. Are you getting any intel from GUNGNIR?｣

｢Its sensors report no humanoid life other than yourself, your companions, and an individual in the east tower. You would be well-advised to find a secure location and rest. Get some sleep, if you can. I will wake you when hostile forces approach Fort Clarion.｣

Oh, sure. Like I'm going to curl up somewhere cozy and have a catnap while there's a sodding weapon of mass destruction overhead. ｢Thanks, Gevurah. Keep me posted.｣

---

### This Week's Theme Song

"The Dark Eternal Night" by Dream Theater, from *Systematic Chaos*

{% youtube XL4iZVCBgQk %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
