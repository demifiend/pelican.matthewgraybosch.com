---
id: 2447
title: '#straightoutta Melnibone: Elric Represents'
date: 2015-08-14T17:45:05+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=23
permalink: /2015/08/straight-outta-melnibone-elric-represents/
sw_cache_timestamp:
  - 401639
bitly_link_linkedIn:
  - http://bit.ly/1Pj1YG4
bitly_link:
  - http://bit.ly/1Pj1YG4
bitly_link_twitter:
  - http://bit.ly/1Pj1YG4
bitly_link_facebook:
  - http://bit.ly/1Pj1YG4
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Stuff I Found
tags:
  - elric
  - 'elric: the balance lost'
  - melnibone
  - meme
  - michael-moorcock
  - public enemy
  - straight outta compton
  - straightoutta
format: image
---
Frankly, I'm surprised nobody else thought to give Elric of Melnibone the [#StraightOutta](https://twitter.com/search?q=%23StraightOutta) treatment. Guess I might as well do it myself, since all advertising is fair game for parody. 🙂

The artwork came from [_Elric: The Balance Lost #9_](http://graphicpolicy.com/2012/03/10/preview-elric-the-balance-lost-9/).