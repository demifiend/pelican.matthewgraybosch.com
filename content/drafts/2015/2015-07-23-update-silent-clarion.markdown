---

title: "Update: Silent Clarion"
modified:
categories: starbreaker
excerpt: "Current word count for Silent Clarion: 103,128"
tags: [silent-clarion, progress, kindle, serial]
cover: silent-clarion-new-banner.jpg
date: 2015-07-23T21:31:32-04:00
---
With Episode 2 of the Kindle serial for *Silent Clarion* in the can, I'm free to return to Chapter 56. Got about 1,100 words down today for a total of 103,128. Some writers might consider such a low word count to be dicking around, but I edit as I write.

Yes, it's a bad habit. I know. I don't give a shit, because my inner editor is also the imaginary violent psychopath with my home address and a tire iron autographed by Tonya Harding who will be maintaining my code after I leave the project for a new one.

Yes, I probably picked the wrong day job.

![A snapshot of my day's work.](/images/typed-silentclarion-20150723.tiff)

This chapter isn't going the way I planned it. I figured that Dusk Patrol would fuck around with Naomi, keeping her on her toes but not engaging her until dawn came without any backup for her. She'd be tired, frazzled, and without support&mdash;and that was when the enemy would strike.

Things are working out differently, though. Instead, she's taking the fight to the enemy, and it looks like she's going to get hurt bad at the end of the chapter. As if the odds aren't uneven enough in a fight between her and Sheriff Robinson&mdash;who was augmented by Dr. Petersen to possess similar regenerative capabilities to a Dusk Patrol soldier.
