---
id: 3425
title: Happy New Year from Matthew Graybosch
excerpt: So, is everybody fuckin' wasted yet?
date: 2015-12-31T00:33:14+00:00
header:
  image: new-year-2016.jpg
  teaser: new-year-2016.jpg
categories:
  - Personal
tags:
  - 2016
  - New Year
  - planning
  - site refresh
---
{% include base_path %}

2015 is almost over. I don't know about you, but I'm almost glad it is. I'm tempted to dismiss it as a not-so-great year for me, but if every moment of every year was one in which I was always happy and got everything I wanted, what would stand out?

After all, I got to discover [some excellent music](http://www.matthewgraybosch.com/2015/12/discovery-playlist-albums-i-missed-in-2015/). When my wife needed to fly to Australia because her father had become suddenly ill, I was able to do so. I rejoined the ranks of the salaried, and now work for Deloitte Consulting as a "solution engineer". More importantly, management is pleased with my work.

Furthermore, despite some crushing and demoralizing overtime, I was able to finish writing [_Silent Clarion_](/starbreaker/novels/silent-clarion/) and complete the web serialization. The Kindle serial will conclude in 2016, and the complete novel will come out in paper and electronic editions soon after.

Finally, I've finally settled on a suitable starting point for the part two of **Starbreaker**, [_Blackened Phoenix_](/starbreaker/novels/blackened-phoenix/). I have a good rough plan for how the story's going to work out, and you're in for some surprises. But if you want to see what happens as I write the book, you're in luck.

You see, I've come to a decision. I've been trying to promote my work as a writer by blogging, and being fun and witty on social media. However, being a good blogger is different from being a good author. You're supposed to pick a topic and focus on it. You're supposed to become a "thought leader" and provide "evergreen content".

Well, screw _that_. I'm a long-haired metalhead who writes science fantasy. WTF am I going to be a thought leader in? Writing sci-fi for metalheads? And how am I going to do _that_? Write long-winded blog posts trying to "establish my authority" when hardly anybody knows my work?

Instead, I'm going to share the rough cuts of my writing. What's more, I'm going to share the original [_Starbreaker_](/starbreaker/novels/the-original-starbreaker/) over the course of 2016. This was the novel that serves as the basis for my [Starbreaker series](/starbreaker/), which I completed in 2009. If I share two or three posts a week, it'll take about a year to share them all.

So I hope you stick around, if only to see how it all began. In the meantime, have a safe and happy new year in 2016.

![Happy new year, motherfuckers!]({{ base_path }}/images/new-year-2016.jpg)
