---
title: "*Silent Clarion*, Track 57: &quot;I Shot the Sheriff&quot; by Bob Marley and the Wailers"
excerpt: "Check out chapter 57 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
When consciousness returned, I was sprawled on the ground with a gunshot wound roughly below where one of my kidneys should be. It didn't look bad. Hell, it looked partially healed, which shouldn't be possible. Sure the exit wound would be far worse, I tried to reach around to check, but my right arm didn't quite work right. And was that bone sticking out of the flesh?

After consciousness came sensation, which consisted mainly of pain unmatched by any prior experience. This was the first time I had ever suffered serious injury, and sweet unholy mother of bloody fuck it hurt. Despite my training, I was a mewling wreck sprawled across the pavement.

Never mind nailing Robinson to a wall. Never mind exposing Project Harker and Ian Malkin. Never mind protecting Mike and helping Renfield and the survivors of Dusk Patrol rejoin society. All I wanted was for my suffering to stop. If Robinson showed up and offered to finish me off…

Wait. Robinson was the son of a bitch who shot me. What the hell did that arsehole use that had enough punch to knock me off the roof, a goddamn anti-tank rifle? Whatever it was, he's probably laughing his sick fucking arse off while I'm here feeling sorry for myself instead of tending to my injuries.

First, the gunshot wound. Though it didn't look as bad as it should, if I didn't do something about that soon, I would be a dead woman. Opening my jacket and blouse one-handed, I lifted my camisole and steeled myself to assess the damage.

Because I had seen such wounds before, I knew what to expect. What I actually saw was impossible. The hole Robinson's shot had punched through my belly was filling itself in. It was closing. It was healing. Sure, Petersen had claimed that I possessed asura potential, but the regenerative capabilities displayed by Dusk Patrol required additional treatments that I had not received.

Pulling on my broken arm, I gritted my teeth and did my best to set it back into place. Once I had done so, the gash where the fractured bone had torn through closed itself. My sword arm was usable again in a few minutes, though I'd want to see a doctor afterward. So were my belly and back. I didn't even have the scar from my knife wound any longer.

Taking out my compact, I looked for the scar along my jawline where Maestro had cut me for the first and last time, at the beginning of my training with him. It too was gone, my skin as pristine as when I first agreed to become an Adversary.

The implications of what was happening hit me almost as hard as my hunger. No, I wasn't merely hungry. That was too polite a word. I was ravening—for fresh meat and for revenge. Sheriff Robinson no longer had a future, and his funeral would be a closed casket affair. Mourners please omit flowers.

First things first, though. Better ping my witness. ｢How are you holding out, Mike?｣

｢Getting a bit low on ammo. I've been keeping these donkey-raping shit eaters at a distance. I wanted to come to you, but Renfield decked me when I tried to get past him. He said you couldn't possibly survive. He told me—｣

｢Looks like I'm down to eight lives. How long was I out?｣

｢Almost five in the morning.｣

That long? Shit. How did I not bleed to death? Was it that healing ability I shouldn't possess because I hadn't been subjected to the same treatments as Renfield's band? Forcing myself to my feet, I rearranged my clothes and put myself back together. ｢Any word from the Phoenix Society? We were waiting for backup, remember?｣

｢Nothing.｣

Great. ｢Malkuth, where's my cavalry?｣

｢Nims! Holy shit, I didn't think you were gonna make it. Didn't they get to you and patch you up?｣

Something's wrong with this picture. My backup hasn't arrived, but Malkuth thinks they got here and provided first aid. He must have told them to get here on the double. ｢What was their updated ETA, Mal?｣

｢They were supposed to get there ASAP. What happened?｣

Looks like I can't just take Robinson's head and drop-kick it across the continent. Not until I've beaten some answers out of him, anyway. ｢I'm going to find out.｣

"There she—"

A series of explosions coming from the direction of the armory interrupted the lucky soldier. Looks like somebody finally found my little surprises. Taking advantage of my enemy's shock, I drew my sword and opened his thigh to the bone with a quick slash.

Leaving the fallen soldier, I took advantage of the fact that the remnants of Dusk Patrol seemed to be rushing to put out the arsenal fire. Unfortunately, I got no farther than the barracks before becoming light-headed as hunger pangs threatened to double me over. Should have searched that guy for rations.

The barracks wasn't locked, but that wasn't necessarily a good sign. What if it was a trap? No way I'm going in there blind. Instead, I leaned against the wall, taking deep breaths until my wooziness departed. ｢You watching, Gevurah?｣

｢Only through the base CCTV array. GUNGNIR has been moved. Don't bother asking who moved it or why. You aren't cleared.｣

｢Is there anybody inside the barracks? I could use a snack.｣

The reply came back a few seconds later. ｢It's clear. I checked the recordings as well. Nobody has been inside all night.｣

｢Awesome.｣ Raiding the pantry, I grabbed a couple packets of beef jerky and filled a canteen from the tap. Not the healthiest of breakfasts, but it was easy protein. I ate while checking over my rifle; it didn't appear to have been damaged in my fall, which was good because every round in its magazine had Robinson's name on it. ｢You still holding out, Mike?｣

｢Are all Adversaries' missions this fucked up?｣

Now there was a thought. Were there Adversaries who had it worse than I did right now? I bet Edmund Cohen could tell a hair-raising story or three. ｢I'm sure some are worse.｣

｢Great. Just fired my last round for the DMR. Gonna borrow Renfield's M16. Where the hell are you?｣

｢On my way to finish this.｣

"Hey! What the—"

Always when I'm eating! I squeezed off a burst from the rifle I held in just one hand before ducking behind the counter. The other was busy stuffing jerky into my mouth.

"Hey, guys! The bitch is in here eating our chow!"

｢Malkuth! Barracks floor plan! Show me a back door now!｣ A flashbang rolled under the counter while I sent the message, and I threw it back just in time; men screamed as the grenade went off in their faces.

｢This way.｣ A floor plan appeared in one corner of my vision, with a bouncy little arrow pointing the way just like in a video game. Not funny, Malkuth!

Soldiers pursued me as I ran, but I didn't dare stop to deter them. Nor did they seem keen on actually catching up to me. Instead, as more of their fellows joined up, they seemed concerned mainly with herding me. ｢Malkuth, can you guide me to Robinson?｣

｢Yeah. This way.｣

"Out of my way, arseholes!" Following the bouncing arrow, I surprised a small pack of soldiers with a sustained burst from my rifle. Before they could recover, I had bulled my way past them.

Minutes later, I passed the front gates and stopped short. Robinson leaned against the guardhouse with a pistol in hand. Kneeling beside him, with their hands bound behind their backs, were Mike and Renfield. The son of a bitch grinned at me as if he hadn't shot me, and wasn't holding two people hostage. "So, Doc Petersen wasn't full of shit after all."

"Never mind that. Where's my backup, and what the fuck are you doing with Mike and Renfield?"

"Backup? You silly bitch, did you really fall for that? Here's a clue for you—the Phoenix Society knows all about what's been happening here, and now you're part of the experiment." Robinson's smile broadened. "I've wanted an excuse to kill Renfield for years, and the Brubaker brat has seen too much and is too idealistic. I should've offed them already, but I thought it would be fun to make you fight for their lives."

"Was it really your idea, or are you still taking orders from whatever rogue element in the Society that's orchestrating this shit?" My question was a stall, meant to keep him busy while I thumbed the fire select switch to single shot. My finger tightened around the trigger, but did not exert enough pressure to fire. Not yet.

"Doesn't matter." Robinson leveled his pistol at Mike's head. "Drop the rifle or—"

Rather than let Robinson finish his threat, I put a shot between his eyes. He swayed a bit, his mouth opening and closing like a fish's out of water as he tried to turn his gun on me. He collapsed before he could manage it, and sprawled face-down in the dirt.

"Holy shit." Mike was almost comically wide-eyed. "You just killed the Sheriff."

Given that his hand still twitched a bit, I wasn't so sure. Approaching Robinson's fallen body, I shot him through the head a second time. A control shot to confirm the kill was a bit of Phoenix Society doctrine I wouldn't discard. "Now I did."

Taking Renfield's knife from its sheath, I cut his bonds before attending to Renfield's. "Sergeant, can you get your men to stand down now that Robinson's out of the picture?"

He nodded. "Probably. How many of 'em did you end up killing?"

Should have known that question was coming. That guy I gutted is probably still alive, but I bet he's righteously pissed off. Same with those guys I shot at while getting out of the barracks. That left one for sure. "Was John Atherton a close friend?"

Renfield shrugged. "Nah. Hell, I saw you take that prick's head off. You saved me the trouble of kicking his ass myself for suckering the other guys into following Robinson."

"Awesome." Mike drew out the word while stretching. "So, can we get the fuck out of here? I mean, it's all over now. Isn't it?"

There's no way things would be that simple. Renfield seemed to hold a similar opinion, for he cocked an eyebrow at Mike. "Kid, it's never over. Something always comes up."

Mike remained insistent. "Collins, Petersen, and Robinson are dead, and Naomi's got evidence to prove that they were ultimately responsible for all the weird shit. What else could possibly happen?"

---

### This Week's Theme Song

"I Shot the Sheriff" by Bob Marley and the Wailers

{% youtube DLanjRCi23w %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
