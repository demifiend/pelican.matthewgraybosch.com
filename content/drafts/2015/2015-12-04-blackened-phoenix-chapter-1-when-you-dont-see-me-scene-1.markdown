---
title: "Blackened Phoenix, Chapter 1: When You Don't See Me (Scene 1)"
modified:
excerpt: Set a few months after the end of Without Bloodshed, Blackened Phoenix opens with Naomi and Morgan seeking the trail of Christabel's murderer.
tags: [ naomi-bradleigh, morgan-stormrider, mordred, MEPOL, gregory-windsor, isaac-magnin, christabel-crowley, alan-thistlewood, rough-cut, opsec, questions, fear, murder, investigation, evidence, cover-up, when-you-dont-see-me ]
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
date: 2015-12-04T00:00:00-05:00
---

Naomi Bradleigh kept her eyes focused on the entrance to Scotland Yard. Doing so kept the MEPOL headquarters' immense glass facade from distracting her as it did so many tourists clogging the Victoria Embankment. They gave her and her companions a wide berth, for which she was grateful. People tended to keep their distance when she wore a sword, as if unsure why a dramatic coloratura soprano would go armed. The fact that one of her companions was a shaggy tuxedo cat the size of a leopard and the other was Morgan Stormrider also proved a considerable help.

Naomi scratched behind the cat's ears, filling the street with purring as she leaned over to Morgan. She whispered in his ear. "Are you sure this is a good idea?"

Morgan, the cheeky bastard, stole a kiss before answering. "It's a terrible idea, which I stipulate for the record. Doing the smart thing and staying home would be worse."

Though Naomi was loathe to admit it, she and Morgan could only hole up in his brownstone on Manhattan's Upper West Side or her own house in Crouch End for so long before the interminable nature of their idyll raised eyebrows. "Magnin expects you to use Bathory's letter of marque. It's a trap."

Morgan nodded, and his smile was grim. "Not using it is also a trap. I'd promise to explain later, but I bet you've already figured it out for yourself."

Rather than admit she had, Naomi kissed him. They had been under constant surveillance via Witness Protocol, which should only run when an Adversary was on the job. Instead, Naomi's whole life was on video, as were those of all her companions. All their plans and discussions went straight to the enemy, but cutting off the feeds at this juncture would also provide Isaac Magnin with valuable intelligence. Even secure talk and secure relay chat were no longer secure, because Magnin had thought to cover that angle. "Are you sure you have to do this?"

Morgan's sudden laughter stopped people in their tracks, and they turned to stare at him. "I'm all dressed up, Nims. I might as well go put on a show. Let's go."

To Naomi's chagrin, she soon learned that Morgan had not spoken in jest of putting on a show. Upon entering the New Scotland Yard lobby he immediately cut a path to the duty officer and drew enough of his sword to display the black cat mark Nakajima had worked into the blade. "We're here about the Christabel Crowley investigation. You have ten minutes to get us a meeting with the officer in charge. Any questions?"

The duty officer swallowed, glanced at Mordred, and swallowed again. "Adversary, pets aren't permitted here."

Mordred, unperturbed by this announcement, began knocking things off the desk. He began with the duty officer's nameplate, and progressed to a mug crammed with pens before making a game of knocking off a stack of folders one folder at a time. Before each, he would fix his blue-eyed gaze on the officer as if daring him to do anything but comply with his human's demands.

Taking pity on the poor cop, Naomi stooped to pick up what Mordred flicked to the floor. "Mordred's actually quite well-behaved as long as people don't insult him by dismissing him as a mere pet."

"Fine. The bloody cat can stay. I don't get paid enough for this anyway."

"Nine minutes." Morgan was pitiless, though he joined Naomi in picking up after the cat, who was currently purring and rubbing his face against the duty officer's as if he were Mordred's new best friend. Naomi kept her amusement to herself, along with the knowledge that not only was Mordred forgiving the man, but claiming him as his own.

A minute later, Chief Inspector Gregor Windsor appeared, his repeatedly broken nose less ruddy than it had been, as if he had joined Edmund Cohen in swearing off alcohol. "My office. Now."

Once Windsor had them alone in his office, he indicated the chairs in front of his desk. "This is just what I needed. Both Morgan Stormrider *and* Naomi Bradleigh here at Scotland Yard, making a scene. To what do I owe this dubious pleasure? And why isn't that overgrown moggie on a leash?"

A hiss from Modred at Windsor's mention of a leash made his opinion on that subject explicit. At least, that was what Naomi thought. Morgan seemed to agree, for he stuck to business. "We're displeased with MEPOL's lack of action on the Christabel Crowley case, and we're here to examine the evidence you gathered so we can continue the investigation ourselves."

As if anticipating Windsor's next question, Morgan produced Elisabeth Bathory's letter of marque, unfolded it, and placed it before Windsor for his perusal. "I trust you will find that this letter of marque from one of the Phoenix Society's executive council gives us the requisite authority to take this affair off MEPOL's hands."

Windsor raised a cynical eyebrow, apparently heedless of Mordred's presence behind him. "Am I supposed to be grateful?"

"Hardly." Morgan softened his tone and smiled as he spread his hands in a placating gesture. "Though my doing everything short of announcing that I was the Devil and that I was here on the Devil's business was just part of the act. I doubted you'd have recognized me if I approached with the tact and diplomacy Naomi would no doubt have preferred."

⎡Also, I'd have my boot up your arse if you ever actually said you were here on the Devil's business in front of me.⎦ Though Naomi was confident Morgan didn't need the reminder, she delivered it over secure talk anyway. Just in case.

Rather than pay attention to Morgan's reply, however, Naomi was too busy holding in laughter as a pair of lynx-pointed black ears tufted with white fur rose behind Windsor to give him the optical illusion of having cat ears. Seeing Morgan hiding a smile behind his fist only made it worse, and Windsor soon caught on. "What's so funny?"

Morgan cleared his throat, and patted his knee. "Fun's over, Mordred. To me, please."

Mordred returned with the alacrity of a well-trained dog, though his whiskers drooped with disappointment at a game interrupted. Taking pity on the cat, Naomi reached into her coat pocket for a baked cat treat and offered it to him. The cat took his cookie, perked up, and sat between her and Morgan while eating it.

"Sorry about that, Chief Inspector. While we understand that our presence is inconvenient at the best of times, I think we may be able to reach a mutually agreeable arrangement regarding the Crowley investigation." Naomi scratched behind Mordred's ears, eliciting a rumble of purr as she waited for the inspector's response.

"Inconvenient?" Windsor sniffed at his coffee before pulling a bottle of scotch from his desk and pouring in a generous dollop. He didn't bother offering his guests any. "You haven't the slightest fucking clue, and you never will. You Adversaries might watch us cops, but who the 'ell is watching *you*? Even if you found somebody to watch you, who would watch *them*?"

"It's turtles all the way down, so get over it." Morgan snapped off the words. "We didn't come to discuss philosophy. We came because you have information you want, information you don't appear to be using yourself."

Windsor gestured with his mug, and narrowly avoided sloshing scotch-infused coffee all over his desk. "And did it occur to you there might be a reason for that? The Crowley investigation is closed, by order of the Phoenix Society. And, no, I can't show you the order. That itself is sealed."

Naomi decided to attack from a different angle. "What about Alan Thistlewood? Morgan remanded him into MEPOL custody, but less than two weeks later he's found dead in low earth orbit. How did *that* happen?"

The hunted expression Windsor wore now warred with outright anger. "I don't know. I don't bloody well *want* to know, because I don't want to end up like that bastard."

Mordred approached Windsor's desk, his whiskers straining forward as he sniffed. Leaning over the desk with his forepaws, he began sniffing everything as Windsor backed away. The chief inspector glared at Morgan. "Get your cat under control, or-"

A stack of papers struck the floor with a resounding thump, and Mordred pounced on them, scattering documents until one near the bottom of the pile caught his interest. He held it down with a massive forepaw that he had yet to grow into despite being at least a decade old, stared directly at Morgan, and let out a plaintive, "Mrrraow?"

Steel rang as Windsor found his service gladius and drew it. Naomi drew her sword in answer, and to forestall Morgan's blade. "Drop your sword, chief inspector."

"But—"

"Drop it. Sit down. Shut up. That way none of what happens next will be your fault. Isaac Magnin won't have any cause to punish you for noncompliance with his demands."

As Windsor sagged in his chair, Morgan retrieved the document his cat found, and read it. "Orders to expect a certain unnamed representative of the Phoenix Society, comply with all instructions, and dispose of all evidence of said representative's involvement. This is completely irregular, but Windsor couldn't report this to the Phoenix Society because it came from them — from the executive council itself."

"Can you see why I tried to stonewall you?" Windsor was deflated, his voice small and defeated as he stared at the sword on the floor. "I saw the video of what Isaac Magnin did to Thistlewood. He could do that to me. To the whole force. Hell, he could do it to you guys."

Naomi shuddered, remembering for the first time in years a similar experience. Ian Malkin, who bore an uncanny resemblance to Isaac Magnin, had also stranded her in deep space for a moment while keeping her alive to prove his absolute power over her. Despite this, she had to see for herself. Morgan had to see for himself, lest he doubt her when she finally told him her story. "Chief Inspector, do you still have the video? I want you to play it for us. We need to see this for ourselves."

"The Society gave me direct orders—"

"The Society isn't here." Morgan thrust the executive council's orders to Windsor into his pocket. He then drew his own sword, and gave Windsor a good long look at the blade. "Naomi and I are. I suggest you take a moment to think on whether you prefer a distant possibility or an immediate certainty for yourself and those under you."

## Author's Notes

Set a few months after the end of [*Without Bloodshed*](/novels/without-bloodshed/), this draft of *Blackened Phoenix* opens with Naomi Bradleigh and Morgan Stormrider seeking the trail of Christabel Crowley's murderer. Though they know Morgan's a walking security breach thanks to events I'll explain in subsequent chapters, Morgan can no longer bear inaction, and is concerned that hiding will reveal to Isaac Magnin that Morgan knows he's under constant surveillance.

I'll be posting a new chapter of the rough draft every weekend until it's finished. This is your chance to get in on the ground floor of the next Starbreaker novel. Comments are open for questions or suggestions.
