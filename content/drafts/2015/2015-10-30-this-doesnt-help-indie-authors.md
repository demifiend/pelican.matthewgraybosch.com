---
title: "This Doesn't Help Indie Authors"
categories:
  - /dev/random
tags:
  - Becky McGraw
  - indie authors
  - Laura Harner
  - plagiarism
  - self-publishing
  - writing
  - wtf
format: link
---
Indie authors, especially self-published ones, have enough crap to deal with. It's hard enough to get people to buy and read your books when you're working against the assumption that you're indie because you aren't good enough for [the Big 5 US publishers](http://publishing.about.com/od/BookPublishingGeneralInfo/a/The-Big-Five-Trade-Book-Publishers.htm) (or, as I like to call them, the [New York Cartel](https://en.wikipedia.org/wiki/Cartel)). The revelation that prolific self-published romance author [Laura Harner resorted to plagiarism](http://www.theguardian.com/books/2015/oct/28/prolific-romantic-fiction-writer-exposed-as-a-plagiarist) doesn't help.

Now, if I wanted to be a dick I could point out that belting out 75 books in 5 years should be the first clue that an author isn't entirely legit. But that could just be envy talking; I'm doing a book every two years, which is pathetic even for a writer with a day job.

#### Image Credit

[The Guardian &#8211; Books](http://www.theguardian.com/books/2015/oct/28/prolific-romantic-fiction-writer-exposed-as-a-plagiarist)
