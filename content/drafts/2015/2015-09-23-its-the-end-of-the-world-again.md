---
title: "It's the End of the World&#8230; Again"
categories:
  - Rants
tags:
  - and I feel fine
  - Apocalypse
  - bullshit
  - End Times
  - the end of the world
  - fundies
  - Lestat
  - prophecies
  - REM
  - wrong again
---
MRW fundies insist that [the world will end](http://www.donotlink.com/grwk) on a certain date in the near future. Or, in this case, _today_.

It's funny how the world looks like it's doing just fine. The weather's pleasant (despite the way we've screwed the climate). The sky's still blue. I'm still on the job.

I guess the fundies are wrong again. Funny how every time Christian fundies say the end times are upon us, God misses their deadline. It's almost like God doesn't give a shit what his fan club expects from him &#8212; or maybe God just doesn't exist.

Or maybe it's time for some R.E.M. It's the end of the world, _again_, and I feel fine. How about you guys?
