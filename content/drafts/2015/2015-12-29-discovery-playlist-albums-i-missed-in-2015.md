---
title: 'Discovery Playlist: Albums I Missed in 2015'
excerpt: Here are thirty heavy metal and progressive rock songs from albums I missed in 2015, most from albums released that year, for your listening pleasure.
header:
  image: stereo-music.jpg
  teaser: stereo-music.jpg
gallery1:
  - url: dan-swano-moontower.jpg
    image_path: dan-swano-moontower.jpg
    alt: "Dan Swanö: Moontower"
  - url: ahab-the-boats-of-the-glen-carrig.jpg
    image_path: ahab-the-boats-of-the-glen-carrig.jpg
    alt: "Ahab: The Boats of the &quot;Glen Carrig&quot;"
  - url: eldritch-underlying-issues.jpg
    image_path: eldritch-underlying-issues.jpg
    alt: "Eldritch: Underlying Issues"
  - url: christian-mistress-to-your-death.jpg
    image_path: christian-mistress-to-your-death.jpg
    alt: "Christian Mistress: To Your Death"
  - url: kauan-sorni-nai.jpg
    image_path: kauan-sorni-nai.jpg
    alt: "Kauan: Sorni Nai"
  - url: denner-shermann-satans-tomb.jpg
    image_path: denner-shermann-satans-tomb.jpg
    alt: "Denner/Shermann: Satan’s Tomb"
  - url: serious-black-as-daylight-breaks.jpg
    image_path: serious-black-as-daylight-breaks.jpg
    alt: "Serious Black: As Daylight Breaks"
  - url: exmortus-slave-to-the-sword.jpg
    image_path: exmortus-slave-to-the-sword.jpg
    alt: "Exmortus: Slave to the Sword"
  - url: kobra-and-the-lotus-words-of-the-prophets.jpg
    image_path: kobra-and-the-lotus-words-of-the-prophets.jpg
    alt: "Kobra and the Lotus: Words of the Prophets"
gallery2:
  - url: unisonic-light-of-dawn.jpg
    image_path: unisonic-light-of-dawn.jpg
    alt: "Unisonic: Light At Dawn"
  - url: pyramaze-disciples-of-the-sun.jpg
    image_path: pyramaze-disciples-of-the-sun.jpg
    alt: "Pyramaze: Disciples of the Sun"
  - url: kingcrow-eidos.png
    image_path: kingcrow-eidos.png
    alt: "Kingcrow: Eidos"
  - url: delain-the-human-contradiction.jpg
    image_path: delain-the-human-contradiction.jpg
    alt: "Delain: The Human Contradiction"
  - url: metal-allegiance.jpg
    image_path: metal-allegiance.jpg
    alt: "Metal Allegiance"
  - url: paradise-lost-the-plague-within.jpg
    image_path: paradise-lost-the-plague-within.jpg
    alt: "Paradise Lost: The Plague Within"
  - url: sun-caged.jpg
    image_path: sun-caged.jpg
    alt: "Sun Caged"
  - url: iced-earth-dystopia.jpg
    image_path: iced-earth-dystopia.jpg
    alt: "Iced Earth: Dystopia"
  - url: borknagar-urd.jpg
    image_path: borknagar-urd.jpg
    alt: "Borknagar: Urd"
gallery3:
  - url: meadows-end-the-sufferwell.jpg
    image_path: meadows-end-the-sufferwell.jpg
    alt: "Meadows End: The Sufferwell"
  - url: arion-last-of-us.jpg
    image_path: arion-last-of-us.jpg
    alt: "Arion: Last of Us"
  - url: hammerforce-dice.jpg
    image_path: hammerforce-dice.jpg
    alt: "Hammerforce: Dice"
  - url: threshold-subsurface.jpg
    image_path: threshold-subsurface.jpg
    alt: "Threshold: Subsurface"
  - url: savage-messiah-the-fateful-dark.jpg
    image_path: savage-messiah-the-fateful-dark.jpg
    alt: "Savage Messiah: The Fateful Dark"
  - url: nightingale-nightfall-overture.jpg
    image_path: nightingale-nightfall-overture.jpg
    alt: "Nightingale: Nightfall Overture"
  - url: the-great-discord-duende.jpg
    image_path: the-great-discord-duende.jpg
    alt: "The Great Discord: Duende"
  - url: year-of-the-goat-angels-necropolis.jpg
    image_path: year-of-the-goat-angels-necropolis.jpg
    alt: "Year of the Goat: Angels’ Necropolis"
  - url: dalriada-mesek-almok-regek.jpg
    image_path: dalriada-mesek-almok-regek.jpg
    alt: "Dalriada: Mesék, Álmok, Regék"
gallery4:
  - url: earthside-a-dream-in-static.jpg
    image_path: earthside-a-dream-in-static.jpg
    alt: "Earthside: A Dream in Static"
  - url: raise-hell-written-in-blood.jpg
    image_path: raise-hell-written-in-blood.jpg
    alt: "Raise Hell: Written in Blood"
  - url: anthrax-for-all-kings.jpg
    image_path: anthrax-for-all-kings.jpg
    alt: "Anthrax: For All Kings"
categories:
  - Music
  - Longform
tags:
  - Ahab
  - Anthrax
  - Arion
  - Borknagar
  - Christian Mistress
  - Dalriada
  - Dan Swanö
  - Delain
  - Denner/Shermann
  - Discover Weekly
  - Earthside
  - Eldritch
  - Exmortus
  - Hammerforce
  - Iced Earth
  - Kauan
  - Kingcrow
  - Kobra and the Lotus
  - "Meadow's End"
  - Metal Allegiance
  - Nightingale
  - Paradise Lost
  - playlist
  - Pyramaze
  - Raise Hell
  - Serious Black
  - Spotify
  - Sun Caged
  - The Great Discord
  - Threshold
  - Unisonic
  - Year of the Goat
---
{% include base_path %}

I've been taking advantage of Spotify's "Discover Weekly" playlist for several months now, but as far as I know the only way to share what Spotify picks for me is to create a new playlist and save each song. And if I'm going to do that, I might as well do it as a web page I can share with all of you with occasional commentary. We've got a shitload of songs from albums released in 2015 in this week's playlist, and I don't think there's a single _bad_ track in the bunch.

I wonder if I can make a weekly event of this. Well, that depends a bit on you guys. If you enjoy this, say so in the comments or share on social media. In the interest of getting the page loaded fast, I'll embed a single playlist instead of individual songs.

## This Week's Playlist

Play this, and then follow along below if you like.

<iframe src="https://embed.spotify.com/?uri=spotify%3Auser%3A1238897208%3Aplaylist%3A32XKwJhqJ0BvrO5GxrwWJk" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

{% include gallery id="gallery1" caption="Albums featured in this playlist (1/4)" %}

## Dan Swanö: "Uncreation"

The third track from [Dan Swanö](http://www.danswano.com/)'s 1998 _Moontower_ album starts out with a prog-style synth and guitar intro, which led me to expect clean Euro vocals. Instead, I got death metal vocals. It doesn't quite work for me, but the music is good.

## Ahab: "Like Red Foam (The Great Storm)"

At just under six and a half minutes, "Like Red Foam (The Great Storm)" from [Ahab](http://www.ahab-doom.de/blog/en/)'s 2015 effort, _The Boats of the "Glen Carrig"_, is the shortest song on the hour-long six-track album. More death metal vocals here, but the choruses are clean.

Apparently this German act does funeral doom metal, and the album is based on [a novel published in 1907](https://en.wikipedia.org/wiki/The_Boats_of_the_%22Glen_Carrig%22) by [William Hope Hodgson](https://en.wikipedia.org/wiki/William_Hope_Hodgson).

## Eldritch: "Changing Blood"

Here's another track from a 2015 album, this time by Italian progressive metal act [Eldritch](http://www.eldritchweb.com/). "Changing Blood" is the blistering opener to _Underlying Issues_, and there's some solid guitar work to be had here. Check it out.

## Christian Mistress: "Neon"

And now for some woman-fronted old-school heavy metal with modern production from Olympia, WA in the US. And from yet _another_ 2015 release. "Neon" by [Christian Mistress](https://christianmistress.bandcamp.com/) is a head-banging anthem perfect for blasting in your car while driving way too fast, with Christine Davis belting out the lyrics like she learned her chops from Wendy O. Williams and Lita Ford.

I _like_ it.

## Kauan: "Akva"

Holy shitballs, "Akva" by [Kauan](https://www.facebook.com/kauanmusic/) is the fourth consecutive song from an album released in 2015 I've hit in this playlist. Kauan is a Russian/Finnish band from Chelyabinsk that draws from folk metal, black metal, and atmospheric doom metal.

Most of their lyrics, as well as their song and album titles, are in Finnish. This makes their songs inaccessible to me, but their _Sorni Nai_ album is probably worth a full listen.

## Denner/Shermann: "War Witch"

And now we've got some old-school metal from the two guitar demons of Mercyful Fate, Hank Shermann and Michael Denner. "War Witch" sounds like it ought to be classical Mercyful Fate, but it isn't King Diamond handling vocal duties this time around.

The track comes from [Denner/Shermann](http://dennershermann.com/)'s 2015 EP, _Satan's Tomb_, and has another dark Euro metal vet on drums: Snowy Shaw. I'm looking forward to a full-length album from these guys.

## Serious Black: "High and Low"

What happens when six power metal veterans and (presumably) Harry Potter fans decide to start a new supergroup? You get a band called [Serious Black](http://www.serious-black.com/), a 2015 debut album called _As Daylight Breaks_, and a single called "High and Low".

Jokes aside, this band is seriously tight, and this is some catchy music. I recommend this one to fans of Helloween, Masterplan, At Vance, etc.

## Exmortus: "Moonlight Sonata (Act 3)"

Well, we got some Beethoven. Now we just need some consensual sex and ultraviolence. Southern California shredders [Exmortus](http://exmortus-official.bandcamp.com/) do a kick-ass of the Moonlight Sonata's third movement for their 2014 album _Slave to the Sword_. Here's another album I'll have to play in its entirety.

## Kobra and the Lotus: "Black Velvet"

You've probably heard this song before. The devil knows I've heard it on the radio often enough since 1989 to be sick of the original performed by [Alannah Myles](https://en.wikipedia.org/wiki/Black_Velvet_%28song%29). Canadian siren Kobra Paige and her band [Kobra and the Lotus (warning: auto-playing music)](http://kobraandthelotus.com/) breath new undeath into this bluesy metal cover for their 2015 _Words of the Prophets_ EP, which also includes a cover of Rush's "The Spirit of Radio".

Another woman-fronted band to add to my collection, since Kobra Paige is another old-school belter like Christine Davis of Christian Mistress.

{% include gallery id="gallery2" caption="Albums featured in this playlist (2/4)" %}

## Unisonic: "Exceptional"

I'm not sure "Exceptional" from German power metal act Unisonic's 2014 _Light of Dawn_ album actually lives up to it's name, but it's a solid track and former Helloween vocalist Michael Kiske still hits the highs. Give it a listen and decide for yourself.

## Pyramaze: "Disciples of the Sun"

I'm not familiar with the Danish progressive/symphonic metal band [Pyramaze](http://www.pyramaze.com/), but listening to the title track from their 2015 album leads me to think that I might be remiss. Oddly enough, this is only their third album, and their first since _Immortal_ in 2008.

Did it take Pyramaze that long to find a successor for vocalist Matt Barlow (ex-Iced Earth)?

## Kingcrow: "The Moth"

Here's more new (to me, at least) prog metal. This time, it's another Italian band called [Kingcrow](http://www.kingcrow.it/) with a 2015 release named after a defunct videogame studio: _Eidos_. I'll have to give it a listen; there's some interesting clean guitar work to be heard in "The Moth", which also has an [animated video](https://www.youtube.com/watch?v=a4Giy35m410).

<iframe width="560" height="315" src="https://www.youtube.com/embed/a4Giy35m410" frameborder="0" allowfullscreen></iframe>

## Delain: "Here Come the Vultures"

This is the opening to Delain's 2014 effort, _The Human Contradiction_. It's a good song from a good album, and Charlotte Wessels has a deeper and sweeter voice than more famous metal sirens like Sharon Den Adel or Tarja Turunen. Unfortunately, neither the song nor the album grabbed me the way previous efforts like _April Rain_ and _We are the Others_ did.

Maybe I haven't played it enough for it to grow on me.

## Metal Allegiance: "We Rock"

Metal Allegiance? I was tempted to dismiss this supergroup founded by musical instrument industry exec [Mark Menghi](https://www.linkedin.com/in/markmenghi) as a cheesy attempt to cash in on metalheads' devotion. It's hard to justify doing so [when you see who's involved in this project](http://www.metalallegiance.com/artists.php).

"We Rock" is a bonus track obviously intended to close out a Metal Allegiance show with the audience on their feet and banging their heads, but it's solid enough for me to give the rest of the eponymous 2015 album a listen.

## Paradise Lost: "No Hope in Sight"

Another Paradise Lost album in 2015? Sweet. I've been a fan of this Halifax, UK doom metal act since they released _Draconian Times_ in the mid-1990s. Naturally, I'll be listening to _The Plague Within_, because "No Hope in Sight" sums up this band perfectly. Hell, I might even put this on a Starbreaker soundtrack.

## Sun Caged: "Closing In"

Here's a blast from the past with an urgent piano intro from 2003. Well, it's a blast from _somebody's_ past, but this is the first I've heard of Dutch progressive metal act _Sun Caged_, which apparently disbanded in 2014 after releasing three albums. I know what I'll be hunting down.

## Iced Earth: "Anthem"

Like Delain's "Here Come the Vultures", "Anthem" from Iced Earth's 2011 quasi-concept album _Dystopia_ is a familiar tune to me. I'm not sure if it's inspired by Ayn Rand's novella _Anthem_ or not, but both the track and the album are worth listening, and vocalist Stu Block is a better fit for Iced Earth without Matt Barlow than Tim "The Ripper" Owens &#8212; though to give the Ripper his due, his Halford-esque voice worked well in _The Glorious Burden_.

## Borknagar: "Frostrite"

I think "Frostrite" from [Borknagar](http://borknagar.com/)'s 2012 album _Urd_ is the first I've heard from this Norwegian progressive black metal band. Their use of multiple vocalists is interesting.

{% include gallery id="gallery3" caption="Albums featured in this playlist (3/4)" %}

## Meadows End: "This Coming Nightfall"

Swedish band [Meadows End](http://www.meadowsend.org/) tries to do symphonic death metal in "This Coming Nightfall" from their 2014 album, _The Sufferwell_, but the death metal vocals really don't fit the music. The cover's badass, though, and you might enjoy them if you don't share my bias in favor of clean vocals.

## Arion: "Last of Us"

[Arion](https://www.facebook.com/OfficialArion) from Finland is a fairly new melodic metal band, having started in 2011. If the rest of their 2014 album _Last of Us_ is anything like the title track I'm in for a treat. They're a tight six-piece act with bombastic melodies; just the palate cleanser I needed after Meadows End.

Now they just need somebody to hit 'em upside the head and tell 'em to get a _real_ website. Facebook pages don't count. Not for Google SEO, and not to me.

## Hammerforce: "Fall of Monsegur"

Power metal from St. Petersburg, Russia? Sure, why not? "Fall of Monsegur" from Hammerforce's 2009 debut _Dice_ is a solid effort, though for songs about the fall of the Cathar citadel at the hands of Roman Catholic forces I prefer Iron Maiden's "Montsegur".

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A02EI7HUkyb5AsZGzYhIG0M" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Threshold: "Mission Profile"

The opening track to the Surrey, UK band [Threshold](http://www.thresh.net/)'s 2004 _Subsurface_ album is melodic, socially conscious progressive metal, like most of their songs. ["Mission Profile"](http://www.lyricsfreak.com/t/threshold/mission+profile_20459583.html) is a fairly direct attack on the War on Terror in general, and the United States' second war against Iraq in particular.

> We've got a system you're going to use it  
> We call it freedom and you are free to choose it  
> If you're not against it you've got to be for it  
> Neutral is dangerous and you cannot ignore it  
> <cite>Threshold, "Mission Profile", <em>Subsurface</em> (2004)</cite>

Now there's a song George W. Bush and Dick Cheney need to be forced to listen to on repeat, though the damage is done.

## Savage Messiah: "Hellblazer"

I actually forgot to cover Savage Messiah's "Hellblazer" when I first wrote this post back in December 2015, which should tell you just how forgettable I found this cut from their 2014 album, *The Fateful Dark*.

"Hellblazer", as you might have guessed, is about magician John Constantine from DC/Vertigo's *Hellblazer* comics. The music's decent thrash/speed metal, but I'd rather just listen to Annihilator or The Worshyp.

## Nightingale: "Nightfall Overture"

Dan Swanö's back, this time in a band called [Nightingale](http://www.danswano.com/nightingale.php) inspired by classic Gothic rock acts like The Sisters of Mercy. The title track to their 2005 album _Nightfall Overture_ features excellent guitar and piano, and (thank all the powers of Heaven and Hell alike) clean vocals.

I'm pretty sure I've listened to their 2015 album, _Retribution_. I recommend it if you want to go dark without sacrificing melody.

## The Great Discord: "The Aging Man"

I'm going to have to check out _Duende_ by [The Great Discord](http://metalblade.com/thegreatdiscord/), a relatively new progressive metal act from Linköping, Sweden grounded by the vocals of alto siren Fia Kempe. "The Aging Man" deals with an individual facing his death, and if the rest of the album is as powerful I'm going have another band to love.

And, yeah. [The rest of _Duende_ is just as powerful. Highly recommended.](/2016/03/duende-by-the-great-discord-lives-up-to-its-name/)

## Year of the Goat: "For the King"

If you've got Ghost, you've got everything, but you might want some occult rock from a different band. [Year of the Goat](https://www.facebook.com/yearofthegoat) from Norrköping, Sweden has you covered. "For the King" is from their 2012 album, _Angel's Necropolis_. It's pretty solid, and I could imagine these guys opening for _Spectres_-era Blue Öyster Cult.

## Dalriada: "A Walesi Bárdok 1. rész"

"A Walesi Bárdok 1. rész" by Dalriada's 2015 folk metal compilation _Mesék, Álmok, Regék_ is... interesting. I've never heard of this Hungarian band before.

{% include gallery id="gallery4" caption="Albums featured in this playlist (4/4)" %}

## Earthside: "The Closest I've Come"

What? Listen to _another_ progressive metal band? Why, I don't mind if I do. If I had to pick a single standout track from this entire playlist, I'm tempted to go with "The Closest I've Come" from [Earthside's](earthsideband.com/) 2015 release _A Dream in Static_. I think I'd enjoy seeing this band live.

## Raise Hell: "Demon Mind"

So, we go from almost ambient progressive metal to Swedish thrash metal with death metal vocals courtesy of [Raise Hell](https://www.facebook.com/raisehell666). "Demon Mind" from Raise Hell's 2015 album _Written in Blood_ isn't bad, but it's ill served by the vocalist's snarling, guttural delivery.

## Anthrax: "Evil Twin"

It's appropriate to close out this playlist with a cut from New York's contribution to thrash metal, [Anthrax](http://anthrax.com/). "Evil Twin" appears on Spotify as a single, most likely to promote the band's upcoming release _For All Kings_. I'm gonna have to check it out. You should, too.
