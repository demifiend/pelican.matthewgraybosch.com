---
title: "*Silent Clarion*, Track 34: &quot;Desecration of Souls&quot; by Mercyful Fate"
excerpt: "Check out chapter 34 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
"Was it God's will that your son die so young, or in so cruel a manner?" Taking Mr. Wilson's hand, I gazed into his streaming eyes. "I wouldn't presume to know what He wants, but I promise we will handle his body with all due respect, and return him to the earth as soon as possible. Help me stop your son's murderers before they steal more lives."

Mrs. Wilson took her husband's hand from mine. "Adversary, I helped wash and dress our son for the funeral. I saw what was done to him. Surely the bites were what killed him?"

Rather than explain the stab wound, I let Mrs. Wilson believe what she wanted to about the bites. "I'm sorry, but I'm curious about an injury your son may have sustained before he was finally killed. You see, the latest victim was shot with a blunt arrow to knock him out before his killers fed on him. I think the same may have been done to your boy."

The parents shared a long look before Mrs. Wilson turned back to me. "Will you pray for my boy when you bury him again, Adversary Bradleigh?"

"I'm -" My breath caught in my throat. How the hell do you tell a grieving mother who has asked you to pray for her boy that you don't share her faith? Sure, I've gone with Jacqueline to the temple when she made offerings to Athena, Hermes, and Zeus before a dangerous mission, but the priests handled the prayers on our behalf. This was different.

It was Sheriff Wilson who saved me. "We'll pray with you, Mrs. Robinson. Adversary Bradleigh wants to see justice done, but she isn't from around here and wouldn't understand."

The reek of embalming fluid assaulted us as we opened the casket, and I was grateful we were outdoors. It was hard to refrain from remarking on how odd it was for Scott Wilson's parents to object to an autopsy and exhumation, but not to letting the local undertaker exercise their craft to stave off the natural processes of decay as long as possible. None of that for me, by the way. Just give me back to the earth and plant an apple tree over my grave.

Shaking off such morbid thoughts, I lifted Scott Wilson's head so I could complete a thorough cranial examination. Though the thin latex gloves complicated my job, I worked by touch, carefully feeling every square centimeter of his scalp. If there was nothing there, we could just slip him back into his casket, close it up, and bury him again. Then I could withdraw and bow my head respectfully while Sheriff Robinson and his deputies prayed with the Wilsons.

No such luck. My thumb brushed against something that felt like stitches, and I looked up at Sheriff Robinson. "Found something. Can you lift him up so I can get a better look?"

Robinson nodded, and Deputy Colby helped him. Parting Wilson's thick hair, I tried not to make a fuss when a clump came loose in my hand. Instead, I put the hair in his casket as discreetly as I could. Fortunately, somebody thought to escort the parents to a squad car before we opened the casket. If we could wrap this up and get the casket closed again before they came back, they wouldn't need to know the undertaker had cut corners while covering up the wound.

The stitches formed a semicircle around the mark imprinted in Scott Wilson's scalp, as if the arrow had struck hard enough to shatter the bone underneath and tear open the skin. Against my better judgment, I pressed the impact wound. The flesh yielded with a small but sickening squelch, and some noisome substance leaked from between the stitches.

Taking a step back and turning from the body, I took deep breaths until my nausea subsided. When I felt I could speak without dry-heaving, I turned to Sheriff Robinson. "He has the same blunt arrow imprint, but I think he was shot with greater force than Charles Foster. The perp might have been closer to Wilson when loosing his arrow."

Robinson nodded, and waved to the deputies. They began the unpleasant task of documenting the wound before getting Scott Wilson back into his casket. "So, we have the same method for two murders. Nail the victim with a small game arrow to knock him out. Then strip him and inflict multiple bites, as if feeding, to simulate a vampire attack. Finally, finish the kill with a knife to the femoral at the juncture between thigh and groin."

"We still don't have a motive. Is this some kind of warning? Did these kids do something to piss these guys off? How are they connected?" The deputies closed the casket, and had lowered it back into the grave. It was time to bury him again, so I reached for a spade.

Robinson put his hand atop mine. "You don't have to do this."

"I might not be comfortable praying with the Wilsons, but it was my idea to dig him up. I owe it to them to help give him back in the earth." Robinson lifted his hand, and I picked up my spade and joined in. Afterward, I stepped back and bowed my head as the others knelt to pray.

The sun had begun setting when the others finished their prayers. The first stars had come out, so I sped a prayer of my own skyward. Hopefully, God would remember Scott Wilson, because his life shouldn't have ended like this.

Robinson found me after the Wilsons and his deputies left the graveyard. "I didn't think you'd stay."

"It would have been disrespectful to leave." More stars were visible now, and the western sky was ablaze, but I still had work to do. "Got any ideas for what to do next?"

Robinson didn't answer immediately. "The only bow-hunters I know are Dr. Petersen and Mayor Collins. But there's no reason to steal my gear when they have their own. Even if we found an arrow on the scene, we couldn't prove ownership. The brand we use is too damn common."

"Regardless, the stitches in Wilson's scalp suggest that Petersen was aware of the wound. An undertaker willing to do a half-assed job of covering damage like that wouldn't bother suturing it with such precision. Yet he never mentioned it in the autopsy report. I think it's time we questioned him."

Robinson grew pensive in the twilit silence. "Can you do me a favor? I'd like to talk to Henrik and Brian as friends, and see if they know anything."

It wasn't a terrible idea. If Sheriff Robinson confronted them in his official capacity, the only thing he'd get out of either of them was a refusal to answer questions without their attorney present. They'd certainly clam up if I showed. They weren't stupid. "So, you need me to find something else to do so my presence doesn't tip them off?"

"Is there some way I can consent in advance to you viewing my Witness Protocol feed?"

Good question. "Let me check with Malkuth."

｢Don't bother.｣ Malkuth must be monitoring my feed. Is he bored, does he watch every Adversary in the field, or did somebody put him up to it? Saul, perhaps? ｢I already spoke with Robinson. He's pretty smart for a cop.｣

｢Should I tell him you said so?｣

｢Only if you want him getting arrogant.｣ With that, he disconnected. I turned my attention back to Robinson. "Malkuth says he contacted you."

"He did. He also said I'd better not try to fuck with you, because he had root on something called Gungnir. Ring any bells?"

A cathedral's worth, as a matter of fact. "AIs get better at social interaction with practice, so I hope he's joking. I guess information about Commonwealth military orbital weapons platforms was a bit above your pay grade."

"Just a bit." Robinson's tone hadn't changed, but he seemed a bit paler than usual.

Giving him a moment to chew on the consequences of backstabbing me, I checked in on Malkuth. I really can't have him threatening people with global thermonuclear war for my sake. ｢Mal, are you listening?｣

｢Always.｣

He'd better not be serious about the 'always'. That would just be creepy. ｢Please refrain from threatening my coworkers with orbital bombardment on my behalf in the future. We're supposed to avoid collateral damage.｣

｢Aww, c'mon. You never let me have any fun.｣

｢Consider it payback for keeping me in the dark about Project Harker.｣

Since Robinson didn't need me tagging along, I decided to do a bit of intelligence-gathering of my own. After today's work, I needed a drink or three. Looking up Kaylee in the town directory, I texted her. ｢Kaylee, want to meet at the Lonely Mountain tonight? Beer's on me.｣

I sent Michael Brubaker the same message. Maybe my new friends could offer a different angle on the situation. Since Kaylee seems to know the town, and Michael the woods, I figured I might get some answers out of them if I asked the right questions. "I'm going to emulate your example and talk to some people over drinks. I'll catch up with you later."

I waited until Robinson was out of sight before heading over to a row of rose bushes and drawing my sword. They had managed one last autumn bloom, which I cut and placed on Scott Wilson's grave. With my respects paid, I too left the graveyard for happier environs.

Quickening my pace, I jogged along the path until I caught up with Robinson. "I thought I'd walk back to town with you. We should probably advise the citizens not to venture out alone after dark."

"Already thought of that. Mayor Collins wasn't too keen on the idea, though. Said I was stirring up paranoia." Robinson shrugged. "I just asked him if he wanted more people to die, and put out the advisory anyway."

"Glad to hear it." No doubt the Mayor was miffed, but he's just looking out for himself and let the rest of the town be damned. I was about to say so when I heard a creaking sound that felt out-of-place. "Down!"

---

### This Week's Theme Song

"Desecration of Souls" by Mercyful Fate, from *Don't Break the Oath*

{% youtube KqHVvbhAAEA %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
