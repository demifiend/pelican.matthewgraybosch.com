---
id: 1725
title: 'Gun Violence as a Recursive C# Function'
date: 2015-10-01T21:51:51+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1725
permalink: /2015/10/gun-violence-as-a-recursive-c-function/
bitly_link:
  - http://bit.ly/1N0i05a
sw_cache_timestamp:
  - 401650
bitly_link_linkedIn:
  - http://bit.ly/1N0i05a
bitly_link_facebook:
  - http://bit.ly/1N0i05a
bitly_link_twitter:
  - http://bit.ly/1N0i05a
snap_MYURL:
  - 
snapEdIT:
  - 1
bitly_link_tumblr:
  - http://bit.ly/1N0i05a
bitly_link_reddit:
  - http://bit.ly/1N0i05a
bitly_link_stumbleupon:
  - http://bit.ly/1N0i05a
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Rants
tags:
  - 'C#'
  - corruption
  - GOP
  - gun-control
  - gun-lobby
  - gun-violence
  - metaphor
  - NRA
  - politics
  - programming
  - spineless-democrats
  - US
---
Here's my understanding of how gun violence in the United States works, expressed as a recursive C# method. Because it's recursive, it calls itself, thus repeating until an external event breaks the cycle. Think of it as a metaphor in code.

I'll be getting into politics afterward, so be warned. Keep in mind that I'm both a dirty long-haired pinko atheist _and_ a gun owner. I don't mind if you disagree with me, but keep it civil.

## The Cycle

<div class="gist-oembed" data-gist="demifiend/face914bc867e9fd25ea.json">
</div>

## Breaking the Cycle

What would it take to end this cycle? The Democrats need to get their shit together in 2016. If they can win the White House, great, but the truly important elections are in Congress. The entire House of Representatives is up for grabs, as well as thirty-three seats in the Senate.

If the Democrats take control of Congress, they have the means to introduce and pass legislation that will make it harder for people who shouldn't have firearms to buy them. They can levy taxes on ammunition and reloading supplies. They can pass legislation mandating that all gun owners carry liability insurance on their firearms. They can force such legislation past a Presidential veto if necessary. They might even be able to put before the states a Constitutional amendment that alters or outright repeals the Second Amendment.

But first, the Democrats have to take Congress. Not a single seat can remain in GOP hands. Their obstruction and collusion with the NRA and the gun manufacturers it truly represents (rather than American's responsible gun owners) has gone on long enough, and can no longer be tolerated.

Neither can spinelessness and incompetence on the part of the Democratic party. They _need_ to reframe the debate and make an iron-clad case for leftist ideals as the way forward for twenty-first century America. We can't afford to let reactionaries make "liberal", "leftist", or "socialist" dirty words any longer.

#### Photo Credit

[Gun Wall by Michael Saechang](https://www.flickr.com/photos/saechang/5810753486)