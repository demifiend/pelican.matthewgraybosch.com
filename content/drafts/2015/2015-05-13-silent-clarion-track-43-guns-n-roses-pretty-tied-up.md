---
title: "*Silent Clarion*, Track 43: &quot;Pretty Tied Up&quot; by Guns 'n Roses"
excerpt: "Check out chapter 43 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
When I regained consciousness, I thought the world had inverted itself around me. It took a second for the fog dulling my reason to clear enough for me to realize that I was upside down. Somebody had bound my hands behind my back with what felt like a cable tie, bound my ankles with nylon rope, and hung me head-down by my ankles from a hook in a meat locker. However, it wasn't the meat locker we found in Fort Clarion's mess hall. This one was bigger, and reeked of fresh blood.

People had died here. They had died tonight, and if I didn't get the hell out of here, I was most likely next on the menu. Fired by this knowledge, I took further stock of my situation. My captors had stripped me of my weapons and armor, leaving me in only my undersuit. With no weapons concealed on my body, simply cutting away my bindings was out of the question. That left me with only one option; I had to break free before somebody came for me.

It would be easy enough to wear out my ankle restraints by swinging from the hook and letting friction do the work for me, but I was half a meter from a hard concrete floor. Once I was off the hook, I would most likely crack my skull. I didn't have enough room to twist my body to land on something I could more easily afford to break.

Hands first, then. It didn't take long, but my wrists were bruised and bloody by the time I had freed them, and my shoulders ached from the effort. Despite time being my enemy, I needed a rest before soldiering on.

My fingertips brushed against concrete, and I pulled my hands back for a moment. Though the blood-stained floor repulsed me, I had an idea for getting my ankles freed. As long as I remained hanging, breaking my ankle bonds was most likely a great way to smear my brains across the floor.

But what if I was hung low enough that doing a handstand would get me off the hook? Taking a deep breath to settle my nerves, I tried it. I couldn't muster the strength to lift myself on the first go, but a perverse corner of my imagination supplied me with a little clip of how I'd die if I didn't get off that damn hook. It involved a knife, a trough, and a platoon of thirsty vampire soldiers. Quite emphatically averse to going that way, I screamed through the pain and lifted myself until the rope binding my ankles cleared the hook.

Retreating to a corner got me away from the worst of the blood, and gave me the reassurance that can only come from having your back against a solid wall. Whatever came next, it wouldn't be able to come at me from behind.

"Shit!" Sudden pain flared in my left hand. Seeing that I was bleeding from a superficial cut, I looked around and found a broken knife blade beside me. It only had a single edge, but using it to cut my ankles loose would still be dangerous. Handling it as gingerly as possible, I began to saw away at the last of my bonds. Despite it being broken, the knife-blade still made fairly short work of what I was using it for.

Now that I was free, it was time to reconsider my situation. I was still unarmed and practically naked. My undersuit covered me neck-to-toe, and held in enough body heat to keep me from freezing in this meat locker. Aside from myself, I had no weapons save the broken blade. Worse, I was stuck in here until somebody opened the door; it was locked and didn't open from inside. Speaking of the door: it opened outward, which precluded use as a blunt instrument.

Though it might just be the meat locker, my implant showed I was offline. If it wasn't just the room, then I had finally gotten into Fort Clarion's underground. Man, they were going to regret bringing me home for dinner.

However, I had one small advantage; my eyes had adapted to the gloom. Anyone coming in would need a second or two to adjust. That second or two was my best hope of getting out of here. I had to strike swiftly, incapacitate the first man to enter, and use whatever weapons he carried to take out any companions he had behind him. Crouching in the shadows of the corner nearest the door, I waited and listened while counting my breaths.

Fortunately, my captors didn't make me wait too long. Minutes after I had taken up my position, I heard approaching footsteps. I tensed as they stopped and a key scraped metal. There was a thunk of metal striking concrete, as if the soldier had simply dropped whatever padlock secured the door's outer latch. The door was silent on well-oiled hinges as it opened to admit the light from the kitchen outside. The shadows preceding the man suggested he was coming in alone, but I didn't relax yet. His buddies might still be outside, but standing where their shadows wouldn't give them away.

"Where the fuck is she?" The soldier stepped inside, blinking at the dark, and I put aside all caution because the son of a bitch had my sword. Not one of the blades Nakajima lent me, but my sidesword, the one I carried throughout my career. I lent Brubaker that blade so he could protect himself in my absence, and if Dusk Patrol had the weapon, they most likely had the kid as well. Whoever this bastard is, he's going to be the first to pay.

The knife-blade I hurled at his face struck his brow and bit deeply, pouring blood across one of his eyes as I leaped upon him with a feral snarl. He grappled with me, but for some reason, refused to take the offensive. Was he reluctant to strike a woman? Too bad for him if he was, because I was done pissing about. Driving him to his knees with a knee to the balls, I kicked him in the face and bounced the back of his head off the wall behind him. "Where's Brubaker?"

"Please, stop." Instead of fighting, he tried to protect himself, curling into a fetal position. "I'm not here to fight you. Please stop hurting me."

Disgusted with him and myself, I gave him one last kick in the guts before taking my sword off him. "You call yourself a soldier? You're pathetic. How can you just let me beat the shit out of you like this?"

"R-Renfield sent me." His breath hitched as he lay shivering on the floor. It took me a moment to realize he was crying. "H-He told me to give you back your sword and bring you to him. He's got the Brubaker k-kid. He's safe."

Brubaker's safe? With the same Renfield who jabbed me with a sedative and let his buddies hang me by my ankles in a godforsaken meat locker? Not bloody likely. Taking a breath, I drew my sword and tapped him with the tip. "On your feet, soldier, and give me a name so I don't have to think of you as Private Crybaby."

He wiped his eyes with the back of his hand. "It's Private Fowler, ma'am."

"Adversary Naomi Bradleigh." Seemed only fair to return the favor. "You said Renfield sent you?"

Fowler nodded. "He told me you'd be miffed about the hypo, but…"

Miffed? Man, Renfield has *no* idea. If he doesn't have a good explanation, and if Brubaker isn't safe, Private Fowler here is going to think I went easy on him. "Take me to him. Shall I belabor the consequences of leading me into a trap?"

"No, ma'am. That won't be necessary." Judging from the glum expression, he had already given the matter some thought. That suited me just fine; I doubted I was sufficiently sadistic to describe crueler tortures than whatever Fowler might imagine.

Fort Clarion's underground was a maze of narrow hallways full of doors marked with evocative labels like 'Isolation Chamber X7' and 'Secure Containment'. Fowler ignored them, and led me to the NCO Barracks. Renfield waited inside, and seemed to be doing his best to ignore the fact that Brubaker had my rifle aimed at his head, his finger on the trigger poised to fire. Since I had not sheathed my blade the entire time Fowler escorted me, I pressed my tip into Renfield's throat. "Start talking. Make it good, and for the sake of those kids your fellow soldiers took, make it quick."

Renfield shook his head. "Those kids are already dead. There's nothing you can do about it. I wasn't going to deny the men their blood."

I leaned into my sword a little, and let the tip taste blood. "Remember what I said about making it good?"

Renfield spat his defiance. "Look, Naomi, you could shove that sword up my ass and it wouldn't change the fact that those kids had it coming. That reclusive motherfucker Yoder had it coming. He led the others down here, and didn't stop them from murdering several of our number in their sleep."

The kids I tried to protect are murderers? That certainly puts this situation in a different light, but how can I be sure Renfield is telling the truth? "Mike, do you know anything about this?"

Mike Brubaker wouldn't look at me, and wouldn't answer until Renfield shook his head. "Tell the lady, kid. This must be the first she's heard of what's really been happening around here lately."

"Renfield's right." Mike still wouldn't look at me, but now I understood why. The kid was ashamed. "I had showed Yoder the entrance once. He led the others down here a month ago, and said something about getting payback for all the people who disappeared in the woods."

Great. I had walked right into the middle of a vicious circle. Locals and visitors sometimes disappear in the woods. Yoder leads some kids he met on the town's secret youth forum to the fort. They murder some Dusk Patrol soldiers in their sleep, without even knowing if they're responsible for the disappearances. Now Dusk Patrol is hunting these people down and making examples of them. And here I am, armed to the teeth and ready to make matters even worse. No wonder Renfield sedated me.

I was still annoyed about being tied up and hung from a hook in a meat locker, but that could wait. Why didn't Brubaker say something? "You must have had a reason for not telling me, but did you at least speak to the Sheriff when you realized what Yoder did?"

"I did. He told me to mind my own business." Brubaker shook his head. "I went to him again after Wilson turned up dead, and told him he had to tell you what was going on. He said he would, but…"

Renfield nodded, and handed me a photograph of two officers and a military policeman. "Instead, he told us. He was sergeant major in charge of the MPs at Fort Clarion back in the day. Mayor Collins was a second lieutenant just out of West Point. We know them, and they look out for us."

And they covered up every disappearance since the resettlement of Fort Clarion. If what Renfield and Brubaker are telling me is true, and those kids are dead, then Robinson and Collins are accessories. But why? And does the reason even matter at this juncture? "What about Dr. Petersen? Where does he fit in?"

"You mean Colonel Petersen? He's still looking out for us, too." Renfield sighed. "He wanted me to shoot you and Brubaker. Him because he might have told you everything, and you because you would expose us. It's what Adversaries like you do. I couldn't do it, or let the others do it either. Brubaker spoke up for us, and you? You're a soldier doing your job, just like us. But when you sided with people who murdered our brothers, you changed everything."

Had I known I'd spend my vacation hacking through years of murderous bullshit, I'd have packed a machete. Next time, I'm damn well bringing one - and keeping a knife up my sleeve in case some asshole ties me up without buying me dinner and arranging a safe-word first.

---

### This Week's Theme Song

"Pretty Tied Up" by Guns 'n Roses, from *Use Your Illusion II*

{% youtube sFbujpjmTzs %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
