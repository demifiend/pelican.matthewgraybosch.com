---
title: "Saturday Scenes: A Blade of Radiant Hatred"
excerpt: "This scene is gonna be metal once I get it right."
date: 2015-03-20T05:44:10+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
tags:
  - '#SaturdayScenes'
  - Adramelech
  - blasphemy
  - Claire Ashecroft
  - defiance
  - duel
  - hatred
  - magic
  - morgan stormrider
  - naomi bradleigh
  - rage
  - torture
  - Starbreaker
  - Blackened Phoenix
---
This is something I'm thinking of using for _Blackened Phoenix_. My wife and I still working out the story, and ​Catherine suggested that the climactic fight be against Adramelech instead of Imaginos.

* * *

Abram Mellech made no gesture, and gave no sign, but a force Morgan could not identify lifted him from his feet and slammed him into the wall. He struggled against the unseen bonds holding him. Naomi drew her sword, and it slipped from her grasp as she fell to her knees; her lip bled as she bit it to keep from screaming from whatever sudden pain had made a flash flood of her tears. Eddie was in no better condition. Even Sid for all his strength and stoicism had fallen, and lay whimpering on the floor.

Only Claire remained standing. Reaching into her purse she drew the monster revolver Morgan had given her as a Winter Solstice gift. Its blued steel barrel flashed beneath the bioluminscent lights, its engraving a warning to all: _Ultima Ratio Regina_ &#8212; the Queen gets the last word. "Is that the best you can do, you choirboy-buggering sack of shite?"

Mellech smiled. "Oh, it's you. I had forgotten that you like pain. Here's some more."

"Sod off." Clenching her streaming eyes against the pain Mellech inflicted without touching her, Claire squeezed the trigger just as Morgan had once taught her. The bullet tore through the false priest's head, leaving wounds that immediately disappeared as if she had never inflicted them. Claire kept firing, emptying her weapon to no greater effect, and fell panting to her knees.

With Morgan's friends neutralized, Mellech approached him, and reached up to caress his cheek. "The Lord empowered me to chastise your friends, little asura, for you all have sinned against Him. But I have left you untouched, because it is the Lord's will that you serve him."

Grasping Morgan's chin, Mellech turned Morgan's head so that he could see Isaac Magnin and Tamara Gellion; the members of the Phoenix Society's executive council they had come to arrest. The case containing the Starbreaker sat on the table before them, the pulsing crystalline blade glittering in its bed of black velvet. "Atone for your sins and those of your friends by taking up the Starbreaker, unleashing it's full power, and striking down Imaginos and Thagirion, who have conspired to deny the Lord His rightful dominion over the life of this world."

It was almost impossible to get the words out with the priest grasping his jaw, but Morgan managed it. "And if I refuse?"

Morgan had heard Naomi scream before, but that had been on the stage, as part of the theatrics demanded of her roles in the rock operas they staged with Christabel as Crowley's Thoth. This was no act, but suffering, terror, and despair given voice. Abram smiled. "Did you know your whore is Imaginos' daughter? Her scarlet eyes mark her as a demifiend, a demon's get. She is stronger than she realizes, which is why she'll be the last to die before I accept that you are so utterly depraved as to be beyond God's grace and mercy."

The last of Abram's words stretched and deepened as Morgan kicked his mind and body into overdrive so that time seemed to slow around him. It was the one ability he had refused to suppress, for it was too useful. _Little asura. Abram Mellech knows what I am as surely as Isaac Magnin and Tamara Gellion do. And I can't beat him as a mere man, or even the man I am now. I can't afford to limit myself any longer._

Morgan barely felt the priest's backhand, despite its impact bouncing the back of his head against the wall. "Will you serve, or deny the will of God?"

One by one, Morgan disengaged the limits he had imposed on the asuric abilities that had awakened too early for him to wield without harming innocent bystanders. An augmented reality driver activated, superimposing over his vision a graphical representation of the energy Abram Mellech somehow drew from the room's Tesla points and used to hijack the nervous systems of Morgan's friends. The same software embedded within Morgan's artificial mind identified his enemy by a different name: the ensof Adramelech.

Without thinking, Morgan mentally reached for the preternatural connections Adramelech had imposed on his friends, wrenched them loose, and redirected them toward him. The induced agony the ensof distributed among four was now a tsunami crashing down on him, but Morgan stood firm, his rage mounting.

Adramelech released him, backing away with slow, careful steps. "Adversary Stormrider, remember that your friends are here. If you aren't careful, you might hurt them as well."

"I appreciate the reminder." Morgan extended his left hand, glaring at Adramelech all the while. Every wrong the ensof had ever done him and his friends, their manipulation of their lives, their corruption of the organization to which they had devoted their lives and pride, and now Adramelech's attempt to use his friends to goad him into killing those he had sworn to bring to justice; he imagined it all focusing, coalescing into a blade of radiant hatred sharp enough to cut down gods. "Is your worthless God watching, Adramelech? He'd better be, because not only will you not live to deliver my answer, but what I do to you I will also do unto him if he even _thinks_ of fucking with my friends ever again. Threaten my friends, and you threaten _me_. _All who threaten me die!_"

The sword he conjured burned his hands as he grasped it, and his body constantly reconfigured tissue to repair the damage he inflicted upon himself. Electromagnetic interference radiated from his sword of deep purple lightning, screwing with his network connection and flashing ghosts across his vision. He sprang forward, driving his preternatural weapon into Adramelech's chest. "No kingdom shall come."

The ensof threw his head back, shrieking his shock and agony as Morgan Stormrider, withdrew the sword of his hatred and struck a second time. "Let _my_ will be done on Earth."

Adramelech staggered backward and threw up a hand to ward off the next blow. Despite the glitches, Morgan saw the layered shield of electromagnetic radiation and compressed air the ensof had between them. He cleaved it with the smile of a killer too long denied for lack of just cause, and brought his blade upward, bifurcating the ensof's body from crotch to crown. It shattered into shimmering dust and faded. _"To Hell with Heaven."_

Morgan cast his blade aside, and it winked out of existence. With barely a glance at Imaginos and Thagirion, who stood now that Adramelech could no longer bind them, he rushed to Naomi and ran diagnostics. Something inside him wrenched as he considered her mangled lower lip. Despite the damage, Naomi smiled as he took her hands. "Want to kiss it and make it feel better."

Despite himself, Morgan laughed. He kissed her forehead instead. "Are you hurt anywhere else?"

Naomi let Morgan help her to her feet, and accepted the sword he retrieved for her. "It was all in my head, but I don't know how he did it."﻿
