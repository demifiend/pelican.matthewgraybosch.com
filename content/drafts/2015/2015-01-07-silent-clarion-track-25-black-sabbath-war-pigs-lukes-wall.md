---
title: "*Silent Clarion*, Track 25: &quot;War Pigs/Luke's Wall&quot; by Black Sabbath"
excerpt: "Check out chapter 25 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Was it rage that made Renfield's hand tremble as he held his knife to my throat, or fear? Either way, I lanced an emotional boil that had festered for years, and his violence was the pus spewing forth. Project Harker must have been the codename of the program that made him and the rest of Dusk Patrol monsters instead of men. But why was he so desperate to preserve its secrets? And why is the Phoenix Society keeping it secret, even from Adversaries?

"You think I'm playing, girl?" Renfield screamed in my face, his eyes bulging as his saliva sprayed across my skin. "I can see your mind working behind those demon eyes. Stop thinking of how you're gonna bullshit me into letting you go and answer my question. Who sent you, and how much did they tell you?"

"The Phoenix Society sent me." Though Renfield surely had the training to completely immobilize me, he didn't do so. He simply straddled my chest, pinning me down with one hand around the base of my throat while he held the edge of his knife under my jaw. Did he think I'd be more willing to spill my guts if he left me a hope of escape?

Unfortunately for him, throwing me to the ground and holding a knife to my throat while barking questions in my face doesn't frighten me enough to make me submit. I brought my hands up, my palms simultaneously striking his temples. Stunned by the blow, throwing him off became a pathetically easy task. I bound his hands behind his back with strip-cuffs as gently as I could; though he had gotten violent with me twice, there was still a chance he might yield valuable testimony if treated carefully.

Taking his knife, I put it behind me where I could retrieve it, but he couldn't. I then picked up my sword and made sure he got a good look at the blade. You'd think he'd be familiar with it by now, since I had drawn on him once already. "I am an Adversary, sworn to root out abuses of power like those you described, and the Phoenix Society didn't tell me a goddamned thing about Project Harker. It's classified. What little I know, I bloody well had to figure out on my own."

He stared at me, the muscles in his arms bunching and twitching as he tried to houdini his way out of the strip-cuffs. "What the hell is the Phoenix Society?"

If he can ask a question like that, then I probably didn't give him a concussion. Still, it might be wise to phrase the answer in terms familiar to him. "Before Nationfall, there was a watchdog group called the North American Civil Liberties Union, wasn't there? We're a more militant version of the NACLU. We don't just file civil suits when somebody alleges an individual rights abuse. We make arrests and put tyrants on trial."

Renfield nodded, and relaxed a bit. "So, am I under arrest? I've assaulted a Phoenix Society officer twice, haven't I?"

"You have." Which was very naughty of him, but saying so would be too flirtatious. "However, I'm prepared to overlook both incidents if you cooperate with me and tell me what I need to know. I'll even cut you loose."

He studied me a moment. "Why are you even here asking about Project Harker if your bosses wouldn't tell you anything? Are you even supposed to be here?"

A truth for a truth. "I was on vacation. I heard about people disappearing around here while getting a drink back in Manhattan, and got curious."

"And then you found the Fort with that local kid."

"How did you know?"

He averted his eyes, as if ashamed. "I saw you two through my scope. Colonel Petersen told me about you and even gave me a pic. I don't think he realized we had already met."

Son of a bitch. What the hell is Petersen's game? How am I supposed to square his behavior with Renfield's characterization of the man as an officer? Is he still just looking out for his troops? "Why am I the primary target? And why didn't you shoot me when you had the chance?"

He still wouldn't look at me. "The Colonel knows more about Project Harker than I do. He knows everything, and it scared the shit out of him. All I know is that we've got to keep the secret. It's the only way to protect the rest of the outfit, and make sure what happened to us never happens again."

His eyes swam with unshed tears, and his voice took on a pleading tone. "I didn't shoot you because something told me you'd understand if we could just talk."

"We're talking now, but you still attacked me. You can make up for it by telling me more. For example, where's the rest of your outfit? Who keeps the base ready for an inspection by a platoon of coked up martinets?"

Unfortunately, that last bit didn't get so much as a chuckle out of Renfield. "We still live at Fort Clarion, but I can't tell you more than that. It isn't safe for them or for you and your men."

That pleading tone was still there, if not more intense than before. If he's this desperate, then maybe I've gotten everything out of him that I can. Maybe he really doesn't know anything else about what was done to him. "Did you ever try to find out about Project Harker for yourself, so you could find a way to reverse what the Army Medical Corps did to you?"

"I'm just a soldier. Colonel Petersen let me have a look at some of the files, but it was all Greek to me. All I caught were a couple of names that kept standing out."

Names are good. Names are leads that I can track down for more answers. "Can you tell me what names stood out to you? Do you remember?"

Renfield nodded. "Like it was yesterday. The first was 'asura.' The reports kept using that word to refer to people like us."

People like us? Did he mean people with CPMD? We're all power-seeking deities from Hindu myth? Not that I felt like a goddess as I shivered despite the fire and forced myself to ask the next logical question. "What else?"

"The project had a civilian consultant. At least I think he was a consultant. Some guy named Ian Malkin."

"You'd better not be bullshitting me, Sergeant." The warning sprang from my lips before I realized it. Was Renfield referring to Dr. Ian Malkin, who reputedly worked for both Ohrmazd Medical Group and the AsgarTech Corporation to develop the first safe implants? If there was any evidence that as prominent a person in medicine and biotech as Malkin was involved in unethical clandestine military experiments, then this case just got a *hell* of a lot more complicated. "I've seen video of the man. He can't be a day over thirty."

"He's one of us, Naomi."

Which meant he could be old enough to claim Gilgamesh as a drinking buddy, but not look it. God damn it. This shit just keeps getting deeper, and I've got no one to throw me a rope so I can pull myself out. "Where can I find evidence to back up what you've told me? Does Petersen keep any documents in his home or office?"

Renfield shrugged. "No idea. But you wanted to see Fort Clarion at night, didn't you? That's why you came out here in the first place. You knew something was up as soon as you got a look inside."

"Yeah." How much time did I have left before dawn? It was two in the morning. "Someone's there, but I didn't see anybody during the day."

"You wouldn't. We only come out at night. If people saw us walking around during the day, it would blow the mystique. We scared everybody, not just the enemy. Hell, even some of my own guys actually believe they're honest-to-God nosferatu." He averted his eyes again as his voice faded.

Acting on instinct, I sheathed my sword. I used his knife to free him, and embraced him from behind as I sheathed his weapon. "It wasn't just physical release for you the second time, was it? It was emotional, too. It overwhelmed you." It wouldn't have been the first time I've seen a man's emotional control crumble in bed.

Renfield blinked away a tear. "It's been so long since anybody has touched me. I lost control. And you seemed to come harder as I bit into you. I didn't mean to drink from you afterward. I'm sorry, Naomi, and I don't expect you to forgive me. I don't deserve it."

Maybe he doesn't, but I've got enough to deal with without carrying a grudge over something that will probably heal up in a couple of days. Despite his lapse, he seemed like a genuinely decent person, somebody worth knowing. "I'm going to forgive you anyway, but I want a promise from you. The next time you taste a lover, it has to be with their consent."

His lips crooked in a half-smile. "You think there are women who will let me get my vamp on with them?"

Was he a virgin before me, or something? "Man, you have no idea how kinky some people can be. Just keep it safe, sane, and consensual so I don't have to kick your ass again and notify you of your rights."

"Fair enough." He chuckled. "Any other conditions?"

Now that I think of it, there was something else. Something that would make a relationship with Renfield viable if that's what we wanted after this was all over. "I think the life you've had to live so far, all the isolation and pretense and secrecy, has wounded you. I think you should talk to a shrink. If you cooperate with me for the duration of my investigation, I might be able to get you help through the Phoenix Society."

"Do you think I'm crazy?" Great. I should have remembered that his society stigmatized mental illness. "I managed to hold it together so far without any help."

"And how long are you going to keep carrying that burden?" Why did I even care? I'd have to be crazy to even consider getting involved with him after this case. Sure, he was hot and pushed all my dials past the red line, but that was never a reasonable basis for a relationship. "Would you insist on holding a position on your own if somebody was ready to relieve you?"

Renfield shook his head, his expression taking on a stubborn cast. "Do you think I'm crazy or not?"

Fuck it. The record will show I *tried* to be reasonable. "I think you're out of your demon-ridden mind, but I'm not one to judge. Insanity is both a prerequisite and an occupational hazard for Adversaries like me. That's why we get therapy after every mission. It keeps the crazy on a nice tight leash."

To my surprise, Renfield threw back his head and laughed, his mirth echoing through the night. Before I could stop him, he kissed me hard while plunging his hands into my hair. "Then let's run mad together when this is over. I don't know how much help I'll be, but I'm your man if you'll have me."

That sort of talk could get one in trouble. "Can you escort me to Fort Clarion? I need to see for myself what goes on over there."

He nodded, and turned toward the fire. "I can do that, but I can't let you inside. It isn't safe at night. They don't know you." Crouching, he gathered up a handful of the earth he had dug up earlier, and threw it atop the coals. "First, let's get this fire put out."

---

### This Week's Theme Song

"War Pigs/Luke's Wall" by Black Sabbath, from *Paranoid*

{% youtube LQUXuQ6Zd9w %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
