---
title: "*Silent Clarion*, Track 56: &quot;Flash of the Blade&quot; by Iron Maiden"
excerpt: "Check out chapter 56 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Despite the weariness, I couldn't just ignore my misgivings concerning the presence of GUNGNIR overhead, take Gevurah's advice, and curl up for a catnap. Not when I had people looking out for me. Not when they were counting on me to look out for them. Instead, I sheathed my sword and climbed onto the roof of the PX.

｢Are you trying to get a better view?｣ No doubt Gevurah objected. ｢That's unnecessary. I've activated the base's CCTV network, and am forwarding the audiovisual feeds to you.｣

Sure enough, I had an overlay in one corner of my vision showing the output of cameras around the base. Which was well and good, except it wasn't the view I wanted, but the visibility. Let Robinson and the others see me. Let them come for me. Let them see the treats I have in store for them.

Though such thoughts sounded like madness, I embraced them as a means of psyching myself up. After all, one against thirty… Yeah, I was officially nuts.

No doubt Gevurah reached a similar conclusion. ｢If you survive this, you should undergo psychiatric evaluation. Taking on thirty enhanced soldiers with only a sword and a sniper for support places you firmly in what Binah would call "too stupid to live" territory.｣

｢And fuck you, too, Gev.｣

｢Do you not realize you're making a target of yourself? This is borderline suicidal.｣ Wow. You'd think Gevurah actually cared about me, insofar as the AI who dedicated himself to the Phoenix Society's security gave a toss about anybody.

Besides, the odds weren't quite as bad as Gevurah made them out to be. I had swords. I had a rifle. My jacket wasn't full armor, but it would offer some protection. It wasn't like I meant to fight them in nothing but my knickers—though that might distract them a bit. ｢If I can keep their attention on me, they won't go after my witness.｣

｢Fine. It's your funeral. And a moot point at the moment, since I'm not picking up anything.｣

Where the hell are they? I'm starting to feel like the ugly girl in a teen drama. You know, the one who gets asked out by the incredibly hot, popular, athletic bloke—only to get stood up in favor of the Head Girl or the Homecoming Queen, depending on where the movie is set?

｢About bloody time they showed up.｣ I wondered if a little song and dance might hurry them up. Perhaps Handel's "Come Get Some" chorus in D Major? Or maybe "You're My Bitch (And You Might as Well Accept It)" by Doomed Space Marines?

｢They're pairing off. Looks like they mean to cover the base in two-man teams. First team is through the fence.｣

And here we go. I shivered a little. It felt a bit like stage nerves. Not quite fright, but just my body's acknowledgement that it was showtime. And right on cue, there was one of the pairs now, slowly advancing.

More followed, none of them approaching my position. Surely they could see me. Couldn't they? Didn't they have the advantage of numbers and superior training in night fighting?

They did. They knew it. More importantly, they were fully aware that I knew it. No doubt they reasoned that I wouldn't face them here, at night, unless I possessed the means to neutralize their advantage. Or did they know I was bluffing, and waited until I had gotten complacent and sloppy to call me on it?

Fuck this. I'm going after them. Once I had reached the ground, I headed straight for them since they didn't carry rifles. They didn't even have swords. Just knives, which were only dangerous up close.

There was no need to run and wear myself out. A slow walk was fine. Besides, it was traditional. Two opponents approach one another at a measured pace, each sizing up the other. Normally, the fight was a straight duel, but I'm easy. I don't mind taking on two at a time. Those odds were almost fair, to them.

Besides, I had done it once before and only needed stitches. One of them knew it. He smiled as he drew his knife. "Ready to die this time? Those guys you offed last time were barely good enough to join the Patrol."

"Whether you're better than the last idiots to face me remains to be seen." If he wanted to taunt, I'd play his game. Letting him get a look at my blade, I favored him with my most sadistic smile. "But I bought a new sword, just for you. Be a dear and give me an excuse to cut you."

His companion spoke up. "You think you're some kinda samurai? How about I shove that sword up your ass instead?"

"I'm an Adversary," I readied my blade. "And I'd love to see you try."

The one who wanted to bugger me with my own sword reached me a split second before my old friend, and paid for it as I sliced open his belly and spilled his guts across the tops of his boots. Leaping back as he collapsed saved me from the other man's edge.

"I had a feeling you'd fuck him up. He always did rush into things." The soldier I had faced before picked up his partner's knife, and wielded one in each hand. "The name's John Atherton. Thought you deserved to know the name of the man who's going to kill you."

"Naomi Bradleigh." Bloody hell; he even looked a bit like my asshole ex. "Drop your weapons and surrender, and you'll be spared. I'm here for Robinson. You're just following orders."

"Generous of you, but I'll take my chances." Atherton tried a thrust, and sliced nothing but air.

No doubt he hoped to draw me out, but I wasn't about to strike just yet. While his fallen companion had tried to rush me and impale me on his knife's tip, Atherton was more careful, and his technique suggested greater proficiency.

He kept attacking with the knife in his right hand, as if he wanted me to focus on that blade to the exclusion of the one in his left. If I fell for it, he'd have me.

His beetled brow and gritted teeth suggested he realized I saw through his tactics. "Stand still and fight me, woman. You've got a fucking sword, for shit's sake."

Talking was a mistake. Though I made to strike for his right hand, it was a trick. He fell for it, and I lopped his left off at the wrist. "Better put that on ice, John."

"Go fuck yourself." He ground out as he let rage and pain overcome his training and rushed me.

Though I had intended only to slash open Atherton's throat, I must have underestimated my own strength, his momentum, or the blade itself. It sliced clean through his throat and spine, and his head rolled off. Oops. Hopefully Atherton wasn't a good friend of Renfield's.

After wiping my blade on his uniform, I sheathed it and fled the area. Fortunately, the man I had gutted had passed out during my danse macabre with Atherton. He wouldn't be able to tell his companions which way I ran.

Though I had the CCTV feeds, they were disorienting—especially when the system rotated to a camera aimed at me, so that I watched myself in real time. Instead, I sought a rooftop for a better view. Two pairs of men linked up, and began patrolling together despite initial orders. A shot rang out as they happened upon my first victims, and one of the, fell to one knee.

"Where is that bitch?" The stricken soldier's shout was as clear as if I was right behind him. If they had seen a muzzle flash from Mike's rifle, they would've probably headed for the tower. Instead, they're still searching. I unslung my rifle and fired a shot of my own. Might as well help them along. My rifle didn't have a flash suppressor, so I was nice and visible.

"There she is!"

"She wants us to come after her. Stick to the plan. Stay visible, keep her on edge, but do not engage. We can move in for the kill once's she's worn out and frazzled."

Don't these stupid gits realize I can hear them? ｢Oi, Malkuth! Want to help me take the piss out of some soldiers?｣

｢What have you got in mind, Nims?｣

This was why I liked Malkuth best. ｢Does Fort Clarion have a PA? Can you patch me into it?｣

｢Direct audio feed? You'd need a handheld.｣

Too bad I don't have one of those. ｢Can you run my texts through a speech synthesis algorithm?｣

｢And make it sound like you?｣

Hmm… Now there was an interesting question. Sure, I could make it sound like me, but surely the Sephiroth possessed sufficient processing capacity between all ten AIs to synthesize other voices recorded via Witness Protocol. Couldn't they? ｢Any chance you could spoof Sheriff Robinson's voice?｣

｢Yeah, I think I can manage. You ready?｣

Oh, yeah! I sent the message, and seconds later, Sheriff Robinson's voice boomed across the base. "All troops, stand down! The Bradleigh bitch has agreed to settle the matter by single combat."

Of course, Mike had no idea what I had in mind. ｢What the fuck, Naomi? Didn't you hear Dr. Petersen? Robinson's augmented. He'll kill you.｣

Mike was right. Robinson might kill me, but I didn't intend to let him. Besides, I told Renfield I'd try to avoid killing more of his men. Going directly after their leader was the best way to keep that promise.

Perhaps Robinson retained some shred of decency after all, because here he was at the gate. Better go down and meet him. Wait, was that a rifle? Was he aiming that at *me*?

---

### This Week's Theme Song

"Flash of the Blade" by Iron Maiden, from *Powerslave*

{% youtube oYKhK1aaVEQ %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
