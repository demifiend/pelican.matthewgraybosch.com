---
title: "*Silent Clarion*, Track 42: &quot;I Am Become Death&quot; by Makeup and Vanity Set"
excerpt: "Check out chapter 42 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
With Cat Tricklebank safely in her truck and headed east, it was time to deal with her husband's mainframe. Or better yet, have Malkuth do it. ｢Mal, I need you. Again.｣

｢Be still, my heart. Is this just to evac another witness?｣

With secure talk being a plain-text medium, it was an unfortunate impossibility to tell whether Malkuth was being his usual flirty self, or had taken a sarcastic turn. ｢Nope. How would you like to have a go at cracking the last working Multics installation on Earth?｣

｢Do tell.｣ Was that curiosity?

｢It's a pre-Nationfall General Atomic mainframe. Model GA-65535. Matt Tricklebank has been renting out space on it since he took it over during resettlement of Clarion twelve years ago. Even the local government use it, so I wouldn't be surprised if Dr. Petersen had an account. There should be lots of little treats for you. Surely the sysadmin's murder is probable cause for a bit of snooping.｣

｢What's your basis for treating the admin's death as a murder?｣

Malkuth should damn well know why, considering what Cat said about her husband refusing to hand over log files without a warrant. ｢I guess you stopped monitoring my feed. Tricklebank refused to hand over IRC logs, and his wife says Robinson, Collins, and Petersen threatened him. There's got to be something on that machine they want buried.｣

｢And now you're going to dig it up. Dumb motherfuckers should have just bribed the guy.｣

No way I could argue with that, though bribery brings its own risks. Individuals who are sufficiently unscrupulous to accept bribes but honest enough to stay bought are a rare commodity. I've never met one. ｢So, can you help me get into Tetragrammaton?｣

｢You've completed your inventory of Fort Clarion's armaments. We can't justify your presence in Clarion any longer.｣

｢After everything I've turned up in the process?｣

｢You've gotten pretty far on probable cause and circumstantial evidence, Naomi, but without an official complaint -｣

｢Never mind all that.｣ What does he want, forms in triplicate filled out by hand? ｢Review my Witness Protocol feed. His wife contends they came to their place of business in civilian clothes to make their threats.｣

｢Not for nothing, Nims, but you're pushing the limits. You're walking on lines you dare not cross.｣

Lines I dare not cross? Seriously? ｢This isn't a police procedural drama, Mal. Does somebody further up want me to back off?｣

A long pause before Malkuth replied. ｢All I can say is that what you think you're discovering is already well-known. Things are as they are in Clarion for reasons I cannot explain because the people who want it this way didn't explain themselves to me.｣

It was hard to believe what Malkuth had just said. To think that somebody highly placed within the Phoenix Society would be aware of the situation, and do nothing about it, was intolerable. That's not the Society's purpose. That isn't my purpose as an Adversary. ｢If I have to face a court martial when it's over, I will, but these murders and disappearances need to stop. I can't turn my back.｣

｢I suspected you'd say that, and I'm not unsympathatic.｣ What was Malkuth risking by helping me? Could he be put on trial? How would we go about doing so, when his hardware probably makes Tetragrammaton look like a handheld? ｢I'll do what I can, but for now you have more pressing concerns. The Fallen Angels escorting your witnesses have dropped off the network.｣

I stopped being miffed about not being able to get access to Tetragrammaton immediately. This took priority. ｢Could they just be maintaining radio silence?｣

｢That's not standard practice. I'm transmitting their last known location. Find out what happened, Adversary Bradleigh.｣ The connection cut out after Malkuth sent the Fallen Angels' location as a set latitude and longitude values.

I plugged them into my GPS app, and got a location: about 20km east of here. I'd need my motorcycle. Sprinting back to The Lonely Mountain, I found Mike Brubaker where I had left him. "I need you to come to my room with me."

"But-"

"Not now." Fortunately, he didn't make me drag him up to my room by his collar. Nor did he ask questions as I opened the closet and yanked out the case of goodies Nakajima sent me care of Eddie Cohen. Opening it, I pulled out the armor and found the bodysuit I was supposed to wear beneath it along with an owner's manual. "Something happened to the escort guarding your friends. I need to check it out. I need you to stay here."

"I should come with you."

"Not happening." I ducked into the bathroom to change, since I wouldn't be able to wear the armor over my regular clothes. When I came out a few minutes later, Mike had most of the pieces arranged on the bed.

He pointed at the boots on the floor. "Step into those first. Then spread out your arms."

I did as instructed, and let him attach the rest. "Have you done this sort of thing before?"

"My big brother played hockey. He was a goalie." He didn't elaborate on why he used the past tense, but continued to attach each piece until all that remained was my helmet. Without asking permission, he found one of my elastic hair bands and used it to tie my hair into a bun before handing me my helmet. "Now we just need to strap on your weapons. Did this come with some kind of harness?"

"In the case."

Mike soon had me rigged, and I handed him my sidesword. "If anybody other than me or the Halfords come through this door, stab them. You don't have to reload a sword."

He drew the sword and tried a thrust; his form was terrible, but at least he knew that holding the sharp end is the other guy's job. "Got it."

Locking the door behind me, I found my motorcycle and fired it up. Seconds later, I left Clarion behind. The engine beneath me purred as I cut through the night and followed the map to the Fallen Angels' last known coordinates. The road was empty, and the cycle's lights were the only relief from the cloudy gloom.

With half a kilometer left to my destination, I pulled off the road and hid my motorcycle. If the bus and its Fallen Angels escort had been attacked, I didn't want to ride into an ambush. Choosing every step with care to avoid detection, I crept forward and found my worst fears confirmed.

The bus was a smoking, ruined hulk of twisted metal. It lay on what had once been its roof. The damage to the nearby road pointed at landmines, or perhaps an improvised explosive device. The Fallen Angels fared no better, but they died with empty guns and broken swords. Whoever killed them had hacked Sergeant Jackson's head from his body and mounted it on a spike. As if this desecration were insufficient, they also stuck a rolled up note in his mouth.

It read, "If you're as smart as you think you are, Ms. Bradleigh, you'll get back on your motorcycle and forget this place even exists. The kids who invaded Fort Clarion are ours now. You cannot save them."

"The bloody hell I can't!"

"Naomi? Is that you?"

Despite the voice sounding like Renfield's, I drew my swords. Even if it wasn't an imposter, there's no guarantee he's trustworthy. I hadn't heard from him since the last night we met. "Who's asking?"

A figure stepped out of the trees, showing empty hands. His brooding expression quickened my pulse, for he had worn it before I took my pleasure with him. He appeared untouched by the violence around me, his uniform spotless and freshly pressed. "Christ, Naomi. It's me, Chris Renfield. Remember that night in the the old basement, when I started to seduce you and you turned the tables?"

Oh, I remembered. And that made me vulnerable. "Not taking any chances. You're going to have to convince me."

He was on me before I could draw one of my unfamiliar swords, pinning me against a tree. He tore my helmet off, and I couldn't see where it rolled as he kissed me breathless. Desire, no, lust threatened to overwhelm me. Never mind that there were kids who needed my help, Renfield had set me ablaze and he was the only one who could quench the flames. From the hard heat pressing against me, he felt the same, and I shivered with the memory of feeling that part of him and taking possession.

As if that weren't enough, there were the teeth. His fangs, gently scraping the tender skin of my throat where he had bitten me last time. This time, I wanted him to bite me. I wanted his cock in me, and my blood in his mouth. How sick was that? "You know, way down deep, that I'm the real deal."

He was the real deal all right. He was all wrong for me, and I bloody well knew it, but at the same time he already had me at the point where the slightest touch might set me off. Who is this bastard, that he could manipulate me like this? Pushing him away, I drew both my swords to keep him off me. "What the hell are you doing here, Renfield? Why should I believe you didn't have a hand in this massacre?"

He shook his head, unable to meet my eyes. "Naomi, I'll swear by anything you call holy I wasn't involved. One of the men, Corporal Seward, rallied the other men to his side. He's been stirring them up, saying it's time we stopped living in hiding and took Clarion for our own. I tried to tell them it would only get us killed, but they won't listen to me."

"He's insane! Right now, he's just dealing with a single Adversary. A lone sword. If he moves openly against Clarion, he'll bring the full force of the Phoenix Society down on Dusk Patrol. They might not care about me, but they'll protect Clarion just to retain legitimacy."

"You know that. I know that." Renfield spread his hands in a gesture meant to calm me down. It only pissed me off. "Seward still thinks that if this turns into a clusterfuck, they can just retreat underground."

"If Seward attacks Clarion, your men will indeed be going back underground. If they're lucky, the Society might even bother to mark their graves."

"Seward said something about how the Colonel was still looking out for us, and had root on something called Gungnir. I don't know what he means, but -"

"I do, and it's bad." If Dr. Petersen has access to the GUNGNIR platform, then we're in deep shit. How the hell do you fight against tungsten lances falling from orbit at terminal velocity? ｢Malkuth, are you paying attention? If this isn't probable cause to hit Tetragrammaton, I don't know what is.｣

Instead of waiting for Malkuth's reply, I returned immediately to Renfield. "I need your help to get inside Fort Clarion's underground. If those kids are still alive, I have to rescue them."

Renfield nodded. "I know a shortcut to the fort from here. I can show you a back door, but for fuck's sake, put your swords away. Having all that naked steel behind me makes me nervous."

I did as he requested, but unslung my rifle instead. "If you have any objection to me shooting anybody who gets in my way, you'd better tell me now. I won't have time to pretend I care once we're under fire."

Renfield didn't say anything. Instead, I felt a pinprick on the back of my neck, in the gap between my armor and my hairline. I tried to whirl on him, to gun him down for his betrayal, but whatever he injected me with took effect too swiftly. My legs collapsed beneath me, leaving me an insensate heap.

---

### This Week's Theme Song

"I Am Become Death" by Makeup and Vanity Set, from *Charles Park III*

{% youtube JqMmtUFY8Cg %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
