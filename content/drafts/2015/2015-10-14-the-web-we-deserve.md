---
id: 1967
title: The Web We Deserve
date: 2015-10-14T11:50:18+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1967
permalink: /2015/10/the-web-we-deserve/
sw_cache_timestamp:
  - 401676
medium_post:
  - 
snap_MYURL:
  - 
snapEdIT:
  - 1
snapPN:
  - 's:229:"a:1:{i:0;a:8:{s:9:"timeToRun";s:0:"";s:9:"apPNBoard";s:18:"224054218900737052";s:10:"SNAPformat";s:15:"%TITLE% - %URL%";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doPN";i:0;}}";'
snapRD:
  - 
snap_isAutoPosted:
  - 1
nc_postLocation:
  - default
twitterID:
  - MGraybosch
bitly_link_linkedIn:
  - http://bit.ly/1LtY1wG
bitly_link_facebook:
  - http://bit.ly/1LtY1wG
snapFB:
  - 
snapTW:
  - 's:295:"a:1:{i:0;a:10:{s:4:"doTW";s:1:"1";s:9:"timeToRun";s:0:"";s:10:"SNAPformat";s:15:"%TITLE% - %URL%";s:8:"attchImg";s:1:"1";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:18:"654323587344547844";s:5:"pDate";s:19:"2015-10-14 15:50:58";}}";'
snapGP:
  - 
bitly_link_twitter:
  - http://bit.ly/1LtY1wG
bitly_link:
  - http://bit.ly/1LtY1wG
bitly_link_tumblr:
  - http://bit.ly/1LtY1wG
bitly_link_reddit:
  - http://bit.ly/1LtY1wG
bitly_link_stumbleupon:
  - http://bit.ly/1LtY1wG
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Social Media is Bullshit
tags:
  - abuse
  - cyberbullying
  - open Web
  - social media
  - toxic environments
  - walled garden
  - Web 2.0
---
I can't help but think that we brought this on ourselves. We bought into Web 2.0. We signed up for sites like Reddit, MySpace, FriendFeed, Facebook, Twitter, and Google+.

Why? Why weren't blogs good enough for us? Why weren't discussion forums good enough? Why weren't RSS feeds good enough? What did modern social media have to offer that the older technologies (which still work) couldn't provide?

Were ease of access and ease of use the only criteria?

<div class="g-post" data-href="https://plus.google.com/+SusanStone/posts/1tbqnRrro9H">
</div>

We got the World Wide Web we deserved when we bought into Web 2.0 and replaced the open web with new walled social media gardens. We got it good and hard.