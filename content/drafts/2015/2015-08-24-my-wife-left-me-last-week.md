---
title: My Wife Left Me Last Week
excerpt: I miss Catherine already, but Ken Liu's *The Grace of Kings* provided a measure of solace.
date: 2015-08-24T13:57:49+00:00
header:
  image: catherine-smile.jpg
  teaser: catherine-smile.jpg
categories:
  - Personal
tags:
  - Australia
  - caregiving
  - Ken Liu
  - left behind
  - loneliness
  - marriage
  - separation
  - The Grace of Kings
  - travel
---
My wife left me last week. She got a one-way ticket back to Australia, and a ride to the airport from me. It's not as bad as it sounds. We didn't fuck up our marriage. At least, I don't think we have.

So, what happened? Her father lapsed into a coma and had to be rushed to the hospital. We got a call Saturday night, our time, from my wife's sister. She asked Cat to come home as soon as possible, and shared what few facts she had.

Once I helped my wife through her initial shock, I dipped into the savings we had started putting together, and took out enough to cover a one-way ticket home. Just one. I'd hold down the fort here, take care of the cats, and keep working. It wasn't possible for us both to go, especially indefinitely.

It wasn't the first time I've let my wife travel without me to visit family abroad. We either didn't have the money for us both to go, or I couldn't take time off without risking my job. So I've been apart from her before. Hell, the four years before we married were spent mostly apart.

But this time was different. When I had gone to meet my wife for the first time after two years of courtship by email and phone, I knew up front that it was only a visit. Likewise for when I had to let her travel without me. I had had time to get used to the idea of a temporary separation.

I kept it together until I was safely home from dropping her off at the airport, but suddenly being alone hurt. We had just put a ricotta cheese and spinach pie that she made into the oven when the call came; we were supposed to share it. Instead, she had only one slice before her departure, leaving me with three quarters of a pie to eat alone.

Normally I'm greedy for Cat's pie, and must be careful not to [bogart](http://www.urbandictionary.com/define.php?term=bogart&defid=247418) it. This time, however, it was entirely too easy to pace myself.

Cutting the smallest pieces I could manage without having the slice fall apart, I tried to stretch the pie as long as possible. I wasn't sure when Cat would be back to make another, you see. I managed to make it last an entire week, and had the last piece this morning.

I probably haven't coped particularly well so far. Sure, I'm keeping it together and doing my job. Sure, I'm still writing. But did I put anything on my blog last week? No.

Instead, I've sought refuge in books as usual. I saw that the Kindle edition [Ken Liu's](http://kenliu.name) [_The Grace of Kings_](http://kenliu.name/novels/the-grace-of-kings/) was available for [$1.99 on Amazon](http://www.amazon.com/The-Grace-Kings-Dandelion-Dynasty-ebook/dp/B00KU4O1CY), and grabbed it. Why not, right?

{% include base_path %}
!["The Grace of Kings" by Ken Liu]({{ base_path}}/images/kenliu-graceofkings-cover.jpg){: .align-center}

I'm not sure what others got out of Liu's tale of a rebellion against a corrupt empire led by a kind-hearted bandit (Kuni Garu) and a heroic scion of deposed nobility (Mata Zyndu) that became a war over whether to unify the islands of Dara properly or try to restore the "good old days" of independent and frequently warring states. However, the subplot concerning Kuni Garu's relationship with his wife Jia, their separation and mutual infidelities, and their reunion and reconciliation proved oddly comforting.

I don't think I'd ever turn to somebody else while my wife was away, and I don't think she'd do so either. And it's not like she's gone off to war while I stay home and mind the kids. She's just being there for her family while I go to work, take care of the kitties, work on _Silent Clarion_, and play _Dark Souls II_.

Regardless, it's strange to actually _feel_ loneliness again. And even though we text and talk using [VoIP](https://en.wikipedia.org/wiki/Voice_over_IP) apps, I still miss her. Being able to hear her voice isn't the same as waking up early in the morning, turning over, and spooning with her. It isn't the same as feeling her breath as she whispers in my ear. It isn't the same as being able to bury my hose in her inky dark curls and draw her scent into me.

I miss my wife. It's really that simple. But moping like I did most of last week isn't going to bring her home any faster. It'll probably just make her worry, and she's got enough of that to do on her father's behalf. No sense adding to her burden.

It was while washing Cat's Pyrex pie plate for her that I came to my senses. The pie is gone, but the memory (and the calories) remain. But Cat always comes back, and when she wants to make pie again the plate will be ready. Now to finish the rest of these dishes.
