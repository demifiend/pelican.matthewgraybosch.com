---
title: "*Silent Clarion*, Track 29: &quot;Dead Men Tell No Tales&quot; by Motörhead"
excerpt: "Check out chapter 29 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
A cold shower, clean clothes, and the prospect of one of The Lonely Mountain's hearty breakfasts was just what I needed. Though still tired, I'd be able to get through the day even if I had to go to the station with Sheriff Robinson after we had a look at the victim in Dr. Petersen's basement. The little black kittens sitting on the table with me, Dante and Virgil, didn't even wait until Bruce Halford set my plate in front of me before beginning their chorus of purr-begging.

"Be patient. I'll save you some bacon." And if I forget that, they could have some of the excess fat off my steak, or the remnants of my omelet. I really shouldn't encourage them, but they were so sweet together.

Unfortunately, Sheriff Robinson's arrival frightened them off. He sat in front of me with a mug of coffee he must have gotten at the bar. "Going to save me anything?"

"Are you a kitten?" He didn't look like one. His lean, hungry aspect was more vulpine than feline; Shakespeare would have thought him a good Cassius. Besides, there was no way in Tartarus I'd let him rush me through breakfast when I was still waiting for the Society's warrant. The victim wouldn't go anywhere; the dead are cool like that.

He shrugged off my question, which was fair enough. It was a bit silly. "Did you get your warrant?" Robinson showed me his before I could answer. To get one this fast, he must have some serious dirt on the judge.

Of course, I wasn't about to imply anything of the sort. "Still waiting. The process is a bit involved."

A secure talk session from Malkuth popped up, and I pressed my fingertips to my ear to show Robinson I was getting incoming comms. ｢I've got a warrant for you to search the Clarion morgue under Petersen Family Medicine and Physical Therapy, and examine the body of one Scott Wilson, an eighteen-year-old male of northern European ancestry.｣

True to his word, my implant alerted me to an incoming document a moment later. The authority granted was a bit narrower than I would have preferred, and no way could I stretch it to include searching the entire premises or copying the data on computers located therein. It was up to me to prove Peterson's culpability with what authority I now possessed. ｢Got it. Anything else?｣

｢Only that I'm a bit jealous of Renfield. How many rivals for your affections have I got, anyway?｣

Of course, he would have seen that. I was technically on duty until I've accomplished my mission, so everything I see and hear gets recorded. Everything. Not that I was the first to screw around on the job. I just can't believe he was so gauche as to mention it.｢Malkuth, just a bit of reference for when you do get a body: never ask a woman about her previous affairs. If she's with you now, that's all that matters.｣

I ended the secure talk session. "Sorry about that, Sheriff. I've got my warrant, and I'll be finished shortly."

Robinson gave my admittedly large plate a dubious look. "You barely started."

"Challenge accepted." I set about demolishing my breakfast, chowing down with the brutal efficiency I learned in ACS instead of savoring every bite like a civilized human being. The Sheriff sipped his coffee, and stared at me throughout my performance.

I finished by downing my coffee, and raising my mug skyward for a refill. Halford came by a minute later, bearing a fresh pot. He filled Robinson's mug as well. "You really were hungry, Adversary. Think you can fit anything else in?"

Robinson grumbled something about a wafer-thin mint.

He'll pay dearly for that, but for now I let it go. "Thanks, Bruce, but I'll just take the check. I'm keeping the Sheriff waiting."

Nurse Thorvaldson seemed surprised by my return. "Hello again, Adversary. Is something wrong with your wound?"

Shaking my head, I sent my warrant to his implant. "I'm here to inspect the homicide victim Dr. Petersen currently has downstairs. Sheriff Robinson is here with me, and has a warrant of his own. Is Dr. Petersen available?"

Here I was, about to serve a search warrant, but serve seemed the wrong word. If anything, the owner of property targeted by a search warrant could argue that those serving him said warrant were doing him a disservice.

Fortunately, Dr. Petersen's prompt arrival drew me out of linguistic musings, for which I wasn't properly qualified. "I understand you brought warrants."

"Yes, doctor." I sent mine to his handheld while Robinson handed his over.

Petersen scanned the Sheriff's before reading mine on his device. "Well, everything's in order. Might I ask that you put on gloves and masks before entering the morgue? Standard safety precautions."

"Of course." Robinson and I left our swords with Thorvaldson before finding smocks and putting on the required protective gear. Petersen led us down into the cold dark. A flick of a switch relieved the gloom, but did nothing about the chill air.

Dr. Petersen led us to a body bag on a gurney. A tag attached to the zipper read, *c*. Instead of opening the bag for us, he withdrew after pointing to a sleeping laptop. Was it my imagination, or did his expression seem just a touch resentful? Perhaps he wasn't used to being second-guessed. "My report is on the computer if you wish to read it. See Nurse Thorvaldson when you're finished or if you have any questions, and he'll fetch me."

Robinson watched Petersen leave before turning to me. "You ever do anything like this before?"

"Only in training. They'd bring us to the city morgue and have us examine actual murder victims. We were evaluated based on how accurately we determined the time and cause of death." I unzipped the bag and spread it open to expose Wilson's body. "This is the first time I've had to check up on an autopsy."

Robinson nodded. "Same here." He spared me a wry smile. "At least you aren't just hassling us cops."

Rather than dignify that remark with a response, I turned my attention to the deceased. My heart sank as I recognized his face, along with the marks on his neck, shoulders, wrists, and thighs. Was his death my fault? Did he die for his involvement in my mission? "He was one of the youth volunteers who came with us to Fort Clarion."

"What the fuck are These? Bite marks?" Robinson sounded as disgusted as he looked as he draped the bag back over Wilson's groin. "Jesus Christ, he's even got bite marks on his…"

"Yes, I noticed."

"Was this some kind of sex ritual gone wrong? Why would Wilson have let somebody do something like this?"

Ignoring his question, I spread open the body bag and took another look at the bite marks. "Is there a ruler around, or a tape measure?"

"What, you want to measure him?"

Now that was just nasty. "Not him, but the bite marks. I don't think they were all inflicted by the same assailant, which means Wilson might not have been a willing participant in whatever kinky scenario you think this is."

"What's your hypothesis, Adversary?" I pushed aside Wilson's genitals, exposing a stab wound incising deep into his upper thigh. It would have opened the femoral artery without being immediately visible, and bled him dry in minutes at most. "Fort Clarion is still inhabited. Renfield told me he was part of an elite all-CPMD military unit called Dusk Patrol that had taken to making their kills look like vampire attacks to confuse, frighten, and demoralize their enemies. The remnants of that unit still live under Fort Clarion."

"Dusk Patrol?" Robinson shuddered. "Fuck." He took a deep breath. "Naomi, are you sure? I haven't heard that name since before the Commonwealth fell apart."

He must have been seriously rattled to address me by name instead of rank. "I'm confident that my hypothesis is consistent with the evidence we've seen thus far, Sheriff. I fought two of them last night. That's how I got wounded, remember?"

"Yeah." Robinson gave the kid a once-over, and shook his head. "So, they weren't actually feeding off this poor kid, were they?"

Should I tell him? Or will he think I've gone round the bend, assuming he doesn't already think I'm crazy? "I'm afraid they were. The members of Dusk Patrol were subjected to a series of experiments designed to enhance them."

"You mean like Captain Commonwealth?"

Perhaps I should be ashamed of the fact that I had to look that up, but right now, my ignorance of pre-Nationfall superheroes wasn't a pressing concern. "That might have been their intended result, but with a codename like Project Harker, I doubt it."

"Christ!" Robinson's curse echoed in the enclosed space. "I was a goddamn MP at Fort Clarion, and I had no idea this shit was happening."

"Petersen knew." That stopped him, and brought his attention back to me. "He opposed Project Harker, but it continued despite him. He knows about the Dusk Patrol survivors hiding under Fort Clarion. He's protecting them."

"That must be why he only mentioned the stab wound, but not where it was located, in his report. He didn't mention any of the bites, either. Makes sense if those guys think they're vampires or are trying to make it look like a vampire kill." Robinson was just about to start stroking his chin when he realized his hand was still encased in a rubber glove that had touched a dead man. "No wonder Mayor Collins thought you were the killer. The wound was probably made with an Army-issue knife readily available at Fort Clarion, but the blade isn't much wider than that sword you always wear in public."

"The assailants would have difficulty inflicting a wound like this in battle." It wasn't impossible, though, but damned improbable. "It's not how I would go about killing somebody. I think it's more likely that they held the victim down, pinning his limbs."

"So, it was staged." Robinson stared at me a moment. "Show me your teeth."

As I did so, he found a tape measure and measured the space between my canines. He then compared it to each of the bites on Wilson's body, as I had meant to do. "Son of a bitch. Some of these look like they could have come from you, but most were made by sets of incisors set too far apart to have been yours."

The implication was obvious, despite my sleep deprivation. "Somebody's trying to frame me."

---

### This Week's Theme Song

"Dead Men Tell No Tales" by Motörhead

{% youtube fu3iVt54DGs %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
