---
title: "*Silent Clarion*, Track 65: &quot;My Famed Disappearing Act&quot; by Thank You Scientist"
excerpt: "Check out chapter 65 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
It had been almost nine months since Jacqueline invited me to stay for dinner. I ended up moving in with her at her insistence. It was a huge house which she owned outright, so as long as I kicked in some dosh for food and other bills and pitched in with the housework I was welcome to stay. Trust me, it was a godsend, and didn't carry the stigma I might have felt if I had moved back in with Howell and Sophie on the farm.

The time simply flew by. Jacqueline and I tendered our resignations amid news that Ian Malkin had committed seppuku and left a note admitting to abusing his position on the Phoenix Society's executive council to cover up what had been happening in Clarion. It seemed unbelievable, given what I had seen the night I agreed to back off, but Jackie and I saw the body. He was dead with his belly sliced open.

His will specified that his entire fortune was to be spent providing restitution for those who had suffered as a result of his crimes. For some reason, that included me. It would be foolish of me to name the sum, but the money is safely invested and paying a modest dividend.

Though it felt like blood money, I didn't quibble. Not when I could finally afford to focus on my music, which I've been doing when not helping Jacqueline plan her dream wedding.

Claire wasn't too keen on being part of the bride's party until her aunt revealed that it was going to be a gender-bending wedding. The women would wear tuxedos, and the men would wear the dresses for a change. Not that getting Claire fitted for a tux was any easier than having her fitted for a dress, despite her being much more cooperative. She kept growing, which buggered the measurements on a regular basis.

Kaylee had been kind enough to bring my Conquest back, and we've stayed in touch since she decided she liked London better. She opened a new Shiny Hobbies shop on Piccadilly Circus of all places, outrageous rents be damned, and actually managed to make a go of it so far. Claire helps out on weekends. So do I; it turned out I rock an awesome Cecilia Harvey cosplay even if the original character didn't have a badass red motorcycle.

Cat Tricklebank had settled in with her family back in southeast Australia. It took me a while and some help from Claire, who was turning to quite the little hacker herself, to track her down. She was alone when she met me atop the Rialto in Melbourne, dressed in mourning black, and fixed an accusing glare on me. She wasn't aiming it at me yet, but she had a semiautomatic pistol in her hand. "You let my husband die."

"Yes, I did. Had I suspected for a moment that he was in danger, I would have done everything I could to persuade him to take you and get the hell out of Clarion. I'm sorry, for what little that's worth."

"Did you come here to just to apologize? If so, you've said your piece. Leave now, while you still can."

At least she had put the gun away. But there was more she needed to hear. "I came to do more than apologize."

Cat had turned away, to gaze at the city, but eventually shrugged and faced me again. "What is it? Going to tell me he was involved, too?"

"No. He created an experimental tool that I used to stop the GUNGNIR satellite from bombarding Clarion. Without his help, I might not have been able to save anybody. I don't know if you encouraged him in his hacking, or just stayed out of his way, but thank you."

Cat's reaction wasn't what I expected. Collapsing onto a bench, she buried her face in her hands and began to sob. Sitting beside her, I put an arm around her and let her get it out of her system. When she had composed herself again, she spoke softly while gazing out over the city. "He wanted to be an Adversary, but didn't make the cut. He had the brains, but he was too nearsighted and too stocky. So he settled for making software Adversaries could use. The Phoenix Society paid him well for his work, but he would have loved to have heard you say that his code helped you save lives."

"I wish I could have told him in person. Is there anything I can do to help?"

Cat shook her head. "I have a job, his life insurance payout, and there was a bequest from that bastard Ian Malkin. I won't have to worry about money."

Knowing Cat Tricklebank would at least have financial security was a weight off my shoulders. Bringing her a measure of peace made my return to London easier; it was one of the few pieces of unfinished business I had remaining.

Another greeted me in New York while I waited at Grand Central Terminal for my maglev to London at a little cafe off the main concourse. Mike Brubaker had taken to wearing severely cut suits that looked good on him. "I didn't expect to see you in the city, Adversary Bradleigh."

Inviting him to join me with a gesture, I waved over the attendant. "Please just call me Naomi. What you said got me thinking, and I decided that I didn't really like the person I was with a sword in my hand and pins in my lapels any more than you did. And how are *you* doing?"

"Wait." Mike stared at me a moment. "You quit the Adversary corps? Because of me?"

"Don't take all the credit. You just got me thinking. I found other reasons quickly enough. Did you hear about Ian Malkin?"

A satisfied smile curved Mike's lips. "Yeah. Can't help but think he was just the fall guy, though. It's too bad he couldn't have been brought to trial so that everything could be exposed."

Time to change the subject lest my own complicity in the coverup become apparent. "So, what have you been doing with yourself?"

"I'm in finance getting my hands dirty. Once I've got fuck you money I'm going to go into law. I want to see if I can advocate for individual rights without taking up the sword."

Kissing him was a sweet mistake. Letting him kiss me back was sweeter still. Leaving the concourse with him to get a hotel room was sweetest of all. The only words spoken were his, a single sentence as I undressed him and pushed him down on the bed: "I haven't been able to get you out of my head."

When we were done, I tucked him in and left a note that became a long letter—or possibly a confession. He said he loved me before drifting off, but that wasn't what I needed. It wasn't what *he* needed. He needed to continue to work toward his dream, to achieve a sense of financial security that would let him live up to his ideals without compromise.

As for me? I had dreams of my own to chase. Besides, I had to be back in London for Jackie's wedding. That thought was foremost in my mind as I walked the darkened streets of Manhattan in the cold gray hours just before dawn.

However, the night wasn't mine alone. Someone pursued me, someone who took care to match their pace with mine so that our footsteps blended together.

Halting halfway to Grand Central Terminal, I turned to face my stalker, and found myself meeting the golden eyes of a raven-haired CPMD+ woman as tall as I was draped in a black fur coat months out of season. Though she didn't appear to be armed, something about her bearing unnerved me. "Who are you?"

The other woman greeted me with the slightest nod, but for some reason that small gesture felt as if she had bowed to me. "Good evening, Ms. Bradleigh. It was not my intention to frighten you. You may call me Theresa Garibaldi. Your father told me you had blossomed into a pale rose before his unfortunate passing."

Something about Garibaldi rubbed me the wrong way. "Are you the bitch who left me to die in a dumpster?"

Electricity seemed to gather around me, and I leaped backward. Good thing I did, too, for lightning struck the sidewalk where I had stood moments before. "No, Naomi. I'm the one who incinerated that bitch because your father Imaginos was too soft-hearted to do so. I am the ensof Thagirion."

Wonderful. Another demon. Just what I needed. "I apologize for my rudeness, but why contact me?"

"Accepted." Thagirion's burgundy-tinted lips curved in a slight smile. "Your father told me you're a dramatic coloratura soprano in need of advanced training."

Now, what could a demoness capable of calling down lightning teach me about music? "What sort of training?"

"The esoteric kind." She glided toward me, and patted my cheek with a gloved hand. "What if I told you I could teach you to sing with a siren's voice and captivate your audience? While imprisoned, you manifested a rare talent. I am the only one who can teach you to master it."

"What's *your* price?"

"There is no price. I want to see what you do with your voice once you've learned to use it to its fullest effect." She withdrew, and her smile turned ironic. "You still look doubtful. What might I say to convince you that I shall never hold you to any debt or obligation toward me? Shall I swear it on the Styx like a goddess of old?"

Nice try, but I'm not a Hellenist. Besides, I had just uploaded a clip of the last few minutes to Port Royal. "There's no need for oaths. Just understand that I can expose you if I wish. My death likewise means your exposure."

Her laughter was the melodious peal of church bells. I glanced around, sure she would draw attention, but we still had the dark street to ourselves. "Oh, you truly are your father's daughter. Now, when shall we begin?"

"Is there a way I can contact you? I was supposed to be back in London hours ago."

"I understand." She handed me a business card. "Call me when you're ready to begin."

Before I could examine the card, or say anything, there was a disorienting sense of *discontinuity*. It was like I had become unstuck in space and time. A moment later I was outside Jackie's house in London. Blinking against the sudden afternoon glare of sunlight, I slipped Thagirion's card into my pocket and opened the door. Thank goodness I was only going to make Jackie late for her hen night! "Sorry! I'll be dressed in a tick!"

---

### This Week's Theme Song

"My Famed Disappearing Act" by Thank You Scientist, from *Maps of Nonexistent Places*

{% youtube Y_GQp2R2Hf8 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
