---
title: "Blackened Phoenix, Chapter 1: When You Don't See Me (Scene 2)"
modified:
excerpt: MEPOL chief inspector Gregory Windsor has seen too much weird shit to be intimidated by Morgan. He and Naomi will have to do more to persuade him to talk.
tags: [ naomi-bradleigh, morgan-stormrider, mordred, MEPOL, gregory-windsor, isaac-magnin, christabel-crowley, alan-thistlewood, questions, fear, when-you-dont-see-me ]
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
date: 2015-12-11T00:00:00-05:00
---

Morgan regretted drawing on Windsor as soon as his blade cleared the scabbard. Playing good cop/bad cop was always a chancy proposition, for the act's success often depended on the dynamic between the Adversaries putting it on. It got riskier when the target was a cop himself. Unfortunately, Morgan could not sheathe his blade without undermining his own position unless Naomi caught on and played her role.

It was Windsor who spoke first. "No. I'm not playing that game, because either way I'm screwed."

"You don't think we can protect you?"

Windsor shook his head at Naomi. "Ms. Bradleigh, you're a singer. What are you going to do? I've seen what Magnin can do, and Stormrider can't stop him no matter how handy with that sword he is."

Morgan bristled at this, but not for his own sake. Naomi was in uniform, no less an Adversary than him, and wore a sword of her own. But did Windsor acknowledge the fact? 「 Shall I remark on Windsor's lack of respect?」

「 Thanks, but I've got this.」 Naomi cleared her throat. "Chief Inspector, you will pardon me if I insist on being addressed as _Adversary_ Bradleigh. You may have noticed my uniform."

"But you're a singer."

"Adversary Stormrider's a guitarist and a tenor. A rather good one, I might add. Are you only a police inspector? Isn't it dreadfully boring to be just one thing?"

Windsor opened his mouth to speak, and closed it as if reconsidering what he was about to say. A moment later, he made a second attempt. "You're right, Adversary Bradleigh. All the same, even if you're both skilled fighters, what can you do against Magnin? Do you have any idea who you're up against?"

Morgan smiled at the question, but refrained from one of the boasts he might normally make in such circumstances. Instead, he used secure relay chat to share an observation with Naomi. 「 Looks like Windsor's trapped inside some nasty circular logic. He wants us to back off, but he can't persuade us to do so without showing us some of the evidence we came to gather.」

「 You noticed that, too? I almost feel sorry for the poor bastard.」

「 Shall I point it out?」

「 Might as well. If I do it, I might reveal something I'd rather discuss with you in private first.」

「 If you've had dealings with Magnin before--」

「 It's complicated.」

Despite himself, Morgan raised an eyebrow at that admission. _Of course Naomi has a past. Naturally she's probably seen and done some crazy shit when she was an Adversary._ 「 Tell me everything later, when we have privacy. I'll try not to freak out.」

「 Thanks.」

With that decided, Morgan stared at Windsor and cracked his knuckles. "I thought you were smarter than that, Chief Inspector. You want us to back away because Isaac Magnin has you scared shitless, but you won't show us why we should be as frightened of the man as you are. Mordred, what do _you_ think of that logic?"

Mordred made a production of yawning, giving Windsor a good long look at his teeth, and then began licking his balls.

Morgan let him get in a couple of licks before clearing his throat. "Manners, big guy. We only do that at home, remember?"

The cat stopped, his long white whiskers drooping a little as he meowed and curled up beside his person. Morgan scratched behind his ears, and smiled at Windsor. "I think it's obvious that even my cat thinks your reasoning is flawed, though he went a bit far in expressing his opinion."

Windsor snorted. "How many treats did it take to bribe that furball into doing that routine on cue?"

Before Morgan could answer, Mordred raised a forepaw, and curled his toes so that only the middle one stood up. He hissed at Windsor.

Windsor, to his credit, met the cat's display with equinamity. "So, he understands English."

Naomi nodded. "Yes, though his speaking vocabulary is limited to meow."

"Meow?" Windsor repeated the word in a dubious tone that made Mordred's tail swish in annoyance, as if his inflection gave the word an especially rude connotation among cats. "Look, he's bloody adorable. At least my nieces would think so. But regardless of what the moggie thinks of my logic, you two can't take on Magnin."

"That really isn't your concern." Something in Naomi's tone suggested that her patience had reached her limit. Her hand hovered over the hilt of her sword as if she meant to draw on Windsor. "Your concern should be what will happen when Magnin decides he can't trust you to remain silent despite your recalcitrance in dealing with us. You won't tell us, but you're bound to tell somebody. You won't tell us, but you're bound to tell somebody. Your secret will weigh you down until you're so desperate for someone to share the burden that you won't care who offers their help. Why not us?"

"Besides", Morgan added, attacking from a different angle, "I'm going to go after that son of a bitch whether you cooperate or not. Your refusal to answer our questions isn't doing us any favors."

"You won't believe what I'm showing you." Windsor tapped at his terminal, and the wallscreen came to life in a flare of colors that quickly faded into a CCTV camera's view of a jail cell.

Alan Thistlewood sat alone in his cell, his lips moving as if he were talking to himself or to the orange mouser patrolling the cellblock. His face betrayed signs of a recent beating, and Morgan relayed a quick note to Malkuth mentioning the abuse. Whether Thistlewood had it coming was for a court to decide, not the guards charged with watching over him.

Windsor soon appeared, carrying dinner, and Morgan suppressed a pang of sympathy as he asked the disgraced inspector, "How many women did I let you hurt because I believed in you?"

"Just Bradleigh. I'll take any oath you ask of me."

"I don't want your oath. I just want you to think about everybody you betrayed because you wouldn't fucking let the past _go_."

Morgan glanced at Naomi. 「 I'm sorry. Thistlewood went after you because of-」

「 I know. I love you anyway. Now pay attention.」

Once Windsor was gone, a snow-blond man in a white suit with a royal blue cravat appeared.

"Rewind, please." Morgan didn't want to believe it. People did not just appear out of nowhere. _This isn't some idealistic space opera, damn it._

Windsor shrugged. "Sure, but I've watched this video a hundred times. Magnin just appears, as if he teleported."

The video played again, and the same thing happened. At 21:07:45, no Isaac Magnin. At 21:07:46, there he was. Furthermore, he smiled at the camera and winked. Morgan clenched his fists at the sight. 「 That smug bastard knows nobody will believe what they just saw.」

「 I know, but what if he has an ability like yours. What if he can hold his place in time, too?」

「 Fair point.」 Forcing himself to relax, Morgan watched the rest of the video. He had missed a fair amount, and returned his attention to it in time to see Thistlewood floating in mid-air, choking against an unseen hand grasping his throat as frost rimed the interior of his cell.

Magnin seemed to study the struggling disgraced cop as if he were a moderately interesting insect. "Offer me a reason to permit the man who laid hands upon my daughter without her consent to enjoy the due process of law."

Thistlewood's answer proved unsatisfactory, for seconds later he was gone. The video stopped, but Morgan did not demand another rewind. If Windsor had expected Morgan to be shocked by Magnin's abilities, he was mistaken. _How the hell does Naomi feel, knowing Magnin claims her as his daughter?_

A long moment passed. Morgan held his silence, hoping Naomi would be first to speak, for he was unsure of what to say himself. A cold sweat had pulled his shirt close to his skin, his vision was narrowing, and his balls felt like they were trying to retract back into his abdomen. _I'm scared of somebody Claire would call a white-haired bishounen because he seems to be displaying abilities that should only be possible in the media she consumes. And if I'm frightened, how must Naomi feel?_

Morgan turned to Naomi, but her expression was composed, her true feelings opaque to him. Yet her knuckles were white as she gripped the scabbard and hilt of her sword.

Windsor tapped his temple, breaking Morgan's reverie. "Are your implants acting up? Some kind of weird EMI is keeping me off the network."

Naomi let go of her sword, and shook herself. "Now that you mention it, I'm getting it too. And why is Mordred purring? Not that I mind, of course, but--"

"It's not purring. Not quite." As Morgan listened to his cat, the difference became clear. Purring possessed a gentle biorhythm based on the cat's heartbeat and respiration. What emanated from Mordred was too chaotic to be his usual contented rumble, and more closely resembled radio static. He reached down to scratch behind his ears. "You all right, Mordred?"

His fingertips brushed bristling fur, and he flew backward with a sudden crack of thunder. His head struck the wall, and for a moment everything went black. "It's all right. I'll get over it."

Naomi shook her head as she helped Morgan to his feet. "You know I hate it when you say that. Here. Turn around and let me have a look."

Her fingers wound into his hair as she probed his scalp, making Morgan want to settle into her embrace. "The skin isn't broken, and there's only a little swelling, but we're going straight to a doctor after this."

_She's scared, too, and she's taking it out on me._ Not that Morgan would say anything of the sort; Naomi's tone would brook no opposition. "We'll find a clinic as soon as the interference subsides."

## Author's Notes

Here's a scene that might not make complete sense unless you've read _Without Bloodshed_ and _Silent Clarion_, though the mention of Alan Thistlewood and the Christabel Crowley investigation in the first scene also ties this novel to _Without Bloodshed_.

Expect more on the connection between Isaac Magnin and Ian Malkin latter. The fact that these names have the same I.M. initials is not a coincidence. It is my design.

Also, more on Mordred and his purring that isn't purring later. Trust me when I say that it's plot-relevant. Thing is, I'll need to release a second edition of _Without Bloodshed_ to give Mordred more time on stage lest readers who glossed over the Boston safehouse scenes in the first book wonder when Morgan Stormrider had time to adopt a kitty.
