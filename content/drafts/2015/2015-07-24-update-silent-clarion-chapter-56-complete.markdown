---

title: "Update: Silent Clarion - Chapter 56 Complete"
modified:
categories: starbreaker
excerpt: "That's right, Chapter 56 is ready for revision. On to Chapter 57. Here's a peek, if anybody's interested."
tags: [silent-clarion, progress]
cover: silent-clarion-new-banner.jpg
date: 2015-07-24T09:05:52-04:00
---
That's right, Chapter 56 is ready for revision. On to Chapter 57. Here's a peek, if anybody's interested.

## Chapter Fifty-Six

Though I had the CCTV feeds wired into my head, they were disorienting -- especially when the system rotated to a camera aimed at me, so that I was watching myself in real time. Instead, I sought a rooftop for a better view. Two pairs of men linked up, and began patrolling together despite initial orders. A shot rang out as they happened upon my first victims, and one of the men fell to one knee. 

"Where is that bitch?" The stricken soldier's shout was as clear as if I was right behind him. If they had seen a muzzle flash from Mike's rifle, they would probably be heading for the tower. Instead, they're still searching. Figuring I should help them, I unslung my rifle and fired a shot of my own. My rifle didn't have a flash suppressor,  so I was nice and visible.

"There she is!"

"She wants us to come after her. Stick to the plan. Stay visible, keep her on edge, but do not engage. We can move in for the kill once's she's worn out and frazzled."

Don't these stupid gits realize I can hear them? [Oi, Malkuth! Want to help me take the piss out of some soldiers?]

[What have you got in mind, Nims?] 

This was why I liked Malkuth best. He actually seemed to like me, and didn't think I was a nutter. [Does Fort Clarion have a PA? Can you patch me into it?]

[Direct audio feed? You'd need a handheld.]

Too bad I don't have one of those. [Can you run my texts through a speech synthesis algorithm?]

[And make it sound like you?]

Hmm… Now *there* was an interesting question. Sure, I could make it sound like me, but surely the Sephiroth possessed sufficient processing capacity between all ten AIs to synthesize other voices recorded via Witness Protocol. Couldn't they? [Any chance you could spoof Sheriff Robinson's voice?]

[Yeah, I think I can manage. You ready.]

Oh, yeah. I was *so* ready. I sent the message, and seconds later Sheriff Robinson's voice boomed across the base. "All troops, stand down! The Bradleigh bitch has agreed to settle the matter by single combat."

Of course, neither Mike nor Sergeant Renfield had any idea what I had in mind. [What the fuck, Naomi? Didn't you hear Dr. Petersen? Robinson's augmented. He'll kill you.]

Mike was right. Robinson might kill me, but I didn't intend to let him. Besides, I told Renfield I'd try to avoid killing more of his men. Going directly after their leader was the best way to keep that promise.

Perhaps Robinson retained some shred of decency after all, because here he was at the gate. Better get down and go met him. Wait, was that a rifle? Was he aiming that at *me*?

## Chapter Fifty-Seven

When consciousness returned, I was sprawled on the ground with a gunshot wound roughly below where one of my kidneys should be. Sure the exit wound would be far worse, I tried to reach around to check, but my right arm didn't quite work right. And was that *bone* sticking out of the flesh?
