---

title: "Universal Basic Income Through Index Funds: Why Not?"
modified:
categories: government
excerpt: "Instead of spending billions on means-tested social programs, or funding universal basic income out of annual tax revenues, why not use existing market systems?"
tags: [UBI, basic-income, investment, index-funds, TANF, SNAP, market, reform]
cover: stock-market-board.png
date: 2015-09-09T17:28:27-04:00
---
I was bored at work, so I was thinking over [Universal Basic Income (UBI)][1]. If you're not familiar with the concept, UBI is different from other means-tested social welfare programs because *everybody* gets it no matter who they are, and there are no strings attached. You can still work if you want to, but you no longer have to take a shitty job just to avoid destitution.

But instead of fantasizing about how nice it would be to have that cushion, I was wondering if UBI was really as hard to finance as people think it is. It shouldn't cost that much to fund annually via tax revenues, especially if UBI replaces other, less efficient forms of social spending. 

According to the 2010 census, the population of the United States is 309.3 million people. According to the [Congressional Budget Office][2], the US government expects to get $348 billion in 2015 from *corporate income taxes* alone. Add individual taxes, payroll taxes, and other revenue streams, and you're looking at almost $3 trillion in 2015.

Just to lend some perspective, the US spent about [$30 billion on TANF (Temporary Assistance to Needy Families)][3] in 2009. In 2014, the US spent about [$75 billion on SNAP (Supplementary Nutrition Assistance Program, AKA "food stamps")][4]. That's almost $110 billion on two Federal means-tested social welfare programs alone, at least a quarter of which gets spent on administrative expenses instead of helping needy Americans.

Bearing that in mind, I can't help but wonder how hard it would be for the US to create bank accounts for every individual in the US, with $1M per account, invest it in index funds like the Standard & Poor's 500, and pay out the interest annually and tax-free to the account holder?

Actually, I know how hard it would be. It would cost about three trillion dollars. Sure it might end poverty for those who get accounts, but that's a year's worth of tax revenue for the Feds down the drain with no money left over for the military, law enforcement, space exploration, or subsidies to big business.

So much for that crackpot idea. If we're going to do basic income, paying for it up front isn't going to work.

[1]:	http://www.basicincome.org/basic-income/
[2]:	https://www.cbo.gov/topics/taxes "Congressional Budget Office/Topics/Taxes"
[3]:	http://www.npc.umich.edu/news/events/safetynet/tanf-spending-factsheet.pdf
[4]:	http://www.fns.usda.gov/sites/default/files/pd/SNAPsummary.pdf