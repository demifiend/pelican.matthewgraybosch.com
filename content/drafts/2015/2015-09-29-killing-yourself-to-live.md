---
id: 1695
title: "Black Sabbath's &quot;Killing Yourself to Live&quot; is Prescient"

categories:
  - Music
  - Rants
tags:
  - antiwork
  - Black Sabbath
  - bullshit jobs
  - burnout
  - David Graeber
  - day job
  - Killing Yourself to Live
  - programming
  - Sabbath Bloody Sabbath
  - writing
---
It's almost as if Iommi, Osbourne, Butler, and Ward predicted the rise of anti-work sentiment like David Graeber's ["On the Phenomenon of Bullshit Jobs"](http://strikemag.org/bullshit-jobs/).

> You work your life away and what do they give?
    
> You're only killing yourself to live. 

"Killing Yourself To Live" from Black Sabbath's 1973 album _Sabbath Bloody Sabbath_ album is pretty much the song of my entire adult life. That of most other working-class Americans and Brits as well, I bet.

Every time I come to the office, the first question I ask is, "Why do I keep doing this to myself?" I don't have a good answer, and I don't have a good alternative, either. I don't stand to inherit wealth, and I don't have a rich wife who's amenable to having a house-husband or a kept man.

So, I work, and fight for time to write and rest when I'm not working. I've been doing it for almost twenty years, and I am burned out. I've been burned out for years. I don't think there's any hope of recovery for me.
