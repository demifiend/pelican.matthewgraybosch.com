---

title: "Esquire: Genre Beat Literary"
modified:
share: true
comments: true
categories: editorial
excerpt: There was a war between genre fiction and literary fiction. Genre won.
link: http://www.esquire.com/entertainment/books/a33599/genre-fiction-vs-literary-fiction/
tags: [genre fiction, literary fiction, esquire]
image:
  feature: esquire-genre-landscape.jpg
  credit: Esquire
  creditlink: http://www.esquire.com/entertainment/books/a33599/genre-fiction-vs-literary-fiction/
date: 2015-03-13T22:13:13-04:00
---
There was a war between genre fiction and literary fiction. Genre won. Not that it was ever much of a contest. Modern literary fiction depends mainly on characterization and style. Inept writers of literary fiction often mistake providing the trivial minutiae of a character's life for deep characterization. And literary writers focused on originality that they forget to tell an interesting story.

Not that genre fiction is without sins of its own. Genre writers too often rely on hackneyed plots and stale character archetypes. They also suffer from an epidemic of coattail-riding, by which I mean legions of writers chasing popular trends instead of writing the stories *they* need to tell.

Where my sympathies lie should be obvious. I'm an unapologetic genre writer, to the point of failing a high school creative writing class for writing (crappy) fantasy stories and submitting them to a teacher who made no secret of her worship of Hemingway and Steinbeck.

Despite my preference for genre fiction, I don't particularly *like* the divide between genre and literary fiction. It is not constructive. Fantasy and science fiction benefit from greater attention to style and characterization. Literature focused entirely on depictions of mundane reality is sterile and uninteresting.

So welcome to the new Romanticism. We're out to make the world interesting again, by depicting reality through the lens of imaginary worlds. Please board the star dragon ship in single file, and pay attention while the crew demonstrate the proper procedure for securing yourselves in your assigned acceleration couches. Our staff magi tend to file union grievances when they have to clean up raspberry jam after a flight.
