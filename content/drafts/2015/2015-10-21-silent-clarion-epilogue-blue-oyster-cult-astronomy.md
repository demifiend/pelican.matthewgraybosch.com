---
title: "*Silent Clarion*, Epilogue: &quot;Astronomy; by the Blue Öyster Cult"
excerpt: "Check out the epilogue of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
NEW SLEEPING SUN VOCALIST CAPTIVATES BUDOKAN IN WINTER SOLSTICE DEBUT
*Dark Eternal* (https://earth.com.news.music.darketernal)  
by Jethro Cooper  
22 December 2096

**Sleeping Sun** might be one of the first classical heavy metal revival acts to rise from the ashes of Nationfall, but of late their star has faded thanks to a succession of lackluster vocalists. These ladies, who shall go unnamed, had the chops but not the presence necessary to bring Sleeping Sun to the level of its inspiration Nightwish.

Readers of *Dark Eternal* might argue that is unfair to hold vocalists in a revival band to the standard set by Tarja Turunen, let alone her successors Annette Olzon and Floor Jansen. This is a fair point, which I can only rebut by saying that the whole point of a revival band is to offer modern audiences an experience as close to the original band as possible.

Newcomer Naomi Bradleigh of London seems to understand this. Despite my initial reluctance to attend Sleeping Sun's Winter Solstice show at the Budokan in Tokyo, I was curious. Was Sleeping Sun really that desperate that they'd hand the mic to an unknown? Who the hell was Naomi Bradleigh?

It was my job to find out, but it soon proved to be my pleasure. An equipment failure crippled the band, and would normally have delayed the show while the band's sound crew figured out what had gone wrong. This tends to annoy the fans, especially if the venue's management can't get on the PA and explain that the band has run into technical difficulties, as was the case last night.

Instead of leaving the fans waiting, Ms. Bradleigh took the stage alone. I don't know how she did it, but when she spoke, everybody could hear her despite her not having a working microphone. Moreover, she bowed and addressed the crowd in Japanese.

Being the ignorant gaijin that I am, I had to ask somebody to translate. This was what she said. Apparently she was as fluent as a native speaker.

> Good evening, Nippon Budokan. I'm Naomi Bradleigh, and this is supposed to be my first performance with Sleeping Sun. Unfortunately, we've run into some trouble with the equipment which we expect to resolve shortly. We appreciate your patience, and humbly apologize for keeping you waiting. In the meantime, will you permit me to entertain you?

I don't know how she did it, but I haven't never heard a crowd go that wild before. I was going ape myself, and all she was doing was apologizing on behalf of the band and venue, but it felt as though she were humbling herself for me personally.

Once the venue was quiet, she began. I hadn't noticed it, but some stagehands had wheeled out an old baby grand piano for her, and she had sat down to play. Again, the whole venue shouldn't have been to hear her play, but every note was as clear as if I were standing right beside her.

But she was smart. Instead of singing, she only played at first. I only recognized a couple of selections from Beethoven and Chopin at first, but once she shifted into the piano intro to "Astronomy" by the Blue Oyster Cult I *knew* something was up.

Right on cue, the lights came up and the rest of the band joined in as Ms. Bradleigh sung the opening lines. And here's the really fucking *weird* part, the part you won't believe unless you were there that night. Even though everybody else played amplified instruments, and even though Ms. Bradleigh didn't have a mic, I could still hear her sing, and I could still hear her piano, *and it was if she were singing and playing just for me.*

You read it first at Dark Eternal, folks. Naomi Bradleigh won't last long with Sleeping Sun. That band is too small a stage for a pianist and vocalist of her caliber. I don't if she's a goddess, some kind of demon, or just an preternaturally skilled young woman—but if you get a chance to see her perform, you owe it to yourself to take it.

*fin*

Matthew Graybosch  
Harrisburg, PA  
15 June 2014—30 August 2015

---

### Final Theme Song

"Astronomy" by the Blue Öyster Cult, from *Secret Treaties*...

{% youtube 7xXEtO3bEe0 %}

...and *Imaginos*.

{% youtube SY7h4VEd_Wk %}
---

Did you start at the end? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
