---

title: "Silent Clarion: Chapter 60"
modified:
categories: starbreaker
excerpt: "It's been over a year, and at Chapter 60 the end is finally in sight for Silent Clarion."
tags: [silent-clarion, milestone, progress]
cover: silent-clarion-new-banner.jpg
date: 2015-08-11T21:40:13-04:00
---
So, I just finished Chapter 60 of *Silent Clarion* tonight. I knew I'd eventually get here, but I didn't think at first that the story would turn out this way. I sure as hell didn't expect to run the original German text of an aria from Mozart's *The Magic Flute* through Google Translate and then reworking the output so I could include it in the novel.

![Silent Clarion: Chapter 60 (using the Typed app for OS X)](/images/silent-clarion-chapter-60.jpg)

And, why do this? Simple: if Naomi's a singer, I should show her singing. Since she's classically trained, and the text for operas by Mozart, Verdi, Wagner, et al are old enough to be in the public domain, I might as well use those. By the way, [the Aria Database](http://www.aria-database.com) lets you search for arias by a variety of parameters, including [fach (vocal specialization)](https://en.wikipedia.org/wiki/Fach).

My outline for this novel had the story going in a rather different direction. There was no mention of an orbiting weapons platform called GUNGNIR. Neither Dr. Petersen, Mayor Collins, nor Sheriff Robinson had been subjected to a refined version of the treatments developed by Project Harker, and Naomi had not manifested any preternatural abilities.

However, that's all changed now. The hard part is showing that Naomi did something that should be impossible from her viewpoint without Naomi herself realizing it. Instead, the revelation that she had managed to project her song directly into somebody's mind through a soundproof Faraday cell has to come after the fact.

However, that's part of the falling action. The climax of the novel was handled in chapters 51-60. A few chapters more, and it's done. The current word count: 110,704.

By the way, it bears mentioning that the model on the current cover of the novel doesn't really match the way I see Naomi. My wife thinks she looks like a trans woman (not that there's anything wrong with that). Fellow CQ author [Matthew Cox](http://www.matthewcoxbooks.com/wordpress/) thinks she looks like a [Drow](https://en.wikipedia.org/wiki/Drow_\(Dungeons_%26_Dragons\)) &mdash; and she could probably teach Drizzt a thing or two despite only using one sword (and occasionally a pistol in her off-hand).

Speaking of Matt Cox, he has a new novel called [*Archon's Queen*](http://www.matthewcoxbooks.com/wordpress/) that also features a white-haired protagonist. He's the guy who edits the Kindle serial version of *Silent Clarion*, by the way.

This photo of Swedish fetish/alt model [Miss Loulou](https://www.facebook.com/ModelLoulou) by [Josefine Jonsson](http://www.josefinejonsson.com) is *much* closer to my image of Naomi.

![Miss Loulou, photographed by Josefine Jonsson](/images/miss-loulou_josefinejonsson.jpg)

Of course, this is Naomi as a performer rather than Naomi as an Adversary. She wouldn't dress like this to perform her duties for the Phoenix Society, though this this outfit *would* look good with a nice Italian side-sword.

![Danelli Armouries Italian Sidesword](/images/naomi-italian-sidesword.jpg)

Maybe something like this, from [Danelli Armouries](http://www.danelliarmouries.com/index.php/custom-swords/side-swords/155-italian-sidesword-da-ss14). It's a handsome weapon, and a bit more practical than a rapier since it can cut as well as thrust.
