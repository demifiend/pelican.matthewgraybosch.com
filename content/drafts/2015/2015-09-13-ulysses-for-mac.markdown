---

title: Ulysses for Mac
modified:
categories: tech
excerpt:
tags: [Ulysses, Mac, Scrivener, Markdown]
cover: ulysses-20150913.jpg
date: 2015-09-13T22:32:17-04:00
---
One of the hard parts of separating every chapter of a novel into its own text file is compiling everything together for release. One book is hard enough, but also compiling episodes for a Kindle serial gets annoying.

It isn’t enough to create one Makefile, you see. But I’ve heard of this Mac app called [Ulysses][1], and heard it worked a bit like [Scrivener][2], looked nicer, and used a proprietary extension of [Markdown][3].

The proprietary Markdown bit had me concerned, but I decided to tinker with the trial version anyway by using it to compile the text of *Silent Clarion*.

It was a rousing success, and the export function is surprisingly versatile. Not only does it handle the usual suspects, like EPUB, PDF, and DOCX, but it even dumps *vanilla* Markdown and plain text. This makes Ulysses perfect for drafting a novel, and then creating a plain text archival copy of your work that will be readable on any machine that groks Unicode.

I’m glad I plunked down fifty bucks for this app. It’s going to save me a lot of time over dicking around with Makefiles to compile individual chapters into a unified whole.

And though [The Soulmen][4] prefer that you store all your shit inside the Ulysses library, which syncs to iCloud by default, Ulysses will happily work with “external folders” (AKA your computer’s filesystem). You just need to be willing to poke about in the preferences and do some setup.

Fortunately, I’m not averse to a bit of tinkering. Here’s a shot of Ulysses in action, if you’re interested. If not, fuck you cause you’re getting one anyway. :)

![Ulysses for Mac in action][5]

The only downside I’ve seen thus far is that the DOCX export is a royal pain in the ass to convert to the [Curiosity Quills Press][6] house style. No big deal, though. I can use the Markdown export and run it through [Pandoc][7], then tweak shit in [LibreOffice][8].


[1]:	http://ulyssesapp.com/mac/ "Ulysses for Mac"
[2]:	https://www.literatureandlatte.com/scrivener.php "Literature and Latte: Scrivener"
[3]:	http://daringfireball.net/projects/markdown/ "Daring Fireball: Markdown"
[4]:	http://www.the-soulmen.com
[5]:	/images/ulysses-20150913.jpg "Ulysses for Mac in action"
[6]:	(http://www.curiosityquills.com "Make yourself useful and buy some fucking books."
[7]:	http://www.pandoc.org
[8]:	http://libreoffice.org