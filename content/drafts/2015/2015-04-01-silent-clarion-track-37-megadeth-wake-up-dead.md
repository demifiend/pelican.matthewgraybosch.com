---
title: "*Silent Clarion*, Track 37: &quot;Wake Up Dead&quot; by Megadeth"
excerpt: "Check out the thirty-seventh chapter of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
I had to shove Kaylee aside to get into bed last night. She was there the next morning, snoring softly. Once I had finished dressing, I tried giving Kaylee a poke to wake her.

She curled up in a fetal position, hugging her pillow close, and snarled sloppily. "Fuggoff."

"It's eight-thirty in the morning. I have to get moving. Shouldn't you be opening your shop?"

Her reply was less intelligible, but I managed. Rescuing Jacqueline from the aftermaths of several pub crawls whose epic excess became the stuff of locker-room legends gave me plenty of practice in understanding Drunklish. "You fuggin' nutsh? Ish Shundy. Fuggoff arreddy."

"Fine, but I'd better not come back and find you dead because you choked on your own puke. You hear?" Trust me; that was the *last* thing I needed.

"Yeah yeah yeah. Fuggoff an' lemme shleep."

Before I fucked off, I stopped at the bar for a quick word with Halford. "Dick, Kaylee is sleeping it off in my room. Can you please check on her later?"

"Sure. Think she might want breakfast?"

Never mind Kaylee, I wanted breakfast. "No rush. Though I'd suggest bringing coffee and aspirin as well. Any chance of getting my usual?"

Dick already had a plate prepared. Talk about service. "Here you go, Adversary. Pick a table, and I'll bring your coffee."

I took my time eating. It was Sunday, after all, and I doubted Ernest Yoder was likely to go anywhere, whether he was still holed up at home or had in fact disappeared. Hopefully, he hadn't suffered a similar fate to Scott Wilson or Charles Foster.

In any case, there was a thing or two I had to get my hands on before tackling that particular mystery, or confronting Matt Tricklebank, Cat's Unix-guru husband. Opening a secure talk session with Malkuth, I sent him an IP address. ｢Malkuth, I need a warrant to search the machine at this IP.｣

｢Can't you just use HermitCrab to penetrate it?｣

｢The sysadmin is one of the HermitCrab developers. He detected my penetration attempt and clamped down.｣

｢You should have bought him a few drinks first to loosen him up. Maybe light some candles and put on some soft music. Gotta set the mood, you know?｣

Oh, great. Now he's joking about buggery. Where does he get this shit? Jacqueline, I suppose. She's a pernicious influence. ｢Malkuth, please at least try to pretend to take this seriously. It's possible we have a third victim, Ernest Yoder. It is also possible that he may have been the first to die. Nobody looked into it sooner because of his reputation for reclusiveness. How the hell did these kids come to the attention of Dusk Patrol? Why would they go after somebody that rarely left his fortress of solitude?｣

｢And you figure he's more sociable on the network than in person? He isn't there. Neither are Wilson, Foster, Brubaker, or your new girlfriend.｣

My new what? ｢How do you know? Did you access the forum's database?｣

｢Hell no. I crawled the forum's pages and pulled the user list and everybody's profiles. User IDs are standard first initial and last name. There aren't any of the more fanciful handles you'd find on a board frequented by unsupervised young people. In fact, nobody under thirty uses this board.｣

｢Even though it's supposed to be open to everybody living in Clarion?｣

｢Would you frequent the same forums as your parents if you were a kid?｣

｢Heck no.｣If they didn't want to use an existing public net community like Phark or 9chan, would they have gotten Cat's husband to set up something private for them?

Network forums are normally served over hypertext transfer protocol, which is sent over port 80 by default. The official forum for ACS cadets was no different, but the sysadmin running the forum also ran a separate, unmonitored server on one of the high-numbered ports allocated for custom and ephemeral connections.

It was like one of those fight clubs mentioned in urban legends. Nobody talked about it, but if you were smart enough to run a port scan and find it on your own, you were welcome. ｢Malkuth, can you run a port scan on that server and see if there's another HTTP daemon listening on a non-standard port?｣

｢Found one. Want me to crawl it?｣

｢No. Do I have sufficient probable cause for a warrant giving me authority to examine that forum with administrative privileges along with the underlying server and filesystem?｣

｢No dice, Nims, but I can get you an entry warrant for Ernest Yoder's digs.｣ True to Malkuth's word, the access code for my warrant came through. If I wanted, I could print it myself. Otherwise, I'd just give the code to the property owner and let them download it themselves. ｢Anything else I can do for you?｣

｢I know better than to answer that. Thanks, Mal.｣
As a courtesy to Sheriff Robinson, I sent the code for my warrant to his office along with a quick note. He had a right to know I meant to kick down Yoder's door and poke around.

Most of the shops were closed, and the streets were empty; I suppose most of the townspeople were in one of the half a dozen or so churches lining Main Street. The doors opened at the closest church, indicating the end of services. The pastor was out front milling through her flock, and giving a final blessing before they returned to their secular lives. She sighted on me and pressed a booklet into my hands. "Good morning, young lady. May God bless you on this beautiful day, and guide you back to us next Sunday to join us at mass."

Keeping my objections to myself, I smiled at the grandmotherly pastor and checked out the booklet. A church that handed out Jefferson Bibles would most likely go easy on the hellfire and brimstone. "Thank you for inviting me, Reverend. I'll consider it."

Since she was holding an offering plate, and the church looked like it could use a fresh coat of paint, I found a crumpled five-milligram banknote in my pocket and forked it over. The pastor seemed shocked by my generosity, and I immediately regretted the gesture.

Reaching Yoder's home without further incident, I knocked on the door to the first-floor apartment on the assumption that the owner would have kept it for themselves while making tenants take the stairs. Am unshaved middle-aged man in stained overalls and a white t-shirt opened the door. "I don't have any places for rent."

"I suppose you're the owner."

"Yeah." Glancing at my hands, his expression hardened. "I don't need whatever you're selling, but I got a message you can pass up to the Almighty."

Thrusting the little Bible in my pocket, I decided against a hard approach. His reaction suggested I wasn't the first to offer the cold consolation of religion. "I'm not on God's payroll. If your problem is of an earthly nature, I may be able to help you."

"You gonna bring my wife back?" Unshed tears glistened in his hard eyes as he spat the words at me.

His barely-restrained anger wasn't specifically for me; I was just handy, somebody who had made the mistake of offering to listen. But there had to be a reason for his pain, and talking him through it might simplify my mission. "Do you want to tell me what happened to your wife, sir? Would it help if I listened to you? No judgment, no Bible talk. I promise."

Thomas Wesker was sobbing by the time he had finished his story, which had a familiar ring. Three years ago he and his wife, Emily Yount, moved to Clarion. They were a pair of artistic newlyweds who had scraped together enough capital to buy property in town instead of spending the money on a fancy wedding or an extravagant honeymoon. Thomas and Emily were happy together for six months, until they set out with a basket full of food and some blankets for an afternoon of lovemaking in the woods. Thomas dozed after loving his Emily, and woke up alone.

He finished his story with his hands in mine. "There was no trace of her. The cops couldn't even find Emily's body. Three years later, here I am, as ignorant today as I was then. I suppose you think I'm pathetic."

Though unused to sight of men weeping, I think Thomas had earned the right to a good cry. Something about his manner suggested he never properly grieved for his loss, and his pain had become cancerous. "Not at all, Mr. Wesker."

His tone sharpened a bit. "Not going to tell me to man up and get over it?"

"I'm not a therapist making a housecall, but I still know better than to impugn your masculinity. It would just put you on the defensive, which is counterproductive."

"You certainly sound like a shrink."

"It's the training. I'm Adversary Naomi Bradleigh, and I've got a warrant authorizing me to enter Ernest Yoder's apartment to verify the occupant's safety or disappearance. I'm not going to give you false hope by promising anything, but I will see what I can learn about Ms. Yount's disappearance. First, I must check in on Mr. Yoder."

Wesker nodded. "Is this one of those new-fangled digital warrants where you give me a code to download?"

Looks like he would have preferred paper. "It is. I apologize for the inconvenience. Can you provide a network address?"

"Yeah." Once I had it, I sent Thomas the warrant ID. "Just give me a minute, Adversary. Yoder's late paying his rent anyway."

"Is he habitually late with his rent?"

Thomas shook his head. "No. That's what I don't get. He's usually a model tenant. But he's late with his rent, and there's this smell coming from upstairs. I should have gone up there sooner, but Ernest isn't quite right because of what happened with his parents." He cocked his head and studied me a moment. "You know what happened to him?"

He was right. There was a stink of rot in the air here, but I couldn't pinpoint its source. "Enough to realize that if he's still alive, he isn't going to appreciate my presence in what had previously been his one refuge from the half of the human race that scares him as much as he scares himself."

Thomas glanced at me, a smirk curving his thin lips. "You sure you're not a shrink?"

"Quite." Stopping at the third-floor landing, I began taking shallow breaths through my mouth. The moist stink of corruption was stronger here than it had been in front of Wesker's apartment. "Is this Yoder's place?"

"Yeah." He cycled through the keys on his ring until he found the right one. "Do you want me to go in first, ma'am? In case it's bad in there?"

Thomas' offer was apprehensive, but well-intentioned. His expression suggested that the present situation at least gave him something to focus on beside his own pain. For that reason alone, it was unfortunate that I had to turn him down. "I appreciate it, Mr. Wesker, but you're a civilian. If it's as bad as the smell suggests, you might unwittingly compromise the crime scene."

Inserting the key, he unlocked the door and backed away. "There you go, Adversary. Good luck."

Good luck, eh? Why did I suspect I'd need it? Steeling myself, I turned the latch.

---

### This Week's Theme Song

"Wake Up Dead" by Megadeth, from *Peace Sells... But Who's Buying?*

{% youtube 4kSvN1dQjxc %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
