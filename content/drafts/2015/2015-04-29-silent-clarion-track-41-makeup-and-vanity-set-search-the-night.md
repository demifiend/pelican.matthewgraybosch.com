---
title: "*Silent Clarion*, Track 41: &quot;Search the Night&quot; by Makeup and Vanity Set"
excerpt: "Check out chapter 41 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Another murder?" Damn it, who had I missed? What was the chance of this person being attacked now that I had seven of the kids that had ventured into the depths of Fort Clarion with me for the last hour? Had I not warned everybody? Was there another who ignored my warning, and had paid for doing so with their life? Or was this something different, and worse? "Who was it? Was it the same method?"

Robinson shook his head. "I think you should see this in person."

Something about his tone told me it was going to be bad. "Better lead the way, then."

Without any solid information to chew on, it was tempting to speculate on who had been murdered, when, how, and why. Such temptations were best resisted, lest I arrive at the scene prejudiced. Even suspecting that this murder had anything in common with the others was a mistake, as now I'd have to guard against the impulse to disregard evidence that doesn't support my theory.

Trying to clear my mind so I could view this kill with fresh eyes, I followed Robinson down Main Street. He led me to a shop called Gibson Hacker Supply, right across the street from Kaylee's Shiny Hobbies. We found Cat from City Hall sitting inside amid racks of a near-infinite variety of electronic components and tools. She blindly leafed through a book, which promised to teach the reader how to build their own Enigma machine.

Before I could approach the shell-shocked receptionist, Robinson stopped me with a hand on my shoulder. "This way."

He led me behind the counter, into the back room. It had been ransacked, with solid-state drives scattered hither and yon as if somebody had been searching for a particular drive. In one corner, a small mainframe hummed, heedless of the violence and death that had visited the room. In another, a corpse slumped against the wall with his legs splayed before him, his head lolling over one shoulder.

In life, he had been a short, stout, long-haired bear of a man. In death, he was battered, his arms and legs bent at profoundly wrong angles. Somebody had driven a heavy-duty screwdriver into his chest. One of his temples was dented by what surely had been a mortal blow. He still clutched a crowbar in his left hand, its end bloodied from the blows he struck against his assailants.

It hadn't been enough. There must have been more than one intruder, and they had overwhelmed him. One of them had sliced his throat open and used his blood to leave a message on the wall. "WE MISS YOU, NAOMI. COME BACK, NAOMI. STAY WITH US FOREVER, NAOMI."

My heart kicked into overdrive, and my vision narrowed. My voice was a snarl through chattering teeth. "Whoever did this knows my name. This is a direct challenge."

Robinson nodded. "That's what I thought. Mind putting your sword away?"

"What?" My training must have taken over, for I had no idea I had it drawn. One of the things we learn as Adversary candidates is to meet fear on the battlefield with anger. While it's true that anger leads to hate and hate leads to suffering, fear can paralyze you. Worse, it sends you fleeing when your companions need you to stand firm. Rage, on the other hand, gives you strength and courage with which to fight, survive, and prevail.

Breathing deep despite the stink of blood and pain, I sheathed my blade. Even if it didn't make Robinson nervous, I can't afford to fence with shadows right now. "Did you get an ID on the victim?"

Robinson nodded. "Matt Tricklebank. Cat's husband. Everybody in Clarion uses that mainframe of his. We just rent capacity on Tetragrammaton."

Oh, shit. Though I suspected the victim had been him from the state Cat was in, I hoped to be wrong. Between being a major HermitCrab contributor and running that mainframe, it's little wonder he called himself the Godfather. He had a hell of a lot of information and power at his fingertips. What was his part in this mess, aside from giving Clarion's youth a virtual speakeasy? Had he also been to Fort Clarion? "Tetragrammaton?"

Robinson shrugged and gave me a sheepish look. "That's what he called that mainframe. Said it was a Unix thing."

Fortunately, I could sit in front of the console without disturbing the crime scene. The keys responded to my fingers with a meaty click as I woke the screen for a shell prompt.

> GENERAL ATOMIC MODEL GA-65536  
> MULTICS VERSION 20481031.23.17  
> UPTIME 10 YEARS, 320 DAYS, 20 HOURS, 15 MINUTES, AND 33.3 SECONDS

> tetragrammaton login:

A quick network search suggested that not only was this most likely the last working Multics installation on the planet, but that this installation ran on a machine last produced prior to Nationfall. Had Tricklebank found this while settling in Clarion? Or was this a relic of the Commonwealth Army's presence in town? In the chaos of this room, it sat untouched, which suggests it wasn't why his assailants had come. "Ten years of uptime on a computer this old? Tricklebank must have been some kind of wizard."

"Sounds about right. We resettled the town about twelve years ago. Tricklebank and his wife found that machine when they bought this building and set up shop."

"Speaking of which, has anybody gotten a statement from Cat?"

Robinson shrugged. "Have you had a good look at her? Figured she was too traumatized to tell us anything useful."

"I'll try talking to her." Might as well, since there wasn't much I could do with Tetragrammaton right now. Even if it had the right port for my device, I couldn't count on HermitCrab being able to talk to it. If it couldn't, I'd need an account and a manual. "Cat?"

She stared up at me, eyes narrow with grief and hatred. "Why did you have to come here?"

Rather than look down on her, I knelt before her and took her hands in mine. "People were disappearing, and nobody else cared to intervene. I'm sorry about your loss, and I would have protected your husband if I had been aware of his peril. There are six young people who are going to live because I got them out of here. You and Mr. Tricklebank could have been among them."

"Why would you have us leave here?" Without anger to lend her voice texture, Cat spoke in a flat monotone.

"Do you know Ernest Yoder? He was murdered a couple of weeks ago. Scott Wilson and Charles Foster died the same way. Whatever's happening here started before I came. If you know anything, please tell me."

Cat looked around, searching for the Sheriff or his deputies. "Not here."

She glanced around the shop again before producing a key and opening a door to a staircase leading down into the cellar. She descended without turning on the light, and I followed. Absolute darkness enveloped us once I closed the door behind me, and my implant flashed a "network connection lost" message at me.

Unsure if the cellar was also soundproof, I whispered. "Your husband built a Faraday cage in the basement?"

"No. It was here when we bought the building. It was prewar construction, and Matt left it in place in case he needed to work in a secure location." Cat flicked a switch, and a soft red glow pushed back some of the gloom. She turned on a display, and the shop above us came into focus via closed-circuit television. "Nobody can hear us down here."

"How do you know?" The sudden flush in Cat's cheeks was answer enough. "Never mind. Given the CCTV, it doesn't really matter since we can see if anybody comes in upstairs. So, what can you tell me? Why are you afraid of Sheriff Robinson?"

Cat didn't immediately answer. Instead, she stared at the screen for a while with the frown of a person gathering their thoughts and deciding how much was safe to tell me. "Matthew told me that Robinson was here a couple weeks ago with the Mayor and Dr. Petersen. They wanted him to release logs from an IRC server he runs for the kids so they have somewhere safe to talk. He told them to produce a warrant or fuck off. They came back last week and made the same demands, but still didn't have a warrant. They said that if he didn't see reason soon, he'd suffer for it."

"Why didn't he go to the Phoenix Society?"

"They told him that if he tried to expose them, they'd trump up charges that would blacken both our names."

"What were they going to do, find some toddlers and coach them to make accusations of Satanic ritual abuse?"

That got a small chuckle out of Cat. "That's what Matt said. But he didn't go to you guys because Robinson wore civilian clothes each visit. I was at work, and Matt told me everything down here, so I don't think Robinson realizes I know."

They threatened the guy because he wouldn't produce IRC logs without a warrant? All three of them? And now he was dead. That made no sense whatsofuckingever. Not unless those kids were talking about something they saw at Fort Clarion that would utterly compromise Collins, Robinson, and Petersen. Something like Project Harker?

If Cat had given me a specific time and date, it would be much easier to check the CCTV footage, or see if they were on the job at the time and thus had Witness Protocol running. With the information I currently had, any such effort would be a fishing expedition. "Cat, I need you to think carefully. Do you have any recordings or other evidence of these threats? I can arrest those bastards tonight if I have some proof of their involvement in your husband's murder. Just give me something I can use."

It wasn't much to ask, despite Cat's recent bereavement. Was it? Even fifteen seconds of video would be enough, if it captured a threat to the deceased. I just needed something more substantial than "my husband told me afterward".

Cat eventually shook her head. "Matt gave me an account on Tetragrammaton and tried to teach me how to admin the machine, but I never got into it like he did. He was like a big kid with the ultimate model railroad."

"I know it's bad practice, but would you be willing to either share your username and password with me?"

She gave me a dubious look. "I could just give you an account of your own with admin rights."

"Wouldn't a new account with sysadmin access be noticed? Robinson isn't an idiot, and neither is Petersen."

Cat sighed, and found a pen and a scrap of paper. "Here."

"Thanks." I saved the credentials, and found a shredder. It wouldn't do to leave a root password to Tetragrammaton lying around for just anyone to find. "Do you have people outside Clarion? You really shouldn't be alone right now."

Cat took a moment to answer. "Some of my cousins are visiting Manhattan. I could join them and follow them back to Melbourne, but what about…" She pushed back a sob. "Matt's funeral?"

"Manhattan should be far enough for you to be safe. I just think you should get out of town for a bit. The arrangements can wait."

---

### This Week's Theme Song

"Search the Night" by Makeup and Vanity Set, from *Charles Park III*

{% youtube Be9VPmV-NJA %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
