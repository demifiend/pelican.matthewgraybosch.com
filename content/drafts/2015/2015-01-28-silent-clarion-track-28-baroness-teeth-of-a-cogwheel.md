---
title: "*Silent Clarion*, Track 28: &quot;Teeth of a Cogwheel&quot; by Baroness"
excerpt: "Check out chapter 28 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Dr. Petersen became utterly still at the mention of Renfield. His fixed gaze seemed to take in the entire room, as if seeking the nearest available weapon. He picked up a scalpel with a trembling hand, considered it for a moment, and put it back. "So, you seduced Sergeant Renfield."

Did he think the man was a victim of my feminine wiles? Not bloody likely, when he was straining at the seam less than thirty seconds after we first met. That wasn't even sufficient time for me to consider an approach more subtle than simply walking up to and propositioning him, let alone actually doing so.

Of course, Renfield wasn't the sort to require a subtle approach, but I'd like to think my involvement was more than that of an enthusiastic participant. "We seduced each other, Doctor. He's quite the physical specimen."

Petersen shrugged. "No doubt he thought the same of you. Not that I disagree. How much did he tell you?"

"Not that much at first. He ended up telling me enough to let me connect him and his buddies living under Fort Clarion to Project Harker." I paused for a moment to gauge Petersen's reaction, but he seemed content to listen for now. "Renfield spoke highly of you, incidentally, and told me you stood up for Dusk Patrol when the Army Medical Corps treated them like lab rats."

I leaned forward, reaching for his hand. He didn't stop me from taking it. "In fact, you've been looking out for them ever since, haven't you?"

Petersen nodded, and withdrew his hand. "I had hoped that yesterday's field trip might have sated your need to dig any deeper, let alone venture into the woods around Fort Clarion at night. I thought you wiser than this, Adversary Bradleigh. Do you have any notion of what you could expose by further investigation?"

Aside from unethical science suppressed by the Phoenix Society, presumably because its conclusions affected everybody with CPMD? There were people out there who had been betrayed and abandoned by the country they swore to defend. They remained locked in a nightmare created for a war long over. Petersen should have ended this years ago, but I gained nothing by questioning the man's ethics at this juncture. "This is bigger than a couple of unexplained disappearances. The deeper I dig, the worse it gets."

"You should stop digging, then." Petersen sighed, and gazed out the window for a couple of minutes before continuing. "Those unfortunates who vanished will be just as dead even if you manage to find an explanation for their disappearances. You're just wasting your time, and meddling with matters you don't fully understand."

Was I just paranoid, or did Petersen's words imply that if I continued to meddle, I might wind up dead? "I understand that Project Harker was an attempt to enhance the combat capabilities of soldiers with CPMD. Whatever they discovered can be applied to anybody with CPMD. Furthermore, the Phoenix Society is already well aware of Project Harker."

That thrust hit home, for Petersen stiffened as if I had stabbed him. "How?"

"Remember a civilian named Ian Malkin?"

Petersen nodded, as if not quite trusting himself to speak.

"Well, I've seen a guy who looks like him from time to time at the London chapter. Scuttlebutt says he's on the Phoenix Society's Executive Council, not that the brass will confirm or deny it. I can't prove they're the same person, but how many men with that name have you heard of?"

"Not many." Petersen swapped in the other bag of blood to continue the transfusion. "I'll admit it's a convenient coincidence. I suppose some of Renfield's men disapproved of his liaison with you."

"I didn't bother to ask my attackers why they picked a fight with me. On a related topic, care to tell me anything about the death for which Robinson arrested me? You're the coroner, aren't you?"

Petersen chuckled, and glanced at the blood pressure readout. "Starting your discovery process early, are you?"

"I didn't think to bring a book, and I doubt you'd be willing to discuss Project Harker, or your post-Nationfall involvement with Dusk Patrol any further."

"We're almost done here, Adversary. So, assuming you manage to persuade the Sheriff he has the wrong person, allow me to offer a small suggestion. The men won't bother you again as long as you and yours stay aboveground. Don't go exploring. It's dangerous."

Now that was a threat, and not just one directed at me. Was it something I could take to Robinson to deflect his suspicion? While I could have Malkuth provide an alibi using my Witness Protocol feed, Robinson would still need a better suspect. But if I directed him toward the Fort Clarion, wouldn't I just be getting him and his deputies killed?

Petersen kept eyeing me, and I doubt it was just because the t-shirt was a bit tight on me. "You're thinking about something, aren't you?"

"Just wondering what you're so desperate to hide." I gave Petersen my sweetest smile as he disconnected the IV and removed it from my arm. "You really should consider leveling with me. Too many have suffered for this secret, including you and the survivors of Dusk Patrol. How long are you going to keep carrying this burden?" I paused for effect, and to let Petersen focus on bandaging my forearm. "Do you really think you can take the truth to your grave? It's most likely that the Phoenix Society already knows everything about Project Harker. They just can't be arsed to tell me anything."

"So you're determined to figure it out on your own, and to hell with the consequences?"

"Pretty much." I refrained from shrugging as he applied pressure to my arm and covered the area with liquid bandage. "Innocents are dying around here. Everything I've seen so far, everything Renfield has told me, and every evasion I've heard out of you suggests that Fort Clarion is at the heart of what's wrong with this town."

Petersen shrugged, and began washing his hands. It probably wasn't just proper hygiene, but an act of renouncing any responsibility for what might happen next. "You won't find out much from a jail cell. But if you get out, and more people die for your curiosity, remember that I warned you."

Putting my jacket back on, I met Petersen's gaze and held it a moment. "Allow me to return the courtesy. If more people die, I'll assume you're culpable and arrest you first."

Finding Robinson still in the waiting room, leafing through a pro sport magazine with my sword across his lap, I held out my hands so he could cuff them. "I'm prepared to cooperate now, Sheriff."

Instead of binding me, he returned my sword and led me outside. "I just spoke with an AI from the Phoenix Society. He reports you reconnoitered Fort Clarion on your own last night, and furnished a map of your movements. Based on that, I don't have probable cause to arrest or detain you, but I'd still like you to come to the station."

Malkuth gave me an alibi? Damn, now I owed him an apology for threatening to kick his ass. "I will, if you tell me why you arrested me in the first place."

Robinson wouldn't look at me as he spoke, which most likely meant he was well aware of his lack of justification. Either that, or Malkuth explained my reconnaissance in detail. Hope he enjoyed the show. "The Town Council tends to just rubber-stamp Mayor Collins' budget proposals every year, and under the Society's regulations, I can't appear at council meetings in my official capacity to speak up for my department."

Robinson swept a hand, as if presenting the town. "Does this look like London or New York to you, Adversary? Hell, does it even look like Pittsburgh or Philadelphia?"

I watched as workers began putting up "Sound of Clarion" banners to advertise the upcoming annual music festival the Halfords kept talking about. "No. It's a small town, with perhaps an annual influx of visitors for the festival."

"Exactly. In a few days, everything's going to go batshit crazy for a couple of weeks. Most of the resources at my disposal are already allocated. I'm stuck, mainly because most of the fucking hicks living here think the goddamn militia is enough to keep the peace. Never mind that none of them are actually paying taxes." Robinson spat contemptuously, and met a passing woman's disapproving glare with one of his own. "Ingrates."

Things were starting to make sense again. "So, when Mayor Collins tells you to bark like a dog, you ask, 'What breed, your honor?'"

Robinson's laughter was bitter.. "You've got it. What Mayor Collins wants, he damn well gets. So when some kid who came into town to see a band at the Lonely Mountain appears to have died of a stab wound on a night you're seen wandering into the woods, the Mayor wants you in a cell so he can tell the good people of Clarion that they don't have to be afraid."

While I could empathize with the need to prevent a panic, I could not forgive the wrongful arrest of an honest citizen to provide the local authorities a semblance of efficacy. It was especially difficult to view the situation from Mayor Collins' position when he was scapegoating me to retain the trust of his electorate.

My hand tightened around my sword's scabbard as I indulged my indignation. It would be so nice to march right into the Mayor's office and enact a bit of an impromptu regime change, but I had more pressing concerns. Besides, Collins was the devil I knew. His removal from office for malfeasance and abuse of power could wait. "The people of Clarion should be afraid, especially Mayor Collins."

Robinson glanced around, as if I had spoken too loudly and frightened the townsfolk. "Adversary or not, I can't have you publicly threatening the lives of town officials."

"Don't worry. A bit of due process won't kill your boss." Flashing a smile at the Sheriff, I turned away from Town Hall. Didn't Robinson realize that Adversaries never investigate violations of their own rights? The pins aren't a license to pursue vendettas. "I was thinking about Fort Clarion. If we don't do something about that place soon, the poor bastard you've got on a slab will only be its latest victim."

"You know something, don't you?" Robinson's expression hardened, but his tone held a note of disgust. "Why can't you just tell me?"

That was a reasonable question, but not one easily answered. Police officers and Adversaries both serve the public, but we're often at cross purposes by necessity. It's their job to uphold public order and arrest those who threaten it. It's ours to second-guess the cops, and make sure they're not ruining innocent lives. While collaboration between cops and Adversaries isn't unprecedented, it's still rare enough to be remarkable.

Beyond all that, lay a simpler truth. I didn't want to waste Robinson's time by jumping to a conclusion without more information. "I haven't seen the victim's body. Have you?"

Robinson shook his head. "No. Dr. Petersen has the town morgue under his practice, and he's the coroner. He's never fucked up an autopsy."

"He might not have buggered this one, either, but you've seen Fort Clarion yourself. Something's wrong there. We should examine the victim for ourselves before I share my suspicions with you." I pulled aside the collar of my t-shirt, and lifted the bandage to show Robinson my love bite. A few passersby stared at me, but they didn't matter. "See this? I think we'll find more on our victim."

"When did you get that?" Robinson narrowed his eyes. Hard to blame him when my bite was already halfway healed thanks to modern medicine.

"Last night from a guy who turns out to be one of Fort Clarion's more rational inhabitants." I fixed my collar and zipped up my jacket. The way Robinson stared at me was starting to creep me out. "Look, Sheriff, I've had a hell of a night. Why don't you come meet me at The Lonely Mountain when you've gotten a warrant to search Petersen's morgue? I have to request a warrant of my own, and I'm not in uniform."

Robinson nodded, and took a deep breath. "Good idea. Do I want to know how you got that bite?"

Seriously, what's wrong with some cops?! They can talk about crime all night in plain, blunt English, but immediately resort to euphemisms should the mere specter of consensual, mutually pleasurable sex come up. "All you need know is that everything leading to the bite was consensual. When I pressed him for an explanation, I learned he lives under Fort Clarion. He's not alone."

"All right." Robinson paused, and dropped the bomb. "I asked because I think know the guy. I've seen him at Petersen's late at night."

---

### This Week's Theme Music

"Teeth of a Cogwheel" by Baroness, from *Red Album*

{% youtube 9v4yX5tapMQ %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
