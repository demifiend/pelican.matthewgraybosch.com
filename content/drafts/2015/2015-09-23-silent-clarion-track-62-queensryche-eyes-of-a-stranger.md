---
title: "*Silent Clarion*, Track 62: &quot;Eyes of a Stranger&quot; by Queensrÿche"
excerpt: "Check out chapter 62 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch. Naomi knew her romance with John wouldn't be forever. She didn't expect it to end like this."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
To my surprise, telling my tale to Dr. Tranh didn't take nearly as long as I expected. She was an excellent listener, and saved her questions until I had finished. If she had reached any conclusions, or had any opinions, she didn't share them with me. She did, however, arrange for me to have my right arm broken again and reset so it would heal straight lest I suffer nerve or muscle damage later on.

She got no objections from me. That was my sword arm, and with one good hand you're limited to playing either the melody on a piano or the bass line, but not both.

This time, however, my arm didn't heal instantly. Whatever sneaky treatment Dr. Petersen had given me had run its course. Instead, I now had an arm encased in an old-fashioned plaster cast.

"I know it's low-tech," Dr. Tranh offered by way of explanation, "but it's dirt cheap, and we've found collecting signatures helps patient morale—which hastens recovery."

Dr. Tranh was first to sign. Jackie and Claire were next, and I made a point of thanking Claire for the manga. More Adversaries came to visit, and they all signed my cast before Edmund Cohen wheeled in a robotic arm while smoking a fat blunt while ignoring my doctor's protests.

"What the hell is *that* for?"

Eddie chuckled and blew a haze of smoke laden with second-hand tetrahydrocannabinol from his nostrils as he signed my cast. He took his time, letting his eyes linger a little too long over my chest and puffing like some species of dragon. *Dracolech erectus*, perhaps? "Malkuth wants to sign your cast, too, but he doesn't have any hands of his own. So I agreed to bring in this waldo."

"Fine. Malkuth, you be careful, all right?"

"Of course." He was as good as his word. His signature was surprisingly florid, and even came with a little Tree of Life. He laid down the pen when he was done, and took my hand. "Get better soon, Nims. We have a date, remember?"

Eddie chuckled, and tapped ashes into my latest vase of flowers. "How come I don't get a date?"

"I didn't want to get between you and your girlfriend Mary Jane." Coughing, I started waving away the smoke. "Could somebody please crack a window?"

"I've a better idea." Dr. Tranh plucked the blunt from Eddie's fingertips. Handling it as if it were radioactive, she dropped it into the toilet and flushed several times. By the time she had finished, the room's ventilation system had dealt with the smoke. "Light up another one of those here, Adversary, and I'll schedule you for a colonoscopy."

Eddie's horror-struck expression was priceless. "Not another one of those. I just had one."

"Not at this hospital you haven't." Tranh seemed to like the idea of having Eddie sedated so she could go spelunking with an endoscope. "You look like the sort of man who leads a shockingly unhealthy lifestyle, so I'm sure I'd find all sorts of interesting things. For science, of course."

Eww… That just sounded kinky. And Jackie looked like she was ready to suggest possibilities. Knowing her, it would start with a gerbil graveyard and get worse from there. "Can you two please get a room?"

Dr. Tranh stared at me a moment while Eddie gave her a once-over. "You think we're flirting?"

"I certainly well hope so."

Eddie chuckled. "Same here, Nims. So, Doc, when does your shift end?"

"I still have to finish my rounds. Think you can wheel this machinery out of here on your own?"

"I think I'll manage." And to Eddie's credit, he didn't seem to put in any effort. Maybe the old bastard's in much better shape than he looks.

More Adversaries filed in to sign my cast, which confused me. If I stood accused of abusing my authority, none of them would want anything to do with me even if I *was* innocent until proven guilty. Regardless, I wasn't going to begrudge the attention. What really shocked me, however, was the sight of Mike Brubaker coming to visit with Christopher Renfield in tow.

Mike looked pretty good for somebody who had gotten a sword through the lung. And Renfield wasn't bad, either. Just bad news. Best to put him behind me.

Careful not to hit Mike with my cast, I gave him a one-armed hug. "Should you be up and about with your wound?"

"I'm fine. The exercise will do me some good. Is it true you were arrested?"

"Yeah, but you wouldn't think so given how nice other Adversaries have been to me. Normally we shun an Adversary who got busted for the slightest infraction as if they were radioactive child molesting bubonic plague carriers."

"Nice." Renfield shook his head. "What happens if the trial happens and the accused is acquitted. Does everybody pretend they didn't shun the poor bastard?"

"To be honest, I don't recall an Adversary accused of a crime and subsequently acquitted ever returning to duty." Not that I ever thought about why. Now that I faced the prospect of a court martial, I understood a little better. To be accused by your fellow Adversaries of being what you swore to oppose stings like betrayal.

Rather than dwell on it, I offered Brubaker a pen. "Want to sign my cast? There should be a clear space somewhere."

"Sure." His grasp was gentle as he added his signature to those preceding his. When he was done, he stood aside for Renfield.

Rather than sign, the old soldier leaned over me and stole a lingering kiss that left me wanting more. "You'll remember that after the cast comes off."

He was right. Before I could demand more, he placed a heavy package wrapped in pastel paper in my lap. "The Society crunched the numbers and decided we were owed back pay for guarding Fort Clarion even though the Commonwealth is long gone. Smart way for them to give us some working capital to get set up without it feeling like charity, if you me. The guys decided to chip in and put together a gift to thank you."

Rather than open the gift, I pushed it away. "I killed some of you, and wounded others."

Renfield shrugged and put the package back in my hands. "We're getting a chance to make new lives for ourselves because of you. So open the goddamn present."

"If you insist." Whenever I unwrapped gifts, I made a game of doing so with a little damage to the paper as possible. My foster mother does it as well. When I asked her why, as a little girl sitting amid a pile of shredded wrapping paper one Winter Solstice, she told me it let her re-use the paper to wrap other presents.

My fastidiousness seemed to amuse Renfield, judging by his smile, but it wasn't long before I had the brown cardboard box unwrapped. Opening it, I found books of sheet music containing a repertoire worthy of a serious pianist that spanned five hundred years of Western music. Taking the topmost from the box, I checked the index. I had played only a third of the pieces contained within. A third I had heard of, but hadn't attempted. The rest were obscure to me, but wouldn't remain so for long. "Thank you. This is a wonderful gift. I'd love to play some selections for you and the others."

"I'd like that. I think some of the other guys would, too. Drop me a line when you're ready, all right?" Rising from his seat, Renfield bent and kissed me again. This time it tasted like farewell, which was probably for the best. "Mike had something he wanted to tell you privately, so I'll wait for him outside."

Mike wouldn't sit down, but remained standing Renfield closed the door behind him. Rather than remark on it, I got to the point. "So, what's the secret?"

"I've decided against becoming an Adversary. Something about the Phoenix Society just doesn't feel right, and I think I can do more to help working outside their system." He didn't look at me until he had finished. "You should get out, too. The way you fought Dr. Petersen—it was like you were *playing* with him. I don't know if you had a mean streak before you took the oath, or if you got it on the job, but I don't want to be the sort of person you were a few days ago."

"I appreciate the advice." Which was a flat-out lie. I could understand him having doubts about the Society since I had my own. But to accuse me of torturing that bastard Petersen? Or was I ready to tear into Brubaker because he was right about me? Maybe I *did* like hurting people when I was sure they had it coming. "Anything else?"

"I might have said too much. You looked like you were ready to throw down." Mike shook his head. "Actually, I should thank you. I might have made two mistakes if you hadn't shown me what you're like on the job. I might have become an Adversary myself, and I might have fallen in love with you."

"Get out." I didn't scream, shout, or even sharpen my tone. However, something in my voice put him to flight despite his wound.

The rage that reddened my vision and made a sword of my voice didn't make sense at first. Sure, Mike was cute and I had respected his willingness to fight beside me in Clarion despite being an all-but-untrained civilian, but I wasn't interested in him. I wouldn't have wanted him to be interested in me. Regardless, his cold, calm rejection was a thrust from a far sharper knife than the ones I faced only a few days ago. The truth cuts deepest of all.

Renfield and Brubaker were my last visitors that day, and while a nurse brought me dinner I was otherwise left alone with my self-image in ruins around me. All I could think about was the introduction to moral philosophy all prospective Adversaries had to pass before they could face their final trials. The first day, we were confronted with Nietzsche's warning to those who fight monsters and stare too long into abysses.

Was that me? Was I becoming what I despised? Would I even recognize myself in a mirror, or would a stranger's eyes meet mine?

"I would say that your ability to consider the possibility proves that you haven't become the monster you set out to fight, but that would be a lie."

Reaching for a weapon as I opened my eyes, I scrambled out of bed to get away from the intruder.

A snow-blonde CPMD+ man in a white suit with a navy blue cravat sat at the foot of my bed, regarding me with cobalt eyes. He favored me with the insouciant smile I recognized from a hundred sparring matches—and from photographs I had taken from Dr. Petersen's account on Clarion's town computer. "Dr. Ian Malkin, I presume. How did you get in here at midnight, without my noticing?"

His smile broadened. "I liked it better when you called me Maestro."

"I liked you better when I knew you as Maestro. And you still haven't answered my question."

---

### This Week's Theme Song

"Eyes of a Stranger" by Queensrÿche, from *Operation: Mindcrime*

{% youtube A4duZjxusGM %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
