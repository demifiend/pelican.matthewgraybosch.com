---
title: "Paradise Lost: *Draconian Times MMXI*"
excerpt: One of the best metal albums of the 1990s is even better live.
header:
  image: paraside-lost-draconian-times-mmxi.jpg
  teaser: paraside-lost-draconian-times-mmxi.jpg
categories:
  - Music
tags:
  - 2011
  - doom metal
  - Draconian Times
  - live album
  - Paradise Lost
---
The 1995 _Draconian Times_ album was the one that drew my attention to the Halifax UK doom metal act **Paradise Lost**, and I felt like sharing that album with you today. Oddly enough, the live version from 2011 is somehow _better_ than the original. Unlike the studio version, I find it easy to just put this on repeat and let it run. The cover art is badass, too.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A2ShDbz7Yk59ntWD1n5jK6h" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

---

Way back when, there was something about the quiet, melancholic piano opening into full-throttle metal that just _grabbed_ me, and it still gets me today. Also, these guys know how to put on a show.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MEvmc3arU3I" frameborder="0" allowfullscreen></iframe>

---

Sometimes all you need is a simple reminder &mdash; or a simple excuse.

{% include base_path %}
![Full cover for "Draconian Times MMXI" by Paradise Lost]({{ base_path}}/images/paraside-lost-draconian-times-mmxi.jpg)
