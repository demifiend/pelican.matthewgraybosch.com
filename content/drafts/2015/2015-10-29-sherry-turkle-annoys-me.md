---
id: 2261
title: Sherry Turkle Annoys Me
date: 2015-10-29T11:46:30+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=2261
permalink: /2015/10/sherry-turkle-annoys-me/
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 
snapEdIT:
  - 1
snapFB:
  - 
snap_MYURL:
  - 
snapPN:
  - 's:229:"a:1:{i:0;a:8:{s:9:"timeToRun";s:0:"";s:9:"apPNBoard";s:18:"224054218900737052";s:10:"SNAPformat";s:15:"%TITLE% - %URL%";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doPN";i:0;}}";'
snap_isAutoPosted:
  - 1
nc_postLocation:
  - default
twitterID:
  - MGraybosch
snapTW:
  - 's:311:"a:1:{i:0;a:10:{s:4:"doTW";s:1:"1";s:9:"timeToRun";s:0:"";s:10:"SNAPformat";s:31:"%TITLE% - %URL% via @MGraybosch";s:8:"attchImg";s:1:"1";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:18:"659758520405852160";s:5:"pDate";s:19:"2015-10-29 15:47:24";}}";'
bitly_link:
  - http://bit.ly/1KHZ6fh
sw_twitter_username:
  - MGraybosch
snapGP:
  - 
snapRD:
  - 's:166:"a:1:{i:0;a:6:{s:4:"doRD";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:8:"postType";s:1:"A";s:10:"SNAPformat";s:0:"";s:11:"isPrePosted";s:1:"1";}}";'
sw_cache_timestamp:
  - 401725
categories:
  - Rants
tags:
  - conversation
  - good old days
  - hiding behind books
  - hiding behind tech
  - interaction
  - Sherry Turkle
---
It isn't reasonable for me to find [MIT Initiative on Technology and Self](http://web.mit.edu/sturkle/techself/) founder/director Sherry Turkle annoying. I haven't met the lady, I haven't read her books, aside from [an excerpt of _Alone Together_](http://www.alonetogetherbook.com/?p=4), and I only know of her work and her concerns that the ubiquity of consumer computing technology from articles in [the Guardian](http://www.theguardian.com/science/2013/may/05/rational-heroes-sherry-turkle-mit) and the [New York Times](http://www.nytimes.com/2012/04/22/opinion/sunday/the-flight-from-conversation.html?pagewanted=all). However, it seems like she thinks that computers have given people a new ability to hide from each other and be "alone together" that didn't previously exist. She writes, and seems to honestly believe sentiments like this:

> We live in a technological universe in which we are always communicating. And yet we have sacrificed conversation for mere connection. 

Again, it isn't reasonable, and it isn't fair to Dr. Turkle, but there's something about her "look up, look at one another" preaching that just raises my hackles.

<img src="http://i2.wp.com/upload.wikimedia.org/wikipedia/commons/3/3b/Gato_enervado_pola_presencia_dun_can.jpg?w=840&#038;ssl=1" alt="A cat hissing and arching its back to make itself appear larger to ward off a threat." data-recalc-dims="1" />

Then again, maybe it isn't just me. Not if [this article on Medium](https://medium.com/@kyze/stop-saying-technology-is-causing-social-isolation-1e004de63a5e#.6o75bpuow) is any indication. Here's the problem. Back in the Good Old Days according to Sherry Turkle:

> Not too long ago, people walked with their heads up, looking at the water, the sky, the sand and at one another, talking. 

And I can't help but think&#8230;

<img src="http://i0.wp.com/pbs.twimg.com/profile_images/1593202305/cool-story-bro-500x499.jpg?resize=500%2C499&#038;ssl=1" alt="Cool story, bro." data-recalc-dims="1" />

&#8230;because I don't remember any such Golden Age when people voluntarily talked to each other instead of finding ways to isolate themselves. My parents didn't need smartphones, not when they had television.

I certainly didn't talk to other people unless I absolutely had to. I learned early on that other people had no use for me, and that conversation with others to ease my loneliness was a fool's errand. I learned that the mere presence of other people was no guarantee that they'd have anything of interest to say.

Back in Turkle's pre-smartphone, pre-Internet "good old days", I had to live under the tyranny of geography. If the people around me had nothing to offer, my only option was to _move_ and hope I'd find community elsewhere. It was far more rewarding to hide behind books and headphones jacked into a Walkman.

Some people don't get this. Some people think that they're more important than the book you're reading, or the friend you're catching up with using your device. Some people think that face-to-face interaction is inherently superior, or somehow more authentic because we somehow can't edit ourselves. Some people romanticize the past at the expense of the present. Some people need to take some laxatives, because they're full of shit.

You can still edit yourself during face-to-face interaction. You can still "be your best self" &#8212; the version of yourself most likely to please and impress others &#8212; instead of saying what you actually think and actually feel. I do it all the time. Why do you think I still have a job?

#### Image Credit

[Marc Smith on Flickr](https://www.flickr.com/photos/marc_smith/5166351572)