---
title: How to Retain Good Developers
excerpt: It's not that hard. Just pay us well, give us clear requirements and sane deadlines, and let us do our jobs.
header:
  image: office-space-printer.jpg
  teaser: office-space-printer.jpg
categories:
  - Programming Considered Harmful
  - Longform
tags:
  - compensation
  - developers
  - employees
  - hours
  - overtime
  - professionalism
  - retention
  - work
  - working conditions
  - workplace
---
DHH, creator of Ruby on Rails, thinks you can't retain every developer forever, and that you shouldn't try. [He says so right here.](https://m.signalvnoise.com/poaching-is-for-animals-not-employees-d9c04d54c0da#.3ic7ggqct)

Bullshit. I’d love for a company to try to keep me around forever, as long as they did it by _earning_ my loyalty. It wouldn't be that hard. Just treat me like a human being instead of a resource to be exploited or a cost to be eliminated if I no longer benefit the company. You think I _want_ to be a mercenary with no ties to anything or anybody?

Hell no. I do it to survive in modern America. I don’t like it any more than you do. So if you’d rather have [samurai](https://en.wikipedia.org/wiki/Samurai) on the job instead of [rōnin](https://en.wikipedia.org/wiki/R%C5%8Dnin), keep reading. Here’s some suggestions for being a company I would willingly stick with throughout my working life.

## Offer Adequate Compensation

You don’t have to pay me hundreds of thousands of dollars, but if I’m working for you as a developer, the last thing you want me thinking about is money. So pay me a salary that ensures that I need not worry about not having enough money, and make sure to adjust that salary to account for inflation.

If you’re going to provide group health insurance, please offer plans that aren’t going to nickel and dime me into the ground. If I can get a better deal at [healthcare.gov](http://healthcare.gov), you’re doing it wrong.

Likewise for retirement benefits. A pension would be lovely, but I’m not that naive. At least offer a 401k with access to index funds and a decent matching percentage.

## Have Reasonable Hours

I would love to work at a shop that didn’t care how many hours I put in as long as I provided the required results. Such a results-based approach would motivate me to deliver quality work quickly and efficiently so that I have more time for myself.

However, I recognize that there are some businesses where you need staff on the job eight hours a day, Monday through Friday. I can deal with that.

But if you’re going to have me work more than forty hours a week, and it isn’t because I screwed up, I’ll be a hell of a lot more willing to do it if the overtime is rare, announced well ahead of time (don’t tell me at 4:45PM Friday afternoon that you need me on the job the following Saturday), and compensated.

## Always Pay Time and a Half

I don’t care what exemptions the Fair Labor Standards Act provides for techies. I don’t care that I’m on salary. If you want me to work more than 40 hours a week, including time spent on call, I want time and a half for overtime.

The formula’s pretty simple, by the way. I’ll express it C-style.

{% highlight C %}
// Let's start with a reasonable salary. Figures in
// comments below are rounded to the nearest cent.
// But I'm not rounding the wages because those centimes
// do eventually add up. :)
private double _salary = 75000;

// $75000 divided by 52 weeks should be about $1442.31/week.
// $1442.31 divided by 40 hours should be about $36.06/hour.
private double _hourlyWage = (salary / 52) / 40;

// Time and a half for $36.06/hour is about $54.09/hour.
// My cat wrote this code, by the way.
private double _timeAndAHalf = hourlyWage * 1.5;
{% endhighlight %}

Now, why pay time and a half for overtime even though Federal labor law doesn’t require it? Simple: remember what I said about loyalty?

You expect to get the work you pay for. I expect to get paid for the work I do. If I only get paid to work forty hours, but you’ve got me working fifty or sixty hours a week and it wasn’t because I screwed up, I’m going to start to suspect that you’re taking advantage of me.

Aside from that, think of it this way: you already have me on the payroll. If you can pay time and a half and get me to put in more time rather than hiring another person, training them, and waiting for them to learn the ropes and figure out the product, you’ll save money in the long run. It’s a win-win situation. I make more money, and you save time and money.

## Offer a Healthier Workplace

Don’t worry, I’m not asking for fancy chairs, on-site yoga classes, or an in-house masseuse (my wife would kill me). But I’d appreciate it if you gave some thought to the spaces in which you expect people to work.

We know that sitting around all day is bad for people, yet we still make people do it. Is it really that hard to modify cubicles so that people can stand and work for at least part of the day?

Also, if you ask me to stay late to work on something, and I offer to come in early the next morning instead, it’s for a reason. I know myself better than you do, and I know when my brain has nothing left to give for the day. Tired programmers make dumb mistakes. I learned this the hard way so you wouldn’t have to.

## Make it Easier to Concentrate

It might not be realistic to give every developer a private office, but there are a few things a company can do to make it easier for me to concentrate on my work, which leads to me making fewer mistakes, which in turn saves the company money.

Don’t be shocked if you see me with headphones on. You wouldn’t believe how many developers find it easier to get into a flow state with the right music. (I prefer [heavy metal](/categories/music/), but tastes vary.) I’m not slacking off, even if it looks like I’m just staring at my screen.

Please don’t just come to my desk and drag me into a meeting. If you have to have meetings at all, it’s better to schedule them ahead of time. Remember that not only is time spent in meetings time not spent working, but once I get out of the meeting I have to get back into the zone.

Please max out the RAM in my computer. RAM’s cheap, and having more means I spend less time waiting for my computer to finish builds and other long-running tasks.

Also, please consider issuing developers machines with SSDs as their OS/software drive. Time spent fighting with one’s tools is time one could spend doing useful work.

## Leverage My Experience

As tempting as it might be to just lay off older developers when embarking upon a new project requiring the use of new technologies, think twice. I might know more than you think, and might have at least tinkered with the new hot language/framework/toolkit in my downtime. If not, I’m willing to RTFM and learn it. The Web being what it is today, you might not even have to reimburse me for books. 🙂

Chances are your experienced developers, even if they are old salts like me, can learn a new programming language, development framework, or toolkit faster than you can find wet-behind-the-ears CS grads willing to work for you, acclimate them to the organizational culture, and teach them how to develop software in the real world. We’re already on the job, we know the ropes, and we won’t make promises we can’t keep.

The smart thing to do would be to keep people like me on the job, especially if you need to modernize an existing product. Developers who don’t understand the original product because they didn’t implement it might make the same mistakes. Developers who were on the job the first time around probably have some ideas for avoiding the mistakes they made the first time around — though we might make new mistakes.

## “I Do the Job, and Then I Get Paid.”

I might not be a passionate developer who loves to code, but that’s not the kind of developer you want. Passion fades, and love affairs eventually end. I’m what you really want, even if I do think of my position at your company as my “day job”.

In fact, you should be glad. It means you don’t have to worry about whether I find my work meaningful. You don’t have to feed my soul. Just pay me on time and don’t stiff me if I go above and beyond for you. I’ll feed my soul on my own time, as long as you haven’t taken it all.

It’s just a job to me, and that means I do the job, do it well, and then I leave for the day. I’m not going to mess around, because wasting your time just wastes my time. I have better things to do with my life.

Chances are I’ll do more for you in a solid eight hours than you’ll get out of a developer trying to prove themselves by spending fourteen hours a day on the job. So don’t choose a “passionate” programmer. Choose a working-class professional like me. I’ll surprise you.

#### Image Credit

[The (uncensored) Printer Scene from _Office Space_ &#8211; Mike Judge's accidental documentary of the American tech industry](https://www.youtube.com/watch?v=N9wsjroVlu8)

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>
