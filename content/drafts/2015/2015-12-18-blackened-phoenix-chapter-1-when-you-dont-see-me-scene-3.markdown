---
title: "Blackened Phoenix, Chapter 1: When You Don't See Me (Scene 3)"
modified:
excerpt: The truth about Christabel Crowley and her reasons for getting involved with Morgan Stormrider and Naomi Bradleigh finally comes out.
tags: [ naomi-bradleigh, morgan-stormrider, mordred, MEPOL, gregory-windsor, isaac-magnin, christabel-crowley, answers, diary, true-feelings, when-you-dont-see-me ]
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
  - Longform
date: 2015-12-18T00:00:00-05:00
---

It had been a long night, as Chief Inspector Gregory Windsor had worked frantically to gather up all of the evidence gathered by the inquest into Christabel Crowley's murder so it could be sealed away. As a result, the question he should have asked when Morgan Stormrider and Naomi Bradleigh first showed up did not occur to him until now. After all, he reviewed every piece to ensure nothing was missing. He had even skimmed Christabel Crowley's diaries and papers. He knew more about her than they problably did. "Got a question for you two, especially you, Adversary Stormrider."

The two Adversaries turned to regard him, their feline eyes piercing him until he felt like a mouse in a hole. Stormrider rubbed the back of his head. "What is it, inspector?"

"Why do you even care about Crowley? Do you have any idea how she felt about you? About Adversary Bradleigh?"

Bradleigh shook her head, and favored Windsor with a gentle, almost condescending child. "We performed with her for ten years. Christabel and Morgan were lovers most of that time. I think we would know how she felt about us better than you."

"You know what? I doubt that." Windsor was unsure of where he found the courage to gainsay the frost-maned Valkyrie who could get away with lording it over Morgan Stormrider and fussing over him when he got himself hurt. Maybe he simply didn't care any longer. If these two refrained from hacking him into cat food, Isaac Magnin would probably make a meat popsicle out of him, so he was fucked regardless.

Opening his closet, he lifted a box of books and papers with a grunt of effort. He dropped it onto his desk with a thump that set other papers fluttering to the floor, and the cat pounced on one of them. Rather than complain, Windsor picked a leather-bound journal out of the box and opened it. Flipping to a random page and squinting at Christabel Crowley's cramped handwriting, he began to read aloud. "Every once in a while that fat slut Claire says something intelligent. She said something about the Uncanny Valley today. Of course, she doesn't understand that my asking her for an explanation was an expression of interest and an attempt to start a conversation instead of the imposition she obviously felt it to be."

Stormrider remained impassive, but Bradleigh wore a small, wry smile. She grasped her lover's hand, and gently squeezed it. "That certainly *sounds* like Christabel. Please continue."

Windsor complied after another glance at Stormrider. "Once she finally got over herself and told me about it, I felt as though a huge part of my life finally made sense. No wonder I want to heave every time Morgan touches me. He isn't human, but he's close enough that the differences give me the fucking creeps. I *live* in the Uncanny Valley."

He paused, and looked at Stormrider again. "Did you have any idea how Christabel really felt about you, Adversary?"

"She had made her feelings explicit the night we broke up, over a year ago. What exactly is the point of this exercise?"

*That's actually a good question. Am I trying to deter Stormrider, or inform him? But if I'm out to inform him, shouldn't I start at the beginning?* Windsor put aside the journal from which he had been reading. It was one of the more recent ones; its first entries dated back to a year and a half ago. Lifting journals and bundles of typewritten letters from the box, he stacked them so that they could be returned in a semblance of chronological order.

He finally lifted the first journal, a battered old-fashioned marble notebook. It was the sort of thing a nostalgia fetishist attending university might have used to make a performance of their studies. The inner cover offered a place for the owner to write down their name and contact details in case they misplaced their notebook. Written there in a neat feminine hand was a name and a street address in Astoria, Queens. "The first thing you should know about your lost love, Adversary Stormrider, is that her name was Annelise Copeland. She was a New Yorker, just like you."

Windsor expected the other man to accuse him of lying. Instead, Stormrider nodded as if he had already suspected this. "Any idea where she lived?"

"Crescent Street in Astoria."

"Practically the girl next door, then, since I lived on 26th." Stormrider had practically spoken under his breath, as if reminiscing. "Does her journal explain why she became Christabel?"

Windsor flipped past class notes and fashion sketches to one of the last few pages. He skimmed them before answering. "I think so. Listen to this."

### 12/13/2096

I met a man at work today, but I don't want to tell anybody about him. I want to keep him all to myself, but if I keep it all in my head I'm afraid I'll forget that this isn't a fantasy. So I'm going to write it down here and make it real.

Besides, Isaac asked me to write it down. He's asked me to write everything down, to keep a hand-written journal. He's given me a typewriter, with which I am to write him letters when we cannot be together. He says the Phoenix Society sees or can see anything that's done with a computer, but ignores paper.

It's weird, but *he*'s weird too. He wears white in New York City. That alone makes him strange, but it looks *good* on him. It sets him apart from other men, and places him above them as if he were a hero or a god.

What would Rachel have called him, since she's always reading those dead German philosophers? Oh, right.

Isaac Magnin is the *ubermensch*, the overman, and he walked into that greasepit of a pizza parlor where I used to work nights. He walked right up to me, and told me, "Annelise, you are beautiful. I've seen you in the off-Broadway revival of *The Crucible*, and you deserve better than this. Come with me, please. Let me be your patron."

I know it's weird that somebody like him would know my name, but who could resist having a man so beautiful come straight to you and acknowledge your deepest and most secret belief? I *do* deserve better than to use my acting skills to persuade people that this garbage Ibrahim keeps making is not only halal pizza, but the best halal pizza in New York. I *do* deserve better than to have to smile and count out change for the fucking Luddites who still pay cash. I *do* deserve better than that loser Jamal who keeps telling me I'd be *sooooo* much hotter if  I let myself get fat.

A patron! Someone who possessed the means and inclination to help me pursue my art and refine it — how could I *not* say yes to having someone like Isaac as my patron. Most people never get such an opportunity.

Of course I went with him. Ibrahim bitched about it, of course. No way I was going to finish my shift, let alone come back for another. But Isaac said something in Arabic that stopped him cold.

After that he took me to Saks on Fifth Avenue, where we met a woman he knew. She was shorter than him and about my height, with black hair and yellow eyes like a cat. She wore a little black dress with black stockings and stiletto heels, and her lipstick was blood-red. I thought at first that they were going to try to involve me in a *menage a trois* and I'd have to explain that — patron or not — I'm really not into girls.

Instead, they took me to a hairdresser, but wouldn't let me see what they had done with my hair. Then, they had me fitted for a new wardrobe — a *rich* woman's wardrobe and makeup. I didn't recognize myself when they finally let me see myself in a mirror. I look like I belong on a runway, modeling fashions nobody can afford unless they're richer than God.

Then he kissed me. Just one kiss, and a question he whispered in my ear. "Would you like to be an actress in history?"

I didn't know what that meant at first, then it hit me. If the world itself is a stage, then Isaac must have chosen me to play a role in events to come. So I asked him what he had in mind.

He kissed me again and said, "Trust me. I will make a new woman of you, and teach you the true history of the world. Then you will play a pivotal role in shaping that history. Nobody will know what role you've played unless they get their hands on the diaries you'll keep while in my service, but when it's done you'll be a rich woman."

There was no way I could say no to that. All my choices in life so far have been constrained by not having *enough* money, and to never have to worry about money again would mean I was free to finally be my true self.

He's given me a completely new set of credentials. As Annelise Copeland, I have 1024.5mg to my name at the local credit union. As Christabel Crowley, I've got ten kilos at Danglars & Mondego. I know who I would rather be.

### 12/14/2096

Isaac and I spent the day at the Wall St. Xanadu House. The other patrons didn't know we were watching them. Turns out the woman who helped with my makeover owns Xanadu House. She owns the entire chain. Her name's Elisabeth Bathory.

I thought at first they were going to make me a courtesan, but Bathory just smiled. Isaac's going to make a musician of me. I start violin lessons tomorrow with another woman friend of his, Theresa Garibaldi.

### 12/15/2096

My fingertips hurt, so a short entry. The violin strings cut into them as I play, especially the really thin one, the E string. Not that the G string is especially comfortable, either. Besides, I have to buy a new notebook. This one's out of paper.

Windsor gently closed the notebook. "Do you see, Adversaries? Isaac Magnin chose Annelise, and *groomed* her for use against you. He made her his agent, to manipulate you. Are you sure you want to know what the rest of the journals say?"

"No." Adversary Bradleigh's refusal was soft, but vehement. "Morgan, *you* deserve better than to find out how this Annelise was groomed to be a Mata Hari and sent to seduce you. And *I* don't want to know what machinations on Isaac Magnin's part ensured that my path crossed Christabel's."

Adversary Stormrider did not immediately speak. Instead, he kissed Naomi's forehead before turning to Windsor. "Since the Crowley investigation is closed, you're just going to have this evidence disposed of, are you not?"

Windsor nodded. "Right. We'll just have it all recyled."

Stormrider spent a long moment in thought, stroking his chin with one hand while scratching behind his cat's ear with the other. "Find me a Braille typewriter, please, and I'll specify an alternate disposal site. If nobody in MEPOL can read Braille, I suggest working with a blind contractor."

Windsor understood Stormrider's reasoning immediately. If the dump site's location was available from Witness Protocol data, the Phoenix Society could intercept the transport. For some reason Stormrider didn't trust his own people. "I don't know what kind of game you're playing, and I don't want to know. But if this is all you want from me, I'll play ball if you'll tell me why you're doing this."

Stormrider shook his head. "I became an assassin to escape poverty. Annelise Copeland became a spy. She wanted a better future as badly as I did. I can't condemn her."

## Author's Notes

I've made several attempts at a scene where Morgan learns the true extent of Christabel Crowley's loathing for him in her own words, but none of the scenes -- not even the one in the original *Starbreaker* -- really worked. They all involved Morgan himself going through Christabel's diaries.

Having a complete outsider to Morgan's [*nakama* ](http://tvtropes.org/pmwiki/pmwiki.php/Main/Nakama) made the scene work, because it's now somebody else telling Morgan, "This woman hated your guts. How the hell could you stay with her?"

The thing is, the Christabel that Morgan thought he knew ceased to exist when he wasn't there. Just like that old song by The Sisters of Mercy, "When You Don't See Me".

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A7nu1rAUwlMIeOlaPbrK9Du" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

Also, you might have wondered what Annelise had 1024.5mg of at the local credit union. The answer is gold. After Nationfall, the Phoenix Society had to rebuild the financial system along with just about everything else. They did so by doing away with fractional reserve banking. Whatever you could buy with a dollar in 1980, you can buy with a milligram of gold in the Starbreaker setting. And whether you have a gram to your name or ten kilos, you can walk in and demand that the bank give you your balance in hard metal.

You'd have to be paranoid to do so, because the Phoenix Society does *not* take kindly to bankers' shenanigans *at all*. They know damn well that if the people running the financial system are allowed to game it for their own benefit, the whole system is fucked.
