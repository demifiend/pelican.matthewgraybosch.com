---
title: "*Silent Clarion*, Track 47: &quot;Jailhouse Rock&quot; by Elvis Presley"
excerpt: "Check out chapter 47 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
We must have made a hell of a sight, marching back into Clarion with a sizable Dusk Patrol contingent escorting us. Fortunately, none of them knew I had an implant that allowed me to send texts over an encrypted connection.

With Robinson a suspect, I couldn't ask him to help me turn the tables on our uniformed friends. Since I lacked the authority to muster them myself, I couldn't count on militia support. That left Deputy Colby, and maybe Kaylee. Between the two of them, I might manage to gather sufficient forces to subdue my escort without the Sheriff's interference.｢Deputy Colby, I could use your help. Same with you, Kaylee.｣

｢What's up?｣

Kaylee's reply wasn't nearly as guarded. ｢Naomi? Last anybody saw of you, you had taken Mike up to your room. So, how was he?｣

｢Kaylee! I'd never take advantage like that. Deputy Colby, can you and Kaylee round up some deputies and a small militia detachment without tipping off Sheriff Robinson or Mayor Collins? I need some  backup while making an arrest. I'd rather this didn't become a lynching.｣

｢Who the hell are you arresting?｣

｢A bunch of soldiers living under Fort Clarion, for the murders of Yoder, Wilson, Foster, and a bunch of other kids. They're escorting me and Mike Brubaker back to town right now. I've also got Brubaker in custody.｣

｢For murder?｣

If I told Colby I meant to nail Robinson on a tyranny charge, would she still be willing to help me? Or would she feel she owes Robinson? Better not take chances by telling her too much. ｢Protective. He's a material witness.｣

｢Can't you get support from the Phoenix Society?｣

As if I had time to say pretty-please and deal with the all bureaucratic bollocks the Society puts in the way of Adversaries who could use a bit of backup. Not that I'd tell Colby or Kaylee anything of the sort.｢HQ prefers we attempt to cooperate with local authorities first.｣

｢OK.｣ Seeing that response was a relief, but a short-lived one. ｢Just tell me one thing: is the Sheriff involved in this, too? He's been getting this shifty-eyed look lately whenever that old army base comes up.｣

｢Pfft. Mayor Collins always looks like that.｣ Thanks, Kaylee. Glad I'm not the only one who thinks so.

｢I'm not sure how much I could divulge without compromising the investigation. Let's just say he's on my radar.｣

｢Fair enough. I'll round up as many as I can. We'll be waiting when you get here.｣

"Who were you talking to?" One of the soldiers barked at me. He pressed his fingertips to his ear, a gesture used to indicate to others around us we were using our implants. "I saw you doing this. It means you're on the phone with somebody, right?"

Damn. It's always the little things that trip you up, like an ingrained habit of pressing a fingertip to your ear so people don't think you're ignoring them. "You're right. I was on the phone. My mother called."

Another soldier snickered. "Yeah. Sure. What did dear old mum want? Grandkittens?"

If a CPMD- person had said anything of the sort, I'd take it as a slur. Coming from another with CPMD, it was merely rude. An old, worn sign placed by the Commonwealth gave me the perfect lie. "She thinks I'm on vacation. I told her I'm out hiking and met some interesting men, but none of them were really my type."

"Hear that, guys? Princess thinks we're not her type."

"Well, you did ruin it for the rest of us." Another soldier shrugged. "What's the big deal? If her mom brings backup, we'll just slit this bitch's throat and the kid's."

It was just as well that they announced his intent. Knowing they'd kill me and Brubaker if I had called in support meant I wouldn't have to feel guilty about going all-out if it came to violence. Not killing more of Dusk Patrol would be nice, but I had a witness to protect and dreams to pursue once I had served my time as an Adversary.

Nobody said anything of substance the rest of the march back to Clarion. My unease grew as we approached the town, for there was no sign of the Sheriff's department or town militia. Instead, the streets were full of residents going about their normal business and visitors beginning to stream in for the annual Clarion Rocks music festival. They recoiled at our approach, though I suspect the tendency of some of the soldiers to leer at young women had something to do with that.

It wasn't until we reached the Clarion police department headquarters that Deputy Colby sprang her trap. As we approached, there was a sudden rumble of boots on pavement around us as Sheriff's deputies and civilians armed with shotguns, farm implements, and a sledgehammer encircled us. Colby had barely stepped up when the soldiers of Dusk Patrol put their hands up. I suppose they realized they had little chance of taking out a force of a hundred when they numbered less than twenty.

One of the men slowly stepped forward, his empty hands held high. "Can you guarantee our safety if we surrender?"

Colby nodded. "I can guarantee that nobody will harm you and yours under my command, but I must place you and your men under arrest for murder."

Indignant shouts rose from the men. "But they murdered our friends first." "This is bullshit!"

I expected Renfield to say something, to make some attempt at persuading the men who once followed him, and now seemed to follow Corporal Seward. Instead, he remained silent. He approached Deputy Colby a step at a time, making no sudden movements that a deputy or irregular with shaky nerves and an itchy trigger finger might mistake for aggression. When he was three steps away from Colby, he turned to face his men, and held his hands behind his back for the handcuffs.

The other Dusk Patrol survivors followed him into the jail, escorted by Colby's deputies. Most of the civilian volunteers dispersed. Kaylee was last to leave, giving a parting wink and a text admonition. ｢Enjoy him while you can.｣

I tried to ignore her as she sashayed past Mike and slapped his ass on the way. Soon I was alone with him. He glanced at the jail. "You aren't going to put me in there with them, are you?"

"I'm not sure there's room. So it looks like you're my prisoner."

He studied me, as if not sure if I was joking. "You really think I aided and abetted Sheriff Robinson's tyranny by keeping quiet?"

Shrugging, I led Brubaker away from the Sheriff's Department. A smart kid like him should have figured out that it was a bullshit charge by now. If I tried to put him in front of a jury, any green lawyer could make a case for witness intimidation on Robinson's part. "It kept those soldiers from lynching you, and gives me an excuse to keep you close that Robinson can't overrule."

That got the wheels turning in his head. "So, if he tries to take me from you, you can nail him for interfering with your investigation?"

"I knew you'd figure it out." Unfortunately, that still left Renfield where Robinson could get at him, but I was more concerned about Brubaker. At his age, Renfield ought to be perfectly capable of taking care of himself.

"What's next, then? Back to The Lonely Mountain? You still haven't gotten any info out of Tetragrammaton."

Quite true. With a suddenly full jail, I doubted the Sheriff's department would get underfoot, which provided a golden opportunity to do some data mining. Besides, I also had an incoming message notification. Looks like Malkuth not only came through on the search warrant, but cracked Tetragrammaton and gave me the keys.

Regardless, a return to The Lonely Mountain seemed an excellent idea. I needed to eat, and a hot shower would be nice. Then again, duty demanded I get back to work immediately. Amid conflicting demands, reason asserted herself and forced a compromise: eat first, then head back to Gibson Hacker Supply.

Fate, or at least Dr. Petersen, had other plans. As soon as we had taken a table in The Lonely Mountain's common room, the doctor took a chair right across from me and Brubaker. "I hear you had a rough night at Fort Clarion. Were you injured?"

"I'm fine, but the same can't be said for some of your younger patients."

The lack of emotion in Petersen's expression and voice meant something. "A tragic and unfortunate loss. They were witnesses under your protection, were they not?"

They were, you son of a bitch, and if I find the slightest scrap of evidence of your involvement in their deaths, I will bloody well crucify you. Not that I said anything of the sort. "A smart Adversary learns when to delegate. Fortunately, I already saw to it that the culprits are in custody."

Petersen steepled his fingers before him, and regarded me in silence a moment. "Yes, you did. You managed to persuade Dusk Patrol to surrender. That makes you a most intriguing young lady. I wonder, what will you do next?"

He wonders, does he? I smiled at him and the approaching Bruce Halford. I hoped the good doc could handle disappointment, because right this moment my plans consisted entirely of an outrageously large breakfast. "Hi, Bruce. The usual for me and Mr. Brubaker, please."

"No problem. What about the doctor?"

*Physician, feed thyself.* I would get to Petersen soon enough. Once I had enough dirt to bury the son of a bitch.

---

### This Week's Theme Song

"Jailhouse Rock" by Elvis Presley

{% youtube PpsUOOfb-vE %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
