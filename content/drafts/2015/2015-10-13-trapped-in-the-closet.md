---
id: 1943
title: Trapped in the Closet
excerpt: This has fuck-all to do with being queer or R. Kelly's song. It's about not being able to participate in fandom.
date: 2015-10-13T22:00:06+00:00
header:
  image: persona-suicide-summoning.jpg
  teaser: persona-suicide-summoning.jpg
categories:
  - Personal
tags:
  - alienation
  - bad parenting
  - bullying
  - childhood
  - connections
  - depression
  - Doctor Who
  - geeking out
  - fandom
  - participation
  - low profile
  - marketing
  - negative self-talk
  - persona
  - self-talk
  - social
  - social media
  - tribes
---
{% include base_path %}

When women or people of color speak out about geekdom not being inclusive and welcoming, I sympathize. There's a huge Nation of Geek out there, full of different tribes for every possible fandom, and women and POCs know it. They're out of the closet and desperate to be part of the conversation. They're just not being allowed to geek out with everybody else because they don't "fit in" according to the white male outcasts who originally banded together because _they_ didn't fit in.

My problem is different. I _should_ fit in easily. I'm a white guy. I'm an [ANSI standard programmer](http://catb.org/jargon/html/appendixb.html). I write science fantasy. Hell, [I've even published some stuff](http://amazon.com/author/matthewgraybosch) through a small press.

You'd think I'd be the sort of guy who could talk people's ears off about the stuff I'm into. But every time I think to reach out, I have to deal with my negative self-talk. In fact, I'm dealing with _this_ shit right now.

![Welcome to NOBODY CARES! Population: over 6 billion]({{ base_path }}/images/nobody-cares.jpg){: .align-center}

I didn't always have this problem. Apparently I was more annoying than a Scientologist on speed when it came to whatever I was into at the time when I was a very young kid, and just wouldn't shut up about whatever I had just read. Maybe I was so entranced by the fact that there was all this information available and I could just _take it_ if I could find the right books that I made a nuisance of myself.

The adults I annoyed were fairly tolerant. Kids my age weren't. To them, I was just the weird kid. There wasn't a tribe of weird kids I could join where we could be nerdy together until I was in _high school_, and by then it was too late. Since I stuck out, I was a target.

My parents did their best. They honestly did. At least, that's what I need to believe so that I don't have to hate them. I'd rather not have to hate my parents, because life's too short to spend it trying to avenge your childhood.

![What my parents taught me]({{ base_path }}/images/what-my-parents-taught-me.png){: .align-center}

Actually, screw charity and filial piety. Even if my parents did have my best interests at heart, they made my adult life a hell of a lot harder just to make my childhood a little bit easier. That's what I call _bad parenting_.

I've been living with this "keep a low profile/fit in to be safe/nobody cares" self-talk for thirty years now. That's right. That's thirty years I've spent undercover, doing my best to fit in as a normal person, to keep my interests to myself. I'm a spy who's gone native.

What has living in the nerd closet gotten me? I'm a man without a country. I'm not comfortable in any fandom. It takes a herculean effort for me to reach out to other people. I must force myself to talk about science fiction and fantasy media. I must force myself to talk about heavy metal. I must force myself to talk about video games. Yes, I must _force_ the words right out of my godforsaken mouth.

Even an interaction like this is a struggle, which it might not be if I was a sane, fully functional person:

> @randomdoctorfan: Hey, @MGraybosch, you a Doctor Who fan?
>
> @MGraybosch: Yep. I grew up watching Tom Baker's run. Used to have a crush on Mary Tamm.
>
> @randomdoctorfan: What about the revival? Who's your favorite?
>
> @MGraybosch: I think I still like Eccleston best, though Matt Smith's first season was fun, too.
>
> @MGraybosch: I still joke with my wife about getting some custard to go with the fish sticks when we're shopping.
>
> @randomdoctorfan: Is there any Who influence in your books?
>
> @MGraybosch: Dunno, but Claire has a t-shirt that says, "What happens in the TARDIS stays in the TARDIS."

And yet, I do it. Even though I wanted nothing more than to just spend the week in bed, I did the [2014 World Fantasy Convention](http://worldfantasy2014.org/). I went out there, talked with other authors, sat at the table and signed people's programs, and was even ready to give a reading &#8212; only nobody showed up. I'll do it again (though possibly without a reading slot) at [WFC 2015 this November](http://www.wfc2015.org/).

![Artwork inspired by ATLUS' Persona 3. Sometimes summoning your persona isn't enough. Sometimes you need to FORCE it to come out and play.]({{ base_path }}/images/persona-suicide-summoning.jpg){: .align-center}

And I'll do it while living with the depression I've struggled with since I was a teenager. Spare me your congratulations. I'm not a fucking hero for telling the world I'm broken. By doing so, I'm giving the world a victory over me. I'm saying, "That's right, fuckers. You broke me."

Thing is, I'm only broken. I might have to fight through my negative self-talk every time I write a blog post, or participate in social media, or interact with fans in person, but I'll damn well do it. I'll keep working my shitty day job, too, even though I leave the office every day wishing I had been fired.

I might even smile when I talk to you, though I might be screaming inside from the effort. So if you've ever been where I am now, if you've ever faced the struggle I face, this song's for you.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A2M5WTwqnkyz7bW6P1CiD6q" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

And if you _are_ broken like me, and feel there's no place for you in the world so you might as well kill yourself, stop and talk to me first. If you feel that the only way to leave your mark on the world or give meaning to your life is by taking a gun and stealing other people's lives, stop and talk to me first. I can't heal you &#8212; I can't even heal myself &#8212; but I've been there and I'll listen.

Tell your story here in the comments, men, if you have one you need to share. Use the name you were given, or any name you choose for yourself. All I ask is that you be considerate towards others telling their story here as well. They're broken, just like you. Let's not make it worse.
