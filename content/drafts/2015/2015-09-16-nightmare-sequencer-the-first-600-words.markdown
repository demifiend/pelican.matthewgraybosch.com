---

title: "Nightmare Sequencer: The First 600 Words"
modified:
categories: starbreaker
excerpt: "Rather than waste time playing Final Fantasy in between doing what it takes to get Silent Clarion's serial finished and the book published, I'm starting the new Starbreaker novel -- Nightmare Sequencer."
tags: [nightmare-sequencer, new-novel, claire, young-adult, opening]
cover: starbreakercollage.jpg
date: 2015-09-16T13:08:25-04:00
---

Rather than waste time playing *Final Fantasy XIV* in between doing what it takes to get [Silent Clarion's serial finished and the book published](/starbreaker/2015/09/13/an-overdue-update/), I'm starting the new Starbreaker novel: *Nightmare Sequencer*. 

*Nightmare Sequencer* is a Starbreaker novel featuring Claire Ashecroft from *Without Bloodshed*. She's the happy-go-lucky, kinky, profane, grey hat hacker/otaku who makes ANSI standard manic pixie dream girls look like fuckin' Eeyore, but she ain't here to help mopey young men learn to enjoy life -- even if she enjoys pegging them.

She's sixteen in this book, so I have to tone thing down *just* a little, but her adventures in this book will make her some new friends. Like Morgan Stormrider and Josefine Malmgren, who I still haven't properly introduced yet. Meantime, here's the first 600 words of *Nightmare Sequencer*. It's rough, and subject to change.

Also, there's a Spotify playlist and each chapter is named after a theme song.

<iframe src="https://embed.spotify.com/?uri=spotify%3Auser%3A1238897208%3Aplaylist%3A1XQ83z55VoXx07qtVRZu8n" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Track 01: Ayreon — “The Dream Sequencer”

It was hard not to suspect something was up when the technician at InceptActive stopped me from using my usual creche for today’s test session. This was supposed to be a bloody literal dream job for a girl my age: test a new massively multiplayer RPG that used computer-guided lucid dreaming instead of virtual reality or the classic player-in-chair-staring-at-screen paradigm. 

Even better, all the NPC enemies are AI-controlled, and the AI can tailor the game to challenge you in real time. Not to mention the network features were incredibly flexible. One person I invited into my game as an ally turned on me halfway through. An enemy who had invaded my game took issue with this, and fought beside me against my betrayer.

The reality wasn’t quite as attractive. It still wasn’t bad, since we got bragging rights, great pay, and the kind of wild parties normally confined to a university setting.

IncestActive — what we play-testers called our employer in the privacy of our own heads because it was a cheap laugh — wanted as much play-testing done as possible with a bare-bones budget. Since more than one CGLD session per day was a bad idea because the brain needs at least one free REM cycle to handle background processing and cleanup, we worked three hours a day.

Naturally management wanted more than one group of testers working per day, so we played in shifts and hot-bunked. Last week, I got stuck with a bunk used by some poor bastard in dire need of medical attention. 

Thank Eris and all her twisted sisters I only had to deal with the lingering remnants of his fartulence, but the only explanation I could imagine was that Satan crawled up his arse and died, only to have half of Hell join him. As it was, I had to play while under the effects of a non-stop poisoned status that constantly sapped my hit points. Talk about real life intruding upon the game; it’s usually the other way around.

Knowing this, the presence of a tech at my assigned creche meant some serious shit had just happened. Furthermore, it was already empty, which was unusual. I usually liked to arrive early, and say hello to my fellow testers as they came back to reality. You wouldn’t believe how many hook-ups I’ve gotten from other testers that way.

Fortunately, I knew Joe Martinez. Nice guy, if you didn’t mind having him stare at your arse. I certainly didn’t. “Why so serious, Joe?”

Poor bastard actually *crossed* himself. Had no idea he was Catholic. Not that there’s anything wrong with that. “Something’s wrong with this rig. Last kid to use it died in it. EMTs must wheeled him out the door just before you arrived.” He glanced around, as if afraid management might be listening. “Only reason I said anything is that I know you like to arrive early and chat up the other testers. We’re gonna get you a different one.”

“Bollocks to that, Joe. I’m taking this one.” 

I was already sitting inside when he grabbed my arm. “Claire, come out of there. What if the last kid’s dream killed him? If you use the same rig, you might dream his dream.”

“And what if the last kid faked his medical records to hide a condition that made CGLD gaming dangerous for him?” That made him think, but he wasn’t convinced. “Look, mate, if I worried about what might go wrong, I’d still be a virgin. Strap me in and fire it up.”

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A4YfaezRBBglPDYiq211A1E" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>