---
title: "*Silent Clarion*, Track 27: &quot;Skin O' My Teeth&quot; by Megadeth"
excerpt: "Check out chapter 27 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Upon my return to town, my first priority was to see Dr. Petersen about getting my side patched up. Unfortunately, Sheriff Robinson had other ideas. Worse, he came with sufficient force to compel a change of plans. Four deputies I didn't recognize followed him, which would make it five against one if we came to blows. Since I was already wounded, and worn out besides, a street brawl was the last thing I needed right now.

Though none of the men surrounding me had drawn their weapons, Robinson was the only one who didn't have a hand on the hilt of his service gladius. "Adversary Bradleigh, I need you to come down to the station and answer some questions."

Despite my reluctance to fight the law in the most literal possible sense, I was equally unwilling to meekly submit and let Robinson detain me when I needed a doctor, a shower, breakfast, and a nap—in that order. "Am I under arrest, Sheriff? If so, what is the charge?"

Robinson raised his arm to block a deputy who had stepped forward and drawn his sword halfway. He must have used secure talk to reprimand his subordinate, because the deputy's expression became sheepish as he backed up and let go of his sword. Robinson shook his head before looking at me again. "You're not under arrest yet, but there's been a murder, and I need to ask you some questions."

Oh, this was just bloody great. It could be hours before I got the care I needed if I went with Robinson. "Sheriff, unless you plan to arrest me, I must insist that you let me come to the station at noon. I was hurt this morning, and I need a doctor."

Robinson finally noticed my left hand, which I kept pressed against my side to control the bleeding. "Whose blood is that all over your hand, Adversary?"

"Mine. I was attacked by two men in the forest. One of them managed to cut me."

Wrong answer, I suppose, because the deputies drew their swords and surrounded me while Robinson began to notify me of my rights. "You have the right to remain silent. Anything you say will be treated as evidence against you. You have the right to an attorney. If you cannot afford an attorney, you will be provided one. You have the right to examine the evidence against you. You have the right to humane treatment while in custody. Do you understand your rights as outlined, Adversary?"

There was no reason for me to put up with this. Not when I could pull rank as an Adversary and summarily strip Robinson and his deputies of their authority for getting in my way. ｢Some backup would be handy right about now, Malkuth.｣

｢We don't have any Adversaries available in either New York or Philadelphia, Naomi. Sorry. Think you can manage on your own?｣

｢I think I'll manage to put my boot up your arse on my own.｣

Without the threat of backup arriving within minutes, I would have to back my command with force. Taking on a couple dozen swords when I was already hurt didn't strike me as a sound decision on either a tactical or a strategic level. Even if I could win by violence, I would only make enemies. Better to fold and play a stronger hand later.

"Yes, Sheriff." Unbuckling my sword belt, I peace-bound my weapon and offered it to Robinson. "I will not resist, but I must insist that you call Dr. Petersen. It would be inhumane to deprive me of needed medical care."

"I can't just call the doctor. I've got to have a deputy verify that you're injured."

Is he serious? I opened my jacket, and lifted my bloodied blouse. Robinson and some of the deputies could see some of my nipples, but I didn't care at the moment. The gauze I had taped over my wound was saturated with my blood, and red trickled down my side. "Still think I'm faking an injury, Sheriff? Get me a fucking doctor."

"Jesus wept." Robinson glared at the deputies and brandished the sword I had surrendered to him as a sign of good faith. "Put your swords away and return to your duties. I can handle it from here."

He offered his hand as they obeyed. "You can still walk to Dr. Petersen's office, right?"

I had already walked a couple of kilometers with this wound. What was another twenty meters to Petersen Family Medicine and Physical Therapy? "We're practically there, Sheriff. But it was kind of you to offer."

The bitchy nurse I met the last time I visited Dr. Petersen's office wasn't there. A young man named Thorvaldson had taken her place. "Good morning, miss. You don't look like you're here for a routine checkup."

"I wish." I fished my wallet from my pocket and showed Thorvaldson my ID. His eyes widened as he realized who I was. "I'm wounded, and need medical attention. Is Dr. Petersen available?"

Thorvaldson shook his head. "He's with another patient, Adversary, but come with me. Sheriff, please wait here."

Robinson shook his head. "Nurse, Adversary Bradleigh is in my custody. I can't let her out of my sight."

Thorvaldson stood his ground. "I'm not going to let her slip out the backdoor, Sheriff. Now sit down and shut up. I won't have you staring over my shoulder while I work."

"Fine." Robinson slumped into a chair and picked up a magazine as the nurse led me to an examination room.

Once the door closed, I removed my jacket and shirt so Thorvaldson could work. "Do you need me to lay down?"

He held out a hospital gown while looking away. "It might be easier for us both if you laid on your side."

"Fair enough." I stretched down on the padded examining table, and adjusted my gown. "I might need stitches and an antibiotic treatment."

Thorvaldson nodded as he peeled my bandage from me and dropped it into a biohazard container. "Nasty cut, just deep enough to need stitches, and it's still bleeding. When did this happen?"

"Two hours ago, I think. I kept pressure on, but couldn't do much else."

"Shit." Rather unprofessional language for a nurse to use around a patient, but my blood pressure reading was lower than it should be. "You must be pretty damn tough to still be on your feet. Do you know your blood type? You're going to need a transfusion."

Good question. This was another one of the differences between people with CPMD and people without it. Regular people fit the usual ABO blood group system.

People with CPMD also fell into four major blood types: X, Y, XY, and Z. None of these types were compatible with regular human blood types. Giving me blood from a regular person could kill me, and giving a regular person my blood was equally dangerous. "XY negative."

Thorvaldson punched this into a handheld. "Good. The local blood bank has a couple of units in stock. Now, I can stitch you up and do the transfusion, but I'm not qualified to prescribe medication. We'll need Dr. Petersen for that."

"All right."

He handed me a tablet displaying an informed consent form and a stylus. "I just need you to review this and sign at the end."

I did so, and reached for the glass of water Thorvaldson placed beside me. I sucked half of it through the waxed paper straw, and closed my eyes as the nurse injected a local anesthetic and set about stitching my side closed. His hands were swift and sure, and he whistled as he worked. I recognized the tune, an uncharacteristically melodic tune by Doomed Space Marines.

Somebody knocked on the door as Thorvaldson tied off the suture and snipped it close to the skin. It was Dr. Petersen, and he brought a folded t-shirt with him. "Sheriff Robinson told me you ruined your shirt in an altercation in the woods. I never figured you were the sort for midnight duels."

"Two against one is hardly a duel." It was probably a mistake to say that, but neither Petersen nor Thorvaldson commented it.

I waited for Thorvaldson to finish applying an ointment similar to what I had in my first aid kit. He then covered the area with a liquid bandage. It rapidly dried, resembling lacquer. Once I was sure I wouldn't ruin the bandage, I turned away from the men and slipped into the tee. Now I was a walking advert for Dr. Petersen's practice. "Thanks. Will it be safe for me to shower later today?"

"It should be. The nanocytes in the salve will dissolve the sutures within forty-eight hours. After that, they'll eat the bandage. Handy stuff, isn't it, Doc?"

Petersen nodded. "Too bad we didn't have this kind of tech before Nationfall. I might have saved more soldiers." He glanced at the chart. "Did you order two units of XY negative for the transfusion?"

"I was going to send Monica to the blood bank, but I can go myself if you want to set up the IV and start the saline drip."

"Two units should do. Once you've brought them, I can handle the rest."

Thorvaldson nodded, and shut the door behind him. Holding out my left arm, I clenched a fist so Petersen could find a vein. He nodded in approval, and swabbed me with alcohol. "Looks like you know the drill. I guess this isn't your first transfusion."

"Actually it is, but my usual physician likes to take blood and run his own tests instead of just pulling the diagnostics off my implant."

Petersen chuckled as he started the saline drip. "Oh, so, he's old-school. I can't blame him; I do that myself when the implant reports something anomalous. I think it pays to have a person confirm the diagnosis. Most of my patients appreciate the effort, and it helps keep me sharp."

"No doubt it's cheaper to use your own brain than to get an AI."

"That, too." Thorvaldson stuck his head in long enough to hand Petersen the blood units before retreating. Until Petersen hooked them up, they looked like vampire takeout. The crimson thread working its way down the line into my arm fascinated me, and for some perverse reason, I wondered whose blood this was. It didn't matter. After this mission, I'd hit a blood bank and make a donation of my own to pay it forward.

"So, what happened to you?" It was the question I expected Petersen to ask, but half-hoped he wouldn't. I wasn't sure how he'd take the news that I had cut down two of the men he once commanded. "And what did you do to your shoulder? That bandage isn't Thorvaldson's work."

Damn it. Now I had to explain that I had met Renfield before the fight. Of course, one might argue that the former led to the latter. Besides, it might rattle Petersen enough to make him reveal information he might otherwise keep to himself. If I was back home seeing my usual doctor, I'd have mentioned my leg, as well. With her, it wouldn't have mattered that I was going commando by necessity. "My shoulder? That was a love bite from one of your former subordinates. Do you remember Christopher Renfield?"

---

### This Week's Theme Song

"Skin O' My Teeth" by Megadeth

{% youtube ePSR7gFt7s4 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
