---

title: "A Favorable Review of Silent Clarion: The Geographic Cure"
modified:
categories: starbreaker
excerpt: "Sarah over at Ink of Blood had lots of nice things to say about Silent Clarion: The Geographic Cure."
tags: [reviews, silent-clarion, the-geographic-cure]
cover: silent-clarion-new-banner.jpg
date: 2015-07-22T14:06:38-04:00
---
It's hard when you're a relative unknown to get people to review your work. Fortunately, [Curiosity Quills](http://curiosityquills.com) tries to help.

Sarah over at [Ink of Blood](http://inkofblood.com/2015/07/02/blog-tour-silent-clarion/) had the following to say:

> I did like the main character in this book. Naomi was a character I found really easy to relate to. I would have liked a bit more detail about why she had a slightly different appearance… but she liked cats, which was a point in her favour.

Well, I *did* mention that she was CPMD+ positive, didn't I? No matter. It will come up again later on. I'm glad Sarah liked Naomi and found her relatable. At risk of generalization, it seems that women tend to be more successful in depicting male characters than men are in depecting female characters. Knowing I did well pleases me.

> It was good to see Naomi’s relationships with the other characters, particularly Jackie. However, I would have liked a bit more detail about how Naomi’s biological parents still kept their rights to her, since I don’t think that’s possible in present times. I did enjoy seeing Naomi with her parents and brothers, though. It was nice to see that Naomi had a good relationship with her family.

Hmm... I do recall mentioning stating that Naomi was fostered, not adopted. Laws governing the rights of parents with children in foster care vary between states, and would also be different in the UK -- so I have a *bit* of wiggle room. Besides, given who her father is, I think he can pull some strings to ensure he could remain involved regardless of the law.

> I thought that the job Naomi performed was a really interesting one. Even though I would have liked to know a bit more detail about the organisation’s history (and why they trained to fight with swords), I thought the idea was a unique one and something I could see actually forming in the future.

This is the problem with just dropping people into a story instead of inflicting infodumps on them. Questions always come up. I guess I'll have to write blog posts explaining this stuff. =^.^=

> It was interesting to see how the society had discovered and named some of the genetic traits, such as the ability to only father male children. I was a bit unclear about what all of the coding referenced, though and why it mattered.

Not sure what Sarah meant by "all of the coding", but that's OK.

> I felt this book worked really well to introduce me to the world and the characters. I would have liked to see more of the technology and also get a bit more background about the disappearances Naomi heard about. What I did read piqued my interest in learning more, though.

She'll get her wish concerning the tech, especially in the second half of *Silent Clarion*. She might love *Without Bloodshed* as well. Lots of crunchy tech there. But the disappearances will come sooner.


