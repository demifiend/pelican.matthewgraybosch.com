---
id: 2190
title: 'One Line Wednesday: 21 October 2015'
excerpt: "Every Wednesday, the #1LineWed hashtag trends on Twitter. Sometimes I participate."
date: 2015-10-22T22:08:02+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - '#1LineWed'
  - challenge
  - one sentence
  - Twitter
  - writing
  - Starbreaker
---
{% include tweet.html %}

Every Wednesday, Twitter has a #1LineWed hashtag trending. The idea is to share one line, a sentence or two, that evokes interest in your story. Here are mine. They moistly come from Starbreaker stories I haven't written yet. I'm reclaiming them from Twitter so they don't get lost in the noise.

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Naomi expected Morgan to protest. Instead, she saw a wan smile. "I was afraid. I still am. I came because staying away hurt more." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656988193917636608">October 22, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "You hid from me, you bastard. Don't pretend you did it out of consideration for me. You were afraid of what I would say." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656987550670811136">October 22, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Why didn't I come see you sooner, Naomi? You were finally getting past your grief. What right did I have to interfere?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656986602204438528">October 22, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Slipping past the mage, Morgan glanced over his shoulder. "Second chance, huh? I'd better not waste it on your crusade, then." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656985214854537216">October 22, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Squaring his shoulder, Desdinova placed himself in Morgan's path. "We're giving you what nobody has ever had. A second chance." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656984873266212864">October 22, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "It means you've manipulated me as surely as your brother Imaginos did. My only debt to you is a boot in the ass, Desdinova." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656981531647459328">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Morgan's smile was murderous. "You didn't tell me that breaking Imaginos' avatar would unleash Sabaoth. Know what that means?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656980791780622336">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Desdinova bridled at Morgan's words. "I brought you back to life, and this is how you thank me?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656980071564091392">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "And you. You're Josefine Malmgren?" "Yes." "Claire told me about you. Make sure this lying asshole pays you in advance." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656978983213506561">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Before Josefine could say anything else, Morgan sat up. "So, Desdinova. You couldn't just let the dead rest, could you?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656978211272814592">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Then you have a problem. That isn't the body he grew up in. It isn't HIS body. I know of no therapy to help him make it his own." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656976242646896641">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "A familiar touch, Dr. Malmgren? We dare not alert Naomi Bradleigh. We must keep Morgan's resurrection secret from her father." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656975459847151616">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    A shrug from Josefine. "This is outside my experience, Desdinova. Perhaps a familiar voice or touch is what Stormrider needs?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656971971150917632">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "If Stormrider is in this 250 series asura emulator body, why does he not wake?" Desdinova considered the inert form on the table. <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656971399966404609">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Josefine met Desdinova's expectant gaze. "The data transfer went perfectly. Morgan will remember it all, including his death." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656970296642113536">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Morgan staggered, the skeletal remains of his hands falling apart, as the onlookers retreated from him. "Call help. Please." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656851773089587200">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Morgan raised his broken sword as if it were still whole, and not merely a conduit for the storm within. "Mênin aeide!" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a> <a href="https://twitter.com/hashtag/homeric?src=hash">#homeric</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656834513927057408">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Your sword is broken, little asura, and your guns empty. How will you defy Me now, little Stormrider?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a> <a href="https://twitter.com/hashtag/starbreaker?src=hash">#starbreaker</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656833221322240000">October 21, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Who am I? I am an Adversary, sworn to eternal hostility against every form of tyranny. Including that of pretentious demons." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/656829786094989312">October 21, 2015</a>
  </p>
</blockquote>
