---
title: "Update: Silent Clarion"
modified:
categories: starbreaker
excerpt: "Current word count for Silent Clarion: 101,900"
tags: [silent-clarion, progress]
cover: silent-clarion-new-banner.jpg
date: 2015-07-21T23:31:32-04:00
---
I'm not sure when this happened but according to my usual method of combining all of my chapter files and using the Unix **wc** tool to get a word count, *Silent Clarion* has broken the 100,000 word milestone. It must have been when I finished Chapter 54.

![Silent Clarion at 101,900 words](/images/silent-clarion-wc-20150721.tiff)

Chapter 53 is going up tomorrow at [Curiosity Quills](http://www.curiosityquills.com), I finished Chapter 55 this afternoon on my lunch break, and I've started on Chapter 56. Naomi is ready to fight Sheriff Robinson and Dusk Patrol on their own ground &mdash; Fort Clarion in the dead of night &mdash; but they have no intention of making it easy for her.

And I've got *nasty* surprises in store for the poor woman once she finally takes down Sheriff Robinson. Oh, yeah...

In addition, I have to do revisions for Episode 2 of the *Silent Clarion* Kindle serial, **Always the Quiet Ones**. It shouldn't take long, since editor [Matthew Cox](http://www.matthewcoxbooks.com/wordpress/) only suggested about 30 changes, and claimed they were "minor". I'll probably knock that out tomorrow.

One of these chapters will have to use this track by Japanese melodic death metal/trance outfit [BLOOD STAIN CHILD](http://www.metal-archives.com/bands/Blood_Stain_Child/9224) as a theme song.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A0YyBaPhV9rGcDhs3dS7V6q" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
