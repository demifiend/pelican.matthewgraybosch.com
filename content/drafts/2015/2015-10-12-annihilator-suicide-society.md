---
title: "Annihilator: *Suicide Society*"
excerpt: The classic Canadian thrashers are back with more socially conscious metal.
header:
  image: annihilator-suicide-society.jpg
  teaser: annihilator-suicide-society.jpg
categories:
  - Music
tags:
  - 2015
  - Annihilator
  - Canada
  - socially conscious
  - thrash metal
  - review
---
Canada’s classic thrash metal act [Annihilator](http://www.annihilatormetal.com) are back with their fifteenth album: _Suicide Society_, nine cuts of excellent thrash metal whose lyrics displaying the sardonic wit and social conscience I've come to expect from founder, lead guitarist, and vocalist Jeff Waters. Here’s the standard edition.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A0f3QvZWjlFfA9x74BgUEoW" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Singles/Videos

Annihilator's current label, UDR Records, released three videos to promote the new album. Here they are.

<iframe width="560" height="315" src="https://www.youtube.com/embed/BDDdAugQZxs" frameborder="0" allowfullscreen></iframe>

---

<iframe width="563" height="337" src="https://www.youtube.com/embed/Yiruzi0eBq8" frameborder="0" allowfullscreen></iframe>

---

<iframe width="563" height="337" src="https://www.youtube.com/embed/CHR_pi9x8fg" frameborder="0" allowfullscreen></iframe>

## Deluxe Edition

In addition to the standard nine-track edition of Suicide Society, UDR Records also released a deluxe edition including:

  * The Ravenstreet Sessions, a setlist from a typical live performance.
  * An interview with founder, lead guitarist and vocalist Jeff Waters.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A1x8yIKhsOjPAciayvWtN4i" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Verdict?

I suspect that _Suicide Society_ will be one of my new favorites. The guitar work is top-notch, the vocals are clear and audible over the rest of the band, and the riffs have me banging my head.

The title track is a blistering indictment of modern capitalism that will go unheard by those who need to hear it most (the same people who need a boot in the ass). “Creepin’ Again” would have been perfect for a _Nightmare on Elm Street_ soundtrack had it been released in the late 80s. “The One You Serve” has a lengthy instrumental instrumental reminiscent of “The Fun Palace” from _Never, Neverland_.

Check it out. If you still buy CDs, this is one to add to your collection.
