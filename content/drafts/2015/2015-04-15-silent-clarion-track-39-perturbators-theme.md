---
title: "*Silent Clarion*, Track 39: &quot;Perturbator's Theme&quot; by Perturbator"
excerpt: "Check out chapter 39 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
The Sheriff's department was still busy when I left, but the cleaners had already arrived and stood by their van, smoking. No doubt Mr. Wesker saw to that. If  not for the circumstances of Ernest Yoder's murder and the ongoing investigation, I daresay he would have put out a "for rent" sign already. Despite my sympathy for his losing his wife a few years ago, he was still a landlord, and landlords tend to be money-grubbing arseholes.

My stomach twinged, and I couldn't tell if I was still nauseous from disgust, or queasy because I had lost my breakfast. I would have to eat again before I did anything else, or I wouldn't be able to focus. By then, Robinson might be back at the station, and I could ask him about Wesker's wife as I had promised to.

After a second shower, and a second breakfast, I walked over to the Sheriff's office and checked with the duty officer. "Hi. I'm here to check out Ernest Yoder's computer."

"It's not here. Sheriff Robinson said you should head over to Town Hall. He'll tell you more there."

That's odd. Was the computer at Town Hall? Since it was accessible to civilians, it didn't make sense to keep evidence there. Hoping Robinson would have a reasonable explanation, I got directions from Cat and headed down into the basement. "So, Sheriff, where's Yoder's computer? The duty officer told me to see you."

Robinson nodded. "I figured that since we had photos of the original crime scene, we could just pull everything out of that bedroom and use a room down here to recreate it."

"Show me."

Robinson complied, and held the door for me as I stepped into a brightly lit basement room with all of Yoder's furniture arranged as it had been in the apartment. The bed, fortunately, had been stripped and the mattress removed. Before doing anything else, I used my implant to photograph the room and sent the images to Malkuth with a note explaining what Sheriff Robinson had done and asking him to run his own comparison with the original.

The nightstand drawer held nothing of direct relevance to my investigation, though the contents shed light on the solitary existence of Ernest Yoder. The container of skin cream for men was a high-end brand full of exotic ingredients, and not available in shops. Despite the shelves crammed with non-fiction and complex novels, his taste in magazines suggested he didn't read them for the articles. I found a lone issue of *Tomcat* beneath the well-worn issues of girlie mags like *Madonna to Whore* and *Harsh Mistress*.

The cover was a familiar one. It was the same pre-Nationfall issue I had found in the Fort Clarion post exchange, with the blue-eyed snow-blonde who might well be an ancestor of mine. How had Ernest Yoder gotten this, and when? It must have been a recent acquisition, judging from the lack of difficulty I experienced in flipping through the contents. "Sheriff, I need an evidence baggie and a marker."

"Find something?"

"A girlie mag that I suspect Yoder took from Fort Clarion."

"Going to show me?"

Not bloody likely, though courtesy demanded the Society share evidence with local authorities since we were both investigating the same crime. Still, there was a protocol for that sort of thing. "File a discovery request, Sheriff."

With the porno mag sorted, Yoder's bedroom beckoned. It didn't look like a standard machine that offers just enough local capacity to connect to an AI megaframe and store a user's current work should the network connection fail. Instead, this was a full-featured personal machine, and most likely custom-built. A cursory look in one of his desk's drawers showed he had the tools for the job. The damned thing was water-cooled. Neon lights inside the transparent case flared to life as I plugged in HermitCrab and fired it up.

Glancing at the attached graphic tablet and stylus, I added this to my mental picture of Yoder while waiting for the machine to boot and for HermitCrab to read the computer's internal storage. He was a loner with an active, albeit solitary, sexual life—and possibly a digital artist as well.

Regardless, I hoped that Yoder wasn't all that savvy about security. If he'd been as careless as most young people, he would probably have left a trail of digital evidence even the most incompetent amateur sleuth might have followed. Since I was a professional, and fairly competent, I anticipated little difficulty. All I needed was a starting point.

First, I tried accessing the 'secret' forum Malkuth found, whose software listened on transmission control protocol port 65535 instead of the standard TCP port for hypertext transfer protocol. Yoder had an account there, as gynophobic\_hikikomori — one who fears women and has withdrawn from society. The name was so apt, I suspected he chose it himself to throw in the faces of those who might mock him for his psychological issues.

A sneaking suspicion grew in the back of my mind as I explored the forum. The people posting here seemed to have been putting on an elaborate charade or chronicling a rich collective fantasy life and so competed to post the most lurid descriptions of the illicit activities in which their parents feared they might engage. While many of Clarion's youth probably did sneak into the woods to drink moonshine and smoke weed, I doubted they all did so. Nor did I believe they'd all gather for a moonlit orgy where they engaged in acts and configurations I suspected even Jacqueline had never heard of, let alone tried.

I did nothing of the sort, but I maintained a practice journal that suggested I was my instructors' idea of a diligent musician. Instead, I developed my vocal and piano technique with forty-five minutes each of deliberate practice per day. With six hours a day at ACS, one of which was devoted to physical training, I had enough time for self-care on Monday through Friday—and I bloody well took the weekends off.

As far as my instructors at Juilliard were concerned, all those long hours of devoted practice paid off with rapid growth. If those schmucks ever found out, it was after I had gotten my degrees and an offer to join the Metropolitan Opera of New York once my time of service as an Adversary was complete. What was that about cheaters never winning?

With the 'secret' forum a bust, I tried a system-wide text search for other instances where he used the name 'gynophobic\_hikikomori', and found it associated with every account that didn't support authentication via Secure Shell. It was even his primary login on this machine.

I suppose the poor bastard's issues were central to his identity as he understood it. Were he still alive, I daresay many a Phoenix Society psychologist would have found him an utterly fascinating case. His choice of passwords wasn't nearly as interesting, however. He used the same password everywhere, '4evr@l0n3', and if that meant 'forever alone', it was no doubt another consequence of too much time spent pitying himself with his dick in hand.

Yoder even used this username/password combination with his credit union, which allowed me an intimate look at his finances. He was indeed a graphic designer and artist, and his balance suggested that not only was he good enough do it for a living, he was good enough to earn an a better living than I did if you compared his monthly income with my Adversary's salary. Not that I begrudged him; with his problems it would have been all too easy to end up in poverty once whatever assets he inherited ran out.

The sites Yoder accessed as gynophobic\_hikikomori were similar to the hoax forum in that they offered no real insight into his character other than that he wanted to overcome his fear of women, but was afraid that if he did and brought female companionship into his life, he'd only turn out to be as abusive as his father. It was the terror of proving his father's son that kept him locked away, only to come out at night.

Out of curiosity, I tried his secure shell login. It demanded a user name and password, so I tried what Yoder used everywhere else. It worked, which for some reason didn't surprise me all that much. Once I was in, the rest of his network history was laid bare. One location stood out in its access frequency, an Internet Relay Chat site on the same IP address as both the official town forum and the 'secret' bulletin board.

It made sense. A sufficiently paranoid system administrator could disable server-side logging, making anything said on an IRC channel ephemeral unless the users enabled logging on their end. If the youth of Clarion were paranoid enough to maintain a decoy forum and use IRC via SSH, I doubted they logged anything.

My reception as I logged in was immediate, and enthusiastic. Nice to know Yoder had some friends.

> RangerMike: Hey, GH! Where you been, man?  

> Doctor\_Feelgood: Yo, GH, you got a woman over there? That what's been keeping you busy?  

> D3M0N01D: GH, that Cecilia Harvey poster you did looks great. Too bad she isn't that hot in the official art.  

> Doctor\_Feelgood: Yo, Demonoid, did you see that snow-blonde Adversary around? Dead ringer for Cecilia, bro. Maybe that's who GH is shacked up with. Lucky bastard.  

> RangerMike: Guys, I know the lady. GH is a good guy, but he'd have a stroke if he met Adversary Bradleigh. Let's leave her out of this.  

Sweet of Brubaker to stick up for me. Too bad I can't thank him without blowing my cover.

> Godfather: RangerMike is right. For all we know, she's spying on us. I caught her trying to crack the public forum database last night using that clunker my wife asked me to lend her.  

> Doctor\_Feelgood: You lent her a machine? Dude, you're fucking whipped.  

> [Doctor\_Feelgood: kicked from channel #clarion\_underground](#)  

> [Doctor\_Feelgood: banned from channel #clarion\_underground for 24 hours](#)  

Wow, Cat's husband doesn't take any shit. He'd probably come after me with some kind of blunt instrument if he knew I was lurking in the #clarion\_underground channel using a dead man's handle.

> RangerMike: So, GH, WTF happened, bro? MrSnotty and Clusterfuck are dead, and we haven't heard from you since you showed us how to get into the basement  a couple weeks ago.  

Jackpot! Man, I could kiss Mike Brubaker right now, but that would probably embarrass the poor kid. But how should I tell him that I know what happened now? If I send him a private message, he'll know I'm not Ernest Yoder unless I'm extremely careful and extremely lucky. Too bad I didn't take time to analyze his style so I could imitate it. The slightest variation from familiar usage or grammar might give me away.

> RangerMike: Adversary Bradleigh isn't stupid, guys. We should probably come clean before she figures it out on her own, or before more of us get whacked.  

> Godfather: You kids were idiots for going in there in the first place. RangerMike, you know the lady. If you don't talk to her, I will. Unless you're already lurking, Naomi. Come out and pay your respects.  

Made again? Who is this guy, and why is he pissing about in an overgrown village like Clarion when he could get tech companies in New York and London embroiled in a bidding war for his expertise? Has he no ambition?

Staring at the screen in frustration, I pounded the desk. Some hand-painted wargaming miniatures jumped at my blow, but that was all I managed to accomplish. Logging out, I immediately returned to the IRC server hosting the #clarion\_underground channel under a different name: CeciliaHarvey. It's silly, but showing these kids I can take a joke might help them open up.

> CeciliaHarvey: Fun time's over, lads. I want everybody who has been inside Fort Clarion to meet me at The Lonely Mountain in one hour. Pack a bag. GH was murdered by the same people who did MrSnotty and Clusterfuck, and it would be lovely if I could stop these arseholes before they kill more of you. Godfather, I trust you'll pass the word along to Doctor\_Feelgood.  

## Track 40: Perturbator — "She is Young, She is Beautiful, She is Next"

The #clarion\_underground channel erupted in virtual tumult at my order, and I had neither the time nor the inclination to deal with the shit storm. Nor was I about to justify myself to these kids. Instead, I pulled still images from my feed, one for each of the victims thus far, and posted them with a simple message: "Do as I say if you want to live."

Rather than stick around for the reaction, I disconnected and shut down Yoder's machine. My pace was swift as I left his simulated home and began my walk back to The Lonely Mountain. On the way, I used my implant to evaluate transportation and lodging options. I needed those kids away from here and in a safe location. If that location remained secret, so much the better, though invoking the Phoenix Society's aegis would probably serve to deter any notion of betrayal on the part of those I must perforce trust to carry these kids off to safety.

｢Malkuth, I need evac for at least four witnesses, and a safe house, in which to keep them. What can you do for me?｣

｢You figure the killers are limited to the vicinity?｣

｢Those kids are dead if I'm wrong about that.｣ How far away was far enough? Pittsburgh was definitely too close, but was New York far enough to be safe? London would be better, but could I justify it? ｢Yoder, Wilson, and Foster had all been under Fort Clarion, but other kids have been down there, too.｣

｢I just dispatched a bus from Pittsburgh to pick up your witnesses at The Lonely Mountain and transport them to the New York Chapter. We can put them up at the hotel across the street, and detail some senior ACS cadets to stand guard. It isn't exactly discreet, but the alternative was a helicopter that wouldn't be available until tomorrow｣

｢The bus is armored, right?｣ I'd love to see the look on Petersen's face as the chopper lifted off, taking my witnesses away. However, Malkuth's right. A bus would do just as well as long as Dusk Patrol doesn't waylay it.

｢Come on, Nims. Give me a little credit. I even arranged a two fireteam escort with the Fallen Angels MC. We're gonna whisk those kids away in style like badass rock stars trying to avoid paparazzi.｣

Knowing that Malkuth had hired Fallen Angels to escort the bus helped me breathe a little easier. The bikers were reliable mercenaries, and I wouldn't be the first Adversary to take advantage of their services. They were bloody expensive, however, which was why I didn't consider hiring them to tear apart Fort Clarion. Furthermore, bringing a few dozen Angels to Clarion would do little to endear me to the locals, or to local authorities. Even ten Fallen Angels seemed a bit excessive. If the Phoenix Society sent as many Adversaries on the same mission, they'd probably be dismantling an interplanetary corporation.｢Thanks, Malkuth. I suppose this is coming out of my salary.｣

｢Let's just say you're going to have a bit of explaining to do next time you report your expenses.｣

Shit. Facing an auditor over this would be no less an ordeal than this job has been, but I couldn't afford to dwell on it now. Got some lives to save. More than I expected, it turns out. Four young men and three young women awaited my arrival at The Lonely Mountain. They sat at one of the biggest tables in the common room, their bags piled up in the corner behind them.

After double-checking the bags, I cleared my throat to get their attention. "We seem to be a bag short. Who mistook this for a day trip?"

Brubaker looked up from cleaning his shotgun. "I'm not going anywhere, Adversary." His explanation was evidently for my eyes only, since it came via secure talk. ｢You need somebody who knows the woods. And I can watch your back.｣

One of the girls began to pout. "If Mike gets to stay, why should the rest of us go?"

Tempting as it was, telling the girl she had to go because I bloody well said so didn't seem likely to persuade any of the youths sitting before me. Instead, I sat down with them. "Who saw the photos I posted to IRC? Raise your hands."

The girl who complained kept her hand down. Likewise for the brunette sitting beside her. "Adversary, I didn't see the photos. David got an email from Mr. Tricklebank and told me we had to leave."

That left the complainer. She narrowed her eyes at me. "Fine. I saw the pics, but they don't explain anything. Why should we be inconvenienced because of a few dead people?"

Brubaker shook his head. "Kelly, stop acting like a bitch. We don't have time for your shit right now. You were under Fort Clarion with the rest of us. For fuck's sake, Scott was your cousin. Do you want the same thing to happen to one of us? Adversary Bradleigh probably thinks we're next."

"But why would they kill any of us? We didn't do anything wrong. The place was abandoned."

"It isn't." That got everybody's attention, even Kelly's. "Fort Clarion was never abandoned. Some of the people stationed there are still alive, and haven't forgotten their duty."

"But wouldn't they be really old?" One of the other youths had a dubious look on his face.

"Do you want to see the photos again? I fought two of them a few nights ago. They are most certainly not old, and had no need to fight at a distance. Were I a bit slower, I might have been gutted."

I had their attention now. "These aren't ordinary soldiers. They've been changed as a result of a pre-Nationall experimental program called Project Harker. Its intended result was to turn people with CPMD into vampires. The subjects were hardened soldiers before the Commonwealth Army's scientists got at them, and are all the deadlier now."

One of the young men started at my use of the name Harker, but Kelly gave a disgusted snort before I could question him. "Vampire soldiers? Under Fort Clarion? Do you have any idea how ridiculous you sound, Adversary? And we thought Yoder was fucked in the head."

"Is it really so ridiculous, Kelly?" The brunette next to her spoke up. "David, isn't there a guy who has a monthly appointment to stop at your parents' grocery store after midnight?"

The young man who had recognized the name earlier nodded. "I wanted to say something before, Jill, but didn't want to interrupt." He turned toward me. "Adversary, there's a standing order at my parents' shop that has been active since before they purchased the store. I'm pretty sure the name on the order is Harker. A man always comes to the shop on the first of the month after midnight to pick it up. He wears an old army uniform, and I think I saw the name Renfield on it. Somebody named Petersen always picks up the tab. I think it's the doc, but I'm not sure."

So, that's why Fort Clarion's pantry had fresh groceries. That son of a bitch Renfield is making monthly midnight shopping trips. But why would he make them midnight if he can get around in the day or dusk? Does he come alone, or brings men with him? And how does he cart his groceries back to the bloody base? It isn't exactly a stroll. "David, this is important. Did Renfield ever show up with anybody else?"

David's eyes narrowed, and he looked past my shoulder. Turning around, I caught a glimpse of Sheriff Robinson shouldering his way through the patrons. A few of them objected, but Robinson calmed them down by flashing his badge.

He seemed neither surprised nor pleased to find me here. "I suppose I have you to thank for the panicked parents screaming at me because their kids packed a bag and bugged out without a word of explanation. Not to mention the fucking Mayor up my ass. Care to tell me what's going on, Adversary?"

He's got the Mayor up his ass? Poor baby. "I know why Yoder, Wilson, and Foster were murdered. A couple of weeks ago, they got into Fort Clarion and poked around underground. These seven were with them. Thinking of their safety first, I arranged for the Phoenix Society to place them in protective custody."

"Where?"

Why would Robinson care about that? Shouldn't he be grateful that the Society is looking out for these kids? "My superiors didn't tell me that."

"Figures." Robinson's chuckle held a bitter note. "OPSEC, need-to-know, and all that spook shit."

"Which doesn't make your job easier, does it? You've still got all those scared parents. What will you tell them?"

Robinson shrugged. "Not my problem any longer. I told 'em to take it up with the Phoenix Society."

Thanks for nothing, but I suppose it was the sensible thing for him to do. It's not like having the kids spirited away was his idea. The revving of motorcycles outside kept me from telling Robinson I understood his passing the buck. "I think that's our ride."

Three Fallen Angels walked in, and that was not a joke. They more closely resembled soldiers than bikers; their jeans and leather had the neatness of uniforms, and their postures and habit of scanning the common room as they approached suggested rigorous training. The one in the middle even had stripes similar to those identifying Renfield sewn into the sleeve of his jacket, and he saluted with his fist over his chest like he was one of ours. "Adversary Bradleigh? I'm Sergeant Jackson from the Fallen Angels. Mind if I transmit the ID for my orders?"

As I returned his salute, I found his IP address and opened a secure talk session. "Ready."

A long string of random text came through, and I passed it to Malkuth. He confirmed its authenticity and relayed to me the orders passed to the Fallen Angels: take my witnesses into custody and escort them to a secure location. The location was redacted, naturally. I didn't need that information, and the Society didn't need me blabbing if captured. "Thank you, Sergeant Jackson. You'll be escorting six tonight."

Turning to the kids, I introduced Jackson as Mike Brubaker left them and joined me. "Sergeant Jackson and his squad will escort you to a secure location away from Clarion, where the Phoenix Society will keep you in protective custody until I've resolved the situation here. Follow his instructions, please."

"Thanks, Adversary Bradleigh. If you folks will just grab your bags and follow me, we'll get you situated. You'll be traveling in style, but don't count on the minibar being stocked." Sergeant Jackson led the motley crew out of the Lonely Mountain, ignoring the boys's disappointed groans. I guess they were looking forward to free liquor.

Robinson was still there, watching them leave. "They weren't the only reason I came looking for you. There's been another murder."

---

### This Week's Theme Song

"Perturbator's Theme" by Perturbator, from *Dangerous Days*

{% youtube lNeQ1B5zpOw %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
