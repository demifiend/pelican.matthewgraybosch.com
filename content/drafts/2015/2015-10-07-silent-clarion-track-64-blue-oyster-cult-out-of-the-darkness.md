---
title: "*Silent Clarion*, Track 64: &quot;Out of the Darkness&quot; by the Blue Öyster Cult"
excerpt: "Check out chapter 64 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
The choice was simple enough. If I made myself complicit in Ian Malkin's conspiracy, I would get a shot at the life I had always wanted. If I kept my oath and held true to my ideals, my only reward would be the ignominious death of a traitor.

It would have been reasonable to consider the possibility that even if I was convicted, enough people might still believe me and believe *in* me, and hold true to the same ideals. They might work and struggle to expose the truth, exonerate me, and expose the rot at the Phoenix Society's blackened heart.

But I wouldn't be alive to see it. I'd just be a martyr, a holy name with everything that made me a person hidden under as many coats of whitewash as those rallying around my image needed. What would that accomplish, besides bringing down the organization that rebuilt the post-Nationfall world?

Whatever would rise from the Phoenix Society's ashes wasn't likely to be a better deal for the people than the current regime. A return to the old regime of warring nation-states where the strong exploited the weak and called it "sound economics" wasn't even the worst scenario I could imagine. The real nightmare scenario was the snow-blond dandy before me taking off the kid gloves, declaring himself openly, and demanding absolute obedience on pain of death.

So that was how I sold my soul. There was no hesitation, and no doubt in my mind as I took a breath and gave my answer. "I don't get paid enough for this shit."

Malkin chuckled at my reply. "No, you most certainly don't. But you'll find in time that you made the right decision."

Before I could say anything else, he disappeared in front of me. Now you see him. Now you don't. Nothing for it but to go back to bed, since it was long after midnight. Not that the night had been young when Malkin showed up.

Though I had expected to spend the remaining hours until breakfast awake and staring at the ceiling, my eyes slipped shut and sleep claimed me as soon as I had the blankets wrapped around me. Dr. Tranh discharged me the next morning, and Jacqueline and Claire were there to meet me at the entrance.

"Oi, Nims!"

"Hi, Jackie." Letting her hug me, I patted her shoulder with my good hand. "Hello, Claire. I understand you're going back to London with your aunt?"

"Yep. Did you know Jackie's getting married?" Claire didn't look too keen on the idea. "She wants me in her party. I gotta wear a dress, dammit."

"Actually, Nims, I meant to ask earlier. I could use a maid of honor."

"I'd love to." It was as easy to say yes to Jackie as it had been to Ian Malkin. Easier, perhaps. I'd want Jackie on my side if I ever found a man I wanted to marry. "But before we head back to London and start preparations, can we get breakfast? I'm bloody famished."

"I got maglev tickets with breakfast service. Let's go."

It was the first time I had eaten something out of the maglev's dining car, and breakfast was shockingly good. The only difference between our breakfast and what we might get at a pub that served breakfast was that there wasn't a screen showing a local football match. We passed the rest of our ride in companionable silence, and I refrained from noting Jacqueline's pensive expression.

Sure, Claire seemed to be absorbed in some kind of tactical game that had her defending various locations against war pigs from outer space. However, I suspected she didn't have the volume cranked up that high. Her headphones would be no guarantee of her not being to hear us.

Perhaps I was only being meddlesome, but it seemed easier to focus on my friend's problems than the clusterfuck I had just made of my own life. In the cold light of day, it was clear now that I had agreed to spend the rest of my life, which could be long indeed, living a lie. Regardless, something was bugging Jackie.

Though I couldn't let it go, I knew better than to be direct. She was as bad  as some men when it came to emotional talk. Then again, I probably wasn't much better. Once we had returned to her house and put the baggage aside to unpack later, I let Jackie press a gin and tonic into my hands. "Thanks for coming to meet me at the hospital."

She mixed one for herself, sipped it, and put the gin away. "No worries. We were in town, and planning to head back anyway. How's the arm?"

"Itches like hell." At least that meant it was healing. "I love that so many people wanted to sign my cast, but the sooner I can swap this for something that'll let me scratch the happier I'll be."

Before Jackie could say anything, little Claire burst into the kitchen and hugged Jackie with such force that the poor girl ended up wearing half her aunt's drink. "Aunt Jackie, I *love* my new room. It's so huge. Thank you! Can I show Aunt Nims?"

Her face screwed itself into a distasteful expression as she sniffed, and ran a hand through her damp auburn curls. "Bugger me with a rusty bazooka. Did you just spill your bloody drink on me?"

Chagrined, Jackie put aside her glass and reached for a towel. "Sorry."

"Claire, you *did* pounce on your aunt out of nowhere. You should give people warning."

"Sorry." She actually looked chastised for half a second before grabbing my hands. "C'mon, Aunt Nims. You gotta see my room. It's awesome."

Accepting the inevitable, I let Claire lead me from the kitchen as I looked over my shoulder at her aunt. "I'll be back in a tick, already? You can finish my drink if you want."

That set to Claire to giggling. "Ooh, an indirect kiss. Aren't you two a bit old for that?"

Rather than protest, I decided discretion was the better part of valor and kept my mouth shut as she led me upstairs. It wasn't until we had reached the attic that I said anything. "Claire, there's nothing here. It's just a big empty space."

"Yeah, but think of the possibilities!" She ran from one end of the attic to the other, opening all the windows and practically bouncing with delight. "Sure, the only furniture we'll be able to get up here will probably come from IKEA, but who gives a toss? I'm just a fuckin' kid; I don't need nice furniture that I'll pass down through the generations."

"That's a fair point." A cool breeze circulated through the attic with all the windows open, which felt delightful. "Lots of room for toys, too."

"And privacy, too. If I stake my claim now I'll have the whole floor to myself. If Aunt Jackie and Reverend Ronnie have kids like they want to, they'll have to make do with rooms downstairs. I'll be a teenager by the time they're old enough to be a pain in my arse, but since I can pull the ladder up and lock the entrance from inside, they won't be able to invade my privacy."

Damn, she's thinking ahead. Not that I was an expert in child development, but that kind of long-term thinking didn't seem common in children Claire's age. She was definitely going to be a handful for Jackie and her husband. "I'd love to see the place again once you've got it all set up."

"Cool." She closed each of the windows, though she had to stand on tiptoe to do so. What kind of life had she had with her mother that drove her to become so independent so soon?

Jacqueline was still in the kitchen when I returned, putting together a beef roast to go in the crock pot. "So, is Claire really set on having the attic?"

"Yeah. She figures it'll keep her cousins out from underfoot when she's older. Is it me, or is Claire unusually precocious?"

Jackie shook her head. "It's not you. She's fine now, but when she was younger she had gelastic epilepsy. She had to have a tumor removed."

"Poor kid." Checking the network, I looked up the condition she mentioned. "She isn't going through puberty already, is she?"

"Christ, I hope not. She's still a bit young." She chuckled as she set the slow cooker's timer. "Though she's looking forward to it. She's in a hurry to grow up. Damned if I know why, though."

And here was my cue to return to the conversation I meant to have before Claire had burst in. "Has something been bothering you? You were quiet the whole ride back."

She shrugged. "I submitted my resignation. You're going to need a new partner once you're back on the job."

What the hell? I thought Jackie loved being an Adversary. "Wasn't being an Adversary your dream as a kid? You told me it was as close as you could get to being a hero."

"Yeah, but I've put in a couple of years and haven't done much good. Sure, we've saved some lives and ended some abuses, but it's like being a janitor. As soon as you clean a mess, somebody makes a new one. This shit never ends. You know what I mean?"

Yeah, I did. Hearing that Jacqueline wanted out made it easier for me to open up. I gave her a one-armed hug. "I know exactly how you feel. I'm going to tender my resignation as well. Being an Adversary was never *my* dream, but I'll never make my dream real if I don't put away my sword. I don't blame you for wanting out."

"Bloody hell, that went better than expected." Now, *that* was the Jacqueline I recognized. "You want to stay for dinner tonight? We gotta talk wedding plans, and I was kinda hoping you'd be my maid of honor."

---

### This Week's Theme Song

"Out of the Darkness" by the Blue Öyster Cult, from *Curse of the Hidden Mirror*

{% youtube c3_7Zd9SJOU %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
