---

title: An Overdue Update
modified:
categories: starbreaker
excerpt: "I should have provided this update on Silent Clarion a coupole of weeks ago. Better late than never."
tags: [silent-clarion, draft, serial]
cover: silent-clarion-new-banner.jpg
date: 2015-09-13T21:03:05-04:00
---
I’ve been busy lately, but it’s time for a bit of quiet before going to bed so I can get up and go back to work tomorrow morning. Now seems like a good time to share some of what’s been going on in my life.

I didn’t make a big deal about it, and I don’t know why, but I completed my draft of *Silent Clarion* on 30 August 2015. Sixty-five chapters all from Naomi Bradleigh’s viewpoint, plus a short epilogue.

The next installment of the Kindle serial — *Silent Clarion, Episode 3: “Life is Short and Love is Over”* — is ready to hit your Kindles, and the remaining episodes will swiftly follow. Here are the titles, if you’re interested:

* *Silent Clarion*, Episode 4: "Death in a Northern Town"
* *Silent Clarion*, Episode 5: “Hard Places and Other Rocks”
* *Silent Clarion*, Episode 6: “Rainchecks for Ragnarok”

You may have noticed that instead of chapter titles, each chapter has a track number and a song. There’s a reason for that; the idea is to tie each chapter to a song that suits that chapter.

Here’s a Spotify playlist to go with the story. Three tracks are missing, unfortunately, because they’re not licensed for Spotify streaming. Oh well.

<iframe src="https://embed.spotify.com/?uri=spotify%3Auser%3A1238897208%3Aplaylist%3A4iBppi9lW4iw5oPUa9JuEv" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

This isn't related to *Silent Clarion*, but if you right-click the image below and pick "Save As...", you can have yourself a nice bit of widescreen wallpaper for your computer.

![Phoenix Society Seal on Red Background](/images/starbreaker-red.png)

Hope you like red.