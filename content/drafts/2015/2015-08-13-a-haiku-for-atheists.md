---
id: 2446
title: A Haiku for Atheists
date: 2015-08-13T06:13:50+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=20
permalink: /2015/08/a-haiku-for-atheists/
sw_cache_timestamp:
  - 401701
bitly_link:
  - http://bit.ly/1N0ku3p
sw_twitter_username:
  - MGraybosch
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Personal
tags:
  - anti-Christian
  - atheism
  - Caspar David Friedrich
  - Fuck God
  - Fuck the Church
  - Fuck the Devil
  - haiku
  - heavy-metal
  - Lemmy
  - personal responsibility
  - Wanderer Above the Sea of Fog
format: image
---
Here's a little haiku for atheists with a Christian background. I'd say this is just a little something to stir the pot, but I _mean_ it. Not gonna apologize for it to anyone. Not even God if it turns out I'm wrong.

**No kingdom will come**
  
**Let _my_ will be done on Earth**
  
**To Hell with Heaven!**

The image above is cropped from a hi-res scan of [Caspar David Friedrich's _Wanderer Above the Sea of Fog_](https://en.wikipedia.org/wiki/Wanderer_above_the_Sea_of_Fog). Naturally, the artwork is public domain.

Also, have some Lemmy. Why? Because fuck you is why.<figure id="attachment_3043" style="width: 736px" class="wp-caption aligncenter">

<a href="http://www.matthewgraybosch.com/2015/08/a-haiku-for-atheists/lemmy-fuck-god/" rel="attachment wp-att-3043"><img src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/08/lemmy-fuck-god.jpg?fit=736%2C552" alt="Lemmy: &quot;Fuck God...&quot;" class="size-full wp-image-3043" srcset="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/08/lemmy-fuck-god.jpg?w=736 736w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/08/lemmy-fuck-god.jpg?resize=300%2C225 300w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/08/lemmy-fuck-god.jpg?resize=610%2C458 610w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px" data-recalc-dims="1" /></a><figcaption class="wp-caption-text">Lemmy: "Fuck God&#8230;"</figcaption></figure>