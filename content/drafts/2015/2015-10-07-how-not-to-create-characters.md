---
id: 1771
title: How NOT to Create Characters
date: 2015-10-07T23:35:28+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1771
permalink: /2015/10/how-not-to-create-characters/
nc_postLocation:
  - default
twitterID:
  - MGraybosch
snap_MYURL:
  - 
snapEdIT:
  - 1
snapFB:
  - 
snapPN:
  - 's:229:"a:1:{i:0;a:8:{s:9:"timeToRun";s:0:"";s:9:"apPNBoard";s:18:"224054218900737052";s:10:"SNAPformat";s:15:"%TITLE% - %URL%";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doPN";i:0;}}";'
snapRD:
  - 
bitly_link_twitter:
  - http://bit.ly/1On9hgH
snap_isAutoPosted:
  - 1
snapTW:
  - 's:295:"a:1:{i:0;a:10:{s:4:"doTW";s:1:"1";s:9:"timeToRun";s:0:"";s:10:"SNAPformat";s:15:"%TITLE% - %URL%";s:8:"attchImg";s:1:"1";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:18:"651964251293265920";s:5:"pDate";s:19:"2015-10-08 03:35:50";}}";'
snapGP:
  - 
bitly_link_facebook:
  - http://bit.ly/1On9hgH
bitly_link_linkedIn:
  - http://bit.ly/1On9hgH
bitly_link:
  - http://bit.ly/1On9hgH
sw_cache_timestamp:
  - 401660
bitly_link_tumblr:
  - http://bit.ly/1On9hgH
bitly_link_reddit:
  - http://bit.ly/1On9hgH
bitly_link_stumbleupon:
  - http://bit.ly/1On9hgH
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - How Not to Write
tags:
  - 2001
  - "blofeld's cat"
  - blue oyster cult
  - characters
  - creation
  - daemon
  - "don't-try-this-at-home"
  - george carlin
  - ghaleon
  - iced earth
  - imaginos
  - lunar
  - process
  - set abominae
  - something wicked this way comes
---
If you're wondering why I'm blogging like an honest-to-Cthulhu _writer_ about how I go about creating characters, [blame this Reddit thread](https://www.reddit.com/r/Fantasy/comments/3nw3sg/writers_of_rfantasy_how_do_you_create_your/). My intended answer is going to get long, so I figured, "why not make it a blog post first?" So, here's a guide to how _not_ to create characters. Don't try this at home, kids; I'm untrained and unprofessional.

Most writers have a detailed process for character creation that they could explain over the course of a semester to an audience of college students merrily racking up student loan debt. Many will insist on using real people as inspiration, or historical figures, or even Jungian archetypes.

## The First Steps

My process is simpler: I take inspiration wherever I can find it, look for interesting implications, and ask myself a metric shitload of questions.

For example, the inspiration for one of the major villains in my **Starbreaker** saga came from a 1988 Blue Öyster Cult song called ["I Am The One You Warned Me Of"](https://en.wikipedia.org/wiki/Imaginos).

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

In particular, this refrain caught my imagination:

> I am the one you warned me of
    
> I am the one who'd never, never lie 

For some reason, it cross-referenced with Nietzsche:

> He who fights with monsters might take care lest he thereby become a monster. And when you gaze long into an abyss the abyss also gazes into you. 

So I had a figure who was unable or perhaps _unwilling_ to pretend to be anything but what he had become &#8212; the monster he set out to fight. However he was just a figure, no more than a silhouette. It wasn't enough. I wanted to do better.

## The Golbez Standard

Yeah, you heard me. _That_ Golbez. Remember him?

A brief aside for those of you who haven't played _Final Fantasy IV_: Golbez is the primary antagonist for most of the game's campaign. He's first mentioned when the player guides Cecil Harvey and his companions to the bombed out castle of Damcyan, and identified as the new commander of the Red Wings, the air force of Baron. However, we learn next to nothing about him until we're more than 80% of the way through the game, when we learn that he's Cecil's brother, and was being manipulated by a being named Zemus.<figure id="attachment_2944" style="width: 660px" class="wp-caption aligncenter">

<a href="http://www.matthewgraybosch.com/2015/10/how-not-to-create-characters/golbez-ffiv/" rel="attachment wp-att-2944"><img src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/Golbez-FFIV.jpg?fit=660%2C1000" alt="Golbez from Final Fantasy IV. Art by Yoshitaka Amano" class="size-full wp-image-2944" srcset="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/Golbez-FFIV.jpg?w=660 660w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/Golbez-FFIV.jpg?resize=198%2C300 198w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/Golbez-FFIV.jpg?resize=610%2C924 610w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px" data-recalc-dims="1" /></a><figcaption class="wp-caption-text">Golbez from Final Fantasy IV. Art by Yoshitaka Amano</figcaption></figure> 

One of my biggest gripes with the fantasy fiction I had read prior to saying "fuck it" and trying to write my own epic fantasy was that the villains were less plausible than the sort of antagonists you might find in SNES-era Japanese RPGs. I've read books whose antagonists make Golbez from look well-written.

That's all we find out about Golbez in _Final Fantasy IV_, aside from that he's a manipulative bastard who's happy to place the heroes into situations where they've no choice but to further his aims. That's because _Final Fantasy IV_ is a video game. It might have been fairly deep when I was twelve years old and just starting to really dig into the stories I consumed, but its depth is still quite shallow by print standards.

Fantasy fiction should aim higher. It should offer deeper characterization than a game Squarest developed in 1991. It was one thing for Tolkien to use Sauron as his primary antagonist in _The Lord of the Rings_ back in 1954, even though Sauron had no dialogue and was never "on stage". Sauron wasn't a human being, and because of events occurring prior to _The Lord of the Rings_ he was a mere shadow of his former self.

However, when you read a novel like Terry Goodkind's 1994 debut _Wizard's First Rule_ and find that its _human_ protagonist, Darken Rahl, is willing to risk destroying the world if he can't rule it but seems to have no plans for what he'll do with the world _after_ he's taken over, it's a monstrous disappointment. A human antagonist shouldn't be evil _just because_. It just doesn't make sense.

## Something Wicked?

So, as I mentioned earlier, I already had a concept for my villain: he was a man who became what he set out to fight, he knew it, and he wasn't going to lie to himself or anybody else about it. I had already decided to take the Imaginos concept and run with it; the notion of an actor in history empowered by an unearthly cabal was too intriguing to resist.

However, I needed to take the concept and make it my own. The next step came courtesy of Iced Earth, whose 1998 album _Something Wicked This Way Comes_ ended with a trilogy of linked songs telling a tale of the Antichrist's advent: "Prophecy", "Birth of the Wicked", and "The Coming Curse.



These songs told the tale of humanity's near-genocide against a wise and peaceful precursor race and the Setian leaders' savage revenge on humanity at the hands of Set Abominae, a vengeance millennia in the making.

## Never Trust a Long-Haired Bishounen

One small problem. Sure, I had _plenty_ of imagery and concepts to play with between my reading, the heavy metal music I've listened to my entire life, and the movies and video games I played. But I was still no closer to actually creating a villain capable of carrying a novel and sustaining its primary conflict.

I need to attack the problem from another angle. Now, I wouldn't normally mention this, but I'm part of a small minority of J. R. R. Tolkien's readers who actually bothered to read _The Silmarillion_. So I know that Sauron was far more than just a malevolent force of nature with an unfortunate habit of dropping his jewelry. I knew that Sauron had once been beautiful and persuasive.

So here I am cross-referencing again, this time to the ["Ghaleon Rule" from the Grand List of Console RPG Cliches (Rule 189)](http://project-apollo.net/text/rpg.html). This rule, named for the [surprisingly idealistic antagonist](http://www.rpgamer.com/editor/2000/q2/050700hk.html) of _Lunar: Silver Star Story_, runs as follows: **Every problem in the universe can be solved by finding the right long-haired prettyboy and beating the crap out of him.**

## The Big Questions

With the above in mind, I had my first Big Question&#8230;

> What if the Dark Lord wore white, already ruled the world, and was trying to _save_ it? 

The first issue is that mere _notion_ of saving the world goes a step or three beyond mere hubris &#8212; as the late George Carlin explains&#8230;

> "The planet is fine. The _people_ are fucked." 

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Besides, every schmuck and their cat seems to be writing an epic fantasy where the existence of the very world is at stake. So, let's assume that the universe will indeed be fine if life on earth goes bye-bye. Here comes the next Big Question&#8230;

> If the people are fucked, and it _isn't_ their own damn fault, what's the threat? 

I can't have my villain be the threat, or at least he can't be the _only_ threat. Remember, he became the thing he hated, like in that Stabbing Westward song. Fortunately, I had an answer thanks to the popularity of _Left Behind_ at the time. God was the enemy.



Of course, bringing God into my story meant bringing in all kinds of baggage. For example: how is humanity supposed to fight God if God is everything Jews, Christians, and Muslims say he is? This leads to the next question&#8230;

> What if my villain set out to fight a daemon _pretending_ to be God? 

Now we're getting somewhere. To the ancient Greeks, [daemons could be either benevolent or malevolent](https://en.wikipedia.org/wiki/Daemon_(classical_mythology)). Agathodaemons and cacodaemons were more than human, but they weren't gods. Furthermore, the fact that daemons are spirits of nature gives me some wiggle room for interpretations.

> Suppose my daemons were once mortal? Suppose they could be transformed. 

Like Dave Bowman in the last part of _2001: A Space Odyssey_, suppose a mortal intelligence could be shepherded to a post-biological state by those who had already made the journey.

<img src="http://i1.wp.com/i.ytimg.com/vi/yazu8vaGPwQ/maxresdefault.jpg?w=840" alt="" data-recalc-dims="1" />

Suppose there were daemons who believed in a kind of Manifest Destiny where all intelligence was post-biological, and any intelligent species that refused to make the transition had to be exterminated for the greater good. Suppose this faction was opposed by another that was content to live and let live? Suppose it came to all-out war that raged until the latter faction created a doomsday weapon capable of killing daemons?

I finally had my main conflict. Imaginos became a daemon to fight Sabbath, who was determined to exterminate Imaginos' people by conning humanity into doing the genocidal dirty work. But that raised another question:

> Why can't Imaginos just take the doomsday weapon and stick it up Sabaoth's butthole? 

Also, how does this doomsday weapon work in the first place? I could keep going all night, but I think I've illustrated my point: by working through successive questions and considering the implications of my answers to each, I was able to build my characters, story, and setting.

Only one question remained. What if it wasn't Blofeld running the show, but his cat?

<img src="http://i0.wp.com/prettycleverfilms.com/files/2013/05/4_blofelds_cat_many.jpg?w=840" alt="" data-recalc-dims="1" />

That's how I ended up with a science fantasy series for metalheads involving cat people putting out fires with gasoline. My villain doesn't need a white cat, because he is one.<figure id="attachment_2942" style="width: 662px" class="wp-caption aligncenter">

<a href="http://www.matthewgraybosch.com/2015/10/how-not-to-create-characters/imaginos_harveybunda/" rel="attachment wp-att-2942"><img src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/imaginos_harveybunda-662x1024.jpg?fit=662%2C1024" alt="Imaginos from Starbreaker. Artwork by Harvey Bunda (2012)" class="size-large wp-image-2942" srcset="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/imaginos_harveybunda.jpg?resize=662%2C1024 662w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/imaginos_harveybunda.jpg?resize=194%2C300 194w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/imaginos_harveybunda.jpg?resize=768%2C1187 768w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/imaginos_harveybunda.jpg?resize=610%2C943 610w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/imaginos_harveybunda.jpg?w=828 828w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px" data-recalc-dims="1" /></a><figcaption class="wp-caption-text">Imaginos from Starbreaker. Artwork by Harvey Bunda (2012)</figcaption></figure> 

If you enjoyed this post, please consider buying a copy of [_Without Bloodshed_](http://www.amazon.com/Without-Bloodshed-Starbreaker-Series-Book-ebook/dp/B00GQ0BJOO) or [_Silent Clarion_](http://www.amazon.com/Silent-Clarion-Geographic-Matthew-Graybosch-ebook/dp/B00YLZI02U/). I'd appreciate the support.