---
title: "*Silent Clarion*, Track 38: &quot;Halo of Flies&quot; by Alice Cooper"
excerpt: "Check out chapter 38 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
My eyes watered as I opened the front door to Ernest Yoder's apartment. A sudden terror sank its talons into my mind, and rationality cowered in a corner left intact as I imagined the overwhelming stench forcing its way into me. The fetid air hung thick and still, a miasma of desperation that invaded my body through every possible entrance, permeating every cell, irreversibly tainting me—as if I would never be clean again.

My stomach clenched, threatening rebellion. It took all of my self-discipline to resist running back outside to wait for Robinson. Instead, I stumbled to the south-facing bay window, my gloved hands fumbling at the latch before I succeeded in yanking it open.. I pressed my face to the screen and drank deep of the fresh air, hoping that any resulting contamination of the crime scene would prove minimal.

Breathed through a handkerchief, the smell reminded me of the forensics lectures I attended at ACS, particularly those that covered estimating times of death. Behind this bedroom door, I would most likely find a week-old corpse, the abdomen bloated from the pressure of gases building up as intestinal flora began feeding on the victim instead of his food. Taking a breath, I opened the door, and immediately slammed it shut again before doubling over and gagging.

The putrefaction of Edward Yoder's corpse was further advanced than I had estimated. Provided favorable conditions by the heat and humidity within the enclosed bedroom, the bacteria within his body and any opportunistic little beasties in the vicinity had taken advantage of his slight obesity and flourished. His belly, strained beyond capacity, began to split open before my eyes.

The rupture reminded me of a slow-motion video of a balloon bursting when struck by a pin. Yoder's skin split open. Liquefied flesh spattered with the force of the explosion, and the release of gases pent within added a new layer of charnel stink to what had previously permeated the flat.

Unable to rein in my gorge any longer, I fled to the bathroom. Once inside, I collapsed before the porcelain idol and offered up my breakfast. Bless me toilet, for I have sinned.

Lightheaded and desperate to avoid another assault of nausea, I took a tentative breath. Despite a stomach now as empty as the depths of space, I heaved until my throat was on fire and stars danced across the back of my eyelids. Though I was desperate to get out, my legs lacked the strength to carry me; I had vomited it all up.

Strong arms lifted me to my feet and guided me out into the hallway, where somebody had opened windows and set up fans to draw in more fresh air. Opening my eyes, I found Robinson holding me upright and offering a sweating bottle of ginger ale. "Here. Drink this."

Straightening, I tried a cautious sip. "Thanks, Sheriff. Did Mr. Wesker call you?"

"Your buddy Malkuth did. Said something about you getting a live horror show." He glanced at the master bedroom. People in hazmat suits waited outside, ready to open the door, and he led me to an open window before signaling his deputies. They opened the door, filed inside, and got busy. I took another gulp of fresh air. Clearly, they were made of sterner stuff than I was. "I guess we know what happened to Earnest Yoder."

"No, we don't." With an effort, I forced myself to consider the burst-open remains. "I haven't examined the body yet."

"Nor should you. A young woman your age shouldn't have to-"

Sheriff Robinson was trying to protect me, and right now, I wanted to let him. Except regardless of his reasons or my desires, I dared not let him insulate me from the reality in that bedroom. If I settled for secondhand information, I would reach faulty conclusions. Besides, I had already seen him burst apart from internal pressure. My day couldn't *possibly* get worse, could it? "I shouldn't have to *what*, Sheriff? Do my bloody job? Yoder could have been murdered in the same manner as Wilson and Foster, and for the same unknown reason. Kaylee told me they were friends. Them and Michael Brubaker, which means *he* might be in danger."

"Shit."

"Shit, indeed." But, why? Had he seen something he shouldn't have? What did these young men know that was worth killing them to keep it secret? "Sheriff, I need you to find Brubaker and take him into protective custody. I think he's a witness."

Naturally enough, Robinson was incredulous. "You think he saw Yoder killed?"

"I wish it was that simple." If I was to convince him, I had to tie Brubaker to the evidence, however tenuous the connection. My intuition wouldn't be enough. "We know Wilson and Foster were murdered in the same manner, and I think we'll find that's the case with Yoder. They all knew each other. Brubaker knew them. If they're being targeted by Dusk Patrol as the bite marks on their bodies suggest, then this is connected with Fort Clarion. I think Brubaker knows why this is happening."

"But Yoder was killed here, wasn't he?"

"Not if the pattern holds true. We didn't find the previous victims where they had actually been killed, or there would been more of their blood at the scene. I think he was likely killed elsewhere, and dumped back in his apartment."

Robinson stared at me for a long moment, as if my conjecture was utterly insane. "We don't even know how Yoder was killed, yet."

Then why are we pissing about out here? Let's pull our fingers out of our arses and check out the dead guy. Not that I said anything of the sort. It wouldn't be politic. "Then tell the deputies to let me in. I know what to look for, and I'd love to be wrong. Because if I'm not, it means that these killings are unrelated to my presence in Clarion."

"All right, but wait here a minute." When he returned, it was with a hazmat suit sized to fit somebody my height. The symbol on the shoulder indicated it was suitable for use against biohazards, but would offer no protection against radiation or chemicals. "Let me help you into this, Adversary. With a separate O2 supply, the smell won't get to you."

"Thanks. Is there one for you?" For Robinson's sake, I hoped so. I doubt he was any fonder of secondhand information than I was, and he deserved to be able to see the body for himself.

He shook his head. "This was supposed to be mine."

I engaged the air supply and pressurized the suit. It was barely worth being called a hazmat suit, and I doubted it would protect me against to the common cold, and even that was doubtful. Regardless of my misgivings, it shielded me from the smell that had driven me to my knees in Yoder's bathroom, which was all I needed.

Giving Robinson a thumbs-up to show I was ready, I steeled my nerves and entered Ernest Yoder's bedroom. The ragged edges of his exploded abdomen were still a repellent sight, especially since it had burst open with sufficient force to spray organic matter in a respectably wide radius around him.

Somebody patched me into a secure relay chat, most likely Crosby. I immediately wished she hadn't, as a deputy named O'Leary pointed at Yoder's gut. ｢I guess he ordered the extra spicy meal.｣

Shaking my head, I refrained from responding, but that didn't discourage him. Fortunately, Crosby had no such scruples. ｢Cram it, kid. Let's just do the job and get the hell out of here.｣

｢Sorry, boss, but you gotta wonder. You think Yoder died a virgin?｣

｢You're gonna die a virgin real fuckin' soon if you don't plug that asshole you keep mistaking for a mouth and finish photographing the goddamned scene. Pardon my French, Adversary.｣

｢Don't worry about it, Crosby.｣ If my ears somehow remained virginal despite growing up with two older brothers, a few days on the job with Jacqueline for a partner would have fixed that in short order. ｢Let's see what happened to this poor sad bastard.｣

The suit was a godsend, since decomposition was sufficiently advanced that flyblown flesh sloughed off the bone when I touched it. Talk about nasty. At least the flies couldn't bother me in here.

Despite this complication, my suspicions were soon confirmed. Yoder had suffered a concussion, most likely from a blunt arrow; multiple bites from individuals with CPMD; and a stab wound between groin and thigh that finished the kill. But the lack of blood resulting from such a wound suggested he had been killed elsewhere and placed here. ｢Sometimes I hate being right.｣

Robinson caught that. ｢Same method as the others, Adversary?｣

｢Unfortunately. You know what that means, don't you?｣

｢Means our jobs just got more complicated. You done in there?｣

｢I wish. This doesn't make sense. Yoder isn't a small man, and he'd be dead weight, so I find it difficult to believe that not only did a single man bring him back home, but managed to do it without drawing attention to themselves.｣

｢I was thinking the same thing, and checked with Wesker. He's got a security camera over the front door.｣

Of course, he does.｢Any useful footage?｣

｢No such luck.｣

｢Then I've got more work to do.｣Closing my eyes, I counted down from a hundred. When I opened them again, I hoped to see something I had missed earlier. Though I'd settle for some of what I had seen before not being there when I opened my eyes again. Like that body.

Yoder's body was still on the bed when I gave the place another onceover. There was something else, though. When he burst, the contents of his body spread across a certain distance that did not extend all the way to the window. Yet there seemed to be the tiniest, almost imperceptible drop of blood right by the wall. Pushing aside the curtains, I found a little more blood smeared on the wall. Scraped paint and splinters around the latch suggested that somebody had used a knife to undo it from outside.

My answer was just outside. ｢They got in using the fire escape. ｣

｢All right. Let us take it from here. You want to wait at the station?｣

｢Have a deputy bring all of Yoder's computer equipment. I can get started with that while you continue investigating this scene.｣ With Yoder dead, I had probable cause to get into his computer and go poking through his data. Hopefully, his preference for online interaction will lead me to where Clarion's youth hangs out on the network, and what connected these young men to Fort Clarion. Otherwise, I will have to resort to other means of persuasion.

---

### This Week's Theme Song

"Halo of Flies" by Alice Cooper, from *Killer*

{% youtube fGOD1-NtxH8 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
