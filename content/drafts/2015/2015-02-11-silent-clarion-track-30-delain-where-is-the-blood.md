---
title: "*Silent Clarion*, Track 30: &quot;Where is the Blood&quot; by Delain"
excerpt: "Check out chapter 30 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
"Somebody's trying to frame you." Robinson repeated my words as he considered the corpse of Scott Wilson, one of the young irregulars who helped me inventory the ordnance stored at Fort Clarion. What the hell has he been doing out here? Had somebody been plotting against him, or was this a crime of opportunity?

At least two people had stripped him bare and fed off his blood, scoring his flesh with their canines before finishing the kill with the thrust of a knife, and I could venture a solid guess as to who had done it and why. The hard part would be convincing Robinson.

He gave Wilson's body another look. Despite his disgust, he studied the wounds with exacting care. "You have CPMD, Adversary, and you could have inflicted some of these bite wounds. If not for your alibi, I would suspect that you were trying to frame somebody. A lesser cop might accuse you of using these murders to drum up support for your investigation, but you're too confrontational to work in so roundabout a fashion."

He was right about that. I can be a stone cold bitch when given cause, but I never saw the sense in using other people when I've got a perfectly good sword with which to get my point across. "I'm not offended, Sheriff, but what makes you so sure of me?"

Reaching over the gurney, Robinson tapped one of my lapel pins. "You take too much pride in wearing these. You came to Clarion alone, and none of the acquaintances you've made in town have CPMD. Like I said, your alibi is armor-plated and it's not your style."

"You'd think I had put a sword to your throat that night, instead of simply telling you to show me a warrant or bugger off."

"I think that would have been your next step." Leaning against the wall, Robinson crossed his arms over his chest. "So give me a better theory. Who would want to frame you? Do you have something on Petersen or Collins that would give them motive to discredit you?"

"Aside from Petersen's involvement in Project Harker, and his possible clandestine involvement with the Dusk Patrol survivors? And let's not forget Collins' pathetically clumsy effort to pin Wilson's murder on me? Don't you think that's quite enough?"

Robinson shook his head. "What about means?"

"Petersen's an old man, Collins is late middle-age, and neither have CPMD." Let's give the devil his due, shall we? Though I will eventually nail Petersen, it won't be for this. "If I'm right about his involvement with the inhabitants of Fort Clarion, he might have gotten some of them to stage the attack, but he would have been smart enough to ensure that the only bite marks on the body were those that might tie me to the crime."

"You telling me the people living under Fort Clarion did this themselves? Why?"

A horrible suspicion dawned on me as I shared my hypothesis with Robinson. "Revenge. One of them threatened to ensure I regretted not letting them kill me quick and easy."

Since Wilson's body could tell me nothing more, I zipped it back into its bag. "I need to see the crime scene, and I need to meet with the person who discovered the body."

He nodded, and pressed his fingertips to his ear for a moment. "I'll take you. The Brubaker kid is still there, giving his statement."

Brubaker? What the hell was he doing out here? Had he been following me? If so, why? And how did we not meet? It wasn't like I dared run the rest of the way to Clarion after I got stabbed.

Though Robinson didn't reveal the location, I suspected I knew where he'd be taking me. And I was right. It was the clearing where I had fought those soldiers, the clearing to which I meant to bring Robinson anyway. Somebody had gotten there before the police, for the bodies were missing.

Brubaker was gone when we arrived, but the Sheriff's deputies remained. One waved an evidence baggie containing the bayonet I had kicked into the brush. "Hey, Sheriff! I think I found the murder weapon."

"You wish." I stepped forward, unwilling to be cowed. "The blood on that knife is mine, which you'll discover for yourself upon analysis."

"Did everybody hear Adversary Bradleigh? This clearing got a bit of traffic last night." Robinson raked his gaze across the area. "Colby! How many blood samples did you take?"

A deputy with a portable forensics lab strapped to her back looked up. "I sampled every bloody spot I could find, Sheriff. I found samples from at least three individuals with CPMD, based on blood type, in addition to our victim's blood. They were at least an hour older."

"Where's the rest of the blood?" The question hit me out of the blue. "There should be a hell of a lot more, given the manner in which Wilson died."

"Aw, shit. There isn't enough to tie Wilson's murder to this location. Somebody must have dumped him here." Colby started looking around, zeroing in on a small mound. "Sheriff, that patch over there has been bugging me the whole time we were here. Did anybody think to bring a shovel?"

Robinson narrowed his eyes, and crouched by the mound. He got a good grip on a patch of grass, and tried lifting it. It came free, leaving the Sheriff with a rectangle of fresh-cut sod in his hand. "Son of a bitch."

He began digging with his hands, ripping cut squares of sod loose and putting them aside. Against my better judgment, I pitched in. The other deputies joined us, including Colby, who shrugged off her forensic backpack with a sigh of relief. Together, we lifted out enough earth to create a hole a quarter meter deep in the rich, loamy ground before somebody thought to fetch a spade.

Heedless of what I was doing to my clothes, I lowered myself into the hole and set about digging out the rest of the fresh grave. My spade bumped against something slim and roughly cylindrical seated vertically in the soil. Digging around it, I exposed the handle of a knife, and then the face of the man I had killed with it.

Whoever buried my enemy had not bothered to pull the bayonet from his skull, or had lacked the strength to do so. It didn't take long for me to expose the rest of the body. Straightening, I pointed at its occupant. "That was the man I killed last night."

Robinson jumped into the grave with me and helped me clear the last of the earth. We lifted the dead soldier out so the other deputies could tag and bag him. One of them tried pulling the knife from his head before he finished zipping up the body bag, but it wouldn't budge. "Hey, Adversary, what did this guy do to piss you off so bad?"

Aside from ruining my favorite leather jacket, and threatening to do God-knows-what to me? "He underestimated me. Two knives weren't enough to beat my sword."

The deputy tried shifting the knife again. "You sure he wasn't your asshole ex or something?"

"Quite." My urge to give a flippant answer showed only in my smile. As amusing as it might be to do so, I'd probably pay a dire price for saying that I wasn't nearly as merciful with my asshole ex. Sheriffs don't select deputies for their sense of humor, and my implication that I had condemned him to live without me would most likely escape this crowd.

Once I had finished helping out, I ended up back at the police station. At least I wasn't under arrest or being detained this time around. It wasn't likely that Robinson would have brought me into his office if either were the case. He pulled a half-full bottle of bourbon and two waxed-paper cups from his desk drawer, poured three fingers into each, and handed me one. "I usually like a drink after seeing death up close. How about you?"

"It's not my normal habit, but I'll join you." I raised my cup to salute Robinson, and sipped it. The whiskey was a smooth burn down my throat that flared in my belly. "You realize that anything I tell you now will be inadmissible as evidence."

Robinson shook his head, and gave me a wry look as he put the bottle away. "It's just a drink, Adversary. I ruled you out as a suspect hours ago."

Though I had suspected as much, it was gratifying to get confirmation in as many words. "Good. I wouldn't have come here to investigate my own crimes."

"Touché." Robinson sipped his whiskey and stared out the window for a long moment. "Did you come here knowing somebody might die?"

"Of course not!"

"You sound angry about it. Why? It's not like you knew the kid."

Now I was angry, but at least I had a deserving target handy. "Damn right I am, Sheriff. Now, what are we going to do to prevent another death like his?"

Robinson finished his whiskey before answering. "We finish the job at Fort Clarion. We tear that godforsaken place out of the ground, round up every remaining member of Dusk Patrol, and get them hospitalized. If we can identify the ones who killed Scott, we put the fuckers on trial."

"I'll drink to that." And I did just that, slamming back the last of my bourbon, crushing my cup, and tossing it into the bin across the room with a flick of my wrist.

---

### This Week's Theme Song

"Where is the Blood" by Delain

{% youtube oF-YR1I2SQ4 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
