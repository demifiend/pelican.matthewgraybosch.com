---
title: "*Silent Clarion*, Track 45: &quot;You Can't Always Get What You Want&quot; by The Rolling Stones"
excerpt: "Check out chapter 45 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Tempted as I was to offer to hold Renfield's hand, I refrained from doing so lest he think I was mocking his experiences. Besides, I didn't have time to comfort him, nor was I sure we had the sort of relationship that would let me do so. Instead, I turned to Brubaker. "Mike, can you keep watch outside with Christopher? He might appreciate the company."

His snarl put the lie to his words. "I'm fine."

"Look, if you want to wait outside by yourself, that's fine. However, I can use you beside me. It will take me longer to figure this place out on my own."

Renfield remained pale, but his hands steadied as he took a deep breath and mastered himself. "I'll stay behind you as long as I can, but I won't go in there first."

Unwilling to make a big deal of the situation, I shrugged and reached inside the doorway to feel around for light switches. I soon found them, and flipped them all at once, banishing the gloom with the cold white glow of flickering florescent lights.

Based on Renfield's initial refusal to cross the threshold, I half-expected a pseudoscientific torture chamber with blood smeared on floors and walls, rusty, crude medical instruments encrusted with old gore, and machinery whose function I dared not guess for my sanity's sake. Instead, the lab was clean aside from a bit of dust covering examination tables and workstations. Computers quietly hummed, their screens prompting me for passwords I did not possess and lacked the time to crack.

Halfway across the lab on the left was a door marked "Isolation Cells". Opposite to it, the pharmacy door stood slightly ajar. At the back loomed a door marked "Records/Computer". Hopefully it was the one to lead me to the answers, because I doubted I would find them in the isolation cells or the pharmacy.

"At first, I'd just wake up down here after laying down in my rack for the night." Renfield's voice was small and quiet. "That wasn't so bad, though nobody would ever tell me why they kept taking my blood, or what the hell they were injecting into me. It got worse when I started waking up in one of those goddamn isolation cells."

Though beginning to shudder, Renfield threw open the door to the isolation cells. He reached inside and turned on the lights. "And once they were sure they had made me into something new, they started cutting on me to see how my body reacted. This whole section's soundproofed so nobody in the rest of the lab could hear me scream."

It stood to reason that Project Harker involved the vivisection of test subjects after they had been altered, presumably to test for enhanced healing and regenerative capabilities. However, I felt as though I should be shocked. I should be outraged, because this right here is why Adversaries like me exist. Violations of this nature were what I swore to oppose. "What about your men?"

"I was the first."

"It can't have been that bad." Brubaker still waited by the entryway, rifle at the ready. Though he had muttered the words, they sliced through the silent room and bit deep into Renfield. "Since you let them do it to the others."

Glaring at Brubaker for a moment, I then turned my back on him to focus on the man behind me. "Are you all right?"

Renfield shrugged, and let me lead him to the pharmacy before speaking again. "He's right. I should have spoken out, and I would have. I think the bastards knew it, because I spent a month under sedation in one of the isolation cells while they performed the Renfield Protocol on the others."

"They named the process after you?" Talk about profoundly fucked up. They secured Renfield's continued cooperation and ensured he remained dedicated to his men in one simple move. How could he leave them when he felt responsible for their situation. The threat of an information leak all the brass needed to undermine him. There's no way they'd follow Renfield if they thought he had sold them out, is there? "And how is it you took this better than the men?"

"Yeah. Said it was some kind of tribute to my having the balls to be the first volunteer." Renfield sighed as he opened the door to the pharmacy so I could examine its contents. "You need to understand, Naomi. We all volunteered. We thought it would make us better soldiers, so we were happy to do it. We just didn't know what it would involve."

Leaning close to me so that only might hear him, he added, "And I'm not handling it well at all. I just tell myself that my men need me more than I need to deal with my feelings."

I had no words that would ease Renfield's pain, and the memory of how he had seemingly betrayed me to save me from his men still burned too hot for me to willingly offer the touch that might have substituted for words. Instead, I considered the drugs before me. Of the chemicals I recognized, most seemed to be antipsychotics. Did the so-called Renfield Process induce psychosis in its subjects? "Renfield, how many of the volunteers were fit for duty after the process was completed? What was the casualty rate?"

"They never gave us exact figures, or told us what happened to them afterward, but at least a third of us didn't make it. As far as I know, they never even got decent funerals."

"Would you like to find out what happened to them?"

Renfield's eyes narrowed as he stared at me. "You think you can get that information?"

I didn't want to promise anything, but it was likely the Project Harker scientists would have kept records of who suffered such adverse actions to the Renfield Process that they either died, or had to be put down. No doubt the researchers would have justified these murders by invoking the need to maintain secrecy. "I'm willing to try, Sergeant. Let's have a look at the records, shall we?"

Getting past the door was easier said than done, since none of Renfield's keys fit. "I thought you had access to everything in the base."

"I thought I did, too." Renfield flipped through his keyring before tossing them to me. "You try it. Maybe I missed one."

Fair enough. Starting with the first key, I tried each in turn until an unlabeled key turned all the tumblers and disengaged the lock. Turning the latch, I gave the door a gentle push and stepped back as it slowly, silently turned on well-oiled hinges. "Better mark it for next time."

"I'd rather there never was a next time." Renfield clipped the keys to his belt, reached inside, and flipped on the lights.

Empty. The bloody room was practically empty. All the bookshelves were bare. There was nary a filing cabinet to its name. The worktables didn't even have dust on them. All that remained was a mainframe humming softly in one corner, so I approached it for a better look. A second bay sat empty, as if it once housed another mainframe.

Finding the system console, I sat down in front of it and tried waking it up. The keys responded in a manner that reminded me of the basement under Gibson Hacking Supply, and the screen flared to life and displayed a familiar prompt.
 
> GENERAL ATOMIC MODEL GA-65536  
> MULTICS VERSION 20481031.23.17  
> UPTIME 10 YEARS, 321 DAYS, 5 HOURS, 9 MINUTES, AND 6.9 SECONDS
 
> tetragrammaton login:
 
"Son of a bitch." What were the odds of there being two of the same model of mainframe, running the same version of the same operating system, with similar uptimes, with the same system name? It was a question I'd have to dump on Malkuth, because the best answer I could come up with is not bloody likely. Regardless of probability, it seemed worthwhile to at least try the credentials Cat gave me. If I got in, we had mirrored systems.

Typing them from my photo of Cat's scrap of paper Cat, I hit the enter key and waited for a response. All I got was a shell prompt.
 
> ctricklebank@tetragrammaton $
 
Now I knew for sure. "This machine's a mirror of the one in Clarion. How is that possible?"

Renfield nodded, and pointed at an empty bay that must have housed a second General Atomic mainframe. "Just before everything went to shit, we got orders to take apart one of the mainframes and transport it into town. Some of the brass must have decided it would be a good idea to keep the backup system in a location that wasn't likely to get bombed because it wasn't a legitimate military target. So we lugged the machine over in pieces, set it up, and hooked it to some kind of monster fiberoptic cable that some other people must have run out to the town, and fired it up."

This could be the break I need. If Petersen was keeping the records for his continued experiments on Tetragrammaton, including that weird breeding program Brubaker insisted was being run on the townspeople, and the two instances of Tetragrammaton remained in sync, I should be able to access it here. First, however, I still needed admin privileges since Cat had given me her credentials.

I tried the usual command, but didn't get the usual response. I got this, instead.
 
Cat, don't forget that if you need to do admin stuff, you can't just use the 'sudo' command like you would on Unix. I rigged a 'su' alias to the actual privilege escalation command, but you need the admin password, not your own.
 
Nice of the guy to leave that little note for Cat, but it didn't help me much. "Shit."

"You got the other password?"

"No. Maybe Cat didn't know about this, or had forgotten." I tried reaching Mike, but without wifi, my implant was useless. "Do me a favor and take over for Brubaker. I need his help."

"Right." I never heard Renfield leave.

Mike soon showed up, but without the rifle. "Need me to log in?"

Standing, I backed away from the machine so he could sit down. "It's worth a try, but I doubt you have the admin password."

"We might not need it." Mike shrugged. "A couple weeks ago, Tricklebank gave me a shell script he wrote, and asked me to run it and help him test some security patches."

Typing a command, he sat back and waited for the result. It wasn't what either of us hoped for. "I guess those patches worked. Too bad the poor bastard never got to find out for sure."

While one shouldn't speak ill of the dead, it was a bloody shame Matt Tricklebank was so good at his job. "Never mind him. Can you get to the network?"

---

### This Week's Theme Song

"You Can't Always Get What You Want" by The Rolling Stones, from *Let it Bleed*

{% youtube oqMl5CRoFdk %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
