---
title: "*Silent Clarion*, Track 63: &quot;In the Presence of Enemies, Part 2&quot; by Dream Theater"
excerpt: "Check out chapter 63 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
My uninvited guest—Ian Malkin, Maestro, or the Devil himself for all I knew—favored me with an indulgent smile. "Call it sufficiently advanced technology. Or magic, if you prefer. It doesn't matter to me either way."

"How dare you claim me as your daughter?" Though I had heard everything he said after that, it seemed irrelevant in the face of his outrageous claim.

"I gain nothing by lying when the truth is just a couple of cheek swabs and a lab test away. Shall we?"

"No, that won't be necessary." Not that I wasn't curious, but this was one situation where I preferred to cope with the doubt over living with the certainty. Besides, it didn't matter. This asshole didn't raise me. He didn't love me despite my turning out to be someone other than the person he hoped I'd become. "Even if you did fuck my mother, neither you nor she could be bothered to actually do the hard part and be my parents."

My accusation actually seemed to wound Malkin, for he fell silent and studied me. The years seemed to weigh more heavily on him despite a visage that suggested he was far too young to have been involved in Nationfall. When he spoke again, he seemed pensive and almost weary. "Had I chosen a different path in life, nothing would have pleased me more than to have been your father. But long before I met your mother I devoted myself to a cause that grants me only rare moments of respite. Had I kept you, I would have relegated you to a succession of nannies and tutors, and you still would have grown up not knowing me. You would have lived under guard, knowing all the while you were a potential bargaining chip to be used against me. I could not in good conscience have condemned you to such an upbringing."

Good conscience? Is this bastard serious? "Malkin, I don't believe for a moment that you ever had a conscience."

"Perhaps not, but consider this: your mother left you to die in a dumpster." Malkin spat the words, and locked his eyes on mine. "She didn't want a demifiend's daughter. It would have been easy enough for me to leave you where I found you, or perhaps even finish the job."

Hadn't they been blue before? Now they were as red as mine. "What happened to your color contacts?"

"I have other ways to hide my true colors." It was impossible to do anything else. Christopher Renfield now lounged in the chair Malkin had occupied only a moment ago, wearing a dress uniform I had never seen the actual Renfield wear. A moment later, I stared across the bed at Colonel Petersen. He saluted, and then shifted back to the snow-blonde dandy in white I had called Maestro. Only now there was two of them. Both spoke together. "When I called myself a demifiend, I was not entirely accurate. My father was a demon, and I was born to an asura mother as you were. I went on to become a demon myself, for reasons that need not concern you."

"A demon? It's the end of the twenty-first century, and you expect me to believe in *demons*?"

Looks like Malkin got tired of the theatrics. There was only one of him, and his eyes were blue again. Thank goodness. I was starting to think I had gone round the bend. Either that, or the painkillers had me tripping balls. "Whether you believe or not is immaterial. Project Harker would have happened without the involvement of Henrik Petersen and Dusk Patrol. The fact that this colonel had created an all-CPMD+ special forces unit was merely an irresistible opportunity."

"An opportunity for *what*? Why would you want to turn CPMD+ people into psionic super-soldiers?"

"You would not believe the truth, since you don't believe that I am a demon, but I will tell you anyway for my own amusement. I intend to create soldiers capable of killing the demon who pretends to be God."

A demon who pretends to be God? And Ian Malkin wants to create a what? A *deicide* squad? "You're right. I *don't* believe you. But since you admitted to conspiring with Dr. Henrik Petersen and conducting unethical scientific experiments on human subject who did not give informed consent, I—"

"Choose your words with exacting care, Naomi. Your idealism has proved amusing thus far, but do not try my patience. You do not yet realize your peril." The menace in his voice sliced the air and in that moment I believed. Ian Malkin was a demon. "I am aware of everything that has happened at Clarion since the inception of Project Harker. Most of my fellow members of the Phoenix Society's executive council is as well, with the exception of a couple of junior members we keep close to ensure their complicity."

My fellow members? Oh, fuck me and marry me young. Ian Malkin's on the bloody XC. "So you've had the authority to block my investigation at every turn. You're the reason Malkuth wouldn't tell me anything. You could have stopped Petersen from activating GUNGNIR, but you were prepared to let him massacre twenty thousand people."

"Twenty thousand is trivial compare to the gigadeaths for which I am already responsible." The matter-of-fact tone with which he made this claim precluded any hope of this being more than his bravado. "Once you were in too deep, I decided that rather than persuading you to leave Clarion alone I would draw you in deeper and see just what you were made of. I wanted to see you in action, to see if you were the one I hoped to create."

The one he hoped to create? This shit just keeps getting deeper, doesn't it. "Who was I supposed to be?"

"That is irrelevant. You are not the one I hoped to create." Magnin began to pace, but kept his eyes on me as if he expected me to strike at him. "I had hoped that like some of the Project Harker subjects, you might prove to be a flowseeker, that you might manifest your psychoenergetic talent under sufficiently extreme duress. I expected GUNGNIR to be the trigger. After activating the system and setting its target, I had locked it down to prevent you from simply jacking in and aborting the launch."

"But I did it anyway, thanks to the tools I found on Tetragrammaton." Damn, I would have to tell Cat Tricklebank that her husband helped save the whole goddamn town. "What did you think I would do, shatter those tungsten carbide rods in midair with my voice?"

The corner of Malkin's mouth crooked as if he wanted to smile, but wouldn't let himself. "That was one possibility. Instead, you found another way to carry out your duties, and played the nightingale in one of the North American Commonwealth's black sites. I could not permit you to finish your duel with Dr. Petersen and bring him to trial, and had you not reached out and touched a guard with your song, you would be there waiting for a court martial that would never come."

It would have been my own personal Chateau d'If. The very notion left me shuddering. "So having me arrested was your idea? Who were the Adversaries you sent to do the job?"

Now Magnin smiled. "Adversaries? More like actors. Call them Rosencrantz and Guildenstern if you must associate names with faces. It is my fond hope you will ever see them again."

"Why is that? Don't want to have them assassinate me?"

Just a second ago, I had been standing in a Philadelphia hospital room interrogating a madman who claimed to be one of the hands pulling the Phoenix Society's strings. Now I had no idea where the *hell* I was. All was darkness around me. The only light was a distant star, so faint that it barely lit the huge dirty snowball tumbling beside me.

Wait. That's what comets are supposed to be. Dirty snowballs. Something bumped into me. Something that could have been a man, his mouth frozen in a rictus. Had I seen him before somewhere?

Before I could scream into the void, I was back in the hospital. Malkin's smile was thoroughly malicious as he regarded me. "If I wanted you dead, Naomi, I would have left you in the middle of the Oort Cloud. Nobody would ever find you out there. You'd just be another meat popsicle."

"What kind of fucked up magic do you do that lets you teleport people into deep space and bring them back alive?" I blurted the question, desperate to grasp at some kind of sense despite being strapped in for a ride on the crazy train. "If you're a demon who can do everything I've seen you do, why don't you rule the world?"

"What makes you doubt that I already do?" Malkin's voice held the quiet confidence of an attorney who had finished delivering an unassailable argument. Sometimes the facts speak for themselves when laid out properly, with no need to give a jury the hard sell. "I don't need to announce myself to the people. In fact, doing so would be counterproductive."

"Everything I've seen and heard has been recorded via Witness Protocol. I can expose you."

"And who controls Witness Protocol? Who has root on the Sephiroth?" Magnin paused, letting his questions strike with the impact of depth charges. "Even if I consigned you to the cold ever-night of interstellar space, you have friends who would demand answers. If I made *them* disappear in turn, I'd only turn more people against me. So, here is how it will be. Your debt is paid. You need no longer serve as an Adversary. The Phoenix Society will celebrate you as a heroine who exposed an old conspiracy, solved multiple murders and disappearances, and saved a town from orbital bombardment—all while on vacation. With the bonus you'll receive, you will have no trouble striking out on your own, though a patron can also be arranged if you wish it."

"What's the price?" There had to be a catch. There's no way this bastard would show he was capable of leaving me in deep space and then offer my fondest desire.

"All you have to do is shut up and play the modest, humble heroine mouthing bromides like the privilege of service. That was Henrik Petersen you bumped into, by the way, and it was time Ian Malkin died as well. A sudden stroke, perhaps. Just walk away and keep your pretty mouth shut."

The hell of it was, I could do it. It would be easy, now that Malkin had proven that the Phoenix Society was rotten. I might owe my fellow Adversaries, but at the same time I had no right to shatter *their* faith. I'd be like a newly minted atheist trying to convince his still-devout neighbors that there were no gods. "What if I refuse, and try to expose you?"

Malkin sighed, as if he expected me to insist upon seeing the stick as well as the carrot. "If you attempt to bring me or Dr. Petersen to trial, I will arrange for the record to reflect that you were in fact arrested for abuse of authority. A court martial will find you guilty on all counts, and condemn you to the guillotine. You will then be made an unperson, your name erased from existence in every way that matters."

---

### This Week's Theme Song

"In the Presence of Enemies, Part 2" by Dream Theater, from *Systematic Chaos*

{% youtube yhHNZXO-RE0 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
