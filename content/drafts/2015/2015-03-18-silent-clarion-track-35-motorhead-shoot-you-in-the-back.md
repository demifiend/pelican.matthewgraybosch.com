---
title: "*Silent Clarion*, Track 35: &quot;Shoot You in the Back&quot; by Motörhead"
excerpt: "Check out chapter 35 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Sheriff Robinson was too slow to heed my warning, so I tackled him. A sickening crack accompanied us as we sprawled across the graveyard path together, my body draped over his to shield him. An arrow thudded into the ground less than a meter away from his head, and rapid footsteps receded into the distance.

Lifting myself from Robinson, I helped him turn over once I realized he was in too much pain to get to his feet right away. It wasn't long before I found out why he was hurt. "Oh, shit. Did I just break your arm?"

Robinson gritted his teeth as he used his good arm to get to his feet. "Probably my fault. Should've hit the deck when you yelled." He spoke again when I crouched to retrieve the arrow we dodged. "Leave that arrow alone."

Damn it. I was going to use it to splint the Sheriff's arm, jury-rigging it with the material from his sleeve, but he was right. It was evidence. "All right, but we need to get your arm splinted, and then get you to the doctor."

Robinson shook his head as an ambulance's siren began wailing. "I've already called for help." He gave a forced smile. "I appreciate it, though. Is there anything you Adversaries don't get training for?"

"If you believe the scuttlebutt, we even get training in lovemaking. It probably helps Adversaries maintain intimate relationships."

The ambulance arrived moments later, and a paramedic cut away the sleeve of Robinson's jacket. He did the same for the shirt underneath before using a prepackaged kit to do a better job of splinting the Sheriff's arm than I could have managed.

The police arrived as the EMT finished. One of them took the arrow into evidence. Another arrested me after I admitted that the Sheriff broke his arm after I tackled him to avoid one of us getting shot. Me and my big mouth.

To my surprise, the police didn't disarm me or book me on criminal charges. They just stuck me in a cell. One of them even offered to get me dinner, but I turned him down with an innocent smile. "An attorney would be nice."

I never got that attorney, but Sheriff Robinson showed up a couple of hours later with his arm in a sling. I didn't bother sitting up as he opened the cell and walked in. "Want me to sign your cast, Sheriff?"

"Not that kind of cast, Adversary. Did my deputies get you dinner?"

"Deputy Rosen offered, but I declined."

"Right. You probably thought you were under arrest." His expression turned sheepish. "I'm sorry about that, Adversary. I wanted to make sure you weren't out where the perp could take another shot at you, but I figured you wouldn't listen. So I had my deputies detain you."

Well, that explains why I wasn't charged or disarmed. "Then I'm free to go?"

He shrugged, and pointed at the open door. "Yeah, but can you do me a favor?"

Let me guess. He wants me to stay out of the demon-ridden woods. As if I didn't already have plans for the night. "I was supposed to meet Kaylee and Michael at The Lonely Mountain. I wanted to see about getting some info out of them."

That was probably what he wanted to hear, because he relaxed. "Good. I was going to ask you to refrain from trying to find our assailant alone."

What kind of stupid git does he take me for? "Not bloody likely now the trail's cold."

He followed me out of the cell. "No hard feelings, right, Adversary? You understood why I had you detained."

Not that I felt particularly understanding. "You wasted valuable time, in which I might have gathered intelligence. Also, what if one of your deputies is the perp, or working with them?"

That got his attention. Riled him up some, too. "How *dare* you suggest my deputies aren't trustworthy, Adversary?"

"Somebody tried to incapacitate one of us today. Unless Dusk Patrol had a little bird in the woods listening to us, then your deputies were the only ones other than the Wilsons who knew where we were."

"Adversary, I trust these kids. For Christ's sake, they all have keys to my house."

"Keys one of them could have used to steal your hunting gear. Did you question all of them in connection with the burglary?"

"No." Robinson wouldn't look at me as he said it. His tone faded from indignant to sullen. "My mother has Alzheimer's, and she goes walkabout. Unfortunately, her disease was too far advanced to be cured by the time a cure was available. I've got a dozen friends around town with keys so they can bring her home if they find her alone at night. Two of them are Kaylee Chambers and the Brubaker kid. You plan to add your friends to the suspect list, or just mine?"

"Thanks for telling me Kaylee and Michael have keys. I'll be sure to check them out as well." And I'll start by asking them why Robinson trusts them with keys to his house. How well do they know each other? Michael seems like a solid kid, but Kaylee doesn't fit. She strikes me as too fun-loving to be friends with an old cop. She's probably reliable, but would he take her seriously?

Chastened, Robinson seemed to calm down, so I tried another question. "Could one your friends have lost their key, or had it stolen?"

Robinson shrugged. "Not likely. My house uses two-factor authentication. You need a physical key, and a numeric passcode."

You could have plotted my hopes as a sine wave as they fell, rose, and fell again. Two-factor authentication would have made a lost or stolen key useless, but a numeric passcode isn't hard to crack, especially if it only has a few digits. "Do I even want to know what the passcode is?"

"One. Two. Three. Four. Five."

Is he serious? Nobody with half a brain would use that on their bloody luggage, let alone their home security. Then again, that might be the only numeric passcode somebody with Alzheimer's could have a halfway decent chance of recalling. "Is your mother able to remember that?"

"No." Robinson shook his head. "It's more for the people I enlisted to help keep an eye on her. If they see her sitting on the porch, they can let her in."

"So, if the keys weren't lost or stolen by somebody who isn't averse to trying to crack a passcode, we have twelve people who need only wait for your mother to give them the perfect pretext for gaining access."

I was pleased with my hypothesis, since it fit the facts and gave us a limited pool of suspects we could rule out with a few simple questions. Naturally, Robinson had to screw it all up. "But nobody has had to let my mother into the house in the last couple of months."

"Dammit." It's fine. This shit happens. You build up an edifice of reasonable logic, and then it falls apart when somebody mentions a new fact that knocks one of your premises out from under it. "Does anybody check on your mother during the day? Would one of your friends drop by when you're not home? Have you had anybody come in to do repairs, like an electrician, or a plumber, or a network or appliance tech?"

Robinson shook his head. "No. Hell, Adversary, I haven't even brought home a date."

"All right, then. Who's her doctor?"

He stared at me as if my brains were made of yogurt. "Dr. Petersen. Who did you think it would be?"

If he thought my last question was bonehead obvious, let's see how he likes this one. "Petersen makes house calls for some patients. Is your mother one of them?"

"Come on, Adversary. Are you saying you think Petersen came to give my mother a checkup, and made off with my bow, arrows, and camera? You think he just walked past my mother with that stuff and out the front door?"

It did sound like a stretch when expressed as Robinson did, but it wasn't impossible. "It depends on how sharp your mother was at the time. Also, you've got a back door, right?"

Robinson nodded. "You think Petersen had an accomplice?"

"It's possible, but not necessary. Depending on where your mother was and how the house is laid out, he might have placed the stolen property just outside the back door, finished his business, walked out the front door, and snuck around back to collect the loot. All he'd need is a pretext for going elsewhere in the house. A bathroom break would do, wouldn't it?"

"Yeah." Robinson ground out the word as if my question had stuck closer to home than he'd like. "I'll ask my mother if she remembers anything before I request a warrant to search Petersen's residence and workplace. I'd invite you along, but she thinks I'm still an MP working at Fort Clarion. If you were there, she'd mistake you for my newest girlfriend."

It wasn't a cold gust of wind just now that made me shiver. Meeting their mothers was one of the worst aspects of dating CPMD- men. They almost always wind up bemoaning my inability to provide them grandchildren, as if that were my sole purpose in life. It's bloody infuriating, and I wouldn't begrudge Robinson sparing me another such experience. "I appreciate it. In the meantime, I'll be at The Lonely Mountain. Michael should be fine, if a bit bored, but I hope Kaylee isn't already too drunk to answer questions."

---

### This Week's Theme Song

"Shoot You in the Back" by Motörhead, from *Ace of Spades*

{% youtube e2xV8cBnGu4 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
