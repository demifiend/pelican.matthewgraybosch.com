---
title: "*Silent Clarion*, Track 58: &quot;I Can't Breathe&quot; by Krypteria"
excerpt: "Check out chapter 58 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
If there's a God, and it possesses any control over the events of our lives, then asking a question like *What else could possibly happen?* is tantamount to teasing a bored cat with a laser pointer. As soon as somebody does so, it's inevitable that causality, fate, or a malicious deity with a juvenile sense of humor will answer.

In our case, the answer came with our return to Clarion after a hike through a forest made almost unnavigable by dense fog. Though we could have waited for the cool morning sun to rise higher in the sky and burn off the fog, we were all hungry, tired, and eager to get the hell away from the fort. Unfortunately, we weren't the only ones to return.

"Good morning, Adversary Bradleigh." Dr. Petersen met us midway down Main Street, smack in middle of the thoroughfare. He had an old-fashioned gentleman's walking stick tucked under one arm, which I had never seen him use before. "Shall I treat you and your companions to breakfast?"

Immediately turning on secure relay chat, I asked Mike and Renfield the obvious question. ｢Have I gone round the bend, or is there a dead man standing in front of us and offering to buy breakfast?｣

｢So what? Let the dead guy buy us breakfast if he wants.｣

Mike's advice was sensible, even if it didn't allay my concerns regarding my mental state. Rather than confront Petersen, if it was indeed him, I nodded. "Thanks. I could certainly use a good meal."

It wasn't until we were cozily tucked away in a far corner of The Lonely Mountain's common room with our breakfasts that I said anything. "I saw you die, Dr. Petersen. One of those soldiers shot you in the throat."

Petersen pulled down his shirt collar to display a wattled but otherwise unblemished neck. "Are you sure? You didn't even think to examine me, let alone confirm brain death."

"Considering the slug ripped out your jugular, Doc, I think Naomi is right. You can't be Dr. Petersen." Renfield paused long enough to take a bit of his four-egg meat lovers' omelet. "So, who the hell are you?"

Mike stared at Renfield. "Who else could this be?"

"Who indeed?" I pointed at Petersen with my fork. "You might as well spill."

"Come now. You've stabbed men through the heart and seen their wounds heal. You yourself got shot with a fifty-caliber rifle, and yet you look perfectly healthy, if a bit underfed. Did you honestly think I would refrain from taking advantage of the technology I tested on Collins and Robinson? The very technology, with which I treated you when you came to get that knife wound stitched up?"

Renfield was on his feet in an instant. "You bastard! You administered the Process on Naomi?"

Petersen sipped his coffee with the insouciance of a man with nothing to fear. "Yes, I did. A far more refined version than what Project Harker inflicted upon you, and strictly temporary."

Hearing that whatever allowed me to survive the previous night was temporary left me torn. I was glad it was, because I was different enough from others already, but the treatment wasn't without its advantages. "How long will whatever you did to me remain in effect?"

"Another two weeks. Assuming you live that long."

Mike glared at Petersen. "What the fuck is that supposed to mean?"

"I suggest you focus on enjoying your breakfast, young man." Petersen's tone oozed condescension.

He took his own advice, but looked up from his bacon and eggs as I rose, drew the side-sword I kept after discarding my other swords in my room, and let Petersen take a good long look at the tip. "If you want to live long enough to enjoy your breakfast, doctor, I suggest you elaborate. Why wouldn't I live long enough for this treatment to wear off?"

"It's quite simple, young lady. None of us are going to live that long. I have activated the GUNGNIR system and programmed it to drop its remaining armament on the town and Fort Clarion." He checked the old-fashioned titanium wristwatch that I had never really noticed him wearing before. "Assuming this old thing is still accurate, I'd say we have about twenty-nine minutes. So dig in. Your last breakfast is getting cold."

Mike dropped his fork. "Have you lost your goddamn mind? Do you have any idea what that weapon can do?"

Petersen nodded. "Of course. I activated it once before, just before everything went to hell, when a bunch of tree-humping peacenik rabble-rousers incited the town to march on Fort Clarion to protest the unethical experiments we were carrying out so that we could better protect their right to be ignorant and lazy while whining about how corrupt and immoral the Commonwealth's government was."

｢Malkuth, are you getting this?｣

｢Yes, Naomi. Petersen is telling the truth. He is in fact guilty of the atrocity for which he just claimed credit.｣

Glaring down at Petersen, I knocked the fork from his hand with my sword. "Why? Tell me why, damn you. Why would you use that weapon to destroy the town you rebuilt, and murder thousands of innocent people?"

"It's really quite simple. The data archive you took from Tetragrammaton didn't contain my later research. There's nothing in there that the Phoenix Society doesn't already possess." Petersen let that sink in for a moment. He gave me a moment, damn him, to realize my time and effort had been in vain. "What do you think the Society would do if they found out that I had found a way to safely and temporarily activate a CPMD+ individual's asura potential? What do you think they'd do if they learned that I had also found a way to give CPMD- individuals the asura potential, and activate it?"

Mike had gone pale, and his voice trembled as he forced out the words. "So, what is this? You're going to sacrifice the town to protect the world?"

Petersen snorted. "No. It's simpler than that, boy. Ian Malkin exiled me here, and condemned me to live out my best years in this shit-hole on pain of exposure. So I will deprive him of the breakthrough that eluded him and his pet scientists." He turned a gimlet eye toward me. "And while I'm at it, I'll deprive him of his daughter."

Daughter? Though I wanted to demand further details, a choked sob from Renfield distracted me. Though he must surely have done and seen far worse as a soldier, learning his former commanding officer's intentions must have left him aghast. "Colonel, you're going to murder Dusk Patrol as well. Don't you care?"

Petersen smiled. "Christopher, it's time you and the rest of the boys were put out of your misery. There's no way you can go back to the world as you are. My only regret is that you won't be buried at Arlington with the honors your service to the Commonwealth would merit."

At that, Renfield bared his teeth and lunged for Dr. Petersen. Mike and I had to combine our strength to keep the enraged sergeant from ripping out his former commanding officer's throat with his bare hands, and I had to turn my sword on him yet again. "Kill him now, and he wins."

Petersen shrugged. "I wouldn't have explained your fate or my motives if the slightest possibility of you stopping me existed."

Mike spat in his face, which should have been beneath him. "Fuck you, old man."

Petersen wiped his mug with his sleeve and checked the time. "Twenty minutes. You might want to say your last goodbyes."

My entire body had begun to tremble, and all I could hear was my heart in my throat as tunnel vision set in. The response was a familiar one, and one my training normally tempered, but this time everything I had learned about remaining calm under pressure deserted me. I was ready to fight, but what threatened me wasn't an enemy I could face with my sword. I was ready to flee, but where would I go to escape the knowledge that I had left thousands to die? ｢Malkuth, please tell me you can do something about this. The Phoenix Society was supposed to have control of GUNGNIR, wasn't it?｣

｢Yes.｣

｢So, GUNGNIR shouldn't have been activated without the Society's permission, right?｣

｢Correct.｣

Oh God, I was even more afraid of where this line of inquiry was leading than I was of imminent orbital bombardment. ｢Who in the Phoenix Society would have the authority to activate GUNGNIR?｣

｢Naomi, I'm really sorry, but I can't tell you.｣

｢God damn you, Mal, I don't have time for your clearance bullshit right now. Tell me who authorized this, because Petersen sure as shit couldn't have done it himself, give me the override codes, or direct me to somebody who can! Thousands of human lives are at stake here. Human lives we are sworn to protect. If you don't, then you'll be complicit in the biggest violation of individual rights since Nationfall.｣

Every second felt like a day as our personal countdown to extinction ticked down. Five minutes passed before Malkuth finally replied. ｢Speak to Edmund Cohen. He's on the Executive Council. I'm sorry I can't do more, Nims.｣

The shaking only worsened, and the Lonely Mountain's walls started to close in on me. Running outside, I tried to connect to Eddie. Please be there, you lecherous old stoner. Please. ｢Nims? You're OK?｣

｢No, I'm not OK. Listen: I need you to help me stop GUNGNIR from bombarding Clarion. Malkuth insists he can't, and told me to speak to you because you're on the XC.｣

｢GUNGNIR? Great. Just fucking wonderful.｣

Mike ran out, his eyes wide and staring. He glanced skyward as he rushed toward me. "Goddamnit, Naomi, what are you doing?"

"Back off." Glaring at Mike, I managed to keep from turning my sword on him, but my voice was still a scared, angry hiss. "I know you're as scared as I am, but this isn't the fucking time."

Mike backed away, and I returned my attention to Eddie while staring up into space. What would the tungsten lances look like as they rained death and destruction on Clarion? A perverse corner of my mind was obsessed with the question. ｢Help me, Eddie. We've only got ten minutes.｣

｢Naomi, I've been trying to help you. I'm going to send you the override code and the satellite's IP address now. I already tried using it, but GUNGNIR wouldn't let me connect. Maybe you'll have better luck.｣

Eddie's message came through as promised, and I immediately attempted to connect. Come on…

> Oppenheimer-Teller Aerospace Corp.  
> OpenBSD 6.9  
> Property of NACAF

> GUNGNIR login:

Bloody hell. They really have put Unix on everything. At least Eddie thought to provide credentials and instructions. I followed them, and got a prompt for the override code, which I sent. The response came seconds later.

> Unauthorized override attempt detected.  
> Terminating remote session.  
> Have a nice day.

Oh, no you don't. Connecting to Tetragrammaton as the sysadmin, I switched to Petersen's directory and checked to see if the old man had been dumb enough to put his credentials in a file I could read. No such luck, which meant I had to crack root.

Figuring the late Matt Tricklebank might have useful tools, I switched to his account and poked around. His copy of the HermitCrab source had a directory called "dbfi-experimental", so I accessed it and opened the README file. Turns out DBFI stand for "distributed brute force intrusion", and this app would attempt to leverage the entire network to crack root on a target machine.

Mike kept glancing skyward, but didn't speak to me. Had I frightened him? Renfield, however, was not so reticent. "Naomi, if you've got an ace in the hole, now would be a really good time to play it."

"Working on it." I ground out as I figured out how to run the DBFI tool. Turns out Tricklebank had a test script that would spawn a few thousand virtual machines, all of them running a DBFI client targeting the machine of my choice. Aiming the script at GUNGNIR, I ran it and soon had a hundred thousand processes trying passwords against the root account.

A hundred thousand wouldn't be enough, but I was already pushing Tetragrammaton to its limit. I needed more power. ｢Malkuth, I've got an idea, but I need your help.｣

｢Shoot.｣

Before explaining, I sent the code. ｢Matt Tricklebank has a distributed brute force intrusion tool with a script that can spawn thousands of virtual machines. I want you and the rest of the Sephiroth to run this script on GUNGNIR. If one of you manages to crack root, the tool will automatically give me control.｣

Five minutes left, and Malkuth had gone quiet. No doubt he and the others were deliberating, but neither I nor the people of Clarion had time for a bunch of AIs to piss about with a discussion.

Four minutes and thirty seconds remained on the clock when the DBFI control panel reported the addition of additional clients. I had jumped from a hundred thousand processes attacking GUNGNIR to a hundred billion. More processes came online, until I had just over a trillion little virtual machines pounding the satellite. Maybe I'd end up frying the onboard computer instead of cracking it. Would that prevent it from deploying its payload?

The clock was down to three minutes when the control panel overlaying my vision disappeared. A terminal connected to GUNGNIR with a control menu appeared in its place. Seeing that one of the options available was "Cancel Current Deployment", I chose it and waited.

Time seemed to stretch as the countdown I ran based on Petersen's estimate ticked down to the point where only seconds remained. Each second felt like an hour, and cold sweat soaked through my clothes as I waited and hoped that any second now a response would come back down the pipe.

---

### This Week's Theme Song

"I Can't Breathe" by Krypteria, from *Bloodangel's Cry*

{% youtube XCkg5DDmrt0 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
