---
id: 2146
title: Naomi Bradleigh Always Steals the Show
date: 2015-10-28T16:00:07+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=2146
permalink: /2015/10/naomi-bradleigh-always-steals-the-show/
sw_twitter_username:
  - MGraybosch
medium_post:
  - 
snap_MYURL:
  - 
snapEdIT:
  - 1
snapPN:
  - 's:229:"a:1:{i:0;a:8:{s:9:"timeToRun";s:0:"";s:9:"apPNBoard";s:18:"224054218900737052";s:10:"SNAPformat";s:15:"%TITLE% - %URL%";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doPN";i:0;}}";'
snapRD:
  - 's:166:"a:1:{i:0;a:6:{s:4:"doRD";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:8:"postType";s:1:"A";s:10:"SNAPformat";s:0:"";s:11:"isPrePosted";s:1:"1";}}";'
nc_postLocation:
  - default
twitterID:
  - MGraybosch
snap_isAutoPosted:
  - 1
bitly_link:
  - http://bit.ly/1Mj5r1P
snapFB:
  - 
snapGP:
  - 
snapTW:
  - 's:311:"a:1:{i:0;a:10:{s:4:"doTW";s:1:"1";s:9:"timeToRun";s:0:"";s:10:"SNAPformat";s:31:"%TITLE% - %URL% via @MGraybosch";s:8:"attchImg";s:1:"1";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:18:"659459969230614528";s:5:"pDate";s:19:"2015-10-28 20:01:04";}}";'
sw_cache_timestamp:
  - 401689
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - How Not to Write
  - Meet the Cast
  - Starbreaker
tags:
  - character
  - character growth
  - characterization
  - naomi bradleigh
  - silent clarion
  - women
  - writing women
---
Naomi Bradleigh wasn’t snow-blonde with scarlet eyes at first. She wasn’t an Adversary, or skilled with an Italian side sword. Her father wasn’t a central figure in the Starbreaker saga. She wasn’t even a member of Crowley’s Thoth, if memory serves, but a session musician brought in to record the keyboard parts for an album called _Glass Earth Falling_. She wasn’t supposed to become the leading woman in my first novel, _Without Bloodshed_. She certainly wasn’t intended to get her very own novel, the recently completed _Silent Clarion_.

She was just supposed to get one scene. _One scene_, to please a girl called Naomi I was dating at the time. Instead, she stole the show. My wife says that another character of mine, Claire Ashecroft, is my id. If you prefer Jung to Freud, maybe Naomi Bradleigh is my [anima](https://www.psychologytoday.com/blog/freudian-sip/201103/jung-dummies-animus-planet-0 "Jung for Dummies: Animus Planet"). Or maybe I’m just being pretentious. 🙂



## A Mind of Her Own

The girl I was dating at the time asked me if she could be in the story; Naomi wanted a cameo as part of the band Morgan Stormrider and Christabel Crowley were in at the beginning of the version of _Starbreaker_ I had been writing at the time. The character’s surname came from the name of the English country house the real Naomi grew up in. But something funny happened after Naomi and I broke up and lost touch with each other.

I sat down to write one night back in 2000 while listening to the [_Ashes are Burning_ album by Renaissance](https://open.spotify.com/album/1C2fgiQmiTF9Dr8NdbPSou "Spotify: Ashes are Burning by Renaissance"), and found that a character I had written to please a girl had become her own woman. This Naomi wasn’t some quiet, bespectacled chestnut-haired goth content to sit in a corner of the studio and play a piano part someone else had written for her.



No. This _new_ Naomi was older, and more mature. She was older than Starbreaker’s hero, Morgan Stormrider. She had warned him against becoming an Adversary when he was a young man, because she had been one herself. Not only had she served, but she was also a classically trained soprano and pianist with a love for rock and heavy metal.

Over the years, I found she had something to say in just about every scene where it made sense for her to be physically present. If there was a fight, she was there with sword in hand. But her voice was nothing like that of my ex.



## Mistakes? I’ve Made a Few

The problem with being a man writing about writing a woman who not only becomes a co-star of sorts in one book (_Without Bloodshed_) and then stars in her own story (_Silent Clarion_) is that it is entirely too easy to wax self-congratulatory. It’s all too tempting to sound like this:

> Check out this character I wrote. She fights beside the men, but is still feminine. She has agency. I didn’t use rape to develop her character. Aren’t I a good little writer man? 

Thing is, I don’t deserve a fucking cookie for writing a character like Naomi. First, her first appearance in _Without Bloodshed_ is at least a little problematic; she too easily accepts Isaac Magnon’s explanation for why he needs to get into her neighbor and former band mate Christabel’s house at face value.

Furthermore, while it’s true none of my women characters were raped (Edmund Cohen wasn’t so fortunate), I did show Naomi getting groped by a dirty cop trying to coerce a confession to murder from her. Moreover, I only showed her able to hold this cop at bay until Morgan, Eddie, and Sid arrived to help her. It would have been better to have her walk out of MEPOL HQ just as Morgan and the guys turn up, and say, “I wouldn’t have minded getting a cab, but I appreciate the ride.”



So, even though I’ve gotten praise for my women characters from women who have read _Without Bloodshed_, I could have done better. Moreover, I _want_ to do better. I don’t care if lit-fic snobs think my prose is only workmanlike at best, but by Arioch I damn well want to be remembered for writing complex, twisty plots and believable, multifaceted characters regardless of gender. (Admittedly, it wouldn’t kill me to do better in terms of racial diversity.)

It was with that in mind that I first wrote a novelette called _Steadfast_, in which a younger Naomi took in a mission that brought her with a surviving subject of an old military experiment called Project Harker. This novelette served as the basis for _Silent Clarion_, the serial that concluded this week.



## If Men’s Stories are Universal, Must They Be About Men?

Lately, there’s been a #StoriesForAll hashtag on Twitter started by YA author Shannon Hale, who’s concerned that boys are discouraged by their parents, siblings, and peers from reading books about girls and women because such books are “girl books”, and thus not appropriate for boys. But books about boys and men are for _everyone_.

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    This cultural belief is so deep it's perceived as "natural":<br />"Men's stories are universal. Women's stories are for girls."<a href="https://twitter.com/hashtag/StoriesForAll?src=hash">#StoriesForAll</a>
  </p>
  
  <p>
    &mdash; Shannon Hale (@haleshannon) <a href="https://twitter.com/haleshannon/status/655027041029328896">October 16, 2015</a>
  </p>
</blockquote>



I won’t claim to be a great feminist ally. However, _Silent Clarion_ is fundamentally a story about a detective-type character on vacation who stumbles upon a mystery that gets real weird, real fast. It’s the sort of story that’s often written with a man as its protagonist, but I could see _no_ reason why it couldn’t be written with a woman instead.

Furthermore, I saw no reason why I couldn’t write a woman from her own viewpoint, in her own voice. In fact, I welcomed the intellectual challenge of getting into Naomi’s head, and seeing her world as she would have when she was twenty.



## Being Naomi Bradleigh

Yeah, I ripped the title off of [Being John Malkovich](https://www.youtube.com/watch?v=K7ahIGLNNwo "Trailer for Being John Malkovich"). However, that’s what it’s like to write Naomi Bradleigh from _her_ perspective. I had to try to think like her, and experience _her_ appetites and desires. I had to try to adopt a [female gaze](https://www.youtube.com/watch?v=FdHJG67xmsQ "http://www.huffingtonpost.com/entry/laci-green-magic-mike_55ae4670e4b07af29d564711"), as opposed to the [male gaze](http://imlportfolio.usc.edu/ctcs505/mulveyVisualPleasureNarrativeCinema.pdf "Visual Pleasure and Narrative Cinema (1975) - Laura Mulvey") I’ve used before, even when writing scenes with women as viewpoint characters.

Moreover, I had to write a younger Naomi than the one readers of _Without Bloodshed_ already knew. Naomi cultivated her collected, demure stage presence to avoid stealing too much of the limelight from Christabel Crowley. I had to avoid projecting it onto Naomi’s younger self, while still portraying her in a manner that would justify the cultivation of such a persona later in her life. She’s brash as a young woman, and perhaps a bit too forthright. I had to write the younger Naomi as a woman caught between her intelligence and training, and her inexperience and relative immaturity.



Have I succeeded? That’s for _you_ to decide. Hopefully you enjoyed _Silent Clarion_ even if my ambition exceeded my ability.



[This post originally appeared at curiosityquills.com on 21 October 2015](https://curiosityquills.com/the-end-of-silent-clarion-naomi-always-steals-the-show/)<figure id="attachment_2884" style="width: 614px" class="wp-caption aligncenter">

<img src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/naomibradleigh_harveybunda-614x1024.jpg?fit=614%2C1024" alt="Naomi Bradleigh - Artwork by Harvey Bunda (2012)" class="size-large wp-image-2884" srcset="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/naomibradleigh_harveybunda.jpg?resize=614%2C1024 614w, http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/naomibradleigh_harveybunda.jpg?resize=180%2C300 180w, http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/naomibradleigh_harveybunda.jpg?w=768 768w, http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2015/10/naomibradleigh_harveybunda.jpg?resize=610%2C1017 610w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px" data-recalc-dims="1" /><figcaption class="wp-caption-text">Naomi Bradleigh &#8211; Artwork by Harvey Bunda (2012)</figcaption></figure>