---
id: 2449
title: Sugar Policy in the West Needs Solid Science
date: 2015-11-02T13:24:38+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=2444
permalink: /2015/11/sugar-policy-west-solid-science/
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 
snap_MYURL:
  - 
snapEdIT:
  - 1
snapFB:
  - 
snapPN:
  - 's:229:"a:1:{i:0;a:8:{s:9:"timeToRun";s:0:"";s:9:"apPNBoard";s:18:"224054218900737052";s:10:"SNAPformat";s:15:"%TITLE% - %URL%";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:9:"isAutoURL";s:1:"A";s:8:"urlToUse";s:0:"";s:4:"doPN";i:0;}}";'
nc_postLocation:
  - default
twitterID:
  - MGraybosch
snap_isAutoPosted:
  - 1
bitly_link:
  - http://bit.ly/1SjjIRf
snapTW:
  - 's:311:"a:1:{i:0;a:10:{s:4:"doTW";s:1:"1";s:9:"timeToRun";s:0:"";s:10:"SNAPformat";s:31:"%TITLE% - %URL% via @MGraybosch";s:8:"attchImg";s:1:"1";s:9:"isAutoImg";s:1:"A";s:8:"imgToUse";s:0:"";s:11:"isPrePosted";s:1:"1";s:8:"isPosted";s:1:"1";s:4:"pgID";s:18:"661247763871584256";s:5:"pDate";s:19:"2015-11-02 18:25:07";}}";'
snapGP:
  - 
snapRD:
  - 's:166:"a:1:{i:0;a:6:{s:4:"doRD";s:1:"1";s:9:"timeToRun";s:0:"";s:11:"SNAPformatT";s:7:"%TITLE%";s:8:"postType";s:1:"A";s:10:"SNAPformat";s:0:"";s:11:"isPrePosted";s:1:"1";}}";'
sw_twitter_username:
  - MGraybosch
sw_cache_timestamp:
  - 401802
categories:
  - News
  - Science
tags:
  - control group
  - obesity
  - replicability
  - Robert Lustig
  - sample size
  - science
  - statistics
  - sugar
  - The Guardian
  - Vox
---
For the most part, I agree with [Dr. Robert Lustig](http://profiles.ucsf.edu/robert.lustig)&#8216;s opinion that added sugar should be taken out of processed food &#8212; or at least taxed at a rate sufficient to make it unattractive to manufacture _or_ purchase. He's a doctor who specializes in pediatric endocrinology. The man's even done studies, as he announces in [a _Guardian_ opinion piece](http://www.theguardian.com/commentisfree/2015/oct/27/science-new-study-case-sugar-tax):

> Here at the University of California, we’ve attacked this problem head-on. Tuesday marks the release – in the journal Obesity – of our study, which dissociates the metabolic effects of dietary sugar from its calories and weight gain. Instead of giving added sugar to adults to see if they got sick, we took the added sugar away from 43 obese children who were already sick, to see if they got well. But if they lost weight, critics would argue that the drop in calories or the loss in weight was the reason for their improvement. Therefore, the study was “isocaloric”; that is, we gave back the same number of calories in starch as we took away in sugar, to make sure they maintained their weight. 

Surely Dr. Lustig knows what's he's doing. He's a doctor, he knows his shit, and he says "the science is in". Right? Maybe not, [as Julia Belluz reports for _Vox_](http://www.vox.com/2015/11/2/9658116/cutting-sugar-myth):

> What got missed in many of the media reports was that the research was incredibly limited. The study ran for only nine days, and involved 43 children — and, importantly, **no comparison (or "control") group.** 

No control group? I went back and read the article again, [checked Ms. Belluz sources](http://www.stats.org/glaring-flaws-in-sugar-toxicity-study/), and then hunted down [the original paper](http://onlinelibrary.wiley.com/enhanced/doi/10.1002/oby.21371/), _Isocaloric fructose restriction and metabolic improvement in children with obesity and metabolic syndrome_ by Robert H. Lustig, Kathleen Mulligan, Susan M. Noworolski, Viva W. Tai, Michael J. Wen, Ayca Erkin-Cakmak, Alejandro Gugliucci, and Jean-Marc Schwarz. Here's the abstract, in case Wiley removes the paper:

> ## Isocaloric fructose restriction and metabolic improvement in children with obesity and metabolic syndrome: Abstract
> 
> ### Objective
> 
> Dietary fructose is implicated in metabolic syndrome, but intervention studies are confounded by positive caloric balance, changes in adiposity, or artifactually high amounts. This study determined whether isocaloric substitution of starch for sugar would improve metabolic parameters in Latino (n = 27) and African-American (n = 16) children with obesity and metabolic syndrome.
> 
> ### Methods
> 
> Participants consumed a diet for 9 days to deliver comparable percentages of protein, fat, and carbohydrate as their self-reported diet; however, dietary sugar was reduced from 28% to 10% and substituted with starch. Participants recorded daily weights, with calories adjusted for weight maintenance. Participants underwent dual-energy X-ray absorptiometry and oral glucose tolerance testing on Days 0 and 10. Biochemical analyses were controlled for weight change by repeated measures ANCOVA.
> 
> ### Results
> 
> Reductions in diastolic blood pressure (−5 mmHg; P = 0.002), lactate (−0.3 mmol/L; P < 0.001), triglyceride, and LDL-cholesterol (−46% and −0.3 mmol/L; P < 0.001) were noted. Glucose tolerance and hyperinsulinemia improved (P < 0.001). Weight reduced by 0.9 ± 0.2 kg (P < 0.001) and fat-free mass by 0.6 kg (P = 0.04). Post hoc sensitivity analysis demonstrates that results in the subcohort that did not lose weight (n = 10) were directionally consistent.
> 
> ### Conclusions
> 
> Isocaloric fructose restriction improved surrogate metabolic parameters in children with obesity and metabolic syndrome irrespective of weight change.
> 
> _Lustig, R. H., Mulligan, K., Noworolski, S. M., Tai, V. W., Wen, M. J., Erkin-Cakmak, A., Gugliucci, A. and Schwarz, J.-M. (2015), Isocaloric fructose restriction and metabolic improvement in children with obesity and metabolic syndrome. Obesity. doi:10.1002/oby.21371_ 

## Interpreting the Sugar Study

Based on the abstract, Lustig, et al used [ANCOVA](https://en.wikipedia.org/wiki/Analysis_of_covariance) techniques to control for weight change. That may have been enough for the people doing peer review, but I still think this study needed a separate population of children _not subject to the experiment_.

You could argue that most of the American population is a control group for experiments of this sort, but it's an obvious and easy attack on a study Dr. Lustig touts as proving that added sugar needs to be dealt with at a public policy level. Never mind that, to my knowledge, this study's results haven't been replicated (more basic science), but without a control group how do you prove that replacing added sugars with starches to maintain caloric balance actually helps?

Furthermore, look at the sample size: 43 children. That's right. The study Dr. Lustig touts only used _forty-three children_. How is that a statistically significant sample of the population? It isn't, but you don't have to take the word of a dumb programmer. [Here's an analysis by somebody who knows statistics](http://www.stats.org/glaring-flaws-in-sugar-toxicity-study/), and it ain't pretty.

As I stated earlier, I'm all for sticking it to the food industry by going after added sugar in processed foods. However, the push to regulate and tax sugar needs to come from unassailable science. The sugar industry has as much to lose from regulation as the fossil fuel industry, and will fight the science just as hard.

Solid science means reproducible experiments with control groups, at a bare minimum. It means peer-reviewed research published in not only _Obesity_, but in _The Lancet_, the _New England Journal of Medicine_, other major medical journals. And general-interest publications like _The Guardian_ and _Vox_ are remiss in not linking to original articles.

#### Photo Credit

"Little kids getting sugared up." Photograph: The Guardian/Alamy

Originally used in op-ed ["The science is in: the case for a sugar tax is overwhelming"](http://www.theguardian.com/commentisfree/2015/oct/27/science-new-study-case-sugar-tax) by Dr. Robert Lustig.