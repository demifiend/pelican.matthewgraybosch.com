---
title: "*Silent Clarion*, Track 40: &quot;She Is Young, She Is Beautiful, She Is Next&quot; by Perturbator"
excerpt: "Check out chapter 40 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
The #clarion\_underground channel erupted in virtual tumult at my order, and I had neither the time nor the inclination to deal with the shit storm. Nor was I about to justify myself to these kids. Instead, I pulled still images from my feed, one for each of the victims thus far, and posted them with a simple message: "Do as I say if you want to live."

Rather than stick around for the reaction, I disconnected and shut down Yoder's machine. My pace was swift as I left his simulated home and began my walk back to The Lonely Mountain. On the way, I used my implant to evaluate transportation and lodging options. I needed those kids away from here and in a safe location. If that location remained secret, so much the better, though invoking the Phoenix Society's aegis would probably serve to deter any notion of betrayal on the part of those I must perforce trust to carry these kids off to safety.

｢Malkuth, I need evac for at least four witnesses, and a safe house, in which to keep them. What can you do for me?｣

｢You figure the killers are limited to the vicinity?｣

｢Those kids are dead if I'm wrong about that.｣ How far away was far enough? Pittsburgh was definitely too close, but was New York far enough to be safe? London would be better, but could I justify it? ｢Yoder, Wilson, and Foster had all been under Fort Clarion, but other kids have been down there, too.｣

｢I just dispatched a bus from Pittsburgh to pick up your witnesses at The Lonely Mountain and transport them to the New York Chapter. We can put them up at the hotel across the street, and detail some senior ACS cadets to stand guard. It isn't exactly discreet, but the alternative was a helicopter that wouldn't be available until tomorrow｣

｢The bus is armored, right?｣ I'd love to see the look on Petersen's face as the chopper lifted off, taking my witnesses away. However, Malkuth's right. A bus would do just as well as long as Dusk Patrol doesn't waylay it.

｢Come on, Nims. Give me a little credit. I even arranged a two fireteam escort with the Fallen Angels MC. We're gonna whisk those kids away in style like badass rock stars trying to avoid paparazzi.｣

Knowing that Malkuth had hired Fallen Angels to escort the bus helped me breathe a little easier. The bikers were reliable mercenaries, and I wouldn't be the first Adversary to take advantage of their services. They were bloody expensive, however, which was why I didn't consider hiring them to tear apart Fort Clarion. Furthermore, bringing a few dozen Angels to Clarion would do little to endear me to the locals, or to local authorities. Even ten Fallen Angels seemed a bit excessive. If the Phoenix Society sent as many Adversaries on the same mission, they'd probably be dismantling an interplanetary corporation.｢Thanks, Malkuth. I suppose this is coming out of my salary.｣

｢Let's just say you're going to have a bit of explaining to do next time you report your expenses.｣

Shit. Facing an auditor over this would be no less an ordeal than this job has been, but I couldn't afford to dwell on it now. Got some lives to save. More than I expected, it turns out. Four young men and three young women awaited my arrival at The Lonely Mountain. They sat at one of the biggest tables in the common room, their bags piled up in the corner behind them.

After double-checking the bags, I cleared my throat to get their attention. "We seem to be a bag short. Who mistook this for a day trip?"

Brubaker looked up from cleaning his shotgun. "I'm not going anywhere, Adversary." His explanation was evidently for my eyes only, since it came via secure talk. ｢You need somebody who knows the woods. And I can watch your back.｣

One of the girls began to pout. "If Mike gets to stay, why should the rest of us go?"

Tempting as it was, telling the girl she had to go because I bloody well said so didn't seem likely to persuade any of the youths sitting before me. Instead, I sat down with them. "Who saw the photos I posted to IRC? Raise your hands."

The girl who complained kept her hand down. Likewise for the brunette sitting beside her. "Adversary, I didn't see the photos. David got an email from Mr. Tricklebank and told me we had to leave."

That left the complainer. She narrowed her eyes at me. "Fine. I saw the pics, but they don't explain anything. Why should we be inconvenienced because of a few dead people?"

Brubaker shook his head. "Kelly, stop acting like a bitch. We don't have time for your shit right now. You were under Fort Clarion with the rest of us. For fuck's sake, Scott was your cousin. Do you want the same thing to happen to one of us? Adversary Bradleigh probably thinks we're next."

"But why would they kill any of us? We didn't do anything wrong. The place was abandoned."

"It isn't." That got everybody's attention, even Kelly's. "Fort Clarion was never abandoned. Some of the people stationed there are still alive, and haven't forgotten their duty."

"But wouldn't they be really old?" One of the other youths had a dubious look on his face.

"Do you want to see the photos again? I fought two of them a few nights ago. They are most certainly not old, and had no need to fight at a distance. Were I a bit slower, I might have been gutted."

I had their attention now. "These aren't ordinary soldiers. They've been changed as a result of a pre-Nationall experimental program called Project Harker. Its intended result was to turn people with CPMD into vampires. The subjects were hardened soldiers before the Commonwealth Army's scientists got at them, and are all the deadlier now."

One of the young men started at my use of the name Harker, but Kelly gave a disgusted snort before I could question him. "Vampire soldiers? Under Fort Clarion? Do you have any idea how ridiculous you sound, Adversary? And we thought Yoder was fucked in the head."

"Is it really so ridiculous, Kelly?" The brunette next to her spoke up. "David, isn't there a guy who has a monthly appointment to stop at your parents' grocery store after midnight?"

The young man who had recognized the name earlier nodded. "I wanted to say something before, Jill, but didn't want to interrupt." He turned toward me. "Adversary, there's a standing order at my parents' shop that has been active since before they purchased the store. I'm pretty sure the name on the order is Harker. A man always comes to the shop on the first of the month after midnight to pick it up. He wears an old army uniform, and I think I saw the name Renfield on it. Somebody named Petersen always picks up the tab. I think it's the doc, but I'm not sure."

So, that's why Fort Clarion's pantry had fresh groceries. That son of a bitch Renfield is making monthly midnight shopping trips. But why would he make them midnight if he can get around in the day or dusk? Does he come alone, or brings men with him? And how does he cart his groceries back to the bloody base? It isn't exactly a stroll. "David, this is important. Did Renfield ever show up with anybody else?"

David's eyes narrowed, and he looked past my shoulder. Turning around, I caught a glimpse of Sheriff Robinson shouldering his way through the patrons. A few of them objected, but Robinson calmed them down by flashing his badge.

He seemed neither surprised nor pleased to find me here. "I suppose I have you to thank for the panicked parents screaming at me because their kids packed a bag and bugged out without a word of explanation. Not to mention the fucking Mayor up my ass. Care to tell me what's going on, Adversary?"

He's got the Mayor up his ass? Poor baby. "I know why Yoder, Wilson, and Foster were murdered. A couple of weeks ago, they got into Fort Clarion and poked around underground. These seven were with them. Thinking of their safety first, I arranged for the Phoenix Society to place them in protective custody."

"Where?"

Why would Robinson care about that? Shouldn't he be grateful that the Society is looking out for these kids? "My superiors didn't tell me that."

"Figures." Robinson's chuckle held a bitter note. "OPSEC, need-to-know, and all that spook shit."

"Which doesn't make your job easier, does it? You've still got all those scared parents. What will you tell them?"

Robinson shrugged. "Not my problem any longer. I told 'em to take it up with the Phoenix Society."

Thanks for nothing, but I suppose it was the sensible thing for him to do. It's not like having the kids spirited away was his idea. The revving of motorcycles outside kept me from telling Robinson I understood his passing the buck. "I think that's our ride."

Three Fallen Angels walked in, and that was not a joke. They more closely resembled soldiers than bikers; their jeans and leather had the neatness of uniforms, and their postures and habit of scanning the common room as they approached suggested rigorous training. The one in the middle even had stripes similar to those identifying Renfield sewn into the sleeve of his jacket, and he saluted with his fist over his chest like he was one of ours. "Adversary Bradleigh? I'm Sergeant Jackson from the Fallen Angels. Mind if I transmit the ID for my orders?"

As I returned his salute, I found his IP address and opened a secure talk session. "Ready."

A long string of random text came through, and I passed it to Malkuth. He confirmed its authenticity and relayed to me the orders passed to the Fallen Angels: take my witnesses into custody and escort them to a secure location. The location was redacted, naturally. I didn't need that information, and the Society didn't need me blabbing if captured. "Thank you, Sergeant Jackson. You'll be escorting six tonight."

Turning to the kids, I introduced Jackson as Mike Brubaker left them and joined me. "Sergeant Jackson and his squad will escort you to a secure location away from Clarion, where the Phoenix Society will keep you in protective custody until I've resolved the situation here. Follow his instructions, please."

"Thanks, Adversary Bradleigh. If you folks will just grab your bags and follow me, we'll get you situated. You'll be traveling in style, but don't count on the minibar being stocked." Sergeant Jackson led the motley crew out of the Lonely Mountain, ignoring the boys's disappointed groans. I guess they were looking forward to free liquor.

Robinson was still there, watching them leave. "They weren't the only reason I came looking for you. There's been another murder."

---

### This Week's Theme Song

"She Is Young, She Is Beautiful, She Is Next" by Perturbator, from *Dangerous Days*

{% youtube IGqeyQhBPMI %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
