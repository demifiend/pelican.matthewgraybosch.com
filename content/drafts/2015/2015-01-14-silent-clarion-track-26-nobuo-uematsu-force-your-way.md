---
title: "*Silent Clarion*, Track 26: &quot;Force Your Way&quot; by Nobuo Uematsu"
excerpt: "Check out chapter 26 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
The tree to which I clung swayed gently in the breeze as I observed Fort Clarion. The base was so brightly lit that I was unable to understand how it managed to go unnoticed so long. The now-anomalous radiance should have made the installation visible from orbit. But as I used my implant to check satellite imagery of the area, I couldn't find a single photo taken at night that showed the base as I saw it.

If the Phoenix Society were hiding Fort Clarion and Project Harker, would their efforts to keep the secret extend as far as doctoring publicly available satellite imagery so that this place doesn't stand out?

Hell, why hasn't anybody from town investigated? Somebody should have noticed the light pollution emanating from this particular neck of the woods. Maybe hunters, or rebellious teenagers having a keg party. The missing tourists and runaway teens I had heard about were most likely the barest tip of the iceberg. What else was Clarion hiding?

Once my eyes adjusted to the light, I spied at least a dozen men bustling throughout the base. They worked in silence, checking every building. The early morning was so still, I could hear that damn recording from the base's post exchange. Would they realize that we had spent most of yesterday afternoon in there? Would my theft of that girlie magazine from the manager's office draw their attention?

The tree began to tremble, and I looked down to see Renfield ascending. I offered him the binoculars so he could take a look, but he just put them back in the case attached to his belt. "Now you have confirmed that Fort Clarion is inhabited, and by whom. What will you do?"

Did he hope I'd back off and leave Fort Clarion alone? "The Phoenix Society ordered me to catalog all materiel inside the base for proper disposal by an arms control team. I'm sorry, but I can't abandon my mission."

Renfield nodded. "Were I a lesser man, the smart thing for me to do would be to push you out of the tree and finish you off on the ground."

My whole body went cold, and my muscles tensed in response to his words. Would he murder me in cold blood for his squad's sake? "Doing so would do little to save your men at this point. If you murdered me, the Phoenix Society would send more Adversaries to investigate. They'd find you and the rest of Dusk Patrol, then rip Fort Clarion out of the earth piece by piece to get to you."

Renfield flashed a predatory grin revealing sharp white canines. "You might be right, but it would be a hell of a last stand."

Though I was only six meters from the ground, and could easily survive the fall, the thought of Dusk Patrol fighting to the last against any Adversaries sent to avenge me left me shivering. Should other Adversaries die at their hands, the Phoenix Society would wage all-out war against Renfield and his men. With control of GUNGNIR, GAEBOLG, and LONGINUS, they could flatten the area. "You would risk all that to save your men?"

"Yes, if I thought it would come down to a fight. But it's never so simple." Renfield looked away for a moment, and began descending. "Please come down. Dawn is approaching."

The eastern horizon proved him right. It was faint, but the first hints of morning twilight had begun to lighten the sky. It took a minute to get over the fear he provoked, but I followed him back to the ground.

"Can you find your way back to Clarion on your own?"

I almost bridled at the question, but it was a fair one given my lack of woodcraft. Fortunately, I had GPS, and had thought to mark that cougar's den so I could avoid it. "I'll be fine, but we're at an impasse. We can't leave things as they are."

Renfield shook his head. "We have conflicting missions, which should make us enemies despite what we shared tonight."

"Sad, but true. I don't want to hurt you or your men." They've suffered enough, and will most likely suffer more once they begin their journey into the end of the twenty-first century.

"I appreciate that." He glanced toward the base. "I'm going to share a secret that might allow you to carry out your mission―"

"Shh." I held up a hand to silence him, and drew my sword as the underbrush rustled. Eyes glowing with reflected retinal light all but surrounded us.

"Run!" Renfield gave me a shove toward the trail our hidden assailants left open. "I can't stop them if you stay."

Holding my naked sword at my side, I fled. At least one Dusk Patrol soldier pursued me if the footfalls behind me were any indication, but I doubted he'd follow me into town where he'd be discovered. Using my implant, I superimposed a map over my vision to guide me. A pulsing green arrow pointed the way to Clarion. All I had to do was follow it, just like in one of the games my younger brother played.

The marker I placed at the cougar's den soon appeared, and I briefly considered leading my pursuer there. With any luck, the cat would treat him as the greater threat to her cubs and distract him. I quickly rejected the notion because I doubted the soldier behind me would stay his hand to avoid condemning animals too young to hunt on their own to a slow death by starvation.

My years of physical training had made me stronger and more agile, and had improved my endurance, but it didn't make me superhuman. Though I felt as if I could run forever, I knew better. I would have to stop and face my pursuer while I could still fight. However, this trail was no place to make a stand. The undergrowth would hem me in.

Sighting a clearing up ahead, I quickened my pace and sprinted the last few meters only to stop short as a second soldier appeared on the other side. A glance over my shoulder showed that the soldier I knew about had not flanked me. Instead, he and his comrade managed something far more devious by keeping my focus on one man while another waited for him to drive me into the trap.

Trapped between hammer and anvil, I pulled my scabbard from my belt. Its reinforcement made it a better parrying weapon than an empty hand, but I'm not ashamed to admit that I regretted not thinking to bring a second knife. Hell, I should have brought my pistol. They can't dodge bullets, can they?

The altered soldiers drew long knives, keeping them between me and their bodies while using their empty hands to protect their torsos. The ease with which they gripped their weapons and confidence with which they approached confirmed my suspicions.

This wouldn't be an easy fight, but it was a fight I could win. They were combat-trained soldiers versed in small-unit tactics, but they assumed I'd be intimidated by the appearance of a second enemy when I had been prepared to fight one.

Though I was afraid, I wouldn't let my fear defeat me. I've prevailed against nastier odds using tactics designed specifically to give lone Adversaries a fighting chance against multiple opponents. The defensive techniques I learned from Maestro gave me another advantage. If I could fight him to a draw, then I can take these guys.

Raising my weapons, I stepped forward and bared my teeth. "Come on, you sons of bitches, do you want to live forever?"

Maybe they did, or perhaps they were just smart. They didn't rush me, as I thought they might. Instead, they crept forward while keeping their distance from each other. That was bad for me; I would have to show one of them my back if I wanted to attack the other. Worse, neither was within easy reach unless I lunged, which would make me even more vulnerable.

Feinting toward the man on my left, I turned and ran the other man through, my sword piercing his shielding hand before sinking into his belly. Sensing his opportunity, the first soldier leaped forward for the kill, but I was too quick for him.

Instead of slipping his knife between two vertebrae and slicing through my spinal cord, his weapon glanced off my scabbard. The soldier tried a second thrust instead of recoiling. His knife's edge caught my side as I spun to face him while ripping my sword from his companion's body.

I wasn't wearing an armored coat. Fortunately, the soldier was sufficiently off-balance from my parry that my jacket kept him from cutting me too deeply. It hurt enough to piss me off, and would probably need proper medical attention.

"That was my favorite jacket!" He backed away as I snarled at him while holding my off hand against the wound to stop the bleeding. I still had my sheath, but I wouldn't be able to parry with it now. When he finally came for me, I slashed open his forearm to the bone.

His knife fell from the loosened grip of a hand he could no longer control, and I knocked him onto his ass with a kick to the belly. Before he could recover and take up his knife with his other hand, I kicked it into the underbrush.

As his companion gurgled behind me, my enemy reached into the flesh of his mangled forearm and pulled the severed tendon until he could join it to the other end, which he held in his teeth. Once he was satisfied, he pulled his fingers from the wound, which closed before my eyes. He bared his teeth in a murderous smile as he flexed his hand.

How had he healed so swiftly without medical attention? Could the other man heal from otherwise critical wounds as quickly? If so, I was in deep shit. Backing toward the soldier still on the ground, I crouched to pick up his knife.

The man whose arm I thought I'd ruined rushed toward me too late to stop me from slicing open his buddy's throat. Let's see him get over that. I raised my bloody, stolen knife and backed away from what I hoped was now a corpse. I pointed at my fallen enemy. "You're next."

Instead of fleeing, or attacking, he crouched by his companion and began to administer first aid. I backed away, waiting for my opportunity to escape, and he glared up at me. "You'd better run, bitch. We're going to find you, and we'll make you regret not letting us kill you here."

Shaking my head, I threw the looted knife. It flew true, and the hilt sprouted from his eye. I then pushed him onto his back and seated the knife firmly into his skull before driving my sword through his heart. I did the same for the other man, who had begun to stir.

Their threat to make me regret not letting them kill me in this clearing was unforgivable, for its implications extended far beyond simple murder. That much, either of these men might accomplish on their own if they were to catch me off-guard, or use a rifle as Renfield might have done had we not met my first night in Clarion.

Regardless, he would grieve the loss of these men. In happier times, they would probably have been his friends. I dared not permit their return to base, where they would tell the others of their defeat at my hands and rouse them to seek vengeance. Not that I wouldn't be equally buggered once their buddies came looking for them and found the corpses I left. Nothing for it but to save the coordinates and bring some irregulars to give these poor bastards a semblance of a decent burial.

I still regretted the necessity as I dressed my wound and cleaned my sword. The gash on my side was a bit deeper than expected. It hurt when I took a deep breath, and I must have lost a fair amount of blood, but I doubted it would prove life-threatening if I got to a doctor. At least I had a plausible excuse to pay Dr. Petersen a visit.
---

### This Week's Theme Music

"Force Your Way" by Nobuo Uematsu

{% youtube zev79FxRjO4 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
