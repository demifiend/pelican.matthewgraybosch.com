---
id: 2445
title: Prometheus Unbound
date: 2015-08-11T16:36:26+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=16
permalink: /2015/08/prometheus-unbound/
sw_cache_timestamp:
  - 401599
bitly_link_twitter:
  - http://bit.ly/1N0ydHy
bitly_link_facebook:
  - http://bit.ly/1N0ydHy
bitly_link_linkedIn:
  - http://bit.ly/1N0ydHy
bitly_link:
  - http://bit.ly/1N0ydHy
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Stuff I Found
tags:
  - art-deco
  - art-nouveau
  - artwork
  - cool
  - poster
  - prometheus-unbound
format: image
---
I don't really have much to say in this post; I just wanted to share this image, which I think is really cool. It was actually for a recent re-imagining of Shelley's _Prometheus Unbound_ that ran in Utah in 2013, and received [a fairly harsh review by the Utah Theatre Bloggers Association](http://utahtheatrebloggers.com/16366/not-even-hercules-could-save-prometheus-unbound).

I found it while looking for art inspired by [Percy Shelley's _Prometheus Unbound_](https://en.wikipedia.org/wiki/Prometheus_Unbound_&#40;Shelley&#41;) that I could use to whip up a fake album cover for the neo-Romantic metal concept album by Crowley's Thoth that features in my novel [_Without Bloodshed_](http://www.amazon.com/Without-Bloodshed-Starbreaker-Series-Book-ebook/dp/B00GQ0BJOO). It's reminiscent of either Art Deco or Art Nouveau, more likely the latter.

Unfortunately, I can't use it for a fake album cover. It's too recent to be in the public domain, but I love the style. Also, my Photoshop-jutsu is worthless and weak.