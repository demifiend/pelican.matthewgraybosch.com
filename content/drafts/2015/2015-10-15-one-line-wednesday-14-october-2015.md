---
id: 2192
title: 'One Line Wednesday: 14 October 2015'
date: 2015-10-15T22:15:35+00:00
excerpt: "Every Wednesday, the #1LineWed hashtag trends on Twitter. Sometimes I participate."
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - '#1LineWed'
  - challenge
  - one sentence
  - Twitter
  - writing
  - Starbreaker
---
{% include tweet.html %}

Every Wednesday, Twitter has a #1LineWed hashtag trending. The idea is to share one line, a sentence or two, that evokes interest in your story. Here are mine. They moistly come from Starbreaker stories I haven’t written yet. I’m reclaiming them from Twitter so they don’t get lost in the noise.

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Now, what could a demoness capable of calling down lightning teach me about music? <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a> <a href="https://t.co/S9GclCZDCF">https://t.co/S9GclCZDCF</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/654303001755172865">October 14, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "I’m the one who incinerated that bitch because your father Imaginos was too soft-hearted to do so. I am the ensof Thagirion." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/654300521524776960">October 14, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Claire wasn’t too keen on being part of the bridal party until her aunt revealed that it was going to be a gender-bending wedding. <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/654291654824841216">October 14, 2015</a>
  </p>
</blockquote>



## 7 October

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    I’d be a martyr, a holy icon with my humanity hidden under as many coats of whitewash as those rallying around my name needed. <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/651768555239833601">October 7, 2015</a>
  </p>
</blockquote>



## 30 September

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Choose your words with exacting care, Naomi. Your idealism has proved amusing thus far, but do not try my patience. <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a> <a href="https://twitter.com/hashtag/silentclarion?src=hash">#silentclarion</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/649275303823548417">September 30, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Even if you fucked my mother, neither you nor she could be bothered to actually do the hard part and be my parents. <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a> <a href="https://twitter.com/hashtag/silentclarion?src=hash">#silentclarion</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/649265790580092929">September 30, 2015</a>
  </p>
</blockquote>



## 12 August

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Two knives weren't enough to beat my sword." &#8211;Naomi Bradleigh in "Silent Clarion". <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631528960275275776">August 12, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "I had put you on a pedestal so tall I couldn't hear what you were actually saying to me." &#8211;Morgan Stormrider to Naomi Bradleigh <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631489158448549888">August 12, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "I created Morgan. His life follows my design. If love proves insufficient, duty will drive him." &#8211;Isaac Magnin <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631481592972709888">August 12, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    I flashed a smile at Jacqueline and offered Christine my hand. “I’m the other woman.” <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a> <a href="https://t.co/uqJyg69SAE">https://t.co/uqJyg69SAE</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631476827144503296">August 12, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "I tried aerobics once. It was the most horrible twenty minutes of my life." &#8211;Claire Ashecroft in "Without Bloodshed" <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631476194702135296">August 12, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Hal, if you got me out of a cuddle sandwich for no good reason your last words will be the lyrics to Daisy." &#8211;Claire Ashcroft <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631471585585704960">August 12, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "I'm tired of being a necessary evil." &#8211;Morgan Stormrider in "Without Bloodshed" <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a> <a href="https://twitter.com/hashtag/starbreaker?src=hash">#starbreaker</a> <a href="https://twitter.com/hashtag/available?src=hash">#available</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631470648385273856">August 12, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Did you think I'd permit the existence of a weapon I could not counter?" &#8211;Isaac Magnin from "Without Bloodshed" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a> <a href="https://twitter.com/hashtag/starbreaker?src=hash">#starbreaker</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631467496751648768">August 12, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    The cold November rain curved around Naomi Bradleigh's sword, as if fearing its edge. <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a> <a href="https://twitter.com/hashtag/GunsNRoses?src=hash">#GunsNRoses</a> <a href="https://twitter.com/hashtag/starbreaker?src=hash">#starbreaker</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/631449677720911873">August 12, 2015</a>
  </p>
</blockquote>
