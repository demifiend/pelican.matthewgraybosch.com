---
id: 1734
title: 'Check out <em>All I Am</em> by End of the Dream'
excerpt: Meet a new symphonic metal act from the Netherlands with a contralto vocalist.
header:
  image: end-of-the-dream-all-i-am.jpg
  teaser: end-of-the-dream-all-i-am.jpg
categories:
  - Music
tags:
  - contralto
  - Den Bosch
  - End of the Dream
  - gothic
  - heavy metal
  - Netherlands
  - Micky Huijsmans
  - woman-fronted
---
I just discovered a woman-fronted metal band from Den Bosch, the Netherlands via Twitter tonight. They're called **End of the Dream**, and they released their first album, _All I Am_, in March 2015. I'm listening to it now. You should check it out.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aartist%3A3XasyudrgZsb1ENK3rbZBr" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

There's some interesting guitar and keyboard work to be had in _All I Am_, but I find vocalist Micky Huijsmans' voice particularly intriguing. The lower end of her range seems deeper than that of other female metal vocalists like Tarja Turunen, Floor Jansen, Sharon Den Adel, Charlotte Wessels, Maria Brink, etc. Most women in metal are either sopranos or mezzo-sopranos. Ms. Huijsmans sounds like a contralto, which is rare and interesting.

**End of the Dream** is definitely a band I'll be following, despite not finishing the album yet. I will, of course, but the musicianship is top-notch and the production is rock solid. The lyrics are more emotional, rather than fantastical or gothic as one might expect from other woman-fronted bands.

{% include base_path %}
![End of the Dream]({{ base_path }}/images/end-of-the-dream-band.jpg)

![End of the Dream: "All I Am" (2015)]({{ base_path }}/images/end-of-the-dream-all-i-am.jpg)
