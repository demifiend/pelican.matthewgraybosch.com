---
title: "*Silent Clarion*, Track 61: &quot;Friends&quot; by Joe Satriani"
excerpt: "Check out chapter 61 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
When I returned to my senses, the black cell was gone. Instead of black pajamas, which had been comfortable and a rather stylish by comparison, I had been downgraded to a hospital gown. The prison cot was now a hospital bed. A cool breeze through open windows made the cut flowers in a vase on my bedside table sway a little.

Though I could have rung for a nurse, I left the call button untouched. It was enough to lie here, listening as the pigeons strutted and cooed along the ledge outside the windows. Someone would be along eventually.

Someone with a familiar voice had other ideas. "Oi, Naomi. I saw you open your eyes. Wake up already."

"Dammit, Jacqueline." Turning away from my friend, I pulled the blanket over my head. "Lemme sleep. It's Saturday."

"You've had plenty of sleep." She pulled them off me, exposing my bare arse to the cool air. "C'mon, Nims. Visiting hours are up in fifteen minutes. You can go back to sleep then."

"Fine. You win." Sitting up, I tied the gown closed behind me. "Where the hell am I?"

"Nightingale Memorial Hospital in Philadelphia. What the hell did you get yourself into in Clarion? I had to come all the way from London to get you out of a black site."

Not that I minded a rescue from Jackie, but it was weird. "Why you?"

A small, sidelong smile suggested it would be an interesting story, but Jackie kept it short. "Malkuth called me. Said something about how he didn't want to miss out on that date."

Looks like Mal is never going to let that promise go, that incorrigible flirt. But if his experience of humanity comes from people like me and Jacqueline, I suppose it's only to be expected. Jackie was a bad influence on me; she was probably even worse for an innocent, naive AI like Malkuth. Not that he seemed to mind being corrupted. "Was that all he told you?

A shrug from Jacqueline. "Pretty much, though he said the tip came from a guard who had a crisis of conscience after seeing that a pretty girl had been locked up alone in a Commonwealth black site under Philadelphia to sing her heart out until she collapsed. He requested anonymity out of fear of reprisals from whoever put you there."

Whoever my arse. It had been the Society that put me there. "Jackie, two Adversaries showed up to arrest me. One of them must have decided I was resisting, 'cause she hit me upside the head.

At least she had the decency to give a sympathetic wince. "Damn. And then you woke up in that cell? That couldn't have been much fun."

"It wasn't. So, when's my court martial?"

"I don't know anything about that. Maybe ask Mal?"

"Good idea." I pressed my fingertips to my ear. ｢Malkuth?｣

No response. No network connection. Dammit. Pressing the call button, I gave Jackie a sidelong glance. "Looks like I'm off the network. Somebody better have a—"

A nurse stuck her head in. "Is something wrong, Ms. Bradleigh?"

"Care to explain why I'm denied network access?" I had snapped the question before getting a good look at her. She wasn't a proper nurse, but a candy-striper. Her ID card marked her as one Jen Simmons. If she was a day over fifteen, I'd eat these flowers. "Sorry, Ms. Simmons. That was rude of me. Would you please find my attending physician and ask him to stop by?"

Simmons gave a hurried nod. "Of course, Ms. Bradleigh."

The doctor arrived twenty minutes later, which gave Jackie and I time to catch up a bit and share news. She was just telling me about some of *her* adventures when a woman softly cleared her throat. "Adversary Bradleigh? She approached the bed as Jackie pushed her chair back, and offered a slim hand. "I'm Doctor Tranh, your attending. Ms. Simmons told me you were concerned about your lack of network access."

"That's right. If I'm a prisoner awaiting trial, I'm still entitled by law to —"

Dr. Tranh nodded. "I understand, and I apologize. I ordered your implant disabled. It is a standard preventative measure for individuals who have suffered recent head injuries."

"I suggest you have my friend's implant re-enabled, as a prophylactic against an acute case of boot-in-arse syndrome."

Dammit. "Thanks, Jackie, but I don't think you're helping."

Fortunately, Dr. Tranh found Jackie's threat amusing. Or was it my embarrassment put that little smile on her face? "I can have her removed, if she's bothering you. I don't see her listed as next-of-kin."

"It's all right. She's my partner." As Dr. Tranh raised a questioning eyebrow, I clarified the relationship. Not that it was any of *her* business. "We're both Adversaries, and usually work together."

"Nice save, Nims."

Dr. Tranh looked down on Jackie. "Partner or not, visiting hours ended about ten minutes ago. You can have five more minutes, but I must insist upon you leaving afterward. My patient needs her rest."

"I just woke up."

"And I can bring have a nurse bring you dinner and a tablet so you can read a book or catch up on the news, if you like. But I want you rested for tomorrow. If the tests all go well, I can discharge you then." With that, Dr. Tranh left in a swirl of white coat and inky black hair.

Once the door was closed, Jackie was back on her feet and reaching into a bag. She produced three thick volumes of manga. One bore the title *Shotgun Exorcist*, and featured a cigar-chomping nun wielding a crucifix and a double-barreled sawed-off—presumably for situations where the power of Christ proved insufficiently compelling. "Those cheap old tablets hospitals lend out will just give you a fuckin' headache. You can read these instead. Seems Claire already had copies."

I flipped through *Shotgun Exorcist* first. It was obvious the artist was a guy; no woman would draw such outrageously proportioned female figures. "Are you talking about—"

Jackie nodded. "You met her on the maglev to New York. Seems my sister-in-law was taking her to live with her parent to get her away from me. Says I'm a bad influence."

"I think the damage is already well and truly done. Did Claire know what her mother was doing?"

A chuckle from Jackie. "No, but she raised all nine circles of hell when she figured it out. And now her mother's miffed that her little girl would rather live with me. Though Claire wasn't exactly diplomatic in making her preferences known. You should have been there, but it's a good thing I thought to record it."

Jackie showed me a handheld, and tapped a button on the screen. The video began playing, and I recognized the voice of the girl who had sat next to me instead of her mother despite the tearful anger and intermittent sniffles. "Fuck you, mum. I'm never gonna be the demure little Stepford daughter you really want, so why not just let me live with Jackie? At least she loves me the way I am."

"Claire, you stop right this instant. What would your dad say?"

"I don't have a father, you lying slag. You got drunk one night, shagged some stranger, ended up with me, and couldn't admit the only difference between you and Aunt Jackie is you were too bloody *stupid* to use birth control. So you fucking lied to me, and let me hope I'd someday get to meet a bloke who actually has no idea I even exist."

Unable to believe any eight-year-old girl would speak so harshly, or with such bitterness, I stared at Jackie. "Is Claire going to be all right?"

Jacqueline sighed. "I don't know. I should have stepped in sooner, but I thought Claire had a right to have her say. But now I don't think she and my sister Charlotte will ever reconcile. But she's currently in my hotel room, so I should go back to her."

That had to be a rough situation for Claire *and* Jackie. It was tempting to talk to my parents, since they had already raised one tomboy, but Claire would be a much harder case than I had been. She already realized that the adults in her life were all too human. "I'd offer to help, but my own future's kinda shaky right now."

"It's fine, Nims." Jackie showed me a modest ring on her left hand. "You know that vicar? We're getting married soon, and he dotes on Claire, so she'll have an uncle of sorts. Can I count on you to stand up with me?"

"Of course."

"Thanks." She drew me into an awkward hug. "And if there *is* a court martial, you can count on me. I'll testify on your behalf."

Before I could express my appreciation, the door opened. Dr. Tranh cleared her throat, and stared daggers at Jacqueline. "Better go, Jackie. We'll talk again tomorrow."

"Right." Jackie blew the doctor a kiss. "Don't get your knickers twisted, Doc. I'm leaving now. You can have your patient all to yourself now. But don't do anything *I* wouldn't do."

Dr. Tranh's expression softened as she examined me. "I sent her away as much for her own good as for yours. She's been at your side most of your stay, with only short breaks to check up on her niece and make sure she had something to eat."

"Jackie's a good friend. So, how's the prognosis? Will I live to stand trial?"

"Most likely, but there's something odd about you. I had a look at your arm, since it's only the slightest bit crooked, and I discovered that you recently broke it. Yet it's somehow already healed. Would you like to tell me what happened? As your attending physician I can offer complete confidentiality."

Rather than answer immediately, which would have resulted in me turning down the offer, I took a moment to consider it. After a while, I nodded. "It's going to be a long, somewhat complicated story. Can we have dinner brought in, first?"

---

### This Week's Theme Song

"Friends" by Joe Satriani, from *The Extremist*

{% youtube IYgbaM0uCu4 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
