---
title: "Byzantium Sucks, but Doesn't Blow"
category: The Tarnished Screen
excerpt: "Neil Jordan is at it again. Twenty years after directing *Interview with the Vampire*, he returns to the genre with *Byzantium*, based on a play by Moira Buffini."
tags:
  - Neil Jordan
  - Moira Buffini
  - Gemma Arterton
  - Saoirse Ronan
  - vampires
  - ireland
  - "2012"
  - "mother/daughter"
  - review
header:
  image: byzantium-still.jpg
  teaser: byzantium-still.jpg
date: 2015-09-14T00:00:00-04:00
---
{% include base_path %}
Neil Jordan is at it again. Twenty years after directing *Interview with the Vampire*, he returns to the genre with *Byzantium*, based on a play by Moira Buffini. It’s a quiet, brooding film centered around a pair of mother and daughter vampires portrayed respectively by Gemma Arterton and Saoirse Ronan.

Adi Robertson at *The Verge* asks, ["Do we need *another* vampire movie?"](http://www.theverge.com/2013/4/29/4282140/byzantium-review-do-we-really-need-another-vampire-movie) It’s a stupid question, though understandable considering *Twilight*. I would suggest that there’s always room for more *good* vampire movies.

This begs the question: is *Byzantium* good? It might be best if you decide for yourself, starting with the trailer. If you read *beyond* the trailer, expect spoilers. Also, expect triggers.

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/MEAGYNhAlSQ" frameborder="0" allowfullscreen></iframe>

---

I enjoyed *Byzantium*, primarily because of Ms. Ronan's performance as Eleanor, whom Jordan had the sense to introduce first. However, the movie got problematic fast once we cut to Ms. Arterton as Clara, Eleanor's mother.

I'll admit that I enjoyed the show, but Byzantium *really* plays up the fact that Clara uses her sexuality to draw out men who prey upon women; these men are Clara's preferred prey, which is fair enough. Who does Eleanor favor when she's hungry? She feeds on elderly individuals who have grown tired of life.

So, we have two vampires of a sort (more on the later) with different moral codes guiding their hunts. Clara goes after misogynistic assholes, and Eleanor is a dark angel of mercy.

We've seen this sort of thing before, at least in Clara's case. Lisbeth Salander also went after misogynistic assholes. Lestat in Anne Rice's books always preferred to feed upon society' evildoers. It let him feel like he had a legitimate role in the "savage garden" of life, and he wasn't munching on anybody whose absence would be missed.

Another problematic aspect of *Byzantium* is its use of the [Our Vampires are Different](http://tvtropes.org/pmwiki/pmwiki.php/Main/OurVampiresAreDifferent) trope. Neither Eleanor nor Clara burn in the sun, though this isn't unprecedented; Dracula in the Stoker novel just needed a hat and sunglasses to tolerate the British sunlight. Also, instead of biting victims, vampires in *Byzantium* draw blood using a thumbnail that grows and sharpens to a point when needed. Finally, becoming a vampire requires making contact with the [*genius loci*](https://en.wikipedia.org/wiki/Genius_loci) of a nameless, accursed island not found on standard maps.

![ a still from Byzantium]({{ base_path }}/images/byzantium-still.jpg)

These are all interesting notions, but one wonders: since the movie is about immortals who have spent the last two hundred years drifting from place to place as the world changes around them, but doesn't faithfully adhere to vampire lore, why make these women vampires at all? Given that *Byzantium's* primary conflict is between Clara's need to protect Eleanor by keeping their secret and Eleanor's need to reveal herself and finally create a life of her own after two centuries tagging along with her mother, I can't help but wonder if the vampire angle and the Brethren were necessary.

Despite these issues, and the fact that both Clara and Eleanor were raped by the same character, which sets them both on their respective roads to immortality, I thought *Byzantium* a solid movie. And the poster’s stylish, which never hurts.

![ Byzantium theatrical poster]({{ base_path }}/images/byzantium-poster.jpg)
