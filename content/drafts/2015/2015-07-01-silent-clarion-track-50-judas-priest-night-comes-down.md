---
title: "*Silent Clarion*, Track 50: &quot;Night Comes Down&quot; by Judas Priest"
excerpt: "Check out chapter 50 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
It'll be dark soon. Are you sure we should stay here?" Mike's unease was evident in his voice, and he was right. We couldn't afford to still be here once night fell. Though I had initially assessed the watchtower as a defensible location, I had done so from the viewpoint of an Adversary fighting human opponents. But we weren't facing regular soldiers, even regular soldiers outfitted with night-vision gear. We were facing what remained of Dusk Patrol.

Dusk Patrol's enhanced abilities come from what Dr. Petersen's research called the "asura potential" in CPMD+ individuals, which the Renfield Protocol forcibly activated. Combined with decades of training in low-light combat, they were the one unit justified in claiming ownership of the night.

But if we left, where would we go? Returning to town would put civilians at risk. Not to mention that word of Collins' death has most likely circulated by now, which would surely make us persona non grata. If we ran fast enough, would we escape the Fallen Angels' fate? Now that I had access to the Project Harker data, could we afford to retreat and wait for backup?

Speaking of backup, where the bloody fuck was the Phoenix Society's arms control unit? They should have been here already. Time to check with Malkuth, but I'd let Mike listen in so I wouldn't have to explain everything afterward. ｢You paying attention, Mal?｣

｢Had I known you'd be so high-maintenance, I wouldn't have asked you for a date.｣

｢If I had known you weren't man enough to handle me, I wouldn't have accepted. Where's that arms control unit the Society was supposed to send after I completed my survey of Fort Clarion? They'd be really handy right now.｣

｢How come? You've gotten most of Dusk Patrol to surrender. I doubt they're going to break out of jail overnight. Or do you think Robinson will let them out and sic them on you?｣

That was exactly what I suspected Robinson might do, especially if he's found out that Mike and I killed Mayor Collins. While I could prove self-defense, I doubt the Sheriff would let me live long enough to stand trial. ｢Dusk Patrol has a non-commissioned officer, Corporal Seward, who appears to be more loyal to Robinson than Renfield. Even if Reinfield were able to keep the men already in custody from joining the fray, Robinson could still make my life unnecessarily complicated. So, where's my backup?｣

｢They'll arrive at 0600 tomorrow morning. Think you can hold out until then?｣

Talk about life-or-death decisions. I can either hold out until morning, or be Dusk Patrol's latest snack. Considering the weapons we gathered, and Mike's steady gaze through his rifle's scope, I shrugged. There were worse places to make a stand, but I had grown convinced that this wasn't the best place for me to make mine. ｢What kind of tactical support can you provide?｣

｢Gevurah can get a satellite over Fort Clarion in forty-five minutes, giving you an eye in the sky. But if you leave the fort, you are not to take any catalogued equipment off-base. That ordnance is Phoenix Society property under the Arms Control and Containment Treaty of 1955.｣

Great. We could be moments away from fighting crazy super-soldiers, and Mal's blithering about a century-old treaty whose signatory nations are all history? Fuck him and his treaty sideways. ｢Given that I booby-trapped the arsenal, that treaty's worth fuck-all if Dusk Patrol gets anywhere near it. Furthermore, as an officer of the Phoenix Society, I possess authority under the terms of the ACCT to arm myself and lend Society property to civilian militia.｣

Disconnecting from Malkuth, I gave Mike an apologetic smile. "Sorry, kid. You're in the militia now."

"Least I'll get paid that way. What's the plan?"

"Still working on it." Which I was, but an idea came to mind. Since I'm not going to download the Project Harker data to this laptop, why should I stick around to babysit the transfer? Cracking open an editor, I whipped up a shell script that would create an archive of Petersen's home directory and upload it to Malkuth. For good measure, I added a line to upload another copy to one of the virtual lockers Port Royal provides as a public service. Paranoid, of course, but I wanted a copy of the evidence that the Phoenix Society couldn't touch. Just in case.

It didn't take long to do a dry run. I could have run it myself, using a terminal multiplexer in detached mode, but I had a better idea. Saving my script, I made a copy of the scheduler's configuration file, and added two lines. One would run my script. The other would clean up after me by moving the original configuration file back into place and deleting my script. Once it was done, I shut down my laptop. "We're done here. Let's bugger off while we've still got some daylight."

Mike had already packed his gear. In what seemed a token effort at honoring Malkuth's strictures, he took apart the designated marksman's rifle. Grinning, he bent the firing pin before reassembling the rifle. Without a new one, the weapon would be useless. "What about the data? And where are we going to go?"

"It's uploading now." Though I knew where to go, I hesitated. Without a warrant, the legality of my next move was questionable despite the probable cause the evidence gave me. "We're going to arrest Dr. Petersen."

"You're going to hold him hostage." Mike's tone was flat as he thrust the accusation home.

Unable to deny the nature of my tactic, I held Mike's stare until he turned away. "If you can suggest a way to survive the night that won't stain your conscience, I'd love to hear it. Otherwise, I'm going to do my bloody job and hold the good doctor accountable for his crimes. Hopefully, having him in hand will give Robinson and Dusk Patrol cause to reconsider attacking us."

"Would the Society approve?"

"That's my concern. No doubt I will face a court martial once this is over, but I'd willingly stand trial if it means exposing Project Harker and bringing Petersen and Robinson to justice." Fortunately, my voice didn't quaver and betray just how shaky a foundation my resolve rested upon.

Despite knowing it was pointless to second-guess myself, I spent the ride back to Clarion in internal debate. I was still at it when we parked in front of Dr. Petersen's house. The setting sun threw long shadows across the street. With a knock on the door, I committed myself.

The door opened silently on well-oiled hinges at my touch, and I drew my sword. There was no way Dr. Petersen would leave his door like that, even in a quiet little town like Clarion. "Stay close, and keep the shotgun ready."

"Right."

Working one room at a time, we cleared the house. Perhaps I should have ordered Mike to wait outside, but he was safer with me and this would be good training for him. It wasn't until we opened the stairwell leading to the attic that the rusted iron stink of spilled blood hit us.

At the far end of the attic, we spied Dr. Petersen sprawled on the floor. A Dusk Patrol soldier on his knees hunched over him, and a soft lapping told me everything. That soldier was feeding, and if the doctor's blood still flowed, he might not be beyond help. With a piercing cry, I threw myself forward, driving all my weight and strength behind the point of the sword I held with one hand on the hilt while carefully grasping the base of the blade with the other.

As I hoped, the soldier stood and faced me, impaling himself on my sword. It bit deep into his belly, and drove him to his knees as the blade glanced off his spine. The arterial red welled at his mouth as his entire blood supply poured into his abdominal cavity from the artery I had sliced open. He wasn't likely to cause further trouble.

Regardless, it never hurt to make sure. Kicking him onto his side, I retrieved my sword before taking his knife. An extra blade might come in handy.

There was a soft click as Mike thumbed his rifle's safety off. "Worry about the doctor. If this asshole moves, I'll blow his fucking head off."

Glad the kid stepped up, I turned to Dr. Petersen. "Doctor, can you hear me?"

"Oh, Natalie. I'm glad you're here. Or were you Nancy?" Petersen slurred the words, which combined with his confusion over my name suggested he had suffered a concussion before being bitten.

Checking him over, I found no other visible injuries. "Mike, let's have some light."

"Sure." He kept his rifle trained on the soldier as he found a switch.

Observing Dr. Petersen's eyes as the room brightened, I relaxed a little as both his pupils contacted to the same size in response to the glare of the ceiling-mounted light. Still, it was best to be careful, so I flipped Petersen the bird. "How many fingers am I holding up?"

"One. And that's a rather rude gesture for a young lady, Adversary Bradford."

"It's Bradleigh, doctor. Naomi Bradleigh. And ask your subordinate if I'm a lady."

Petersen turned his head, his eyes widening. "I'm surprised you didn't kill him."

Not about to admit that I couldn't bring myself to finish the job, I grinned at the doctor while taking the first-aid kit from my belt. "Like you, he's more useful to me as a prisoner than a carcass."

I glanced at Mike, who held his shotgun at the ready in case anybody came up the stairs, before patching Petersen's shoulder. He glanced back at me, and shook his head. "He's a doctor, ain't he? He can go heal himself."

Ignoring Mike, I finished the job and packed my kit back up. When I was done, I patted the doctor's leathery cheek. "Think you can manage to make it downstairs with us? I mean to have an intimate chat with you, and I'd rather not do it here. Besides, we need to secure your house."

"You said something about me being a prisoner. Why?"

That got a chuckle from Mike. "I'm terribly sorry, Dr. Petersen. I must have neglected to inform you that you are under arrest for crimes against humanity."

---

### This Week's Theme Song

"Night Comes Down" by Judas Priest, from *Defenders of the Faith*

{% youtube 0Rrlrje3klQ %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
