---
title: "Damnation Angels' *Bringer of Light* is Brilliant"
excerpt: These guys sound a bit like Kamelot, only better.
date: 2015-09-08T03:56:44+00:00
header:
  image: da-bol.png
  teaser: da-bol.png
categories:
  - Music
tags:
  - Damnation Angels
  - guest vocalist
  - Hard Rock Haven
  - Kamelot
  - king diamond
  - Metal Temple
  - Metallica
  - Pellek
  - power metal
  - symphonic metal
  - UK
---
_Bringer of Light_ by UK symphonic power metal act [**Damnation Angels**](http://www.damnationangels.com) is really doing it for me today with its mix of orchestral passages and metal. It reminds me of Kamelot, only the mix is cleaner and more listenable. Also, the vocalist sounds a bit better.

Standout tracks include "The Longest Day of My Life" and "Pride (The Warrior's Way)", an epic track incorporating Japanese folk elements. I suspect both will find their way into a Starbreaker playlist. Also, I strongly suspect that King Diamond sings on "I Hope" after the three-minute mark.

I recommend that you check it out if you like metal with slower, orchestral passages instead of relentless aggression.

[Hard Rock Haven](http://hardrockhaven.net/online/2013/02/damnation-angels-bringer-of-light-cd-review/) provides a favorable and much more detailed review, and also notes the similarities between Damnation Angels and Kamelot.

> _Bringer of Light_ has several things going for it, but there are three aspects that stand out in particular. The first is that under everything else, this is just a very well written, expertly executed melodic power metal album. The guitar work is very strong, the keyboards and piano are used brilliantly and never overused, and the melodies…well, calling them infectious is an understatement. They will be lodged in your head long after the album ends. You hear the basic power metal strengths most in the inter-connected “Someone Else,” “Bringer of Light” and “Shadow Symphony.” Factor in the symphonic elements and the album is that much stronger.

[Metal Temple](http://www.metal-temple.com/site/catalogues/entry/reviews/cd_3/d_2/damnation-angels.htm)&#8216;s reviewer was likewise pleased with _Bringer of Light_.

> The splendid sonic execution is more than perfect and truly smashing, like an immaculate ideal matching and even improved, as it was already trying by ROYAL HUNT / RHAPSODY and DRAGONLAND. One of the best track and most original song is “Pride (The Warrior's Way)”, with some Japanese's shamisen motif mixed with staccato riff in a full orchestral envelope until it runs into, a real catchy and unexpected chorus. What an achievement. Another way to keep the surprise and excitement, and also to break theroutine bringing a certain tension which is missing in most of Symphonic Power Metal, they choose to cover 2 impressive cut from METALLICA & X-JAPAN, Amazing !!!

## Spotify Embed

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A44rfFdFwXFCRHfSEbXtfRA" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
