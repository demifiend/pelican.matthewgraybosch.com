---
title: "*Silent Clarion*, Track 46: &quot;A Rock and a Hard Place&quot; by The Sisters of Mercy"
excerpt: "Check out chapter 46 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
｢You're under Fort Clarion? Are you serious?｣ Malkuth's reply came less than a second after I figured out how to open a secure talk session with him from Tetragrammaton. ｢Your IP address says you're at Gibson Hacker Supply in Clarion.｣

｢When they paired the mainframes, they must have set up some kind of spoofing to make all network activity appear to come from the town instead of the base.｣

｢Never mind all that. What's up? I've been trying to reach you. The Fallen Angels escort got wasted a few kilometers out of Clarion.｣

｢Haven't you been paying attention to my Witness Protocol feed? I saw what happened to them. Then I got tranked, tied up, and had to houdini my way out of a bloody meat locker.｣

That must have made an impression on Malkuth, because a minute elapsed before his next response. ｢Any idea what happened to the kids the Angels were supposed to escort?｣

｢The bastards living under Fort Clarion got 'em. Unfortunately, I've uncovered new evidence suggesting that they might not have been innocent victims. I have Brubaker with me, and he confirmed Sergeant Renfield's allegation that the local kids got in and killed several soldiers in their sleep.｣

｢He's the kid you thought might make a good Adversary. You're keeping him safe, right?｣

｢Of course I'm keeping him safe, but look, I'm down here surrounded by decades of profoundly fucked up history with Renfield and Brubaker. We've gone unnoticed thus far, but my luck won't hold out forever, and I'm using our best shot at figuring out what's really happening here.｣

｢What do you think is happening here?｣

That it was the obvious question didn't stop me from dreading it. ｢I suspect Dr. Petersen, Mayor Collins, and Sheriff Robinson of conspiring to commit multiple murders to conceal the continued existence of survivors from Dusk Patrol.｣

Damn AI kept me waiting several minutes this time, which suggested he wasn't just thinking it over, but talking with the sort of people who give me my orders. ｢Your reports and Witness Protocol data show a reasonable basis for such inferences. However, we need evidence.｣

｢I think the evidence is here on Tetragrammaton, but I need a search warrant and help escalating my privileges so I can get at the data. Oh, and a quick rundown on the Multics shell.｣ Cat's husband might have provided POSIX compatibility for users who wanted to stick with Unix-style commands rather than learn a new system, but I wouldn't be surprised if he considered it a crutch and refused to use it himself. ｢The fact that somebody bothered to maintain a constant connection between these mainframes suggests that Tetragrammaton isn't just some clever boffin's salvage project.｣

｢Agreed. Take down this warrant ID for reference.｣ Malkuth followed with the usual hexadecimal string, which I filed away using my implant. No way I'd remember it without augmented memory. ｢Be careful down there. If you're right about the two machines being linked, you might want to consider retreating to town. By the time you get back, I'll have sent you code for the privilege escalation you need. In the meantime I will see what information I can copy over should anything go wrong.｣

｢OK. Disconnecting now.｣ Once I had cleaned up after myself and logged out, I turned to Brubaker and Renfield. "Change of plans, guys. We're going to get the hell out of here, and search Tetragrammaton in town."

"Not likely." A vaguely familiar voice spoke behind us as men clicked off their rifles' safeties.

Turning to face them, I recognized their leader. He was the soldier I had left alive, but severely wounded, the night of Scott Wilson's murder. "Hello again."

My greeting didn't amuse him. "Renfield, I told you to kill your fuck toy once you were done with her."

"And when did a sergeant ever take orders from a corporal, Seward?" Renfield advanced upon the other men, heedless of the soldiers' rifles trained on him. "Adversary Bradleigh is trying to help us."

"She's a British spy."

"There is no United Queensreich of Great Britain anymore, you moron." Renfield swept a hand through the air as if to brush off an irrelevant past. "Just like there's no longer a North American Commonwealth. I keep telling you people, but you just don't get it. You're too busy listening to Sergeant Major Robinson's bullshit."

Is Renfield referring to the Sheriff? I'll have to ask him later if I get the chance. "Gentlemen, Sergeant Renfield is right. I am here to help, though matters have grown rather complicated. It seems some of you have been killing residents of Clarion, presumably because they've trespassed on Fort Clarion and -"

"They killed Jones. And Casey!" One of the soldiers interrupted me.

Another soldier joined in. "They snuck down here while we were sleeping, drove wooden stakes into their hearts, and cut off their goddamn heads. Those superstitious idiots think we're vampires."

"Look at where we are. I am aware about the experiments. Renfield told me about Project Harker and the psychological warfare that resulted from it. I've read the D Corps novels, and I understand some of you took inspiration from the books into battle."

Seward glared at Renfield. "What else did you tell her?"

"Dammit, Seward, she's on our side. She says the Phoenix Society can help us go back to the world. We don't have to live like this any longer."

"And you believe her?" Seward sneered before remembering my presence. "What else did you tell Renfield while you two were fucking? Did you tell him that there was a cure for the process that made us what we are? Did you promise him that nobody would try to figure out what made us what we are and try to use that knowledge to create more like us?"

"Naomi didn't-"

Though I appreciated Renfield speaking in my defense, I cut him off. These were charges I had to answer myself if I wanted any credibility. "Corporal Seward, I made no claims concerning the possibility of reversing the Renfield Process. I am not aware of how it actually works, since I have not yet accessed the Project Harker archives stored on the computer behind me. The process may in fact be irreversible. Even if that were not the case, I could not in good conscience promise a cure, because I'm not a scientist."

"No, you aren't." Seward glanced at the sword on my hip. "I doubt any scientist would fight as you did that night. You realize we expect you to answer for killing one of us, don't you?"

"Is the right to self-defense exclusively yours, Corporal Seward?"

"Maybe not, but even if you didn't have our blood on your hands, you tried to protect others who killed our brothers. You're protecting one of them now."

Brubaker tightened his grip on my rifle. "I spoke out. I told them to leave you alone. They wouldn't listen."

"Talk's cheap, kid. You were there, and didn't do anything to stop them. You didn't even report them to civilian authorities, did you?" Seward's words dripped venom, but I said nothing. If this was true, then Brubaker deceived me as well. If he had witnessed the murders of several Dusk Patrol soldiers in their sleep and didn't report them, then he was an accessory, and I would, perforce, arrest him.

Brubaker reddened at the accusation. "The Hell I didn't! I went straight to Sheriff Robinson. I told him everything, and he did nothing but tell me to keep quiet if I knew what was good for me."

Before anybody could say anything, I wrested my rifle from Brubaker's grasp and smacked him upside the head. "When were you planning to mention that he did nothing about it, but told you to keep quiet and used threats to secure your silence? I could have arrested that son of a bitch already on a textbook abuse-of-power charge."

Brubaker looked away, his voice barely audible. "I didn't think you could protect me. And I was right, wasn't I? You couldn't even protect my friends."

"See?" Seward spread his hands. "He doesn't care about anybody but himself and his gang. No way Corporal Robinson would tell a witness to stay quiet. We know the guy. He looks out for us."

"He turned you into mushrooms." How can Seward and the rest of Dawn Patrol not realize that the people they trusted are screwing them over? "Robinson, Petersen, and Collins keep you in the dark and feed you bullshit. They use your fear of Project Harker's secrets getting out into the world to keep you here, where every once in a while some rebellious kids nobody really cares about might get lucky and take a few of your heads."

Seward shrugged. "Maybe you're right, but why should we trust you?"

"I don't give a toss if you idiots trust me or not. You tried to kill me, remember, and threatened to do worse because I had the nerve to fight back. Regardless, you should know that the secret is already out. The Phoenix Society already knows everything about Project Harker. They just won't tell me."

One of the soldiers spat on the floor. "So we should trust you, some pale bitch nobody knows? Why should we? Because you look like a Tomcat Treat from way back when?"

Is that what Tomcat called their models? Doesn't matter. If I'm going to get these people to trust me long enough for us to get out of here without a fight, there's only one way. If I let them have Brubaker, they'd kill him. If I didn't make a show of authority against Brubaker, they'd come after both of us. Sure, I'm throwing him under a maglev again, but it might be the safest place for him right now. "Michael Brubaker, you are under arrest on the charge of abetting abuses of power on the part of one Sheriff Robinson of Clarion. You have the right to remain silent. Anything you say may be used against you at trial. You have the right to consult an attorney and have them present with you during questioning. If you cannot afford an attorney, the Phoenix Society will engage one on your behalf. You may exercise these rights at any time, with no repercussions. In addition, you have the right to humane treatment while in custody. Do you understand your rights as a person accused of a crime, Mr. Brubaker?"

He wouldn't look at me, and wouldn't answer at first. I was about to repeat my recital when he finally spoke up. "I understand."

Renfield was kind enough to offer me a zip tie, which allowed me to secure Brubaker's hands behind his back. Once I had him bound, I turned to Seward and the rest of Dusk Patrol. "Are you satisfied? Michael Brubaker will receive the due process of law. There is no need for further violence."

Seward shook his head. "You think you can make a show of arresting him in front of us, and then set him free as soon as you're out of our sight."

"That's not happening. The arrest is on record. I cannot dismiss the charges on my own, so Mr. Brubaker's fate is now up to a jury to decide."

With a sign from Corporal Seward, the remnants of Dusk Patrol surrounded me, Brubaker, and Renfield. Seward himself took a place beside me. "And we're going to make sure he faces that jury. If he doesn't, you're going to face us."

---

### This Week's Theme Song

"A Rock and a Hard Place" by The Sisters of Mercy, from *First and Last and Always*

{% youtube Zgigi5zn-r8 %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
