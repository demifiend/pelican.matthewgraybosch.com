---
title: "*Silent Clarion*, Track 53: &quot;And We Run (featuring Xzibit)&quot; by Within Temptation"
excerpt: "Check out chapter 53 of *Silent Clarion*, a Starbreaker novel by Matthew Graybosch."
categories:
    - Serials
    - Silent Clarion
tags:
    - New Adult
    - Science Fiction
    - Science Fantasy
    - Romance
    - Breakup
    - Sci-fi
    - Sci-fi Romance
    - Naomi Bradleigh
    - London
    - vampires
    - heavy metal
    - conspiracies
    - thriller
    - Starbreaker
    - Nemesea
    - No More
    - draft
    - web serial
header:
    image: silent-clarion-new-banner.jpg
    teaser: silent-clarion-new-cover.jpg
---
Gently pushing Mike aside, I checked the screens myself. His saying we had a problem was an understatement worthy of Shakespeare. Sheriff Robinson was there, and his broken arm looked like it worked just fine. He waved his hands as if conducting a sledgehammer symphony, directing his deputy to gut the interior of the shop above us.

It wasn't the subtlest method for finding a hidden passage, but it was effective. Like the man said: when in doubt, use brute force.

If the Sheriff's presence and that of his deputies wasn't sufficient cause for alarm, that prick Robinson had also brought along Dusk Patrol. One of them offered him a megaphone. Not that I needed an audio feed to figure out what he meant to say. It was most likely something along the lines of, "If we have to come down there after you, we'll beat the shit out of you and tell everyone you were resisting arrest."

Turning off the displays, I favored Dr. Petersen with my hardest stare. "When they find the way down here, they'll come in shooting. Hell, they'll probably chuck a grenade or three down the stairs first. I certainly would."

Petersen nodded. "So, you would have me reveal the underground passage to Fort Clarion."

Mike glanced upward, wincing at a clang of metal on metal. They must have opened the outer door, and were now trying to break down the inner one. If they fail to get through that 50mm slab of steel with hammers, and couldn't find welding tools that would let them cut through, I suspected their next step would involve high explosives. Being around for that might prove unpleasant. "The only other way out is to force our way through those assholes upstairs. How valuable a hostage do you think you are, Doc?"

A shrug from Petersen was the only answer Mike got. The doctor rose, winced at another ringing blow to the door upstairs, and pointed at a shelf. "You kids need to lift that out of the way."

"Take that side. On three." Mike grabbed the other side and braced himself as I counted. It was heavy, but Mike and I managed to heft it up. The question now was where to put the damn thing, but I had an idea.

With a smile that probably resembled a rictus, I cocked my head in the general direction of the clangs. "Let's put this in front of the stairs and then move some furniture to prop it up."

Minutes later, we had the barricade rigged up. In the meantime, Petersen had opened the tunnel. He must have found a cache inside, for he came out bearing three rifles. "Here. The passageway is almost twenty kilometers long, with concrete barricades for cover placed throughout. A fighting retreat is our best option."

We each took a rifle. Since I had never been down here before, these weapons weren't part of the ordnance catalog I had compiled for the arms control team coming tomorrow. "Got spare magazines?"

Petersen nodded, and handed them out. "We'll find more ammo along the way. Rations, too."

Having worked with Adversaries who once served in pre-Nationfall militaries, I shuddered at the thought. While "Meals Rejected Elsewhere" wasn't what the acronym meant, it was the one they used. Decades-old MREs would most likely kill us before the enemy could. Mike's disgusted expression suggested he harbored similar suspicions. "I think we'll pass on the rations."

For some reason that amused Dr. Petersen, for I heard a soft chuckle from the old man as he passed me and stepped into the tunnel. Picking up a small case of grenades, he pressed Mike into service. "Hold on to this. It'll come in handy when they catch up."

We ran through pitch darkness, or so it seemed until my eyes adjusted enough make out the faint blue-green glow radiating from fungi growing along the tunnel walls. The glow steadily brightened as my eyes adjusted further. A wrathful voice resembling Robinson's echoed at our tails. "Bradleigh, you bitch! Get your ass back here! You are under arrest."

Definitely Robinson. Turning back, I cupped my hands around my mouth to amplify my voice as best I could. Rather than shout a crude taunt, I sang in a high clear tone. "Sheriff Robinson thought to catch a white lark. He went home frustrated 'cause he feared the dark."

The couplet probably wouldn't work if written down, but I was more concerned with emotional impact than scansion. It got a laugh out of Mike, and an enraged shout from Robinson. Must have struck a nerve.

Heeding my instinct to duck might well have saved my life, for the burst of gunfire shredded the air above me before I heard the gunshots. Closing my eyes and averting them to avoid having the muzzle flash burned into my retinas, I returned fire.

A howl of pain mingled with rage followed us as we fled further down the tunnel. I must have hit somebody back there, but a headshot was too much to hope for.

"I got thirty men with me, Bradleigh, and they all want a taste of you. What do you think of those odds?"

"Sounds like a target-rich environment to me." To emphasize my point, I fired another long burst behind me as I kept running. An agonized shriek suggested I had scored another hit. Hopefully I blew Robinson's balls off.

I took the lead as we approached the first of the barricades, and used near-field comms to link the others in a secure relay chat. ｢Barricade ahead on the left.｣

Petersen wove around the waist-high concrete barrier and took cover behind it. Mike and I joined him for a breather after vaulting over the top. Some idiot sparked a flashlight, giving me a sense of their distance, and I rewarded their foolishness with a burst from my carbine. Despite the rage in Robinson's voice, they advanced at a methodical pace, checking every meter as if Mike and I had found time to set traps.

Too bad I didn't have any shriekers handy. ｢Doc, where's the next cache?｣

｢Two barricades ahead. You out of ammo already?｣

｢Got any Mandrakes hidden? If anything's going to slow them down…｣ I'd probably maim a few of them, but that didn't stop me from setting traps in the Fort Clarion armory. If Dusk Patrol wanted war, they were welcome to it. Likewise, for Robinson's deputies. They too had a choice between upholding the law and obeying a man who had set himself above the law, and they made the wrong choice.

｢No mines down here. Sorry to disappoint you.｣

｢No worries.｣ Indulging in a bit of reconnaissance by fire, I squeezed off another burst. The ensuing shout of enraged pain was closer, and laden with the sort of words not spoken by men who respect women. Were Jacqueline here, she might have dropped some quip about my victim kissing his sister with that mouth. ｢You ready to move on, Doc? Your troops are closer.｣

｢You could leave me behind.｣

｢We all have dreams. Wake up and move your arse.｣ The old bastard let out a weary sigh, but complied with acceptable alacrity. Though in good shape for his age, he was still much older than Mike and me. We were thus forced to hold back and match his pace, which recalled to mind the jokes about how one goes about outrunning various wild animals with a taste for human flesh. You didn't have to outrun the animal; you needed only to outrun your companion.

We stopped again at the next barricade. Our pursuers had gained ground, and I needed to do something about that. Fortunately, I had an idea. ｢Mike, give me one of those stun grenades.｣

He pressed two into my hand, and squinted his eyes shut as he covered his ears. Petersen did the same as I pulled the pin and hurled it back the way we had come. The fuse ran long enough for me to hear it skitter across the floor as I took cover. Despite my distance, the blast was still uncomfortably loud, though not as bad as my rifle when I followed up by emptying my magazine in short bursts.

Despite my liberal use of gunfire, nobody on Robinson's side had fired back. I figured some return fire would be in order as I ducked behind the barrier again to reload, but instead of shots or a grenade hurled my way, all I got was more of the Sheriff's raving. "This tunnel's gonna be your grave, Naomi! You hear me? Your fucking grave!"

Promises, promises. If that fuckwit had the ability or the nerve to make good on any of his threats, he would have done so already. But he either didn't bring firearms with him, or some factor unknown to me prevented him from returning fire thus far. I was about to reward his cheap talk with action when a gunshot rang out from Robinson's direction. Five more followed. Only a revolver, most likely, but it suggested the Sheriff had a pair after all. I threw the other stun grenade Mike had given me just to show Robinson I still cared. ｢Come on. Can you still run, doc, or do I have to carry you piggyback?｣

｢I can manage another five hundred meters. After that, you won't have to worry about me.｣

Rather than waste time asking Petersen what he meant, I ran and trusted the men to follow. Follow they did, so I was first to behold what awaited us. I couldn't believe what I saw at first, and dismissed it as a wishful thinking or a hallucination born of eyestrain. Petersen pulled ahead of me as I slowed to a surprised stop, climbed into the jeep, and started it up. Squinting against the sudden radiance of the taillights, I jumped in. ｢Why didn't you tell me there was a bloody jeep down here?｣

｢Wasn't sure if still was.｣ Petersen gunned the engine as Mike clambered aboard and sat beside me in the back. ｢Wasn't even sure if it had gas after all the round trips hauling parts of Tetragrammaton 0, but looks like we got a quarter tank left.｣

The jeep's engine was loud, but not so loud I couldn't hear Robinson yelling behind me. Something about how wheels weren't going to save me from whatever the hell it was he fantasized about doing to me. Rather than bleat over the engine, I turned in my seat and saluted with an upraised middle finger.

---

### This Week's Theme Song

"And We Run (featuring Xzibit)" by Within Temptation, from *Hydra*

{% youtube awvqIi427_A %}

---

Want to know what happens next? Check out [*Silent Clarion* by Matthew Graybosch](/books/starbreaker/silent-clarion/). Thanks for reading!
