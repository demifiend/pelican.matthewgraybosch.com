---
id: 2182
title: 'One Line Wednesday: 28 October 2015'
excerpt: "Every Wednesday, the #1LineWed hashtag trends on Twitter. Sometimes I participate."
date: 2015-10-29T08:30:16+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - '#1LineWed'
  - challenge
  - one sentence
  - Twitter
  - writing
  - Starbreaker
---
{% include tweet.html %}

Every Wednesday, Twitter has a #1LineWed hashtag trending. The idea is to share one line, a sentence or two, that evokes interest in your story. Here are mine. They moistly come from Starbreaker stories I haven't written yet. I'm reclaiming them from Twitter so they don't get lost in the noise.

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Why the subterfuge, Imaginos? Why the staged murder? Why all these conspiraces? If you needed our help, why didn't you just ask?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659458257589784576">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Don't be obtuse, Adversary. Sabaoth is not humanity's only enemy. If you would be free, my companions and I must also fall." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659460993567748096">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Nakajima Chihiro hoped letting Morgan order dinner would be the last thing she'd regret. There was too much, even with the cat. <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659457362701504512">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Hey, Thagirion? Instead of complaining that the world's in the hands of metalheads and gamers, just be glad we're not hobbits." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659439232809902080">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    In the silence following Claire's announcement, Ashtoreth blinked away and returned with a jug of tequila. "Brain bleach, anyone?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659436462497275905">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Claire turned from the Morgan's feed as Imaginos staggered backward. "Oi, Naomi. Your boyfriend just gave your dad the shocker." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659431179104493568">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "What do you hope to do to me bare-handed that you couldn't with rifle and sword, Adversary? I am beyond human weapons." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659427875490287617">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "So, rather than be directly presumptuous, you'd rather be passive-aggressive? Thank God you found me a foster family." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659425191710302208">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "You're almost 40, Naomi. Rather than tell you who to date, I would simply have men of whom I disapproved&#8230; eliminated." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659424958398005248">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "You were my first, remember? I didn't expect to be your last when I was a kid, and I don't expect to be your last now." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659390160820424704">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    His first answer was a searing kiss. "If that happens, I'll record a cover of Sinatra's 'Thanks For the Memories'." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659387660251865090">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Morgan had a point, but Naomi wasn't about to yield. "How do you know I won't get tired of you? I am a serial monogamist." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659387304935563265">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Put aside your anger and think. If I dumped Christabel for your sake, what would stop me from better-dealing YOU later on?" <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659376921009115137">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Don't flatter yourself, Adversary. There's no prophecy. Of 666 asura emulators, you're the last and the only one worth a damn." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659374095965036544">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a> Claire gave Morgan a lingering look before winking at Naomi and whispering. "He's got a great arse. Want to borrow my strap-on?"
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659369878839808000">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a> "If you made me get out of a cuddle sandwich to deal with shenanigans, Hal, your swan song will be Daisy." WITHOUT BLOODSHED
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659365517447307265">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Had the sorcerer-scientist just been another dirty executive, Morgan would have happily let him be somebody else's problem. <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659361369175801856">October 28, 2015</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Damn it, Morgan, why can't you realize Imaginos is just manipulating you? He's playing you like you play a guitar." <a href="https://twitter.com/hashtag/1lineWed?src=hash">#1lineWed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/659352428266037248">October 28, 2015</a>
  </p>
</blockquote>
