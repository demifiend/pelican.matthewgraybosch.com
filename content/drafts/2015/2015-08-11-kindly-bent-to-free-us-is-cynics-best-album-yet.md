---
title: "*Kindly Bent to Free Us* is Cynic's best album yet"
except: Technical Death Metal Nirvana
date: 2015-08-11T03:49:48+00:00
header:
  image: cynic-kindly-bent.jpg
  teaser: cynic-kindly-bent.jpg
categories:
  - Music
tags:
  - Cynic
  - fusion
  - jazz
  - Kindly Bent to Free Us
  - progressive metal
  - technical death metal
---
If somebody asked me if it was possible to find a heavy metal band that resembled John McLaughlin's **Mahavishnu Orchestra**, the first band I'd point to would be **Cynic**, and I would use their 2014 album _Kindly Bent to Free Us_ as my justification.

While their 2011 comeback album _Traced In Air_ was both a follow-up to 1993's _Focus_ and a harbinger of things to come, _Kindly Bent to Free Us_ contains almost no traces of the the inaccessible elements of technical death metal prevalent in _Focus_. Instead, KBtFU is a gentle, almost meditative album&mdash;the sort Buddha might fire up when he wanted to rock out.

Hell, even my father likes it, and he never got into heavy metal beyond classic Deep Purple.

## Spotify Embed

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A0L9q210Dguhq0nmGlCe1hR" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
