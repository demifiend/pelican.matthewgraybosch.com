---
title: "Gloryhammer &mdash; *Space 1992: Rise of the Chaos Wizards*"
excerpt: "Tired of listening to Bolt Thrower while playing Warhammer? Try this instead."
header:
  image: Gloryhammer_-_Space_1992.jpg
  teaser: Gloryhammer_-_Space_1992.jpg
categories:
  - Music
tags:
  - 2015
  - Alestorm
  - Gloryhammer
  - heroic fantasy power metal
  - music
  - power metal
  - Scotland
  - "Space 1992: Rise of the Chaos Wizards"
  - space metal
  - Warhammer 40K
---
Some outfit called [Gloryhammer](http://www.gloryhammer.com) that bills themselves as doing _heroic fantasy power metal_ showed up in my new releases playlist this afternoon with an album called _Space 1992: Rise of the Chaos Wizards_.

I had never heard of these guys before, but since I'm using Spotify I figured it wouldn't hurt to give 'em a listen. I didn't know at the time that they were lead by Christopher Bowes of Alestorm.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A3fpspMTkc6HAF9flYAiFJZ" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

---

Wait a minute. When was 1992 the future? Oh, right, this is some kind of whacked-out concept album. And there's a video with some dude in what looks like Green Lantern cosplay gone terribly wrong.

<iframe width="563" height="337" src="https://www.youtube.com/embed/YGV6bCTMM5w" frameborder="0" allowfullscreen></iframe>

---

It's certainly a listenable album, with lots of crunchy guitar work and some solid orchestration. The story presented by the lyrics aren't any more nonsensical than any of Rhapsody of Fire's narratives, or Ayreon's, though I suspect Bowes takes his tale of Scottish heroes _in Spaaaaaaaaaace_ far less seriously. You don't have to be a Warhammer 40K fan to enjoy the latest Gloryhammer album, though I suspect it helps.

So put on your kilt, let your hair down, and rock out already. :metal:
