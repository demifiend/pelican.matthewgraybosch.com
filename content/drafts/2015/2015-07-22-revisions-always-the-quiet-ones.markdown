---

title: "Revisions: Always the Quiet Ones"
modified:
categories: starbreaker
excerpt: "I just finished Episode 2 of the Kindle serial for Silent Clarion: Always the Quiet Ones. But I'm not done yet."
tags: [silent-clarion, progress, kindle, serial]
cover: silent-clarion-new-banner.jpg
date: 2015-07-22T21:01:47-04:00
---
It was a bit of a slog, but I managed to complete all of the recommended revisions for *Silent Clarion: Always the Quiet Ones*, Episode 2 of my Kindle serial. There were about 300, all minor, but I'd be doing my readers a disservice if I gave any of them short shrift.

It helped that I found yet *another* new band. I know I was listening to Annihilator yesterday, but while poking around I found a Swedish alt-metal band called Machinae Supremacy, which incorporates chiptunes using the SID chip of Commodore 64 fame into their music.

Here's their most recent album.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A4SVumwgabl7YbFnnkKftgR" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

Tomorrow I have a choice. Either get back to writing Chapter 56, or put together the draft for Episode 3 of the Kindle serial: *Life is Short and Love is Over*.

Fans of The Sisters of Mercy might suspect a reference, and they would be right.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A0cddvowG9DCT9VeZxRlPJm" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

But there are more installments to come, and more to be named. Stay tuned.
