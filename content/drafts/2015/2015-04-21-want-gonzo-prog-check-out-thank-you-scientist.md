---
id: 2212
title: Want Gonzo Prog? Check out THANK YOU SCIENTIST
excerpt: Since Frank Zappa's been dead a while, these guys will have to do.
date: 2015-04-21T20:47:31+00:00
header:
  image: thank-you-scientist.jpg
  teaser: thank-you-scientist.jpg
categories:
  - Music
tags:
  - fusion
  - gonzo
  - jazz
  - Maps of Non-Existent Places
  - The Perils of Time Travel
  - progressive rock
  - rock
  - Thank You Scientist
---
{% include base_path %}
In the beginning, there was this band from New York called [Coheed and Cambria](http://www.coheedandcambria.com/cc/), and they were badass. This post is actually about a different band called **Thank You Scientist**, but context matters.

[!["Coheed and Cambria by Harry Nesbitt"]({{base_path}}/images/coheed_wallpaper.png)](http://www.harrynesbitt.com/illustration/coheed-and-cambria/)

All of their albums were concept albums forming a single unified and profoundly fucked up continuity set in a universe called Heaven's Fence, and came with comic books and other supplementary media to further flesh out the story of Coheed, Cambria, their son Claudio, and their fight against the Supreme Tri-Mage Wilhelm Ryan. There was even a writer who was somehow creating all the rest of this weird shit &#8211; and his muse was a talking bicycle.

Yeah.

So, one day I decided to dick around with Spotify's "related artist" page for Coheed and Cambria, and one of the first bands on the list was an outfit called [Thank You Scientist](http://www.thankyouscientist.net/). They're from New Jersey (not that there's anything wrong with that), and they started in 2011 with an EP called _The Perils of Time Travel_.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A6uNiAPq3caw4opTgD7wwr4" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

This EP displays Thank You Scientist's varied style. As the band's website puts it, they're&#8230;

> Frank Zappa for the indie generation? Incubus jamming with the Mahavishnu Orchestra? Mr. Bungle and Steely Dan joining forces to fight Godzilla? King Crimson and the Brecker Brothers serenading you at your bedside? It all seems so strange on paper, yet it’s sure to satisfy your ears in the best of ways.

That's how they sell their 2012 debut LP, _Maps of Non-Existent Places_, whose title alone was enough to sell me. Better still, they live up to some of their hype. I've never listened to Incubus, Mr. Bungle, the Brecker Brothers, or Steely Dan, but Frank Zappa, the Mahavishnu Orchestra, and King Crimson?

<img src="http://i2.wp.com/cdn.meme.am/instances/500x/59021222.jpg?w=840" alt="I'm there, dude." data-recalc-dims="1" />

Seriously, this album is fucking awesome, and once I put it on, I tend to just let it loop while I'm coding or writing. It's good driving music, too.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A3e0QZg478r71EiIcDSaR0v" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

And it's _definitely_ reminiscent of Frank Zappa, especially in tracks like "A Salesman's Guide to Non-Existence" and "Blood on the Radio".

Unfortunately, neither of these tracks are available on Soundcloud if you want to try out _Maps of Non-Existent Places_, but don't want to use services like Spotify. Instead, there's "My Famed Disappearing Act", which serves as the album's closer.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A7wXFyf91scZr0zyxiy3v1E" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

I strongly recommend giving Thank You Scientist a listen. They might prove your new favorite band. They're one of mine.
