---
id: 1632
title: I Like Final Fantasy XIV, But I Might Quit
date: 2015-09-24T14:52:55+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=1632
permalink: /2015/09/i-like-final-fantasy-xiv-but-i-might-quit/
sw_cache_timestamp:
  - 401684
bitly_link:
  - http://bit.ly/1PiUqmG
bitly_link_twitter:
  - http://bit.ly/1PiUqmG
bitly_link_facebook:
  - http://bit.ly/1PiUqmG
bitly_link_linkedIn:
  - http://bit.ly/1PiUqmG
bitly_link_tumblr:
  - http://bit.ly/1PiUqmG
bitly_link_reddit:
  - http://bit.ly/1PiUqmG
bitly_link_stumbleupon:
  - http://bit.ly/1PiUqmG
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Games
tags:
  - a-realm-reborn
  - balance
  - ffxiv
  - final fantasy
  - first world problems
  - mmorpg
  - obsession
  - quitting
  - time
---
Last week, I decided to take advantage of the fact that the PlayStation Network had the vanilla edition of _Final Fantasy XIV: A Realm Reborn_ on sale for ten bucks. I knew it was a MMORPG, but since my wife was away and I was jonesing for a new RPG, I decided to go for it anyway. I didn't expect to like it, but I did. I might quit anyway, and I'll explain why.

## Update: 9 March 2016

I _did_ quit a week or so after posting this, but never got around to updating the post. Oops. 🙂

## My Experience with Final Fantasy XIV

First, however, let me describe my experience. Installing the game from the PSN took several hours, because of all the content that needs to be downloaded. I might have avoided this by buying physical media, but this gave me time for housework, cuddling kitties, and research.

You might scoff at the notion of my researching a videogame, butI had no idea what I was getting into since I'm a complete MMORPG newbie. All I knew were the horror stories concerning fanatical _EverQuest_ and _World of Warcraft_ players neglecting every other aspect of their lives to get more game time. The closest I had ever come to getting involved in games like this was back when BioWare's _Neverwinter Nights_ was still popular and had an active player community.

I had some learning to do. My first step was to subscribe to [r/ffxiv on Reddit](https://www.reddit.com/r/ffxiv/), which has links to good information when people aren't griping about the developers buggering off for a vacation instead of releasing a patch on the _Heavensward_ expansion.

There, I learned about the game's classes and major roles, and decided that the Arcanist class would be a good starting point. It gets to summon pets, do healing, and deal damage over time via spells like Bio and Miasma. It also gets some decent cross-class skills like Aero, Protect, Surecast, and Quelling Strike. Basically, the Arcanist is a self-sufficient adventurer &#8212; ideal for unsociable gamers like me.

Once the game was installed, it was time to create a toon. Choosing the Primal (NA) data center, I took a slot on the Behemoth server since I didn't have friends playing elsewhere. I started with a human dude, being one myself, called him "Matthew Loveless", chose Arcanist as my starting class, and sat back for the opening cinematics.

The initial quests are intended primarily as an introduction to the FFXIV setting. You start by exploring your toon's hometown (Limsa Lominsa in my case) and its vicinity. Because you're an adventurer fresh off the boat, nobody knows who the hell you are. You have to make a name for yourself before things get _interesting_.

## Playing Well With Others

Once you've made a name for yourself in the main story and guild quests and get sent out to represent your city-state's leaders to the leaders of Eorzea's other city-states, you'll soon discover that the _really_ interesting (not to mention lucrative in terms of XP, loot, and money) content requires that you join parties. I knew this would happen, and expected it to be a royal pain in the ass. As I mentioned before, I'm not a sociable gamer. Furthermore, chat is a royal pain in the ass on the PS4, since you're stuck with a virtual keyboard if you don't have a USB/Bluetooth keyboard hooked up.

Fortunately, _Final Fantasy XIV_ doesn't require you to find a slot in a party yourself or form your own party before entering dungeons. You _can_ if you want to, or just use the Duty Finder after selecting a dungeon or a "roulette". The Duty Finder will automatically match you with other players based on your class' role in a party.

As an Arcanist, I play the Damager (DPS/DOT) role. I can heal myself or other players, and one of the pets I can summon can draw the enemy's attention, but Tank and Healer are auxiliary roles for my toon's primary class.

If you don't know that means, here's the deal. MMORPG parties and raids tend to have three major roles:

  * **Tanks** draw the enemy's attention (aggro) and withstand enemy attacks.
  * **Healers** keep the **tank** alive, provide support, and heal **damagers** when they screw up and take hits.
  * **Damagers** kill enemies as quickly as possible using burst damage or damage-over-time skills.

Because _Final Fantasy XIV_ is a MMORPG with a heavy emphasis on its single-player story, no class is "pure" in that it only gets skills suited for a particular party role. Otherwise, most players would have trouble getting to level 10, when the first party-oriented content (Guildhests) becomes available. Regardless, it's important to know your primary role and play it properly when playing with others.

Knowing this ahead of time, I do the best I can in every dungeon run. Other players have noticed, because I've collected almost twenty commendations according to my character screen's Reputation tab. Every player in a party can award commendations; I usually give mine to the tank or the healer, since them doing their job lets me do mine.

I've even been asked to join a few Free Companies (raiding guilds created and run by players). I eventually joined the Shinra Corporation, mainly because neither the Returners, SeeD, nor Operation Mindcrime seemed to be recruiting. 🙂

## A Better Game Than I Expected&#8230;

As I said earlier, I didn't expect much from _Final Fantasy XIV_. I figured it would be a grind fest where it takes days of gameplay just to level up. I was wrong. I've had little trouble developing my toon after a week and a half of gameplay; I got his main Archanist class to 30 last night, and leveled Thaumaturge to 15. After that, I completed "The Austerities of Flame" and earned the Summoner job, which is an improved version of Arcanist. If I wanted to level up Conjuror, I could also earn the Scholar job.

The players for the most part are either friendly or content to leave me alone while they do their own thing. The only annoying people I've seen so far are the gil sellers, who have a nasty habit of spamming players. You'd think Square Enix would would be able to monitor chat and deal with these idiot, but I guess it would take a massive player exodus for that to happen.

The arcanist guild's quest line had an interesting little quest line whose resolution I won't spoil for you, but it involves helping a young woman bring a pirate who had enslaved her to justice. The main story quest is also getting interesting, since the story seems to be setting up a major revelation involving an ambitious officer of the Garlean Empire and the Primals, elemental forces that appear as destructive gods when summoned by those desperate for their aid.

The only negative experience I had involved a dungeon run gone bad. I was in the [Tam-Tara Deepcroft](http://ffxiv.consolegameswiki.com/wiki/Tam-Tara_Deepcroft), a dungeon accessible via the main story quest at level 16. Everything went smoothly until we reached the boss, Galvanth the Dominator. The healer got herself killed, the tank went full [Leeroy Jenkins](https://www.youtube.com/watch?v=hooKVstzbz0), and the other damager wasn't paying attention.

Guess who was left facing a pissed off dungeon boss: me. Sure, I could have just let the boss kill my toon, but I don't like to lose a fight if there's any chance I can win. So I used every trick I knew. I used my summons as a tank, healed it when not slapping damage-over-time effects on the boss, and slowly wore it down.

I figured I just had to hold out for a minute until the other guys got back. _They never returned_. They never even used chat. It's as if they had been booted out of the game. So I ended up soloing a dungeon boss.

## &#8230;But I Might Quit Anyway

Even my one not-so-great experience of the game was kinda badass by gaming standards, but you know what? I might quit _Final Fantasy XIV_ anyway. It's not the game. It's me.

You see, I tend to be obsessive. Why else do you think I worked on [Starbreaker](http://www.matthewgraybosch.com/category/starbreaker/) for twenty years? That's precisely the problem. Games like this are tailor-made for people like me. There's always another quest, another level to reach, some new piece of equipment to grind for.

It would be too easy to let the game eclipse the rest of my life. Maybe it's because [my wife is in Australia until next month](http://www.matthewgraybosch.com/2015/08/24/my-wife-left-me-last-week/) and I don't really have much else going on, but I've hardly written anything except on my lunch breaks at work. I've been staying up too late. I've been eating out of a microwave instead of cooking.

All of that might have been OK in my early twenties before I met my wife, but I can't afford to be like that now when I have Catherine to consider. Not when I have to finish the Kindle serial for [_Silent Clarion_](http://www.matthewgraybosch.com/category/starbreaker/silent-clarion/) and publish the full book.

Nor is being obsessed with a MMORPG conducive to writing more Starbreaker novels like _Nightmare Sequencer_, _Edge of Thorns_, a second edition of _Without Bloodshed_, _Blackened Phoenix_, _Heartless Savior_, and _A Tyranny of Angels_. Also, I keep teasing readers with hints of what went down during Nationfall. I'm going to have to tell _that_ story as well. Besides I have a day job that I literally _can't_ afford to lose even though I hate it.

So, I will probably stop playing _Final Fantasy XIV_. If they had made a single-player version of _A Realm Reborn_, I'd have no trouble playing through the story with my wife at an hour or two a night and putting it aside when it was over. But I don't think I can with FFXIV, even this is the epitome of [First World Problems](http://knowyourmeme.com/memes/first-world-problems). Therefore, while I enjoyed my virtual stay in Eorzea, I'm not sticking around. The financial cost of doing so is bearable, but the opportunity cost isn't. My life is hardly enviable, but I'm not willing to fuck it up over a MMORPG.