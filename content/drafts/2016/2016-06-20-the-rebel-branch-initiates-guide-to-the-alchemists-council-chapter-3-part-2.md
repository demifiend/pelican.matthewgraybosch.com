---
title: "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 2"
excerpt: "Welcome back to my readers' guide to *The Alchemists' Council* by Cynthea Masson, with guest commentary by Eric Higby. This post covers pages 148-156."
header:
  image: bee-649952_1280.jpg
  teaser: bee-649952_1280.jpg
  caption: "[Source: Pixabay](https://pixabay.com/en/bee-flying-flower-insect-fly-649952/)"
coauthor: ehigby
toc: true
categories:
  - Books
  - Longform
tags:
  - Cynthea Masson
  - The Alchemists' Council
  - ECW Press
  - Canada
  - 2016
  - literary fantasy
  - intrigue
  - politics
  - alchemy
  - environmentalism
  - intention
  - ego
  - Buddhism
  - urban fantasy
  - portal fantasy
  - commentary
  - read-through
  - read-along
  - reader's guide
  - Eric Higby
  - Chapter 3
  - Part 2
  - Kabbalah
  - Qliphoth
  - the Left-Hand Path
---
The length of these posts are probably getting just a bit ridiculous; the [first half of my analysis for Chapter 3]({% post_url 2016-06-17-the-rebel-branch-initiates-guide-to-the-alchemists-council-chapter-3-part-1 %}) was as long as [Chapter 1]({% post_url 2016-05-23-the-rebel-branch-initiates-guide-to-the-alchemists-council-chapter-1 %}) and [Chapter 2]({% post_url 2016-06-17-the-rebel-branch-initiates-guide-to-the-alchemists-council-chapter-3-part-1 %}), so let's try a different approach. To go easier on everybody, let's try one post per scene. Don't worry; when we've gone through the whole book I'll find a way to offer the Rebel Branch Initiates' Guide as a single ebook.

In the meantime, last Friday's post covered about half a dozen topics from [the first half of Chapter 3]({% post_url 2016-06-17-the-rebel-branch-initiates-guide-to-the-alchemists-council-chapter-3-part-1 %}). You might at least want to review Eric Higby's summary of the chapter for context.

* Lead Into Gold: The "Hello World" of Alchemy
* The Conjunction of Senior Magistrate Tesu
* Unintended Consequences of Stalking
* Sadira's Rebellious Impulses
* Kalina's Disappearing Act
* Jaden's Distrust and the SNAFU Principle
* The Rebel Branch: Walking the Left-Hand Path
* Sadira's Doubts

In today's post, we're covering the following topics.

* Jaden Tastes the Dragon's Blood

### Update for 6/21

Turns out I fail at reading comprehension, I was misspelling Draecen's name throughout this post. I've since corrected this.

**THE ALCHEMISTS' COUNCIL FORBIDS YOU TO READ ANY FURTHER.**

**THE REBEL BRANCH ENCOURAGES YOU TO CONTINUE, BUT THE CHOICE IS YOURS...**

### Jaden Tastes the Dragon's Blood

Getting back to Jaden, we find that Kalina appears to be missing. Jaden didn't see her at lunch. Linden made no mention of her absence in his afternoon class, and skipped over her name when checking attendance. Even Zelkova, from Kalina's quarto of Senior Initiates, didn't realize she was missing.

Jaden's smart enough to smell a rat. She's also smart enough to stop asking questions, since she would eventually have to explain her curiosity.

However, after dinner she notices a flashing red light coming from the woods of the Council grounds. The light repeats, and settles into a rhythmic pattern that Jaden likens to a code. So, what does she do? She sneaks out of the residence hall and follows the light.

Once she gets to the willow tree, she finds Kalina, who asks Jaden to follow her and quickly, since they "don't have much time" (p. 150). Jaden initially resists, and asks Kalina how she knows that:

1. Jaden would have been the one to respond to her signal.
2. Others wouldn't have seen it and come to investigate.

According to Kalina, the light frequencies were alchemically keyed to Jaden's elemental essence so that only she could see them. This allays Jaden's concerns enough for curiosity to take over, and Jaden returns with Kalina to the cliff face where a man in blue robes awaits.

Is it the *same* man in blue Jaden saw earlier this chapter? I think so, because it doesn't make sense for there to be *two* men in blue robes appearing at the cliff face unless *The Alchemists' Council* suddenly became Middle-Earth fanfic and Jaden just met the two lost Blue Wizards who went east of Mordor and were never heard from again.

I think what's really going on is **Chekhov's Gun**, named for Russian dramatist Anton Chekhov, who expressed it as follows:

> Remove everything that has no relevance to the story. If you say in the first chapter that there's a rifle hanging on the wall, in the second or third chapter it absolutely must go off. If it's not going to be fired, it shouldn't be hanging there.

Given the extremely low probability of Cynthea Masson being ignorant of this principle, I think it makes sense to assume despite the continuing lack of *explicit* textual evidence that the man in blue robes we're seeing in this scene is the same as in Jaden's last scene. Likewise, I think Kalina was the one arguing with Blue Robes earlier. But I could be wrong, and we'll just have to see how things turn out later on. Just keep in mind that there might be two alchemists in blue.

But let's call *this* blue-robed alchemist by his true name, since Kalina is kind enough to finally introduce him. His name is Dracaen. Jaden isn't sure if Dracaen is the same blue-robed man she had seen disappear last time, but I think it's likely for the reasons I explained above.

Naturally, Dracaen immediately requests Jaden's trust. When Jaden protests, Dracaen insists because, "Otherwise, we will repeat this perpetually."

And he isn't talking about eternal recurrence. Rather, he's referring to a prior meeting that Jaden doesn't remember. Likewise, without intervention, Jaden won't remember this meeting later on:

> "We have met in the past and we will meet in the future, but you will know me only in the present. I exist here. Elsewhere, I am erased." (p. 151)

Seems like Dracaen has been a very naughty alchemist, at least as far as the Council's concerned. Speaking of the Council, Kalina asks Jaden to come with her and Dracaen, because it "isn't safe here".

Jaden complies, and finds herself in a cave hung with wooden wind chimes. Dracaen and Kalina invite her to sit at their table, and Dracaen serves her a draft of "Dragon's Blood", an alchemical cocktail of spiced wine infused with essence of the Dragonblood Stone.

And what is the Dragonblood Stone? It is what the Alchemists' Council calls the "Flaw in the Stone". In Council dimension, the Flaw is almost impossible to see, but in Dracaen's cave it outshines the Lapis. Though the Council would love to eradicate the Flaw, it is what allows free will within Council dimension, the negative space Dracaen and the Rebel Branch inhabit, and the outside world.

Getting back to the Dragon's Blood: drinking it will allow Jaden to remember things she had previously forgotten, but the effect will only last a few hours once she returns to Lapidarian proximity because the Stone and the Flaw vie for supremacy just as the Council and the Rebel Branch do. As above, so below.

When Jaden drinks the Dragon's Blood, she does indeed recall having met Dracaen before, though she doesn't remember him by name. Instead at the time he was just a man who shared his umbrella with her as she stood in the rain waiting for a bus. He gave her a small red gemstone as a good luck charm, warning her to keep it with her at all times.

If you guessed that the stone was a fragment of the Dragonblood Stone, have yourself a cookie. Dracaen and the other Rebels knew Jaden was to be the next Initiate. By giving her that fragment, they hoped she would keep it and thus bring it with her into Council dimension, since keeping even a splinter of the Dragonblood stone there can counter Lapidarian memory loss after consuming the Dragon's Blood tonic.

Not that Jaden knew any of this at the time, or that it would have done her much good if she did. The time wasn't right until now. Now, however, Dracaen insists that Jaden keep the stone on her at all times lest she forget about him again, because she must willingly *choose* alchemical proximity to the Dragonblood Stone. Unlike the Lapis, Dragonblood proximity cannot be forced on people.

Kalina, however, offers a different warning. If Jaden is caught with the stone, she'll be branded a Rebel. And that's how the cat got out of the bag: at Jaden's demand, Dracaen reveals himself as High Azoth of the Rebel Branch of the Alchemists' Council, a carrier of the Dragonblood pendant for four hundred and forty-three years, and the restorer of the Flaw during the Third Rebellion.

His current misson? "The recruitment of Jaden." That's what *I* call leading from the front.

Stay tuned for "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 3". If you want more of the Rebel Branch Initiates' Guide, [check out the main page](/commentaries/the-rebel-branch-initiates-guide-to-the-alchemists-council/).
