---
id: 3748
title: Watch Me Fail at Dark Souls III
date: 2016-04-15T18:40:49+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3748
permalink: /2016/04/watch-me-fail-at-dark-souls-iii/
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"31b110e5dc26";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:75:"https://medium.com/@MGraybosch/watch-me-fail-at-dark-souls-iii-31b110e5dc26";}'
categories:
  - Dark Souls III
  - Games
tags:
  - Dark Souls III
  - EddieVanHelsing
  - ps4
  - PVE
  - PVP
  - streaming
  - twitch
---
I've decided to start streaming as I play Dark Souls III. Watch me fail. Watch me persevere. Watch me occasionally do epic shit.

Or better yet, fire up your PS4 and look for me. My handle is EddieVanHelsing.



<a href="https://www.twitch.tv/starbreakerauthor?tt_medium=live_embed&#038;tt_content=text_link" style="padding:2px 0px 4px; display:block; width:345px; font-weight:normal; font-size:10px;text-decoration:underline;">Watch live video from starbreakerauthor on www.twitch.tv</a>

If you find my stream amusing or useful, please support me by checking out (and possibly buying) my novel _Without Bloodshed_.