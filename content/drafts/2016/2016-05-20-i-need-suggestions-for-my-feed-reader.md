---
title: "I Need Suggestions For My Feed Reader"
excerpt: "Looking to promote your website or blog? I'm looking for sites to add to my RSS feed reader. If you've got something good, tell me in the comments."
header:
  image: moar-kitten-because-moar.jpg
  teaser: moar-kitten-because-moar.jpg
categories:
  - Personal
  - Website
tags:
  - blogs
  - crowdsourcing
  - feed reader
  - Inoreader
  - requests
  - RSS
  - websites
---
I want to branch out my Web reading instead of just bouncing between same few lame-ass sites all the time or wasting time on Reddit. I'm looking for US and world news, science, comedy, sci-fi, fantasy, Unix, art, history, philosphy, music (classical, heavy metal, jazz, rock), and webcomics. These are the sites I have so far:

## SF/F Video and Comics

* [The Mary Sue](http://www.themarysue.com/)

## NSFW

* [Oglaf](http://oglaf.com/latest/)

## News

* [BBC News - World](http://www.bbc.co.uk/news/world/#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa)

## Science/Nature

* [BBC News - Science & Environment](http://www.bbc.co.uk/news/science_and_environment/#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa)

## Comics

* [XKCD](http://xkcd.com/)
* [Cyanide & Happiness](http://www.explosm.net/rss.php)
* [Adventures of Business Cat](http://www.businesscat.happyjar.com/)

## Education

* [Open Culture](http://www.openculture.com/)

## Publishers

* [Curiosity Quills Press](https://curiosityquills.com/) (They publish my books, BTW.)
* [Tor](http://www.tor.com/)

## Authors

* [Cynthea Masson - News from Council Dimension](https://cyntheamasson.com/)
* [Dave Higgins - Davetopia](https://davidjhiggins.wordpress.com/)
* [Rick Wayne - Serum](https://rickwayneauthor.wordpress.com/)
* [Daniel Swensen - Surly Muse](http://surlymuse.com/)
* [LJ Cohen - Once in a Blue Muse](http://ljcbluemuse.blogspot.com/)

## Web

* [Jekyll (static website generation in Ruby)](http://jekyllrb.com/)

## Code/Tech

* [Coding Horror](http://blog.codinghorror.com/)
* [The Daily WTF](http://thedailywtf.com/)

## Text SF/F

* [Stile Teckel's The Caverns](http://www.thecaverns.net/Wordpress)
* [Tor.com Original Fiction](http://www.tor.com/)

## Reviews

* [Jeff Ford - At the Mountains of Radness](http://atmorbookreviews.blogspot.com/)

## Books/Literature/Writing

* [McSweeney's Internet Tendency](http://www.mcsweeneys.net/tendency)
* [The Passive Voice](http://www.thepassivevoice.com/)
* [Writers Helping Writers](http://writershelpingwriters.net/)

## You Could Be Next

If you're an acquaintance of mine and you have a blog, please share the URL in the comments. Otherwise, if you're an actual human being and not a spammer or a brand, and you know an interesting blog or site that provides a RSS feed, please tell me about it below. If I add your site to my feed, it's possible I'll end up sharing your posts, or -- better still -- quoting and linking to them in posts of my own.
