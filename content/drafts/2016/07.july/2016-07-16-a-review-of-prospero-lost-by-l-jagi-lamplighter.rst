*Prospero Lost* by L. Jagi Lamplighter
######################################

:date: 2016-07-16 12:00
:category: Hiding Behind Books
:summary: Catherine came back from Australia with a copy of L. Jagi Lamplighther's *Prospero Lost* as a gift, and I devoured it in two nights. Here's my review.
:tags: l. jagi lamplighter, prospero lost, prospero's daughter, shakespeare, the tempest, prospero, miranda, fantasy, urban fantasy, noir, demons, spirits, mythology, santa claus
:summary_only: true
:og_image: /images/l-jagi-lamplighter-prospero-lost.jpg


The Bard didn't tell the whole story. Not even close. The real story began after the curtain closed and the actors took their bows.

While it helps to be familiar with Shakespeare's last play, *The Tempest*, this familiarity isn't necessary because Ms. Lamplighter gives you the gist of the original story while justifying her novel's deviations from Shakespeare. However, to suggest she deviates is something of a disservice. Rather, she plays a series of variations on Shakespeare's themes while writing a modern literary fantasy of her own that begins with Miranda, Prospero's daughter and Handmaiden of Eurynome (which Ms. Lamplighter has identified to an extent with Sophia of gnostic belief), finding a message from her father in letters of fire.

Like any sensible CEO, Miranda attempts to delegate Prospero's request to warn the rest of her profoundly and hilariously dysfunctional family to one of her bound spirits, but a direct assault against her home forces a personal approach. On her journey, Miranda and her companions face demons, the fiancee who jilted her only to return with a Hell of an excuse, Santa Claus, and an elf prince offering her what she desires most if only she dares accept his gift.

The original cover is gorgeous, though as Ms. Lamplighter recently explained to me on Twitter, it can be somewhat misleading. *Prospero Lost* isn't some airy-fairy confection, but rather a rough-and-tumble ride with a tough-minded heroine dealing with as much insanity coming from more directions as a reader might find in one of Roger Zelazny's **Amber** novels or a **Repairman Jack** outing by F. Paul Wilson.

While I enjoyed *Prospero Lost* and devoured it in two nights after my wife brought home the US paperback after a visit to Australia, I must advise potential readers with a poor cliffhanger tolerance to either buy it with *Prospero in Hell* and *Prospero Regained* or not at all. However, if you ignore the *Prospero's Daughter* trilogy, you're doing yourself a disservice. This modern fantasy work is damnably underrated.

Get these covers while you can, incidentally. L. Jagi Lamplighter is working on `reissues with new artwork`__.

.. __: https://www.ljagilamplighter.com/2016/07/08/magic-come-to-life/

.. image:: {filename}/images/l-jagi-lamplighter-prospero-lost.jpg
  :alt: original cover for Prospero Lost by L. Jagi Lamplighter
  :align: left

Get this next.

.. image:: {filename}/images/l-jagi-lamplighter-prospero-in-hell.jpg
  :alt: original cover for Prospero in Hell by L. Jagi Lamplighter
  :align: left

Don't forget the end of the trilogy.

.. image:: {filename}/images/l-jagi-lamplighter-prospero-regained.jpg
  :alt: original cover for Prospero Regained by L. Jagi Lamplighter
  :align: left
