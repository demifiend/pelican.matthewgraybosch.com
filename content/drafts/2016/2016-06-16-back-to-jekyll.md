---
title: "Back to Jekyll"
excerpt: "I've gone back to Jekyll, because I just can't deal with WordPress any longer."
categories:
  - Website
tags:
  - Jekyll
  - WordPress
  - admin
  - headaches
  - comment spam
---

I've gone back to running my website using [Jekyll](http://jekyllrb.com), because I've come to dread the necessity of logging into WordPress and seeing that it needs more updates -- or that I've gotten more spam comments.

Things are a bit rough, but I'll sort them out as soon as possible. Assuming anybody even reads this blog any longer. If you have any questions, email me at [contact@matthewgraybosch.com](mailto:contact@matthewgraybosch.com) or find me on [Twitter](https://twitter.com/MGraybosch).

Since I'm running the site on a [Dreamhost](https://dreamhost.com) VPS, I've got a cronjob rigged to build the site hourly using a variation on this [shell script](https://github.com/matthewgraybosch/barebones-jekyll/blob/master/website-build.sh).

Hopefully the management at Dreamhost doesn't have kittens over this, but automating the deployment fixes the only reason I had for using WordPress. Now all I have to do is commit my changes and push them to [GitHub](https://github.com/matthewgraybosch), and this script does the rest.

And since I can write new posts *in* GitHub, I can blog from just about any machine. :cat:
