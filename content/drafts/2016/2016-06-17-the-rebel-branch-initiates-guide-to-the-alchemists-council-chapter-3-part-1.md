---
title: "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 1"
excerpt: "Welcome back to my readers' guide to *The Alchemists' Council* by Cynthea Masson, with guest commentary by Eric Higby. This post covers about half of Chapter 3."
header:
  image: bee-649952_1280.jpg
  teaser: bee-649952_1280.jpg
  caption: "[Source: Pixabay](https://pixabay.com/en/bee-flying-flower-insect-fly-649952/)"
coauthor: ehigby
toc: true
categories:
  - Books
  - Longform
tags:
  - Cynthea Masson
  - The Alchemists' Council
  - ECW Press
  - Canada
  - 2016
  - literary fantasy
  - intrigue
  - politics
  - alchemy
  - environmentalism
  - intention
  - ego
  - Buddhism
  - urban fantasy
  - portal fantasy
  - commentary
  - read-through
  - read-along
  - reader's guide
  - Eric Higby
  - Chapter 3
  - Part 1
  - Kabbalah
  - Qliphoth
  - the Left-Hand Path
mentions:
    - url: https://cyntheamasson.com/2016/06/19/reading-the-alchemists-council-chapter-3-part-1/
      title: "Reading The Alchemists’ Council: Chapter 3, Part 1"
---
In the last installment of the Rebel Branch Initiates' Guide, [Eric Higby](http://thecaverns.net) and I dug deep into the second chapter of [Cynthea Masson's](http://www.cyntheamasson.com) new literary fantasy, [*The Alchemists' Council*](https://www.goodreads.com/book/show/26113691-the-alchemists-council) published by [ECW Press](http://ecwpress.com/collections/books/products/alchemists-council). In [The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 2]({% post_url 2016-06-06-the-rebel-branch-initiates-guide-to-the-alchemists-council-chapter-2 %}), Eric and I covered the following topics:

* Erasure: Making an Unperson
* The Missing Lapidarian Bees, Part 2: Grunt Work for the Junior Initiates
* Arjan's Reaction and Promise
* Sadira's Concerns About Arjan
* The Missing Lapidarian Bees, Part 3: As Below, So Above
* A Field Trip to Santa Fe
* Searching for Pendants
* Why Turquoise?
* Broken Memories of Kalina
* The Missing Lapidarian Bees, Part 4: A Ritual Interrupted

As before, I'll present Eric Higby's commentary first, since his take on Chapter 3 is more concise than my own. Mine will follow for readers who wish to go into greater detail. Expect spoilers ahead.

**THE ALCHEMISTS' COUNCIL FORBIDS YOU TO READ ANY FURTHER.**

**THE REBEL BRANCH ENCOURAGES YOU TO CONTINUE, BUT THE CHOICE IS YOURS...**

## Eric Higby on Chapter 3 of *The Alchemists' Council*

Chapter 3 of The Alchemists’ Council begins a year in the past from Jaden’s perspective. We commence in the Elixir chamber which is a scene that I can personally relate to and find much love in.

{% picture test-lab-hi.png alt="a test lab" %}

An alchemy lab is a time old image that in my opinion brings thoughts of magic and the possibility of anything. The ability to take the raw ingredients of the world and create is a fantastic concept that most can understand.

Often you see in the fantasy genre the majestic wizard labs with bubbling beakers, boiling cauldrons, and flashing lights.

Alchemy is not limited to the fantasy world though being very much a part of this one. I growing up had a large chemistry table along with chemistry equipment being passed down to a third generation family member. Boasting hundreds of test tubes, beakers, bunsen burner, centrifuge, microscopes, and chemicals you would be hard pressed to obtain today, I learned much and had many adventures using my chemistry books.

Jaden’s adventure takes us through a mix of the immersion of those feelings. The magical alchemy labs that we envision the gnome making magic in, or that of the tall woman in a lab coat with glasses as she peers at her clipboard.

{% picture professor-hi.png alt="a professor" %}

While the story is taking us through an enjoyable and immersive experience, it is also teaching us about the world we are being introduced to, at the same time as Jaden. This past perspective felt as much for us to learn as it was for Jaden to remember.

We continue to learn about conjunction, the pros, and cons. How promotion itself in the council is achieved through that of conjunction, while at the same time one is risking their “self” for.

At the same time though we learn more about the relationship between the rebels and the council and how the Lapis effects the realm. I used the analogy of Yin and Yang in one of my earlier pieces but I also very much see a chaos vs. order type of relationship at play as well.  

{% picture chaos-symbol-corrupt-raige.png alt="Chaos symbol by Corrupt Raige, most likely inspired by Michael Moorcock" %}

I very much see the Rebel’s being on the side of chaos and the council being the side of order. I incorporate this into the Yin, and Yang analogy as in I personally believe that a balance between chaos and order is the answer.

As we learn about the relationship that exists between the groups, we also learn that Jaden is being recruited fro the rebels! This may or may not surprise you. I certainly have felt that she would make a good candidate and may end up there. That is a long stretch from finding out that she is being groomed, and they have methods in place on whom to watch for as potential candidates.

{% picture hourglass.png alt="an hourglass" %}

We also learn that time and space itself is not beyond the abilities of the Alchemist, in this chapter. That they are merely perspectives depending on where one may be located or how one may view things.

The flaw in the Lapis allows the council dimension itself and the loss of it would cause both the council and the rebels to cease to exist. This is what the council strives for! It seems selfless and noble and yet as we also learn in this chapter they have been known to abuse power.

A perfect example of this corruption is that of Cedar and her acquisition of a drug called Sephrim. Sadira worries that Cedar is obtaining this from the rebels and approaches her about the concerns only to be told that is not the case. Whatever the supplier, this drug ensures that the person in a conjunction will be the “winner” of the outcome.

{% picture queen-bee-hi.png alt="a queen bee" %}

Their corruption of power is not just on their own but in controlling our world. The most disturbing plot in this chapter being that by releasing Lapidarian Bees into our world, in sufficient number, will lead to the loss of our free will. I want the bee population concerns to be addressed, but that is a bit extreme!

The Rebels fight to keep this from happening just as they work to maintain the flaw in the Lapis. Their very efforts are allowing them and the council to continue to exist in the negative space that is being created by the defect.

To me, it is clear that Jaden is being convinced to join the Rebel. I know I am being swayed!

## A Rebel Branch Initiate's Guide to *The Alchemists' Council*: Chapter 3, Part 1

Hot on the heels of the explosive ending of Chapter Two comes an extended flashback. The entire chapter takes place one year before the present, and introduces some elements from the Rebel Branch. I'll also be going heavy on comparisons to real-world esotericism (Kaballah, the Left-Hand Path, Discordianism, etc.), so buckle up.

Before we can explain why the Council expelled Kalina and subjected her to erasure, we must first introduce Kalina and the Rebel Branch. We'll also be meeting the younger Jaden. Finally, if you skipped the *Prima Materia* (p. vii), now would be a good time to review it. Doing so will provide valuable context for this chapter (pages 120-174).

Because this chapter is so big, and demands even more analysis than the previous chapters, I'll be splitting the Rebel Branch Initiate's Guide for Chapter 3 into two parts. This is the first, and will cover the following topics:

* Lead Into Gold: The "Hello World" of Alchemy
* The Conjunction of Senior Magistrate Tesu
* Unintended Consequences of Stalking
* Sadira's Rebellious Impulses
* Kalina's Disappearing Act
* Jaden's Distrust and the SNAFU Principle
* The Rebel Branch: Walking the Left-Hand Path
* Sadira's Doubts

I'll cover the rest of the chapter in subsequent, where I'll discuss the following topics:

* Jaden Tastes the Dragon's Blood
* Cedar and Obeche at Cross Purposes
* Dracaen's Pitch
* Obeche's Zeal
* Passing Notes
* How Kalina Got Made
* Jaden's Loss

### Lead Into Gold: The "Hello World" of Alchemy

We don't get the meet Kalina right away; that would be too easy. Instead, to drive home the fact that we've gone back to a year ago in the story, Cynthea Masson opens Chapter 3 with a lab experiment for the Junior Initiates: the [transmutation of lead into gold](http://www.scientificamerican.com/article/fact-or-fiction-lead-can-be-turned-into-gold/). This transmutation was one of the classic goals of Western alchemy, along with the creation of the Philosopher's Stone, the alkahest (universal solvent), and an Elixir of immortality.

For Junior Initiates like Jaden, the transmutation of lead into gold is merely a first step, a trivial task intended to introduce and apply basic alchemical concepts that will continue to serve Jaden as she progresses through the ranks of the Alchemists' Council. In that regard, it's similar to the "hello world" program many first-year computer science and software development students are taught to write. Here's one such program, [implemented in C](http://c.learncodethehardway.org/book/ex1.html).

``` C
int main(int argc, char *argv[])
{
    puts("Hello world.");

    return 0;
}
```

By itself, "hello world" is a trivial program, but it is also the smallest *complete* program a programmer can write. If you can't write a working "hello world", you won't be able to write more complex code. Likewise, transmuting lead into gold is child's play for the Alchemists' Council, and thus a suitable learning exercise for Junior Initiates.

However, Jaden builds up this first practical exercise in alchemical principles in her mind, investing it with additional meaning. To begin, she hopes to impress Sadira, who she has already marked as her favorite instructor. Furthermore, Jaden hopes that by actually *doing* alchemy she'll finally be able to get over her desire to escape Council dimension and return to her old life -- if only for a little while.

For these reasons, Jaden got to class early and hopes that she would at least impress Sadira as somebody who has their act together, since as the current newbie Jaden lacks sufficient knowledge of alchemical practice to distinguish herself in that regard, and certainly can't outdo Cercis. Nor can she compete with Laurel on the basis of appearance, though I suspect this is mainly the negative self-talk common to many young women. Finally, Ritha has them all beat on experience as the eldest of the Junior Initiates.

As the eldest, Ritha is eager to progress, but cannot do so until two alchemists further up undergo conjunction. Fortunately, as she confides to Jaden in class, the next conjunction should happen soon. According to her, one of the candidates is Tesu.

### The Conjunction of Senior Magistrate Tesu

The scuttlebutt Ritha shares with Jaden concerning Tesu proves true later on. Azoth Ravenea confirms it at the next Council meeting by her announcement, though Jaden is too busy trying to work out the meaning of conjunction by listening to the section of the Law Codes pertaining to conjunction the other alchemists recite to pay full attention.

It is Rowan Esche who introduces Tesu's partner on conjunction. She's a young blonde in garnet robes who Jaden had not previously noticed before, but Cercis, Laurel, and Ritha all recognize her: this is Kalina.

Apparently the pairing of Kalina and Tesu is a shock to many of the council's members, despite it being approved by the Elder Council, but Cercis and Ritha share a different reaction: "that could have been me" (p. 128). Ritha in particular seems to feel like she dodged a bullet, since she would have been up for consideration if she had become a Senior Initiate instead of Zelkova.

### Unintended Consequences of Stalking

Though Jaden had not previously noticed Kalina before, the [Baader Meinhof Phenomenon](https://www.damninteresting.com/the-baader-meinhof-phenomenon/) strikes with a vengeance, and now she sees the garnet-robed blonde everywhere. Though in Jaden's case, it's less a frequency illusion and more a matter of Jaden constantly looking for Kalina -- and looking for reasons to talk with her. Jaden even compliments Kalina on her pendant, a dark green stone set in silver repoussé (metal hammered from the reverse side to create a low relief), but can't get more than a smile out of her.

Not that Kalina's cordial aloofness stops Jaden. Instead, it only fuels her determination to get through to the older woman. After a fight between two birds outside her window wakes Jaden, she sees Kalina leaving the residence hall and taking a stone path into the backwoods of Council dimension.

Naturally, Jaden follows, but soon loses Kalina. Instead, she happens upon a man and a woman arguing. She can only see one of them, a figure in blue robes, without revealing her presence. However, she manages to get close enough to hear their conversation.

It's about Tesu. They're concerned he saw them, but the woman is confident that they had not yet [been made](http://english.stackexchange.com/a/188783). This confidence is misplaced thanks to Jaden's presence, though she has no idea what the hell is going on.

The conversation soon ends, and the blue-robed man withdraws a red stone from within his robes. After chanting an incantation, the man soon disappears into mist, and the woman leaves on foot -- or so Jaden thinks.

Jaden's too interested in how the blue-robed man left to worry about her *own* [OPSEC](http://www.opsecprofessionals.org/what.html). This lack of [tradecraft](http://www.merriam-webster.com/dictionary/tradecraft) is how Kalina manages to find *her* studying the cliff face and speculating about the possibility of escaping Council dimension.

Note, however, that Cynthea Masson doesn't clearly state that it was Kalina talking to the blue-robed man. We see Kalina heading toward the woods, and we see her when she confronts Jaden, but the only evidence we have of Kalina talking with the blue-robed man are based on inference:

* the scene didn't introduce any other named characters.
* the man asks the woman if Tesu saw her.
* the woman didn't disappear through the mist portal, and thus could have circled around Jaden and found her.

However, I'm getting ahead of myself. It isn't until page 135 that we learn it was Kalina who caught Jaden. Instead, we cut to Sadira.

### Sadira's Rebellious Impulses

We find Sadira in an interesting position in this chapter. She's still raw over the loss of Saule because of her conjunction with Cedar, but she's taken Cedar as her lover despite the Novillian Scribe being the person who "for all intents and purposes, had murdered the love of her [Sadira's] life" (p. 133).

She's stuck on a guilt trip, despising herself because she obeyed the Council and its dictates instead of rebelling against them. No doubt that's why we see her voice such sentiments as "chaos is underrated" (p. 134) when Zelkova asks for help with her scribing.

Zelkova's problem is easily solved; her pen just needs a new nib because the old one was cracked, but if she were working on a real Lapidarian manuscript, a broken nib could apparently disrupt elemental balance.

Never mind losing the kingdom for want of a nail; try losing the world for want of a pen.

Zelkova, suffering an acute attack of sanity, says that she doesn't want such power or responsibility, but Sadira insists that she's duty-bound, and must accept it. Explaining that Zelkova has years, if not centuries, of training ahead of her before she becomes a Lapidarian scribe, Sadira tells the Initiate that all she has to do is heed the Council's lessons and learn from them.

I wonder if Sadira believes that herself. Regardless, I suspect that if Jaden were to go Rebel at least Sadira would sympathize.

### Kalina's Disappearing Act

I had mentioned earlier that we didn't have direct evidence for Kalina being the one to talk with the man in the blue robes. We still don't, but Kalina confronting Jaden at the cliff-face is the first chance Jaden has gotten to actually talk to the other woman.

Of course, Kalina's a bit more interested in grilling Jaden and finding out what *she* saw than in conversation. Likewise, Jaden lacks evidence that would let her think Kalina had been talking to the blue-robed man, and this lack of evidence shapes her answers to Kalina accordingly -- at least until Jaden remembers her backbone and demands an explanation.

Rather than provide one, Kalina begins the following exchange (on page 137):

> "Do you trust me?" she asked.

> "Trust you? I barely know you!" Jaden replied. She pulled away from Kalina.

> "Not yet," she [Kalina] said.

> "I don't trust you yet?"

> "You don't *know* me yet. And there may come a time when you don't know me again."

> "What?"

> "I can't explain right now. I have to go. You will understand soon."

While Jaden doesn't know it at the time, careful readers who have gotten this far will no doubt realize that Kalina is talking about erasure. Even if we don't know for sure that Kalina was the one speaking with the man in the blue robes, this dialogue is suggestive.

The line, "And there may come a time when you don't know me again," implies that Kalina knows she's up to something dangerous, something that could make her an unperson. She doesn't explain herself to Jaden yet, and I think that's because her initial question, "Do you trust me?" implies an unspoken question: "Can I trust you?"

Kalina probably isn't sure she can trust Jaden yet. However, her use of the same portal as the blue-robed man has some interesting implications.

### Jaden's Distrust and the SNAFU Principle

Jaden certainly doesn't know what to do about Kalina, or about the arguing pair she saw while following the Senior Initiate. And there's only one person in the Council she trusts enough to ask for advice. Getting to talk to Sadira alone, however, proves impossible.

In addition to her pedagogical duties as one of the eight Magistrates assigned to teaching the Junior and Senior Initiates, Sadira (along with Linden) is currently busy supervising the other tutors. No doubt Cynthea Masson's drawing on experience from her day job as a VIU English professor as she describes Sadira's supervisory duties:

* showing new tutors the ropes
* dividing Junior and Senior classes based on the tutors' abilities
* providing course materials and lesson plans
* outlines techniques for individualizing lesson plans to account for varying ability and experience in initiates

The last one is probably the hardest. Though class sizes are quite small compared to a typical US classroom (remember, there are only *four* Junior Initiates and *twelve* Senior Initiates for eight Magistrates to teach) it probably isn't easy to adapt official Council training materials to engage an experienced Senior Initiate stuck in the same class with a fresh Junior Initiate possessed of no knowledge of alchemy not distorted by popular culture.

It's hard work, and Sadira finds it tiring. Linden, her fellow faculty supervisor, insists he finds the bureaucratic work accompanying the start of a new course rotation "invigorating" (p. 139), which only leads Sadira to wonder why men on the Council -- where conjunction often blurs gender to the point of making it all but irrelevant -- still insist on displaying machismo by claiming an inexhaustible capacity for work.

It's a timely question, given that our inability to say no both to more work and more *stuff* seems to be killing the planet's biosphere, but that rant is beyond the scope of this commentary.

Getting back to the story, if Sadira had any thought of upbraiding Linden for being too perky and eager, Jaden precluded the possibility of acting on them by throwing open the double doors to the library and rushing to her. Once she catches her breath, Jaden asks to speak to Sadira privately.

Sadira, reasonably enough, asks if Jaden wants to talk about a personal matter or Council business. Since Jaden says it might be of importance to the council, Linden insists on getting involved.

There's just one little problem where Jaden is concerned: she doesn't know if she can trust Linden. When she asks Linden, "Can I trust you?" (p. 140), Linden immediately dismisses her concerns by countering, "Are you suggesting that certain members of the Alchemists' Council are not to be trusted?"

I think that will prove to be a mistake on Linden's part. While Jaden didn't come to them intending to edit the truth to avoid implicating Kalina, his brusque insistence that every member of the Council is beyond reproach leads her to realize that she has no particular reason to trust *any* member of the Alchemists' Council, whether it was Linden or Kalina. After all, the Council had virtually abducted her, and the alchemical vapors kept her drugged.

Add to this Jaden's status as the Alchemists' Council's newest initiate, placing her at the *bottom* of the hierarchy, and you have the perfect set of conditions for the [SNAFU Principle](http://explorations.chasrmartin.com/2008/01/29/the-snafu-principle/) to come into play. This principle comes from Robert Anton Wilson, who explains it in *The Illuminatus! Trilogy* with Robert Shea:

> It's what I call the "SNAFU principle." Communication only occurs between equals -- real communication, that is -- because when you are dealing with people above you in a hierarchy, you learn not to tell them anything they don't want to hear. If you tell them anything they don't want to hear, the response is, "One more word Bumstead and I'll fire you!" Or in the military, "One more word and you're court-martialed." It's throughout the whole system.

> So the higher up in the hierarchy you go, the more lies are being told to flatter those above them. So those at the top have no idea what is going on at all. Those at the bottom have to adjust to the rules made by those at the top who don't know what's going on. Those at the top can write rules about this, that and the other, while those at the bottom have got to adjust reality to fit the rules as much as they can.

In any organization where those at the bottom of the hierarchy can be punished for speaking too freely by those further up the chain, "all fucked up" becomes "situation normal" because the people in charge have no idea what the hell's going on. Fortunately the effect of the SNAFU Principle on the Alchemists' Council is limited because there are only ten levels of hierarchy and only 101 members, but Jaden still has incentive to hold back information -- and this reticence buys time for Kalina.

It won't save her, however. Despite sticking firmly to what she understands as her minimum duty to the Council, Jaden told Sadira and Linden enough for them to conclude that Jaden's report needs to go straight to the Elders, and for Linden to suggest that she had witnessed Rebel Branch activity, complete with an incursion into Council dimension.

### The Rebel Branch: Walking the Left-Hand Path

Though Jaden may have foiled a Rebel Branch plot despite being only a Junior Initiate, she has no idea what the Rebel Branch is. Linden explains them to her in the following passage on page 143:

> "Vigilantes," said Linden. "A self-sustaining branch of the Alchemical Tree as old as the Council itself. For thousands of years, legions of generations, the Council has maintained control over the Tree, its Rebel Branch, and the outside world, but not without losses and irreparable damage."

This explanation may be suitable for a Junior Initiate, but I think the truth is more complicated than that when we consider [Cynthea Masson's](https://cyntheamasson.com/2016/06/09/reading-the-alchemists-council-chapter-2/) response to my commentary on Chapter 2:

> For me the main conflict of the novel revolves around opposing philosophies regarding free will and power. Since the era of the "primordial myth" with which the book opens, the Alchemists' Council and the Rebel Branch have been at war. Thus the conflict is as ancient as the dimensions themselves rather than based in particular memories that any living alchemist or rebel may have. The goal of the Alchemists' Council is to remove the Flaw in the Stone, whereas the goal of the Rebel Branch is to increase it. The Flaw in the Stone is what permits free will. If the Flaw were to be removed completely, the Council believes everyone would be saved in the dimensional equivalent of a unified afterlife. The Rebel Branch, on the other hand, wants to maintain their current existence as individuals with choice (rather than being forced into a collective "One" by the alchemists). This main conflict is explored through a variety of lenses throughout the book. Since I teach medieval literature, much of my inspiration for these conflicts came from philosophical debates on free will found in works such as Book IV of Chaucer's *Troilus and Criseyde*.

Considering this response by Masson to [Jana Nyman's review at *Fantasy Literature*](http://www.fantasyliterature.com/reviews/the-alchemists-council/), we can view the Rebel Branch through several lenses.

For readers familiar with the fantasy works of Michael Moorcock, we can view the world of *The Alchemists' Council* as one where Law is dominant and Chaos fights tooth and nail to maintain a foothold. If the Council succeeded in extirpating the Rebel Branch, it would mean that Law would reign unchecked, a state as inimical to humanity and free will as that of absolute Chaos.

While Linden describes the Rebel Branch as a part of the Alchemical Tree ultimately under the Alchemists' Council's control, I think the Rebel Branch is a conceit on the Council's part. Readers who reviewed the *Prima Materia* may recall that the original conflict was between Aralia, the first being to attain ego and intention, and Osmanthus, the second to do so. Both combatants gathered followers, who continued the conflict after Aralia and Osmanthus reconciled with each other and the Calculus Macula through alchemical conjunction.

If the Alchemists' Council descended from the Aralians, then the Rebel Branch probably descended from the Osmanthians, and constitutes a separate and *adverse* Alchemical Tree. The notion of an "adverse tree" brings to mind some concepts from Kabbalah, Jewish esotericism. In Kabbalah, the light of God descends to the material world via the tree of life, the Sephiroth.

However, there's also an "impure" counterpart to the Sephiroth that distorts divinity, the [Qliphoth](https://en.wikipedia.org/wiki/Qliphoth) or Sitra Ahra ("the other side"). While most Jewish Kabbalistic practice steered clear of the Qliphoth, viewing them as shells or remnants of the pure Sephiroth, Gentile Hermetic systems often treat the other side as a necessary balance. Consider this illustration from [Newaeon Tarot on Tumblr](http://newaeontarot.tumblr.com/post/17018540458):

{% picture sephiroth-qliphoth-tree.jpg alt="Illustration of the Qliphoth as the roots of the Sephiroth" %}

Have you noticed that Malkuth and Lilith, the light and dark corresponding with the mundane/material world, intersect in the illustration above? I think this is how the Rebel Branch actually works. Both trees arise out of the Calculus Macula, but in opposing directions.

Both, I think, share a common goal, but their methods and attitudes are such that I've come to view the Alchemists' Council as a white-light [right-hand path](https://en.wikipedia.org/wiki/Left-hand_path_and_right-hand_path) practice, whereas the Rebel Branch uses the [left-hand path](https://en.wikipedia.org/wiki/Left-hand_path_and_right-hand_path). Neither side wants the world's elemental balance to fall apart, or for the outside world to suffer irreparable damage. However, we'll see later on that the Rebel Branch attempts to *persuade* people, and seems to honor individuality and volition.

### Sadira's Doubts

Though Sadira shares Linden's opinion that Jaden's report needs to be passed up the chain, she isn't sure it's wise to go straight to the Azoths. After all, Jaden might not have seen Rebels. She might have seen Council members she didn't recognize on legitimate council business. The cliff face can also be used by Council members to transfer to the eastern hemisphere of the outside world.

Instead, Sadira wants to consult one of the Novillian Scribes, like Amur or Cedar. However, she'll have to make do with Obeche. This is a mixed blessing. While Obeche is willing to listen, and agrees to convene the Elder Council, in this chapter he seems to pursue Rebel Branch alchemists as zealously as FBI director [J. Edgar Hoover](https://en.wikipedia.org/wiki/J._Edgar_Hoover) used to hunt Communists and other subversives.

With Obeche on the job, Sadira has cause for fear. If the Elders launch a sufficiently wide investigation, it might uncover Cedar's little sideline as a Sephrim pusher.

Stay tuned for "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 2". If you want more of the Rebel Branch Initiates' Guide, [check out the main page](/commentaries/the-rebel-branch-initiates-guide-to-the-alchemists-council/).
