---
id: 3752
title: 'Dark Souls III: The Knight of Thorns Falls'
date: 2016-04-15T22:20:27+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3752
permalink: /2016/04/dark-souls-iii-the-knight-of-thorns-falls/
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"2e72a9943882";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:85:"https://medium.com/@MGraybosch/dark-souls-iii-the-knight-of-thorns-falls-2e72a9943882";}'
categories:
  - Dark Souls III
  - Games
tags:
  - Cathedral of the Deep
  - dark souls
  - Dark Souls III
  - Fair Lady
  - Knight of Thorns
  - Longfinger Kirk
  - lore
  - Quelaag
  - "Rosaria's Fingers"
  - Witch of Izalith
---
Players of the original Dark Souls may remember Kirk, the Knight of Thorns. He's back in Dark Souls III, and already beat me once. Watch my rematch.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Not a hell of a lot of finesse on my part, but this isn't the Olympic Games. I have to admit, however, that finishing the fight with that thrust to the face was especially gratifying.

But is Longfinger Kirk truly the Knight of Thorns? Didn't the original _Dark Souls_ take place in an age long past? In _Dark Souls III_, Longfinger Kirk drops a Barbed Straight Sword and a Spiked Shield when killed.

The Barbed Straight Sword's description says only this:

> Sword of Longfinger Kirk, the infamous Knight of Thorns. This sword's blade is lined with countless deadly thorns.
> 
> The thorns of this ominous weapon induce heavy bleeding. 

The Spiked Shield is described as follows:

> Shield of Longfinger Kirk, the notorious Knight of Thorns. The surface bristles with thorns.
> 
> Its vicious design makes it an effective weapon, and its thorns can inflict heavy bleeding on those unfortunate enough to be struck. 

Both items confirm that Longfinger Kirk is indeed the Knight of Thorns, but I think it's highly unlikely that Longfinger Kirk is the _same_ Kirk who hunted undead throughout the lost land of Lordran so he could steal their humanity and offer it to the Fair Lady, that unfortunate daughter of the Witch of Izalith.

Instead, Longfinger Kirk is probably a successor, a man who found the arms the old Knight of Thorns once bore into battle and took them up in service to a different cause. But what cause might that be? Knowing that Rosaria's Fingers is headquartered somewhere in the Cathedral of the Deep where we encounter Kirk, might Rosaria be the latter Kirk's "Fair Lady"?

We don't know for sure. However, VaatiVidya does explain the relationship between the original Kirk of Thorns and Quelaag's pale sister in this video.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

#### Image Credit

[KIRK: Knight of Thorns by ggutuart on Tumblr](http://ggutuart.tumblr.com/post/130580588373/k-i-r-k-knight-of-thorns-check-out-the-other-dark)