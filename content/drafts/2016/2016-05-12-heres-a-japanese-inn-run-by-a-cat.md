---
title: "Visit a Traditional Japanese Inn, and Get Greeted by a Cat?"
excerpt: "Being greeted by a hotel cat is more likely than you think as accommodations seek to differentiate themselves and attract visitors."
header:
  image: japanese-inn-cat.jpg
  teaser: japanese-inn-cat.jpg
  caption: "[Chinami Tajika for Asahi Shimbun](http://www.asahi.com/ajw/articles/AJ201605120005.html)"
categories:
  - "/dev/random"
tags:
  - Japan
  - cats
  - Asahi Shimbun
  - news
  - fun
---

Somewhere in Japan's Yamagata Prefecture there's a ryokan, a traditional inn, whose nominal landlady is a cat. [I'm not joking](http://www.asahi.com/ajw/articles/AJ201605120005.html). Her name is Mai.

The human owners let Mai prowl around the inn, where she greets guests with a friendly "meow" and gets pettings. Apparently she's been doing it for three years.

For some reason this reminds me of the anime *Aria*, which deals with gondoliers on Mars who work for companies headed by cats.

---

<iframe width="420" height="315" src="https://www.youtube.com/embed/3gg2GM-HGfE" frameborder="0" allowfullscreen></iframe>

---

Also, there's Tama the Japanese railroad kitty. [Remember her](https://en.wikipedia.org/wiki/Tama_(cat))?

[![Tama, the stationmaster of Kishi Station, Wakayama Electric Railway, located in Kinokawa, Wakayama, Japan](https://upload.wikimedia.org/wikipedia/commons/f/f0/Station-Master_Tama.JPG)](https://en.wikipedia.org/wiki/Tama_(cat)#/media/File:Station-Master_Tama.JPG)
