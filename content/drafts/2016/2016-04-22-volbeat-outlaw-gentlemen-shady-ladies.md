---
title: "*Outlaw Gentlemen &amp; Shady Ladies* by **Volbeat** (2013)"
category: Music
tags: [ "heavy metal", "hard rock", "groove metal", rockabilly, Denmark, Western, "Lola Montez", "King Diamond", "Sarah Blackwood" ]
excerpt: "The rockabilly elements differentiate Volbeat's 2013 album *Outlaw Gentlemen &amp; Shady Ladies* from my usual fare, but I enjoy a good concept/theme album."
header:
  image: volbeat-outlaw-gentlemen-shady-ladies.jpg
  teaser: volbeat-outlaw-gentlemen-shady-ladies.jpg
---
{% include base_path %}

*Outlaw Gentlemen &amp; Shady Ladies* is the fifth studio album by Denmark's **Volbeat**, and has been out since 2013. The rockabilly elements differentiate it from my usual fare, but I enjoy a good concept/theme album. This one draws from the history of the American West, and features some guest vocalists, like King Diamond on "Room 24" and Sarah Blackwood on "Lonesome Rider".

## Listen

{% include spotify-album.html image="volbeat-outlaw-gentlemen-shady-ladies.jpg" album="5SBrIIYCvThaqN9r1SV2pv" caption="<em>Outlaw Gentlemen & Shady Ladies</em> by <strong>Volbeat</strong> (2013)" %}

## Watch

<iframe width="560" height="315" src="https://www.youtube.com/embed/FPJ1KIMArWA" frameborder="0" allowfullscreen></iframe>
