Is Handsome Jack a Misunderstood Hero?
######################################

:date: 2016-01-16 03:35
:summary: Handsome Jack from *Borderlands* is my favorite sort of villain, an idealist who uses questionable methods because the end justifies the means.
:category: Hardcore Casual
:tags: borderlands 2, borderlands: the pre-sequel, handsome jack, law of eristic escalation, end justifies means, machiavellian, tragic hero, tragic villain
:summary_only: true


I've been playing *Borderlands: the Handsome Collection* lately, and Handsome Jack is definitely one of my favorite gaming villains along with Kefka Palazzo from *Final Fantasy VI* and Revolver Ocelot in *Metal Gear Solid* and *Metal Gear Solid IV: Grumpy Old Soldiers*. Jack's also my favorite sort of villain, an idealist who uses questionable methods because **the end justifies the means**.

Game Theory tackles Jack in this video_, which is actually halfway decent.

.. _video: https://www.youtube.com/watch?v=Z5onlG_8J18

Handsome Jack's ultimate mistake, besides yielding to his paranoia and just casually murdering anybody who might be in a position to betray him (like the scientists who helped him liberate Helios Station), is a classic: he's trying to *impose* order on Pandora and its people from above in the most literal sense possible given that he oversees events on both Pandora and its moon Elpis from a Hyperion (which means "watcher from above") space station.

When you impose order from on high, you only escalate chaos. Order must arise out of chaos if its to have any hope of enduring.

One could even argue that Handsome Jack is the embodiment of the `Law of Eristic Escalation`__, a fundamental Discordian concept.

.. __: http://relaxedfocus.blogspot.com/2015/07/law-of-eristic-escalation-imposition-of.html