title: Why Galneryus is the Best Power Metal Band You've Probably Never Listened To
summary: At their best, Galneryus is the band lesser power metal bands want to be when they grow up. Chances are you haven't even heard of 'em.
date: 2016-01-28 03:36
modified: 2018-05-19 13:26
category: Metal Appreciation
tags: clean vocals, english lyrics, guitar, japan, japanese lyrics, j-metal, keyboards, melodic metal, neo-classical metal, power metal, shredding, syu, yama b, amano yoshitaka
og_image: /images/galneryus-band.jpg
summary_only: true


At their best, [Galneryus](http://www.metal-archives.com/bands/Galneryus/19417) is the band lesser power metal bands want to be when they grow up. When they're not at their best, they're still damn good. Unfortunately, they don't get nearly enough attention outside of Japan.

## Revisions

This post has been subject to several revisions since initial publication. See below for details.

### Update for 19 May 2018

Cleaning up this post for the new version of my website. Future updates will cover new albums released since 2015.

### Update for 28 January 2016

I originally published this in 2015 on a site called [The Black Sword](http://theblacksword.org/heavy-metal/galneryus/). This is a cleaned-up and updated version.

Galneryus' music is available for streaming on Spotify, and includes a compilation called *The Iron-Hearted Flag* that I haven't covered in this post. They also have some kind of [*Fist of the North Star*](https://en.wikipedia.org/wiki/Fist_of_the_North_Star) tribute album called *Fist of the Blue Sky*, which I also haven't covered. Finally, I haven't covered any singles other than *Alsatia/Cause Disarray*.

## Who are Galneryus?

Guitarist and bandleader Syu started Galneryus with the help of vocalist Masahiro "Yama-B" Yamaguchi in Osaka, Japan in 2001. Both were veteran musicians at the time. Syu got his start with the [visual kei](http://en.wikipedia.org/wiki/Visual_kei) band Valkyr in 1998, and also worked with Spinalcord (formerly Aushvitz) and [Animetal](http://en.wikipedia.org/wiki/Animetal) while getting Galneryus off the ground. Yama-B was frontman for a band called AxBites, and also performed as Rekion, River End and Gunbridge.

Syu and Yama-B rounded out the band's classic lineup with Ryosuke "Tsui" Matsui (AxBites), Yuhki (Ark Storm, Marge Litch), and Junichi Satoh (Concerto Moon) on bass, keyboards, and drums respectively.

## How I discovered Galneryus

I found out about Galneryus by virtue of their 2008 song "Alsatia" being featured as the opening theme to the six-episode anime [*Mnemosyne*](http://en.wikipedia.org/wiki/Mnemosyne_%28anime%29), which is set in modern and near-future Tokyo and focuses on Rin Asougi, an immortal female private investigator. It's not safe for work, and inappropriate for children. Galneryus also did the ending theme, "Cause Disarray". Please refer to [*Alsatia/Cause Disarray*](#alsatiacause-disarray-single) for details.

## Why Galneryus is Obscure

I blame the band's record label, VAP Inc., a subsidiary of Nippon Television Holdings Inc. They don't seem to distribute the band's albums outside of Japan except via the iTunes Music Store; the only listings I found on Amazon were for imports priced between $30-60. I guess it hasn't occurred to anybody over there to license these albums to an outfit like Nuclear Blast or Century Media for distribution in North America and Europe, because not everybody uses Apple gear.

## Classic Galneryus

The classic period for Galneryus begins with the band's inception and ends with the release of their third album, *Beyond the End of Despair*. The following three LPs are essential listening for any metalhead who likes melody, musicianship, and clean vocals.

* 2003: *The Flag of Punishment*
* 2005: *Advance to the Fall*
* 2006: *Beyond the End of Despair*

With guitarist Syu's constant presence, Galneryus has maintained a distinctive sound throughout their career. All of their albums tend to open with a short (1.5-2 minutes) instrumental that leads into the subsequent track, which tends to a blistering track meant to kick the listener's ass. The rest of the album features anthems with soaring choruses and guitar and keyboard solos that make me want to get out of my seat and bang my head without any regard for the presence of others. Beginning with *Advance to the Fall* in 2005, the last track before any bonus material is usually another short instrumental that often reprises themes and motifs from the opening track.

Yama-B is an excellent vocalist, though not quite on a level with luminaries like Rob Halford (Judas Priest), Bruce Dickinson (Iron Maiden), or Matt Barlow (Iced Earth). He delivers the lyrics he wrote for the first three albums with precise articulation and intonation, and a passionate sincerity that works well with Syu's shredding and Yuhki's keyboard theatrics.

Yama-B's lyrics for Galneryus draw upon [standard power metal motifs](http://www.metalstorm.net/pub/fun_comments.php?fun_id=6) with a Japanese flavor, and while his English is occasionally awkward, it gets the job done. We can treat "Struggle for the Freedom Flag" from 2003's *The Flag of Punishment* as emblematic of Galneryus anthems while Yama-B worked with them:

> See the blood of our friends that is sticking to us  
> Rising out of the sea of sorrow  
> There's nothing to lose anymore

> We will be fighting till we get hold of our victory  
> Oh my sword leads us to the castle where the evil lies

> The sacrifice was big, but we became stronger  
> We get hold of glory again  
> On the road, we will defeat our enemy even if we'll become the dust

> The armors protect our body and departed souls protect our mind  
> Now the time, we'll break through the gate of steel  
> Into perdition  
> Struggle for freedom, it's our rule

Now that I think of it, this would be a good theme song for games like *Demon's Souls*, *Dark Souls*, and *Dark Souls II*.

## 2003: *The Flag of Punishment*

Believe it or not, *The Flag of Punishment* is some kind of post-apocalyptic concept album with a cover by *Final Fantasy* and *Vampire Hunter D* artist Yoshitaka Amano.

There isn't a single bad track on this album, but my favorite is "Struggle for the Freedom Flag", a chest-thumping, head-banging anthem in the vein of "One Shot At Glory" by Judas Priest and "Aces High" by Iron Maiden. As soon as you hear Syu drag his pick down the string, you *know* he means business.

"Requiem" is probably one of the best metal instrumentals I've ever heard, and the first piece I'd use as evidence that Syu is a superior guitarist to Yngwie Malmsteen. "Final Resolution" is a great song for a last stand against overwhelming odds, and the intro captures the mood Syu and Yama-B intended to set *perfectly*.

## 2005: *Advance to the Fall*

I'm not sure who did the cover on this album. It's more minimalistic than Yoshitaka Amano's cover for *The Flag of Punishment*. The face looks like something Amano would do, though.

*Advance to the Fall* is where keyboardist Yuhki really seems to stand out with his piano work, especially in "Stillness Dawn", "Silent Revelation" (whose opening feels like a quotation from an older piece), "Deep Affection", "Eternal Regret", and "Fly With Red Winds". This last is an almost cinematic piece, and one of the album's three instrumentals along with "Stillness Dawn" and "Glorious Aggressor".

Speaking of "Glorious Aggressor": now there's an instrumental that lives up to its name. It is indeed aggressive. Syu doesn't waste a beat, but grabs the listener by the throat and pushes them up against a wall from the first chord.

## 2006: *Beyond the End of Despair*

Here's the first of many dragon covers that we'll see on Galneryus albums. It's to be expected. This *is* power metal, and it beats the hell out of Yama-B sticking the phrase "so far away" in every other song the way Dragonforce seemed to do in their early albums.

If you buy a single Galneryus album, this is the one you should get. This is an album you can just put on repeat all day without a single track getting stale. It's also one of my go-to albums when I need to get into a flow state and write.

The opening and closing instrumentals, "Arise" and "Rebirth..." are perfect bookends for the other ten prime cuts of heavy metal you'll find in *Beyond the End of Despair*. The closest this album has to skippable tracks is "Braving Flag", and it's a great song on its own merits. It's just that it seems weak compared to tracks like "Shriek of the Vengeance", "Raid Again", "In the Cage", and "My Last Farewell".

If you like power metal, you will love this album. I guarantee it.

## Galneryus ver. 1.1

2006's *Beyond the End of Despair* was a tough act to follow, and I suspect that vocalist/songwriter Yama-B was getting burned out by the demands of touring and the need to continually surpass previous efforts. Galneryus released two LPs during this period, which ended with Yama-B's departure, as well as the double A-side single *Alsatia/Cause Disarray*.

* 2007: *One for All&mdash;All for One*
* 2008: *Reincarnation*

While I suspect burnout, Wikipedia has the following to say concerning Yama-B's reasons for leaving Galneryus:

> According to Yama-B's note left to the fans on Galneryus' official site, he left due to the band's growth and change. In the note, Yama-B stated that due to differences in opinion on the band's style of music, he was leaving. He and the rest of Galneryus are all okay with this and wished each other well as they went their separate ways. Yama-B later elaborated in 2013, recalling that while he and Syu had different musical tastes from the beginning, with every release the band slowly chased after a new sound "instead of doing music we loved". While Yama-B "likes to stick to a certain set of rules, concepts", particularly Syu would spontaneously abandon a style in order to incorporate new inspirations.

## 2007: *One for All&mdash;All for One*

And here we have a new logo for the band, and Japanese lyrics for the first time in the band's history.

This was the first Galneryus album I bought, back when I was still using a Mac and an iPod and thus had no reason not to use iTunes. These days I see this album as similar to Metallica's *...And Justice For All*, even though Yama-B is still with Galneryus as vocalist whereas Metallica had to bring bassist Jason Newstead on board because of Cliff Burton's untimely death.

I make this comparison because *All for One - One for All* as a whole is similar to *...And Justice For All*. They're both solid but uneven follow-ups to nearly-perfect albums. Yes, I'm comparing *Beyond the End of Despair* to *Master of Puppets* by implication.

The musicianship is still excellent, but the English-language lyrics seem to suffer compared to previous efforts. The opening and closing instrumentals are rather mellow for a metal album, and the presence of four filler tracks in a row ("Aim at the Top", "Everlasting", "Last New Song" and "Don't Touch") is disappointing in light of the band's first three albums.

## 2008: *Reincarnation*

If you look up the songwriting credits, you'll find they're more diverse this time, with guitarist Syu writing the music to only half this album's songs. *Reincarnation* also continues the trend of songs with Japanese lyrics mixed with English-language songs. And unlike previous albums, *Reincarnation* doesn't begin and end with instrumental tracks.

## 2008: *Alsatia/Cause Disarray* (Single)

*Alsatia/Cause Disarray* is a 2008 double A-side single whose title tracks were used as the opening and ending themes of the six-episode anime [*Mnemosyne*](http://en.wikipedia.org/wiki/Mnemosyne_%28anime%29).

"Wings" and "The Awakening" aren't terrible songs, but they don't really hold their own against the blistering assault of "Alsatia", whose opening riff I use as my phone's wake-up alarm. Even "Cause Disarray" can't match this EP's opener, though it makes a respectable effort.

## Galneryus ver. 2.0

With the departure of vocalist and songwriter Yama-B, Syu had to find a successor. In 2009 found one in veteran performer [Masatoshi "Sho" Ono](http://en.wikipedia.org/wiki/Masatoshi_Ono). In the process completed the band's shift from English-language lyrics to songs written primarily in Japanese.

This isn't as problematic as listers unfamiliar with Germany's Rammstein might suspect. You don't really need to understand the lyrics unless you're at a show and want to sing along. Unfortunately, Sho's vocals seem to lack the clarity of Yama-B's, and his English-language lyrics are often a cheesier than his predecessor's.

The Galneryus 2.0 lineup is still going strong, with the following full-length studio albums to their credit.

* 2010: *Resurrection*
* 2011: *Phoenix Rising*
* 2012: *Angel of Salvation*
* 2014: *Vetelgyus*

I'm not familiar with the band's most recent work, but *Resurrection* and *Phoenix Rising* are similar in style and quality to 2007's *One for All - All for One*.

## 2010: *Resurrection*

Hey, we got another dragon cover! A phoenix might have been more appropriate, but we'll get there shortly.

*Resurrection* is an album on par with 2007's *All for One - One for All*. It sticks to the opening and closing instrumentals formula, has mainly Japanese lyrics, and a few filler tracks, though your mileage may vary. "United Blood" ties into "Burn My Heart" the way Judas Priest's "The Hellion" segues into "Electric Eye" on Priest's 1984 album *Screaming for Vengeance*. "Carry On" shares its title with a song by Brazilian power metal band Angra's 1993 debut, *Angels Cry*, but no other similarities. "Destinations" is a ballad, and not half bad thanks as usual to Syu's music. "Emotions" is an instrumental similar to "Requiem" from *The Flag of Punishment*, and one of the best tracks on the album.

## 2011: *Phoenix Rising*

Remember how I said that a phoenix would have been more appropriate for the first Galneryus album with Sho on vocals? Here we go.

I have a copy of this album, and I've tried listening to it a few times, but I've never really gotten into it. This might be one of those LPs that grows on listeners if you give it time. The cover's gorgeous, though.

## 2012: *Angel of Salvation*

You know what? I don't think I've ever tried listening to this album even though it's on Spotify. The intro for "Reach to the Sky" sounds pretty cinematic, however. I'm playing it now. :)

You can find more information about [*Angel of Salvation* on Wikipedia](http://en.wikipedia.org/wiki/Angel_of_Salvation).

## 2014: *Vetelgyus*

This is another album that I don't think I've listened to recently enough to offer a reasonable opinion. I suspect the title is a variation on [Betelgeuse](https://en.wikipedia.org/wiki/Betelgeuse).

You can find more information about [*Vetelgyus* on Wikipedia](http://en.wikipedia.org/wiki/Vetelgyus).

## Galneryus on the Web

The following sites have more information on Galneryus.

 * [JaME U.S.A: Exclusive Interview with Syu (2006)](http://www.jame-world.com/us/articles-1503-exlusive-interview-with-syu.html)
 * [Wikipedia: Galneryus](http://en.wikipedia.org/wiki/Galneryus)
 * [Galneryus Band Website (Most content in Japanese)](http://galneryusyumacher.com/)
