Touch The Fishy?
################

:date: 2016-01-20 06:56
:summary: Seen on antisocial media. This cat *really* wants to touch the fishy.
:tags: cat, fish, touch, fishy, gif, animation, imgur


Seen on antisocial media. This cat *really* wants to touch the fishy. 

.. raw:: html

  <noscript>
    <p>The embed below might not work without JavaScript. Here's a link to the <a title="'Touch the Fishy' on Imgur" href="https://imgur.com/gallery/UQ2rrVf">touch the fishy</a> video.</p>
  </noscript>
  <blockquote class="imgur-embed-pub" lang="en" data-id="UQ2rrVf"><a href="//imgur.com/UQ2rrVf">Touch tha fishy</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

I found this on `Google+`__. I'm laughing my ass off here.

.. __: https://plus.google.com/+KarenConlin/posts/2RgzFcMedmf
