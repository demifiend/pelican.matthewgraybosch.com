---
title: "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 5"
excerpt: "Welcome back to my readers' guide to *The Alchemists' Council* by Cynthea Masson, with guest commentary by Eric Higby. This post covers the rest of the chapter, pages 164-174."
header:
  image: bee-649952_1280.jpg
  teaser: bee-649952_1280.jpg
  caption: "[Source: Pixabay](https://pixabay.com/en/bee-flying-flower-insect-fly-649952/)"
coauthor: ehigby
toc: true
categories:
  - Books
  - Longform
tags:
  - Cynthea Masson
  - The Alchemists' Council
  - ECW Press
  - Canada
  - 2016
  - literary fantasy
  - intrigue
  - politics
  - alchemy
  - environmentalism
  - intention
  - ego
  - Buddhism
  - urban fantasy
  - portal fantasy
  - commentary
  - read-through
  - read-along
  - reader's guide
  - Eric Higby
  - Chapter 3
  - Part 5
  - Kabbalah
  - Qliphoth
  - the Left-Hand Path
---
Rebel Branch alchemists seem to get caught rather easily, as we'll see in the last four scenes of Chapter III. However, elder members of the Alchemists' Council who have dedicated themselves to rooting out Rebels aren't terribly subtle, either.

I'll be showing how Kalina got caught and the circumstances of her erasure in this post, which ties into the end of Chapter II where Laurel regains memories of her weekend at the spa in Vienna with Kalina. Expect analysis for Chapter IV starting on July 18, as I'm taking some paid vacation in between projects at Deloitte (my day job).

**THE ALCHEMISTS' COUNCIL FORBIDS YOU TO READ ANY FURTHER.**

**THE REBEL BRANCH ENCOURAGES YOU TO CONTINUE, BUT THE CHOICE IS YOURS...**

### Obeche's Zeal

While Kalina has tried to be mindful of the time, and is careful to return to Council dimension with Jaden before their absence is noticed, she wasn't prepared for Obeche. Switching to Cedar's viewpoint for this scene (starting on page 164), we find her heading back from her visit with Sadira to her rooms in the Novillian Scribes' wing.

Obeche is in her way. Knowing what we know of his character, particularly his adversarial relationship with Cedar and his obsession with rooting out Rebel subversion, I imagine him saying, "I would have expected you to be in your chambers at this hour," is not an observation. Rather, it seems like he wants to demand an explanation of her, but doesn't dare.

Were he a Rowan, an Azoth, or Azoth Magen himself he might pull rank, but he's Cedar's equal at most and must choose his words with care. Considering Obeche's officious behavior, Cedar's reply, "Yet once again I defy expectations," is positively restrained.

Despite Cynthea Masson's economy of words, I think unpacking Cedar's line reveals additional shades of meaning. I think Cedar is also saying to Obeche, "I will continue to run roughshod over your expectations, because there is nothing you can do to enforce them upon me."

If there's an unspoken contest of wills in progress here, Obeche loses and acknowledges his loss by changing the subject. Kalina is gone. After the meeting, Obeche came to the Initiates' wing, knocked several times on Kalina's door, and got no response. So what did he do?

He opened the door anyway. Cedar remonstrates, insisting Obeche had no right, but Obeche is unrepentant. Azoth Ruis requested he monitor both Kalina and Tesu, and he suspects Kalina may be in danger (p. 165), so he has all the justification he needs.

He's just following orders, just as if he were the subject of a Milgram experiment.

Not even Cedar's accusation concerning his true motives, that he suspects Kalina of endangering the council, can deter him. As a member of the Elder Council, he considers himself justified in intruding upon an initiate whether she appears to present a danger to herself or the Council.

Worse, he insists that Cedar join him in searching for Kalina. Or, at least, he demands she wait there in the hallway with him for Kalina to return to her rooms whereupon he'll grill her over her nocturnal adventures. Not even Cedar's rational explanation, that Kalina is in one of the libraries catching up, is enough to allay Obeche's suspicions.

Yet, that is just the explanation Kalina offers when she finally does return on page 166. Though Obeche tries to catch her in a lie by asking specific questions concerning her studies, Kalina is evidently prepared, and answers all of Obeche's questions with ease.

No doubt Obeche wants to accuse her at this point, but Kalina's given him no cause. Instead, she asks him what's wrong, putting him on the defensive. Cedar realizes the extent of Obeche's miscalculation, noting after Kalina slips into her chambers that Obeche just tipped his hand, warning Kalina to do her rebel work more discreetly.

Obeche's unrepentant, and insists he may have prevented the Fourth Rebellion. Cedar, perhaps a more astute student of history, or perhaps less biased by zeal, notes that the Fourth Rebellion cannot be prevented. It can only be delayed.

I suspect, however, that it's also possible to *precipitate* the Fourth Rebellion. If so, Obeche's zeal may prove one of the major causes. As we've seen in preceding scenes and in this one, he expresses his sense of duty to the Alchemists' Council through constant vigilance. He's on a constant lookout for any hint of Rebel Branch activity, regardless of evidence. As a member of the Elder Council, Obeche's self-appointed mission to root out the Rebel Branch may be one of the abuses Dracaen spoke of when explaining the Rebel cause to Jaden.

How many alchemists has Obeche *driven* to rebel? It's hard to tell, but I find Obeche's example applicable to real-world politics. If you're looking for rebels, you'll find them though they might be rebels of your own making.

### Passing Notes

The next scene is a short one (pp 167-169) from Jaden's viewpoint. However, it covers some plot points that will prove important very soon.

First, Jaden found her piece of the Dragonblood stone. It was right where she left it, in her old jacket. She transfers it to a deep pocket in her robes, where she believes it will be safe. We'll come back to this soon.

Next, while Jaden is careful not to approach Kalina, she still wants the Senior Initiate to know that she found her piece of the stone and therefore remembers their meeting in Rebel dimension.

However, the class they share together isn't the time or the place. Not when Obeche interrupts and announces the winners of the winter quarter award for academic achievement. Laurel and Kalina get the prize: three days at a spa in Vienna.

Remember how in Chapter II Laurel had a vague memory of spending a few days at a Viennese spa with Kalina? It happened. I suspect, however, that it's a ruse to get Kalina out of Council dimension for a few days. More on *that* later, too.

Jaden never got to talk to Kalina about finding her piece of the stone, but Kalina managed to pass her a note in the courtyard later that afternoon. It's nothing explicit, just a reference to *Sapientiae Aeternae 1818*. No doubt Jaden would find more explicit information somewhere in that codex.

Kalina's technique is similar to dead drops used by real-world spies, but suffers from a couple of flaws. Kalina should have told Jaden about *Sapientiae Aeternae 1818* while in Rebel dimension if she had to tell the younger woman herself. Dropping a scrap of paper is not only obvious, but what if an errant breeze had blown it away from Jaden and into unfriendly hands (like Obeche's)?

Unfortunately for Kalina, she's no spy. We'll find in the next scene just how badly Kalina's lack of tradecraft betrays her.

### How Kalina Got Made

While it would have been interesting to see the action of pages 169-172 from Kalina's viewpoint, we instead witness Kalina's return from her holiday with Laurel through Cedar's eyes.Cedar awaits the young Initiates in the Hotel Sacher in Vienna, so she can escort them back to Council dimension.

While Kalina possesses the power to make the return trip on her own, the Council's trust only extends so far. Give Senior Initiate pendants to keep them within Lapidarian proximity at all times? No problem. Letting them travel between Council dimension and the outside world is another proposition altogether.

Besides, there's another reason for Cedar's presence. The holiday in Vienna wasn't all chocolate massages and other indulgences. The real objective, implied by Cedar's observation that Kalina might be disoriented from getting dosed with Lapidarian Amrita, was to give Kalina rope and see if she'd hang herself.

Laurel has no idea what's going on, and happily takes the trip at face value. She talks Cedar's ear off about the fun she had at the spa as Cedar escorts her and the quiet Kalina back to the Council protectorate, and from there back to Council dimension through the Quercus gate.

Upon their arrival at the protectorate, a twenty minute walk from the Hotel Sacher, we learn that Cedar consulted with Linden before she begins escorting the initiates back to Council dimension. We aren't privy to the details, but I wouldn't be surprised if Cedar warned Linden that Kalina might attempt to escape while Cedar's busy escorting Laurel back to Council dimension.

If so, it's reasonable to wonder why Cedar didn't just escort Kalina back first. I think there are a couple of reasons:

1. Escorting Laurel back first returns her to the safety of Council dimension faster.
2. The setup in Vienna is probably more delicate than it appears. Laurel had to go with Kalina, and she had to be ignorant of the real reason for the holiday, otherwise Kalina might have smelled a rat. She might have taken the opportunity to escape Council control before they could strip her of whatever power she had accrued thus far.

In any case, once Laurel is back in Council dimension she thanks Cedar and then runs off to find Cercis. Cedar then brings Kalina back to Council dimension, where she finds a most unwelcome welcoming party awaiting her. Obeche is there, along with both Rowans, Kai and Esche.

And what does Obeche do? Only what he's probably wanted to do for a few days now: he grabs Kalina by the arm and intones, "By order of the Alchemists' Council, you are hereby accused of high treason."

Now, one might think Obeche's use of "high treason" instead of "treason" is just a rhetorical flourish, or for dramatic effect. I don't think that's the case. English common law once distinguished between high treason and petty treason.

High treason was treason against the state, the sort of treason explicitly defined in Article III, Section 3 of the Constitution of the United States to ensure it was never used as a catch-all charge. Petty treason, a crime now abolished, denoted treason against one's lawful superior -- such as a servant murdering their master.

So, if petty treason doesn't exist any longer in countries that inherited English common law, what does that make high treason? Well, Canada still distinguishes between treason and high treason. One of the major differences between treason and high treason in Canada is whether the crime was allegedly committed while the country was at war.

Of course, I could be reading too much into the fact that Cynthea Masson is a Canadian author and might be aware of the difference between treason and high treason under her country's laws. It's also possible that Obeche is being pompous, *or* that because the Alchemists' Council is so old and steeped in tradition that it tends toward archaic usage in its proceedings.

Once Obeche has accused Kalina he, the Rowans, and Cedar forcibly escort her through the Council's back corridors to the Azothian chambers where the rest of the Elder Council had assembled. Once everybody was seated, Obeche makes his case. (p 171)

As Cedar suspected, Obeche managed to slip Kalina some Lapidarian Amrita. A pastry Kalina ordered in Vienna had been infused with the stuff. It probably wasn't hard to bribe the hotel cooks, if necessary, though I have to wonder why Kalina didn't notice her treat had been tampered with. Is Lapidarian Amrita tasteless and odorless? Did Kalina lack any alchemical means of detection?

Regardless, the Amrita allowed Obeche to track Kalina's movements and photograph her as she met with Dracaen. It also let him follow and photograph her as she snuck into the Vienna protectorate, slipped past security, and got her hands on Lapidarian ink and some of the manuscripts in the southeast sector.

Looks like the Readers and Scribes are going to be working overtime.

Most damning of all is the evidence Azoth Ravenea finds in Kalina's pendant: its memory had been wiped, which she describes as the work of an alchemist skilled with Dragon's Blood tonic. Ravenea's recommendation? Complete erasure.

It isn't until Azoth Magen Ailanthus gives the order to gather up all manuscripts pertaining to Kalina to begin the erasure process that Kalina speaks. She accuses Cedar of betraying her, an Initiate Cedar herself had brought over.

Cedar knows better than to show any sympathy, and tells Kalina she betrayed herself. Obeche, secure in his triumph, can't resist observing that all Rebels eventually betray themselves.

Another aside before we cover the last scene in the chapter. In previous chapters we've seen Lapidarian ink and Lapidarian honey, each possessing alchemical properties granted by the Lapis, but what is Lapidarian Amrita? All we know is that whoever ingests it can easily be tracked.

However, the original Sanskrit word from which "amrita" is derived shares etymological origins with the Greek word "ambrosia" because Sanskrit and Greek are Indo-European languages. Both words share the same meaning. Both amrita and ambrosia are food for the gods. It first occurred in the *Rigveda*, whose author used "amrita" as a synonym for *soma*, the drink that confers immortality upon the gods.

I doubt that Lapidarian Amrita is so potent, but it served its purpose. It helped Obeche gather the evidence he needed to brand Kalina a rebel and a traitor.

### Jaden's Loss

We close the chapter with Jaden's viewpoint. She's on her way back to the North Library via a shortcut, where she intends to examine "for the third time in as many days" (p. 173) folio 16 of *Sapientiae Aeternae 1818*. Whatever message Kalina intended for Jaden to find there remains hidden, and it will soon be too late.

On her way, she ran into Kalina, Obeche, and Cedar outside the portal chamber. Obeche demands of Jaden an explanation, and Jaden tells him part of the truth; she's on her way to the library.She hasn't read the situation before her, as we see from the following passage:

> ...She then turned to Kalina, "How was your time in Vienna?"

> "Delightful," replied Kalina flatly.

> "Will you be busy later?" Jaden asked Kalina. "I could use some help with my lesson review."

> "Yes, she is busy," answered Obeche. "She is headed to the outside world."

> "Again?"

At this point, Jaden has no idea that Kalina has been judged a traitor. Kalina, for her part, remains calmly defiant.

> "No worries, Jaden. I will return."

> Obeche slapped Kalina across the face. "You will not return!"

At this point, Jaden reaches out to defend Kalina, and why wouldn't she? All Jaden knows is that a man just turned and hit a woman she considered a friend. Obeche, however, is too angry to think straight and turns his fury on Jaden, whom he grabs by the collar and wrenches away from Kalina.

Jaden falls beside one of the corridor benches, her robes snagging on the bench's wrought iron embellishment. Though Cedar and Kalina help her up, the damage is done. Jaden has lost her pouch containing the Dragonblood Stone.

She flees under threat of an official reprimand from Obeche, intending neither to go far nor to stay away long, but it's too late. Lapidarian proximity had already begun the work of altering her memories. First, she forgot her desire to return for the stone. Next, she forgot about *Sapientiae Aeternae 1818*. Finally, she forgot about Kalina.

But has Kalina forgotten about Jaden? Has Dracaen? We'll see...

Stay tuned for "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 4". If you want more of the Rebel Branch Initiates' Guide, [check out the main page](/commentaries/the-rebel-branch-initiates-guide-to-the-alchemists-council/).
