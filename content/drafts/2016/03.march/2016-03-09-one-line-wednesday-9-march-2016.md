---
id: 2996
title: 'One Line Wednesday: 9 March 2016'
excerpt: "Every Wednesday, the #1LineWed hashtag trends on Twitter. Sometimes I participate."
date: 2016-03-09T06:22:38+00:00
header:
  image: starbreaker-adversary-symbol.jpg
  teaser: starbreaker-adversary-symbol.jpg
categories:
  - Outtakes
tags:
  - '#1LineWed'
  - challenge
  - one sentence
  - Twitter
  - writing
  - Starbreaker
---
{% include tweet.html %}

Every Wednesday, Twitter has a #1LineWed hashtag trending. The idea is to share one line, a sentence or two, that evokes interest in your story. Here are mine. They moistly come from Starbreaker stories I haven’t written yet. I’m reclaiming them from Twitter so they don’t get lost in the noise.

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Whereas if Naomi and I walked in with a warrant," said Morgan, "Murdoch would stall us until all the evidence was gone." <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707524695206305792">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Sid cracked his knuckles. "I can find a single worker willing to testify, we can shut the corporation down before any cleanup." <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707524380578947072">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Murdoch might suspect, if she knows we're friends, but can't refuse a routine employee inspection." <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707523373211975681">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "Sure we do, for all the good it does us." Naomi shook her head. "Magnin's running everything, but we can't prove that, either." <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707522787632652288">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Eddie burst into a coughing fit. "Why the worker exploitation angle? Don't we already know about the devil-killer rifles?" <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707522285054390272">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    "After all, I'll be citing your inspection as probable cause for my warrant to rip Murdoch's operation apart." <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707515219040464896">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Once his friend's expression softened, Morgan continued. "You know me better than that. If you find anything, the credit's yours." <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707514384902463488">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Morgan shook his head. "Actually, it's because if any of us is likely to spot worker exploitation, it's you." <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707513905971654656">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Sid narrowed his eyes, and gave a sarcastic laugh. "Oh, sure. Make the black guy do all the work while you get the credit?" <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707513622692552704">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Good idea, Naomi. Sid, would you be willing to do working-conditions inspection mission if Malkuth could arrange it? <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707513103227953154">March 9, 2016</a>
  </p>
</blockquote>

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    Better not count on Bathory's letter of marque to get us into the Murdoch factory, Morgan. Not if you want admissible evidence. <a href="https://twitter.com/hashtag/1linewed?src=hash">#1linewed</a>
  </p>

  <p>
    &mdash; Matthew Graybosch (@MGraybosch) <a href="https://twitter.com/MGraybosch/status/707511847629557760">March 9, 2016</a>
  </p>
</blockquote>
