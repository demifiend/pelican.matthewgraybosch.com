---
id: 3584
title: 'Blackened Phoenix: Main Plotline and Cast'
date: 2016-03-29T05:01:50+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3584
permalink: /2016/03/blackened-phoenix-main-plotline-and-cast/
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"40c9a152f9e2";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:84:"https://medium.com/@MGraybosch/blackened-phoenix-main-plotline-and-cast-40c9a152f9e2";}'
categories:
  - Blackened Phoenix
  - Design Works
  - Starbreaker
tags:
  - cast
  - conflict
  - design
  - main plotline
  - pre-writing
---
I'm taking a huge risk here by letting you guys come back stage and see how I make the sausage for my next book, _Blackened Phoenix_. I'm not really worried about anybody ripping off my ideas, since they can't and won't do the voodoo like I do, but by posting this stuff online I'm making a public commitment.

## Main Plotline

Morgan and Naomi seek evidence against Imaginos, who appears to be using the Phoenix Society for his own nefarious ends, while fending off new threats from anti-Phoenix Society extremists, the new prototype asura emulator Polaris, and the newly unbound and vengeful demon Sabaoth.

## Cast

  * Morgan Stormrider
  * Naomi Bradleigh
  * Edmund Cohen
  * Sid Schneider
  * Claire Ashecroft
  * Josefine Malmgren
  * Sarah Kohlrynn
  * Christabel Crowley (deceased?)
  * Annelise Copeland
  * Astarte
  * Mordred
  * Saul Rosenbaum
  * Iris Deschat
  * Munakata Tetsuo
  * Alexander Liebenthal
  * Nakajima Chihiro
  * Victoria Murdoch
  * Gregory Windsor
  * Malkuth
  * Kether
  * Gevurah
  * Binah
  * Tiphareth
  * Imaginos (Isaac Magnin)
  * Thagiron (Tamara Gellion/Theresa Garibaldi)
  * Ashtoreth (Elisabeth Bathory)
  * Adramelech (Abram Mellech)
  * Sathariel (Samuel Terell)
  * Sabaoth (pretends to be Yahweh/Jehovah/Allah)
  * Polaris
  * Vincent Rubicante
  * Catherine Gatto