---
id: 3415
title: Write First. Then Research.
date: 2016-03-24T06:29:57+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3415
permalink: /2016/03/write-first-then-research/
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - How Not to Write
tags:
  - contrarian
  - fact-checking
  - research
  - Ronnie James Dio
  - verisimilitude
  - when should I research
  - when to research
  - write what you know
  - "writer's block"
  - writing
---
My wife Catherine asked me an interesting question on our way home from work this evening: _When do you research?_ She had started writing again after reading an egregious novella she bought off Amazon. It was apparently an erotic romance involving a werefox, and Catherine was convinced she could do better.

There was just one small problem: she was already getting bogged down. She wanted to set her story out West, but had never been there, didn't know much about wild horses or ranching, and was afraid more knowledgable readers would find her out if she didn't get her facts straight _before_ she started writing. This fear threatened to keep her from writing anything at all.

It no doubt stops a great many writers dead in their tracks. Hell, it's even stopped me from time to time. However, I've always tried not to let the need to get my facts straight stop me for long. My solution is simple: **Write first. Then research.**

This may seem counterintuitive, if not outright crazy&mdash;especially for writers of science fiction and fantasy. However, I've found that writers who take ["write what you know"](https://litreactor.com/columns/write-what-you-like-why-write-what-you-know-is-bad-advice) too literally either write nothing at all, or wind up writing literary fiction.

So, I ignore that advice. I don't need to know how a Gauss rifle works to use it in a first draft of a novel like [_Without Bloodshed_](http://www.amazon.com/Without-Bloodshed-Starbreaker-Series-Book-ebook/dp/B00GQ0BJOO). I just need to know how it fits into my story. I just need to know what effect it's likely to have on my characters and the plot. I can leave notes in my text and come back to it during revision.

Most importantly, I just need to know it's [friggin' awesome](http://tvtropes.org/pmwiki/pmwiki.php/Main/RuleOfCool). That's right. I go by what I think is cool. I advise you to do the same. Don't write what you know. Write what makes you want to _rock out with your cock out_. (If you don't have a cock, strap one on.)

Under ideal conditions, you should be typing with _one_ hand while throwing the horns with the other. If you don't know how to throw the horns, let the late Ronnie James Dio show you how it's done.<figure style="width: 512px" class="wp-caption aligncenter">

[<img src="http://i1.wp.com/upload.wikimedia.org/wikipedia/commons/thumb/2/22/Dio_throwing_Horns.jpg/512px-Dio_throwing_Horns.jpg?resize=512%2C583&#038;ssl=1" alt="By rjforster from Worcester, UK (Heaven And Hell img_7339.jpg) [CC BY 2.0 (http://creativecommons.org/licenses/by/2.0)], via Wikimedia Commons" class data-recalc-dims="1" />](http://i0.wp.com/commons.wikimedia.org/wiki/File%3ADio_throwing_Horns.jpg?ssl=1)<figcaption class="wp-caption-text">Ronnie James Dio throwing the horns. Photo by rjforster from Worcester, UK, via Wikimedia Commons</figcaption></figure> 

Get the story written first. Work out the plot. Develop your characters. Get them interacting. Put them into conflict. Once you have a first draft completed, there will be _plenty_ of time for research.

In fact, one of the purposes of revision is to get your facts straight and verify that your story is sufficiently plausible to allow the reader to maintain a [willing suspension of disbelief](https://en.wikipedia.org/wiki/Suspension_of_disbelief). It doesn't necessarily matter whether dragons are real as long as you describe them consistently throughout your story. However, if you're writing a story about a dragon terrorizing a country and a hero who takes down the dragon with an anti-materiel rifle, it's probably a good idea to know how such a weapon works.

To that end, allow me to propose a few principles of verisimilitude for writers&mdash;as pretentious as that might sound:

  1. The author must be prepared to justify every deviation from reality in their work when questioned by readers.
  2. The author's research should not be visible in the story unless it is relevant to characterization or plot.
  3. Details that appear irrelevant will trip you up every single time.

So, write the story and make it awesome. That's the _first_ step. Getting the facts straight so that the story doesn't fall apart at the first hint of scrutiny is the _next_ step.

#### Image Credit

[Writer Cat by Cassandra Leigh Grotto](https://www.flickr.com/photos/threecheersformcr_xo/5340309206)