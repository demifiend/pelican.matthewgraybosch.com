---
id: 3408
title: How to Automate Jekyll Site Deployment on OSX
date: 2016-03-24T06:17:33+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3408
permalink: /2016/03/how-to-automate-jekyll-site-deployment-on-osx/
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Running Mewnix
tags:
  - automate
  - automation
  - Jekyll
  - launchd
  - octopress
  - OSX
  - Rakefile
  - rsync
  - ssh
  - static
  - static blog generator
  - website
---
[Jekyll](http://jekyllrb.com) is great for generating static blogs, but if you want automatic deployment you need to follow the Unix way. Here's how I did it.

## Update: 24 March 2016

Obviously I'm not using Jekyll any longer, but I'm restoring this post as a resource for people who want to tinker with static website generation.

## Why Not GitHub Pages?

You're probably wondering why I don't just host on [GitHub Pages](https://pages.github.com). That's a fair question. I could do that, but that would mean playing by GitHub's rules. This site isn't for a development project, so it isn't quite what GitHub Pages was intended to host.

I don't _think_ they'd boot me, but why take the chance? Renting my own virtual machine from [Dreamhost](http://dreamhost.com) makes me a paying customer instead of a freeloading guest. Besides, I register my domains through them and I've had no trouble hosting with them, so I might as well stick with them.

## Prerequisites

With git not an option for deployment, following the Unix way is a little more complicated. Since I already use Jekyll and similar Ruby gems, I start by installing [Octopress](https://github.com/octopress/octopress) and [Octopress Deploy](https://github.com/octopress/deploy).

Octopress Deploy allows me to define a configuration that it can use to run rsync when I invoke the `octopress deploy` command inside my site's project directory.

With Octopress configured, manual deployment of a Jekyll site is as easy a running two commands. I can even chain them together like so.

[shell]
  
jekyll build && octopress deploy
  
[/shell]

But if I want automatic uploads to Dreamhost, I need additional setup. In particular, I need to create SSH keys and have them mirrored on my virtual server. [This is where `ssh-keygen` and `ssh-copy-id` come in.](http://www.lindonslog.com/linux-unix/ssh-keygen-keys/)

Unfortunately, ssh-copy-id isn't part of OS X by default, but that's why we have [Homebrew](http://brew.sh). Running `brew install ssh-copy-id` will sort things out.

## Implementing Automatic Deployment

Since I'm using a Macbook, I followed instructions specific to [launchd provided by Chris Hulbert](http://www.splinter.com.au/using-launchd-to-run-a-script-every-5-mins-on/). I also used commands specific to OS X to determine whether or not wifi is up.

If you're using a different Unix system, you can probably get away with just setting a [cron job](http://www.unixgeeks.org/security/newbie/unix/cron-1.html). To check wifi state, you'll probably want to try `iwgetid`.

To begin with, here's the shell script I used. It's quite simplistic, really.

[shell]
  
!# /usr/bin/env bash

export CURRENT&#95;SSID=`/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk '/ SSID/ {print substr($0, index($0, $2))}'`

if [ -n CURRENT&#95;SSID ]
      
then
          
cd ~/Sites/jekyll-website-folder
          
jekyll build
          
octopress deploy
          
cd ~
  
fi
  
[/shell]

Making this script executable with `chmod +x` and placing it somewhere in my $PATH makes it immediately useful, but I'm not done yet. I need to make launchd run it at a reasonable interval.

The first step is to create a plist file, which is just XML with a different extension. Yes, I know XML is crappy for config files. Take it up with Apple. 🙂

[xml]
  
<?xml version="1.0&#8243; encoding="UTF-8&#8243;?>
  
<plist version="1.0"> <dict>
      
<key>Label</key>
      
<string>com.REDACTED.website.upload</string>
      
<key>ProgramArguments</key>
      
<array>
          
<string>/Users/REDACTED/bin/run-jekyll-upload.sh</string>
      
</array>
      
<key>StartInterval</key>
      
<integer>1800</integer>
  
</dict> </plist> [/xml]

The StartInterval value is a count of seconds, so setting it to 1,800 means that our shell script will run every half hour. With the plist file created, we're ready for the final steps.

## Final Steps

The next step is to place this file in the proper location for launchd to run it while the current user is logged in.

[shell]
  
sudo cp com.REDACTED.website.upload.plist /Library/LaunchAgents
  
[/shell]

Enabling it is as simple as typing another command. This will update launchd in place, making a reboot optional.

[shell]
  
sudo launchd load -w com.REDACTED.website.upload.plist
  
[/shell]

If you change your mind, you can always disable automatic deployment by either turning off wifi, or running the following command.

[shell]
  
sudo launchd unload com.REDACTED.website.upload.plist
  
[/shell]

## Bonus Mode

Of course, simply building the site and uploading it might not be enough. Maybe you want to send a remote procedure call to Ping-O-Matic every time you post. Maybe you want to minify your HTML, JavaScript, and CSS. Maybe you've rigged your Jekyll install to generate sitemaps using the jekyll-sitemap gem, and want to give Google (and Bing, I suppose) a heads-up.

You could do that in the shell script I provided earlier, but I've been using the following Rakefile, which is just a makefile for rake (Ruby Make) that uses Ruby instead of [M4](https://en.wikipedia.org/wiki/M4_%5C(computer_language%5C)). You need only replace the `jekyll build` and `octopress deploy` commands in your shell script with `rake deploy`.

[ruby]
  
#&#35;###########&#35;
  
\# Jekyll tasks
  
#&#35;###########&#35;

\# Usage: rake serve
  
desc "Serve Jekyll site locally"
  
task :serve do
    
system "bundle exec jekyll serve watch &#8211;config &#95;config-dev.yml"
  
end # task :serve

\# Usage: rake build
  
desc "Build production Jekyll site"
  
task :build do
    
system "bundle exec jekyll build"
  
end # task :build

\# Usage: rake drafts
  
desc "Build local Jekyll site with &#95;drafts posts"
  
task :drafts do
    
system "bundle exec jekyll build &#8211;drafts &#8211;config &#95;config-dev.yml"
  
end # task :drafts

#&#35;###############&#35;
  
\# Production tasks
  
#&#35;###############&#35;

\# Usage: rake minify
  
desc "Minify files"
  
task :minify do
    
puts &#8216;&#42; Minifying files'
    
system "java -jar \_build/htmlcompressor-1.5.3.jar -r &#8211;type html &#8211;compress-js -o \_site &#95;site"
  
end # task :minify

\# Ping Pingomatic
  
desc &#8216;Ping pingomatic'
  
task :pingomatic do
    
begin
      
require &#8216;xmlrpc/client'
      
puts &#8216;* Pinging ping-o-matic'
      
XMLRPC::Client.new(&#8216;rpc.pingomatic.com',
  
&#8216;/').call(&#8216;weblogUpdates.extendedPing', &#8216;matthewgraybosch.com' ,
  
&#8216;https://matthewgraybosch.com', &#8216;https://matthewgraybosch.com/feed.xml')
    
rescue LoadError
      
puts &#8216;! Could not ping ping-o-matic, because XMLRPC::Client could
  
not be found.'
    
end
  
end # task :pingomatic

\# Ping Google
  
desc &#8216;Notify Google of the new sitemap'
  
task :sitemapgoogle do
    
begin
      
require &#8216;net/http'
      
require &#8216;uri'
      
puts &#8216;* Pinging Google about our sitemap'
      
Net::HTTP.get(&#8216;www.google.com', &#8216;/webmasters/tools/ping?sitemap=' +
  
URI.escape(&#8216;https://matthewgraybosch.com/sitemap.xml'))
    
rescue LoadError
      
puts &#8216;! Could not ping Google about our sitemap, because Net::HTTP
  
or URI could not be found.'
    
end
  
end # task :sitemapgoogle

\# Ping Bing
  
desc &#8216;Notify Bing of the new sitemap'
  
task :sitemapbing do
    
begin
      
require &#8216;net/http'
      
require &#8216;uri'
      
puts &#8216;* Pinging Bing about our sitemap'
      
Net::HTTP.get(&#8216;www.bing.com', &#8216;/webmaster/ping.aspx?siteMap=' +
  
URI.escape(&#8216;https://matthewgraybosch.com/sitemap.xml'))
    
rescue LoadError
      
puts &#8216;! Could not ping Bing about our sitemap, because Net::HTTP or
  
URI could not be found.'
    
end
  
end # task :sitemapbing

\# Usage: rake notify
  
desc &#8216;Notify various services about new content'
  
task :notify => [:pingomatic, :sitemapgoogle, :sitemapbing] do
  
end # task :notify

\# Usage: rake rsync
  
desc &#8216;rsync the contents of ./&#95;site to the server'
  
task :rsync do
    
puts &#8216;&#42; rsyncing the contents of ./&#95;site to the server'
    
system &#8216;octopress deploy'
  
end # task :rsync

\# Usage: rake deploy
  
desc &#8216;Build &#95;site, minify files, rsync the files, and notify services
  
about new content'
  
task :deploy => [:build, :minify, :rsync, :notify] do
  
end # task :deploy
  
[/ruby]

If you're already using Jekyll to build static blogs and websites on OS X, you might already know how to do this. Regardless, I hope it proves a useful reference for newcomers. If you'd like to suggest a refinement to my methods, feel free to comment below.