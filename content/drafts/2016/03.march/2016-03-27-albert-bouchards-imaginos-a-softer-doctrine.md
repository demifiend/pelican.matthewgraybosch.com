---
title: "Albert Bouchard's *Imaginos*: a Softer Doctrine"
excerpt: "Listen to the album that might have been if Columbia records hadn't demanded one last Blue Öyster Cult album in 1988."
header:
  image: blueoystercult-imaginos-cover.jpg
  teaser: blueoystercult-imaginos-cover.jpg
categories:
  - Music
tags:
  - Albert Bouchard
  - Sandy Pearlman
  - aliens
  - Blue Öyster Cult
  - bootleg
  - concept album
  - Demo
  - hard rock
  - heavy metal
  - Imaginos
  - occult history
  - rock opera
  - Starbreaker inspiration
  - YouTube
---
I've always loved the _Imaginos_ album released in 1988 as the Blue Öyster Cult's "swan song" (at least until they released _Heaven Forbid_ in 1998), but I know it isn't a _real_ BÖC album.

There's [all sorts of secret history involved](http://thequietus.com/articles/12881-blue-oyster-cult-imaginos-review-anniversary), like lawsuits between band members and record label meddling. This video contains a bootleg copy of former BOC member Albert Bouchard's demo tape for _his_ version of the _Imaginos_ album. There are more songs, and there's none of that "random access history" nonsense.

---

<iframe width="563" height="337" src="https://www.youtube.com/embed/soOHIaSyqtU" frameborder="0" allowfullscreen></iframe>

---

It's pretty damn rough, but I'd say it's 80% there. I still love the official version, since it was the first and inspired my writing. Which do you prefer?
