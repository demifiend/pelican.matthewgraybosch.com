---
title: "Listening to Gramophone's Top 10 Viola Works"
excerpt: When I was a young man, I wanted to play the violin. I got saddled with a viola instead, and nobody told me there was a reportoire of music composed specifically for the violin's alto sister.
header:
  image: viola-551818_1920.jpg
  teaser: viola-551818_1920.jpg
categories:
  - Music
  - Personal
  - Longform
tags:
  - Bartók
  - Benjamin Britten
  - classical music
  - Dmitri Shostakovich
  - Duncan Druce
  - Ernest Bloch
  - Franz Schubert
  - Gramophone
  - György Ligeti
  - imposter syndrome
  - Johannes Brahms
  - Joseph Joachim
  - Luciano Berio
  - magazine
  - Max Bruch
  - music for viola
  - Paul Hindemith
  - repertoire
  - Robert Schumann
  - Rudolph Haken
  - Spotify
  - Telemann
  - Top 10
  - viola
  - William Walton
  - Your Lie in April
---
[_Gramophone_](http://www.gramophone.co.uk/) magazine did a [Top 10 Viola Works](http://www.gramophone.co.uk/feature/top-10-viola-works) back in January 2015 that I only recently discovered. I used to play the [viola](http://www.viola.com/)., so I decided to expand a bit on the original article.

## Yes, Virginia, there _is_ a classical repertoire for viola.

To be honest, however, I didn't simply "discover" this article. I went searching for it, for reasons I'll explain later on.

I don't think music teachers in school orchestras bother telling the kids who got stuck playing the viola anything but silly jokes about violists being [unable to play](http://www.mit.edu/~jcb/jokes/viola.html), or mention the existence of a small corpus of works for violin.

Duncan Druce, writing for _Gramophone_, says otherwise&#8230;

> The viola possesses a history as long and distinguished as that of its fellow violin family members. An essential part of the string consort music of the 16th and 17th centuries, it then took its place in the orchestra and in many of the most popular forms of chamber music. But whereas violinists and cellists can choose from many fine concertos and sonatas, viola players have far fewer solo options. Why should this be so? The lack is sometimes traced to a low standard of viola playing (hence the ubiquitous ‘viola jokes’), but the truth is surely rather different: until comparatively recent times, viola parts were played by violinists, who swapped instruments as occasion demanded, preferring the more brilliant violin for solo performance. Despite this, there is a small pre-1900 repertoire of outstanding viola works, including three items on the list I’ve included here.

## The Works

None of the pieces Duncan Druce recommended are playable from the article, so I thought I'd hunt them down on Spotify and link them in a post of my own. If you know somebody who plays the viola, feel free to share it with them.

### Bartók's Viola Concerto

Druce suggested the ECM New Series recording of Kim Kashkashian (_not_ to be confused with Kim Kardashian) with the Netherlands Radio Chamber Orchestra conducted by Peter Eötvös. Instead, here's Marcus Thompson with the Slovenian Radio Symphony Orchestra conducted by Paul Freeman. This album also includes a suite for viola by Ernest Bloch.

{% include spotify url="https://open.spotify.com/embed/album/02e0P4i4n8o8Zhb39wMA4P" %}

### Ligeti's _Solo Viola Sonata_

Unlike Bartók's Viola Concerto (BB 128), the recommended recording György Ligeti solo viola sonata by Tabea Zimmermann for Sony Classical is available on Spotify. Scroll down to track 21 for the first movement.

{% include spotify url="https://open.spotify.com/embed/album/36ElQvpNHCyCQQtsEPQ7wt" %}

This is modern classical music, composed between 1991 and 1994, so it would no doubt sound different from the work of earlier composers.

### Shostakovich's _Sonata for Viola and Piano_

Dmitri Shostakovich represents a huge gap in my musical education. I had no idea he was active until 1975, completing his final work, a duet for viola and piano, three years before I was born.

It's a sombre, stark revelation; and its second movement is the most striking piece I've heard out of this list thus far. Its final movement's piano part may be familiar to some; Shostakovish was quoting from Beethoven's _Moonlight Sonata_ as an homage to his predecessor.

{% include spotify url="https://open.spotify.com/embed/album/6XP1Jhmo64rJycTv8lfwZO" %}

### Berio's _Sequenza VI_

I haven't heard anything like Christophe Desjardins' recording of Luciano Berio's _Sequenza VI_ for viola since the first time I listened to _Brain Salad Surgery_ by Emerson, Lake, & Palmer and heard Keith Emerson's rendition of Alberto Ginastera's Toccata.

{% include spotify url="https://open.spotify.com/embed/track/1LwHSrKh306QaTHcqidG9A" %}

It's just a single track, but I suspect that the rest of _Alto/Multiples_ may be of interest since it includes a viola sonata by Paul Hindemith.

{% include spotify url="https://open.spotify.com/embed/album/5jMxYsccM6gcDAwPJvWts8" %}

### Britten's _Lachrymae_

Druce's preferred recording of Benjamin Britten's _Lachrymae, Op. 48 "Reflections on a Song of John Dowland"_ isn't available on Spotify. Oddly enough, the services offers plenty of other recordings by Maxim Rysanov, but that's just how the licensing cookie crumbles.

Not being familiar enough with the piece to have a preferred recording, I'm content with Matthew Jones on viola and Annabel Thwaite on piano.

{% include spotify url="https://open.spotify.com/embed/track/55mwj2rk8SBCpoWuaC4gAK" %}

The _Lachrymae_ is the last track on the album, which may merit a listen.

{% include spotify url="https://open.spotify.com/embed/album/4Domt226qgC0HzLbHEh2zn" %}

### Walton's _Viola Concerto_

Being unable to track down Lawrence Power's recording with the BBC Scottish Symphony Orchestra, I must make do with Yuri Bashmet's performance instead. The Bashmet recording also includes work for viola by Max Bruch.

{% include spotify url="https://open.spotify.com/embed/album/7aCwT72Dsl2kEkhNPEwnRj" %}

### Bax's _Viola Sonata_

This is an analog recording from 1929 of violist Lionel Tertis performing Arnold Bax's _Sonata for Viola and Piano_ with the composer as accompanist. As such you'll want to crank up the volume, and you'll just have to deal with the crackle.

{% include spotify url="https://open.spotify.com/embed/album/4VYiwwjDeeHAcAm47nIZeC" %}

### Joachim's _Variations, Op. 10_

The original article specifies Bernard and Naomi Zaslav's recording of the 22 minute _Variations on an original theme, Op. 10_ by Joseph Joachim, but the album on in which the Joachim piece is included _also_ includes two viola sonatas by Johannes Brahms.

{% include spotify url="https://open.spotify.com/embed/track/6sLaJJd7X4wlO7Y8uxF6C7" %}

Why didn't any of my teachers tell me Brahms wrote for viola?

{% include spotify url="https://open.spotify.com/embed/album/41YTtchkClSoSfZcqY3OZo" %}

### Schumann's _Märchenbilder, Op 113_

Robert Schumann's _Märchenbilder, Op 113_ is Duncan Druce's main draw for this recording of Lise Berthaud on viola, but the album also includes sonatas for viola and piano by Schubert and Brahms.

{% include spotify url="https://open.spotify.com/embed/album/0uHQJgxrgPrCbn3DUyjq96" %}

### Telemann's _Viola Concerto in G_

Druce recommends the Arte Nova recording of Peter Langgartner's performance with the Cis Collegium Mozarteum Salzburg directed by Jürgen Geise. That's not available, so I found a recording of Conrad Zwicky with the Playaden ensemble that also includes a seven-movement suite for viola and strings in D Major, a concerto for two violas in G Major, and a concerto for viola in e minor.

{% include spotify url="https://open.spotify.com/embed/album/3xZNnGcnP4e2XB75rVLbmA" %}

## Spotify? Seriously?

No doubt some snobs will scoff at using Spotify to listen to classical music, but if they complain you can send them to me. I'll strap 'em down and blast 'em with choice cuts from Venom's _Black Metal_ album through $5 earbuds to show them just how much worse things can get. It'll be therapeutic. Kinda like this:

{% include base_path %}
![The Ludovico Technique in A CLOCKWORK ORANGE, directed by Stanley Kubrick from the Anthony Burgess novel]({{ base_path }}/images/ludovico-technique.jpg)

Think of it as the Ludovico Technique for obnoxious audiophiles.

## Why I Gave Up the Viola

I used to play the viola as a young man. I had wanted to play the violin, but my school orchestra already had plenty of violinists, cellists, and even bassists. The viola was a booby prize for kids who wanted to play violin but weren't already getting private lessons.

I gave up the instrument when I was eighteen and done with school for a few reasons:

  * Bad memories associated with bullying
  * Ignorance concerning possibilities for violists as solo performers
  * A perceived lack of a repertoire written specifically for the viola
  * Lack of belief in my own ability
  * I had become convinced that while I was a decent technician, I wasn't putting any heart into my playing

I used to say that I gave up music because being a musician had been my mother's dream, and I had needed to find a dream of my own, but that was only partially true. Here's a little more of the truth: I wanted to help do for the viola what Clapton, Hendrix, Van Halen, Santana, and many others did for the guitar, but couldn't find a path forward.

I wasn't aware of [Rudolf Haken](http://www.rudolfhaken.com/) at the time.

---

<iframe width="563" height="337" src="https://www.youtube.com/embed/03q3ytBru-I" frameborder="0" allowfullscreen></iframe>

---

Here's the rest of the truth: I had come to believe that I would never be _good enough_ as a violist. I had yet to learn that _I didn't have to be "good enough" for anybody else_, because I didn't owe the rest of the world anything. And I'd never even heard the phrase [**imposter syndrome**](https://en.wikipedia.org/wiki/Impostor_syndrome) before. After all, what could some long-haired metalhead from New York with a working-class background hope to accomplish as a musician?

## What if I Played Again?

I put aside my viola twenty years ago, and the last thing I remember playing was an attempt at Iron Maiden's "Fear of the Dark" that proved to be nothing but butchery.

{% include spotify url="https://open.spotify.com/embed/track/0PLUzbu0ST6IOSgtUknxWV" %}

But every once in a while I think of picking it up again. My wife used to try to encourage me, but she knows better now. She knows I can't do it for her, or for anybody else.

But what if I could it for myself? Never mind that I'm most likely rusty as hell from twenty years of not playing. Never mind that I probably don't even remember how to read music any longer. Never mind that I would most likely have to start from zero. And never mind that I might not be able to play without breaking down.

It isn't like I have to be perfect. It isn't like the composers are going to drag their carcasses out of their graves or call collect from the depths of Hell to castigate me for butchering their work. They had their chance to do it their way.

It's still my turn to do it _my_ way. Maybe it's just emotional fallout from watching _Your Lie in April_ with my wife and seeing something of myself in one of the leads, but&#8230; _What if I were free?_
