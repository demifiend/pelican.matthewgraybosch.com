---
id: 3342
title: Dark Souls III Character Build Planner
date: 2016-03-20T06:54:22+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3342
permalink: /2016/03/dark-souls-iii-character-build-planner/
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"da0f5bf9125f";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:82:"https://medium.com/@MGraybosch/dark-souls-iii-character-build-planner-da0f5bf9125f";}'
categories:
  - Dark Souls III
  - Games
tags:
  - assassin
  - build
  - build planner
  - Builds
  - character
  - character development
  - character growth
  - cleric
  - dark souls
  - Dark Souls III
  - deprived
  - FromSoftware
  - Google Sheets
  - herald
  - knight
  - mercenary
  - pyromancer
  - sorcerer
  - thief
  - warrior
---
If you're looking to play Dark Souls III in April, and are giving some thought to what sort of character you want to create, I created a tool to help you.

Powered by Google Sheets, my Build Planner lets you choose a starting class and allocate additional points to determine how you'll level up.

It isn't as comprehensive as the [MugenMonkey](https://www.mugenmonkey.com/) tools because it only considers stats, not equipment, but it suited my purposes. You're welcome to use it, too. Just use the link below and make a copy; I had to lock down the original to keep trolls from defacing it.

<a href="https://docs.google.com/spreadsheets/d/1iguTA1-2xc6z0tgv1WwYJtgMTioIc-Xj01q7wRw330Q/edit?usp=sharing" target="_blank">Click here or on the screenshot below to access my build planner on Google Sheets. Link opens in a new tab.</a><figure id="attachment_3347" style="width: 840px" class="wp-caption aligncenter">

<a href="https://docs.google.com/spreadsheets/d/1iguTA1-2xc6z0tgv1WwYJtgMTioIc-Xj01q7wRw330Q/edit?usp=sharing" target="_blank" rel="attachment wp-att-3347"><img src="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/03/darksouls3-planner-screenshot-1024x498.png?fit=840%2C409" alt="My Dark Souls 3 character planner in use." class="size-large wp-image-3347" srcset="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/03/darksouls3-planner-screenshot.png?resize=1024%2C498 1024w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/03/darksouls3-planner-screenshot.png?resize=300%2C146 300w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/03/darksouls3-planner-screenshot.png?resize=768%2C373 768w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/03/darksouls3-planner-screenshot.png?resize=610%2C297 610w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/03/darksouls3-planner-screenshot.png?resize=1200%2C583 1200w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/03/darksouls3-planner-screenshot.png?w=1370 1370w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" data-recalc-dims="1" /></a><figcaption class="wp-caption-text">My Dark Souls 3 character planner in use.</figcaption></figure> 

If you get the PS4 version of _Dark Souls III_, and you end up using this, feel free to say hello on PSN. Look for my summoning sign or challenge me to a duel. My PSN handle is **EddieVanHelsing**.

&#92;[T]/ PRAISE THE SUN! &#92;[T]/

Also, if you found this helpful, please support me by checking out (and possibly buying) my novel _Without Bloodshed_. Thanks!



#### image credit

banner taken from [darksouls3.com](http://darksouls3.com)&#8230;