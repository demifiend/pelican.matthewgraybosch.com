---
title: "Ghost's *Meliora* is the Perfect Easter Album"
excerpt: "Play it loud, mutha. If anybody complains, just say the Devil made you do it."
date: 2016-03-27T03:25:27+00:00
header:
  image: ghost-meliora-cover.jpg
  teaser: ghost-meliora-cover.jpg
categories:
  - Music
tags:
  - Absolution
  - black metal
  - Cirice
  - doom metal
  - esoteric lyrics
  - occult
  - From the Pinnacle to the Pit
  - Ghost
  - He Is
  - Meliora
  - Part 1
  - "Phantom's Divine Comedy"
  - progressive rock
  - psychedelic rock
  - Satanic lyrics
  - Satanism
---
It takes some serious chutzpah to name your album _Meliora_, which is Latin for "better", but Swedish doom-metal act [Ghost](http://ghost-official.com) has the chops to back it up.

## Review

_Meliora_, the third album by [Ghost](http://ghost-official.com), takes its name from the Latin adjective for "better". There's a reason for this; according to an interview with a Nameless Ghoul by Metal Injection, they weren't satisfied with how their previous album _Infestissumam_ worked out.

{% youtube CPRa5SEO3v0 %}

That's fair enough, but I think _Meliora_ is not a mere improvement on their eponymous debut and _Infestissumam_. Instead, what happened in the studio was almost a Hegelian synthesis, or perhaps a chemical wedding. They took the heavy vibe from their first album and combined it with the catchy melodies and lyrics of _Infestissumam_ to create an album showcasing both.

So, what does _Meliora_ sound like? You should listen to it and decide for yourself, but the crooning of Papa Emeritus III supported by the instrumental work by the Nameless Ghouls creates a sound that makes _Meliora_ a suitable companion to Alice Cooper's _Welcome to My Nightmare_, the Blue Öyster Cult's _Spectres_, and classic Judas Priest albums from the 1970s like _Sad Wings of Destiny_ and _Sin After Sin_.

Furthermore, Ghost backs off a little from their Satanic Church concept; the lyrics aren't all Satan all the time (though to be fair, _Infestissumam_ also had departures like the excellent "Ghuleh/Zombie Queen").

For example, "Spirit" is a paean to the green fairy, absinthe. "Mummy Dust" is just sinister rock 'n roll that for some reason reminds me of "I'm the Slime" by Frank Zappa. And one of _Meliora's_ standout tracks, "Absolution", reads like a direct attack on the American culture of overachievement in service to capital we shove down our children's throats:

> Ever since you were born you've been dying  
> Every day a little more you've been dying  
> Dying to reach the setting sun  
>
> As a child, with your mind on the horizon  
> Over corpses, to the prize you kept your eyes on  
> Trying to be the chosen one
>
> <cite>"Absolution" by Ghost, from <em>Meliora</em> (2015)</cite>

Never mind the lyrics; "Absolution" deserves attention just for the double keyboards. I can't think of any other songs that layer piano atop organ, and it really works well here.

If you're here for the Devil's sake, however, Ghost doesn't disappoint. "From the Pinnacle to the Pit" starts off with a fat, dirty, thumping bass reminiscent of Les Claypool and Primus, and is catchy enough to be dangerous to play while driving. "Cirice" is almost a demonic love song, right up there with "N.I.B." by Black Sabbath, and contains a direct stab at Christianity's doctrine of original sin. "He Is" is the sort of hymn to Lucifer one might join in singing after a Black Mass. "Majesty" has an intro straight out of classic Deep Purple, and doesn't let up.

And then there's "Deus in Absentia". The lyrics are kinda Satanic, but I'm pretty sure that with the right partner you could dance a tango to this song.

All told, Ghost's _Meliora_ is an improvement over their previous work in every respect. The production is clean, none of the instruments get lost in the mix, and Papa Emeritus III's clean, melodic vocals work really well with the lyrics if you aren't caught up in expectations of how this sort of heavy metal "should" sound.

## Spotify Stream

Don't just take my word for it; listen to _Meliora_ and decide for yourself.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A74QTwjBLo1eLqpjL320rXX" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Videos

Not only does Ghost do excellent music, and give great interviews (By Arioch, I want one of those devil masks), but they bring a subtle, sardonic sense of humor to their music videos for "From the Pinnacle to the Pit" and "Cirice".

### "From the Pinnacle to the Pit"

{% youtube 6A-IoOEPbUs %}

### "Cirice"

{% youtube -0Ao4t_fe0I %}

### "Absolution (Deezer Session)"

{% youtube SGj_KQYD3FE %}

### Deezer Sessions

You might also be interested in Ghost's complete [Deezer Session](https://www.youtube.com/watch?v=315qJJ3Y9uA), in which they perform "Pinnacle/Pit", "Cirice", and "Absolution".

{% youtube 315qJJ3Y9uA %}

## You Might Also Like&#8230;

If you liked _Meliora_ by Ghost, you might also want to check out _Part 1_ by Phantom's Divine Comedy. They also obscured the identities of their members, with the vocalist named Phantom and other members named X, Y, and Z.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A6F9RG8RQ2YMDWS91Wg6yov" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

Their lyrics have a similar esoticism to Ghost's, but without the Satanism, and they don't get nearly as much attention as the Blue Öyster Cult. Standout tracks include "Merlin" and "Welcome to Hell".

#### Image Credit

Album cover by [Spinefarm Records/Loma Vista Recordings](http://www.spinefarmrecords.com/world/news/2015/07/22/ghost-premiere-from-the-pinnacle-to-the-pit-from-forthcoming-album-meliora/)
