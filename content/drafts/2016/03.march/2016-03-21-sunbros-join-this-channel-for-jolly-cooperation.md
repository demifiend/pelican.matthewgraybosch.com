---
id: 3387
title: 'Sunbros: Join this Channel for Jolly Cooperation'
date: 2016-03-21T05:27:28+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3387
permalink: /2016/03/sunbros-join-this-channel-for-jolly-cooperation/
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
categories:
  - Dark Souls III
tags:
  - co-op
  - Dark Souls III
  - discord
  - jolly co-op
  - jolly cooperation
  - online co-op
  - online coop
  - PC
  - ps4
  - sunbro
  - text chat
  - voice chat
  - warriors of sunlight
  - Xbone
---
If you plan to join the Warriors of Sunlight covenant in _Dark Souls III_ to engage in jolly cooperation with other players, some guys on Reddit fired up a [Warriors of Sunlight channel on Discord](https://discordapp.com/channels/161257052792946688/161257530759053312) to help with matchmaking, teamwork, and general social fun.

Sign up, say hello, and don't forget to specify which version you're going to play. The channels available for all versions of the game, whether Windows, PS4, or Xbone. I'll be on PS4 as **EddieVanHelsing**.

And if you don't know what it means to be a Sunbro, don't worry. We'll look out for you.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

#### Image Credit

Featured image: [Sunbro Vuitton by Sitru@DeviantArt](http://sitru.deviantart.com/art/Sunbro-Vuitton-444355675)