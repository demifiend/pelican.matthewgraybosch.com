---
id: 3022
title: Mulan Has Had Enough of Your Shit
date: 2016-03-10T10:59:27+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3022
permalink: /2016/03/mulan-has-had-enough-of-your-shit/
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Stuff I Found
tags:
  - armored women
  - badass
  - Chinese cinema
  - Frankie Chan
  - Legendary Amazons
  - Mulan
  - promotional art
  - Reddit
---
Actually, it probably has fuck-all to do with the actual [Hua Mulan](https://en.wikipedia.org/wiki/Hua_Mulan), who inspired the Disney movie. But she looks badass. And I couldn't resist the headline.

I saw this on Reddit's [r/ArmoredWomen subreddit](https://www.reddit.com/r/armoredwomen/comments/49ti70/promotional_art_for_legendary_amazons/). Apparently it's promotional art for a 2011 movie called _Legendary Amazons_. [According to Wikipedia&#8230;](https://en.wikipedia.org/wiki/Legendary_Amazons)

> The film is set in early 11th century China during the reign of Emperor Renzong of the Song dynasty. The emperor neglects state affairs and indulges in personal pleasures, while the government sinks into corruption and war continues to rage on at the borders of the Song dynasty. The Song dynasty is being invaded by the armies of the rival state of Western Xia.
> 
> Yang Zongbao is the last man standing in the Yang clan, a family of generals who have dedicated their lives to defending the Song dynasty from foreign invaders. He apparently dies in battle tragically when the treacherous Imperial Grand Tutor (minister) Pang refuses to send reinforcements to aid him. Yang Zongbao's widowed wife, Mu Guiying, leads the other widows of the Yang clan into battle to continue the legacy of their husbands. 

Here's a trailer, if you care.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>