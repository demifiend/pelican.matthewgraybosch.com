---
title: "Star of Ash: *Ghouleh*"
excerpt: "Dark, ambient avant-garde neoclassical from Norway"
header:
  image: starofash-ghouleh.jpg
  teaser: starofash-ghouleh.jpg
categories:
  - Music
tags:
  - 2014
  - avant-garde metal
  - Ghouleh
  - Heidi Solberg Tveitan
  - neoclassical
  - Norway
  - Star of Ash
  - Starofash
---
Looking for some dark ambient post-metal atmosphere? Check out _Ghouleh_ (2014) by [Star of Ash (or Starofash)](http://www.starofash.com/).

---

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A1ONZfvRcTdbrYlI4xgrkvr" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

---

For some reason, Heidi Solberg Tveitan's high, pure tone reminds me of Annie Haslam from Renaissance. I don't know why.

{% include base_path %}
![]({{ base_path }}/images/starofash-ghouleh.jpg)
