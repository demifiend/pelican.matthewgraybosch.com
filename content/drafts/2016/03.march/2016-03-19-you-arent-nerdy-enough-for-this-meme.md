---
title: "You Aren't Nerdy Enough for This Meme"
categories:
  - Bad Jokes
tags:
  - final fantasy
  - humor
  - knock you all down
  - Merrick Garland
  - my wife is going to kill me
  - SCOTUS
  - Supreme Court
---
As soon as I heard that President Obama had nominated Federal appellate judge [Merrick Garland](https://www.washingtonpost.com/world/national-security/president-obama-to-nominate-merrick-garland-to-the-supreme-court-sources-say/2016/03/16/3bc90bc8-eb7c-11e5-a6f3-21ccdbc5f74e_story.html) for the Supreme Court, I went googling to see if anybody else made the obvious (to me) connection. I couldn't find anything, so I whipped this up. You're welcome.

You won't get this unless you played the original _Final Fantasy_, in which the first (and last) boss Garland told the player, "I, Garland, will knock you all down!"

[<img src="http://i0.wp.com/600games.files.wordpress.com/2015/03/wpid-20150315_231440.jpg?w=840&#038;ssl=1" alt="" data-recalc-dims="1" />](http://600-games.com/2015/03/16/3162015-nes-20-final-fantasy/)

[Legends of Localization](http://legendsoflocalization.com/) provides a reasonable explanation for the [notorious line](http://legendsoflocalization.com/the-origin-of-final-fantasys-i-will-knock-you-all-down/). It's basically what happens when you have a Japanese person translate a game without consulting with native English speakers.

All the same, I can't help but wonder if the line was lodged in President Obama's subconscious when he made his post-Scalia pick. After all, you'd figure a moderate white guy with a history of judicial restraint (or [deference](http://www.bna.com/observers-judge-garland-n57982068626/) would knock down Senate Republicans' objections.

Sorry. I'll stop now.
