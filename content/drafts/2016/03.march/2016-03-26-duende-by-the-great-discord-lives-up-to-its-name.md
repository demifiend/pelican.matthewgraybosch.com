---
title: "*Duende* by The Great Discord lives up to its name."
excerpt: "Contralto vocalist Fia Kempe's passionate delivery makes this album a must-have for fans of woman-fronted metal."
header:
  image: the-great-discord-duende.jpg
  teaser: the-great-discord-duende.jpg
categories:
  - Music
tags:
  - Duende
  - Eigengrau
  - Fia Kempe
  - film noir
  - human condition
  - Linkoping
  - progressive metal
  - Sweden
  - The Aging Man
  - The Great Discord
  - woman-fronted
---
The name of The Great Discord's 2015 debut album, _Duende_, suggests that not only does this Swedish metal act from Linkoping have soul, but chutzpah to match. Contralto vocalist Fia Kempe's passionate delivery makes this album a must-have for fans of woman-fronted metal.

## Review

I discovered Swedish progressive metal act The Great Discord's _Duende_ album just after Christmas courtesy of a [Spotify Discover Weekly playlist](http://www.matthewgraybosch.com/2015/12/discovery-playlist-albums-i-missed-in-2015/) that included a radio edit of "The Aging Man" that starts with alto Fia Kempe belting out lyrics that sound like:

> Guiding my end  
> Leave me, I like  
> The quiet, the calm  
> Good wishes leave me cold

> So, let me be  
> The time I have left  
> I'll spend alone  
> Down in the darkness

> <cite>"The Aging Man" by The Great Discord, from <em>Duende</em> (2015)</cite>

The rhythm with which Ms. Kempe sings this refrain is driving and catchy, but "The Aging Man" is far more effective if you listen to the full-length studio version instead of the radio edit, which includes an instrumental introduction beginning with a low-key piano that switches to bass, and then introduces the guitars and drums while becoming more urgent all the while.

The rest of the album proceeds in similar fashion, with Ms. Kempe belting out lyrics pertaining to ["the mundane as well as the extremes of what you as a person potentially live with"](http://www.metalblade.com/thegreatdiscord/). Because she's exploring the darker corners and extremes of the human experience, her alto voice is perfectly suited to the material whereas the higher and sometimes sweeter tones of the sopranos more common in woman-fronted metal bands might not work as well.

Fortunately, Fia Kempe's vocals aren't the only reason to listen to The Great Discord. Ms. Kempe is also a skilled pianist (which reminds me of one of my characters), and the paired guitars of André Axell and Gustav Almberg along with Rasmus Carlson's bass provide a rich, sound throughout the album. This is a band equally suited to fast-paced songs like "L'homme Mauvais" and slower selections like "Woes".

Because of these qualities, _Duende_ can be said to possess its namesake quality. It's a soulful album as well as a technically excellent one. If heavy metal were to combine with _film noir_, I suspect the result would closely resemble this album. However, despite several members claiming Genesis and King Crimson as influences, I think that _Duende_ also bears a slight semblance to Opeth's 2003 album _Damnation_.

And now I'd love to see The Great Discord cover Opeth's "Hope Leaves".

<iframe width="560" height="315" src="https://www.youtube.com/embed/Anuzo1_KSY8" frameborder="0" allowfullscreen></iframe>

---

If _Duende_ can be said to possess a particular flaw, it's that Ms. Kempe's lyrics can be difficult to discern in songs like "Deus Ex Homine" and "Eigengrau" despite repeated listening with good-quality headphones. I don't think the production is the issue; the self-produced album has a clean mix and a wide dynamic range given the state of the Loudness Wars. Nor can Ms. Kempe's articulation be blamed without doing the lady an injustice.

Maybe it's because I'm old enough for "The Aging Man" to resonate, but I'm not so old yet that I can't appreciate an excellent progressive metal album like _Duende_.

## Spotify Embed

Don't just read my opinion; listen and decide for yourself if you have a free Spotify account.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A3R0dvRWuJnbxyRjL23PtNI" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Videos

If you don't want to use Spotify, Metal Blade Records has also provided some official videos from The Great Discord's _Duende_.

### "The Aging Man"

<iframe width="563" height="337" src="https://www.youtube.com/embed/UEKcl8MzWLQ" frameborder="0" allowfullscreen></iframe>

### "The Aging Man (Accoustic)"

<iframe width="563" height="337" src="https://www.youtube.com/embed/9OMouuS904M" frameborder="0" allowfullscreen></iframe>

### "Eigengrau"

<iframe width="563" height="337" src="https://www.youtube.com/embed/C-20TB57XJ4" frameborder="0" allowfullscreen></iframe>

## Trivia

Not only is ["duende"](https://en.wikipedia.org/wiki/Duende_%28art%29) the kind of soul genuinely good art can be said to have, but [fairy-like spirits called "duende"](https://en.wikipedia.org/wiki/Duende_%28mythology%29) also figure prominently in Iberian (Spanish and Portugese), Latin American, and Filipino folklore.
