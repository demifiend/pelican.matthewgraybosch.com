---
id: 3587
title: 'Blackened Phoenix: Dramatis Personae'
date: 2016-03-30T05:22:55+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3587
permalink: /2016/03/blackened-phoenix-dramatis-personae/
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"fd0f4d1a8539";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:79:"https://medium.com/@MGraybosch/blackened-phoenix-dramatis-personae-fd0f4d1a8539";}'
categories:
  - Blackened Phoenix
  - Design Works
  - Meet the Cast
  - Starbreaker
tags:
  - bands
  - cast
  - characters
  - conflict
  - conspiracies
  - dramatis personae
  - factions
  - organizations
  - pre-writing
  - spoilers
---
Unlike [the **Cast** section](http://www.matthewgraybosch.com/2016/03/blackened-phoenix-main-plotline-and-cast/), this part arranges characters by faction/organization and may thus contain redundancies. There be spoilers ahead, me hearties!

<!--more-->

## Dramatis Personae

For the most part, this works for _Without Bloodshed_ as well. However, most minor/supporting characters appearing in the first novel aren't mentioned here.

  * Annelise Copeland &#8211; a rising New York fashion designer with her own boutique on Fifth Avenue. Bears a striking resemblance to the late Christabel Crowley.
  * Sabaoth &#8211; an ensof imprisoned beneath the Antarctic ice cap by Imaginos. Frequently appears before humans, claims to be God, and demands humans obey his commands to commit genocide.
  * Mordred &#8211; A rakshasa who has adopted Morgan Stormrider and his companions. Can understand English, but his spoken vocabulary is limited to "meow".

### Crowley's Thoth

Crowley's Thoth is a neo-Romantic heavy metal band active between 2101 and 2112. Noted for albums like _Prometheus Unbound_, _Glass Earth Falling_, and _Water Margin_; Crowley's Thoth is also famous for its three-hour live shows, which often start without the band's founder and violinist.

  * Christabel Crowley (deceased?) &#8211; Violin/Viola
  * Morgan Stormrider &#8211; Guitar/Vocals
  * Naomi Bradleigh &#8211; Lead Vocals/Keyboards

### The Phoenix Society

The Phoenix Society is nominally a non-government organization dedicated to the defense of free and open societies through active policing of corruption and abuses of power in government, commerce, and religion. The truth is a bit more sinister.

#### Adversaries

Adversaries are agents of the Phoenix Society's enforcement arm, the **Individual Rights Defense** corps. If Solid Snake from _Metal Gear Solid_ worked for the ACLU, he'd be similar to what Adversaries are supposed to be.

  * Morgan Stormrider
  * Naomi Bradleigh
  * Edmund Cohen
  * Sid Schneider
  * Sarah Kohlrynn
  * Catherine Gatto

#### IRD Corps Directors

Though the AIs comprising the Sephiroth do most of the work of assigning work to Adversaries, human officers have the final say.

  * Saul Rosenbaum
  * Iris Deschat
  * Karen Del Rio

#### The Sephiroth

Ten artificial intelligences govern most aspects of the Phoenix Society's operations, but serve other purposes and have their own agenda.

  * Malkuth
  * Kether
  * Gevurah
  * Binah
  * Tiphareth

#### The Executive Council

The identity of the Phoenix Council's executive council is a carefull-guarded secret to keep the Society's enemies from striking at the Society's heart &#8212; and to preserve the secret of the Society's true nature and purpose.

  * Isaac Magnin
  * Charles Desdinova
  * Edmund Cohen
  * Tamara Gellion
  * Elisabeth Bathory
  * Samuel Terell
  * Abram Mellech

### Port Royal

A distributed digital republic composed primarily of techies, hackers, crackers, gamers, and the occasional trolls. Port Royal is a frequent source of anti-Phoenix Society sentiment, and occasional attacks against the Society's technical infrastructure.

  * Claire Ashecroft &#8211; a gray-hat hacker who frequently works with Morgan Stormrider and his crew

### The AsgarTech Corporation

The Asgard Technological Development Corporation (AsgarTech) is one with a long and unsavory history spanning four incarnations. The most recent under Isaac Magnin is responsible for the Asura Emulator Project and primarily funded by the Phoenix Society &mdash; but this is a closely guarded secret.

  * Isaac Magnin, CEO
  * Dr. Josefine Malmgren, ÆsirOS developer/Project Æsir (Asura Emulator Project)
  * Polaris, Project Æsir prototype

### Ohrmazd Medical group

Ostensibly a benevolent organization specializing in providing bleeding-edge medical care to the masses, Ohrmazd Medical is also involved in the Asura Emulator Project.

  * Dr. Charles Desdinova, Founder

### Disciples of the Watch

A cabal of ensof sworn to guard the Starbreaker, the Disciples are likewise sworn to destroy any ensof who threatens sapient species under their protection, like devas/asuras and humans.

  * Thagirion
  * Ashtoreth
  * Sathariel
  * Adramelech (cast out for treachery)
  * Imaginos

### Nakajima Armaments of Kyoto

A manufacturer of arms and armor trusted by Adversaries since its foundation in 2071 by Nakajima Kaoru, it continues to supply Adversaries under the supervision of Kaoru's daughter Chihiro, who took over the company in 2104.

  * Nakajima Kaoru (deceased), &#8211; Founder
  * Nakajima Chihiro, President
  * Ms. Yamagishi, administrative assistant to Nakajima Chihiro
  * Masamune, Design and Production AI for Nakajima Armaments

### Murdoch Defense Industries

A manufacturer of cheap equipment propped up by the Phoenix Society, Murdoch also makes additional profits by selling black-market arms &mdash; with the Society's tacit approval.

  * Victoria Murdoch, CEO
  * Don Kissel, factory foreman

### MEPOL

Though Adversaries have had to clamp down on abuses committed by officers of the Metropolitan Police of London, the modern MEPOL is an efficient, professional police force.

  * Chief Inspector Gregory Windsor

### Blackened Phoenix

This opposition group sprang into prominence in the wake of the Phoenix Society's counterrevolutionary operations in Boston. Initially demanding "justice for Liebenthal" through social media activity and non-violent demonstrations outside Society installations, Blackened Phoenix has recently resorted to attacking the Society's Technological infrastructure.

  * Munakata Tetsuo
  * Alexander Liebenthal
  * Vincent Rubicante
  * Sarah Kohlrynn

### Household AIs

Digital butlers/secretaries serving private individuals are slowly becoming more common as costs decrease, but remain expensive because AIs are considered persons under the law, and are therefore protected by the Universal Declaration of Individual Rights.

  * Astarte (works with Morgan)
  * Hal (works with Claire)
  * Wolfgang (works with Naomi)
  * Aleister (worked with Christabel)
  * Savannah (works with Eddie)
  * Howl (works with Josefine)