---
id: 3009
title: Silent Clarion Coming to Google+ Collections
date: 2016-03-10T10:29:13+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3009
permalink: /2016/03/silent-clarion-coming-to-google-collections/
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
categories:
  - Novels
  - Rough Cuts
  - Silent Clarion
  - Social Media is Bullshit
  - Starbreaker
tags:
  - breakup
  - collection
  - conspiracy
  - experiments
  - google
  - military
  - naomi bradleigh
  - serial
  - vacation from hell
  - vampires
  - war crimes
  - working vacation
---
The rough cuts of my serial novel _Silent Clarion_ is coming to Google+ as a collection. If you want to read them, [start here](https://plus.google.com/collection/YGk4PB).