Catherine Lucille Moore: Queen of the Pulps
###########################################

:date: 2016-02-29 02:30
:category: On The Shoulders of Giants
:tags: catherine lucille moore, c. l. moore, northwest smith, jirel of joiry, weird tales, pulp, fantasy, science fiction, sci-fi, doctor who, the last mimzy, henry kuttner, women authors, 1930s, 1940s, 1950s
:summary: Women in speculative fiction don't get much respect, and one of the great originals is all but forgotten. Catherine Lucille Moore deserves better.
:summary_only: true
:og_image: /images/best-of-cl-moore-del-rey.jpg

Women in SFF don't get much respect, and one of the great originals is all but forgotten.

Today we're going to go old-school and introduce an American woman who was writing about badass warrior women way back in the 1930s for pulp magazines like *Weird Tales* and *Astounding*. Morgaine, Red Sonja, Harry Crewe, Oone the Dreamthief, Monza Murcatto, and hundreds of other heroines of fantasy fiction can probably trace their ancestry back to Jirel of Joiry, and Northwest Smith is Han Solo's grandpa.

But Catherine Lucille Moore, who wrote as C. L. Moore to avoid getting in trouble with her day job, didn't restrict herself to these two major characters, one an iconic figure in the sword and sorcery of fantasy, and the other a venerable hero of space opera. If you scroll down to "The Best of C. L. Moore", you'll find summaries of ten of her classic works, but expect spoilers.

How I Got Into Moore's Work
===========================

Among my other acquisitions at the `2014 World Fantasy Convention <http://worldfantasy2014.org/>`_, I was fortunate to find at one of the convention's used book dealers a 1977 Taplinger Publishing edition of *The Best of C. L. Moore* edited by SF luminary `Lester Del Rey <http://en.wikipedia.org/wiki/Lester_del_Rey>`_. I've heard of Catherine Lucille Moore before; Michael Moorcock praised her work in *Wizardry and Wild Romance*, but I never got around to reading her stories until recently.

How to Get Moore's Work
=======================

The following collections are currently available for Kindle readers:

- `The Best of C. L. Moore <https://www.amazon.com/dp/B016C0HO4Q/>`_
- `Jirel of Joiry <https://www.amazon.com/Jirel-Joiry-C-L-Moore-ebook/dp/B015NAUORA/>`_
- `Northwest Smith <https://www.amazon.com/Northwest-Smith-C-L-Moore-ebook/dp/B015NAUVYG/>`_

Who Was Catherine Lucille Moore?
================================

Born in Indianapolis, Indiana on January 24, 1911, Catherine L. Moore was a sickly girl who spent much of her time reading fantastic literature. As a young woman, she began studying at Indiana University in 1929 and contributed at least three stories to the the university's literary journal, *The Vagabond*. One of them, `"Happily Ever After" <http://webapp1.dlib.indiana.edu/findingaids/view?brand=general&docId=InU-Ar-VAC2229&chunk.id=VAC2229-00582&startDoc=1#mets=http%3A%2F%2Fpurl.dlib.indiana.edu%2Fiudl%2Farchives%2Fmets%2FVAC2229-00596&page=3>`_, picks up where the classic fairy tale left off.

Unfortunately, the Great Depression forced Ms. Moore out of Indiana University during her sophomore year and into a business school where she learned the rudiments of shorthand and typing. Before she had finished this course, she pounced on a job opening at the Fletcher Trust Company in Indianapolis. As Ms. Moore put in her 1975 afterword to *The Best of C. L. Moore*, entitled *Footnote to "Shambleau" -- and Others*, "In those days you didn't mess around. You bluffed, prayed, and grabbed."

She continued to practice her typing and almost by accident started typing the first lines of her first and perhaps most famous story, "Shambleau". Once it was finished, she submitted the story to `Weird Tales` in 1933 since it was the only magazine of its type with which she was familiar, and immediately received a check for $100, or `$1779 in 2014 dollars <http://www.in2013dollars.com/1933-dollars-in-2014?amount=100>`_.

If you'll pardon the baseball metaphor, she stepped up to the plate and knocked the ball right out of the park her first time at bat. It's a damned good thing she did, too, for she claims in *Footnote to "Shambleau" -- and Others* that she was "too unsure of herself to have hammered on the door of every publishing house in New York" if *Weird Tales* had turned her down. Instead, Ms. Moore claims she would have turned to some other activities, which would have been our all but incalculable loss.

In 1936, Ms. Moore met fellow SF writer `Henry Kuttner <http://en.wikipedia.org/wiki/Henry_Kuttner>`_ who wrote her a fan letter in which he mistook her for a man. That must have been awkward for everybody involved, but Kuttner's mistake sparked a romance between the two that led to their marriage in 1940.

This was most likely a happy marriage for them both. At least, I hope it was. They were so compatible that they could finish each other's sentences, and collaborated on dozens of stories under a wide array of pseudonyms during the 1940s and most of the 1950s. Together as Lewis Padgett they published `"Mimsy Were the Borogoves" <http://en.wikipedia.org/wiki/Mimsy_Were_the_Borogoves>`_, which New Line Cinema loosely adapted as `The Last Mimzy <http://en.wikipedia.org/wiki/The_Last_Mimzy>`_ in 2007.

Moore and Kuttner spent several years living just north of New York City before moving to California in the 1950s, where they took advantage of the `G.I. Bill <http://en.wikipedia.org/wiki/G.I._Bill>`_ and finished their college degrees. Kuttner's death at 42 in 1958 ended this partnership, but Moore continued to teach his writing course at the University of Southern California while writing for television until she married Thomas Reggie in 1963.

Thomas Reggie was not a writer, and Moore never published again between 1963 and her death in 1987, except for *Footnote to "Shambleau" -- and Others*, her 1975 afterword to *The Best of C. L. Moore*. It's tempting to say it's too bad Moore didn't continue to publish, and to speculate on why her marriage to Thomas Reggie ended her thirty-year literary career.

I think it's better to be grateful for the three decades in which she gave American science fiction and fantasy a much-needed woman's touch. She infused the science fiction and fantasy genres with an emotional depth, psychological insight, and sensuality many male authors of the 30s, 40s, and 50s could not offer -- and richly deserved every accolade given to her.

*The Best of C. L. Moore*
=========================

Based on what I've found on Google, my copy is most likely a reprint of the 1975 Nelson Doubleday *Best of C. L. Moore* compilation. Taplinger appears to have been a small press that went out of business in the early 80s. It consists of an introduction by Lester del Rey ("Forty Years of C. L. Moore"), an afterword by Ms. Moore herself (Footnote to "Shambleau" and Others), and the following stories.

- Shambleau (a Northwest Smith story)
- Black Thirst (a Northwest Smith story)
- The Bright Illusion
- Black God's Kiss (a Jirel of Joiry story)
- Tryst in Time
- Greater Than Gods
- Fruit of Knowledge
- No Woman Born
- Daemon
- Vintage Season

Originally culled from the pages of such famous pulp magazines as *Weird Tales*, *Astounding Stories*, *Astounding Science Fiction*, *Unknown*, and *Famous Fantastic Mysteries*, these ten novelettes remain excellent reading decades after Moore's death from Alzheimer's disease in 1987.

.. image:: {filename}/images/best-of-cl-moore-del-rey.jpg
	:alt: Cover for Del Rey's &quot;The Best of C. L. Moore&quot;
	:align: left

1933: "Shambleau" (published in *Weird Tales*)
----------------------------------------------

Before Han Solo shaved a few parsecs off the Kessel Run and Malcolm Reynolds fought the good fight against the gorram Feds, Northwest Smith plied interplanetary space and visited Mars, Venus, and stranger planets.

In the first of many adventures, interplanetary outlaw Northwest Smith saves a strange girl from a Martian lynch mob. They disperse when he claims her as his own. Northwest doesn't understand why at first, but he soon finds out when he succumbs to her embrace.

The Shambleau are not only alien, but prey upon men and have done so for so long that an echo of a memory of their depredations lives on in the Perseus myth -- along with a way for Northwest's Venusian friend Yarol to rescue him from the Shambleau's hunger.

1934: "Black Thirst" (published in *Weird Tales*)
-------------------------------------------------

Northwest Smith is back, and still finding trouble in the company of strange women. This time he's on Venus in the city of Ednes, and the woman's name is Vaudir. Vaudir was bred for beauty in the Minga, a castle whose existence predated that of Ednes. Northwest soon learns that Vaudir is not the loveliest of the girls bred in the Minga, but rather a throwback possessed of an intelligence and independence the Minga's ruler thought he had bred out of his prey.

Just as humans raise cattle and other animals for food, so does the inhuman Alendar raise women so that he might feed on their beauty. But the Alendar has grown bored with his tame food supply, and hungers for the wilder and more masculine beauty he sees in Northwest Smith.

1934: "The Bright Illusion" (published in *Astounding Stories*)
---------------------------------------------------------------

A doomed soldier lost in the Middle East, Dixon finds an entrancing golden egg half-buried in the sand -- and the corpses of other men who had discovered it before him. He remembers caution too late, and is engulfed in a radiance that offers him a hope for survival he wouldn't have on his own. The golden radiance is a dispossessed god, and it needs Dixon's help.

Dixon must travel to another world in a universe utterly alien to his own, and find a weakness in the defenses of the god who rules it. Instead, in a world governed by physical laws utterly different from his own world's physics and inhabited by intelligent beings utterly different from humanity with forms he finds intolerably grotesque, he finds a love capable of transcending death.

1934: "Black God's Kiss" (published in *Weird Tales*)
-----------------------------------------------------

Before Red Sonja, Xena the Warrior Princess, Oone the Dreamthief, and even Naomi Bradleigh -- there was Jirel of Joiry, the grandmother of every warrior woman in sword-and-sorcery and epic fantasy. She was a redhead, a good Catholic girl, and she doesn't easily suffer the foolishness of men *or* demons.

Unfortunately, Jirel isn't that smart, as we see in her first adventure "Black God's Kiss". The fiefdom of Joiry has fallen to Guillaume, and he's determined to conquer Joiry's liege lady as well. However, her hatred for the man who defeated her burns with such heat that she willingly descends into Hell armed with nothing but her sword and the blessing of Father Gervase to find a weapon with which she may utterly destroy Guillaume.

The blade proves more useful, and Jirel soon finds her weapon with the help of a demon who takes her form. She claims her weapon by kissing a black idol in a forgotten temple, and carries it within her back through the darkness to Joiry. Knowing all the while that if she does not use it on Guillaume it will turn against her, she gives the man she hated most in all her world a fatal kiss.

1936: "Tryst in Time" (published in *Astounding Stories*)
---------------------------------------------------------

Eric Rosner is thirty, and has seen and done everything an adventurous man can possibly do. He had made and lost fortunes, fought on both sides of hundreds of battles, and pitted himself against human torturers and hungry beasts. He indulged himself in the arms of hundreds of women who adored him, but none of them meant anything to *him*.

Desperate for novelty, he finds it as a result of his unlikely friendship with scientist Walter Dow. Dow invents a device that allows its user to "drag an anchor" through space and time to achieve time travel. But the probability of being able to return to one's original timestream is infinitesimal, and any change in the past by a time traveler shunts that traveler into a version of spacetime in which his actions and their consequences were going to happen anyway.

Dow doesn't dare let people test his device, because the very nature of time travel recludes any hope of obtaining meaningful experimental results. Eric has no such concerns, and doesn't give a damn if he ever comes back to his own world and time. Through all the times and places he visits, he finds only one constant: a woman he cannot forget until they are finally reunited at the end of everything.

1936: "Greater Than Gods" (published in *Astounding Science Fiction*)
---------------------------------------------------------------------

Dr. William Cory is a biologist on the verge of a breakthrough in prenatal sex determination, and also a man who must choose one of two women to be his wife. Sallie is a sweet young woman who seems likely to offer the man who marries her a happy, comfortable home life. Dr. Marta Mayhew is a brilliant chemist and Dr. Cory's intellectual equal, and their marriage could spur them both to new heights.

An encounter with a fellow scientist, Dr. Ashley, results in a conversation about the possibility of looking into the Probability Plane to see the influences of one's choices on future events. Cory dismisses Ashley's speculation as `woo <http://rationalwiki.org/wiki/Woo>`_, but after Ashley leaves something strange happens.

Sallie's photograph on Dr. Cory's desk becomes a portal into the future that will happen if he marries Sallie. Some influence skews the odds so that more girls than boys are born every year, and the skewed sex ratio gets worse every year. Soon, there aren't enough men to rule the world. Women take over, and outlaw war. They refuse to support research that could be turned to warlike ends, even if it can also be used to improve living conditions. Human habitation changes, sprawling outward instead of reaching for the sky, and people stabilize the climate and make the world their peaceful garden. In an age of peace and plenty, ambition becomes irrelevant.

Dr. Cory rejects this future, but Marta's photograph also begins to shimmer. A young man with many of Cory's features greets him, and claims to be his descendant. In this future, Cory continued his research on prenatal sex determination, but he discovers a flaw in the technology when he tests it on dogs: the resulting puppies are especially obedient.

Despite this, the technology is soon applied to humans, and the resulting boys are also far more obedient than usual. The ruling classes, seeing the potential for breeding armies of perfect soldiers, use Cory's flawed methods on the lower classes, but not for themselves. They unite the world under their rule, and are well on their way to uniting the solar system when they reach backward through time to contact Cory.

Rebelling against this future as well, William Cory rejects both and chooses a third option. But he does so with the full understanding that he is consigning potential descendants to oblivion.

1940: "Fruit of Knowledge" (published in *Unknown*)
---------------------------------------------------

In "Fruit of Knowledge", Catherine Moore retells the Genesis myth from `Lilith's <http://en.wikipedia.org/wiki/Lilith>`_ viewpoint while also identifying her as the Queen of Air and Darkness. Seeing in Adam a power capable of rivaling God's, she seduces him, only to be thwarted. Her vengeance disrupts the divine plan and changes everything.

1944: "No Woman Born" (published in *Astounding Science Fiction*)
-----------------------------------------------------------------

The beloved performer Dierdre, horribly burned in a theatre fire, has her brain salvaged and encased in an artificial prosthetic body by a scientist named Maltzer, who helps her adapt to her new circumstances.

As Dierdre becomes more confident and competent in her new form, Maltzer begins to have doubts about the wisdom of saving her and giving her a new body. He asks Dierdre's former manager John Harris to meet Dierdre and see for himself.

Together, they watch Dierdre become something more than human while losing touch with humanity.

1946: "Daemon" (published in *Famous Fantastic Mysteries*)
----------------------------------------------------------

Luiz o Bobo, Luiz the Simple, is a Brazilian man without a soul. Without a soul, he doesn't have a daemon of his own to watch over him until the hour of his death. With his end finally near, he makes his last confession and tells a tale of an island in which the old gods of antiquity found refuge after a star blazed over Bethlehem. Those with souls cannot see the old gods, and do not believe in them, but Luiz could see, and *did* believe.

1946: "Vintage Season" (published in *Astounding Science Fiction*)
------------------------------------------------------------------

It was the most perfect May ever, and Oliver Wilson had to spend it with three strange houseguests who had paid handsomely to spend the month in his house. But when an eccentric woman offers him an incredible sum of he'll sell the house to her before the end of his guests' stay, he wants his guests out as soon as possible. It doesn't help at all that they're weird foreigners, almost alien.

Oliver soon finds out *why* his guests as so weird, and why Mrs. Hollia wants to buy his house so quickly. His guests are temporal tourists, here to savor the most beautiful spring in their history, before continuing to other times and places. But despite being from the future, they let him see for himself what happens *after* their departure.

Legacy
======

I can only scratch the surface when describing the influence Catherine L. Moore's work exerts on science fiction and fantasy. The Internet Movie Database (IMDB) link I provide in the next section handles the adaptations of her work, but I think Moore's influence extends further. However, an exhaustive examination of her legacy isn't a suitable for a section in a blog post, but a subject worthy of a book. I'll stick with the stories compiled in *The Best of C. L. Moore*, which is the only work of hers of which I own a copy.

Both "Shambleau" and "Black Thirst" can be credited with helping bring vampire imagery into science fiction. In addition, the protagonist of both stories has become a staple archetype in science fiction. Whether their creators are aware or not, I suspect that **Northwest Smith** is a spiritual ancestor of Han Solo of *Star Wars*, Spike Spiegel of *Cowboy Bebop*, Gene Starwind of *Outlaw Star*, and Malcolm Reynolds of *Firefly*. I'm willing to bet that some of his spirit even lives on in Captain James Tiberius Kirk from *Star Trek*.

If all the stereotypical strong female characters in fantasy were part of a single clan, I suspect **Jirel of Joiry** would be their matriarch. From what I've read, she was one of the first, if not *the* first, warrior women to pit their swords against sorcery and devilry in the pulps. I suspect that somewhere in some imaginary version of ancient Greece, Xena and Gabrielle are pouring a libation of good strong wine in her honor right now.

I don't know if musician `Arjen Anthony Lucassen <http://www.arjenlucassen.com/content/>`_ ever read "Greater Than Gods", but the conceit of people reaching backward through time with a message to the past is the central premise of his first rock opera with progressive metal act `Ayreon <http://www.arjenlucassen.com/content/arjens-projects/ayreon/>`_. In `The Final Experiment (1995) <http://www.arjenlucassen.com/content/the-final-experiment/>`_, scientists from the year 2084 use a technology called "time telepathy" to send warnings of humanity's imminent destruction due to war, natural disasters caused by environmental collapse, and rampant computer technology into the past. They find a receptive mind in a blind mistrel living in King Arthur's court, but Ayreon's warnings arouse Merlin's ire.

Having read "Fruit of Knowledge", I wonder if Roger Zelazny read it before he wrote *Lord of Light*. I also wonder if it had some influence on Steven Brust when he wrote *To Reign in Hell*, a retelling of *Paradise Lost* in which Yahweh is the first of the angels, and not their creator.

When I read "No Woman Born", I immediately thought of Masamune Shirow's famous manga `Ghost in the Shell <http://en.wikipedia.org/wiki/Ghost_in_the_Shell>`_, though the author claims his primary inspiration came from `The Ghost in the Machine by Arthur Koestler <http://en.wikipedia.org/wiki/The_Ghost_in_the_Machine>`_, and doesn't seem aware of Moore's story.

The daemons in C. L. Moore's story "Daemon" are reminiscent of Philip Pullman's *His Dark Materials* sequence, but further research is required to determine if Moore's story served as an inspiration or antecedent. Finally, "Vintage Season" reads like it could have been a "Doctor Who" story that didn't feature the Doctor, but rather a visit by other Time Lords and Time Ladies travelling incognito.

Additional Reading
==================

For more information about Catherine L. Moore and her work, consider visiting the following:

- `Kirkus Reviews: The Many Names of Catherine Lucille Moore <https://www.kirkusreviews.com/features/many-names-catherine-lucille-moore/>`_
- `Internet Speculative Fiction Database: C. L. Moore <http://www.isfdb.org/cgi-bin/ea.cgi?453>`_
- `The Encyclopedia of Science Fiction: Moore, C. L. <http://www.sf-encyclopedia.com/entry/moore_c_l>`_
- `IMDB: C. L. Moore <http://www.imdb.com/name/nm0600987/>`_
- `Women in Pulp Fiction: C. L. Moore <http://www.vintagelibrary.com/wordpress/women-in-pulp-fiction-c-l-moore/>`_
- `The Finch & Pea: The Sensual Science Fiction of C. L. Moore <http://thefinchandpea.com/2012/05/24/the-sensual-science-fiction-of-c-l-moore/>`_
- `C. L. Moore and Leigh Brackett (pulps – 1950s) <http://scifi-spaceopera.blogspot.com/2016/04/c-l-moore-and-leigh-brackett-pulps-1950s.html>`_

You can also refer to `Wikipedia <http://en.wikipedia.org/wiki/C._L._Moore>`_.
