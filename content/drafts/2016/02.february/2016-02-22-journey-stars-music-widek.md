title: Journey to the Stars with music by Widek
date: 2016-02-22 10:45
summary: Widek's *Journey to the Stars* is a good little album.
category: Metal Appreciation
tags: widek, journey to the stars, indie, instrumental
---
I'm listening to Widek's _Journey to the Stars_. I think I reviewed this album a while ago; I have to dig up the post and put it up, along with lots of others.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A3GvuaGadWlpBPZMvYf1yQc" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
