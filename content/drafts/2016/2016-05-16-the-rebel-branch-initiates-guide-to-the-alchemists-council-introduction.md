---
title: "The Rebel Branch Initiate's Guide to the Alchemists' Council: Introduction"
excerpt: "Welcome to the beginning of my reader's guide to *The Alchemists' Council* by Cynthea Masson, with guest commentary by Eric Higby. In this post: the Prima Materia"
header:
  image: bee-839761_1280.jpg
  teaser: bee-839761_1280.jpg
  caption: "[Bee photo from Pixabay](https://pixabay.com/en/bee-sun-flower-yellow-busy-bee-839761/)"
coauthor: ehigby
toc: true
categories:
  - Books
  - Longform
tags:
  - Cynthea Masson
  - The Alchemists' Council
  - ECW Press
  - Canada
  - 2016
  - literary fantasy
  - intrigue
  - politics
  - alchemy
  - environmentalism
  - intention
  - ego
  - Buddhism
  - urban fantasy
  - portal fantasy
  - commentary
  - read-through
  - read-along
  - reader's guide
  - Eric Higby
  - Prima Materia
  - Orders of the Alchemists' Council
  - Prologue
mentions:
  - url: http://ecwpress.com/blogs/whats-new/117876421-a-reader-s-guide-to-cynthea-masson-s-the-alchemists-council
    title: "ECW Press: A Reader’s Guide to Cynthea Masson’s *The Alchemists’ Council*"
  - url: https://cyntheamasson.com/2016/05/17/reading-the-alchemists-council-part-1/
    title: "Cynthea Masson: Reading *The Alchemists’ Council*: Prima Materia and Prologue"
  - url: http://www.thecaverns.net/Wordpress/recaping-neon-milk-idea-graybosch-higby-readings/
    title: "Caverns, Dungeons, and Beyond: Recap’ing Neon Milk – An Idea From the Graybosch & Higby Readings"
---
{% include base_path %}

Because I've been enjoying *The Alchemists' Council* by Canadian author Cynthea Masson ([website](http://cyntheamasson.com), [twitter](https://twitter.com/cyntheamasson)) since getting an ARC at the 2015 World Fantasy Convention, I've decided to go beyond a mere review and write a series of blog posts providing commentary on the text.

[Eric “Stile Teckel” Higby](http://thecaverns.net) will be joining me, and I'll let him have his say first.

## About Eric Higby

Mr. Higby is a business owner for an online retail store selling Gaming, LARP, and Geek related products. He works full-time as a purchasing agent and materials manager for a large medical company. He's been a gamer and avid reader of Science-Fiction, Fantasy, and Horror for over three decades. He's written over 3,000 articles on his blog thecaverns.net to tens of thousands “gamer and geek” users. You may learn more about some of his projects by viewing his portfolio page at [thecaverns.net](http://thecaverns.net)

### Contacting Eric Higby

* Email: stile at thecaverns.net
* Facebook: Stile Teckel
* Twitter: @StileTeckel
* Google+: Stile Teckel
* Pinterest: The Caverns, Dungeons, and Beyond
* Tumblr: [http://stileteckel.tumblr.com/](http://stileteckel.tumblr.com/)
* [http://www.thecaverns.net](http://www.thecaverns.net)
* [http://www.caveloot.com](http://www.caveloot.com)
* [http://www.echoes/thecaverns.net](http://www.echoes/thecaverns.net)

## About *The Alchemists' Council*

The first of a trilogy, this novel explores the power of words and free will, and also deals with the real-world issue of mass bee deaths due to pesticide abuse.

{% picture cynthea-masson-alchemists-council.jpg alt="The Alchemists Council by Cynthea Masson" %}

> As a new Initiate with the Alchemists' Council, Jaden is trained to maintain the elemental balance of the world while fending off interference by the malevolent Rebel Branch. Bees are disappearing both from the pages of the ancient manuscripts in Council dimension and the outside world, threatening its very existence. Jaden navigates alchemy's complexities, but the more she learns, the more she begins to question Council practices. She realizes the Rebel Branch might not be the enemy she was taught to fight against. As the Council finds itself at the brink of war, Jaden must decide where her allegiance lies.
>
> *from the back of the 2016 paperback edition*

Cynthea Masson currently teaches medieval literature and composition at Vancouver Island University. She is the author of another novel, *The Elijah Tree* (Rebel Satori, 2009), and is more recently one of the editors of *Reading Joss Whedon* (Syracuse University Press, 2014).

## Eric Higby's introduction to *The Alchemists' Council*

{% picture eric-higby.jpeg alt="Eric Higby" %}

I read the Alchemists Council in its fullness as an advanced copy a couple of months ago. As I am re-reading it in final print to begin to discuss it as a project with another author, I am finding the plot and intrigue aspects even more enjoyable than the first time, as I delve even deeper and become more immersed in the rich and full details presented in this book.

The Prima Materia and Prologue introduce you to some of the most prevailing concepts of the book which will be pivotal in understanding the story you are about to enter.

A larger concept of the greater good than that of the individual are revealed. While I think this is something we are all familiar with the Author begins to present this concept in a world where it is a much more meaningful but a way of life.

We learn of a time when things were in “conjunction” or my interpretation, as a sort of Yin-Yang. Things are good and all is warm and fuzzy, until as all so often happens, someone got a bit high and mighty. A being named Aralia claims things to be their own which of course leads to war. The Crystalline wars began. This eventually leads to two factions you will follow in the book.  The Alchemists council and their other half, the Rebels.

One of the most important things to pay attention to in these segments is the discussion of not only the balance between dimensions but also between those in The Alchemists Council.  

They work continually, one hundred and one of them, to keep balance in our world as well as trying to reestablish the balance that once was. Those of who are clueless to our benefactors watching over us dependent upon them to maintain our way of life, while they struggle to fix the issues we cause with the balance.  

Making room for more of their numbers as Junior initiates come into the Order, which is how we will follow the book, a ritual occurs. A binding of two is becoming one. Two consciousness, intelligence merged to create a whole new individual.

Shall we further turn the pages to see how this turns out?

## A Rebel Branch Initiate's Guide to *The Alchemists' Council*: Introduction

{% picture author2.png alt="Matthew Graybosch" %}

Because *The Alchemists' Council* focuses on alchemical practice inspired by real-world traditions from medieval and classical Europe and Asia, readers unfamiliar with medieval alchemy may encounter difficulties while reading this novel.

Readers should be aware that Professor Masson uses intrigue and politics to drive the plot of *The Alchemists' Council*. As such, the novel is best suited to readers who also enjoyed Frank Herbert's *Dune*. The heavy emphasis on organizational politics is a consequence of Jaden's status as a Junior Initiate of the Alchemists' Council. As the newest initiate, Jaden is still learning the ropes. The reader must therefore learn the ropes with her.

Furthermore, the alchemical terminology may prove challenging to some readers. While the novel isn't as reliant on scholarly prowess as certain works by the late Umberto Eco (e.g. *Foucault's Pendulum* and *The Name of the Rose*), having some background knowledge of alchemy that didn't come from either *Fullmetal Alchemist* or Andrezj Sapkowski's *The Witcher* may prove helpful.

Since I possess some of this knowledge, I've decided to compile a rough reader's guide to *The Alchemists' Council*. I'll be working from the 2016 ECW Press paperback, and will cite page numbers when quoting passages.

It is my hope that the following commentary will help illuminate aspects of the text that might otherwise remain occult. However, beyond this point lies spoilers.

**THE ALCHEMISTS' COUNCIL FORBIDS YOU TO READ ANY FURTHER.**

**THE REBEL BRANCH ENCOURAGES YOU TO CONTINUE, BUT THE CHOICE IS YOURS...**

## Prima Materia

In the beginning, there was no Alchemists' Council. Such a thing was unnecessary, for the Lapis and its ruby Flaw coexisted in perfect harmony as the Calculus Macula (CM), sharing quintessence in a world where everything simply existed without intention. This state of affairs continued until somebody named Aralia became conscious of themselves as a being capable of acting intentionally.

This Aralia subsequently got the notion that they were superior to all others, and claimed the CM for their own. Another being, Osmanthus, followed Aralia's lead. Naturally, the two opposed one another for two beings possessed of ego and sure of their own superior can't possibly coexist or share.

Matters rapidly degenerated from that point as other people became conscious of themselves as individuals, chose sides, and went to war. Their combat affected the CM itself, as Aralian victories shifted the balance toward blue and Osmanthian successes pulled the balance toward red.

Eventually Aralia and Osmanthus put aside their arms and reconciled, becoming One again through the first alchemical Conjunction, but it was too late. Nobody else was willing to relinquish ego and intention. Worse, their chemical wedding *fractured* the world, the Prima Materia, into three dimensions. The Aralian faction took one faction. The Osmanthians claimed the other. They retained access to the CM and understanding of the world's true nature. Everybody else in the third dimension became the province of whichever faction controlled the CM, while remaining ignorant of the true nature of the world.

### Were you paying attention? This may be relevant later on.

On the immediate story level, this myth can be used to explain the origins of the Alchemists' Council (Aralians?), the Rebel Branch (Osmanthians?), and everybody else (muggles?). The third dimension is the real world, and those who live there are mainly ignorant of the alchemical machinations behind the scenes. A scholar might get a quick peek behind the curtain, only to dismiss their insight as one brought on by fatigue.

Digging deeper, a reader familiar with both the Judeo-Christian myth of the Garden of Eden and the tenets of Buddhism may notice that Masson used a synthesis of the two in the Prima Materia origin story. She makes no mention of *sin* or of disobedience, but it is plain that Aralia changed when they gained *intention*. Assuming this is a story that initiates of the Alchemists' Council learn as an explanation for the necessity of the Council's Great Work, it implies that the Council frowns upon individual intention and free will.

Furthermore, the emphasis on conjunction, where the essence of one person merges with that of another to create a single being where two once existed, will recur throughout *The Alchemists' Council*, and the results of conjunctions between Council members can determine who lives, who dies, and who gets initiated into the Council later on.

### Familiar names?

For some reason, the name Aralia reminds me of Aradia, a figure currently important in Wicca and some neo-Pagan traditions. However, Aradia originally appeared in the work of American folklorist Charles Godfrey Leland, who published *Aradia, or The Gospel of the Witches* in 1899.

According to Leland, this book was a religious text belonging to Tuscan covens who venerated Diana as the Queen of the Witches. No doubt Hecate had somewhat to say about Diana muscling in on her turf, but gods supplant each other all the time. Just ask Zeus about his dad Chronos.

However, googling the names Aralia and Osmanthus reveals that these are the names of two plant genera. Aralia is a genus consisting of 68 species of trees, shrubs, and perennial herbs native to Asia and North America. Osmanthus is a genus of 30 flowering plant species ranging in size from shrubs to small trees, all evergreen. I don't know if Prof. Masson was aware of this connection, but I find it interesting.

### So, you want to be an alchemist?

The Alchemists' Council must occasionally replenish its ranks by reaching out to an uninitiated individual whose presence their texts foretold, and initiating them. Once initiated, an alchemist leaves the mundane realm and takes up residence in Council dimension, where their lives are extended through access to quintessence, the fifth element, obtained by proximity to the Lapis. All members of the Council are endowed by the Lapis with the ability to speak with one another and be understood, and live in beauty and splendor.

However, they don't spend their time turning lead into gold, or creating the Philosopher's Stone. Rather, the initiates of the Alchemists' Council, who walk the line between science and magic, chemistry and mysticism, work to maintain the elemental balance of the world and ensure it remains hospitable to life.

A hundred and one initiated members constitute the Alchemists' Council, though the number of actual members can vary due to conjunctions and erasures. More on both later, but when either happens, the Council must recruit new initiates from the mundane world into Council dimension.

Like any esoteric order, the Alchemists' Council possesses varying degrees of initiation. In fact, the Council's orders roughly correspond to the hierarchy of the Hermetic Order of the Golden Dawn, which in turn borrowed the structure from the Societas Rosicruciana in Anglia, who derived it from the Order of the Golden and Rosy Cross.

{% picture rose-cross.png alt="Rose Cross of the Golden Dawn" %}

One progresses through the ranks through study and accomplishment of the prescribed work for one's rank, though an individual's progress can be interrupted if an initiate conjoins with another, and their essence is consumed. Likewise, should one transgress against the council, they may be subject to erasure and permanent removal from Council dimension.

## Prologue

After the introduction to the setting and explanation of the ranks of the Alchemists' Council provided in Prima Materia, the Prologue touches on events occurring five years prior to be beginning of the novel:

 * The conjunction of Saule
 * The search for initiation of Jaden
 * Cedar's betrayal of Saule
 * The debate over whether to release the Council's bees into the outside world.

The fact that Cynthea Masson chose to depict these events, one from Saule's viewpoint and the other three from Cedar's (whose essence consumed Saule's during their conjunction) suggests that Cedar is an extremely important character, and possibly an antagonist of Jaden's. The implication that Cedar somehow betrayed Saule during their conjunction and that somebody named Sadira might have suffered as a result further cements my impression.

The bee question is also important, and not simple. Cedar believes that if the Council's bees are not released into mundane space to repair the world, the outside environment will suffer catastrophic failures within five years. However, Ruis insists that turning the bees loose would cause irreparable damage to Council dimension.

However, the question of whether to loose the bees isn't for either of them to decide. Nor will it be decided here.

### What is Conjunction?

Conjunction is an alchemical process in which the essence of two individuals become one. The process is reminiscent to the Sacred Marriage described in an allegorical romance entitled *The Chymical Wedding of Christian Rosenkruetz*, the third manifesto of the original Rosicrucians, a German philosophical secret society active in the early seventeenth century.

When two initiates of the Alchemists' Council conjoin, their essences combine and only one person remains. The result is variable; one personality may dominate, or the person coming out of conjunction may be a synthesis of the original two.

{% picture aurora-consurgens-hermaphrodite.jpg alt="An illustration of a hermaphrodite from Aurora Consurgens" %}

The act of conjunction is also a process of eliminating possibilities. Consider a hypothetical conjunction between Alice and Barbara. Should Alice's essence consume Barbara's, then Claire would be initiated while Diane would die of natural causes, her potential unrealized. However, if Barbara's essence dominates, it will be Diane who joins the council, while Claire dies. While Alice and Barbara remain unconjoined, all possibilities remain in play but unrealized.

Regardless of which personality dominates, or whether two equally strong personalities form a synthesis, conjunction is Thunderdome: **Two alchemists enter; one alchemist leaves.**

{% youtube pmRAiUPdRjk %}

This is important because Junior Initiates in the Alchemists' Council aren't ordinarily told the truth about conjunction. Learning the truth, as Jaden does later on, will shape her character and drive her actions in the novel.

### What about Final Conjunction?

As I understand it, the Final Conjunction is different from other conjunctions. Rather than the essence of two alchemists combining into one (with the attendant clash of personalities), the Final Conjunction is one between the Azoth Magen (the eldest and highest-ranking member of the Alchemists' Council) and the Lapis itself. In the Final Conjunction, the Azoth Magen becomes part of the Lapis, surrendering their individuality and returning Quintessence to the Lapis.

Just as in regular conjunctions, the Final Conjunction results in an open position within the Alchemists' Council. A new Azoth Magen must be chosen, most likely from among the Azoths, and so on down the ranks until new Junior Initiates are recruited. The rise of a new Azoth Magen also presages the beginning of a new Council, as I understand it. At the moment, Ailanthus presides. Whether that remains the case is an open question.

If you want more of the Rebel Branch Initiates' Guide, [check out the main page](/commentaries/the-rebel-branch-initiates-guide-to-the-alchemists-council/).
