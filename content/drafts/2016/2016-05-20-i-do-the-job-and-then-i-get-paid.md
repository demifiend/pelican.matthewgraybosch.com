---
title: "I Do the Job, and Then I Get Paid"
excerpt: "I'm a good enough programmer that I've made a living at it since 2000. I'm not passionate about programming, and as a professional I don't need to be."
header:
  image: firefly-do-the-job-get-paid.jpg
  teaser: firefly-do-the-job-get-paid.jpg
categories:
  - Programming Considered Harmful
tags:
  - competence
  - ego
  - emotional labor
  - passion
  - professionalism
  - programming
  - reliability
  - skill
  - talent
---
[Tom Ritchford at Medium](https://medium.com/@TomSwirly/this-whole-article-is-baffling-98f9390b3359#.yxcbm7gy1) has a problem with the notion that [programming doesn't require talent or passion](https://medium.com/@WordcorpGlobal/programming-doesnt-require-talent-or-even-passion-11422270e1e4#.a7knuqtah). He asks... 

> Why would you even want to work in a field where you had no talent and no passion? Why would you want to work on something where you are always going to be second rate, and which you will never really enjoy?

> And why would anyone hire you? Would you want to eat a meal by someone with no talent for cooking? Or go to a doctor with no talent for medicine?

I don't know if Mr. Ritchford is a colleague or not. Either way, he seems to have drunk the new corporate Flavor-Aid that demands that workers not merely be skilled, but *passionate* about their work. He doesn't understand what sort of [**emotional labor**](https://en.wikipedia.org/wiki/Emotional_labor) he expects from programmers.

So I'll explain. I'll try to use small words. Here's the deal: I have the talent. I don’t have the passion. I don’t need it, but some people don’t understand why somebody who doesn’t love to code does it for a living.

I do it because I’m an author who can’t afford to quit his day job yet, and programming pays a hell of a lot more than waiting tables or cleaning toilets — a fact I learned the hard way. Rather than whine about how life isn’t fair because I can’t make a living doing what I love, I swallowed my pride and got a job that paid.

I don’t need to be passionate about writing yet another custom web application for a client that does nothing but provide a pretty skin over arcane business logic and CRUD operations. And while you might not want to admit it to yourself, most of the programming work done in the US is the sort of boring, business-oriented stuff that used to be written in COBOL and run on IBM mainframes.

With the sort of coding I do (so you don’t have to), passion is a liability. Passionate programmers let their ego get in the way. When somebody reports a bug in their code, they scramble to find alternative explanations.

I outgrew that nonsense my first years on the job, because it’s never worth it. Instead, I take a breath, diagnose the problem, and fix it— even if it means working past midnight on my birthday or wedding anniversary. Why? Because I am an adult and a professional.

I might not enjoy the work, but I love not being poor. Because having money is so goddamn awesome, I’m determined to give my employer and their clients their money’s worth so that they keep paying me. That means being reliable, being competent, and keeping my skills current.

Yes, I’m a mercenary. I do the job, and then I get paid. Deal with it. How I feel about the work is none of your business, because we’re not friends. If you want me to pretend I love coding, [you’re gonna have to pay extra](http://www.btlaborrelations.com/wp-content/uploads/2016/05/t-mobile.pdf).
