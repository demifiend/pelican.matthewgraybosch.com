---
title: "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 4"
excerpt: "Welcome back to my readers' guide to *The Alchemists' Council* by Cynthea Masson, with guest commentary by Eric Higby. This post covers pages 160-164."
header:
  image: bee-649952_1280.jpg
  teaser: bee-649952_1280.jpg
  caption: "[Source: Pixabay](https://pixabay.com/en/bee-flying-flower-insect-fly-649952/)"
coauthor: ehigby
toc: true
categories:
  - Books
  - Longform
tags:
  - Cynthea Masson
  - The Alchemists' Council
  - ECW Press
  - Canada
  - 2016
  - literary fantasy
  - intrigue
  - politics
  - alchemy
  - environmentalism
  - intention
  - ego
  - Buddhism
  - urban fantasy
  - portal fantasy
  - commentary
  - read-through
  - read-along
  - reader's guide
  - Eric Higby
  - Chapter 3
  - Part 4
  - Kabbalah
  - Qliphoth
  - the Left-Hand Path
---
While the Elder Council is locked away in a meeting, the Rebel Branch will play. And today their game is a long one, planting seeds of doubt in Jaden's mind to sprout as she rises through the ranks of the Alchemists' Council.

**THE ALCHEMISTS' COUNCIL FORBIDS YOU TO READ ANY FURTHER.**

**THE REBEL BRANCH ENCOURAGES YOU TO CONTINUE, BUT THE CHOICE IS YOURS...**

### Dracaen's Pitch

Getting back to Jaden, Draecen, and Kalina: Jaden is surprised that the Rebel Branch wants to recruit *her*, but the Rebel Branch has their own Elders, and their own Scribes and Readers. The Dragonian interpretation of the Lapidarian manuscripts -- with the aid of some masterful palimpsest revision and a well-placed lacuna or two -- suggests that Jaden belongs with the Rebel Branch, rather than the Alchemists' Council.

No doubt Jaden feels a bit like Neo did in *The Matrix* when Morpheus explained that not only was he right to suspect that something was terribly wrong with the world, but that the truth was worse than he dared imagine.

{% picture morpheus-alchemists-council.jpg alt="What if I told you that the Alchemists' Council was SEELE, and they were still working on the Human Instrumentality Project?" %}

However, like Morpheus and his crew, the Rebel Branch are adepts at manipulating Lapidarian manuscripts and the alchemical aspects of the world to their own ends, such as the preservation of volition ensuring that the Elder Council didn't find cause to block Jaden's initiation.

However, as Kalina explains on page 161, "the people of the outside world are currently at risk because of archaic Council protocols and abuse of Azothian power."

Unfortunately, Jaden doesn't know who to believe. She has no reason to trust the Alchemists' Council when they conscripted her, but she doesn't know enough yet to decide whether the Rebel Branch is any better. All she knows for sure is that if destiny can be written or rewritten, then her *own* destiny might be similarly malleable.

Draecen, to his credit, states that the Rebel Branch won't force Jaden to choose immediately. Instead, he promises that as long as the Rebel Branch exists, Jaden will always have a choice. He is similarly forthcoming when Jaden asks what exactly the Council is doing to endanger the outside world:

> "Human beings, with their pervasive pollution and obsession with technological advancements, are destroying the balance of the Earth beyond basic alchemical repair. To repair the world, the Council may opt to eradicate the free will of the people of the outside world." (p. 162)

How will the Council do this? The Lapidarian bees are the key. Released en masse, their wings vibrate at a frequency capable of interfering with people's ability to think for themselves. (If you're thinking that people don't seem to think for themselves *now*, just imagine how much worse it can get.) For the *Neon Genesis Evangelion* fans reading this, the Alchemists' Council's endgame is similar to SEELE's: a [Human Instrumentality Project](http://wiki.evageeks.org/Human_Instrumentality_Project) created not through Third Impact, but by erasing the Flaw in the Stone.

Draecen isn't giving Jaden the hard sell now because the Rebel Branch plays the long game. They hope to gain her sympathy, even if she doesn't join them outright, so that they've got somebody on the inside as Jaden rises through the ranks of the Alchemists' Council. Their primary aim isn't to swell their ranks, but to maintain the Flaw in the Stone and prevent the Council from gaining absolute control over both Council dimension and the outside world.

As I pointed out earlier, they're advocates for chaos in a world dominated by order.

However, Jaden and Kalina can't stay any longer. The Elder Council meeting they used as cover will have ended, and that means the members of the Elder Council are free to notice that a Senior Initiate and a Junior Initiate have disappeared from Council dimension.

However, Draecen has a parting warning for Jaden: she must find her Dragonblood fragment and keep it on her at all times, otherwise Lapidarian proximity will blank out her memories again. Draecen can't give Jaden a new fragment, because she won't be able to carry it directly from negative space (Rebel Branch territory) into the positive space of the Alchemists' Council. Nor can she detour safely through the outside world without drawing notice.

The situation would be different if Jaden had her pendant, which can be inlaid with a fragment of the Dragonblood stone that would safely cross directly into Council dimension, but Jaden hasn't been part of the Council long enough to get one.

#### A Digression on Dragon Sybolism

Before I continue to the next scene, however, it would be a good time to remark on Cynthea Masson's use of draconic imagery for the Rebel Branch. The dragon is an interesting symbol for the Rebel Branch because of the different meanings attributed to it by different cultures.

In Judeo-Christian tradition, the dragon is associated with Satan (or more properly, *ha-Satan*, the Adversary) who in the Old Testament was God's enforcer but in the New is God's enemy.

{% picture william-blake-red-dragon.jpg alt="The Red Dragon and the Woman Clothed in Sun, by William Blake" %}

---

{% picture william-blake-great-red-dragon.jpg alt="The Great Red Dragon and the Beast from the Sea, by William Blake" %}

In China and Japan, however, dragons are emblems of the forces of nature: powerful, wise, and fundamentally benevolent.

{% picture chen-rong-nine-dragons-detail.jpg alt="Detail from the Nine Dragons scroll by Chen Rong" %}

---

{% picture qing-dynasty-flag-1889-1912.png alt="Flag of the Qing Dynasty (1889-1912)" %}

Furthermore, there exists in modern occult practice a "draconian tradition" associated with the Left-Hand Path and the Qliphoth that uses "dark"/"evil" powers like Leviathan, Lucifer, and Lilith as archetypes. One such order is the [Dragon Rouge](http://www.dragonrouge.net/english/general.html), founded by Swedish occultist [Thomas Karlsson](https://en.wikipedia.org/wiki/Thomas_Karlsson) in 1989 (who also served as lyricist for the band [Therion](http://therion.se/) from 1996, and sings for a band called Shadowseeds).

Was Cynthea Masson aware of these associations? Perhaps not, but the Ouroboros, the dragon swallowing its own tail, is a major symbol in alchemy (and probably a distant cousin of Jormungandr, the Midgard serpent).

[{% picture ouroboros-cemetery-door.jpg alt="Ouroboros on a cemetary door by Swiertz" %}](https://commons.wikimedia.org/wiki/File:Ouroboros_on_a_cemetery_door.jpg)

---

{% picture thor-und-die-midgardsschlange.jpg alt="Thor didn't bring enough gun." %}

Stay tuned for "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 5". If you want more of the Rebel Branch Initiates' Guide, [check out the main page](/commentaries/the-rebel-branch-initiates-guide-to-the-alchemists-council/).
