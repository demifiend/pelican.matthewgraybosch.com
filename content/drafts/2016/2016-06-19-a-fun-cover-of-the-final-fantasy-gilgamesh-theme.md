---
title: "A Fun Cover of the *Final Fantasy* Gilgamesh Theme"
excerpt: "It can be hard to find good covers of &quot;Clash on the Big Bridge&quot;."
categories:
    - Games
    - Music
tags:
    - Final Fantasy
    - YouTube
    - remix
    - cover
    - violin
    - keyboard
    - guitar
    - rock
    - Gilgamesh
    - Clash on the Big Bridge
    - Nobuo Uematsu
---
Here's just a little something fun I found on YouTube. When you grow up playing *Final Fantasy*, you develop a certain affection for its soundtracks, especially Nobuo Uematsu's.

{% youtube yTtUtwvv50s %}
