---
id: 3742
title: 'Dark Souls III: Minimalist Firekeeper Art'
date: 2016-04-13T05:33:05+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3742
permalink: /2016/04/dark-souls-iii-minimalist-firekeeper-art/
yuzo_related_post_metabox:
  - 'a:3:{s:17:"yuzo_include_post";s:0:"";s:17:"yuzo_exclude_post";s:0:"";s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"d92ab6a8594f";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:84:"https://medium.com/@MGraybosch/dark-souls-iii-minimalist-firekeeper-art-d92ab6a8594f";}'
categories:
  - Dark Souls III
  - Games
  - Stuff I Found
tags:
  - art
  - art-nouveau
  - blonde
  - Dark Souls III
  - Fire Keeper
  - Firekeeper
  - girl
  - minimalist
  - pretty
  - Stef Tastan
  - waifu
---
[Just something I saw on Reddit](https://www.reddit.com/r/darksouls3/comments/4ekc7u/no_spoilers_a_digital_painting_i_made_of_the/): [The Firekeeper in _Dark Souls III_, by Stef Tastan](http://imgur.com/lDFtAM2)