---
title: "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 1"
excerpt: "Welcome back to my readers' guide to *The Alchemists' Council* by Cynthea Masson, with guest commentary by Eric Higby. This post covers Chapter 1."
header:
  image: bee-1040521_1280.jpg
  teaser: bee-1040521_1280.jpg
  caption: "[Source: Pixabay](https://pixabay.com/en/bee-lavender-insect-nature-yellow-1040521/)"
coauthor: ehigby
toc: true
categories:
  - Books
  - Longform
tags:
  - Cynthea Masson
  - The Alchemists' Council
  - ECW Press
  - Canada
  - 2016
  - literary fantasy
  - intrigue
  - politics
  - alchemy
  - environmentalism
  - intention
  - ego
  - Buddhism
  - urban fantasy
  - portal fantasy
  - commentary
  - read-through
  - read-along
  - reader's guide
  - Eric Higby
  - Chapter 1
mentions:
  - url: http://ecwpress.com/blogs/whats-new/117876421-a-reader-s-guide-to-cynthea-masson-s-the-alchemists-council
    title: "ECW Press: A Reader’s Guide to Cynthea Masson’s *The Alchemists’ Council*"
  - url: https://cyntheamasson.com/2016/05/27/reading-the-alchemists-council-chapter-1/
    title: "Cynthea Masson: Reading *The Alchemists’ Council*: Chapter 1"
---
Last week, [Eric Higby](http://thecaverns.net) and I tackled the opening sections of [Cynthea Masson's](http://www.cyntheamasson.com) new literary fantasy, [*The Alchemists' Council*](https://www.goodreads.com/book/show/26113691-the-alchemists-council) published by [ECW Press](http://ecwpress.com/collections/books/products/alchemists-council). In ["The Rebel Branch Initiate's Guide to the Alchemists' Council: Introduction"]({% post_url 2016-05-16-the-rebel-branch-initiates-guide-to-the-alchemists-council-introduction %}), we covered the origin myth for Masson's setting, the Prima Materia. We also touched on the Orders of the Alchemists' Council before moving on to the Prologue, which introduced concepts, characters, and issues that will drive the rest of the novel's plot.

As before, I'll present Eric Higby's commentary first, since his take on Chapter 1 is more concise than my own. Mine will follow for readers who wish to go into greater detail. Expect spoilers ahead.

**THE ALCHEMISTS' COUNCIL FORBIDS YOU TO READ ANY FURTHER.**

**THE REBEL BRANCH ENCOURAGES YOU TO CONTINUE, BUT THE CHOICE IS YOURS...**

## Eric Higby on Chapter 1 of *The Alchemists' Council*

It is time for us to take a peek at chapter one of *The Alchemists' Council* written by Cynthea Masson. Please be warned that the following iteration of random thoughts that I can only hope someone may care to read does contain spoilers!

Chapter One begins to let us explore the inner workings of the Alchemists Council not only with some its existing members that we were previously introduced to but also that of a new initiate. You may remember that the council maintains a 101 membership roster and with the conjunction that took place, a new imitate must be found.

Fear not if you took a liking quickly to Jaden and Cedar though as they do maintain a leading character role throughout the story. Our new member of the council, Arjan, does quickly take a forward spot in the story though as well as teaming up with Jaden.

Let's take a step back though and go to the start of the chapter.

{% picture bee-getting-erased.jpg alt="A bee getting erased" %}

We start off by being introduced not only to *Lapidarian Bees* as well as a mystery. They are disappearing!  No, not real ones! Rather it looks like they are starting to vanish from books, scrolls, and other formats of record keeping. The images themselves disappearing from the pages. As you continue the chapter, you learn more both about the importance of these bees as well as the libraries.

What I liked about this in *The Alchemists' Council* is the disappearance of these bees are a severe issue and plot line of the story. I know several beekeepers in real life and last year in our area we lost 50% of our bee population in our state.

Luckily this year is doing better but overall the bee population of the world is dropping, and it is a far-reaching concern. Fiction while being enjoyable, can also teach us lessons to take to heart in our real lives. I love that this plot aspect reflects a real world concern as you never know when something like that may inspire and cause change for good.

This also begins to introduce us to the stories characters, the politics taking place in the Council, as Cedar brings the concern to everyone's attention.  

We learn of the Councils ability to live long lifespans, cure illness and diseases, and most injuries. They have a Cult-like focus on their work and beliefs. An impression Jaden has when she flashes back to her recent initiation into the Council.

We learn more of conjunction, the power of pendants, and how powerful the written word is for the Council.

{% picture quill-and-ink.png alt="Quill and Lapidarian Ink" %}

In a world in which turning lead into gold is an elementary lesson of which the ability to do is thought little of, working with inks and quill give new meaning to the pen being mightier than the sword!

If you have ever watched the Canadian TV show [*Lost Girl*](http://www.syfy.com/lostgirl) it has a strong story aspect of a Blood King who can alter the rules and laws that everyone lives by, by writing with his own blood.

Through an accident during lessons of the initiates and the harsh lectures that follow, you learn that these one hundred and one individuals carry similar awe-inspiring powers, and DO use those skills. It is also when I truly begin as a reader to become suspicious of the Alchemist Council and whether or not they are delusional in some of their beliefs.

There is much more to learn in the first chapter of this book but I will not take your hand and walk you through all of it, so this is where my words, with none of the magic of the Council, will end for now.

I just want to leave you with one final thought. Chapter 1 in the published paperback is 55 pages long starting on page 11, ending on page 66. 5 +5 = 10. On the periodic table of Elements Neon is identified as the number 10. We most often use Neon in red signs which we often view with an almost magical awe, much like the inks and letters the Council use. Coincidence? We may never know.

With that, I will see you shortly in the Initiates Common Room as we start chapter 2!

## A Rebel Branch Initiate's Guide to *The Alchemists' Council*: Chapter 1

The following matters are of particular interest in the first chapter of *The Alchemists' Council*. Covering them will take us from page 11 to 66 of the paperback.

* The Missing Lapidarian Bees, Part 1
* The Next Conjunction
* Jaden's Resentment
* The Recruitment of Arjan
* Sephrim: the Alchemist's Little Helper
* The Care and Feeding of Alchemists in Council Dimension
* Playing Telephone with the Lapis
* Erasure and its Consequences
* Giving Schrödinger's Cat a Belly Rub

### The Missing Lapidarian Bees, Part 1

Novillian Scribe Cedar reports to Azoth Ruis that Junior Magistrate Linden observed the *disappearance* of bees from the alchemical manuscript *Ruach 2103*, [folio](https://en.wikipedia.org/wiki/Folio) 51 [verso](https://en.wikipedia.org/wiki/Recto_and_verso), which is located outside of Council dimension in a library in the Council's Vienna protectorate. It is important to note that Cedar is not talking about bees from the Council's apiaries, or bees in the outside world. Rather, the bees in question are part of illuminations within the manuscripts.

Furthermore, the illuminations are not mere artistic embellishments, as they were in medieval manuscripts, Bibles, and Books of Hours. The illuminations of alchemical manuscripts are part of the text. Altering them alters the manuscript, and the meaning thereof. Furthermore, Cedar seems to think the bees are leaving on their own, which implies that the manuscripts are *changing themselves*.

Considering that the Alchemists' Council bases its actions on the Readers' interpretation of the manuscripts, this change doesn't bode well. Linden suspects the involvement of the Rebel Branch, a concern Ruis dismisses, thinking it beneath them. He instructs Cedar to seek further evidence, which she finds by comparing her notes on *Sursum Deorsum 5055* folio 63 verso, which she compiled as a Senior Initiate, with the original manuscript. Compared to her notes, the original is likewise missing bees.

As a result of Cedar's reports, the wider Council has taken up the matter of the bees again, which we learn from Amur as he blames Cedar for getting bees on the agenda. In addition, the Council returns to the question of whether to release the Lapidarian bees into the outside world to compensate for [colony collapse disorder](http://www.ars.usda.gov/News/docs.htm?docid=15572) that the author first introduced in the Prologue.

I suspect Cedar will prove an antagonist, but further evidence is required. She strikes me as the manipulative sort, since she notes Amur's attraction to her, but regards him as a sacrifical lamb and means to use him. The question is, against whom will Cedar use Amur? Azoth Ruis seems a likely target, given this line on page 18:

> As for Ruis, she had loved him once, had welcomed his power, but had more recently observed his abuses.

More on Cedar and the bees later, in subsequent chapters.

### The Next Conjunction, Part 1

The next alchemical conjunction will be between Novillian Scribe Amur and Senior Magistrate Sadira. More on this after we discuss Jaden.

### Jaden's Resentment

We learn about the participants in the upcoming alchemical conjunction through Jaden, the protagonist who had thus far remained off-stage. She has a small personal stake in the conjunction's outcome; she would prefer that Sadira be the one to survive, and not Amur, because Sadira is one of the few Senior Magistrates whose lessons Jaden finds tolerable.

Nor is Jaden particularly interested in the bee issue; she recalls that the last time the subject came up the Council debated for over two hours with no result. Furthermore, she notes that the bees could all just disappear while the Council deliberates.

It bears mentioning that Jaden isn't her real name. Before Cedar approached her she was a university student in Vancouver. She had lived and studied there for two years, had no immediate family, and wasn't especially close to her extended family. Aside from the friends she made at university, she was alone in the world.

As Jaden herself noted when Cedar first sat down at her table in the Vancouver Art Gallery's café, she was an ideal target for recruitment into a cult. Furthermore, now that the thrill of knowing her existence was foretold by the Nahzin Prophecies has faded, the Alchemists' Council closely resembles a cult.

While Council dimension is beautiful and Jaden lives in comfort with privileges beyond her reach in the outside world, she is also subjected to alchemically induced vapors meant to ease her transition away from her old life. Nor is she permitted to leave Council dimension or communicate with her old friends. Instead, as described in the *Orders of the Alchemists Council*, she is expected to *purge* herself of outside influences so she may better engage with the Great Work.

In all honesty, this would remind me of a cult even if Jaden had not been savvy enough to note the similarities herself. As it is, it calls to mind ["A Touch of Blessing"](https://www.youtube.com/watch?v=TQ8PXXrCasE) by Swedish melodic metal act Evergrey's 2004 album, *The Inner Circle*.

> Misled by beauty, one you rarely find...

{% youtube TQ8PXXrCasE %}

> All the dreams I had, all my future wishes  
> Put aside for a greater journey  
> All the things I planned, left my friends so coldly  
> Put aside for a higher...

In addition to the restrictions on movement imposed on Jaden and other Junior Initiates, her status is currently that of a mushroom. The Elder members of the council keep her in the dark and feed her bullshit, rather than giving her honest answers to her questions -- especially about the Council's recruitment methods.

Instead, Jaden is encouraged to view the alchemical manuscripts as the Word Eternal. "From the Lapis to the Scribe; from the Scribe to the Reader," is the formula quoted to her on page 23. However, an observant reader may notice that this very aphorism shows that the Lapis' relevations are not directly shared with all members of the Alchemists' Council, but are potentially filtered by at least two human minds.

Because the Council is so tightly bound by their interpretation of alchemical manuscripts as revelations from the Lapis, Jaden is determined to become a Scribe herself so that she can interpret Lapidarian visions in a manner that suits *her*, so that she might regain control of her destiny. She is determined to do so despite Cedar's warnings that the Law Codes governing the Council's actions do not permit self-interested interpretations of either Lapidarian visions or the manuscripts, and that those permitted to rise beyond Initiate status are permitted because they are sufficiently indoctrinated to refrain from trangression.

### The Next Conjunction, Part 2

After our introduction to Jaden, who I personally suspect to be a Rebel alchemist in the making, we meet the other two Junior Initiates, Laurel and Cercis. Jaden dislikes them both, finding them "obnoxious and intelligent" (page 23). Even worse, they're lovers and not at all discreet about it, since they flirt and clown around in class. Rather than deal with them most of the time, Jaden would either plot her escape or (if Sadira was teaching) actually engage the alchemical text under discussion.

Even worse, to Jaden's view, Cercis (and most likely Laurel) are eager to climb the ranks. Cercis regards the upcoming conjunction between Amur and Sadira with interest, because regardless of who prevails people will get promoted to fill open positions. Cersis is a good little corporate climber, and wants to make Senior Initiate as soon as possible.

Jaden, being more cynical, observes that it's possible for the conjunction to fail, citing the failed conjunction between Cedar and Ruis as precedent. Cercis counters by mentioning Ilex and Melia, lovers who conjoined during the 17th Council, and expresses a hope that he will similarly conjoin with Laurel, and subsequently rise to Azoth with her essence part of his.

Poor Laurel. I wonder if she knows what sort of plans Cercis has conconcted. Would she willingly sacrifice herself to his ambition?

Sadira herself is concerned about the upcoming conjunction, as we learn in the next scene on page 28. While she's waiting to meet Arjan outside the [Blue Mosque of Tabriz](https://en.wikipedia.org/wiki/Blue_Mosque,_Tabriz) in Iran, Sadira considered the possible results of her joining with the Novillian Scribe, Amur.

Though Sadira has taken certain precautions at Cedar's behest, her understanding of Council history suggests that when a senior member of the Council is paired with a junior member, it is the senior member's essence that dominates and subsumes the other. This contradicts official Council doctrine, but Sadira has thus far kept her doubts to herself. However, she suspects that she might not survive the Conjunction.

### The Recruitment of Arjan

I can't prove it yet for lack of textual evidence, but I think Cynthea Masson wrote Arjan's character as a [foil](https://en.wikipedia.org/wiki/Foil_\(literature\)) for Jaden. While Jaden was ignorant of the Council's existence and of alchemy prior to her conscription by Cedar, Arjan not only possesses some experience with mundane alchemy, but has seen his name mentioned in an alchemical manuscript while "reading of the elemental qualities of *Terminalia arjuna*" (page 30).

[*Terminalia arjuna*](https://en.wikipedia.org/wiki/Terminalia_arjuna), incidentally, is a tree native to the Indian subcontinent. Its leaves are used in silk production and in Ayurvedic medicine, and plays a symbolic role in Theravada Buddhism as a tree representing achieved enlightenment. Furthermore, [Arjuna](https://en.wikipedia.org/wiki/Arjuna) is the protagonist of the [*Mahabharata*](https://en.wikipedia.org/wiki/Mahabharata), one of ancient India's major epics (along with the *Ramayana*).

So, Arjan knows who he is. He's familiar with mundane alchemy. He knows he is to be an initiate of the Alchemists' council, and is eager to depart with Sadira.

### Sephrim: the Alchemist's Little Helper

Upon her return to Council dimension, the first person Sadira seeks out after reporting in and getting Arjan settled is Cedar, who is currently in the lower archives hunting for more evidence of bees disappearing from manuscripts to convince Azoth Ruis that Scribe Linden identified a legitimate phenomenon. It soon becomes obvious that the two are lovers, but both have more pressing concerns at the moment.

Sadira, for her part, is suspicious of how well Arjan took the recruitment, but has no evidence to suggest that either one of the Azoths made first contact, or that the Rebel Branch got to Arjan first. It would make sense for them to do so, though, and Cedar is obligated to ask because of the missing bees -- which may also be Rebel shenanigans.

However, Sadira has no evidence of either Rebel or Azothian involvement because it *is* possible for mundanes to obtain basic alchemical knowledge by studying the right manuscripts. It just isn't probable, especially in a Western society where [scientism](https://en.wikipedia.org/wiki/Scientism) (belief in the universal applicability of the scientific method) is one of the dominant ideologies.

The scene ends with Cedar slipping Sadira a brown packet of Sephrim, a drug for alchemists. It appears that Sadira is a long-time user, and has gotten the stuff from Cedar many times before, but she isn't sure where Cedar gets it or from whom. Cedar herself offers no information about her source.

While I have no textual evidence to back this hypothesis, I suspect that Cedar's getting her Sephrim from a Rebel alchemist either in an out-of-the-way part of Council dimension, or some neutral location. Regardless, it's probable that Sadira's [jones](http://mentalfloss.com/article/49625/how-did-jones-come-mean-craving) will prove plot-relevant later on. It certainly gives Cedar a handle she could use, were she of a mind to do so.

### The Care and Feeding of Alchemists in Council Dimension

Next is a brief interlude which begins with Jaden messing around in class drawing naughtier versions of the Rebis, the alchemical hermaphrodite, than the one shown below. That makes her something of a student after my own heart.

{% picture rebis.jpg alt="Kuthuma of Erks: the Rebis" %}

Arjan joins the class, making a splash with a joke about the mystical nature of conjunction that goes over Linden's head and [straight to plaid](https://www.youtube.com/watch?v=NAWL8ejf2nM). (Sorry. I couldn't resist. Anybody who knows me knows I'll eventually work in a *Spaceballs* allusion.)

With the Junior Initiates filled out, it's chow time. Instead of taking her usual table, Jaden joins her fellow JIs; her interest in Arjan currently outweighs her dislike of Cercis and Laurel.

As the new [FNG](https://en.wikipedia.org/wiki/FNG_syndrome) succeeding Jaden (who has now been in Council dimension over a year), Arjan is full of questions inspired by the surprisingly excellent food. He doesn't know that the Alchemists' Council is served by a large support staff of non-alchemists.

These outside contractors get paid in Lapidarian honey, created by the bees living in the Council's apiaries. The Council recruits them from the outside world, but does not necessarily *conscript* them as Jaden feels Cedar did to her. Instead, the Council looks for experienced, skilled people who are tired of life in the outside world and looking for a way out. At least, that's what Laurel tells Arjan on pages 39-40. There's no textual evidence to contradict that yet.

Lapidarian honey is a wondrous substance: it heals, fosters good health, and extends the lives of non-alchemists who eat it. It thus works similarly to the Elixir granted to Council alchemists once they turn thirty. Combined with the the alchemical vapors of Council dimension, which act as a sovereign antidepressant, Council support staff supposedly live long, purposeful lives free of despair.

Instead of living on Lapidarian honey, Alchemists are granted Elixir once they turn thirty. The Elixir dramatically slows aging, and becomes more effective as an alchemist rises through the ranks and develops their essence. It also heals injury and cures diseases, but alchemists with life-threatening illnesses or diseases that the Elixir cannot heal within a few days are placed inside special alembics within the catacombs.

{% picture bacta-tank.jpg alt="bacta tank used in Star Wars V: The Empire Strikes Back" %}

These alembics work similarly to devices like the bacta tank used in *Star Wars V: The Empire Strikes Back*, but might not necessarily look the same. Thanks to the Elixir and the catacombs, it is highly unlikely that an alchemist will actually *die*. Instead, there are only three ways to permanently leave Council dimension: conjunction, Final Ascension, and erasure.

### Playing Telephone with the Lapis

I had previously mentioned that messages from the Lapis pass through at least two human filters before the Alchemists' Council acts on them. It's actually *much* more complicated than that, as I'll show below.

First, consider Cersis' explanation of how Lapidarian ink is made on page 47. Novillian Scribes are endowed with the ability to "bleed the Lapis", scraping fine dust from it with a jewel blade, after undergoing a ritual conducted by the Azoth Magen called the Blood of the Green Lion. The dust is then mixed with channel water in a crystal bottle, and stirred with a rod of pure gold. This creates Lapidarian ink of a random color.

Attentive readers may recall page 5, where Cedar performs this process and creates azure ink instead of the indigo requested by Lapidarian Scribe Katsura. However, the creation of Lapidarian ink could be considered a secondary function of the Novillian Scribes, since the ability to bleed the Lapis must be granted by the Azoth Magen. Their primary function is *sapientia*; by placing one hand on the Lapis and another on a blank slate, a Novillian Scribe's mind and body becomes a conduit through which the Lapis transmits messages to a temporary medium.

The freshly inscribed slates are given to the Lapidarian Scribes, who use them to draft new manuscripts using Lapidarian ink made using the process above. Once the alchemical manuscript has been drafted, a Novillian Scribe reviews and revises the text. The manuscript is then brought to the Readers, who interpret the finalized text.

This process brings to mind a kindergarten game called [telephone](https://en.wikipedia.org/wiki/Chinese_whispers). A child would whisper a message to their neighbor, who would pass it on until it came to the last child in the group. The last child in the group would then announce the message as they understood it. The fun was in comparing the final message to the original, but the game also possesses instructive value in that it shows how easily errors can creep into a message passed between people.

To reiterate, a Reader gets information from the Lapis through one of two channels:

1. Alice touches the Lapis. Barbara transcribes Alice's slate. Claire reviews and revises Barbara's draft.
2. Alice touches the Lapis. Barbara transcribes Alice's slate. Alice reviews and revises Barbara's draft.

In the first case, the Readers get messages from the Lapis three steps removed from the source (Alice, Barbara, Claire). In the second, the messages come two steps removed (Alice, Barbara, Alice), but the original Novillian Scribe has the opportunity to alter the message they originally sent to the Lapidarian Scribes. Either way, I think it's probable that the Alchemists' Council has been acting on faulty information at least part of the time.

### Erasure and its Consequences

Everything we previously discussed sets the stage for the climax of this chapter, a confrontation between Jaden and Cedar. As part of a lesson in recognizing Lapidarian ink, Sadira tasked the Junior Initiates with attempting to distinguish between Lapidarian ink and the ordinary variety. Unable to do so by sight, Jaden attempted to do so by scent -- and spilled it all over a manuscript.

As a result, Jaden got summoned to Cedar's office for having rendered illegible nine square centimeters of *Elementa Chemicae 5663* folio 26 recto. Nine square centimeters isn't much; it's only 1.4 square inches for readers unfamiliar with metric units. You can see for yourself if you've got a ruler handy. Just draw a square 3cm wide on all sides.

Furthermore, the Council possesses backups of *Elementa Chemicae 5663* and other alchemical manuscripts used to teach initiates just in case one of them has a case of the butterfingers. However, restoring a manuscript from the backups is a painstaking process that distracts the Novillian Scribes from other duties, so naturally Cedar would prefer that Jaden be much more careful in the future.

To that end, Cedar explains to Jaden that she destroyed a Lapidarian section of an original alchemical manuscript that dates back to the 17th Council. Considering that *The Alchemists' Council* is set during the 18th Council, this implies that *Elementa Chemicae 5663* is not a new manuscript.

The manuscript is valuable because of its age alone, but Lapidarian portions of alchemical manuscripts are *puissant*. Changing such texts or the illuminations can alter Council history, obscure the identities of future Initiates so the Council cannot find and recruit them, or delete references to existing members of the Council -- which could change who they are or *erase* them.

Cedar doesn't want to be erased, not after having served the Council for centuries and rising to the position of Novillian Scribe, though full erasure is a complicated procedure that involves rooting out *every* reference to a person in the manuscripts. Furthermore, erasing Cedar would harm alchemists below Cedar's position.

Cedar drives home the consequences of erasing her on pages 57-58. If Jaden were to take out Cedar, she would go down with her. So would everybody else Cedar ever initiated into the Council. All would suffer. Worse, the Flaw in the Lapis would grow for lack of Quintessence.

This makes *deliberate* erasure of any alchemist granted a pendant containing Elixir a "nuclear option" for dealing with recalcitrant members of the Council, one the Council would not use unless a member caused sufficient trouble to convince the rest that matters had passed the [Godzilla Threshold](http://tvtropes.org/pmwiki/pmwiki.php/Main/GodzillaThreshold).

It also makes *accidental* or *malicious* erasure of the kind Jaden could have caused an *excellent* way to make enemies within the Council, and most likely a good way to get a one-way ticket back to muggledom -- though as Cedar points out, Jaden need only ask if she wants out that badly.

### Giving Schrödinger's Cat a Belly Rub

After Jaden learns on page 60 that she could return to her mundane life, and thus still has a choice, she does not make an immediate decision. Instead, she goes off on a tangent and asks Cedar why the Alchemists' Council hasn't initiated every human being on Earth. Cedar's answer on page 61 is thus:

> "We are the elite, Jaden. We are the Alchemists' Council. And our responsibility to the world is unfathomable to the uninitiated."

I don't know about you, but this raises my hackles. I could easily imagine Charles Manson saying something similar to his Family, insisting that they're the only ones who can bring Helter Skelter down on the world.

Jaden, however, persists and says, "Expand the Tree. Save the World."

Unfortunately, it isn't so simple. Apparently the Lapis can only sustain a Council of a hundred and one members (similar to the United States Senate, when the Vice President acts as President of that once-august body). When a pendant-carrying member of the Council is erased, that person's essence is lost and can never return to the Lapis. While the erased member is eventually replaced, this is not the preferred method to create open slots for Junior Initiates.

The ideal method is renew the Tree through Azothian Final Conjunction, where the Azoth Magen conjoins with the Lapis and contributes their refined essence, or through alchemical conjunction. However, the consequences of conjunction go beyond the participants despite my earlier joke about conjunction being alchemical Thunderdome.

It isn't just a matter of two alchemists enter, and only one leaving. I explained earlier that who gets initiated depends on who survives conjunction, and my explanation prompted Ms. Masson to ["wonder if one of the #CouncilCats is named Schrödinger"](https://cyntheamasson.com/2016/05/17/reading-the-alchemists-council-part-1/).

It's no joke, as Jaden learns on pages 62-66 after she asked Cedar whether she had been through conjunction. Upon learning that Cedar had conjoined with Saule, consuming the other alchemist's essence while remaining herself, Jaden is outraged. She views the process of conjunction as one of murder, and finally decides that she wants out. She wants no part of a Council whose Readers suggest conjuctive pairings that the Elders then approve, so that one continues to live through the centuries while the other dies.

However, Cedar has one final revelation to offer on page 65:

> "Saule conjoined with me so you could live and renew the Council. Her death gave you the potential for eternal life as the new Initiate. If Saule had been victorious, you would have died on the day Saule and I conjoined."

Instead, it was a potential initiate named Taimi who died, her possibilities consumed along with Saule's essence. It's a hell of a guilt trip for Jaden, but that's the way the waveform collapses.

If you want more of the Rebel Branch Initiates' Guide, [check out the main page](/commentaries/the-rebel-branch-initiates-guide-to-the-alchemists-council/).
