---
title: BOHICA
category: Website
tags: [ "new website", "site refresh", reboot, Jekyll, "GitHub Pages", "Minimal Mistakes", "Dark Souls III", "Cynthea Masson", "The Alchemists' Council", "Glen Cook", "The Black Company", BOHICA, "Bend Over. Here it comes again." ]
excerpt: Yep. I'm doing yet *another* site refresh now that I figured out Github Pages.
header:
  image: waiting_spider_web.jpg
  teaser: waiting_spider_web.jpg
---
{% include base_path %}

Welcome to the new [matthewgraybosch.com](https://matthewgraybosch.com), or what will be the new [matthewgraybosch.com](https://matthewgraybosch.com) once I finish building out the site and change the DNS.

Instead of clunky old WordPress or one of my own hamfisted attempts at designing a nice-looking website, I'm building out a new site using the following tools:

* [Jekyll](http://jekyllrb.com)
* [GitHub Pages](https://pages.github.com/)
* [Jekyll Now](http://www.jekyllnow.com/)
* the [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/) theme by Michael Rose

## Enough with the nerdiness. What else have you got to offer?

Well, if you're going to cop *that* attitude, how about some crappy *Dark Souls III* videos?

<iframe width="560" height="315" src="https://www.youtube.com/embed/oc8-EaCB47c" frameborder="0" allowfullscreen></iframe>

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/9PV65PjI7QA" frameborder="0" allowfullscreen></iframe>

## Aren't you supposed to be a writer? Do you even read?

Sure I do. How about a ten-post series in which [Eric Higby](https://twitter.com/StileTeckel) and I read the new literary fantasy by Canadian novelist [Cynthea Masson](https://cyntheamasson.com/), ***The Alchemists' Council***? My copy should come today, according to Amazon. :cat:

![THE ALCHEMISTS' COUNCIL by Cynthea Masson]({{ base_path }}/images/cynthea-masson-alchemists-council.jpg)

Or how about a solo read-through of Glen Cook's *The Black Company*?

Or how about some badass sci-fi like the original *Starbreaker*, which I've never before offered to the public? Or a mirror of the web serial of *Silent Clarion*? The completed novel contains hundreds of changes; will *you* be able to spot the differences?

Watch this space for updates!
