---
title: "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 2"
excerpt: "Welcome back to my readers' guide to *The Alchemists' Council* by Cynthea Masson, with guest commentary by Eric Higby. This post covers Chapter 2."
header:
  image: honeybee-964178_1280.jpg
  teaser: honeybee-964178_1280.jpg
  caption: "[Source: Pixabay](https://pixabay.com/en/honeybee-pollinator-insect-flower-964178/)"
coauthor: ehigby
toc: true
categories:
  - Books
  - Longform
tags:
  - Cynthea Masson
  - The Alchemists' Council
  - ECW Press
  - Canada
  - 2016
  - literary fantasy
  - intrigue
  - politics
  - alchemy
  - environmentalism
  - intention
  - ego
  - Buddhism
  - urban fantasy
  - portal fantasy
  - commentary
  - read-through
  - read-along
  - reader's guide
  - Eric Higby
  - Chapter 2
mentions:
  - url: http://ecwpress.com/blogs/whats-new/117876421-a-reader-s-guide-to-cynthea-masson-s-the-alchemists-council
    title: "ECW Press: A Reader’s Guide to Cynthea Masson’s *The Alchemists’ Council*"
  - url: https://cyntheamasson.com/2016/06/09/reading-the-alchemists-council-chapter-2/
    title: "Cynthea Masson: Reading *The Alchemists’ Council*: Chapter 2"
---
A couple of weeks ago, [Eric Higby](http://thecaverns.net) and I  dug deep into the first chapter of [Cynthea Masson's](http://www.cyntheamasson.com) new literary fantasy, [*The Alchemists' Council*](https://www.goodreads.com/book/show/26113691-the-alchemists-council) published by [ECW Press](http://ecwpress.com/collections/books/products/alchemists-council). In [The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 1]({% post_url 2016-05-23-the-rebel-branch-initiates-guide-to-the-alchemists-council-chapter-1 %}), Eric and I covered the following topics:

* The Missing Lapidarian Bees, Part 1
* The Next Conjunction
* Jaden's Resentment
* The Recruitment of Arjan
* Sephrim: the Alchemist's Little Helper
* The Care and Feeding of Alchemists in Council Dimension
* Playing Telephone with the Lapis
* Erasure and its Consequences
* Giving Schrödinger's Cat a Belly Rub

This week, we'll be building on the themes introduced in Chapter 1 and continuing several subplots. As before, I'll present Eric Higby's commentary first, since his take on Chapter 2 is more concise than my own. Mine will follow for readers who wish to go into greater detail. Expect spoilers ahead.

**THE ALCHEMISTS' COUNCIL FORBIDS YOU TO READ ANY FURTHER.**

**THE REBEL BRANCH ENCOURAGES YOU TO CONTINUE, BUT THE CHOICE IS YOURS...**

## Eric Higby on Chapter 2 of *The Alchemists' Council*

Last time we all met up to talk about the Alchemists Council I told you to meet me in the Initiate Common Room. Apparently, if you are here, then you figured out where that was, and we can continue with chapter two! Sit down, grab a snack and beverage, and let’s get going. I’ll wait for you.

We go into chapter two with Jaden very upset about what she has learned about conjunction. Her investigations begin to dig up more information which helps us learn with Jaden about the world she is in.

The plot thickens in relationship to the disappearing bees, and more and more people are getting involved with the concern. The council seems to face a problem that I know I and all too many others face in real life. Not enough time to handle the workload! Cedar, as a result, starts putting some unusual (at least for the Council) ideas into place to help the situation.

This puts Jaden and Arjen working together back in our mundane world, thus allowing them to further their investigations which are now focusing mostly on erasure.

Erasure is a technique by the alchemists in which when one of them is banished to the mundane world; they are also erased from the council manuscripts. This then causes others to forget them.

This was one of the first things that came up that gave me a bad taste for the council. Altering the memories of individuals for their needs certainly does not seem something like the “good” guy would do. It does not sit too well with Jaden either, and she wants to know everything forgotten.

I believe if I went around erasing people on occasion, I would get a bit more worried when things start disappearing from the library, like the bees, eh? Karma and all that.

The chapter ends with what the Council feels is a rebellion attack, by someone previously erased! We are left as we go into chapter three with the single name. “Kalina.”

Let’s meet up next week in Elixir Chamber of the initiate room, and well toss back some random bottles before we look at chapter three. Could be interesting!

## A Rebel Branch Initiate's Guide to *The Alchemists' Council*: Chapter 2

The climax of Chapter One left Jaden bearing a heavy burden of guilt over the two deaths that opened the way for her induction into the Alchemists' Council. Her determination to find out who opened the way for Arjan will lead her to a revelation more shocking than those she's already weathered. Buckle up for spoilers; I'm covering the following topics in pages 67-119 of the paperback.

* Erasure: Making an Unperson
* The Missing Lapidarian Bees, Part 2: Grunt Work for the Junior Initiates
* Arjan's Reaction and Promise
* Sadira's Concerns About Arjan
* The Missing Lapidarian Bees, Part 3: As Below, So Above
* A Field Trip to Santa Fe
* Searching for Pendants
* Why Turquoise?
* Broken Memories of Kalina
* The Missing Lapidarian Bees, Part 4: A Ritual Interrupted

### Erasure: Making an Unperson

{% picture yezhov-and-stalin.jpg alt="Kliment Voroshilov, Vyacheslav Molotov, Joseph Stalin, and Nikolai Yezhov, going out for cocktails" %}

As I mentioned before, learning that Saule died during alchemical conjunction with Cedar and the potential initiate Taimi followed her rocked Jaden, and left her guilt-ridden. Since Cercis and Laurel are senior to Jaden, and she dislikes them anyway, the only Junior Initiate with whom she can share the truth about conjunction and how new Initiates gain admission to the Council is Arjan. She believes that Arjan would share her outrage.

However, Jaden doesn't know who conjoined to open up Arjan's slot. She attempts to gain access to the current century's Council minutes for research, and explains to Scribe Obeche that she needs the information for a history assignment.

Rather than tell her, Obeche asks Jaden what exactly she's looking for, pointing out with no little pride his repute as an "archival mastermind". His pause after Jaden asked about the most recent conjunction should be an attentive reader's first clue that he's hiding something. The next clue is that he cites the most recent conjunction as that of Cedar and Saule, but that was the conjunction that led to *Jaden's* initiation -- which Jaden already knows.

Jaden presses him, demanding to know who conjoined before Arjan's initiation, and Obeche claims to have forgotten about Arjan as if new Initiates -- especially those who found themselves mentioned in Lapidarian manuscripts prior to recruitment -- were so common as to be beneath notice. However, there's no dancing around the truth now. Obeche admits that Arjan's place was opened by *erasure*, not conjunction.

Obeche justifies the necessity of erasure as necessary for the survival of the Council, which is continually threatened by the actions of the Rebel Branch, but that is cold comfort to Jaden. I'll explain why in a moment.

As I explained in "Chapter One: Erasure and its Consequences", alchemical erasure is the Council's preferred method for handling recalcitrant members. The scribes root out every reference to the erased person from the Lapidarian manuscripts, and the former Alchemist gets booted out of Council dimension.

Alchemical erasure is not a new concept. The Soviet Union under Stalin regularly made unpersons of members of the Communist Party who fell from favor. The word "unperson" comes from George Orwell's [*1984*](http://www.goodreads.com/book/show/5470.1984), the warning against totalitarian socialism that became the unofficial playbook for Anglo-American police and intelligence agencies. The Romans reserved erasure for traitors, and gave us the Latin term [*damnatio memoriae*](https://en.wikipedia.org/wiki/Damnatio_memoriae): the "condemnation of memory". Even the Pharaohs of ancient Egypt regularly made unpersons of their predecessors, especially Ramses II, striking their names from monuments and replacing them with their own.

However, the Alchemists' Council can do what real-world tyrannies cannot: it can erase memories of its outcasts from the minds of individuals within Lapidarian proximity. Even the identity of an erased person is on a need-to-know basis, and those not trusted with this information do not remember the names of erased friends within Council dimension. If they carry pendants, then they cannot even remember the erased when they return to the outside world.

Thus, Jaden now knows that *somebody* had been erased to open the way for Arjan's recruitment. She doesn't know who, and is naturally distressed. Imagine somebody you cared about was made to disappear, and you weren't even allowed to remember their names.

{% picture the-commissar-vanishes.jpg alt="'Nikolai Yezhov? Who's that, comrade? Kliment, Vyacheslav, do you know who this Yezhov fellow is?' 'Nyet!'" %}

### The Missing Lapidarian Bees, Part 2: Grunt Work for the Junior Initiates

After meeting with Jaden at the end of Chapter One, Cedar got the word that Azoth Ravenea wanted to meet with all of the Novillian Scribes, two Lapidarian Scribes, and to Readers in the North Library. This is another talky scene, but along with with Obeche's revelation to Jaden about the full effects of erasure, it will drive the action of the rest of this chapter.

Ravenea's concerned about the bees disappearing from Lapidarian manuscripts in the Vienna protectorate, even though Ruis doesn't seem to care. She means to do something about the situation herself since it barely got a mention at the last Council meeting.

Lapidarian Scribe Katsura suggests that all Scribes and Readers focus on investigation to find a definitive cause, but Obeche dissents. He thinks the work is too difficult, too mundane, and would distract from the concerned orders' normal duties to the detriment of the Council and Council dimension. Finding new bees were they didn't exist before would be enough of a pain in the ass, but disappearing bees? I suspect Obeche is as interested in finding *missing* manuscript bees as I would be in reverse-engineering a legacy system implemented in ten million lines of COBOL code.

Cedar, true to what I still suspect is her form, pounces on the opportunity to win points with Obeche by suggesting that even devoting half of the Scribes and Readers to the job is problematic, since the work would require that the alchemists assigned make extended visits to the protectorate libraries located in the outside world. Instead, she suggests that two groups composed of two Novillian Scribes, two Lapidarian Scribes, two Readers, two Senior Magistrates, two Junior Magistrates, and two Senior Initiates work in rotating shifts to investigate the missing bees.

When Obeche questions Cedar on her suggestion, Cedar points out that she herself found a missing bee in *Sursum Deorsum 5055*, in Council dimension. (Incidentally, "sursum deorsum" literally means "Up Down" or "Upside Down" in Latin.) This blows the hypothesis that it's just the protectorate libraries under attack out of the water.

The only objection to Cedar's proposal comes from Obeche, who insists that Lapidarian Scribe Amur not be sent on this mission because his impending conjunction makes him a valuable target. He offers to go in Amur's stead, which would settle the matter if not for an additional proposal from Cedar.

Cedar's suggestion that all four Junior Initiates also be brought on board results in several council members having kittens. (If you adopted one, please post a pic on Twitter with the hashtag #CouncilCats and tag @cyntheamasson.) Obeche is concerned that the Junior Initiates aren't ready to handle such work due to their limited grasp of basic concepts.

Cedar counters Obeche's objection by framing her proposal as an opportunity to not only train the Juniors and continue their indoctrination to prevent a repeat of the "most recent debacle" (p. 78), but give them practical anti-Rebel experience. The Juniors could work in pairs, with each pair assigned to a different protectorate library with a Scribe to escort them in and out of Council dimension.

While precedent for putting the newbies to work exists, there's still one little problem. Without pendants, the Junior Initiates sent outside will leave Lapidarian proximity. That means they may start to remember the erasure Obeche mentioned to Jaden earlier, as well as the events leading to it.

However, Cedar has an answer for this as well: interim pendants made of turquoise infused with Lapidarian essence, which were last given to Junior Initiates during the Second Rebellion. The essence-infused pendants grant Lapidarian proximity for a limited time, and must be recharged, so the Junior Initiates remain leashed to the Council. Though this doesn't satisfy Obeche, Cedar manages to persuade everybody else.

### Arjan's Reaction and Promise

Jaden finally gets the chance to tell Arjan about how conjunction really works, but he does not react as she hoped/expected he would. Instead of being as outraged as Jaden, Arjan remains sanguine about the necessity of conjunction.

However, he suggests another possibility. Cedar (and Jaden) might be wrong about Saule being gone. Something of Saule might still exist within Cedar, so that the post-conjunction Cedar's personality is subtly different from that of pre-conjunction Cedar.

This is something Jaden hadn't considered, and it doesn't jell with the impression she got from Cedar. When she points this out to Arjan, he suggests that the result of each conjunction differs, depending on the participants.

With no counterargument, Jaden instead changes the subject and tells Arjan that his initiation was made possible through erasure. Again, he is much more sanguine about the revelation than Jaden had been. However, it helps the Sadira had already told him. It also helps that Arjan didn't know any of the people involved.

However, once Jaden makes Arjan face the possibility that the next person to be erased may be somebody important to *him*, he agrees to help Jaden. He counsels Jaden to be patient, and wait for an opportunity to escape Council dimension.

### Sadira's Concerns About Arjan

Poor Sadira's got a lot on her mind lately. Arjan's foreknowledge of his initiation thanks to his prior alchemical learning isn't the only quality that makes him a person of interest to the Council; various Readers have compiled "extensive evidence" (p. 88) of his significance.

However, Arjan isn't unprecedented. The annals of the 7th Council preserve a tale of "a prophet who had forseen her alchemical destiny". Years later, this person singlehandedly undermined a Rebel push threatening Council control of the outside world called the Breach of the Yggdrasil.

{% picture ash-yggdrasil-heine.jpg alt="'The Ash Yggdrasil' (1886) by Friedrich Wilhelm Heine" %}

---

Yggdrasil, by the way, is the World Tree that holds in its boughs the [nine worlds of Norse mythology](https://en.wikipedia.org/wiki/Norse_cosmology) (Asgard, Midgard, Vanaheim, Muspelheim, Nifelheim, Alfheim, Svartalfheim, Jotunheim, and Helheim). It is also the tree from which Odin hanged himself for nine days, sacrificing himself to himself to learn the secret of the runes in the poem *Hávamál*, from the *Poetic Edda*. Stanza 137 tells the tale:

> I know that I hung on a windy tree  
> nine long nights,  
> wounded with a spear, dedicated to Odin,  
> myself to myself,  
> on that tree of which no man knows  
> from where its roots run.

For those with a musical bent, the Swedish symphonic metal band Therion also dedicated a concept album to Yggdrasil and the Nine World called [*Secret of the Runes*](http://www.metal-archives.com/albums/Therion/Secret_of_the_Runes/417). Readers with Spotify accounts can listen using the widget below.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A22TlRcrCGqRIefG2HRxf3b" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

---

Whoever singlehandedly defeated the Rebel Branch during the Breach of the Yggdrasil left a hell of a pair of shoes for Arjan to fill. Whether he'll do so remains to be seen. In the meantime, Obeche is happy to remind Sadira that since Arjan is still only an initiate, he can always be erased. While he's correct, I doubt Sadira finds this reminder helpful; no doubt it would reflect poorly on Sadira if one of her initiates were to become an unperson, just as any misconduct on her part would reflect badly on Cedar.

Speaking of Cedar: apparently Sadira had turned to Cedar for comfort after Cedar's conjunction with Saule. Prior to this conjunction, Sadira had apparently been close enough to Saule to desperately fear losing her (p. 89). Afterward, Sadira had latched on to Cedar, who forged such a strong and comforting emotional bond with Sadira that the younger alchemist had been the one to make the relationship a more intimate one.

So, she has no doubts about Cedar. Instead, she continues to harbor doubts about her future with the Council. Like Jaden, Sadira also resisted the Council's indoctrination at first, and researched dimensional space in hope of find a way to escape Council dimension. She had hoped the tunnels beneath the Council grounds would offer a way out, but they never did.

That old restlessness returned along with the memory, and Sadira deals with it by finding her Sephrim stash (which she stores using the classic device of a hollowed out book) and does a hit. As shown on page 91, the drug doesn't just affect Sadira; it also appears to affect her pendant. I think this implies that Sephrim isn't just a drug, but somehow boosts or amplifies the essence held within its user and their pendant.

Either way, it leaves Sadira nice and purry while she waits for Cedar.

### The Missing Lapidarian Bees, Part 3: As Below, So Above

It's bee time again as Cedar hunts down Amur to ensure she can count on his support. She found him in the lower archives, poring over a manuscript. Next time him sat Obeche, flipping through magazines from the outside world and doing research on colony collapse disorder.

Though Cedar thinks Obeche is just wasting time, no doubt due to their frequent disagreements, there's a method to his apparent madness. Thus far, the relationship between Council dimension and the real world could be described by the formula ["as above, so below"](http://www.themystica.com/mystica/articles/a/below_above.html): because the microcosm affects the macrocosm, actions performed in Council dimension could affect the outside world. Were this not the case, then the Council's efforts to maintain elemental balance would do nothing to preserve the outside world.

However, Obeche has come to suspect that the cause of the missing bees in Lapidarian manuscripts is far more ominous than mere Rebel shenanigans. He thinks that the real culprit is colony collapse disorder in the outside world. If he's correct, the disappearance of bees in the real world is reflected by the disappearance of bees in the manuscripts.

Obeche blames this phenomenon on a second flaw in the Calculus Macula, a dimensional fissure that allows the outside world, or somebody there, to affect Council dimension and its protectorates. He hasn't mentioned his hypothesis to the Azoths yet, nor has he found evidence of the "as below, so above" phenomenon.

Thus Cedar comes to Sadira without accomplishing her original objective, talking to Amur about the conjunction. Not that Sadira minds. She has other uses for Cedar.

### A Field Trip to Santa Fe

The next day, Jaden gets sidetracked on her way to the North Library by the sight of Laurel and Sadira chatting by a fountain. Cercis is there, as well as Arjan and Cedar. They're about to make a jaunt outside, and were waiting for Jaden to find them.

I'm surprised the Council doesn't keep pages on staff (paid in Lapidarian honey, naturally) to find members of the Council and tell them when and where they're wanted, but that might be one of the hats a Junior Initiate gets stuck wearing for everybody else's benefit. Rank hath its privileges, even in Council dimension.

In any case, the jaunt isn't just a bit of fun. Cedar will escort the Initiates to Santa Fe, New Mexico for a test; those who pass will "progress to the next stage of the venture", according to Sadira (p. 97). She doesn't provide any further detail than that, save that they'll be using the Salix portal instead of Quercus because they're heading to North America -- and that they'll need to bring layers to accommodate temperature changes.

This is the very opportunity Jaden had been hoping to get, time outside of Lapidarian proximity to recover her memories. However, the fact that the order came from Cedar makes Jaden nervous. Unlike Sadira, the younger woman doesn't trust the Novillian Scribe.

{% picture san-miguel-chapel-santa-fe.jpg alt="the San Miguel Chapel in Santa Fe, NM" %}

### Searching for Pendants

Jaden was first to return outside with Cedar's help, but Cedar left her standing on a street corner in Santa Fe with instructions to wait. Wait Jaden did for over thirty minutes, pacing outside the Loretto Chapel and worrying that she had been abandoned in a foreign city without money or documentation, until Cedar showed up again with Arjan in tow.

Cedar's explanation? An "impromptu meeting with Obeche" (p. 100). That's all she told Jaden, and Jaden has little choice but to take the older woman's explanation at face value, but *I* smell a Lapidarian rat. Given the antagonistic relationship between Cedar and Obeche, I'm surprised Obeche would want an impromptu meeting with her.

In any case, after Jaden tells Cedar she's tired of waiting on the street, Cedar suggests that she and Arjan wait at the cafe in La Fonda. She then returned to Council dimension, prompting Jaden to wonder if muggles notice members of the Alchemists council appearing and disappearing in normal space. "Apparently they don't", according to Arjan (p. 101).

Since they're alone together, Jaden and Arjan start talking erasure again. Jaden approached Sadira on the subject, but all she learned was that the Council doesn't simply *dump* its outcasts back in the real world. Instead, they are provided the basics they need to make new lives for themselves: a place to live, an identity, and some cash. The more Elixir an erased alchemist has ingested prior to being stripped of their Pendant, the longer they'll survive outside. Meanwhile, the Scribes manually and alchemically remove references to them from all *known* manuscripts.

This raises the possibility that a reference to an outcast can survive in manuscripts unknown to the Alchemists' Council, though it strikes Jaden and Arjan as somewhat unlikely. However, I doubt it would have come up if it wasn't somehow relevant. Keep an eye out in subsequent chapters.

In the meantime, Jaden grows frustrated that the memories haven't started to come back yet, but Arjan reminds her that it takes time away from Council dimension. Furthermore, the restoration is temporary; she'll lose them again when she returns, and different memories might resurface the *next* time she leaves Lapidarian proximity long enough to recall the erased. However, it's possible that Jaden had already *written* about the erased person since she has an old journal in which she hasn't written for some time.

Once everybody's gathered, Jaden pays for her coffee and Arjan's. Arjan, in turn, buys her a pen and some paper while chiding her about not taking advantage of her alchemical skills to create wealth for herself. They follow Cedar to the restaurant in La Plazuela, where it finally occurs to Jaden that Cedar has a *history*, and that Santa Fe might be part of this history. The city might have meant as much to her as Vancouver means to Jaden.

Once the orders are placed and the drinks served, Cedar makes a toast with a special meaning to Jaden: "May you find here all that you seek" (p. 107). She then explains the Initiates' mission in Santa Fe: they are to find stones suitable for use as interim pendants, so that they can help the senior alchemists research the missing bee effect in the protectorate libraries. These stones must be capable of withstanding Lapidarian infusion, and apparently turquoise works best.

{% picture turquoise-pendant.jpg alt="an Anasazi freeform turquoise pendant" %}

### Why Turquoise?

Cedar didn't explain why the Initiates should look specifically for turquoise, but a bit of research reveals some possibilities.

Turquoise posesses some interesting chemical properties as a hydrated phosphate of copper and aluminum. It never forms single crystals, but instead has a triclinic crystalline structure. The vectors are never the same length, and never intersect at right angles. Furthermore, turquoise can only be dissolved in heated hydrochloric acid, and despite being fairly soft compared to other gemstones (6 on the Mohs scale, slightly harder than window glass), turquoise takes on a good polish.

While our name for the stone comes from *turque*, the French word for Turk, because turquoise entered the modern European trade via the Silk Road, humans have used turquoise to decorate objects and make amulets since the First Dynasty thousands of years ago.

King Tutankhamun's burial mask is inlaid with turquoise. The stone is also associated with the goddess Hathor, who was the patroness of Serabit el-Khadim, where ancient Egyptians once mined the stone. In Chapter 28 of the Book of Exodus, turquoise is one of the stones inlaying the breastplate worn by the High Priest of the Jews. The Persians used it to decorate just about *everything*, and the stone also saw use in Mesopotamia and in China during the Shang dynasty.

{% picture king-tut-burial-mask.jpg alt="The iconic gold burial mask of Tutankhamun, inlaid with turquoise, lapis lazuli, carnelian and coloured glass." %}

In the west, pre-Columbian Native Americans used turquoise to make amulets and for decorative purposes. Though the silver and turquoise jewelry made by the Navajo and other tribes in the American Southwest is of a more modern design, and made primarily for trade with outsiders, it was once as sacred to them as it was to the Aztec and the Maya.

{% picture xiuhtecuhtli-mask.jpg alt="Turquoise mosaic mask of Xiuhtecuhtli, the aztec god of fire." %}

No doubt the author considered many of these factors when choosing the recommended material for the interim pendants.

### Broken Memories of Kalina

Cedar leaves them after lunch with instructions to meet at the San Miguel Mission at five, but the Initiates talk amongst themselves first. While discussing the historical precedent for their being granted interim pendants, Laurel remarks that she hasn't had this much fun since visiting a spa in Vienna with somebody named Kalina.

There's just one problem: nobody knows who Kalina is, and Laurel has to explain that she's a Senior initiate. Or, rather, she *was*. Not even Laurel actually remembers Kalina. She just has a scene from a memory: her and Kalina at the spa in Vienna.

Jaden writes this intelligence down, noting that the one erased may be Kalina, and reluctantly begins her search for a pendant. However, when she returns to Council dimension and cracks open her notebook to review it, *all of the pages are blank*. This suggests two possibilities:

* Erasure can effect textual references to erased individuals in non-Lapidarian writings *after* erasure.
* Lapidarian proximity doesn't simply erase memory; it also affects *perception*. What Jaden wrote in Santa Fe might still be there, but she can't see it because it mentions Kalina.

I think the latter is more likely, simply because that's how *I* would write it, but we don't have enough textual evidence to rule out either possibility. We will have to see if Jaden sees what she wrote the next time she leaves Council dimension without a pendant, or if Cedar gets her hands on the notebook and sees Kalina's name mentioned.

### The Missing Lapidarian Bees, Part 4: A Ritual Interrupted

Several days after the Junior Initiates' visit to Santa Fe, Cedar participates in a partial Ritual of Restoration led by the Azoth Magen himself, Ailanthus. This partial ritual is aimed directly at protecting and restoring the Lapidarian manuscripts.

Like many rituals, the Ritual of Restoration is structured so that the Azoth Magen leads the invocations, and those gathered with him offer the prescribed response when appropriate. Christian readers -- especially Catholics, Anglicans, and Lutherans -- may note similiarties between the Ritual and traditional celebrations of the Eucharist in this regard.

The Azoth Magen invokes the elements (Earth, Wind, Sea (water), and Ember (fire)), and the arboreal essence of plants and trees. He then invokes the "Lapidarian promise of *Ruach 2103* folio 51 verso -- the manuscript leaf from which Linden first witnessed the bees disappearing" (p. 117). In addition to calling upon the apiarian memory of this text, Ailanthus summoned that of all other Lapidarian manuscripts.

The Elder Council spent over an hour on this ritual intended to restore the Lapidarian bees to the manuscripts, but something has gone wrong. The alchemical blue light generated by their work is fading. However, Cedar is so deep in the flow of the ritual that she doesn't notice she's the last person still chanting until Ailanthus commands silence.

As the blue light fade, a scarlet light replaced it. With it came strident laughter in a voice everybody believed had been banished from Council dimension through erasure. Thoroughly panicked by this voice, the Elder council scrambles to find evidence in the Lapidarian manuscripts of an imminent rebellion, or for evidence that the erasure itself had failed on an elemental level.

They found nothing. Kalina shouldn't have been able to reach back into Council dimension to laugh at them. All the same, she was able to do so and offer the Elders a token of her contempt.

{% picture erased-commissars-return.jpg alt="Return of the Erased Commissar" %}

↑ Sorry. I couldn't resist. ↑ If you want more of the Rebel Branch Initiates' Guide, [check out the main page](/commentaries/the-rebel-branch-initiates-guide-to-the-alchemists-council/).
