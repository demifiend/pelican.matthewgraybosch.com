---
title: "Did I Really Waste Seven Years Listening to Digital Music?"
excerpt: "'Cause it sure *feels* like I wasted the last 7 years listening exclusively to digital music."
categories:
    - Personal
    - Music
tags:
    - stereo
    - low-end system
    - bookshelf system
    - CDs
    - MP3
    - MP4
    - audiophile
    - home system
    - sound
    - albums
    - heavy metal
header:
    image:  my-new-stereo.jpg
    teaser: my-new-stereo.jpg
---
Did I really waste the last seven years listening almost exclusively to digital music I purchased from services like iTunes Music or streamed from services like Spotify? 'Cause it feels that way.

I got this stereo while Catherine's away, figuring that it would be a good time to get into the habit of writing at home instead of going out and eating pizza every day.

I used to have a boom box with a tape deck and CD player as a young man. Back in the day I also had a little routine where I'd start up an album and let it just run straight through while I worked. I wouldn't get up until it was time to either start the album up again or put in a different one.

I had all this music on CD that for the last few years had just been useless clutter. I had a choice: either get rid of the clutter, or make it useful again. I didn't really want to get rid of these albums, many of which I bought when I was in high school or college, or while I was courting Catherine. We used to listen to many of these together, especially the stuff by Savatage and Ayreon.

So I decided to make them useful again, indulging in a bit of nostalgia as I bought a bookshelf system that (if you ignore the NFC logo and USB jack) rather closely resembled a home stereo receiver from the 1980s. However, there's an upside to the nostalgia: the music sounds *better* on disc and on a stereo system instead of piped through headphones.

At least, the albums I've played today do:

 * Thank You Scientist: *Maps of Non-Existent Places*
 * Haken: *Affinity*
 * Iron Maiden: *Dance of Death*
 * Black Sabbath: *Cross Purposes*
 * Edguy: *Rocket Ride*
 * Ars nova: *Goddess of the Darkness*

And this is on a consumer-grade Sony bookshelf system I bought at Worst Buy for $200. I wonder how a good component system would sound. I guess I'll find out once Catherine and I have bought a house with a finished basement that I can claim as my man cave.
