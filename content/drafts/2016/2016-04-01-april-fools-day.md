---
id: 3604
title: April Fools Day
date: 2016-04-01T20:30:44+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3604
permalink: /2016/04/april-fools-day/
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"96832c72f062";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:55:"https://medium.com/@MGraybosch/april-fools-96832c72f062";}'
categories:
  - Bad Jokes
tags:
  - april fools
  - give up writing
  - google
  - starbreaker cancelled
  - troll
  - trolling
---
No, I haven't given up writing or cancelled Starbreaker. I was [just fucking with people on Google+](http://plus.google.com/u/0/+MatthewGraybosch/posts/M9R5QYM1gL1) on April Fools Day, and nobody fell for it. Maybe G+ _is_ the social network for smart people.<figure id="attachment_3615" style="width: 531px" class="wp-caption aligncenter">

<a href="http://www.matthewgraybosch.com/2016/04/april-fools-day/gplus-april-fools-screenshot/" rel="attachment wp-att-3615"><img src="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/gplus-april-fools-screenshot.png?fit=531%2C746" alt="Google+ April Fools Screenshot because embeds no longer work" class="size-full wp-image-3615" srcset="http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/gplus-april-fools-screenshot.png?w=531 531w, http://i1.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/gplus-april-fools-screenshot.png?resize=214%2C300 214w" sizes="(max-width: 531px) 85vw, 531px" data-recalc-dims="1" /></a><figcaption class="wp-caption-text">Google+ April Fools Screenshot because embeds no longer work</figcaption></figure> 

Too bad the nerds at Google don't know what they're doing. The screenshot above is a case in point. Why can't I embed posts on WordPress any longer?