---
title: Unsolicited Advice on Beds
excerpt: Queen-sized IKEA beds and non-IKEA mattresses with split box springs don't exactly go together. You might be better off getting a new frame.
date: 2016-04-01T15:11:13+00:00
header:
  image: ikea-hopen-bed.jpg
  teaser: ikea-hopen-bed.jpg
categories:
  - Personal
tags:
  - bed
  - box spring
  - Costco
  - frame
  - IKEA
  - queen size
  - split box spring
  - two piece box spring
---
If you have a queen-size slat bed from IKEA like [any of these](http://www.ikea.com/us/en/catalog/categories/departments/bedroom/16284/), and you decide to get a new mattress with a box spring, think twice about getting a split box spring. A split box spring comes in two pieces, and they don't exactly fit correctly in an IKEA bed, as my wife and I found out the hard way.

No, not like that. We were both on the bed, and one of our cats decided to join us. The box spring on my side slipped out of place and _down_, spilling me out of the bed. I'm still sore, and Virgil's tale is still puffed, but nothing's broken. Fortunately, I can get a replacement frame [at Costco](http://www.costco.com/Premium-Universal-Lev-R-Lock%C2%AE-Bed-Frame.product.100232767.html) for a reasonable price.

#### Image Credit

Picture taken from [Pinterest](https://www.pinterest.com/pin/99079260522794830/)
