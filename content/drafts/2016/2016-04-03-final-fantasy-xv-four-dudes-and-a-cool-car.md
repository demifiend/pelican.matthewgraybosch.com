---
id: 3668
title: 'Final Fantasy XV: Four Dudes and a Cool Car'
date: 2016-04-03T03:34:44+00:00
author: Matthew Graybosch

guid: http://www.matthewgraybosch.com/?p=3668
permalink: /2016/04/final-fantasy-xv-four-dudes-and-a-cool-car/
yuzo_related_post_metabox:
  - 'a:1:{s:21:"yuzo_disabled_related";N;}'
medium_post:
  - 'O:11:"Medium_Post":11:{s:16:"author_image_url";s:69:"https://cdn-images-1.medium.com/fit/c/200/200/0*ZEOBGOamcybOvRWa.jpeg";s:10:"author_url";s:30:"https://medium.com/@MGraybosch";s:11:"byline_name";N;s:12:"byline_email";N;s:10:"cross_link";s:3:"yes";s:2:"id";s:12:"a19580fd2d5e";s:21:"follower_notification";s:3:"yes";s:7:"license";s:19:"all-rights-reserved";s:14:"publication_id";s:2:"-1";s:6:"status";s:6:"public";s:3:"url";s:86:"https://medium.com/@MGraybosch/final-fantasy-xv-four-dudes-and-a-cool-car-a19580fd2d5e";}'
categories:
  - Games
tags:
  - aranea highwind
  - bromance
  - brotherhood
  - final fantasy xv
  - gladio
  - hype train
  - ignis
  - kingsglaive
  - marilith
  - multimedia
  - noctis
  - prompto
  - road trip
  - sausage fest
  - square Enix
  - uncovered
  - web series
---
Square-Enix has finally started their marketing push for Final Fantasy XV with a playable demo and a web series introducing our four heroes. There be spoilers ahead.

<!--more-->

## FFXV Brotherhood

This is some backstory for Noctis and friends, which supposedly ties into the Platinum Demo. Not sure how yet because I've only seen the first episode and the rest aren't available yet.

#### Episode 1: Before the Storm

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Hopefully we'll get to fight the marilith (six-armed snake chick) in the game. That'll be an epic shout-out to the original _Final Fantasy_.

## Pre-Release Impressions

In _Final Fantasy XV_ you play the role of Noctis, crown prince of Lucis, as he flees the conquered kingdom in the company of his friends Ignis (dude with glasses), Prompto (blonde dude), and Gladio (muscular dude). This was supposed to be a road trip before Noctis got married, but because this is _Final Fantasy_ everything's gone nuts. Lucis has been conquered by <strike>Nilfgaard</strike>Niflheim, the entire royal family is presumed dead, and Noctis is on the run at the beginning of the web series Square Enix is running on YouTube to promote FFXV: _Brotherhood_.

The setup reminds me of Dalmasca from _Final Fantasy XII_, but at least this time we don't have to suffer through ten hours with characters like Vaan and Penelo before we get to the real cast. On the downside, no bunny-eared Amazons.

I played the Platinum Demo, and while I'm not completely aboard the hype train, I'm willing to wait and see if Square Enix can redeem themselves after the clusterfuck they made of _Final Fantasy XIII_. Besides, I suspect they're going to be going heavy on the promo since they're not only doing a web series, but a full-length feature called _Kingsglaive_ featuring actors from HBO's _Tits & Dragons_.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Unless there are additional playable characters we haven't seen unveiled, the main party is gonna be a complete sausage fest. It looks like there will be some women in the supporting cast, but that hardly counts. I suspect Tumblr will become a three-way fight between feminists griping about lack of female representation in the main party, fans writing slashfic, and dudes trolling the feminist and boys' love factions.

On the other hand, this is supposedly one of the major antagonists you'll face in the game, the dragoon Aranea Highwind.<figure id="attachment_3670" style="width: 840px" class="wp-caption aligncenter">

<a href="http://www.matthewgraybosch.com/2016/04/final-fantasy-xv-four-dudes-and-a-cool-car/ffxv-aranea-highwind/" rel="attachment wp-att-3670"><img src="http://i2.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/ffxv-aranea-highwind-1024x576.jpg?fit=840%2C473" alt="FFXV: Aranea Highwind" class="size-large wp-image-3670" srcset="http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/ffxv-aranea-highwind.jpg?resize=1024%2C576 1024w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/ffxv-aranea-highwind.jpg?resize=300%2C169 300w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/ffxv-aranea-highwind.jpg?resize=768%2C432 768w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/ffxv-aranea-highwind.jpg?resize=610%2C343 610w, http://i0.wp.com/www.matthewgraybosch.com/wp-content/uploads/2016/04/ffxv-aranea-highwind.jpg?resize=1200%2C675 1200w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" data-recalc-dims="1" /></a><figcaption class="wp-caption-text">FFXV: Aranea Highwind</figcaption></figure> 

I'm not convinced the cleavage was necessary, and the armor looks better suited to BDSM than combat. Maybe I'm just getting old, but I can't help but wonder what designers are thinking when they come up with characters like this? Who's the target demographic?

We'll have to see. For now, I'll update this post as new episodes of _FFXV Brotherhood_ hit YouTube. In the meantime, you can check out _Final Fantasy XV Uncovered_ if you want to torture yourself.

The video might not have a proper thumbnail, but it's playable.

<div class="jetpack-video-wrapper">
  <span class="embed-youtube" style="text-align:center; display: block;"></span>
</div>

Skip to [5:55](http://www.youtube.com/watch?v=4AptAyPIL88&t=5m55s) to avoid the title screen.

Skip to [1:00:00](http://www.youtube.com/watch?v=4AptAyPIL88&t=1h) to avoid the Square-Enix community managers.