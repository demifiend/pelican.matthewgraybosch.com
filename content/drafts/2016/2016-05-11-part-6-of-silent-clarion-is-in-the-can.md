---
title: "Part 6 of *Silent Clarion* Is In The Can"
excerpt: "I just sent in my rewrite of the last episode of *Silent Clarion*, &quot;Rainchecks for Ragnarok&quot;. It's gonna be heavy."
header:
  image: silent-clarion-new-banner.jpg
  teaser: silent-clarion-new-banner.jpg
categories:
  - Project News
tags:
  - Naomi Bradleigh
  - Project Harker
  - Silent Clarion
  - Starbreaker
  - serial
  - Part 6
  - Rainchecks for Ragnarok
  - NA
  - new adult
  - sci-fi
  - science fiction
  - military
  - conspiracy
  - thriller
  - action
---
I just sent the rewrite for Part 6 of *Silent Clarion*, "Rainchecks for Ragnarok" to Curiosity Quills Press for editing. Assuming the editors find no major problems with it, it should be published shortly with the complete *Silent Clarion* novel to follow.

Hold onto your butts. The stakes keep getting higher as Naomi finally learns the truth about Project Harker.

{% include base_path %}
![Silent Clarion cover]({{ base_path }}/images/silent-clarion-new-cover.jpg)

If anybody cares, the word count on Part 6 is 30,760, but that includes summaries of previous episodes in the *Silent Clarion* serial, the glossary, and the acknowledgements. I suspect, however, that the full novel will weigh in at 120,000 words minimum, and that includes the glossary, etc. That makes it even longer than *Without Bloodshed*, despite being first-person narration with a smaller cast.
