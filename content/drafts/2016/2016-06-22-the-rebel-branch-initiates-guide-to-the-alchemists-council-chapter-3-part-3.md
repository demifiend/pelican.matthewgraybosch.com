---
title: "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 3"
excerpt: "Welcome back to my readers' guide to *The Alchemists' Council* by Cynthea Masson, with guest commentary by Eric Higby. This post covers pages 156-160."
header:
  image: bee-649952_1280.jpg
  teaser: bee-649952_1280.jpg
  caption: "[Source: Pixabay](https://pixabay.com/en/bee-flying-flower-insect-fly-649952/)"
coauthor: ehigby
toc: true
categories:
  - Books
tags:
  - Cynthea Masson
  - The Alchemists' Council
  - ECW Press
  - Canada
  - 2016
  - literary fantasy
  - intrigue
  - politics
  - alchemy
  - environmentalism
  - intention
  - ego
  - Buddhism
  - urban fantasy
  - portal fantasy
  - commentary
  - read-through
  - read-along
  - reader's guide
  - Eric Higby
  - Chapter 3
  - Part 3
  - Kabbalah
  - Qliphoth
  - the Left-Hand Path
---
What are the Elders of the Alchemists' Council doing while Jaden meets Draecen, the High Azoth of the Alchemists' Council? You'll see soon enough. :)

Now, I'll admit it: as a software developer I positively *loathe* meetings, and I don't think I've ever been to a meeting of more than three people that couldn't have been replaced with an email. It's probably a mercy that Obeche hasn't discovered [PowerPoint](https://www.edwardtufte.com/tufte/powerpoint). He'd probably love it.

**THE ALCHEMISTS' COUNCIL FORBIDS YOU TO READ ANY FURTHER.**

**THE REBEL BRANCH ENCOURAGES YOU TO CONTINUE, BUT THE CHOICE IS YOURS...**

### Cedar and Obeche at Cross Purposes

We don't get to see Draecen make his case to Jaden just yet. Instead, because Kalina is taking advantage of the Elder Council being in yet [another meeting](http://www.theguardian.com/news/oliver-burkeman-s-blog/2014/may/01/meetings-soul-sucking-waste-time-you-thought), we're going to cut to the meeting and look over Cedar's shoulder. Here's what we learn:

1. Obeche is paranoid, as I mentioned before, and has the same bee in his bonnet about the Rebel Branch that Joe McCarthy had about Communism, and a similar track record of seeing enemy agents where none actually existed.
2. The Elder Council meeting was mostly a waste of time, with the Elders only unanimously agreeing to one proposal: members of the Senior Magistrate would be charged with monitoring the cliff face 24/7. Normally the Readers would get stuck with this detail, but they're currently busy identifying the next Initiate to fill the opening that will result from the conjunction of Kalina and Tesu.
3. Obeche isn't keen on this conjunction, either, but Azoth Ravenea has no time for his objections. Not that this stops Obeche. He insists that the timing is fishy, and that both Tesu and Kalina should be monitored. Ailanthus orders Obeche to do it himself, which ought to fix *his* little red wagon.

{% picture boy-1242260_1280.jpg alt="Boy and his wagon. genalouise/pixabay" %}

Cedar, surprisingly enough given what we've seen of her relationship with Obeche, offers to help. I doubt she offered for his sake, though; Cedar herself says that, "Conjunction is too great a sacrament to place at risk," (p. 158).

Afterward, Cedar asks Sadira who *she* suspects Jaden saw at the cliff face. Instead, Sadira demands that Cedar tell her the truth about the strangers Jaden saw. Sadira thinks they're Cedar's source for the Sephrim. However, as Cedar herself points out, it would be extraordinarily stupid of Cedar to jeopardize their supply and reputations by inviting her source for an illicit drug into Council dimension.

Sadira seems to calm down, but a careful reader will notice on page 160 that Cynthea Masson appears to change viewpoint from third person close with Cedar as the viewpoint character to third person omniscient in order to get into Sadira's head and show that while Cedar thinks the matter of where she gets the Sephrim is closed, Sadira still harbors doubts.

Then again, I might have been misreading the book the whole time. Masson might always have been using third person omniscient.

Stay tuned for "The Rebel Branch Initiate's Guide to the Alchemists' Council: Chapter 3, Part 4". If you want more of the Rebel Branch Initiates' Guide, [check out the main page](/commentaries/the-rebel-branch-initiates-guide-to-the-alchemists-council/).
