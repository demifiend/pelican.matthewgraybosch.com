---
title: "I Woke Up This Mornin', and I Got Myself a Beer"
excerpt: "Just like in that old Doors song, &quot;Roadhouse Blues&quot;. Hell of a way to start a vacation, ain't it?"
categories:
    - Personal
    - Project News
tags:
    - beer
    - breakfast
    - eggs
    - sausages
    - The Doors
    - Roadhouse Blues
    - Haken
    - Affinity
    - stereo
    - weight loss
    - portion control
    - missing my wife
    - solitude
    - vacation
    - two weeks off
    - driving
    - DUI
    - Deloitte
    - DEM
    - COBOL
    - .NET
    - WCF
header:
    image:  beer-eggs-sausages-bachelor-breakfast.jpg
    teaser: beer-eggs-sausages-bachelor-breakfast.jpg
---
Today's the first day of the first honest-to-goddess vacation I've had since I rage-quit my job at Quality Data Service of Waterbury, CT back in 2009. Don't worry; I didn't rage-quit my current day job. I still work for Deloitte, but I rolled off the Delaware Eligibility Modernization (DEM) project yesterday.

What was DEM about? Well, it was a state government project and I probably shouldn't tell you too much. Let's just say it involved reverse-engineering a shitload of old COBOL and implementing the functionality using Microsoft .NET and Windows Communications Foundation (WCF). It was fairly challenging work; I was the senior developer on the team, and wasn't only responsible for my own fuckups.

### Time for a Long-Overdue Break

But that's over now. I'm between projects at Deloitte, and "should be" networking to line up a new one. Well, screw that. I had 80 hours of paid time off held over from last year that I would lose at the end of 2016 if I didn't use it, so I'm using it now.

I'm off my day job for the next two weeks, and my wife is in Australia again because her father died (RIP Alfred Gatt). I miss Catherine, but I always miss Catherine when she isn't around. Sometimes I suspect I'm more dependent on her presence than is healthy, but fuck it. I miss her.

But missing Catherine isn't going to stop me from taking advantage of my time away from my day job. I've got *plans*.

First, I'm going to get off sugars and starches. I've got an incorrigible sweet tooth, and I easily eat entirely too much bread, rice, and pasta, and it hasn't done me any favors. I'm not going to tell you how much I weigh, or show you any current photos, so let's just say I currently wear size 50 jeans. I *can* stuff myself into a size 48, but I'd like to fit comfortably into a 48 within two weeks.

That means it's time I got serious about controlling how many calories I take in during the day. It means I need to make some changes in what I eat -- *and* what I drink.

And, yes, I did indeed wake up this morning and get myself a beer. Just like in "Roadhouse Blues" by the Doors.

{% youtube BgQg3J7xU1k %}

### Beer vs Soda

I've been thinking for a while about exchanging my soda habit for a beer habit. Soda's relatively cheap; I can buy a 1L bottle of various Pepsi or Coca-Cola brands at my local Giant supermarket for a dollar, but a six pack of good beer ([Yuengling](https://www.yuengling.com/), [Appalachian Brewing Company](http://abcbrew.com/), or [Victory Brewing Co.](http://victorybeer.com) costs at least $5.

While beer isn't exactly a low-calorie or low-cab beverage, information I've found online suggests that [a pint of beer (16-19.2oz depending on whether we're talking US pint or Imperial pint) contains 10-20 grams of carbohydrates](http://www.diabetes.co.uk/alcohol-and-blood-sugar.html). Because of the fermentation process, most of that carbohydrate is *not* simple sugar, and it definitely isn't fructose or sucrose. A 12oz serving of soda (same size as the bottle of beer I had for breakfast) has 30-45 grams of sugar, and it's all high-fructose corn syrup.

It gets worse, though. I can drink *one* 12oz. bottle of beer and be satisfied. After all, I've got to be able to drive, and after I've had a beer it takes at least an hour before I can safely do so. Don't want to hurt other people because I was driving drunk, right?

But try crunching the numbers. The serving size for most sodas is 12oz, for which you get 46g of sugar. A whole 1L bottle of soda is 3.5 12oz servings, or 42oz. That's 105-157.5g of sugar. No wonder I'm a fucking lardass.

{% picture table-flip-fuck-that-shit.jpg alt="Fuck that shit." %}

Now, even if I drank three beers a day, I'd probably be getting 60g of carbohydrates at most. I'm not sure how many grams were in the bottle of [Victory Summer Love](http://www.victorybeer.com/beers/summerlove/) I had with breakfast, but at [160 calories](http://www.victorybeer.com/2011/06/questions1/) I'm guessing no more than 14, which is also how many are in a Yuengling Black & Tan (their Lager is only 12g at 140 calories).

Could I drink more than three beers a day? Maybe while I'm on vacation, but there ain't a chance in hell I'm drinking that much once I go back to work. Catherine would have kittens at the mere notion of me lubricating myself with a beer before work, and my bosses would probably shit themselves if I had a beer on my lunch break, and three beers a night after work?

Probably not such a good idea. After all, we've known for years that alcohol provokes desire, but detracts from the performance. Just ask the porter.

{% youtube Yr4jULsh9Lg %}

### Man Does Not Live on Beer Alone

Naturally, a maximum of three beers a day is hardly enough to sustain a man, though the Devil knows I've got plenty of fat to burn off. And I'd get drunker faster if I didn't eat something while drinking my brewskis.

The question is: what should I eat? That's a rhetorical question, incidentally. It's my experience that when you're fat, everybody thinks they know better than you just because *they* aren't fat *yet*.

But let's get back to food. Gotta fuel my body somehow, and the fuel I choose has to meet a few basic requirements:

1. I have to enjoy eating it.
2. I have to be able to prepare extra and pack some away to take to work.
3. It has to come in a form whose serving size and calorie count I can easily quantify.
4. It can't leave me hungry again half an hour later.
5. I must be able to cook it at home.

So, what kind of food meets these requirements? Well, Evo Terra and his physician Terry Simpson settled on sausages, and wrote about their experiment in [*The Beer Diet: A Brew Story*](http://brewdiet.com/). I happen to like sausages that aren't attached to men, but there's no reason I have to eat nothing but various kinds of sausage. I can also have steak, or chicken cutlets, or pork chops, or do a roast in my crock pot. I can have turkey, never mind that it isn't Thanksgiving yet. Or maybe lamb. Or even fish, if I can find a decent recipe.

But I definitely need to keep track of what I eat and how much of it I eat if I want to get into those size 48 jeans in two weeks. That's where you come in. [Every day I'm going to post what I ate and when](/diet/), to hold myself publicly accountable.

Let's start with breakfast:

|Food|Quantity|Calories|Total|
|----------------------------|
|Victory Summer Love 12oz.|1|160|160|
|Scrambled Eggs|2|80|160|
|Wegmans Pork Breakfast Sausage|4|70|280|
|Meal Total|||600|

600 calories for breakfast looks pretty reasonable, but I actually had two servings of the sausage. I'll do better tomorrow. If I can keep each meal (including beer) within 500-600 calories, that's a max intake of 1800 calories per day. I'll probably plateau eventually, but this should at least get my weight loss started.

The hard part will be persuading Catherine that I'm not going to kill myself doing this, but these blog posts might help. If she sees I've been doing it while she was gone, and am still fine and happy to see her when she returns (and up for reunion sex if she wants it), it'll probably go a long way toward persuading her.

### Never Mind the Diet. What About Starbreaker?

If you think I'm going to spend my whole vacation obsessing about my diet, you've got another thing coming. You see, I'm just waiting for my editor to sign off and say that part 6 of [*Silent Clarion*](/books/starbreaker/silent-clarion/), "Rainchecks for Ragnarok", is finished and ready for publication.

And if you've been holding off on [*Silent Clarion*](/books/starbreaker/silent-clarion/) because you want a full-length book the way [*Without Bloodshed*](/books/starbreaker/without-bloodshed/) was, don't worry. That's *next* on the agenda. I'll be working on that this week.

Also, since [Curiosity Quills Press](http://curiosityquills.com) is making noises about doing hardcover editions, I mean to clean up [*Without Bloodshed*](/books/starbreaker/without-bloodshed/) for its hardcover debut. Here are some of the changes I plan to make:

1. Better separation of texting dialogue from interior dialogue
2. Minor retcons to account for *Silent Clarion*
3. Changes to Naomi Bradleigh's characterization to give her more agency, and account for the difference in her personality between *Without Bloodshed* and *Silent Clarion*

I'm also going to lobby for new covers/branding to make it clear to potential readers that both *Without Bloodshed* and *Silent Clarion* are part of a series. If you have the first book in paperback, hold on to it. It might become a collector's item someday. :cat:

Also, you might have noticed that you can read the original rough draft of *Silent Clarion* on this site, [starting here]({% post_url 2014-07-23-silent-clarion-track-01-nemesea-no-more %}). That's no mistake, and I'm thinking of doing the same for *Without Bloodshed* and the original *Starbreaker* novel, the draft of which I finished in 2009.

And I'll do the same with the draft of *Blackened Phoenix* and subsequent Starbreaker novels as I write them. You see, I want you to come to my website. I want you to become a *frequent* visitor. I'd rather you read my stuff for free here than via some pirate site.

### Time for Materialistic Self-Indulgence

In addition to changing my diet and putting more time into getting the next Starbreaker book out before a mostly indifferent public, I've also gotten myself some treats.

{% picture evangelion-haken.jpg alt="Anime and Progressive Rock. Two nerdy tastes even nerdier together." %}

I've been wanting to see the *Rebuild of Evangelion* movies for a while now, and I figure they'd be interesting to share with Catherine when she comes home. And I got into Haken while listening to one of their earlier albums, *The Mountain* on Spotify.

Of course, *Affinity* is also on Spotify, so why buy the CD? To be honest: I *miss* having a stereo and playing a CD while I write. Sure, I've got music on my computer and it's nice and convenient, but the ability to ALT+TAB away from my writing and fuck with the music player app is an unnecessary distraction. I think it would be easier to focus at home if I used a stereo to play a CD, and keep my ass *in the chair* until the album was finished.

And I've got a whole [list](https://amzn.com/w/3IQCW14GZM2IJ) of albums I want to own on disc. Of course, I *also* have a couple of boxes of albums I haven't played in years; I left them in the box I packed them in back in 2009 because I didn't have anywhere to put them. I'll be changing that soon, though. And when I do I'll have a place to put my new stereo, too.

But in the meantime, it's time to rip this album. I might want to listen to CDs at home, but I'll be damned if I'll carry CDs around outside my apartment. :)

{% picture haken-affinity-open-digipak.jpg alt="Opening Haken's AFFINITY album." %}

So, what are *you* up to this summer? Tell me in the comments, or recommend albums based on [my to-buy list](https://amzn.com/w/3IQCW14GZM2IJ), or just say hello!
