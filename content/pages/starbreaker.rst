About Starbreaker
#################

:summary: Want to know more about the Starbreaker saga by Matthew Graybosch? Start here.
:url: starbreaker/
:save_as: starbreaker/index.html


I began writing the Starbreaker saga in 1996, seeking to write a literary rock opera paying tribute to classic heavy metal and progressive rock. The sensible thing to do would have been get a few other people together and start a tribute band, but I already knew I wasn't enough of a musician to do the job.

The epic first took the form of a previously unpublished novel in 2009. After years of work, I re-imagined the Starbreaker saga as a quartet of novels, and released the first of them in 2013. After getting side-tracked with a prequel featuring a major supporting character as its protagonist -- and the responsibilities of a married man with a day job -- I'm ready to continue the saga.

*Starbreaker* takes place in a future similar ours where... 

* First contact happened over 10,000 years ago.
* Aliens have lived among us throughout the history of civilization.
* Humanity has begun to colonize the entire solar system while still healing the scars of a global collapse they remember as *Nationfall*.
* Benevolent artificial intelligence works alongside humans.
* An elite corps of tribunes investigates and punishes abuses of power wherever they occur under the aegis of the Phoenix Society.
* All-too-human androids rock out with swashbuckling sopranos.
* The gods came from outer space.
* An ancient evil is bound beneath the ice of Antarctica.
* The dark lord wears white, already rules the world, and is trying to save it.

Welcome to *Starbreaker*, where all-too-human androids and swashbuckling soprano catgirls expose corruption and fight demons from outer space on a near-future alternate Earth. Humanity has risen from global collapse to an interplanetary Renaissance in less than a century. Meanwhile a clandestine war between demons threatens to go hot once more. Morgan, Naomi, and their friends are caught in the middle and they are *not* pleased.

If you're interested, I'm releasing the rough cuts online. You can start with the short stories, or go right to the novels.

Shorter Stories
===============

Start here if you just want a taste.

* `"The Milgram Battery"`__
* `"Limited Liability"`__
* `"The Holiday Rush"`__
* `"Tattoo Vampire"`__
* `"Steadfast"`__

.. __: /starbreaker/stories/milgram-battery/
.. __: /starbreaker/stories/limited-liability/
.. __: /starbreaker/stories/holiday-rush/
.. __: /starbreaker/stories/tattoo-vampire/
.. __: /starbreaker/stories/steadfast/

Novels
======

The stories listed above are only a taste of *Starbreaker*. If you want the full story, you've got a *lot* of reading to do.

* `Without Bloodshed - Part One`__
* `Silent Clarion - a Starbreaker Novel`__
* `Dissident Aggressor - Part Two`__
* `Shattered Guardian - Part Three`__

.. __: /starbreaker/novels/without-bloodshed/
.. __: /starbreaker/novels/silent-clarion/
.. __: /starbreaker/novels/dissident-aggressor/
.. __: /starbreaker/novels/shattered-guardian/

.. image:: {filename}/images/starbreakercollage.jpg
	:width: 40em
	:alt: Some of the cast of Starbreaker. Art by Harvey Bunda commissioned by Matthew Graybosch
	:align: left

If you want to dig down deep and see the roots of *Starbreaker*, you could also try reading `the original 2009 draft`__. It's for hardcore fans only.

.. __: /starbreaker/novels/original-starbreaker/
