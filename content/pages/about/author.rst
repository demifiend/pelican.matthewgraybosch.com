Author Bio
##########

:summary: This is the author bio for Matthew Graybosch that goes on all of my books. I also give it to bloggers and journalists.
:url: about/author/
:save_as: about/author/index.html


According to official records maintained by the state of New York,
Matthew was born somewhere on Long Island in 1978.

Urban legends suggest he might be Rosemary’s Baby or the result of
top-secret DOD attempts to continue Nazi experiments combining human
technology and black magic. The most outlandish tale suggests that he
sprang fully grown from his father’s forehead with a sledgehammer in one
hand and the second edition of *The C Programming Language* in the other
— and has been giving the poor man headaches ever since.

The truth is more prosaic. Matthew is an author from New York who lives
with his wife and cats in central Pennsylvania. He is also an avid
reader, a long-haired metalhead, and an unrepentant nerd with delusions
of erudition.

*Without Bloodshed* (2013) is his first published novel, followed by
*Silent Clarion* (2016). He has also written short stories like "The
Milgram Battery" and "Limited Liability". He is allegedly working on the
next Starbreaker novel, *Shattered Guardian*, but sources within the NSA
suggest he’s doing nothing of the sort.

His day job is software development, and we're not sure how he remains
sane. We could ask, but we suspect he’d say, "I'm not sane. I’m
high-functioning."

.. image:: {filename}/images/author-nyc.jpg
    :alt: Matthew Graybosch atop Rockefeller Center in NYC. Photo by Catherine Gatt
    :align: center
