How to Contact Me
#################

:summary: If you need to reach me, email is your best bet if you're not on Mastodon or Google+.
:url: about/contact/
:save_as: about/contact/index.html


You might need to get in touch with me, and email is still the best way to do so. `Drop me a line <mailto:mgraybosch@fastmail.com>`_.

Using Social Media
==================

You won't find me on Twitter or Facebook. If you like social media, here are your options.

- I often post on `Mastodon <https://octodon.social/@starbreaker>`_. Create an account and tag me in public.
- You can still `find me on Google+ <https://plus.google.com/+MatthewGraybosch>`_, and tag me in public.

The following aren't really social media, but more for techies.

- If you're a hipster like me and use `twtxt <https://github.com/buckket/twtxt>`_ you can follow me as `@demifiend </twtxt.txt>`_.
- I use `GitHub <https://github.com/matthewgraybosch>`_ for various projects.

Note for Tech Recruiters
========================

Please `read my resume </about/resume/>`_ before contacting me. 
It'll save everybody involved some time and effort.
