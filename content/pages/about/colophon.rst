Colophon
########

:summary: Technical information about this website and its design
:url: about/colophon/
:save_as: about/colophon/index.html
:modified: 2018-04-19 00:33


**matthewgraybosch.com** is hosted by Dreamhost_, and has been in one form or another for almost ten years. I run this website myself, and build it using free and open source software such as:

- Pelican_
- Vim_
- OpenBSD_

You can view the code I use on GitHub, and use my Oedipus_ Pelican Starter for your own projects. It's solid; I'm eating my own dog food here.

About the Blog
==============

The blog_ is called "A Day Job and a Dream" because of a marketing slogan that the New York Lottery had used when I first started writing. They used to say, "All you need is a dollar and a dream," to have a shot at winning the lottery. 

Since the odds of making a living as an author are little better than those of hitting the lottery, I had taken to saying about being a writer: "All you need is a *day job* and a dream."

License Information
===================

Any code I create is available under the terms of the `GNU General Public License, v3 <http://gplv3.fsf.org/>`_.

Any code I use that other people wrote is governed by the terms of the licenses *they* chose.

Any content I create is available under the terms of the `Creative Commons BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>`_ license.

Privacy Information
===================

The only information I would *personally* collect from you is your name and email address, and that's *only* if my subscription_ page had a form that would let you subscribe to my newsletter for updates -- which is currently doesn't.

I don't sell the information I collect to third parties, or share it with them, but if the Feds show up with a warrant there isn't a hell of a lot I can do. However, the Feds haven't demanded my mailing list *yet*.

With that said, you should still be aware that pages on my site that contain embedded content from other websites -- such as YouTube and Spotify -- may also contain tracking code from those websites. They may also attempt to place cookies on your device.

The  Lynx_ text-mode browser is the safest way to visit this site. Lynx is a good kitty and doesn't implement JavaScript. Don't worry; you can read this site with Lynx. I know this because I tested it.

Warrant Canary
==============

I have yet to receive a subpoena, warrant, or court order from the United States government.

.. _Pelican: https://getpelican.com/
.. _Vim: https://vim.org/
.. _OpenBSD: https://openbsd.org/
.. _Oedipus: https://github.com/matthewgraybosch/oedipus-pelican-starter
.. _Dreamhost: https://www.dreamhost.com/
.. _blog: /blog/
.. _subscription: /subscribe/
.. _Lynx: http://lynx.invisible-island.net/
