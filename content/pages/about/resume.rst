Resume
#######################################

:summary: This is Matthew Graybosch's resume, which he trots out when looking for a new day job.
:status: hidden
:slug: resume
:url: about/resume/
:save_as: about/resume/index.html

email: `mgraybosch at fastmail dot com`__ (serious enquiries only, please)

.. __: mailto:mgraybosch@fastmail.com

I'm looking for short and medium term engagements that push my existing skills to the limit and afford opportunities to expand upon my knowledge. I'm not interested in being the smartest person in the room. I would rather learn from them as their strong right hand.

Skills
======

My skill with Microsoft-based technologies come with years of professional experience, and my familiarity with free/open source tech comes from years of home use and running my own website.

**Programming Languages**
	C#, SQL, VB.NET, PowerShell, bash, ksh, Ruby, Python, JavaScript, PHP, Apex, Java, C

**Frameworks**:
	.NET, .NET Core, WCF, LINQ

**Databases**
	Microsoft SQL Server, Entity Framework, SQL Server Integration Services, MySQL

**Operating Systems**
	GNU/Linux, OpenBSD, Android, OSX, Windows

**Virtualization**
	VirtualBox, Docker

**Software Configuration Management**
	Git, Subversion, Team Foundation Server
	
**Cloud Platforms**
	Salesforce, AWS, Digital Ocean

**Continuous Integration**
	Travis CI, Team Foundation Services

**Web Backend**
	Apache, NGinx, WordPress, NodeJS

**Web Frontend**
	HTML5, CSS3, Bootstrap 3, Bootstrap 4, Pelican, JQuery, Jekyll, Angular, React, Jinja 2, Liquid

Experience
==========

Note: Everything I post online, whether on this website or elsewhere on the internet, *is strictly my own opinion* and not to be taken as representative of the views of any current or previous employer.

matthewgraybosch.com
--------------------

**Owner/Operator**, *January 2010 to present*

I built this website using `Pelican <https://getpelican.com>`_, Jinja templates, HTML5, CSS3, and Python, and manage its hosting with help from `Dreamhost <https://dreamhost.com>`_.

Deloitte Consulting, LLP
------------------------

**Solution Specialist**, *June 2015 to present*

Delaware Eligibility Modernization: Maintenance & Operations
............................................................

Using .NET 4.x, C#, SQL, WCF, PowerShell, $Universe, and SSIS, I...

- assisted with refactoring and optimization of batch processes
- provided analysis and solution proposals for technical problems arising over the system's lifetime
- assisted with new batch process development for LIHEAP project

Michigan Department of State AHS Replacement
............................................

Using Salesforce Custom Cloud, VisualForce, Apex, Bootstrap3, and JQuery, I...

- assisted project lead by taking on development tasks from his personal workload
- improved ADA Section 508/WCAG 2.0 Level AA compliance on public-facing web pages
- refactored substantial portions of the application to improve code quality and ensure compliance with best practices

Commonwealth of Pennsylvania Hiram G. Andrews Proof-of-Concept
..............................................................

Using Salesforce Custom Cloud, VisualForce, and Apex, I...

- performed database and UI design for a proposed migration from Lotus Notes
- implemented a prototype suitable for presentation to the client

Delaware Eligibility Modernization, Release 3, Batch Processing
...............................................................

Using .NET 4.x, C#, SQL, SSIS, COBOL, and JCL I...

- continued in the role I had occupied as contractor with TEKsystems

TEKsystems
----------

**Contractor**, *November 2011 to June 2015*

Deloitte: Delaware Eligibility Modernization, Release 3, Batch Processing
.........................................................................

Using .NET 4.x, C#, SQL, SSIS, COBOL, and JCL I...

- led the effort to reverse-engineer legacy batch programs implemented in COBOL and JCL for an IBM mainframe using DB2
- wrote batch specifications
- implemented several key batches: mass change process, individual alerts, and monthly TANF reports

Deloitte: Commonwealth of PA Workers' Compensation Automation and Integration System
....................................................................................

Using HTML, CSS, .NET 4.x, ASP.NET, VB.NET, SQL, and JavaScript, I...

- assisted in the development of the WCAIS application across two releases
- developed functionality for appeals, disputes, judicial management, and searches
- developed common components
- assisted architecture team by debugging custom controls and low-level functionality

Computer Aid, Inc.
------------------

**Developer**, *September 2010 to October 2011*

West Palm Beach County, Florida: Healthy Beginnings Data Systems
................................................................

Using HTML, CSS, .NET 4, ASP.NET, C#, LINQ, Entity Framework 5, and SQL,
I...

- assisted in implementation of the project throughout the software development lifecycle
- assisted lead developer in debugging custom model/view/view-model architecture

Conduit Internet Technologies
-----------------------------

**Developer**, *August 2009 to July 2010*

Using PHP, JavaScript, Microsoft SQL Server, .NET 3.5, VB.NET, C#, SSIS, MySQL, Adobe Illustrator, HTML, and CSS, I...

- developed a data format for online parts catalogs
- assisted in ETL (extract, transform, load) operations on parts catalog data for heavy equipment manufacturers
- developed automation techniques to convert assembly drawings and diagrams for online display
- assisted in debugging data conversion packages for clients

(Note: this company no longer exists. After my departure it was acquired by Servigistics, which was in turn acquired by PTC.)

Quality Data Service, Inc.
--------------------------

**Developer**, *March 2000 to July 2009*

Using Visual Basic 6, Microsoft SQL Server, .NET 1.0, and C# I...

- acted as sole developer and maintainer for a Windows desktop application marketed toward municipal tax assessors
- provided end user support as required
- produced rapid adjustments in response to changing statutory requirements
- provided custom functionality for individual clients
- ensured compatibility with the company's other products
- ensured compatibility with competing assessment tools and computer-aided mass assessment applications

