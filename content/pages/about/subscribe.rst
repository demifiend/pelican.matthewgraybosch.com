Subscription Info
#################

:summary: Information on how to subscribe and get updates.
:url: about/subscribe/
:save_as: about/subscribe/index.html
:modified: 2018-04-19 00:37

If you want to follow me and get updates, I provide `XML feeds for
syndication <www.matthewgraybosch.com/feeds/>`_.

Atom Feeds
==========

You can subscribe to my site's Atom feeds with any modern feed reader to get
recent posts.  Atom feeds contain both the full text of a post and a
summary, so you can read at your convenience.

* `Recent Posts (Atom) <https://www.matthewgraybosch.com/feeds/atom.xml>`_
* `All Posts (Atom) <https://www.matthewgraybosch.com/feeds/all.atom.xml>`_

RSS Feeds
=========

RSS is an older XML syndication tech, which I provide for readers whose
preferred feed-reading software can't handle Atom.  You'll have to visit for
the full story.

* `Recent Posts (RSS) <https://www.matthewgraybosch.com/feeds/rss.xml>`_
* `All Posts (RSS) <https://www.matthewgraybosch.com/feeds/all.rss.xml>`_

ActivityStream
==============

It isn't really supported yet, but I also provide an ActivityStream `JSON
feed`__

.. __: https://www.matthewgraybosch.com/feeds/ap/outbox.json