Dissident Aggressor
###################

:author: Matthew Graybosch
:summary: Having learned that he had spent his life betraying his own ideals by serving as the Phoenix Society's avenging sword, Morgan sees no option but to investigate the organization he once believed beyond reproach. The truth won't set him free; it will only bind him to the purpose for which he was originally designed.
:url: starbreaker/novels/dissident-aggressor/
:save_as: starbreaker/novels/dissident-aggressor/index.html
:og_image: /images/starbreakercollage.jpg
:modified: 2018-06-06 00:07


Part Two of *Starbreaker*
    Having learned that he had spent his life betraying his own ideals by
    serving as the Phoenix Society's avenging sword, Morgan sees no option
    but to investigate the organization he once believed beyond reproach. 
    The truth won't set him free; it will only bind him to the purpose for
    which he was originally designed.

This draft is available under a `Creative Commons license`__.

.. __: https://creativecommons.org/licenses/by-sa/4.0/

.. contents:: **Table of Contents**
    :depth: 2
    :backlinks: top

Inspiration
===========

The title comes from a classic Judas Priest anthem: "`Dissident
Aggressor`__", *Sin After Sin* (1977)

.. __: https://www.youtube.com/watch?v=4_sSV_KqU4o

New Characters
==============

I'm adding some new characters to the mix because the cast wasn't big enough
last time around. If you thought the cast of *Without Bloodshed* was too
big, I've got two words for you: "cry more".

Josefine Malmgren
-----------------

A scientist working for the AsgarTech Corporation, Josefine Malmgren was
involved in the new Asura Emulator Project, publicly known as Project Aesir,
and designed the operating system for 200 Series asura emulators like
Polaris.

Polaris
-------

A 200 Series asura emulator who knows that they are not human, and feels no
real need to pretend to humanity.  They do not stick to a specific gender,
shape, or coloration for long.  A foil of sorts for Morgan Stormrider, who
is heavily invested in his human identity.

Olivia Tate
-----------

Olivia is a failed Adversary candidate, washed out of ACS after training,
facing the Milgram Battery, and displaying a high M-factor indicating a
highly submissive attitude toward authority.  She joined the Congregation of
the Repentant in Christ seeing Abram Mellech denounce the Phoenix Society in
Boston.

As one of the Repentant, Olivia is responsible for creating and operating
the Mastema worm, which exposed Adversaries' violent methods and abuses of
power to the general public.

Mastema
-------

An artificial intelligence trained by Olivia Tate and her allies inside the
Congregation of the Repentant in Christ to crack Phoenix Society records and
expose wrongdoing by Adversaries to public view.

Plot
=======

Think I'll actually manage to stick to any of this? Let's see!

Chapter 1: Demon Speeding
-------------------------

On their way back to Morgan's home in Manhattan, Morgan and Naomi are
attacked by a flash mob calling them fascists and demanding they stand trial
for aiding the Phoenix Society's tyranny.  Trapped and unable to get away by
normal means, Morgan resorts to demon speeding to get him and Naomi away
from their attackers.

Chapter 2
---------

Once home and assured of their safety by the household daemon, Astarte,
Morgan and Naomi contact Claire and ask her to see if there's been anything
on the network that would explain the flash mob that sprang up around them. 
Claire is already on the case, and warns them that there's a new software
available for implants called "Mastema" that promises to show the user other
people's dirty little secrets.

Chapter 3: Leper Messiah
------------------------

While Sid Schneider is out with his family in Central Park, he hears Abram
Mellech ascend a podium and begin preaching.  Though it sounds innocuous at
first as Mellech talks up the virtues of the Congregation of the Repentant
in Christ, people heading to hear Mellech speak start looking askance at Sid
and his family as they try to get out of Central Park.  Seeing this, Sid
tells his wife to take the children and go before sending Morgan a call for
help and turning back toward Mellech.

Chapter 4: Afraid to Shoot Strangers
------------------------------------

Olivia Tate looks on as Sid Schneider approaches the podium while wearing
his Adversary's pins.  Though Mastema hasn't shown any dirt on him, the
increasingly restive crowd recognizes him and is close to erupting into
violence.  She wants to tell Mellech and ask him to turn his rhetoric back
toward the Gospel, but she can't get to him in time and the fun begins.

Chapter 5
---------

Josefine Malmgren needs Polaris' help to get through the crowd of protesters
gathered outside the AsgarTech Building.  The protesters demand that
AsgarTech come clean about its involvement with the Phoenix Society, which
prompts Josefine to ask her patron and boss Isaac Magnin some questions. 
Naturally, Magnin isn't forthcoming.

Chapter 6
---------

Munakata Tetsuo has taken to helping Sarah Kohlrynn with her rehabilitation
because it's ultimately his fault that Alexander Liebenthal shot Sarah
instead of Naomi Bradleigh during the events of *Without Bloodshed*.  While
bringing her back to Claire's place from physical therapy, they run into
Vincent Rubicante, who had challenged Munakata to a duel.  Tetsuo asks
Vincent for a postponement to keep Sarah from getting involved, but Vincent
refuses and says she'll do as a second.

Chapter 7
---------

Annelise Copeland finishes with the last of her clients in her Brooklyn
custom fashion boutique.  She's happy with her new life, and the memories of
her previous life are fading.  But after she's closed up her shop and was
about to head upstairs for the night, somebody throws a brick through her
shop window.  Attached is a note accusing her of being an "Adversary's
whore".

Chapter 8
---------

Olivia Tate is having second thoughts about Mastema based on what she saw at
Central Park.  She had thought that capitalizing on latent anti-Phoenix
Society sentiment and provoking public protests by exposing violent
enforcement methods and abuses of power by Adversaries using Mastema woud
help curb violence, but instead people are mobbing Adversaries--and some
Adversaries are fighting back.  She takes her concerns to Abram Mellech, who
trots out the "through a mirror darkly" bullshit.

Chapter 9: Vulgar Display of Power
----------------------------------

Saul Rosenbaum finds Morgan, Naomi, and Sid sharing a NYPD holding cell and
wishes he hadn't volunteered to come bail them out so they could be briefed
on Mastema and enlisted into the search for its origin.  He makes the
mistake of asking *why* they're in a holding cell, and to the chagrin of
everybody present Morgan explains that it was either surrender or kill a
whole bunch of cops and militia volunteers, because blocking a fusillade of
gunfire in midair wasn't enough to intimidate people any longer.

Chapter 10
----------



Dedication
==========

For Catherine, purr usual.  It certainly took me long enough to get this one
going, didn't it?

Disclaimer
==========

The following is a work of fiction and contains content that may be
offensive, triggering, or inappropriate for certain readers.  Any views or
opinions expressed by the characters in this novel are strictly their own
and do not necessarily reflect those of the author or the publisher.

Any resemblance or similarities between the characters depicted within to
living or dead persons in this world or any parallel world within the known
multiverse are either a coincidence; an allusion to real, alternate,
invented, or secret history; or a parody.

The stunts in this work were performed by trained professionals; attempting
them at home can result in property damage, civil or criminal liability,
personal injury, and premature death.

If you find any allegory or applicability in the following novel, please
consult a qualified professional for psychiatric evaluation and treatment.
