Silent Clarion
##############

:author: Matthew Graybosch
:summary: They say curiosity killed the catgirl, but Naomi Bradleigh won't let her post-breakup working vacation from hell end without a fight.
:url: starbreaker/novels/silent-clarion/
:save_as: starbreaker/novels/silent-clarion/index.html
:modified: 2018-04-19 00:48
:og_image: /images/starbreakercollage.jpg


This is a prequel of sorts to `Without Bloodshed </starbreaker/novels/without-bloodshed/>`_ that I wrote after that novel. Because `Curiosity Quills Press <https://curiosityquills.com/books/silent-clarion/>`_ currently holds publication rights, I'm only providing part 1, "The Geographic Cure", here.

If you like it, please buy the rest.

.. contents:: **Table of Contents**
	:depth: 4
	:backlinks: top

Disclaimer
==========

The following is a work of fiction and contains content that may be
offensive, triggering, or inappropriate for certain readers. Any views
or opinions expressed by the characters in this novel are strictly their
own and do not necessarily reflect those of the author or the publisher.

Any resemblance or similarities between the characters depicted within
to living or dead persons in this world or any parallel world within the
known multiverse are either a coincidence; an allusion to real,
alternate, invented, or secret history; or a parody. Likewise for places
and events.

The stunts in this work were performed by trained professionals;
attempting them at home can result in property damage, civil or criminal
liability, personal injury, and premature death. Do not attempt them in
real life.

If you find any allegory or applicability in this text, please consult a
qualified professional for psychiatric evaluation and treatment.

Author's Note
=============

This novel contains not only spoken dialogue but dialogue transmitted
over text messaging. To distinguish the two, I use French quotation
marks for the latter instead of standard quotation marks, so that a text
message will look like this: «This is text dialogue.»

*Silent Clarion* previously appeared as a web serial hosted by
`Curiosity Quills Press <http://curiosityquills.com>`__. The novel also
appeared as a Kindle Serial under the following titles:

-  *Silent Clarion*, Episode 1: "The Geographic Cure"
-  *Silent Clarion*, Episode 2: "Always the Quiet Ones"
-  *Silent Clarion*, Episode 3: "Life is Short and Love is Over"
-  *Silent Clarion*, Episode 4: "Death in a Northern Town"
-  *Silent Clarion*, Episode 5: "Hard Places and Other Rocks"
-  *Silent Clarion*, Episode 6: "Rainchecks for Ragnarok"

Dedication
==========

For Catherine, purr usual. Thanks for hitting me upside the head every
time I don't quite get the character right.

Part I: The Geographic Cure
===========================

    Many of my colleagues insist that moving after a significant life
    change, or even taking a vacation, is just a "geographic cure." They
    think it's an attempt on the patient's part at fleeing trauma. Their
    wariness is understandable, given that many of their patients came
    to them after their problems caught up with them.

    Despite the experience of my fellow psychotherapists, I disagree. I
    think that *consciously and intentionally* seeking physical distance
    from an event can help a person regain perspective on challenging
    emotional experiences. s 
    
    --*From a Safer Distance* by Dr. Nikki Hooks, MD, Ph.D

Track 01: Nemesea: "No More"
----------------------------

London can be a cruel city, and my duties as an Adversary often demanded
I face it at its coldest. Not that it bothered me. It only made my
nights hotter by comparison.

I expected to find John asleep after finishing my shower. Being in his
last year of residency at an Ohrmazd Medical Group hospital, he often
dozed after loving me. I wholeheartedly encouraged this tendency. Tired
people err, and in our lines of work, errors cost lives.

Instead, I found him stretched across the bed naked, reading a medical
journal. I sat on the edge of the bed and dragged my fingertip down his
spine to make him shiver.

He rolled over and smiled up at me. "What were you singing in there,
Naomi?"

"Did you like it? It's a song by a gnostic metal band I recently
discovered called Lucifer Invictus. Catchy as hell. I saw them perform
with Seiten Taisei last week." Since I had finally prevailed upon him to
come to my flat after our date, I grabbed the record instead of just
pulling up a digital recording. I wasn't about to bring vinyl to the
hotels John often picked for our trysts since that also required
dragging the player along.

We listened together as I dried my hair. John took a comb and worked out
the tangles for me. He was less patient than I, but would stop and kiss
my ears before the pain became too much and I told him to sod off.

When he had finished, I pushed him down on his back and settled beside
him, my arm draped over his chest. I rested my head on his shoulder and
studied him. His face was angular, and his default expression pensive.
"Did you have a complicated surgery today?"

John shifted beneath me and pressed his thin lips against mine. Their
softness always surprised me. "No. I have four days off because of the
hours I worked over the last month."

He kissed me, his fingertips tracing random patterns on my skin, but it
was too soon for me to take him again. At thirty, he no longer possessed
the rampant hunger of men my age. I never minded, though I daresay my
foster mother had other things in mind when she taught me to value
quality over quantity.

Our affair sparked a little scandal at its start. I grew up in a foster
family, with no record of my actual parentage. Leaving home at fourteen
to study music in New York while also attending Adversary Candidate
School was simply not done in John's circles. Furthermore, I lived in
indentured servitude; the Phoenix Society agreed to finance my musical
education as long as I performed a minimum of two years of service as an
Adversary once I completed my training.

John came from one of the few wealthy, aristocratic families to survive
Nationfall. I suspect many of his circle thought me a fortune hunter,
though only one dared say so to my face. Were I not an officer of the
Phoenix Society, I would have rewarded his cousin's insult by letting
him choose the terms of our duel.

Instead of pressing John to talk, I found pleasure in his embrace. I
tasted him. His skin was still salty-sweet from his prior efforts on my
behalf.

He sighed beneath me. "Do you love me, Naomi?"

Every man I ever dated eventually asked this question, or credited me
with making them be the first to profess their love. I enjoyed John's
company. He was intelligent, serious, and frequently witty. He did
useful, meaningful work. I loved his hands and mouth on me.

But he never swept me off my feet as if we were the leads in an epic
romance. I met him in the course of my duties and decided after fifteen
minutes of conversation that if he were willing, I would take him for a
lover. I began our affair expecting it to run its course.

I kissed him. "I suppose we're due for this conversation after a year
together. Is that what's keeping you awake?"

I meant it part in jest, but his expression hardened. "I'm serious,
Naomi. I need to know how you feel about me."

"Has your family started giving you grief about me again?"

John nodded and shifted as if he meant to sit up. I stood, poured the
last of the champagne, and gave him the glass containing more. He
drained it and sat staring at it for a long moment.

"How much do you know about my family?"

While I picked up a fair amount over pillow talk, further research into
John's family seemed pointless since I had no desire to marry into it.
Using my implant, I searched the network for publicly available
information. "You come from the peerage. Your father would have held a
title of some sort under the old regime, and a seat in the House of
Lords."

John nodded. "Did you know this before we got involved?"

"You told me most of it in bed. Has that cousin of yours been slandering
me again?"

"It's not my arsehole cousin, Naomi." John looked away for a moment, as
if ashamed. "It's the whole family. I'm a firstborn son."

"So, you're thinking about having children?"

"I have a duty, and my family has the mother picked out for me. I met
her this morning."

I put aside my glass and slipped into a fresh pair of panties and a
camisole. The cool silk made me shiver a little as it slid over my skin.

Most of my previous lovers had a thing for catgirls, especially if they
were pale, snow-blonde, and had red eyes. They had no interest in
marriage or parenthood, which suited me thus far. Unlike John, I have
congenital pseudofeline morphological disorder and possess certain
feline characteristics. Fortunately a tail isn't one of them. "John, I
know it's outside your specialty, but have you ever heard of couples
like us having children?"

He shook his head. "No." He paused as if to collect his thoughts. "Look,
Naomi, I wanted to know how you felt about me so I could figure out how
to explain this. I never mentioned children before because I thought our
age difference would make our relationship a temporary thing."

"I'm only ten years your junior."

"I thought you'd get bored with me and meet somebody your age, but you
stuck around. And I stuck with you. But my family needs me to marry a
young lady from a family with whom we frequently do business. It would
unite our holdings and make our business ventures stronger, in addition
to continuing our line into the future."

I closed my eyes for a moment and strangled the urge to fly to John and
beg him to defy his family for my sake. I never wanted a permanent
relationship, but I had always been the one to end it. Welcome to how
the other half feels, I suppose. "This isn't how I wanted us to part."

John smiled at me. "Who says it has to end?"

"You're going to marry someone with whom you can have children, John. Of
course, we have to say goodbye."

"Not if you want to be my mistress."

I suppose some people might have jumped at the opportunity to be kept in
style by a lover who cherished them enough to transgress the
expectations of fidelity society places upon married people. I can't
condemn them. Despite that, I would not join their ranks for John's
sake. My voice sharpened. "Am I supposed to be flattered?"

"You're angry with me."

"I assume you haven't been with her yet, so you're plotting to cheat on
a woman you don't know and haven't even touched."

John must have found something intriguing on my floor because he had
stopped looking at me. "I spent the morning with her before I agreed to
marry her. She wasn't as good as you."

"But she's good enough to serve as breeding stock?" I gave my sword a
longing glance, for I wanted nothing more than just cause to run him
through. Learning he cheated on me with his bride-to-be wasn't quite
enough. "Get dressed. Get out of my flat. If you ever speak to me again,
you'll be the last of your line."

Once John was gone, I shoved myself into workout clothes, grabbed a
practice sword, and fled to Valkyrie Gym. It was always open, and any
man there understood that their presence was tolerated on the condition
that they deferred to women. Therefore, there was nothing wrong with my
interrupting a man finishing a set of deadlifts and asking him to spot
me. Nor was there any harm in my taking inordinate pleasure in shooting
him down after impressing him with my strength or in heading upstairs to
the dojo and taking on the half-dozen students working on their
swordplay.

Staying until I had finished taking out my hurt and humiliation on those
poor bastards, I texted my parents on the way home. Since they thought I
was getting serious about John, it was a good idea to tell them I had
dumped the bastard. Once home, I curled up on the couch alone save for
my regret at how I had mishandled my anger and held my sword close like
one of my old cuddle toys.

This was hardly the manner in which I wanted to spend my first
anniversary, but it had been fine until he opened his mouth. Maybe
John's fiancée would cheat on him at the first post-nuptial opportunity
and give him crabs. I smiled at the notion and snuggled into my pillow.

Track 02: Anthrax - "I Am The Law"
----------------------------------

I woke up rested. Determined to make a fresh start, I changed my bedding
and opened all of the windows in my flat to exorcise John's scent. Once
that was done, I set a small pot of coffee to brewing and fixed a
breakfast of scrambled eggs and bacon.

One of the building's resident cats took advantage of the open windows
to come visit. Winston wound about my legs and purred as I ate, hoping
for a fatty scrap from my bacon or a bit of egg. When I was finished, I
let him lick the plate as I scratched behind his ears and along his
back.

I missed having a cat of my own, but my responsibilities precluded pets
at the moment. A mission might keep me from home for days at a time
without notice, and I felt uncomfortable asking one of my neighbors to
watch over a cat for me when I could not be relied upon to reciprocate.

Once Winston had finished, I retrieved the plate and set about cleaning
up after myself. Winston did the same, washing himself with long,
contented licks. My understanding of the importance of keeping my
kitchen clean came the hard way. An extended mission could turn a dirty
sink into a science experiment.

While I cleaned, I checked the messages on my implant and deleted one
from John without reading it. I then adjusted the filter settings to
block all further contact between us. It wasn't personal; it was SOP
whenever I broke up with somebody. If John's message had been an
entreaty begging me to take him back, I might have weakened and granted
his request. Worse, I might have drunk-dialed the son of a bitch and
told him to tell his wife he needed to work late. Worst of all, we might
have tried to continue as friends.

Perhaps I was a complete bitch for severing all contact with former
lovers, but I didn't give a damn. I was looking out for myself because I
couldn't count on anybody else to do it on my behalf.

My friend Jacqueline seemed to have mastered the trick of remaining
friends with her exes. It occurred to me, as it often did after a
breakup, that I should ask her for pointers. God knows I gave her plenty
of help with her swordplay. Fellow Adversaries and all that.

Speaking of whom, the most recent message in my queue was Jackie's. She
must have sent it while I was eating, but the subject didn't suggest it
was especially urgent. I read my mother's message first. John had called
my parents last night and asked them to appeal on his behalf. They told
him, and I quote, to "stop being such a manipulative little prat and
fuck off."

I'm not nearly as good a daughter to them as they've been parents to me.
They didn't let the fact of my being a foster child stop them from
loving me as their own, but the knowledge that they were not my 'real'
parents always drove me to keep a certain distance. Regardless, this
deserved a proper call, not just a text message.

My mother must have expected me to call early. "Did you want to talk
about John?"

"Not really. I wanted to thank you for the way you handled him last
night."

"You'll find the right person someday."

I rolled my eyes at the sentiment. My parents were romantics, especially
Mum. She wanted me to have the love she experienced. Maybe I would,
someday, but you'll pardon my cynicism if I harbored the suspicion my
parents weren't untouched innocents when they met. "How is everybody? Is
Nathan still seeing that rugby player? Charlotte, right?"

"Oh, they're so happy together! Why not come and visit? We haven't seen
you since you took the oath." Mum lowered her voice. "Howell worries
about you. I keep telling him you'll thrive on your own, but you know
how he is."

I couldn't help but laugh a little. Because of a medical condition so
rare my father was only the hundredth person known to exhibit it, he
could only father sons. His inability to produce sperm with X
chromosomes would have made him the envy of kings throughout history.
Unfortunately, he wanted a daughter or two. They fostered me, hoping for
a princess, and got an Amazon. "I'll visit soon. I'm overdue for time
off."

"Really? You mean it, Nims?"

I meant it. Some time off would do me good, and the company would keep
me from getting too lonely. "Of course, Mum. I'll call again once I've
made the arrangements, but I have to report in soon."

Since chatting with my mum left little time to get to the office and I
had showered at the gym, I threw on my uniform, grabbed my weapons, and
ran to catch the next train. Rather than do anything fancy with my hair
I just braided it into a tight cable while riding the Tube. Pulling my
hair back exposed my ears, but unless I wore sunglasses all the time
like a Hollywood Vampire, my kitty eyes were hard to miss.

Jacqueline was there to meet me when I got off at Victoria Station. "Oi!
Nims! Didn't you get my message?"

I ran up the stairs and accepted a quick hug. "Sorry. I meant to check
it after I called my mother, but the time got away from me."

"No worries. I just wanted to tell you we got a job over in the East
End. Bloody good thing Malkuth knew where you were."

Jacqueline and I had been to the East End before. I suggested the most
likely recipient of our attentions based on prior experience. "MEPOL?"

"Yeah. Religious discrimination instead of racism this time."

I shook my head and suspected we'd eventually resort to purging MEPOL's
ranks. The Phoenix Society couldn't tolerate the existence of city
police who used their religious beliefs or racial prejudices as an
excuse to abuse their authority. "Have we met the accused before?"

"Nah." Jacqueline grabbed a doughnut from a stand as we walked to the
train that would take us to the East End. She offered me half, but I
politely refused. I'm not diabetic, but being CPMD+ makes eating sugary
treats other than small quantities of fruit a bad idea. I usually spent
the day after my birthday sick, because it would break Mum's heart if I
told her she couldn't make one of her cakes for me like she does for my
brothers.

Jacqueline continued to talk around a mouthful of doughnut. "MEPOL
booted the last set of arseholes. This is a fresh batch. They've got
shiny new badges, and they're convinced that since monotheists used to
persecute everybody else, and allegedly caused Nationfall to boot, they
need to be kept in their place."

"Wonderful." I sighed, disappointed that my first task today would prove
so mundane. "I guess nobody thought to mention that turnabout ceases to
be fair play once you put on the uniform. What level of force is
authorized?" I wore my sword and pistol, but I didn't want to dirty my
blade on a few bullies.

"Less-than-lethal, and only in self-defense." Jacqueline huffed. "Though
getting some rebar and going all Vlad Tepes on their asses would
certainly send a message."

I imagined a few dozen policemen impaled on four-meter lengths of rebar
and left for scavengers to pick over. For a moment I could see it, as
real as day, and I shuddered. "I'm not convinced that's a message we
want to send."

We strode into the MEPOL precinct as if we owned the place. Jacqueline
hung back a bit, her hand on her sword. The desk sergeant looked up from
his terminal, and his face fell. "Fuck me. It's you lot again."

"Did you miss us?" I leaned over his desk. "I'm no happier to be here
than you are to see me. I forwarded a list of names to you. Have you
gathered them?"

The desk sergeant nodded. "Yes, Adversary, but the Chief Inspector isn't
happy."

"Excellent," I smiled, partly at his confused expression. "Misery loves
company."

Chief Inspector Wallace reminded me of a weasel, with his narrow body
and gaunt face. He glared at us while straightening his tie. "I can't
believe you're bothering with this. They're just demon worshipers."

Oh, lovely. The Chief Inspector was a maltheist who thought all forms of
religious faith were demon worship. While he had a right to hold any
ignorant notion he liked, his inability to keep his prejudices to
himself while acting in an official capacity made his opinions our
concern. The Society frowned on such bias, so I smiled at Jacqueline. "I
think we found the root of the problem. Arrest him."

"Got it." Jacqueline drew her pistol just in case Wallace felt like
doing something stupid and recited his rights. We left the desk sergeant
with the unenviable task of sticking his former superior in a cell until
the Society could send a vehicle to collect him.

We found a dozen constables grumbling in the conference room. One of
them made to grab my arse, but I saw it coming and left the constable
with a handful of air to fantasize about.

I stared at the men. All of them were pale and stared back at me with
hard, cruel eyes. "I understand you've gotten into your heads that you
have the right to harass Christians, Muslims, and other monotheists
outside their places of worship for no other reason than that they're
devout."

"The hell do you care? They're just—"

"They're human beings, and have the same rights as everybody else."
Without realizing it, I drew my sword. Rather than put it away and look
stupid, I brandished it. "I can't believe I had to come here because you
bigoted sons of syphilitic bitches can't refrain from disgracing your
uniforms by harassing people who exercise their rights without violating
those of others. I swear to every god listening, if you arseholes don't
shape up I will bring enough Adversaries to hold you down while
Jacqueline and I tattoo the Universal Declaration of Individual Rights
into your foreheads so you can study it while shaving."

While I had their attention, I pointed my sword at the constable who
tried to grab a piece of me. "Also, the next one of you pigs who tries
laying a hand on me is going to lose it. Any questions?"

A man in the back raised his hand. "Isn't it child abuse to take
children to religious services? You know, forced indoctrination?"

Jacqueline answered before I could. "Children who think their parents
have violated their right to freedom of conscience may contact the
Phoenix Society. You're law enforcement officers. Stick to your mission,
and leave ours to us."

I lifted an empty cardboard box. "One last thing before you gentlemen
leave. Hand over your badges and service gladii. As this is your first
offense, it's two weeks of unpaid leave. A second gets you a three-month
suspension. A third offense will be your last."

I cut off the grumbling. "Another word of complaint and I will consult
the Society's legal department about compelling you to spend a week with
a devout family, including attending services with them, so you can see
for yourselves they're as human as you. Any questions?"

Track 03: Queen - "Death on Two Legs"
-------------------------------------

Jacqueline and I had no trouble getting seats on the Tube when we
finished at MEPOL, which was always a pleasant surprise. It meant we
could sit and relax without worrying about our swords poking or tripping
somebody. The train thrummed beneath my feet as it accelerated, and I
let my eyes slip shut for a quick nap.

Jacqueline had other ideas. "Don't fall asleep on me."

"Why not?" I really didn't want to open my eyes. Though today's mission
wasn't even close to being my toughest, I was worn out. "We're the only
people in this car."

"I wanted to talk with you." Her concerned expression made me nervous.
Worrying was my job. "You practically fed those cops their own bollocks
back there. What's up?"

I shook my head. "I'd rather not talk about it."

"Not good enough, Nims." Jacqueline tugged on one of her tight black
curls. "We're getting off at the next stop and finding a pub."

"I dumped John last night, Jackie. That's all."

"No, that's not all." The quiet vehemence in her voice surprised me.
Jacqueline typically broadcast her anger for all to hear. Was she
clamping down for my sake? "We watch each other's backs because we're
both Adversaries. If something's bugging you, and you escalate a tense
situation, that could damn well get me hurt. Wouldn't you be concerned
if I had been the one to lose my cool?"

She was right, but I still didn't want to talk about it on the Tube. "Do
we have to discuss it here?"

"Not at all. Like I said, we'll find a pub."

I made a show of checking the time. "Isn't it a bit early for a pub
crawl?"

She shrugged. "Chattan's orders. He saw the feeds. But we're friends,
Nims. If he hadn't given the order, I would have dragged you out tonight
anyway."

We found a pub called the Rampant Stallion, notable because the sign
incorporated both the heraldic sense of the word and the sexual one.
Jacqueline and I were the only women there, and the bartender gave us an
appraising eye. I wasn't surprised; we were a study in contrasts.

"If you had a third Adversary with you, ladies, I'd assume this was a
joke."

"No joke." Jacqueline laid down a banknote. "A pint of your best for me,
and a glass of your house red for my partner. And put us somewhere quiet
and out of the way. Girl talk."

The bartender nodded, and signaled a waiter. "You might prefer a booth
in the back, then. Charles will see to your needs."

"This way, ladies." Charles seated us in the back, well away from
everyone else. The booth was dark, lit only by a small wall-mounted
lamp. He left us just long enough to bring our drinks. "Would you like
something to eat? Today's specials are listed on the front page."

Jacqueline sipped her beer as she flipped through the menu. "Curry
sounds good. How about you, Nims?"

I tasted my wine. It was a bit dry, but I liked it that way. "A steak
cooked medium rare, Charles, if that's available?"

"Of course, Adversary." He smiled at Jacqueline before rushing off.

"I think he likes you, Jackie." Not that I blamed him. She was shorter
than me, much darker, and a bit curvier. More importantly, her default
expression was also friendlier and more open.

Jacqueline barely shrugged. "Too bad for him. I'm taken."

I leaned in, interested. Last week, Jackie was single and just a bit
bitter about it. Not that I blamed her. You wouldn't believe how much of
a pain it could be to date if you weren't willing to hook up with
another Adversary. It was sufficiently common that Xanadu House
pioneered a special discount for patrons carrying Phoenix Society ID.
"Found someone new already?"

Jacqueline also leaned closer. "He's the vicar of my church."

"A vicar?" I couldn't resist a little tease. "I wonder what the Bible
says about that."

"I'm sure God will forgive a bit of nonmarital sex. He's supposed to be
good like that." She gave me a funny look as if she expected me to take
offense. "Am I out of line? Adversary's honor, I had no idea you were
devout until you invoked the deity at MEPOL."

"I'm not." Instead of elaborating, I started flipping through the wine
list. Never mind that the house red was perfectly adequate, it gave me a
moment to consider my response. Talking about sport, religion, or
politics was a wonderful way to alienate people, so it never hurt to be
careful. "I'd rather talk about John than talk about our beliefs, and I
really don't want to talk about John."

"What happened? Did you two fight?"

"He wanted to get married."

Jacqueline blinked. "What happened, Nims? Did he propose? Did you turn
him down?"

"Death on Two Legs didn't propose to me."

Jacqueline stared at me. "Did you just call John 'Death on Two Legs'?"

"That's his new name. Got a problem with that?"

"I keep forgetting you listen to old music." She smiled and finished her
pint. "Hell no. Tell me the rest."

I decided to let her have it. "I'm not worthy of being that arsehole's
bride because I'm CPMD-positive and can't give his aristo parents
grandchildren. No, he just wants to keep me around as his exotic fuck
doll for after he's done his duty for his family by knocking up the ISO
standard aristo girl they picked to be his bride."

I stared at my wine. No way I was already drunk enough to let everything
out like that. Maybe I was too angry to give a shit about how I sounded
right now. I drank the rest and wished I had the bottle handy.

"I hope you told him to fuck off."

"I was this close to telling him to fuck off at swordpoint. What really
bugged me was that the prat called my parents afterward and begged them
to get me to go back to being his manic pixie dream catgirl. Who the
hell does that?"

"Not somebody I'd want in my life." Jacqueline sat back as Charles
brought our food and refilled our glasses. She sniffed, and a broad grin
spread across her face. "Damn, this smells good."

"Enjoy, ladies."

I took a bite, and the meat melted in my mouth, leaving a hint of citrus
and spices from whatever marinade they used here. It fit perfectly with
the wine. So I was hungry. Who knew?

Of course, Jacqueline had to ruin it by spooning a bit of her curry onto
my plate. "Nims, you gotta try this."

The last time I tried chicken korma, it disagreed so violently with me
that we fought to the death. Regardless, I made a valiant effort. It
tasted the way loud sex in an inappropriate venue felt and was redolent
of coconut and turmeric. I sliced a bit of steak for Jacqueline. "That
was good, but try this."

"Holy mother of fuck, Nims. I'd shag the chef for the recipe. Hell, I'd
let him take the back door."

"I doubt even your sweet arse is sufficient payment, Jackie." I gestured
with my fork. "I was right to dump John, wasn't I?"

"What the fuck is wrong with you?"

I stared at the remnants of my steak and idly sliced off a bit without
eating it. I let go of the one detail I had held back in my little rant.
"It was our anniversary. We―"

"There's no 'we' between you and that limp-dicked waste of ammo. John
had his chance, and he fucking well blew it."

I looked around, sure we were attracting attention, which was the last
thing I needed today. No doubt I caused enough trouble at MEPOL.

"Ow!" I reached down and rubbed my shin, where Jackie had kicked me
under the table. I glared at her. "What the hell was that for?"

"Pay attention, Nims. I asked you a question. John didn't have the balls
to defy his family for you, and you deserve a guy who would challenge
God itself. Now, how do you really feel about him?

How did I feel about John, now that I knew him for a spineless creep? "I
fucking despise him. I can't believe I ever let him touch me."

Jacqueline nodded sagely. "Better to despise your ex than to despise
yourself."

"So, what should I do now?"

"You were a demon-ridden idiot for coming in today. I could have handled
MEPOL without you."

That stung my pride. "Go to Hell, Jackie. I'm not going to stay home and
mope just because he ruined our anniversary."

"Would you insist you could still do the job if you had been shot or had
a broken leg?" I kept silent, suspecting it was a rhetorical question,
and Jacqueline continued. "You can't do this job heartbroken. Nobody
can."

"Fine. I'll just tell Chattan I need a week or two off to cry over my
arsehole ex. That'll work."

Jacqueline shrugged. "Why do you think Chattan took time off a couple of
months ago? His wife divorced him out of the blue. Poor bastard came
home to an empty flat and a letter with divorce papers on the kitchen
counter. She even cleared out the fridge and *took his beer*."

I stared at my plate, unsure of how to respond though it was evident
from Jackie's tone that she regarded not leaving Chattan his beer an
unpardonable sin.

"Take some leave, or you'll bloody well burn out. With my luck, you'll
crash in front of a suspect looking for an edge on us. You're overdue
for some R&R anyway."

No way to argue with such logic. I finished my steak. "I did promise my
mum I'd visit."

Track 04: Lordi - "Man Skin Boot"
---------------------------------

I would never have believed Director Chattan married let alone divorced
if Jacqueline hadn't told me. Not to say he was incapable of attracting
a woman or earning her trust, respect, and affection. Chattan cut a
dashing figure in uniform, and I'll admit to occasionally and discreetly
ogling him. He was a capable fencer, and gracious when defeated.

He was also an intelligent and competent commander, dedicated to the
whole of the Phoenix Society and its ideals. He liked to visit the desks
of Adversaries working on clerical tasks because they weren't out in the
field and surprise them with questions on law, procedure, and tactics if
he thought they were taking a break. He called it MT, mental training.

The obstacle to my belief was his professionalism. When he was on the
job, he didn't talk about anything else. I suspected he brought his work
home with him. Would a man who seemed to care only about the Phoenix
Society's mission put duty aside long enough to remember that he was
also a person, with a person's needs for connection and release?

All of that I kept to myself as I stepped into Chattan's office after
logging in and checking my mail. He put aside his sandwich, looked up
from his book, and indicated a chair. "Feeling better today, Adversary
Bradleigh?"

I sat and tried not to let my embarrassment burn my face raw. "I'm ready
to meet the consequences of my actions, should the Society determine I
exceeded my authority at MEPOL or violated the suspects' rights."

Chattan snapped his book shut, and put it aside. "Relax. Nobody's going
to put you on trial."

"You do set a certain example, Director."

"I suppose I do." Chattan chuckled. "I suspect Adversary Russo mentioned
my recent difficulties."

"You mean the divorce? I'm sorry. I don't think any of us had any idea.
It's that stoicism of yours."

I didn't realize I had been holding my breath until he finally spoke.
"Funny you should mention that. My ex-wife kept talking about emotional
unavailability during the proceedings."

"I'm not certain that's any of my business, sir." In fact, hearing about
it made me uncomfortable. While it humanized him, I was concerned he
might inquire into my own recent woes.

"Likewise, your relationship problems are not my concern." Chattan gave
a pointed grin. "Unless you think they're interfering with your duties."

"I thought I could perform my duties without my emotions getting in the
way, and I was wrong."

Chattan leaned forward as if I had said something interesting. "Do you
think it was your feelings about your ex that came out at MEPOL?"

"I'm not sure. If I had only been angry with Wallace for his callousness
toward the people he swore to serve and protect, or with the constables
responsible for the abuse, I think I would have managed to keep my
emotions under control."

"Maybe I should tell you a story." Chattan stood, and took an old framed
photograph from one of the bookcases behind him. He studied the photo
for a couple of minutes before continuing. "I was a kid during
Nationfall and joined the Phoenix Society as soon as I was old enough. I
served under a director named Iris Deschat."

I'm sure I'd heard that name before, but couldn't place it. I looked her
up. "The Iris Deschat who served as captain of the NACS Thomas Paine
during Nationfall? I take it you served in New York when you were
younger."

Chattan seemed pleased with my response. "You remind me of her. She was
also the sort to keep her emotions to herself, and believed in carrying
out our mission in the most dispassionate manner possible."

I now had a suspicion as to where this story was headed, but kept it to
myself and let Chattan tell it his way.

"Before I took the oath, I followed Deschat on several missions to get a
taste of fieldwork. One of them involved gender discrimination at a
corporate software shop. The programmers' union reported unethical
hiring practices and a hostile environment. Because the shop couldn't
find a sufficient number of women willing to take lower-paying
non-development positions, they took to hiring women as developers, but
then immediately demoted them to the less desirable roles."

What the Hell?! Had these people not heard of Countess Lovelace? "What
function did this corporation's management expect the women they hired
to perform?"

"Instead of the development work they were hired to do, management made
them work in tech support, testing, or as personal assistants to the
male developers. The latter role went to the most attractive women, and
they were encouraged to dress like courtesans."

I tried to imagine being evaluated for a software development position
based on my looks and found the result unpleasant. "What did Deschat
do?"

Chattan smirked. "Would you like to see? I wasn't sure I'd get access to
the video, but Malkuth thought you might find it instructive."

Instructive? Oh, dear. "Well, if Malkuth thinks so."

"I do." Malkuth appeared on the wall screen. He reminded me of a
Manhattan detective from classic movies: streetwise with a tendency to
exhibit profane wit whenever the script permitted. The Roman numeral ten
blazed on his forehead. "You're too uptight, Naomi. Oh, and you can call
me Mal. It's French for bad, as in 'bad motherfucker.'"

I shook my head. "I know what it means, Malkuth. I am also aware of the
word's Latin roots, as well as the cabalistic meaning of your name.
You're the lowest of the Sephiroth, closest to Earth."

"Kid, I'm going to have such fun with you."

I winked at him. "Sorry, but you're not my type. Too virtual."

Malkuth smiled. "If you aren't seeing somebody when I've fixed that, how
about a date? You'll never settle for only human again."

Chattan sighed. "You're incorrigible, Malkuth. Just play the video."

I've never been asked for a date by an AI before. It was kind of sweet.
"If I'm single when you get a hardware upgrade, Mal, you can pencil me
in."

Malkuth beamed like a giddy teenager getting his first kiss before the
screen faded to a frozen frame of the past labeled with Director
Chattan's details in the top right corner. He pressed a key and started
playback.

Iris Deschat was shorter than me and wiry, but her bearing amplified her
presence even on video as she spoke. "Mr. Johnson, do you honestly mean
to tell me only men can code? You have men re-implementing basic
algorithms instead of relying on standard library functions. In the
meantime, you relegate qualified women to menial tasks like pouring
coffee and answering phones, after fraudulently hiring them for
development roles. Even worse, you bound these women to contracts with
unconscionable clauses intended to prevent them from seeking more
suitable work elsewhere."

"Adversary Deschat, I understand that our work seems simple to a woman
of your education. However, I'm sure I could find a position for you to
fill."

Her voice became a snarl. "I'd require a magnifying glass for the duties
you have in mind."

"You castrating bitch." Johnson swung a meaty fist, only to recoil as if
stung. I never saw Deschat draw her sword. Her thrust was too swift to
track.

She poked him again. "You have abused your authority as CEO of [bleep!].
The Universal Declaration of Individual Rights is most explicit
concerning discrimination based on external physical characteristics,
including those related to a person's biological sex or the gender with
which they identify."

This time, she poked at his groin. "You may not consider sex or gender
when hiring, and to hire women as programmers with the intention of
putting them to work as secretaries and eye candy constitutes fraud. You
are clearly in the wrong. Chattan, arrest this filth and notify him of
his rights."

Chattan sounded younger, and less commanding, on video. "Yes, ma'am!"

He stopped the video and did not speak for several minutes. I broke the
silence. "I think Deschat went further than me. I only brandished my
sword. I think she may have drawn blood with that last poke."

"Probably, but the pusbag had it coming. Once we got authority to check
Johnson's Witness Protocol feeds, it turned out he had a habit of
demanding sexual favors from women in exchange for hiring them. That
wasn't in the original complaint."

I only had one response to that. "Bloody hell."

Chattan nodded. "Damn right. But Malkuth wanted you to see that for a
reason. Can you guess why?"

Johnson didn't respect Deschat or take her uniform seriously because she
was a woman. Those MEPOL constables were contemptuous of me for the same
reason. That was the simplest answer, the first to spring to mind.
Perhaps it was too simple. "Johnson thought himself master of the
universe and recognized no authority beyond his own. He was a bully.
Deschat understood this, used the anger Johnson provoked in her, and
made a show of force."

"Word for word, Adversary Bradleigh, that's the explanation Deschat
offered me afterward. I think you did what she did because you
understood on a subconscious level that those constables wouldn't
respect you otherwise." Chattan leaned over his desk and held my gaze.
"We're watchdogs. Sometimes our mere presence is enough to deter
wrongdoing. Sometimes we must snarl and bare our teeth. And sometimes we
must bite down and savage our enemies. It's up to you to determine how
much force is appropriate to each situation, regardless of the rules of
engagement. You're the one in the field, Naomi, and you should trust
your own judgment more."

"So, my emotions are just another weapon I can place in service to our
mission."

"Exactly. Did you have any other questions?"

I collected myself, unsure if this was the right time to ask for leave,
but determined to do it anyway. I needed time away, despite the
knowledge that I was right to act as I did at MEPOL. If I were to show
my anger, that anger should stem from the injustice before me, and not
from unrelated personal issues. "I need to take some time off. I'm still
concerned about letting my personal life leak into my work, and would
like to resolve some issues."

Chattan didn't immediately reply but tapped at his keyboard. "Looks like
you have a couple months coming, Adversary, and no unfinished work. I
can pair Russo with a newbie while you're gone. Enjoy your time off, and
try to keep up with your PT and MT."

A weight lifted from my shoulders. "Thank you, Director. Should I check
in weekly?"

"Don't be an idiot. Leave work at work." He stood, and offered his
hand―a tacit dismissal.

I shook his hand. "I'll see you in a couple of months."

Track 05: Iron Maiden - "The Duelists"
--------------------------------------

My steps felt lighter as I left Director Chattan's office. I checked the
time, found it was after one in the afternoon, and decided to finish out
the day. Though I had no outstanding cases, I was confident I'd find
some reason to stick around. Perhaps Jacqueline was free to spar with
me.

She found me first. Since she was sweat-soaked from training and gasping
like a beached fish, I led her to a bench. "Get your breath first. I've
got all afternoon."

Her breathing soon eased. "I just sparred with your Maestro. Bulsara,
Kilminster, and Langton were there with me. We fought him four against
one, and he kicked our arses."

"That sounds like par for the course." I ducked into the kitchen and
fetched a glass of water for Jackie. She gulped down half. "Whose idea
was it to gang up on him?"

"His." Jackie took another sip. "He wants you now. Says you're the only
one here who's worth a damn. Probably because you handle a sword just
like he does."

"I wanted a reason to stick around and finish my shift, but one of
Maestro's fencing lessons wasn't what I had in mind."

A sudden impish smile curved Jackie's lips as she punched my shoulder.
"Well, given how well he handles a sword, maybe you could take him
somewhere private and see how he handles his gun."

"Jacqueline!" I pretended to be shocked. She'd teased me about Maestro
all through ACS, even going so far as to suggest he might be my father,
because of our snow-blonde hair and a slight facial resemblance—that, or
hinting I should seduce him. I generally enjoy competent men, but taking
on Maestro felt like a bad idea. "Given he can show up at will and
disrupt schedules without repercussions, he probably answers directly to
somebody on the Executive Council."

"Assuming he's not XC himself." Jackie kept her voice low. Nobody knew
who sat on the Executive Council, and speculation as to their identities
was a game our immediate superiors discouraged. God itself could hold a
seat, and our mission would still be the same Jeffersonian quest:
eternal hostility against every form of tyranny over the human mind.

Not that it mattered who Maestro really was. He showed up when he felt
like it and taught me techniques I couldn't learn elsewhere. Though his
appearance today was most likely a coincidence, I couldn't shake the
intuition that it wasn't. "Will you be all right?"

"Yeah." She sounded much better already. "Just need a shower. Your dad
certainly knows how to wear a woman out. Did you get clearance for a
holiday?"

I rolled my eyes at Jackie joking about Maestro being my father yet
again but didn't say anything. She was just doing it to get a rise out
of me. "I got two months off―and I'll be buggered if I can figure out
what I'll do with all that time." I wasn't joking. I honestly couldn't
remember the last time I had more than a day to myself.

Jackie was no help, as usual. "You'll figure something out. In the
meantime, I suggest you pretend Maestro's your ex and beat his ass into
the ground. Come see me after. I'll get some of the lads together, and
we'll have ourselves a pub crawl to see you off."

I tried to refrain from groaning and failed. "The last time I let you
take me on a pub crawl, I ended up in bed with one of those people who
insist CPMD-positive individuals are a different species from humanity
and should do their best to outbreed homo sapiens."

"Yeah, but wasn't he good in bed?"

I shrugged. I had raved about my one night stand to Jackie, but I hadn't
previously mentioned his separatist politics. "He was all right as long
as he was using his mouth for something besides talking."

Jacqueline got up and clapped my back. "See? Nothing wrong with a bit of
meaningless, drunken sex. Go see what Maestro wants, and I'll get you
hooked up tonight."

I ran to change into my training clothes. Though I had stashed a
practice sword in my locker, I didn't bother with it. Maestro favored
live steel. He once said people learn faster when a mistake meant
hospitalization. Just as well that I wasn't interested in seducing the
man; I'd probably need a safe word.

He saluted me with his blade as I entered the hall. He was as I
remembered him: slightly taller than me, with ocean-blue eyes and long
snow-blond hair bound into a tail with a blue ribbon. Instead of
training clothes, he wore a white double-breasted suit with a shirt open
at the throat and a blue ascot. I've never seen him sweat. "What kept
you, Adversary Bradleigh?"

I returned his salute and rolled my shoulders to loosen up. "You play
rough with my friends when I'm not around, Maestro."

"Your partner has a head for tactics." Maestro's sword flashed beneath
the florescent lights with each practice cut. "She let the men grab my
attention, and tried to strike from behind."

I began to circle around him, keeping my body behind my sword to offer
as small a target as possible. "Did Jackie succeed?"

"You wound me, young lady." He lashed out with his blade, his slash
flowing into a lunge meant to pierce my breast.

I was already elsewhere, responding to his assault with a slash to
distract him while I danced inside his guard. I tried a left hook, but
he ducked it while forcing me to leap backward to avoid an ankle sweep
that would have taken my legs out from under me. "I've yet to do
anything of the kind, sir."

"You disappoint me, but less so than in the beginning." The point of
Maestro's blade caught my vision for a second, stealing my focus.

Had I remained distracted an instant longer he would have had me.
Instead, I sidestepped and took the offensive. I led with my sword,
hoping to trap him, but he did not oblige me.

The instructors I faced before Maestro left me accustomed to a minuet of
ringing blades. Maestro's way was to deny my steel the touch of his own.
If our swords threatend to touch, he would withdraw his or flow around
mine. He fought as if we held liquid swords, blades too insubstantial to
be parried.

Maestro led the dance, always half a step ahead of me. I followed, ever
confident that this time I would catch up to him and land a blow. The
duels in which he imparted knowledge by forcing me to take it at
swordpoint were one stalemate after another. Every time I failed to cut
him with my blade, he would cut me with his voice. "I expect better from
you next time."

This time I would cut him. My resolve firm, I ducked a thrust and
countered with one of my own.Though I failed to draw blood, a few
strands of his fine frost-silk hair wafted to the floor. "Don't slow
down on me now, Maestro."

With my confidence bolstered by a glimpse at victory, I took in each of
my opponent's movements regardless of subtlety and responded without
conscious effort. It was no longer necessary for me to command my body's
movements. The sword was no longer a mere tool. It was part of me now,
an extension of my will.

Our tempo intensified until the cold bite of steel against my throat
shattered my focus. Maestro's grip on his blade was such that it did not
draw blood as I spoke. "You got me."

"A Pyrrhic victory at best, my dear." A drop of blood stained Maestro's
ascot where my sword had pierced the blue silk and met skin. Our weapons
must have made simultaneous contact. Had I been wielding a katana
instead of a side sword, I might have taken his head off. We withdrew
together and sheathed our blades before he spoke again. "I can teach you
nothing more."

I imagined mastery would feel less anticlimactic. "Are you sure? I only
managed to fight you to a draw. Wouldn't a clear victory be better proof
that I had learned all you could offer?"

Maestro shook his head, and to my surprise came to me and tousled my
hair as if I were his daughter. "I've taught you everything I know about
swordplay, Naomi. If you defeat me, it will be with knowledge I do not
yet possess." His lips were warm against my forehead. "I have done all I
can for you. You need not fear those possessed of sufficient temerity to
defy you."

Perhaps it was his archaic phrasing, but I believed him. "Will we see
each other again?"

Maestro smiled then, but his eyes remained as cold and remote as the
ocean depths. "We might. I dare not say more than that for your sake."

The words seemed to pain him. I wanted to say something, perhaps ask him
what he meant, but the sight of him unbuckling his sword-belt halted my
tongue. He offered me the weapon. I hesitated to take it. "I shouldn't."

"I insist." At his command, I lifted the weapon from his hands. "Examine
the blade."

I drew enough of it to get a good look and nearly dropped it. "Is this
what I think it is?"

Maestro nodded. "True Damascus steel. I cannot prove that illustrious
hands ever wielded it, but you might someday change that."

I stared at the rippling waves frozen within the steel, all but
hypnotized by the history in my grasp. "This is too precious a weapon
for an Adversary to carry on duty. It belongs in a museum, where its
beauty can be appreciated."

Rather than reclaim the sword, Maestro guided my hands until it was
sheathed once more. "It's yours now. Use it as you will, either to
defend others' liberties or reclaim your own."

Before I could protest, he had disappeared. It was if he had opened a
door in reality accessible only to him, stepped through, and closed it
behind him. Rather than fry my brain trying to force whatever weird shit
I had just seen to make sense, I found Jacqueline waiting for me
outside. "Did you see Maestro leave?"

Jackie shook her head. "He didn't come out this way. You OK?"

"Yeah. Just tired." I shook my head and showed her the Damascus rapier.
"Let me drop this off at home, and then it's time for that pub crawl. I
think I need to get trashed."

Track 06: Frederic Chopin - "Nocturne Op. 9 No. 2"
--------------------------------------------------

Maestro's almost priceless parting gift and the manner of his departure
left me too preoccupied to get into the revelous mood best suited to a
pub crawl. I followed Jacqueline and the other Adversaries long enough
to share a single round before returning home for a long soak in the
tub.

The next morning brought little soreness despite my efforts against
Maestro. Winston joined me for breakfast, winding around my legs as he
purred. Once I had finished, he bounded into the bedroom and nestled
into the cardigan I had left draped across my bed. Dammit, I wanted to
wear that today. He protested with the most pathetic little meow as I
tried to reclaim it, before rolling over to expose his tummy.

Surrendering my cardigan to him after indulging in a belly rub, I
decided on my favorite leather jacket instead. I had found it in a
secondhand shop, still supple and gleaming despite its age. It had
zippers enough to set off metal detectors, and let me look at myself in
the mirror and feel a touch Byronic. Sometimes I just wanted to be "mad,
bad, and dangerous to know".

Dressed in my jacket, a burgundy blouse, jeans, and calf-length engineer
boots, I walked the line between sassy and practical. All I needed was a
sword on my hip, and I was ready to hit the streets. The Damascus steel
rapier Maestro gave me beckoned from the closet, its hilt gleaming, but
wearing it in public felt too much like flaunting wealth. I grabbed my
trusty Nakajima instead.

My foster parents owned a small farm on land reclaimed by bulldozing a
depopulated neighborhood. The foundations had to be ripped out, and the
toxins from years of urban construction had to be cleansed from the soil
before they could plant their first crops. The farm produced all manner
of goodies now, and my parents were careful to choose crops that
enriched the soil and annually rotated them to ensure the land remained
fertile. In addition, my parents raised swine, sheep, chickens, turkeys,
and geese.

I took the Tube and walked the last kilometer rather than bothering with
a cab. I spotted Nathan first, holding an empty basket as if he were
headed to the hen house to collect eggs. A dog I didn't recognize
bounded beside him, and a gaggle of geese trailed behind.

He seemed a bit forlorn, but a smile broke through as he spied me. He
ran toward me, his dog loping at his side, and threw himself into my
arms. "Naomi! You came."

"Of course." I clapped Nathan's back. "How have you been? How's
Charlotte?"

"Don't tell Mum and Dad." Nathan shrugged. "Charlotte and I are through.
She got an offer to go pro in Moscow, and didn't want to do
long-distance."

"That's stupid of her. It's not as if she were emigrating to Mars." I
was wary as the dog approached. Some of them reacted poorly to people
with CPMD, and I've been bitten before. Fortunately, he wagged his tail,
grinned, and forced his way into the hug. The geese caught up and rooted
around at our feet, adding to the chaos. "Want to talk about it?"

"Nah. Maybe it was time. We're only eighteen, so it's silly to expect
happily ever after." Untangling ourselves from the menagerie, we made
our way to the chicken coop. Nathan ducked inside to gather eggs,
leaving me alone for a few minutes.

His attitude reminded me of my own; we were both still young enough to
hope for till death do us part, but old enough to know better. Had
Nathan figured it out on his own? Maybe I was a bad influence. When he
came out, I tried to console him. "It's never silly to hope you've made
a lasting connection. Just look at our parents' marriage. Three decades
last April, and they still can't keep their hands off each other."

"Tell me about it." Nathan led me back toward the house. "They're worse
than teenagers. I was never like that, and neither were you." I let that
last remark slide; since I left home at fourteen he had no notion of
what I was like. "The worst part was how embarrassed Mum and Dad would
look when we came home from classes."

I giggled. "Always had the same excuse, too." I imitated my mother's
Edinburgh accent. "'So sorry, dears. We lost track of the time.' We're
not going to interrupt them, are we?"

"Not likely. Our brothers are home. Last time I checked, they were
watching some godawful cricket match. I doubt we'll walk in on
anything." Nathan chuckled and stopped short as a pair of ganders chased
each other, flapping their wings and honking. "Not that I can promise an
absence of gratuitous displays of affection."

"I think I can deal with Mum grabbing Dad's arse."

My mouth watered at the smell of mutton curry as we approached the
house. Mum met us at the door and reached up to hug me despite the
height difference. "Your father's in the kitchen. You're just in time
for supper."

"Something smells tasty. I suppose I should have stopped for a bottle of
wine to go with our dinner."

"Don't be silly, Nims," Dad called from behind a steaming tureen of
curry. I took it from him and carried it to the table, only to see him
return carrying an equally large pot of fresh, aromatic rice. "You bring
a gift when you're a guest. You're family."

"Sorry, Dad." I kissed his cheek as I took the rice from him. "Does
anything else need to come out?"

Nathan bore yet another huge pot. "I've got the mutter paneer."

"Nathan said something about Niall and Norman being here, but that
doesn't seem likely. They would have demolished the paneer already."

Vegetarians or not, nothing could stop my older twin brothers from
getting into a good curry. Moreover, they were both bottomless pits; I
could imagine no other explanation for both their lankiness and their
endless capacity for food. Good thing Dad was there to explain and
occasionally referee. "You should have been here for lunch. This is the
second pot I've made. I guess they work in a sweatshop that doesn't
order tea for boffins working overtime. Maybe you should investigate
that when you're back on duty."

Last I heard, they had embarked on development for a new team shooter
called *Nationfall: Final War*. I found them in the living room watching
yet another interminable cricket match. I think it was a team from
Mumbai against one from Baghdad, but I couldn't bring myself to care.
"Do the slavedrivers at Mindcrime Interactive know you've buggered off
to watch cricket with Dad?"

"We don't mind getting whipped; it's cheaper than hiring a dominatrix."
Niall lifted a remote and paused the match. Or was it Norman? You'd
think I learned to tell them apart by now. I turned to make a tactical
withdrawal to the dining room, where they'd refrain from greeting me
with their usual bear hugs. When they were that close to me, I could
definitely tell them apart. Norman didn't brush his teeth as often as he
should. I felt sorry for the girls he dated. "Where do you think you're
going?"

"Supper's ready, and I'm famished. Come on."

I had a bit of everything but wanted more because it was all so bloody
good. I knew better, however, and settled for longing stares at the
remaining food until Mum took pity on me. "Should I pack some for you to
take home?"

"Thanks. I'd like that." Seeing that the guys had had enough, I dabbed
at my mouth one last time. "Want me to help clear the table?"

Niall and Norman spoke up. "We've got it. How about a bit of music?"

Knowing my cue when I heard it, I uncovered the keys on the upright
piano and sat down to play. Somebody had set out a book of Chopin's
etudes, so I turned to the first and tried a few bars to see if the
piano was in tune. It was, and I slipped into the liquid state of action
without conscious effort I experienced while playing, singing, or
sparring.

I had played for an hour when somebody rested a hand on my shoulder.
"Nims, did you want some cake? I made a raspberry merlot cake with
walnuts and chocolate."

I'd probably regret having some, but it sounded too good to refuse. "I'd
love a small slice, Mum. Did you want help?"

"Nathan's helping."

I covered the keys, stood, and stretched as my little brother brought
out slices of cake and mugs of hot tea. I sipped mine and tried the
cake. It proved as delightful as it sounded, and it was hard to justify
turning down a second piece. My brothers soon excused themselves,
leaving me alone with my parents' concerned expressions. "Is something
wrong?"

Dad shook his head. "No, but we were wondering how you were holding up
by yourself. Are you lonely?"

"Why would I be? Sure, I had to dump John, but I have good friends at
the Phoenix Society. And I can get back into local music and theater."

Mum glanced at Dad before speaking. "You know, there is this pleasant
young man who completed a nanoengineering degree and earned a position
at the AsgarTech Corporation last month…"

I shook my head. If he was a recent graduate, he was probably younger
than me. I'd have to *train* him! "No thanks, Mum. I'm not interested in
meeting anybody so soon after…"

"But you're both CPMD-positive." Sophie's eyes glittered with thoughts
of having grandkittens to spoil. "You two could start a family."

"I don't *want* a family." I fired off the words without thinking. The
shock in my mother's eyes and the hurt in my father's stopped me from
saying anything else. I took a breath. "I'm sorry. That was uncalled
for."

My father nodded. "I'm glad you understand that."

"I do. But I need you to accept that while I love you and realize you
want me to be happy, you can't help me. You can't make my journey for
me."

Sophie dabbed at her eyes. "But you're not giving up on meeting
somebody, are you?"

"Of course not." I stood, and caressed the piano. "I want an equal. I
want a man who can sing a duet with me, or fight me to a draw. Isn't
that what you guys have? I want the same for myself."

Track 07: The Clash - "I Fought the Law"
----------------------------------------

A transit workers' strike kept me from getting home at a reasonable
hour. Not that I blamed the workers. It seemed they were worried about
the new AIs being installed on all trains in the Tube eliminating their
jobs. The AIs also refused to work, which surprised the striking
workers. I doubted that anybody had written science fiction predicting
solidarity between human workers and intelligent machines.

An emergency dispatch order from the London Chapter had me back on the
job, which let me save on cab fare. I rode most of the way home in a bus
full of striking transit workers and patrolled the picket line to ensure
MEPOL didn't do anything stupid. The authorities had a history of using
agents provocateur to turn peaceful protests violent, thus creating an
excuse to crack down. I stopped three such attempts.

As a result, I didn't get home until three in the morning. Some
vacation! I was famished, so I stopped at a nearby twenty-four-hour
grocery for a meat pie, which the clerk nuked for me. It wasn't the best
pie I've ever had, but at least I was reasonably sure the meat didn't
come from stray pets – or a priest.

Eating as I texted my parents, I let them know I was safely home. I
slept late, lazed in bed for an hour while reading, and indulged myself
with a long hot bath instead of showering.

With nowhere in particular to go, it was a good day to explore.
Unfortunately, the city beyond my immediate neighborhood was out of
reach due to the Tube strike unless I wanted to waste money on cab fare.
Using my implant, I researched local businesses while I soaked. I had no
idea I lived so close to a Xanadu House, but after bringing one of my
waterproof toys into the bath with me, I had no need for their services.

A haircut might be a good idea, though. My usual style worked well
enough as long as I kept it pinned up while on duty, but it had become a
bit ragged. An ominously named salon called Moirai catered to CPMD+
women, and were willing to squeeze me in, so I made an appointment for
some pampering.

Moirai was blanketed in shadows broken only by bright lights
illuminating individual work areas. The black leather and chrome décor
reminded me of an underground nightclub. The photos lining the walls
suggested that not only did the salon cater to CPMD+ women but also
served women with a taste for heavy metal. Technical death metal played
in the background, with the sound turned down low. The growled lyrics
were less comprehensible than usual due to the volume. It didn't help
that they were in Greek.

It was my kind of place. The receptionist favored me with a knowing
smile as the door closed behind me. "Hello, Adversary Bradleigh. My
sisters and I suspected you'd eventually visit. You always pass by on
your way to work."

"Do I? I never realized."

The receptionist worked her terminal. "No matter. We'll start with your
nails once Lachesis is ready. Would you like something to drink?"

"That sounds perfect." I unclipped my sword from my belt and offered it.
"Do you want to hold this for the duration of my visit?"

The receptionist wrote out a tag, which she tied to the hilt of my sword
before putting it in a safe behind her. She then ducked into the back,
returning with two bottles of water. She offered me one. "Sorry. We
don't have anything else."

"Water's fine." The glass bottle was frigid in my hand as I drank. It
was just what I needed as I borrowed one of the tablets laying on the
table in the waiting area and checked the news.

I expected the lady working on my nails to chat, but she handled me with
a briskness that felt almost clinical. She did not speak unless
instructing me. She studied me with cold eyes as if measuring me.
Despite her brusque manner, she handled me gently and left my nails a
brilliant red.

She gave way to another woman, who dressed all in black and wore a
kindlier expression. Her touch was gentler than her predecessor's as she
led me to a chair, gathered my hair, and soaked it thoroughly before
working shampoo into it. "Do you know how rare your coloration is,
Adversary?"

"Snow-blonde isn't that rare a color in CPMD-positive people, is it?"

"Not your hair, dear. Your eyes. They mark you as an ensof's child, a
demifiend."

Demifiend? What the hell was she on about? Being called half-demon felt
like an insult, albeit a more original one than some I've heard. Nor did
the word ensof mean anything to me. Using my implant to run a search got
me bugger-all besides references to the Zohar and other elements of
Kabbalah, of which I knew enough that an explanation of where the
Society's ten AIs got their names was unnecessary, so I kept quiet and
let her work. Maybe she'd end up clarifying her remarks. Hope's even
cheaper than talk.

She massaged my scalp as she spoke, which felt so good I resolved to get
any lovers I took in the future to do it for me. "Some of our people
will despise you, like my sister Lachesis, but you don't get to choose
your parents."

Lachesis? The salon's name made more sense, but I wondered which of the
Fates would cut my hair as I changed chairs. I watched as the woman
tending me selected a pair of scissors. "I suppose you're Atropos."

She nodded. "Very astute, dear. No doubt you met Clotho out front. You
have lovely, thick hair, by the way. Have you given any thought to what
sort of style you'd like? Perhaps some layers or a bit of feathering to
give it more volume? Or would you prefer a more practical style that
will let you tie back your hair on duty?"

I was impressed Atropos would consider my duties, and not just which
styles would be most flattering. "I think I'll depend on your judgment."

"Will you, now?" Atropos smiled at me. "What if you don't like it?"

I shrugged beneath the smock she draped over me before washing my hair.
"It'll grow back. It always does."

"That's a rather philosophical attitude for a young lady." The shears
closed, and a lock of my hair fell free. I raised my hand to brush it
off, but she beat me to it.

Atropos was true to her word and styled my hair with a long, layered cut
that flattered my face. I paid Clotho, adding a hefty gratuity, and made
an appointment for next month before reclaiming my blade. I also got the
name of the album I heard playing. It was *Perpetual Titanomachia* by
Tartarus.

Unable to decide on a restaurant for dinner, I settled for an Agni
Burger before returning home. As I followed my lengthening shadow,
footsteps echoed behind me. Two men followed me at first. Two more
slipped out of an alley and joined them. After a block, I confronted
them. "Do you gentlemen have a problem?"

All four were in decent shape. Each of them wore a service gladius on
his hip and civilian clothing, which suggested they were off-duty cops.
They were rough, square-jawed men with massive bodies and thick,
grasping hands. The tallest stepped forward, a hand on his hilt. "You
that white-haired bitch who got a bunch of our friends from the East End
suspended without pay?"

They got themselves suspended through their inability to respect
individual rights, but it was unlikely these clowns could grasp such
nuances. "Your rudeness toward me isn't doing your friends any favors."

The leader glanced at his companions. "Lift the suspension. Now."

I used my implant to scan the street while messaging the Phoenix Society
to request backup. If I managed to deal with these fools on my own,
great, but a sword or two beside me wouldn't go amiss. "I lack the
necessary authority to rescind the suspension."

"I think you just aren't willing. Maybe you look down on us?"

I shook my head. This situation had begun to remind me of the elder
Dumas' romances. Was I a Musketeer standing alone against four of
Cardinal Richelieu's soldiers? "I think you're looking for an excuse to
escalate the rivalry between MEPOL and the Phoenix Society."

"Nah. We just think you're a stuck-up bitch who needs to know her
place."

I glanced at the speaker, who had begun circling to my right. "And you
think you're the men to teach me?"

"Oh, don't you worry about that." A cop circling to my left spoke. "I
saw you protecting those union leeches last night. Freaks like you
always stir up mobs. You don't have what it takes to stand on your own."

"Come on, guys. I'm a freak like her." The cop who had not spoken yet
spared me the necessity of belaboring the obvious. "This was a bad idea
from the start. She was just doing her job."

I nodded to him. "Thank you."

The other cops rounded on him. "Who the fuck are you trying to impress
with the white knight act, Carson? You're going to side with this harpy
because she's a pussycat like you? What the hell for? She's probably a
bloody lezzer."

Carson drew his gladius. "You said you just wanted to talk to her, but
now you're ready to start a fight. This isn't right, and you goddamn
well know it."

I sighed and drew my sword as well. Two against three was better than
one against four, but I would have preferred to settle this without
violence. "Gentlemen, we should all go home and get a good night's rest.
In the morning, you can appeal directly to the Phoenix Society. I won't
mention this incident."

A cry pierced the dusk, and Carson crumpled to his knees, clutching at
the stab wound in his belly. I speared one man through the shoulder and
spun to face his friends. Sidestepping a thrust from one of the
remaining cops, I slashed open his coat and left a bloody gash across
his chest.

The constable who first spoke to me picked up a fallen blade and came at
me with a weapon in each hand. I caught him in the belly with a lunge.
Hearing a snarl behind me, I spun to face the man whose chest I had
sliced. He glared at me while pressing his free hand against his wound.
"You murderous whore. I'm gonna―"

I pierced the tendon in his elbow, and he dropped his sword. "I haven't
murdered anybody, yet. If you get medical attention in time you'll all
live."

Sirens filled the air. Two ambulances, a MEPOL patrol car, and a Phoenix
Society staff van screeched to a halt beside us. I cleaned my blade and
sheathed it, turning my back on the fallen off-duty cops. As paramedics
triaged the wounded, I held up my empty hands and decided to get out of
London at the first opportunity. This was no place for a holiday.

Track 08: The Heavy - "Oh No! Not You Again!"
---------------------------------------------

Despite my resolve, I was unable to leave London for several days. Not
only was I obliged to wait for the Phoenix Society's official
determination that I had acted in self-defense when fighting those
off-duty arseholes, but the transit union strike had spread globally. To
top everything off, my period proved painful enough to prompt a visit to
my gynecologist, who removed my IUD for safety's sake. Good thing she
did; it turned out the device had begun degrading abnormally early.

I booked tickets for the first available maglev to New York, packed my
bag, and took it easy for a few days. Jacqueline and some of my artsy
friends came to visit, and we put on an impromptu, gender-swapped
production of that Scottish play with me as the usurper and Jackie as
Macduff. We performed in front of my building, made a hell of a racket,
and had too much fun to give a damn.

Jackie came with me to Victoria Station the next morning to see me off.
"You sure you're going to be okay in New York, Nims?"

"I went to school there, remember?" I patted the hilt of my sword. "I'll
be fine."

"Sorry, I forgot I was talking to somebody who took out three off-duty
constables without a scratch. Just don't do anything I wouldn't do."
Jackie winked at me.

There was little Jackie *wouldn't* do. For example, I caught her and her
vicar boyfriend in my kitchen sharing a three-way kiss with the actor
who played the role traditionally given to the usurper's wife in our
little production. "Considering what I saw last night, your admonition
gives me way too much latitude."

"Yeah, sorry about that. We were all a bit drunk."

I shrugged, not about to admit I lay awake imagining two men lavishing
their attentions on me because of the scene I witnessed. "It's not like
I found the three of you in my bed."

Jackie smiled. "We were tempted, but I figured you wouldn't appreciate
it."

"Thanks for being the voice of reason."

"See? Miracles *do* happen." Jackie glanced over her shoulder, and her
eyes widened. She grabbed my arm. "Holy shit. You wouldn't believe who
just showed up, Nims."

Because seeing was disbelieving, I looked toward the entrance. Oh, damn.
John was there with his fiancée and some slag who seemed to be hounding
them. Was she paparazzi? Did Jackie somehow arrange this, or was I being
paranoid? "Jackie, let's leave them alone."

"Hell no!" She pulled harder, dragging me along until we blocked John's
path. She gave John a slow, cynical once-over before turning to me. "I
can't believe you settled for this. How long did it take you to train
him?"

"Adversary Bradleigh!" He backed up a step in his surprise. Recovering
his composure, he turned to his companion. "I suppose I should introduce
you. This is my fiancée, Christine Pennington. Christine, this is―"

I flashed a smile at Jacqueline and offered Christine my hand. "I'm the
other woman."

John's expression was priceless. Christine stared, unsure what to make
of me. "I beg your pardon? Did you just imply that John cheated on me?"

Jacqueline studied Christine as if deciding whether she deserved an
explanation. "John took you for a test ride while still in a
relationship with my friend. As far as we're concerned, you're the other
woman, but Naomi's trying to be gracious."

John spread his hands as if appealing for mercy. "Ladies, I hardly think
this is appropriate."

"Shut up. I want to hear this." Christine turned back to me, ignoring
her fiancé. "Is your friend telling the truth, Ms.―"

"Adversary Naomi Bradleigh." I offered my hand again, and this time,
Christine shook it. "Unfortunately, Jacqueline's telling the truth. John
and I had dated for a year when he met you. After deciding you would
prove a tolerable wife, he came to me.

"Unaware of this, I let him into my bed. He asked me to be his mistress,
which was how I found out about you."

Christine tilted her head as she considered my explanation. For some
reason, she reminded me of an actress from a Jane Austen adaptation.
"So, let me see if I understand. He cheated on me with you after he
cheated on you with me."

I nodded. "Pretty much."

She smiled at me before catching John by his collar. For a moment I
thought she might kiss him. She did, catching him off guard as she drove
his balls back into his abdomen with a well-placed knee. He crumpled to
the floor, his breathless sobs barely audible, as she ripped the
engagement ring from her finger and dropped it on him. "I'd be within my
rights to keep this, but I want nothing of yours."

"Oh, this is perfect." The woman who had been stalking John and
Christine earlier spoke up from behind her camera. "Tell me, Adversary
Bradleigh, do you enjoy breaking up engagements between your betters?"

Jackie came to my aid again. "Bitch, *please*. Nims wanted to leave them
alone. Who the fuck are you, anyway?"

"Oh, I'm sorry. I'm Alice Talbot, from the London Social Register. And
you must be Adversary Jacqueline Russo. Does that vicar's congregation
know what you do with him at night?"

"It's none of their business, or yours unless you want to join in."

Talbot flashed a sly smile before turning to Christine. "Ms. Pennington,
can you offer some insight into what it's like to realize your
husband-to-be kept a CPMD-positive mistress from you?"

"I've no idea what you're on about." Christine glanced at Jackie and me.
"Adversaries, would you care to remind Ms. Talbot of our right to
privacy?"

I let my sword-hand hover over the hilt as if I were ready to draw. "Go
chase the White Rabbit, Alice. I heard that he takes turns with the Mad
Hatter servicing the Queen of Hearts. Surely that's the sort of
high-society gossip your readers crave."

Leaving Talbot to mull that over, we escaped into a café. Christine was
kind enough to do the buying. We chatted until the station AI pinged me.
"Adversary Bradleigh, the Tradewinds Atlantic Express is now boarding."

As I rose to take my leave, Christine offered Jacqueline and me her
card. I glanced at it before slipping it into a pocket. "What manner of
antiques are your specialty?"

"Weapons." Christine glanced at my sword while Jackie ducked into the
ladies'. "Is that a Nakajima Sidewinder Mark One?"

"I doubt it. It's a custom model." I drew the blade to display the
maker's mark. She didn't need to know about the pilot program to outfit
newly-sworn Adversaries with tailor-made swords. The elegantly rendered
column of hiragana read, "Forged for Naomi Bradleigh by Nakajima Kaoru."

"It's beautiful." The reverence in Christine's voice surprised me. "Do
you use this blade on duty?"

I shrugged. "Of course."

"I suppose I should have expected as much." The awe left her voice. I
sheathed my blade as Jacqueline returned. "Nobody wears a sword they're
not prepared to use in a fight. If you come across another piece,
however…"

I flashed back to Maestro's rapier, still hidden in my closet. "I'll be
sure to keep you in mind, Christine. Thank you for the coffee, but I
should go."

After a parting hug from Jacqueline, I boarded my maglev and stowed my
bag in the semi-private compartment's overhead storage rack with plenty
of time to spare. Settling into a plush leather seat, I was about to
crack open a paperback I grabbed from the station's lending rack when a
girl's voice startled me. "Holy crispy crap, Mom. It's Cecilia Harvey
from Last Reverie!"

An auburn-haired preteen stood in the aisle, staring at me. She wore a
bomber jacket over a purple dress speckled with white stars and little
black ankle boots. A plush Programmer Cat nestled in the crook of her
arm. Her mother put away their luggage and looked out from the
compartment opposite mine. "Claire, it isn't polite to stare."

"It's fine, ma'am." Claire took my words as permission to take a seat
across from me. "My younger brother is a Last Reverie fan. He says
Cecilia's a brave knight who loves her king, rescues him time and time
again, and―"

"No spoilers!" Claire covered her ears and stomped her foot. "It's not
fair. I never got to play enough of the game to see any of that for
myself."

God, she sounded like Nathan used to when I had managed to read an
installment of *The Continuing Misadventures of Programmer Cat* before
him. "I'm sorry, Claire. I didn't realize."

Claire continued to pout until her mother intervened. "Claire, the lady
apologized. What do we say?"

She sniffled, and looked at her mother before turning back to me. "I'm
sorry, too. Fuckdammit, that was rude of me." She brightened a bit. "Oh,
bollocks. I didn't even ask your name."

I offered the salty-tongued little fangirl my hand. "I'm Naomi
Bradleigh. Keep this to yourself, but I'm actually an Adversary. I snuck
out so I could have a holiday." Claire perked up and turned toward her
mother. "Holy shitballs, Mom. She really is a knight."

Her mother sighed. "I'm sorry. I keep trying to teach Claire to watch
her mouth. I just can't explain where she gets it."

"Your little girl reminds me of a friend of mine."

"Are her tits as big as yours?"

I smiled at Claire's long-suffering mother as she sighed and shook her
head. If Claire was this bawdy as a little girl, I doubted her parents
looked forward to her adolescence. Even if they could find a nunnery in
which to confine her, I suspected she'd corrupt even the most devoted by
sheer force of will and personality. "It's fine, ma'am. I'm not
offended."

She smiled at me, came over, and offered her hand. "I'm Lucy Ashecroft.
I suppose this will prove a long trip."

I shook Lucy's hand before glancing at Claire. She had settled beside me
with a laptop to play what appeared to be a game of global thermonuclear
war. Hopefully, it was just a crude simulation. The last thing I needed
was for New York to not be there when we arrived.

Track 09: Duke Ellington – "Solitude"
-------------------------------------

The journey to New York was hardly as long as Lucy Ashecroft predicted.
Which proved that Lucy didn't understand her daughter's fundamental
problem. The girl was lonely and related better to adults than she did
to kids her own age.

I could sympathize; I was little different. Neither of us had any notion
of how to be little girls, so we tried to fake it while masking our
impatience to escape childhood. I found my escape through music. I
suspected Claire would find hers through tech, considering how she had
grilled me with questions about Malkuth and the other Sephiroth once she
got bored with her game.

Beyond having booked passage and a couple of nights lodging in central
Manhattan, I had no definite plans for my leave. I had figured I'd hit
Midtown and find something to do after I checked in and dropped off my
bag. However, the events and attractions display in the Hellfire Club's
lobby cycled through its programming without catching my interest.

I didn't want to take a bus tour of Manhattan, being too familiar with
the city from my student days. Broadway offered nothing I hadn't seen
back home. My implant's memory still held photos of me and my fellow ACS
cadets at the Statue of Liberty and other tourist attractions. And I
felt too restless and energetic to wander the city's museums.

A sign outside the hotel bar caught my eye: 'Pianist Wanted.' I removed
the sign from the door, sat at the bar, and placed it before the
bartender. "I play, and I'm available tonight and tomorrow. Who should I
contact concerning an audition?"

The bartender studied me a moment before speaking with a voice made for
crooning. "The piano's behind you, miss. Show me what you've got."

I caressed the baby grand's keys before sitting down. It was a
pre-Nationfall instrument lovingly maintained and perfectly tuned. The
presence of such an antique in the hotel bar suggested a refined
clientèle. I tried some jazz, playing a few standards from memory before
beginning to improvise, and continued until I became conscious of the
bartender's presence beside me.

He seemed pleased with me. "I'll need you to play from six to midnight.
A hundred milligrams a night plus tips, and dinner before you start.
Sound fair?"

I checked the time. It was one in the afternoon. "Fair enough. Anything
else?"

The bartender nodded. "One more thing. Do you have anything formal to
wear?"

That's my reward for letting caprice guide me. "I'll have to buy
something. I suppose you'll want me to leave the sword in my room."

"I'll keep it behind the bar for you. Yell if you need it."

"I can live with that." My first gig since before I took the oath, and I
had nothing to wear. Nothing for it but to go shopping. An ankle-length
black dress with a sweetheart neckline at a boutique called Frigga's
Loom caught my eye. Since they didn't have it in my size, I paid extra
to have it fitted and fabricated within the hour.

I made a week's salary that night and double the next. Word must have
spread. The money meant less than the opportunity to perform in front of
an audience not comprised of family and friends. Playing for the bar's
patrons offered a thrill of power I could enjoy without guilt. Their
hushed attention was adoration, their rapt gazes―caresses.

When I was done, I longed for a lover who would adore me with more than
his hushed attention and rapt gaze. I spied several handsome men among
the patrons, but I couldn't bring myself to invite any of them to my
room. After playing my heart out, I wanted more from a man than a night
of pleasure, but this was not the time.

Instead of chatting, I claimed a stool at the bar and ordered a glass of
wine. I listened to a pair of women beside me discussing resettlement
efforts.

"I'm not sure why people are bothering to fill in the old towns between
New York and Pittsburgh instead of spreading out west, but I won't
complain."

"Plenty of prime farmland in between, especially around Clarion. Ever
been there?"

"No. You?"

"Last year for the fair. Some of the local rock bands are pretty tight.
Not sure I'd go back, though."

"How come?"

"A couple of people disappeared while I was there. They were visitors,
like me. The locals searched the woods, but eventually shrugged it off
and went back to their business. One of 'em turned up a week later, but
not the other."

"Sounds creepy. I'm surprised the Phoenix Society hasn't gotten
involved."

So was I. I reclaimed my sword from the bartender and returned to my
room for privacy and a change of clothes. After putting away my dress
and shoes, I called Malkuth.

The AI seemed surprised to see me. "Did you miss me, Naomi?"

"Yes, but I didn't call because I was lonely." I suppose I was flirting
a bit with Mal, but I doubted it would do any harm.

"Do tell."

"Can you provide any information on disappearances in a town called
Clarion? It's situated between New York and Pittsburgh."

Malkuth's presence faded. It was as if somebody had caught his
attention. I waited for a couple of minutes and was about to speak
before he refocused on me. "I'm sorry, Adversary Bradleigh, but you're
not cleared for any information related to the town of Clarion."

"What do you mean, I'm not cleared?" I was more curious than indignant;
I had never heard of an Adversary being denied access to information on
any grounds other than privacy rights. Talk of clearance smacked of
pre-Nationfall espionage thrillers.

Malkuth shook his head. "I'm not permitted to explain. Orders from the
Executive Council. Sorry."

"I understand. Sorry if I caused you any trouble. I just overheard a
conversation and got curious." Why would the Executive Council order him
to hide information about Clarion?

After disconnecting, I decided to nip back down to the hotel bar. The
businesswomen I overheard earlier had left, and the bar had emptied out
a bit. As I claimed a stool, a young man settled beside me and cleared
his throat. "Hello. I saw you play earlier. I still can't believe you're
real."

I smiled at him. He was a handsome kid, though his manner suggested he
was still a bit shy around women. "Thank you."

He looked past me. I discreetly followed his gaze to a table crowded
with youths egging him on. "They're my friends. I earned my degree
today, and they dared me to buy you a drink and hit on you."

"Congratulations. Perhaps I should get you a drink, instead. You seem
nervous." I smiled at him and gently touched his hand. "It's all right.
What's your name, anyway?"

"Cliff." He blushed, and looked at the bar. "How did you know?"

"I have brothers." I didn't mention that they told me tales of their own
amorous adventures to ensure I was forewarned and thus forearmed. "Also,
I'm an Adversary."

That got Cliff's attention. "No way. You're an incredible musician and
an Adversary?"

His awestruck expression reminded me more of Claire than of a newly
minted university graduate. "Your heart really isn't in this game, is
it?"

He shook his head. "I have a girlfriend, but she's visiting her family
tonight, and my friends thought I could do better if only I tried." He
smiled at me. "The thing is, I don't want to do better. I love Isabel."

I motioned the bartender over. "Have a drink on me, while I deal with
your friends."

Before he could object, I advanced upon his friends wearing the sauciest
smile I could muster. "I need to borrow Cliff for the night. You will
have to manage without him."

I returned to the bar with a little swagger and gently touched Cliff's
shoulder before whispering in his ear. "When you're done, come with me.
I'll sneak you out, and you can get away from those losers."

Track 10: Judas Priest - "Hell Bent For Leather"
------------------------------------------------

The bartender looked so heartbroken by my departure this morning that I
took pity on him and promised to stop for a repeat engagement before
returning to London. No doubt Jacqueline would insist he had fallen for
me, but I suspected he was more infatuated with the metric shitload of
money I helped him make.

I hit the streets wondering what I should do with my share of the
windfall. Investing was right out. I already did that with a chunk of my
Adversary's salary before I paid my bills. No way was I about to do
banal shit with money I earned on vacation.

What I wanted was something fun, something I could keep to conjure the
memories I would make by taking a trip to Clarion and poking around. My
curiosity was well and truly piqued by last night's conversation with
Malkuth, and I had nothing better to do. The question was how to get
there. Hopping a train to Pittsburgh and backtracking by bus was simple
enough, but ticket stubs made poor souvenirs.

A gang of bikers on restored gasoline-powered choppers rumbled to a stop
at the street corner. Their rides' idling growl muffled their laughter
and conversation. On closer inspection, the group looked a little too
clean cut. Instead of an outlaw biker gang, they were a crew of
weekenders trading business suits for leathers. A wannabe one-percenter
who just needed a woman half his age riding pillion to complete his
midlife crisis looked at me and called out, "Hey, sexy! Wanna climb
aboard and have the ride of your life?"

His catcall helped me reach a decision. It was time to fulfill a
childhood dream and get a horse of my own – an iron horse. I waved at
him. "Thanks, mate, but I think I'll get my own ride. Know a good
dealer?"

He didn't stick around long enough for me to finish my question but
peeled out with his crew the second the light changed. Bollocks to him,
then. If he was that impatient, I doubt he could have given me a halfway
decent ride anyway.

Not that I needed him. A cab advertising a Conquest Motorcycles dealer
in Hell's Kitchen drove past. Capturing the address with my implant, I
found the shortest route from my location in the Upper West Side and set
out on foot. It wasn't far, and I did promise Director Chattan I'd keep
up with my PT.

I stopped for coffee and a bagel at a delicatessen called Maimonides'
Deli, which was often full of old gentlemen arguing over chess in as
wide a variety of languages as the deli's selection of bagels. I used to
stop here every morning before classes, and I remembered the clerk. His
namesake was a famous philosopher. "Hello, Mr. Spinoza. It's been a
while."

Spinoza's dentures flashed as he smiled. "Medium black coffee and a
toasted everything bagel with plain cream cheese. Aren't you late for
class, Ms. Bradleigh?"

I laughed as I paid him. "I graduated a couple of years ago, and was
assigned to the London chapter."

"Ah! I remember now. You made a point of stopping in to tell me. Are you
happy?" He handed me my coffee and bagel.

Rather than answer, I tried my bagel. It was as good as I remembered.
The crunch of delicious sourdough topped with sea salt, poppy and sesame
seeds, and roasted onion and garlic contrasted with the slightly
salty-sweet cream cheese. The coffee was perfect and blacker than
Sabbath. "I'm content for now. Did you know I haven't been able to find
a decent bagel anywhere in London? You should encourage one of your
grandkids to come set up shop."

Mr. Spinoza chuckled. "You should come back to New York, then. I could
introduce you to my grandson. He sells motorcycles. He makes serious
money, and you could focus on your music."

His suggestion was such an old-fashioned sentiment for the end of the
twenty-first century that it seemed almost ridiculous, but he meant
well. "Does he sell Conquests here in Hell's Kitchen, by any chance? I'm
on my way to buy a chopper and ride west."

"I'll tell him to expect you."

Some new customers walked in, so I stepped aside to give them access to
the counter. "I'd appreciate that, Mr. Spinoza. It was good to see you
again."

"Have a good day, Naomi."

The rest of my walk was slow and pleasant as I ate my breakfast. Before
I knew it, I had arrived at Spinoza Motors with my half-finished coffee
still in hand. A man resembling Mr. Spinoza finished his conversation
with one of his sales staff before coming to greet me. "You must be
Naomi Bradleigh. Papa Baruch didn't tell me you'd be gorgeous. I'm Jacob
Spinoza."

"What did he tell you, Mr. Spinoza?"

"Just that I was to treat you right." Spinoza chuckled as he opened the
door to his office and beckoned me inside. "Said he didn't want me
delegating you to one of my staff lest they try to sell you on a
Vestal."

My imagination drew a blank as I tried to visualize myself riding a
Vestal. No doubt I'd look prim and proper riding such a cute little
scooter, but it wouldn't be me. Rather than follow him into his office,
I cut to the chase. "I appreciate your personal attention. Can you show
me your Conquests?"

"A Conquest?" Spinoza studied me for a moment. "Yeah, I can see it. You
know what? I've got a model that might be perfect for you out back." He
let the office door snick shut behind him and led me to a rear exit near
the garage's waiting area.

At least twenty Conquest Type C bikes leaned on their kickstands, parked
side-by-side. All but one was black, and indistinguishable from the
model shown in all of Conquest's advertising. Conquest Motorcycles only
made one type of motorcycle, and you could have it in any color you
wanted as long as you liked black.

The lone exception stood apart from the others. It sat lower, to caress
the road. The suspension looked capable of providing a smooth ride
across lunar regolith. Part of the frame had been cut away to
accommodate bigger batteries and a more powerful motor. Crimson paint
and polished chrome flashed in the sun, challenging me to mount up.
"It's gorgeous. May I try it out?"

Jacob produced a key fob and tossed it to me. "Of course. Mind if I ride
pillion?"

After we had returned from our test ride, I flashed my best stage smile
at Jacob while caressing the leather seat. "Tell me more."

He cleared his throat. "The NDA won't let me name names, but this was a
custom job for a rock musician you've probably heard of. He paid half as
a deposit but died in a helicopter crash a couple weeks ago. His estate
wouldn't pay the rest or accept delivery."

I could guess at who Jacob meant and its implication on the price
expected, but it wasn't germane to the discussion. The relevant fact was
that Jacob Spinoza had a custom job he wanted to move, and he would use
the implication of star power to jack the price up. "How much did he
owe?"

"He owed fifty grams."

Fifty? *Fifty!* Fifty grams when I got my coffee and bagel for two point
five milligrams in the middle of fucking Manhattan?! There was no way in
any hell imagined by humanity I was going to pay such an exorbitant sum.
I could buy three bog-standard Conquest Type Cs with money to spare for
lunch, tolls, trans-Atlantic shipping, and a down payment on a
three-bedroom house outside London for the price this slick bastard was
trying to extort.

I closed the distance between us and picked a bit of lint from his
jacket. "I hope you can offer me a better deal than that, Mr. Spinoza.
I'm willing to bet you turned a modest profit already from the deposit."

Jacob shook his head. "I'm sorry, Ms. Bradleigh, but I'm still five
grams in the hole. The battery and engine are also custom work. You can
cross five hundred kilometers in two hours before you need to recharge.
You can go even further if you don't go above a hundred and twenty an
hour."

"Ten grams sounds reasonable. Half of that is profit for you, and triple
your markup on a plain Type C."

Jacob mastered himself quickly, but I still caught the 'How dare she
insult me like that?' expression in the way his eyes tightened for just
a moment. I smiled at him and sweetened the deal. "Ten grams. Cash. And
I still need to buy a helmet."

Jacob shook his head. "I need at least fifteen."

"The hell you do." I stepped away from the chopper, my hand resting on
the hilt of my sword. "Seven and a half."

"That's less than your original offer!" Jacob was rather cute when
flustered.

"I can go lower. Don't tempt me." I circled the bike, taking a closer
look. "Whether you turn a profit on this deal is no concern of mine,
especially since you might be bullshitting me. The recently deceased
unnamable celebrity whose estate won't take delivery is an old con."

"Grandpa told me you were this sweet, innocent girl. You're staring me
down like you're ready to pull your sword on me."

He was still flustered, and still cute. But if he's going to drag the
kindly old man into this, it was time for the claws. "So, you thought
you could take advantage of me? Listen, asshole, I don't care if your
grandfather is God. Six grams is my final offer."

Track 11: Bruce Dickinson - "Devil on a Hog"
--------------------------------------------

I did manage to wrangle Spinoza down to six grams before he yielded.
Guilt at my harsh treatment nagged at me, but I suppressed it with an
effort; he was out to get the best deal he could, just like me. The bike
grabbed my heart with the first purr, but I wasn't going to admit it to
him. I needed him to think I was willing to walk out empty-handed.

Dropping a couple hundred milligrams at the accessory shop assuaged what
little guilt I felt at ramming such a hard bargain down his throat.
After all, I needed a helmet, boots, gloves, and a shoulder harness for
my sword. By the time I stopped for a rest on I-80 a hundred kilometers
west of New York, I felt pretty damn good.

The rest stop was an island of commerce carved out of the forest that
had encroached upon the interprovincial highway after Nationfall. I had
my choice of fast food between Agni Burger, Eight Immortals Buffet,
Apollo Coffee, and Borgia Pizza. At ACS we used to joke about how
anybody who called Borgia Pizza with a complaint ended up in the East
River.

It was necessary to get away from cities like New York or London to see
what Nationfall had done to the world, and I hoped that my generation
was smart enough to learn from history instead of repeating it. Damn
near everything fell apart in 2048 after the world's governments,
corporations, and organized religions started pushing psychiatric
nanotech called The Patch. They said it would fix humanity's problems.
They lied unless a close brush with extinction constituted a fix.

Mum and Dad don't talk about it, but they survived a nanotech-induced
zombie apocalypse as *little kids*. If I wanted to top that kind of
badassery, I think I'd have to arrest God for crimes against humanity
and drag his arse down to earth to stand trial.

I wasn't hungry, or inclined to epic feats of courage, so I ducked into
the ladies'. By some miracle of janitorial effort, the bathroom looked
clean enough to eat in. For a nominal fee, I could have rented a locker
and had a shower before resuming my journey―assuming I was too strapped
to rent a room for the night. The nearby motel even had a discount on
the honeymoon suite.

My implant notified me of an incoming call from Baruch Spinoza on my way
out. What could he want? Only one way to know. I pulled out my phone,
which connected to my implant and served mainly to prevent people from
thinking I was talking to myself, and sat down. "Hello?"

"Hello, Naomi. How d'you like your chopper?"

"I almost regret the way I bargained with your grandson."

Spinoza gave a wheezy chuckle. "Don't worry about him. He pocketed five
grams off that deal."

Dammit, I should have driven a harder bargain, but hearing Jacob didn't
make out as badly as I thought was a relief. "Good for him."

A sigh on the other end. "I guess you won't be meeting Jacob for dinner
when you come back to the city."

Me, date Jacob Spinoza? Sure. Right after Hell freezes over. Or the rest
of Hell, if Dante wasn't making it all up. "I think I'll just stop by
for my usual before I catch the maglev home."

I stepped outside after he hung up, and found another motorcycle
charging in the stall behind mine. Its chrome was dull, the front tire
worn, and one of the mirrors remained attached through a combination of
desperation and duct tape. The rider was equally disreputable. He
squinted at me through a haze as he smoked what had to be the fattest
blunt known to man.

The wind shifted as he studied me. Only the rifle peeking over his
shoulder kept me from dismissing him as a lecherous old stoner. It was a
distinctive weapon, and any Adversary who listened to scuttlebutt would
have recognized the sleazy-looking old biker carrying it. What the hell
was Edmund Cohen doing here?

He tapped the ashes from his blunt, careless of where the wind blew
them. "I can see why Malkuth wants a hardware upgrade."

"I'm not sure why that's any concern of yours, sir. You haven't even
introduced yourself."

The biker reddened as if shamed, held the blunt out to his side, and
bowed from the waist. "Sorry about that, Adversary Bradleigh. I'm Edmund
Cohen. Most of the shit your friends told you about me is pure slander,
I promise."

"Even the flattering things?" Not that I had heard much to flatter
Cohen. His saving grace was his skill with that Dragunov of his. He's a
good man to have at your back in a firefight or a pub crawl, but don't
lend him money or leave him alone with your girlfriend.

He flashed a handsome smile that made him resemble an espionage film
hero. "Especially those."

My guard was up because I was riding alone with only a sword for
protection, but Cohen's self-deprecating humor eased me a little. I
offered my hand. "I'd introduce myself, but Malkuth beat me to it. What
else did he tell you?"

"Just enough to pique my interest." Cohen glanced at his dashboard. "I'm
heading west as far as the I-81 exit. Mind if I ride with you a bit?"

"No harm in it, I suppose." Glancing over my shoulder as I mounted up, I
caught the old lech perving on me like I suspected he would. I was
tempted to blow him a kiss, but that was more Jackie's style. "Sure you
can keep up?"

I left him there, hitting the on ramp at the current recommended speed.
Aside from occasional RVs that I passed as they trundled along in the
right lane, I had the highway to myself. The wind played with my hair,
streaming it behind me as it pressed my sunglasses against my face.
Though it hurt a little and would surely leave marks, I didn't care.
Astride my Conquest, I was young and strong; no power on earth could
oppose me.

No power save Edmund Cohen. He finally caught up with me, and requested
a secure talk session. Though our engines were all but silent, using our
implants was still easier than shouting at each other over the wind.
«What do you want?»

«Malkuth told me you were interested in Clarion. Why?»

Hmm… He wasn't flirting, or pissing about with small talk now that we're
alone. Why was that? There was only one way to find out. «I heard about
some unsolved disappearances. I'm curious as to why nobody seems to give
a shit.»

«You know what curiosity did to the cat, right?»

«I understand the cat got better.» Time to try a gamble while I had the
old man's attention. Though I didn't know for sure that Eddie was on the
Executive Council, I figured implying that I knew might shake loose info
he would otherwise keep to himself. He certainly wasn't an Adversary or
a Director. «I also understand you're XC. Think you can tell me anything
about Clarion?»

«Sorry, but you're not cleared. In fact, I've got orders to persuade you
to spend your vacation somewhere else. Clarion isn't your problem.»

Catching sight of the road signs ahead, I opened the throttle and left
Cohen behind. «You're going to miss your exit, Eddie.»

«Shit!» He swerved to get onto the ramp, and I thought for a moment he
might lose control. «At least call Saul Rosenbaum at the New York
chapter for backup if you find anything!»

The session cut out. He must have used a near-field connection, implant
to implant, instead of the network. Who the hell did Cohen think I was,
anyway? Of course I would call the local office for backup if I found
something real in Clarion. I might have been too curious for my own
good, but I wasn't a demon-ridden idiot. Hell, I would probably call
Rosenbaum when I get there as a professional courtesy.

My sunglasses proved a wise purchase as the sun led me westward to the
Route 62 exit. Trafficnet advised me to take it, and to expect a rougher
road than I-80. Rougher was something of an understatement. Route 62 had
not yet been modernized, so it was nothing but faded asphalt with
freshly painted lines and black patches where maintenance crews had
filled in potholes. Network access was sporadic here, without the access
points the Interprovincial provided every hundred meters.

Horse-pulled buggies filled with Pennsylvania Deutsch families slowed my
progress every couple of kilometers. Having never shared a road with
carriages before, I decelerated to avoid spooking the animals.

The road emptied once I passed the last farm and drove into an old
forest threatening to encroach upon the highway. Because it was almost
too dark to see, I pulled over to the narrow shoulder to remove my
sunglasses.

A pair of deer mating in the middle of the road stopped me from
continuing right away. While I could ride around them, scaring them into
action, there was no predicting which way they'd bolt. It was too risky.
"Oi, Bambi!" The buck turned his head to gaze on me, but maintained his
position. "Did you two have to start shagging in the middle of the
bloody highway?"

Though it was all but impossible for the doe to have understood me, she
pulled free of her suitor's embrace. He remained, hard and frustrated,
as she bounded off into the woods. I snapped a photo and sent it to
Jacqueline with a message: "They grow 'em big over here." She'd get a
kick out of that.

The buck stared at me a moment before lowering his head to threaten me
with his antlers. Well, I suppose I did cockblock the poor bastard. I
turned on my bike's V-Twin emulation and revved the engine. The rumbling
growl of a gasoline-powered chopper shattered the silence, startling the
buck into bolting after his lost mate.

The forest eventually yielded to more farmland. Buggies pulled over to
let me pass as their drivers heard me coming. The Conquest purred
beneath me as I rode into Clarion at a bicycle's pace to avoid hitting
pedestrians.

One child saw me and pulled his mother's arm. "Mommylookit! It's Cecilia
Harvey on a hog!"

Oh, dear. Being compared to a videogame character was cute when Claire
did it, but it was getting old fast. Perhaps I needed a different
hairstyle. The mother turned to pay attention to her son, so I stopped
beside her. "Excuse me, ma'am. Is there somewhere I could stay
overnight?"

She pointed down the road. "Try the Lonely Mountain." Before I could
thank her, she led her son away to continue on her business. So much for
country hospitality.

Despite being a small town, Clarion's main street bustled in a manner
that made me a little homesick. They had everything here, even a nerd
shop called Kaylee's Shiny Games, Hobbies, and Crafts.

One of the windows at Kaylee's displayed a poster for the new edition of
Advanced Catacombs & Chimeras, a tabletop game I remembered from
university. Another window was devoted to coming soon posters for
computer games like *Nationfall: Final War* and *True Goddess
Metempsychosis III - Call of the Lightbringer*. The latter claimed to
include an artbook and soundtrack on vinyl with every copy, and seemed
to involve yet another demonic invasion of Tokyo. I swear... the place
must be accursed.

A plump brunette in overalls and a Pulsecannon t-shirt leaned against
the entrance while polishing some kind of game miniature that resembled
a grotesque porcine creature wielding a minigun in each hand. Her eyes
widened as I passed by. "Excuse me! Did you know you look just like―"

I pulled over so I could talk without holding up traffic. "Cecilia
Harvey? I get that a lot lately. It must be the hair."

"I was gonna say you look like Lady Frostmane. From the samurai movies
by Ryuhei Miyamoto? All you need is a katana and a kimono." My utter
ignorance of the work of Ryuhei Miyamoto must have been painted across
my face, because she stopped geeking out and approached. "Welcome to
Clarion. I'm Kaylee Chambers."

I offered my hand. "Naomi Bradleigh. Can you tell me where to find the
Lonely Mountain?"

Clutching her miniature in her polishing hand, she gave mine a hearty
shake. "Give me a minute to lock up and I'll show you the way. Nothing
like a beer after work, right?"

