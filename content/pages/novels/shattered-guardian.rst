Shattered Guardian
##################

:author: Matthew Graybosch
:summary: The lies we tell ourselves are the lies that define us. Morgan faces a trial for which no Adversary can prepare, and his friends won't be able to save him this time. He must confront the lies that define him if he hopes to protect everything he values.
:url: starbreaker/novels/shattered-guardian/
:save_as: starbreaker/novels/shattered-guardian/index.html
:og_image: /images/starbreakercollage.jpg
:modified: 2018-05-30 13:10

Part Three of *Starbreaker*
    The lies we tell ourselves are the lies that define us. Morgan 
    faces a trial for which no Adversary can prepare, and his friends 
    won't be able to save him this time.  He must confront the lies 
    that define him if he hopes to protect everything he values.


.. contents:: **Table of Contents**
	:depth: 2
	:backlinks: top
	
Dedication
==========

For Catherine, purr usual. It certainly took me long enough to get this
one going, didn't it?

Disclaimer
==========

The following is a work of fiction and contains content that may be
offensive, triggering, or inappropriate for certain readers. Any views
or opinions expressed by the characters in this novel are strictly their
own and do not necessarily reflect those of the author or the publisher.

Any resemblance or similarities between the characters depicted within
to living or dead persons in this world or any parallel world within the
known multiverse are either a coincidence; an allusion to real,
alternate, invented, or secret history; or a parody.

The stunts in this work were performed by trained professionals;
attempting them at home can result in property damage, civil or criminal
liability, personal injury, and premature death. 

If you find any allegory or applicability in the following novel, 
please consult a qualified professional for psychiatric evaluation 
and treatment.

Chapter One - Cemetery Gates
============================

In which a swashbuckling soprano in mourning is offered a new hope, and a
revenant turns his face from the truth...

Queens - Naomi Bradleigh
------------------------

.. session 1: 2018-04-17 08:00

Naomi had tuned out the cab driver's chatter soon after settling into the
plush leather seat and specifying her destination.  It was a habit she
normally tried to avoid; once a driver recognized her they tended to ask the
same questions in the same order.  It would have been easy to only
half-listen and give canned responses to starstruck strangers, but she had
decided to accept fame as a gift rather than a burden, and treat the people
whose interest in her and her work as a musician made her life possible with
actual courtesy.

It was hard to live up to that ideal tonight, she admitted to herself.  She
had put off this visit for months, but Claire had finally prevailed upon her
to come to New York, order two bouquets of black-tipped scarlet roses and
visit the Adversaries' graveyard.  "Even if Morgan is too dead to give a
shit," their friend Claire had said, "This isn't about him.  It's about you. 
You need to acknowledge his death, pay your respects, mourn your loss, and
move the hell *on*."

The cab's lights were all that held back the night as it slowed, leaning
into a turn, and Naomi caught a glimpse of faded granite stones.  Each
marked the grave of one of Nationfall's dead, the disposable heroes whose
vain struggles only prolonged the collapse.

The hiss of tires on wet asphalt ceased.  The mechanical back-and-forth of
the windshield wipers halted.  "We're here, Ms.  Bradleigh," said the
cabbie, who remained in his seat.  "Are you sure it's safe to be here alone,
in the dark?"

"Thanks for caring, but I brought a light and my sword.  I should be fine,"
said Naomi as she counted out money for the fare, and included a generous
tip.  "Will you await my return?"

"Sure.  Let me help you out." The driver muttered what was most likely a
curse in Yoruba as his head struck the doorframe.  He was still rubbing it
as he opened the door closest to Naomi and offered her his hand.  Taking his
leather-gloved hand in hers, she stepped out of the cab.  She stretched,
looked skyward, and wished the sky were clear.

.. session 2: 2018-04-17 17:17

Selene would have been full tonight, and her light combined with that of the
stars would have been enough to guide Naomi now that New York had gone dark
until the morning.  Instead, the flashlight she had purchased for this visit
would have to suffice.

Remembering the roses, Naomi turned back to the cab and found the driver
waiting with a bouquet cradled in each arm.  Taking one, she allowed him to
place the other in her grasp.  "Thanks again.  I shan't be long."

The driver shrugged and pulled a cigarette case from his pocket.  Naomi
caught a whiff of cannabis as he selected a joint and fired it up.  He
wouldn't be able to drive for at least an hour; the daemon monitoring him
and the cab would refuse to so much as let the motor start.  "Take as long
as you like."

*I'd rather not be here at all.* Naomi kept that to herself as she squared
her shoulders and approached the entrance.  Massive wrought iron gates three
meters tall barred her way, the arch inscribed with a motto that her implant
automatically translated from Latin to overlay in English: "You who prosper
in peace and plenty, disturb not our well-earned rest."

Unsure of how to request entry, Naomi thrust her flashlight into one of the
pockets of her burgundy wool overcoat and reached for the latch.

.. « »

A text message from the daemon overseeing the cemetery appeared in Naomi's
overlay.  «Cemetery visiting hours are 08:00 to 20:00.  Return tomorrow
morning.»

Naomi bared her teeth, even though the daemon was not there to see her
display of wrath.  «I am Naomi Bradleigh, Adversary Emeritus and still sworn
to the Phoenix Society's service.  Who are *you* to order me about?»

«You may call me Cerberus, Adversary Emeritus.»

That figured.  It seemed to Naomi that every daemon set to manage a cemetery
took the name of a god or monster associated with the various underworlds. 
«I came to pay my respects to fallen comrades.  Must I disturb Malkuth to
tell him you're being a bad dog?»

«But I'm a good dog.  A *good* dog.»

«Prove it, please.  Let me in, so that I can visit my dead discreetly and
not have paparazzi baying at my heels.»

.. session 3: 2018-04-18 12:13 -- Join following paragraph to line of
.. dialogue above.
.. session 4: 2018-04-20 12:08 -- revising text from session 3 and
.. continuing

A whir of machinery broke the silence, followed by the clicks of locks
disengaging.  Naomi reached for one of the gates, sure she would have to put
her back into opening.  She was sure the gate would creak and scream for
lack of oil.  Instead, the gate receded at her touch, silently opening
inward as if the hinges had been oiled before the caretakers went home for
the night.

Granite stones in rank and file shaded by trees on the verge of bursting
into leaf stood sentry on either side of the brick-paved path Naomi
followed. Lesser, narrower paths branched off at intervals, but Naomi
ignored those. A map appeared in her overlay, showing her current position
and providing guidance to the inner necropolis where Adversaries who had
served honorably were buried regardless of how they died.

Even if she had lacked the technology to follow a map drawn by a tiny
computer implanted in her head, Naomi would have had no trouble finding her
destination. A lone flame flickered in the distance, visible through the
tree branches. Passing a breach in a stone wall that separated Nationfall's
dead from the much smaller plot dedicated to the repose of the Phoenix
Society's sworn swords, Naomi stood before another gate. 

This one bore a shorter but more ominous Latin motto: *Mors Omnibus
Tyrannis*.  As before, Naomi's implant provided a translation in the
overlay, but she didn't need it.  Despite the emphasis on due process,
"death to all tyrants" was the Adversaries' purpose.  

Naomi passed the gate as it opened before her and checked her surroundings. 
She was definitely in the Adversaries' graveyard now; the headstones were
all basalt rather than granite, and devoid of any hint of religious
symbolism.  Whatever faith they had held in life was irrelevant; whether
they had fallen in the line of duty or not they had dedicated themselves not
to their chosen gods, but to the ideals of liberty, equity, and justice for
all.  As such, every stone bore the sword, serpent, and scales emblematic of
the Civil Liberties Defense Corps.  In place of an epitaph, every grave bore
the same Latin motto Naomi had seen over the gate.

Naomi turned off the miniature map in her overlay.  The flame was closer
now, her path to it direct.  Within minutes she stood before a massive
granite pedestal.  A heroically proportioned male figure hewn from basalt
reclined against a mountain crag, nude save for broken manacles and leg
irons.  He crushed a dead eagle underfoot, and in the palm of his upraised
left hand an eternal flame burned.  Naomi had not needed to read the
inscription or translate it from Latin to recognize the figure or the
allegory in marble before her.  Here stood Prometheus, the unconquered
lightbringer, watching over those who had given at least a part of their
lives to the liberation of their fellows.

Approaching the pedestal, Naomi placed one of the bouquets before the
massive foot Prometheus was not using to trample symbolic raptors,
retreated, and kneeled as if to pray. "For my fallen comrades," she
whispered. She remained kneeling for several minutes, but held the humanist
titan's stony, unseeing gaze.

It was one thing to kneel out of respect for her fellow Adversaries, but
Naomi would not do so with eyes downcast as if the sculpture erected in
their honor represented a god.  *What would Morgan think if he saw me
cowering before a mere statue of a god?*

.. session 5: 2018-04-21 - Time to break out the weird shit.

The statue winked at Naomi.  Before she could dismiss it as her imagination
or a trick of the light, its igneus jaw opened to speak.  "Such a brave
little demifiend to glare at me with such defiance in your eyes."

Half sure she had finally gone round the bend, Naomi left her roses on the
ground as she rose to her feet.  "I was just thinking that if you really
were Prometheus and not a figment of my imagination you'd appreciate a bit
of sass."

"Sorry to disappoint you, but I'm as real as the stone over your toyboy's
grave.  He died for nothing, you know."

*Maybe he did, but I'll be damned if I'll forgive this arsehole for saying
so.* A distorted bass hum set Naomi's teeth on edge as she drew her sword. 
A crystalline gray blade whose black veins pulsed with tenebrous light
caught the firelight and drank it in.  She raised the blade so that its tip
pointed toward the statue's face despite her sudden and urgent longing to
drop the loathsome weapon and flee.  "Why don't you come down here so I can
kill you?"

"What's this?  Another false Starbreaker?" The blade itself snarled as if
insulted, but the statue continued.  "No, not a false one this time.  You
know, little demifiend, Morgan might actually have killed me if you had
trusted him with your little secret."

Naomi's vision blurred as the realization struck home, and despite herself
she struggled to find the words to explain what had really happened.  Before
she could, the statue added, "Perhaps you never truly loved him.  Ah!  Could
that be the truth, that rather than loving him you loved the idea of him
loving *you*?"

"That is *quite* enough," said Naomi.  Springing onto the pedestal, she
thrust her sword into the basaltic breast of the idol through which the
presence taunting her spoke.  Cracks radiated through the stone from the
point of impact as the weapon's bass hum intensified.

"Go and pay your respects," said the statue, though it had lost its lower
jaw.  "I will reunite you with your lover soon."

"Wait.  What do you mean?" Naomi suspected the statue's last words were a
threat, but craved confirmation.  Before she could withdraw the weapon, the
statue crumbled into glittering dust.  Covering her nose and mouth to avoid
inhaling the dust that had once been a statue hewn from volcanic rock, Naomi
sprang back while remaining on guard.  The voice that had spoke through the
statue spoke no more.

Instead, Cerberus cut in on a restored connection whose interruption Naomi
had not noticed.  «I didn't let you in here so you could wreck priceless
statuary and pick fights!  Bad human!»

.. session 6: 2018-04-23 - Time for a viewpoint shift.

Brooklyn - Annelise Copeland
----------------------------

Life had been easy for Annelise Copeland since giving up her stage name and
her career in music.  While Isaac no longer came to her and played her body
with greater virtuosity than she ever brought to the violin -- or gave her
that drug by the name of World Without End that heightened her sensitivity
until the slightest breath upon her skin threatened to set her off -- he had
delivered on everything he had promised Annelise when he first asked her to
leave her life behind and become an actor in history.

She had never had to worry about money.  People bought the clothes she
designed and wore them in public.  Her little boutique in Brooklyn enjoyed
exactly the sort of discreet prosperity Annelise had come to crave, and she
no longer missed being a wizard's lover and spy.  She had been glad to leave
all of the weirdness behind.

*Naturally*, thought Annelise as she woke from a nightmare of flames and
voices in the sky that still occasionally recurred months after that futbol
match in Tikal, *the weirdness just had to come and find me again.*

Giving up on sleep, Annelise wrapped herself in a cotton robe.  Navigating
mostly by feel, she padded into the kitchen before turning on a light to
make coffee.

The screen mounted by the stove flashed an "incoming call" message, and
Annelise considered telling the building's daemon to dismiss it.  Instead,
she asked, "Who would *dare* call me at one in the morning?"

The daemon replied over the speaker in a soft, masculine voice because
Annelise had gotten her implant removed as soon as she had left Isaac
Magnin's service.  "Ms.  Copeland, it's Isaac Magnin from the AsgarTech
Corp--"

"I know who Isaac Magnin is.  Put him on."

"I would wish bid you a good evening, Annelise, but I suspect it has not
been for you.  Did I wake you, my dear?"

*He ignores me for months, but now it's 'my dear'?  Not likely.*
A glance at the screen showed Magnin in his usual double-breasted white
suit, wearing a pair of sapphire cufflinks she had given him as a Winter
Solstice gift.  "You didn't wake me, but I'm not in the mood for diamonds
and rust right now."

Magnin shook his head, and a small, sad smile Annelise could not recall ever
having seen before curved his lips.  "You resent my absence, then?  I'm
sorry; I had thought that you'd want some time on your own to remember how
to be yourself again and happy."

"I needed the time," said Annelise, and her voice slipped the leash, "But I
missed you too."

"We can't be what we were before."

"But I have nobody else with whom I can talk about the last decade of my
life."

Now Magnin's smile took a wry turn.  "I might be able to fix that, but you
should be careful what you wish for.  I need you to accompany me on a late
night graveyard visit.  I suspect Naomi will be there soon, finally paying
her respects at Morgan's grave, and I need to speak with her.  Having you
there with me might keep her from drawing her sword long enough to actually
listen to me."

"What if I'd prefer not to?"

"Then I can bring your understudy," Annelise shuddered at the mention of the
artificial being who had taken on her identity so she could escape the life
she built in Magnin's service, "but it might be better for everybody
involved if you came."

After a long moment, Annelise made her decision. "Can you pick me up?"

"A limousine awaits. Come down when you're ready."

.. session 7: 2018-04-23 18:25

An hour later, Annelise followed Isaac down the paths of a cemetary to which
she had once been denied entry on account of her civilian status.  The
ordinary soldiers' headstones had unsettled her because of their sheer
number, but it was the Adversaries' stones that sent dread worming through
her guts.  Though she lacked any notion of what the mottos enscribed on the
basaltic grave markers meant, she doubted it was anything uplifting about
awaiting one's friends and families in a better world than this one.

Lost in such thoughts, she walked into Isaac's left arm as she raised it to
bar her way.  "What's wrong?"

Without waiting for an answer, Annelise looked ahead to find a basalt statue
of a Greek god holding an eternal flame, and a tall, snow-blonde woman
standing before it with a naked sword in her hand.  "Look.  Naomi's just up
ahead."

At least, Annelise thought it was Naomi.  She knew few other women as tall
as the soprano with whom she had worked in a former life, and she recognized
the cut of the other woman's burgundy wool overcoat as being her own design. 
However, she did not recognize the sword she held.  "That *is* Naomi, isn't
it?"

Isaac nodded.  "Yes, but somebody else is there with her.  Somebody inhuman. 
Stand back, please."

As Annelise complied, Naomi sprang upon the pedestal and thrust her strange
gray sword into the statue's breast. Its jawbone unhinged and fell before
the rest of the statue crumbled into dust. Naomi sprang backward, and Isaac
chuckled.

"What's so funny?"

Isaac glanced at Annelise. "I never cared for that sculpture. I always
thought the artist was pussyfooting, trying for an inoffensive compromise,
but the unconquered lightbringer is not Prometheus. Prometheus submitted
to Zeus and *accepted* his punishment."

Not knowing what Isaac Magnin was going on about was nothing new for
Annelise, so she accepted that this was one of *those* conversations and
tried to go along with it. "Then who is the unconquered lightbringer?"

Rather than answer, Isaac waited for Naomi to leave before snapping his
fingers.  The dust whirled as if lifted by a breeze, but did not disperse. 
Instead, the dust coalesced into a statue of an angel with broken wings
staring silent defiance at the heavens with eyes of fire.  Annelise gazed
upon the statue, and thought she recognized it.  "Is that *Morgan*?"

"No, but close enough. Come along. Naomi won't be far off."

.. session 8: 2018-04-24

Queens - Naomi Bradleigh
------------------------

After enduring a lecture on the respect due the dead from Cerberus, Naomi
followed the daemon's guidance deeper into the Adversaries' graveyard to
find a plot set apart from the others by a circle of carpet roses. Two newly
planted saplings flanked a hunk of basalt thrice the size of other
Adversaries' grave markers, and bore the following inscription.

| Morgan Stormrider
| 2082 - 2113
| 2100 - 2112
| Weapon by design. Human by choice.

Below this, the memorialist responsible for this stone had carved the
majority of the Crowley's Thoth discography.  It began with the first album
Morgan and Naomi had written together, *Prometheus Unbound*, and ended with
their most recent rock opera *The Stars My Destination*.

The grass in front of the grave was littered with bouquets of flowers whose
scent fought a desperate rearguard action against the reek of cheap whiskey
somebody had poured out on the ground.  Somebody had left an autographed
copy of the long out-of-print *Prometheus Hits the Road* double live album,
as if Morgan might have needed it.

.. Salt burned Naomi's eyes in a flash flood of tears, and she dropped the
.. roses she had meant to place before Morgan's grave.  "You bastard.  You just
.. *had* to play the hero." 

.. session 9: 1 May 2018

"You bastard.  You just *had* to play the hero." Salt burned Naomi's eyes in
a flash flood of tears, and she dropped the roses she had meant to place
before Morgan's grave.  "I kept saying it would get you killed someday."

"That might have been what he wanted," said a once-familiar voice Naomi had
never expected to hear again.  She turned to face a slim brunette in an open
midnight blue wool overcoat over a navy suit.  Poised on spike heels, she
was almost as tall as Naomi, and though she taken on the drawn, haggard
aspect Naomi recognized in herself after too many sleepless nights and too
little to eat, there was no mistaking her gray-eyed arrogance or the band of
orange streaking one of her eyes.  "No doubt that was partially my fault."

Rage set Naomi ablaze, and her hand was on the hilt of her sword before
reason could assert itself and remind Naomi that the other woman was
unarmed.  "Christabel?  Is that you, or another hallucination?"

The other woman took a step forward.  "Of course it's me, Naomi, but please
call me Annelise.  When did you take up the sword?"

"At least a decade too late.  Would that I had had a blade handy when I
first saw you sniffing around Morgan."

"I suppose you're right to detest me," said Annelise, "How much did he
learn?"

"He learned enough," said Naomi, her eyes narrowing as a snow-blond man in a
white double-breasted suit approached. Naomi recognized him, and knew him
for what he was. Though as tall and gracile as a European fashion model, a
cruel and calculating power lay behind the dandy's mask, a power born of a
bitter heart that bides its time and bites.

The dandy glanced at Annelise, "Unfortunately, ladies, I must ask that you
continue your reunion in my limousine."

Though he wore no sword, Naomi drew hers.  Motioning for Annelise to get
behind her, she placed herself squarely in the pale man's path.  "Leave her
alone, Imaginos."

"I have done her no harm, nor do I intend any.  This I would swear by the
Styx, if such oaths carry any weight with you.  All I ask is that you come
with me, Naomi.  Morgan needs you."

.. session 10: 3 May 2018

AsgarTech Spire - Josefine Malmgren - 24 hours ago
--------------------------------------------------

*He's finally awake.* Though it meant that Josefine Malmgren's efforts had
proved successful, the thought brought no flush of pride. Instead of the
triumphant sense of accomplishment that Isaac Magnin and John Desdinova had
promised her, there was only the fear of having made a terrible mistake churning
in the pit of her stomach and the taste of acid creeping up the back of her
throat. Though the man beyond the door looming before Josefine had been friendly
to her, she could not help but suspect that he would be less kindly toward her
once he learned that her work had been instrumental in returning him from the
dead.

The door swung open at Josefine's touch as if it weighed nothing, revealing a
dark man with a lithe build and long black hair lying in a hospital bed. "Excuse
me, Adversary," said Josefine after clearing her throat.

He opened his eyes, and appraised her at a glance that reminded Josefine of a
cat sizing up possible prey. "Good evening, doctor--"

"Malmgren, Josefine Malmgren." She cringed at how easily the formulaic
introduction slipped off her tongue. "We've met before, though you might not
remember."

Morgan nodded. "Are you my attending physician?"

"I'm not that kind of doctor," said Josefine, her tone as firm as she could
manage. "Do you know where you are?"

Rather than answer immediately, Morgan got out of bed. Before she could protest,
he turned his back on her and indulged in a spine-crackling stretch that
rendered every muscle in his back, bottom, and legs in sharp relief. "Would you
mind telling me where I am, and what sort of doctor you are if not a physician?
When was I injured, and how long was I out?"

Josefine braced herself as if she expected to have to take a punch, and drew in
a deep breath. "You're in the AsgarTech Spire, in the Asura Emulator Project's
R&D Lab. My doctorate is in biomechanical engineering, though I've considerable
background in artificial psychology."

Morgan glanced at Josefine over his shoulder. "It seems you know what I am."

*Here's a delicate situation if ever there was one.* If Josefine's friend Claire
had been here, she would have warned Josefine to phrase her response with
exacting care lest she upset Morgan and risk permanently alienating him.
Instead, Magnin and Desdinova had made a point of advising her to handle
Morgan's ego gently. However, Morgan had not been the first asura emulator for
whom Josefine had had to play Whisperer. *But Polaris had been more volatile. At
least I know what Morgan wants to hear.*

Forcing herself to wear just the right sort of smile, Josefine answered Morgan.
"I know exactly what you are. You're a man, though human by choice rather than
by origin."

.. session 11: 5 May 2018 21:51

Morgan remained silent for a moment before speaking. "Are you afraid of me,
doctor?"

"No," she lied. "All right, just a little. Maybe I watched too many bad horror
movies with Claire, but I keep worrying that you may have come back wrong."

"I think you had better explain exactly what has happened to me," said Morgan.
"But first, some clothes. After that, perhaps we could get something to eat?
Unless your professional ethics forbid it, of course."

An hour later, Josefine found herself wishing Claire was here to see Morgan
wearing jeans and a polo shirt emblazoned with the AsgarTech Corporation's
rainbow bridge logo and slogan--"Building Bridges to Our Future"--as he
sauntered over to their table carrying a tray laden with food in each hand. For
Josefine, it just seemed strange to see Morgan dressed as if he worked here,
though she suspected Claire would capitalize on the unfortunate fact that
Morgan's shirt was a size too small and thus clung to him as if airbrushed onto
his skin.

The scent of spices preceded Morgan, making Josefine's stomach rumble as if
reminding her that it had been entirely too long since she last ate. "They still
had some jambalaya left?"

Morgan placed a tray with two heaping dishes and an oversized mug of coffee
before her. "The staff seem to know you. The cook made a point of putting 
together a fresh batch as soon as I told them I was with you."

Her eyes watered, and Josefine found herself glad she could blame the spices.
"I'm lucky they look out for me. I get so wrapped up in my work sometimes that I
forget to eat. Is it ever like that for you in the studio?"

Morgan speared a chunk of sausage and thoughtfully chewed for a long moment
before answering. "Sometimes, but I think it's fair to say I was built to
withstand a fast better than you can. That was part of the work your
predecessors here did, was it not?"

"You don't think I did it?"

"Dr. Malmgren, I am at least thirty years old. You would have had to have been
an infant prodigy, or far better at looking after yourself than you would have
me believe."

"It seems your reasoning capabilities are undiminished," said Josefine as Morgan
pulled at his collar. "I'm sorry they didn't have your size. I suppose you'd
prefer a Crowley's Thoth t-shirt in any event."

"Wear my own band's t-shirt?" To Josefine's surprise, Morgan actually seemed
appalled by the notion. He leaned forward, gesturing with his spoon. "It's
obvious you're not a metalhead, doctor. No musician wears their own band's
t-shirt, especially if they're headlining. Instead, you wear another
band's--preferably the warm-up act, to support 'em."

Now it was Josefine's turn to blush. "I only began listening to metal because I had 
started reading *Eddie Van Helsing*, and didn't get any of the references."

"Seriously?" Morgan shook his head. "That damned manga has been a pain in my ass
ever since its first issue."

"It's fun. I love the way Charlotte and Natalie keep Eddie in his place. And the
shows seem so awesome--as long as there aren't any vampires in the crowd."

That got a smile from him. "Yeah, there's nothing like a good concert. Ever been
to one?"

"Not yet. I was saving my Crowley's Thoth t-shirt for your next tour, but--"
Josefine caught herself. "Shit. I'm sorry."

Morgan shook his head, and busied himself with his meal for a while. When his
plate was half empty he said, "Only the really hardcore fans wear t-shirts for
the band they're going to see."

"Is that another one of those things I don't know because I'm not a real
metalhead?"

"I shouldn't have said that, and I apologize. Everybody starts somewhere."

"Where did you start?"

Morgan shifted in his seat. "This is a little embarrassing. The first band I
ever got into was Alcatrazz. I had this worn-out copy of *No Parole from Rock 'n
Roll* --"

"That sounds like something the author of *Eddie Van Helsing* would have made
up."

"I know, but it's the devil's honest truth. I played that album constantly. It
was the only one I had, and I didn't know any better. What was your first album
as a little girl?"

.. session 12: 2018-05-08 13:10

"Do you really want to know?" said Josefine, hoping Morgan would admit that he
had only asked for politeness' sake, saving her the embarrassment of admitting
that she had never cared enough about music to buy albums. *Though I should be
glad he's capable of having a friendly conversation. It might make it easier for
me to explain his condition if we've got rapport.*

Morgan leaned forward. "I would not have asked unless I was curious."

*Just make something up. He's obviously a hipster, so come up with something old
and obscure.* Using her implant, Josefine skimmed through the last hundred of so
songs she had streamed while working until she found something that dated more
than a decade before Nationfall? "Ever hear of an album called *Purple Rain*?

Morgan's eyes widened, "You mean the Prince and the Revolution album from 1984?"

Josefine nodded. "Yes. You've heard of it, right?"

Morgan flashed a smile that lit up his face. "I've got a copy on vinyl at home,"
he said before leaning forward and lowering his voice, "I had not figured you as
the album-collecting type. Did you go looking for an album to name-drop just to
impress me?"

"Yeah." Josefine looked at her plate and pushed around the remnants of her
jambalaya. "Was it so obvious that I was lying?"

Morgan sat back. "It isn't something I like to talk about because it's just
another quality that sets me apart, but as soon as you used your implant I could
*see* it. I must concentrate or my vision extends from the resonance of
electrons into the near ultraviolet. If my concentration had not lapsed, I might
not have seen you reach out to the network with your implant. If not for that, I
might not have realized you were trying to bullshit me to keep the conversation
going."

.. session 13: 2018-05-30

"If I were trying to bullshit you," said Josefine, "It was not to keep the 
conversation going. As for your explanation of your capabilities: try telling me
something I didn't already know."

Morgan held her gaze. "You first. Where's Naomi Bradleigh? How badly is she hurt? 
She was driving the car when whatever happened to me happened."

*He really is deluding himself.* "I'll get that informaton for you, but I need you
to understand something first. You weren't in an auto crash. You were not in a coma.
You were dead, and your psyche had to be restored from backup into a new body."

"Prove it," said Morgan, his voice 
