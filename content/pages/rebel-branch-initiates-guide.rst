The Rebel Branch Initiate's Guide
#################################

:summary: Spoiler-laden commentaries on the novels of Cynthea Masson
:url: rebel-branch-initiates-guide/
:save_as: rebel-branch-initiates-guide/index.html


It started innocently enough. I got an advance review copy of Cynthea Masson's 
*The Alchemists' Council* at the 2015 World Fantasy Convention. I read it. 
I enjoyed it. I found the author on Twitter and told her so.

Then I bought the novel. Twice, in fact, both the ebook and the paperback.

As I read it while working the night shift, I found myself taking notes. These 
notes became an unfinished commentary, a Rebel Branch Initiate's Guide.

* `The Rebel Branch Initiates' Guide to *The Alchemists' Council* </rebel-branch-initiates-guide/alchemists-council/>`_

I intend to finish the RBIG for *The Alchemists' Council*, so that I can move 
on to Professor Masson's next novel, *The Flaw in the Stone*. No doubt that
will get a RBIG as well.
