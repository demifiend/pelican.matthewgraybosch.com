About
#####

:summary: Who the hell is Matthew Graybosch, anyway?


I'm the `author </about/author/>`_ of several novels and short stories
that are all part of a saga called `Starbreaker </starbreaker/about/>`_. 
I'm supposed to be writing a new novel called 
`Shattered Guardian </novels/shattered-guardian/>`_, 
and I might finish it before Ragnarok.

I grew up in Sayville, NY and currently live in Harrisburg, PA with
my wife Catherine and several cats. I have a day job as a software
developer for a large consultancy, but I don't identify my employer
because everything I post online is *strictly* my own opinion.

.. image:: {filename}/images/author-nyc.jpg
    :alt: Matthew Graybosch atop Rockefeller Center in NYC. Photo by Catherine Gatt
    :align: center
    
