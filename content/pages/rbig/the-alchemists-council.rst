The Rebel Branch Initiates' Guide to *The Alchemists' Council*
##############################################################

:author: Matthew Graybosch, Cynthea Masson
:summary: A spoiler-laden commentary by Matthew Graybosch on Cynthea Masson's *The Alchemists' Council*
:url: rebel-branch-initiates-guide/alchemists-council/
:save_as: rebel-branch-initiates-guide/alchemists-council/index.html
:modified: 2018-04-19 00:30


It is my hope that the following commentaries will help illuminate
aspects of Cynthea Masson's *The Alchemists' Council* that might
otherwise remain occult. However, beyond this point lie spoilers. This
is the only warning you may expect to receive.

**The Alchemists' Council forbids you to read any further.**

.. contents:: **Table of Contents**
	:depth: 5
	:backlinks: top

**The Rebel Branch encourages you to continue, but the choice is yours...**


Introduction
============

*The Alchemists' Council* is a literary fantasy novel by Cynthea Masson
(`website <http://cyntheamasson.com>`__ \|
`twitter <https://twitter.com/cyntheamasson>`__), published by `ECW
Press <http://ecwpress.com>`__ of Canada in 2016. The first of a
trilogy, this novel explores the power of words and free will, and also
deals with the real-world issue of mass bee deaths due to pesticide
abuse.

Cynthea Masson currently teaches medieval literature and composition at
Vancouver Island University. She is the author of another novel, *The
Elijah Tree* (Rebel Satori, 2009), and is more recently one of the
editors of *Reading Joss Whedon* (Syracuse University Press, 2014).

    As a new Initiate with the Alchemists' Council, Jaden is trained to
    maintain the elemental balance of the world while fending off
    interference by the malevolent Rebel Branch. Bees are disappearing
    both from the pages of the ancient manuscripts in Council dimension
    and the outside world, threatening its very existence. Jaden
    navigates alchemy's complexities, but the more she learns, the more
    she begins to question Council practices. She realizes the Rebel
    Branch might not be the enemy she was taught to fight against. As
    the Council finds itself at the brink of war, Jaden must decide
    where her allegiance lies.

    — from the back of the 2016 paperback edition

Because *The Alchemists' Council* focuses on alchemical practice
inspired by real-world traditions from medieval and classical Europe and
Asia, readers unfamiliar with medieval alchemy may encounter
difficulties while reading this novel.

Readers should be aware that Professor Masson uses intrigue and politics
to drive the plot of *The Alchemists' Council* As such, the novel is
best suited to readers who also enjoyed Frank Herbert's *Dune*. The
heavy emphasis on organizational politics is a consequence of Jaden's
status as a Junior Initiate of the Alchemists' Council. As the newest
initiate, Jaden is still learning the ropes. The reader must therefore
learn the ropes with her.

Furthermore, the alchemical terminology may prove challenging to some
readers. While the novel isn't as reliant on scholarly prowess as
certain works by the late Umberto Eco (e.g. *Foucault's Pendulum* and
*The Name of the Rose*, having some background knowledge of alchemy that
didn't come from either *Fullmetal Alchemist* or Andrezj Sapkowski's
*The Witcher* may prove helpful.

Since I possess some of this knowledge, I've decided to compile a rough
reader's guide to *The Alchemists' Council*. I'll be working from the
2016 ECW Press paperback, and will cite page numbers when quoting
passages.

.. figure:: ../../images/cynthea-masson-alchemists-council.jpg
   :alt: Cover for the 2016 edition from ECW Press

   Cover for the 2016 edition from ECW Press

Prima Materia
=============

In the beginning, there was no Alchemists' Council. Such a thing was
unnecessary, for the Lapis and its ruby Flaw coexisted in perfect
harmony as the Calculus Macula (CM), sharing quintessence in a world
where everything simply existed without intention. This state of affairs
continued until somebody named Aralia became conscious of themselves as
a being capable of acting intentionally.

This Aralia subsequently got the notion that they were superior to all
others, and claimed the CM for their own. Another being, Osmanthus,
followed Aralia's lead. Naturally, the two opposed one another for two
beings possessed of ego and sure of their own superior can't possibly
coexist or share.

Matters rapidly degenerated from that point as other people became
conscious of themselves as individuals, chose sides, and went to war.
Their combat affected the CM itself, as Aralian victories shifted the
balance toward blue and Osmanthian successes pulled the balance toward
red.

Eventually Aralia and Osmanthus put aside their arms and reconciled,
becoming One again through the first alchemical Conjunction, but it was
too late. Nobody else was willing to relinquish ego and intention.
Worse, their chemical wedding *fractured* the world, the Prima Materia,
into three dimensions. The Aralian faction took one faction. The
Osmanthians claimed the other. They retained access to the CM and
understanding of the world's true nature. Everybody else in the third
dimension became the province of whichever faction controlled the CM,
while remaining ignorant of the true nature of the world.

Paying Attention? This Will Come Up Again
-----------------------------------------

On the immediate story level, this myth can be used to explain the
origins of the Alchemists' Council (Aralians?), the Rebel Branch
(Osmanthians?), and everybody else (muggles?). The third dimension is
the real world, and those who live there are mainly ignorant of the
alchemical machinations behind the scenes. A scholar might get a quick
peek behind the curtain, only to dismiss their insight as one brought on
by fatigue.

Digging deeper, a reader familiar with both the Judeo-Christian myth of
the Garden of Eden and the tenets of Buddhism may notice that Masson
used a synthesis of the two in the Prima Materia origin story. She makes
no mention of *sin* or of disobedience, but it is plain that Aralia
changed when they gained *intention*. Assuming this is a story that
initiates of the Alchemists' Council learn as an explanation for the
necessity of the Council's Great Work, it implies that the Council
frowns upon individual intention and free will.

Furthermore, the emphasis on conjunction, where the essence of one
person merges with that of another to create a single being where two
once existed, will recur throughout *The Alchemists' Council* and the
results of conjunctions between Council members can determine who lives,
who dies, and who gets initiated into the Council later on.

Familiar Names?
---------------

For some reason, the name Aralia reminds me of Aradia, a figure
currently important in Wicca and some neo-Pagan traditions. However,
Aradia originally appeared in the work of American folklorist Charles
Godfrey Leland, who published *Aradia, or The Gospel of the Witches* in
1899.

According to Leland, this book was a religious text belonging to Tuscan
covens who venerated Diana as the Queen of the Witches. No doubt Hecate
had somewhat to say about Diana muscling in on her turf, but gods
supplant each other all the time. Just ask Zeus about his dad Chronos.

However, googling the names Aralia and Osmanthus reveals that these are
the names of two plant genera. Aralia is a genus consisting of 68
species of trees, shrubs, and perennial herbs native to Asia and North
America. Osmanthus is a genus of 30 flowering plant species ranging in
size from shrubs to small trees, all evergreen. I don't know if Prof.
Masson was aware of this connection, but I find it interesting.

So, You Want to Be an Alchemist?
--------------------------------

The Alchemists' Council must occasionally replenish its ranks by
reaching out to an uninitiated individual whose presence their texts
foretold, and initiating them. Once initiated, an alchemist leaves the
mundane realm and takes up residence in Council dimension, where their
lives are extended through access to quintessence, the fifth element,
obtained by proximity to the Lapis. All members of the Council are
endowed by the Lapis with the ability to speak with one another and be
understood, and live in beauty and splendor.

However, they don't spend their time turning lead into gold, or creating
the Philosopher's Stone. Rather, the initiates of the Alchemists'
Council, who walk the line between science and magic, chemistry and
mysticism, work to maintain the elemental balance of the world and
ensure it remains hospitable to life.

A hundred and one initiated members constitute the Alchemists' Council,
though the number of actual members can vary due to conjunctions and
erasures. More on both later, but when either happens, the Council must
recruit new initiates from the mundane world into a pocket universe
referred to throughout the novel as "Council dimension".

Like any esoteric order, the Alchemists' Council possesses varying
degrees of initiation. In fact, the Council's orders roughly correspond
to `the hierarchy of the Hermetic Order of the Golden
Dawn <https://en.wikipedia.org/wiki/Hermetic_Order_of_the_Golden_Dawn#Structure_and_grades>`__,
which in turn borrowed the structure from the Societas Rosicruciana in
Anglia, who derived it from the Order of the Golden and Rosy Cross.

.. figure:: ../../images/rose-cross-lamen.png
   :alt: Rose Cross Lamen of the Golden Dawn

   Rose Cross Lamen of the Golden Dawn

One progresses through the ranks through study and accomplishment of the
prescribed work for one's rank, though an individual's progress can be
interrupted if an initiate conjoins with another, and their essence is
consumed. Likewise, should one transgress against the council, they may
be subject to erasure and permanent removal from Council dimension.

Prologue
========

After the introduction to the setting and explanation of the ranks of
the Alchemists' Council provided in Prima Materia, the Prologue touches
on events occurring five years prior to be beginning of the novel:

-  The conjunction of Saule
-  The search for initiation of Jaden
-  Cedar's betrayal of Saule
-  The debate over whether to release the Council's bees into the
   outside world.

The fact that Cynthea Masson chose to depict these events, one from
Saule's viewpoint and the other three from Cedar's (whose essence
consumed Saule's during their conjunction) suggests that Cedar is an
extremely important character, and possibly an antagonist of Jaden's.
The implication that Cedar somehow betrayed Saule during their
conjunction and that somebody named Sadira might have suffered as a
result further cements my impression.

The bee question is also important, and not simple. Cedar believes that
if the Council's bees are not released into mundane space to repair the
world, the outside environment will suffer catastrophic failures within
five years. However, Ruis insists that turning the bees loose would
cause irreparable damage to Council dimension.

However, the question of whether to loose the bees isn't for either of
them to decide. Nor will it be decided here.

What is Conjunction?
--------------------

Conjunction is an alchemical process in which the essence of two
individuals become one. The process is reminiscent to the Sacred
Marriage described in an allegorical romance entitled *The Chymical
Wedding of Christian Rosenkruetz*, the third manifesto of the original
Rosicrucians, a German philosophical secret society active in the early
seventeenth century.

When two initiates of the Alchemists' Council conjoin, their essences
combine and only one person remains. The result is variable; one
personality may dominate, or the person coming out of conjunction may be
a synthesis of the original two.

The act of conjunction is also a process of eliminating possibilities.
Consider a hypothetical conjunction between Alice and Barbara. Should
Alice's essence consume Barbara's, then Claire would be initiated while
Diane would die of natural causes, her potential unrealized. However, if
Barbara's essence dominates, it will be Diane who joins the council,
while Claire dies. While Alice and Barbara remain unconjoined, all
possibilities remain in play but unrealized.

Regardless of which personality dominates, or whether two equally strong
personalities form a synthesis, conjunction is Thunderdome: **Two
alchemists enter; one alchemist leaves.**

This is important because Junior Initiates in the Alchemists' Council
aren't ordinarily told the truth about conjunction. Learning the truth,
as Jaden does later on, will shape her character and drive her actions
in the novel.

What about Final Conjunction?
-----------------------------

As I understand it, the Final Conjunction is different from other
conjunctions. Rather than the essence of two alchemists combining into
one (with the attendant clash of personalities), the Final Conjunction
is one between the Azoth Magen (the eldest and highest-ranking member of
the Alchemists' Council) and the Lapis itself. In the Final Conjunction,
the Azoth Magen becomes part of the Lapis, surrendering their
individuality and returning Quintessence to the Lapis.

Just as in regular conjunctions, the Final Conjunction results in an
open position within the Alchemists' Council. A new Azoth Magen must be
chosen, most likely from among the Azoths, and so on down the ranks
until new Junior Initiates are recruited. The rise of a new Azoth Magen
also presages the beginning of a new Council, as I understand it. At the
moment, Ailanthus presides. Whether that remains the case is an open
question.

Chapter One
===========

The following matters are of particular interest in the first chapter of
*The Alchemists' Council*. Covering them will take us from page 11 to 66
of the paperback.

-  The Missing Lapidarian Bees, Part 1
-  The Next Conjunction
-  Jaden's Resentment
-  The Recruitment of Arjan
-  Sephrim: the Alchemist's Little Helper
-  The Care and Feeding of Alchemists in Council Dimension
-  Playing Telephone with the Lapis
-  Erasure and its Consequences
-  Giving Schrödinger's Cat a Belly Rub

The Missing Lapidarian Bees, Part 1
-----------------------------------

Novillian Scribe Cedar reports to Azoth Ruis that Junior Magistrate
Linden observed the *disappearance* of bees from the alchemical
manuscript *Ruach 2103*, `folio <https://en.wikipedia.org/wiki/Folio>`__
51 `verso <https://en.wikipedia.org/wiki/Recto_and_verso>`__, which is
located outside of Council dimension in a library in the Council's
Vienna protectorate. It is important to note that Cedar is not talking
about bees from the Council's apiaries, or bees in the outside world.
Rather, the bees in question are part of illuminations within the
manuscripts.

Furthermore, the illuminations are not mere artistic embellishments, as
they were in medieval manuscripts, Bibles, and Books of Hours. The
illuminations of alchemical manuscripts are part of the text. Altering
them alters the manuscript, and the meaning thereof. Furthermore, Cedar
seems to think the bees are leaving on their own, which implies that the
manuscripts are *changing themselves*.

Considering that the Alchemists' Council bases its actions on the
Readers' interpretation of the manuscripts, this change doesn't bode
well. Linden suspects the involvement of the Rebel Branch, a concern
Ruis dismisses, thinking it beneath them. He instructs Cedar to seek
further evidence, which she finds by comparing her notes on *Sursum
Deorsum 5055* folio 63 verso, which she compiled as a Senior Initiate,
with the original manuscript. Compared to her notes, the original is
likewise missing bees.

As a result of Cedar's reports, the wider Council has taken up the
matter of the bees again, which we learn from Amur as he blames Cedar
for getting bees on the agenda. In addition, the Council returns to the
question of whether to release the Lapidarian bees into the outside
world to compensate for `colony collapse
disorder <http://www.ars.usda.gov/News/docs.htm?docid=15572>`__ that the
author first introduced in the Prologue.

I suspect Cedar will prove an antagonist, but further evidence is
required. She strikes me as the manipulative sort, since she notes
Amur's attraction to her, but regards him as a sacrifical lamb and means
to use him. The question is, against whom will Cedar use Amur? Azoth
Ruis seems a likely target, given this line on page 18:

    As for Ruis, she had loved him once, had welcomed his power, but had
    more recently observed his abuses.

More on Cedar later and the bees later.

The Next Conjunction, Part 1
----------------------------

The next alchemical conjunction will be between Novillian Scribe Amur
and Senior Magistrate Sadira. More on this after we discuss Jaden.

Jaden's Resentment
------------------

We learn about the participants in the upcoming alchemical conjunction
through Jaden, the protagonist who had thus far remained off-stage. She
has a small personal stake in the conjunction's outcome; she would
prefer that Sadira be the one to survive, and not Amur, because Sadira
is one of the few Senior Magistrates whose lessons Jaden finds
tolerable.

Nor is Jaden particularly interested in the bee issue; she recalls that
the last time the subject came up the Council debated for over two hours
with no result. Furthermore, she notes that the bees could all just
disappear while the Council deliberates.

It bears mentioning that Jaden isn't her real name. Before Cedar
approached her she was a university student in Vancouver. She had lived
and studied there for two years, had no immediate family, and wasn't
especially close to her extended family. Aside from the friends she made
at university, she was alone in the world.

As Jaden herself noted when Cedar first sat down at her table in the
Vancouver Art Gallery's café, she was an ideal target for recruitment
into a cult. Furthermore, now that the thrill of knowing her existence
was foretold by the Nahzin Prophecies has faded, the Alchemists' Council
closely resembles a cult.

While Council dimension is beautiful and Jaden lives in comfort with
privileges beyond her reach in the outside world, she is also subjected
to alchemically induced vapors meant to ease her transition away from
her old life. Nor is she permitted to leave Council dimension or
communicate with her old friends. Instead, as described in the *Orders
of the Alchemists Council*, she is expected to *purge* herself of
outside influences so she may better engage with the Great Work.

In all honesty, this would remind me of a cult even if Jaden had not
been savvy enough to note the similarities herself. As it is, it calls
to mind `"A Touch of
Blessing" <https://www.youtube.com/watch?v=TQ8PXXrCasE>`__ by Swedish
melodic metal act Evergrey's 2004 album, *The Inner Circle*.

| Misled by beauty, one you rarely find...

.. raw:: html

   <div class="center">
   <iframe width="640" height="360"
   src="https://www.youtube-nocookie.com/embed/TQ8PXXrCasE"
   frameborder="0" gesture="media" allowfullscreen></iframe>
   </div>

| All the dreams I had, all my future wishes
| Put aside for a greater journey
| All the things I planned, left my friends so coldly
| Put aside for a higher...

In addition to the restrictions on movement imposed on Jaden and other
Junior Initiates, her status is currently that of a mushroom. The Elder
members of the council keep her in the dark and feed her bullshit,
rather than giving her honest answers to her questions — especially
about the Council's recruitment methods.

Instead, Jaden is encouraged to view the alchemical manuscripts as the
Word Eternal. "From the Lapis to the Scribe; from the Scribe to the
Reader," is the formula quoted to her on page 23. However, an observant
reader may notice that this very aphorism shows that the Lapis'
relevations are not directly shared with all members of the Alchemists'
Council, but are potentially filtered by at least two human minds.

Because the Council is so tightly bound by their interpretation of
alchemical manuscripts as revelations from the Lapis, Jaden is
determined to become a Scribe herself and interpret Lapidarian visions
in a manner that suits *her*, so that she might regain control of her
destiny. She is determined to do so despite Cedar's warnings that the
Law Codes governing the Council's actions do not permit self-interested
interpretations of either Lapidarian visions or the manuscripts, and
that those permitted to rise beyond Initiate status are permitted
because they are sufficiently indoctrinated to refrain from
trangression.

The Next Conjunction, Part 2
----------------------------

After our introduction to Jaden, who I personally suspect to be a Rebel
alchemist in the making, we meet the other two Junior Initiates, Laurel
and Cercis. Jaden dislikes them both, finding them "obnoxious and
intelligent" (page 23). Even worse, they're lovers and not at all
discreet about it, since they flirt and clown around in class. Rather
than deal with them most of the time, Jaden would either plot her escape
or (if Sadira was teaching) actually engage the alchemical text under
discussion.

Even worse, to Jaden's view, Cercis (and most likely Laurel) are eager
to climb the ranks. Cercis regards the upcoming conjunction between Amur
and Sadira with interest, because regardless of who prevails people will
get promoted to fill open positions. Cersis is a good little corporate
climber, and wants to make Senior Initiate as soon as possible.

Jaden, being more cynical, observes that it's possible for the
conjunction to fail, citing the failed conjunction between Cedar and
Ruis as precedent. Cercis counters by mentioning Ilex and Melia, lovers
who conjoined during the 17th Council, and expresses a hope that he will
similarly conjoin with Laurel, and subsequently rise to Azoth with her
essence part of his.

Poor Laurel. I wonder if she knows what sort of plans Cercis has
concocted. Would she willingly sacrifice herself to his ambition?

Sadira herself is concerned about the upcoming conjunction, as we learn
in the next scene on page 28. While she's waiting to meet Arjan outside
the `Blue Mosque of
Tabriz <https://en.wikipedia.org/wiki/Blue_Mosque,_Tabriz>`__ in Iran,
Sadira considered the possible results of her joining with the Novillian
Scribe, Amur.

Though Sadira has taken certain precautions at Cedar's behest, her
understanding of Council history suggests that when a senior member of
the Council is paired with a junior member, it is the senior member's
essence that dominates and subsumes the other. This contradicts official
Council doctrine, but Sadira has thus far kept her doubts to herself.
However, she suspects that she might not survive the Conjunction.

The Recruitment of Arjan
------------------------

I can't prove it yet for lack of textual evidence, but I think Cynthea
Masson wrote Arjan's character as a
`foil <https://en.wikipedia.org/wiki/Foil_(literature)>`__ for Jaden.
While Jaden was ignorant of the Council's existence and of alchemy prior
to her conscription by Cedar, Arjan not only possesses some experience
with mundane alchemy, but has seen his name mentioned in an alchemical
manuscript while "reading of the elemental qualities of *Terminalia
arjuna*" (page 30).

`*Terminalia
arjuna* <https://en.wikipedia.org/wiki/Terminalia_arjuna>`__,
incidentally, is a tree native to the Indian subcontinent. Its leaves
are used in silk production and in Ayurvedic medicine, and plays a
symbolic role in Theravada Buddhism as a tree representing achieved
enlightenment. Furthermore,
`Arjuna <https://en.wikipedia.org/wiki/Arjuna>`__ is the protagonist of
the `*Mahabharata* <https://en.wikipedia.org/wiki/Mahabharata>`__, one
of ancient India's major epics (along with the *Ramayana*).

So, Arjan knows who he is. He's familiar with mundane alchemy. He knows
he is to be an initiate of the Alchemists' council, and is eager to
depart with Sadira.

Sephrim: the Alchemist's Little Helper
--------------------------------------

Upon her return to Council dimension, the first person Sadira seeks out
after reporting in and getting Arjan settled is Cedar, who is currently
in the lower archives hunting for more evidence of bees disappearing
from manuscripts to convince Azoth Ruis that Scribe Linden identified a
legitimate phenomenon. It soon becomes obvious that the two are lovers,
but both have more pressing concerns at the moment.

Sadira, for her part, is suspicious of how well Arjan took the
recruitment, but has no evidence to suggest that either one of the
Azoths made first contact, or that the Rebel Branch got to Arjan first.
It would make sense for them to do so, though, and Cedar is obligated to
ask because of the missing bees — which may also be Rebel shenanigans.

However, Sadira has no evidence of either Rebel or Azothian involvement
because it *is* possible for the uninitiated to obtain basic alchemical
knowledge by studying the right manuscripts. It just isn't probable,
especially in a Western society where
`scientism <https://en.wikipedia.org/wiki/Scientism>`__ (belief in the
universal applicability of the scientific method) is one of the dominant
ideologies.

The scene ends with Cedar slipping Sadira a brown packet of Sephrim, a
drug for alchemists. It appears that Sadira is a long-time user, and has
gotten the stuff from Cedar many times before, but she isn't sure where
Cedar gets it or from whom. Cedar herself offers no information about
her source.

While I have no textual evidence to back this hypothesis, I suspect that
Cedar's getting her Sephrim from a Rebel alchemist either in an
out-of-the-way part of Council dimension, or some neutral location.
Regardless, it's probable that Sadira's
`jones <http://mentalfloss.com/article/49625/how-did-jones-come-mean-craving>`__
will prove plot-relevant later on. It certainly gives Cedar a handle she
could use, were she of a mind to do so.

The Care and Feeding of Alchemists in Council Dimension
-------------------------------------------------------

Next is a brief interlude which begins with Jaden messing around in
class drawing naughtier versions of the Rebis, the alchemical
hermaphrodite, than the one shown below. That makes her something of a
student after my own heart.

.. figure:: ../../images/kuthuma-erks-rebis.jpg
   :alt: Kuthuma of Erks: the Rebis

   Kuthuma of Erks: the Rebis

Arjan joins the class, making a splash with a joke about the mystical
nature of conjunction that goes over Linden's head and `straight to
plaid <https://www.youtube.com/watch?v=NAWL8ejf2nM>`__. (Sorry. I
couldn't resist. Anybody who knows me knows I'll eventually work in a
*Spaceballs* allusion.)

With the Junior Initiates filled out, it's chow time. Instead of taking
her usual table, Jaden joins her fellow JIs; her interest in Arjan
currently outweighs her dislike of Cercis and Laurel.

As the new `FNG <https://en.wikipedia.org/wiki/FNG_syndrome>`__
succeeding Jaden (who has now been in Council dimension over a year),
Arjan is full of questions inspired by the surprisingly excellent food.
He doesn't know that the Alchemists' Council is served by a large
support staff of non-alchemists.

These outside contractors get paid in Lapidarian honey, created by the
bees living in the Council's apiaries. The Council recruits them from
the outside world, but does not necessarily *conscript* them as Jaden
feels Cedar did to her. Instead, the Council looks for experienced,
skilled people who are tired of life in the outside world and looking
for a way out. At least, that's what Laurel tells Arjan on pages 39-40.
There's no textual evidence to contradict that yet.

Lapidarian honey is a wondrous substance: it heals, fosters good health,
and extends the lives of non-alchemists who eat it. It thus works
similarly to the Elixir granted to Council alchemists once they turn
thirty. Combined with the the alchemical vapors of Council dimension,
which act as a sovereign antidepressant, Council support staff
supposedly live long, purposeful lives free of despair.

Instead of living on Lapidarian honey, Alchemists are granted Elixir
once they turn thirty. The Elixir dramatically slows aging, and becomes
more effective as an alchemist rises through the ranks and develops
their essence. It also heals injury and cures diseases, but alchemists
with life-threatening illnesses or diseases that the Elixir cannot heal
within a few days are placed inside special alembics within the
catacombs.

.. figure:: ../../images/star-wars-bacta-tank.jpg
   :alt: Bacta tank used in "The Empire Strikes Back"

   Bacta tank used in "The Empire Strikes Back"

These alembics work similarly to devices like the bacta tank used in
*Star Wars V: The Empire Strikes Back*, but might not necessarily look
the same. Thanks to the Elixir and the catacombs, it is highly unlikely
that an alchemist will actually *die*. Instead, there are only three
ways to permanently leave Council dimension: conjunction, Final
Ascension, and erasure.

Playing Telephone with the Lapis
--------------------------------

I had previously mentioned that messages from the Lapis pass through at
least two human filters before the Alchemists' Council acts on them.
It's actually *much* more complicated than that, as I'll show below.

First, consider Cersis' explanation of how Lapidarian ink is made on
page 47. Novillian Scribes are endowed with the ability to "bleed the
Lapis", scraping fine dust from it with a jewel blade, after undergoing
a ritual conducted by the Azoth Magen called the Blood of the Green
Lion. The dust is then mixed with channel water in a crystal bottle, and
stirred with a rod of pure gold. This creates Lapidarian ink of a random
color.

Attentive readers may recall page 5, where Cedar performs this process
and creates azure ink instead of the indigo requested by Lapidarian
Scribe Katsura. However, the creation of Lapidarian ink could be
considered a secondary function of the Novillian Scribes, since the
ability to bleed the Lapis must be granted by the Azoth Magen. Their
primary function is *sapientia*; by placing one hand on the Lapis and
another on a blank slate, a Novillian Scribe's mind and body becomes a
conduit through which the Lapis transmits messages to a temporary
medium.

The freshly inscribed slates are given to the Lapidarian Scribes, who
use them to draft new manuscripts using Lapidarian ink made using the
process above. Once the alchemical manuscript has been drafted, a
Novillian Scribe reviews and revises the text. The manuscript is then
brought to the Readers, who interpret the finalized text.

This process brings to mind a kindergarten game called
`telephone <https://en.wikipedia.org/wiki/Chinese_whispers>`__. A child
would whisper a message to their neighbor, who would pass it on until it
came to the last child in the group. The last child in the group would
then announce the message as they understood it. The fun was in
comparing the final message to the original, but the game also possesses
instructive value in that it shows how easily errors can creep into a
message passed between people.

To reiterate, a Reader gets information from the Lapis through one of
two channels:

-  Via three people:

   #. Alice touches the Lapis.
   #. Barbara transcribes Alice's slate.
   #. Claire reviews and revises Barbara's draft.

-  Via two people:

   #. Alice touches the Lapis.
   #. Barbara transcribes Alice's slate.
   #. Alice reviews and revises Barbara's draft.

In the first case, the Readers get messages from the Lapis three steps
removed from the source (Alice, Barbara, Claire). In the second, the
messages come two steps removed (Alice, Barbara, Alice), but the
original Novillian Scribe has the opportunity to alter the message they
originally sent to the Lapidarian Scribes. Either way, I think it's
probable that the Alchemists' Council has been acting on faulty
information at least part of the time.

Erasure and its Consequences
----------------------------

Everything we previously discussed sets the stage for the climax of this
chapter, a confrontation between Jaden and Cedar. As part of a lesson in
recognizing Lapidarian ink, Sadira tasked the Junior Initiates with
attempting to distinguish between Lapidarian ink and the ordinary
variety. Unable to do so by sight, Jaden attempted to do so by scent —
and spilled it all over a manuscript.

As a result, Jaden got summoned to Cedar's office for having rendered
illegible nine square centimeters of *Elementa Chemicae 5663* folio 26
recto. Nine square centimeters isn't much; it's only 1.4 square inches
for readers unfamiliar with metric units. You can see for yourself if
you've got a ruler handy. Just draw a square 3cm wide on all sides.

Furthermore, the Council possesses backups of *Elementa Chemicae 5663*
and other alchemical manuscripts used to teach initiates just in case
one of them gets a case of the butterfingers. However, restoring a
manuscript from the backups is a painstaking process that distracts the
Novillian Scribes from other duties, so naturally Cedar would prefer
that Jaden be much more careful in the future.

To that end, Cedar explains to Jaden that she destroyed a Lapidarian
section of an original alchemical manuscript that dates back to the 17th
Council. Considering that *The Alchemists' Council* is set during the
18th Council, this implies that *Elementa Chemicae 5663* is not a new
manuscript.

The manuscript is valuable because of its age alone, but Lapidarian
portions of alchemical manuscripts are *puissant*. Changing such texts
or the illuminations can alter Council history, obscure the identities
of future Initiates so the Council cannot find and recruit them, or
delete references to existing members of the Council — which could
change who they are or *erase* them.

Cedar doesn't want to be erased, not after having served the Council for
centuries and rising to the position of Novillian Scribe, though full
erasure is a complicated procedure that involves rooting out *every*
reference to a person in the manuscripts. Furthermore, erasing Cedar
would harm alchemists below Cedar's position.

Cedar drives home the consequences of erasing her on pages 57-58. If
Jaden were to take out Cedar, she would go down with her. So would
everybody else Cedar ever initiated into the Council. All would suffer.
Worse, the Flaw in the Lapis would grow for lack of Quintessence.

This makes *deliberate* erasure of any alchemist granted a pendant
containing Elixir a "nuclear option" for dealing with recalcitrant
members of the Council, one the Council would not use unless a member
caused sufficient trouble to convince the rest that matters had passed
the `Godzilla
Threshold <http://tvtropes.org/pmwiki/pmwiki.php/Main/GodzillaThreshold>`__.

It also makes *accidental* or *malicious* erasure of the kind Jaden
could have caused an *excellent* way to make enemies within the Council,
and most likely a good way to get a one-way ticket back to muggledom —
though as Cedar points out, Jaden need only ask if she wants out that
badly.

Giving Schrödinger's Cat a Belly Rub
------------------------------------

After Jaden learns on page 60 that she could return to her mundane life,
and thus still has a choice, she does not make an immediate decision.
Instead, she goes off on a tangent and asks Cedar why the Alchemists'
Council hasn't initiated every human being on Earth. Cedar's answer on
page 61 is thus:

    "We are the elite, Jaden. We are the Alchemists' Council. And our
    responsibility to the world is unfathomable to the uninitiated."

I don't know about you, but this raises my hackles. I could easily
imagine Charles Manson saying something similar to his Family, insisting
that they're the only ones who can bring Helter Skelter down on the
world.

Jaden, however, persists and says, "Expand the Tree. Save the World."

Unfortunately, it isn't so simple. Apparently the Lapis can only sustain
a Council of a hundred and one members (similar to the United States
Senate, when the Vice President acts as President of that once-august
body). When a pendant-carrying member of the Council is erased, that
person's essence is lost and can never return to the Lapis. While the
erased member is eventually replaced, this is not the preferred method
to create open slots for Junior Initiates.

The ideal method is renew the Tree through Azothian Final Conjunction,
where the Azoth Magen conjoins with the Lapis and contributes their
refined essence, or through alchemical conjunction. However, the
consequences of conjunction go beyond the participants despite my
earlier joke about conjunction being alchemical Thunderdome.

It isn't just a matter of two alchemists enter, and only one leaving. I
explained earlier that who gets initiated depends on who survives
conjunction, and my explanation prompted Ms. Masson to `"wonder if one
of the #CouncilCats is named
Schrödinger" <https://cyntheamasson.com/2016/05/17/reading-the-alchemists-council-part-1/>`__.

It's no joke, as Jaden learns on pages 62-66 after she asked Cedar
whether she had been through conjunction. Upon learning that Cedar had
conjoined with Saule, consuming the other alchemist's essence while
remaining herself, Jaden is outraged. She views the process of
conjunction as one of murder, and finally decides that she wants out.
She wants no part of a Council whose Readers suggest conjuctive pairings
that the Elders then approve, so that one continues to live through the
centuries while the other dies.

However, Cedar has one final revelation to offer on page 65:

    "Saule conjoined with me so you could live and renew the Council.
    Her death gave you the potential for eternal life as the new
    Initiate. If Saule had been victorious, you would have died on the
    day Saule and I conjoined."

Instead, it was a potential initiate named Taimi who died, her
possibilities consumed along with Saule's essence. It's a hell of a
guilt trip for Jaden, but that's the way the waveform collapses.

Chapter Two
===========

The climax of Chapter One left Jaden bearing a heavy burden of guilt
over the two deaths that opened the way for her induction into the
Alchemists' Council. Her determination to find out who opened the way
for Arjan will lead her to a revelation more shocking than those she's
already weathered. Buckle up for spoilers; I'm covering the following
topics in pages 67-119 of the paperback.

-  Erasure: Making an Unperson
-  The Missing Lapidarian Bees, Part 2: Grunt Work for the Junior
   Initiates
-  Arjan's Reaction and Promise
-  Sadira's Concerns About Arjan
-  The Missing Lapidarian Bees, Part 3: As Below, So Above
-  A Field Trip to Santa Fe
-  Searching for Pendants
-  Why Turquoise?
-  Broken Memories of Kalina
-  The Missing Lapidarian Bees, Part 4: A Ritual Interrupted

Erasure: Making an Unperson
---------------------------

As I mentioned before, learning that Saule died during alchemical
conjunction with Cedar and the potential initiate Taimi followed her
rocked Jaden, and left her guilt-ridden. Since Cercis and Laurel are
senior to Jaden, and she dislikes them anyway, the only Junior Initiate
with whom she can share the truth about conjunction and how new
Initiates gain admission to the Council is Arjan. She believes that
Arjan would share her outrage.

However, Jaden doesn't know who conjoined to open up Arjan's slot. She
attempts to gain access to the current century's Council minutes for
research, and explains to Scribe Obeche that she needs the information
for a history assignment.

Rather than tell her, Obeche asks Jaden what exactly she's looking for,
pointing out with no little pride his repute as an "archival
mastermind". His pause after Jaden asked about the most recent
conjunction should be an attentive reader's first clue that he's hiding
something. The next clue is that he cites the most recent conjunction as
that of Cedar and Saule, but that was the conjunction that led to
*Jaden's* initiation -- which Jaden already knows.

Jaden presses him, demanding to know who conjoined before Arjan's
initiation, and Obeche claims to have forgotten about Arjan as if new
Initiates -- especially those who found themselves mentioned in
Lapidarian manuscripts prior to recruitment -- were so common as to be
beneath notice. However, there's no dancing around the truth now. Obeche
admits that Arjan's place was opened by *erasure*, not conjunction.

Obeche justifies the necessity of erasure as necessary for the survival
of the Council, which is continually threatened by the actions of the
Rebel Branch, but that is cold comfort to Jaden. I'll explain why in a
moment.

As I explained in "Chapter One: Erasure and its Consequences",
alchemical erasure is the Council's preferred method for handling
recalcitrant members. The scribes root out every reference to the erased
person from the Lapidarian manuscripts, and the former Alchemist gets
booted out of Council dimension.

Alchemical erasure is not a new concept. The Soviet Union under Stalin
regularly made unpersons of members of the Communist Party who fell from
favor, such as Nikolai Yezhov.

.. figure:: ../../images/Voroshilov,_Molotov,_Stalin,_with_Nikolai_Yezhov.jpg
   :alt: Kliment Voroshilov, Vyacheslav Molotov, Joseph Stalin, and Nikolai Yezhov, going out for cocktails

   Kliment Voroshilov, Vyacheslav Molotov, Joseph Stalin, and Nikolai Yezhov, going out for cocktails

The word "unperson" comes from George Orwell's
`*1984* <https://www.goodreads.com/book/show/5470.1984>`__, the warning
against totalitarian socialism that became the unofficial playbook for
Anglo-American police and intelligence agencies. The Romans reserved
erasure for traitors, and gave us the Latin term `*damnatio
memoriae* <https://en.wikipedia.org/wiki/Damnatio_memoriae>`__: the
"condemnation of memory". Even the Pharaohs of ancient Egypt regularly
made unpersons of their predecessors, especially Ramses II, striking
their names from monuments and replacing them with their own.

However, the Alchemists' Council can do what real-world tyrannies
cannot: it can erase memories of its outcasts from the minds of
individuals within Lapidarian proximity. Even the identity of an erased
person is on a need-to-know basis, and those not trusted with this
information do not remember the names of erased friends within Council
dimension. If they carry pendants, then they cannot even remember the
erased when they return to the outside world.

Thus, Jaden now knows that *somebody* had been erased to open the way
for Arjan's recruitment. She doesn't know who, and is naturally
distressed. Imagine somebody you cared about was made to disappear, and
you weren't even allowed to remember their names.

Nikolai Yezhov thought *he* got a raw deal, but at least people
remembered him and restored his memory after the USSR fell.

.. figure:: ../../images/The_Commissar_Vanishes_2.jpg
   :alt: '"Nikolai Yezhov," said Stalin. "Who's that, comrade? Kliment, Vyacheslav, do you know who this Yezhov fellow is?" "Nope!" "Never heard of him, boss."'

   '"Nikolai Yezhov," said Stalin. "Who's that, comrade? Kliment, Vyacheslav, do you know who this Yezhov fellow is?" "Nope!" "Never heard of him, boss."'

The Missing Lapidarian Bees, Part 2: Grunt Work for the Junior Initiates
------------------------------------------------------------------------

After meeting with Jaden at the end of Chapter One, Cedar got the word
that Azoth Ravenea wanted to meet with all of the Novillian Scribes, two
Lapidarian Scribes, and to Readers in the North Library. This is another
talky scene, but along with Obeche's revelation to Jaden about the full
effects of erasure, it will drive the action of the rest of this
chapter.

Ravenea's concerned about the bees disappearing from Lapidarian
manuscripts in the Vienna protectorate, even though Ruis doesn't seem to
care. She means to do something about the situation herself since it
barely got a mention at the last Council meeting.

Lapidarian Scribe Katsura suggests that all Scribes and Readers focus on
investigation to find a definitive cause, but Obeche dissents. He thinks
the work is too difficult, too mundane, and would distract from the
concerned orders' normal duties to the detriment of the Council and
Council dimension. Finding new bees where they didn't exist before would
be enough of a pain in the ass, but disappearing bees? I suspect Obeche
is as interested in finding *missing* manuscript bees as I would be in
reverse-engineering a legacy system implemented in ten million lines of
COBOL code.

Cedar, true to what I still suspect is her form, pounces on the
opportunity to win points with Obeche by suggesting that even devoting
half of the Scribes and Readers to the job is problematic, since the
work would require that the alchemists assigned make extended visits to
the protectorate libraries located in the outside world. Instead, she
suggests that two groups composed of two Novillian Scribes, two
Lapidarian Scribes, two Readers, two Senior Magistrates, two Junior
Magistrates, and two Senior Initiates work in rotating shifts to
investigate the missing bees.

When Obeche questions Cedar on her suggestion, Cedar points out that she
herself found a missing bee in *Sursum Deorsum 5055*, in Council
dimension. (Incidentally, "sursum deorsum" literally means "Up Down" or
"Upside Down" in Latin.) This blows the hypothesis that it's just the
protectorate libraries under attack out of the water.

The only objection to Cedar's proposal comes from Obeche, who insists
that Lapidarian Scribe Amur not be sent on this mission because his
impending conjunction makes him a valuable target. He offers to go in
Amur's stead, which would settle the matter if not for an additional
proposal from Cedar.

Cedar's suggestion that all four Junior Initiates also be brought on
board results in several council members having kittens. (If you adopted
one, please post a pic on Twitter with the hashtag #CouncilCats and tag
@cyntheamasson.) Obeche is concerned that the Junior Initiates aren't
ready to handle such work due to their limited grasp of basic concepts.

Cedar counters Obeche's objection by framing her proposal as an
opportunity to not only train the Juniors and continue their
indoctrination to prevent a repeat of the "most recent debacle" (p. 78),
but give them practical anti-Rebel experience. The Juniors could work in
pairs, with each pair assigned to a different protectorate library with
a Scribe to escort them in and out of Council dimension.

While precedent for putting the newbies to work exists, there's still
one little problem. Without pendants, the Junior Initiates sent outside
will leave Lapidarian proximity. That means they may start to remember
the erasure Obeche mentioned to Jaden earlier, as well as the events
leading to it.

However, Cedar has an answer for this as well: interim pendants made of
turquoise infused with Lapidarian essence, which were last given to
Junior Initiates during the Second Rebellion. The essence-infused
pendants grant Lapidarian proximity for a limited time, and must be
recharged, so the Junior Initiates remain leashed to the Council. Though
this doesn't satisfy Obeche, Cedar manages to persuade everybody else.

Arjan's Reaction and Promise
----------------------------

Jaden finally gets the chance to tell Arjan about how conjunction really
works, but he does not react as she hoped/expected he would. Instead of
being as outraged as Jaden, Arjan remains sanguine about the necessity
of conjunction.

However, he suggests another possibility. Cedar (and Jaden) might be
wrong about Saule being gone. Something of Saule might still exist
within Cedar, so that the post-conjunction Cedar's personality is subtly
different from that of pre-conjunction Cedar.

This is something Jaden hadn't considered, and it doesn't match the
impression she got from Cedar. When she points this out to Arjan, he
suggests that the result of each conjunction differs, depending on the
participants.

With no counterargument, Jaden instead changes the subject and tells
Arjan that his initiation was made possible through erasure. Again, he
is much more sanguine about the revelation than Jaden had been. However,
it helps the Sadira had already told him. It also helps that Arjan
didn't know any of the people involved.

However, once Jaden makes Arjan face the possibility that the next
person to be erased may be somebody important to *him*, he agrees to
help Jaden. He counsels Jaden to be patient, and wait for an opportunity
to escape Council dimension.

Sadira's Concerns About Arjan
-----------------------------

Poor Sadira's got a lot on her mind lately. Arjan's foreknowledge of his
initiation thanks to his prior alchemical learning isn't the only
quality that makes him a person of interest to the Council; various
Readers have compiled "extensive evidence" (p. 88) of his significance.

However, Arjan isn't unprecedented. The annals of the 7th Council
preserve a tale of "a prophet who had forseen her alchemical destiny".
Years later, this person singlehandedly undermined a Rebel push
threatening Council control of the outside world called the Breach of
the Yggdrasil.

[!["The Ash Yggdrasil" (1886) by Friedrich Wilhelm
Heine](\ https://upload.wikimedia.org/wikipedia/commons/8/8d/The_Ash_Yggdrasil_by_Friedrich_Wilhelm_Heine.jpg)](\ https://commons.wikimedia.org/wiki/File:The_Ash_Yggdrasil_by_Friedrich_Wilhelm_Heine.jpg#/media/File:The_Ash_Yggdrasil_by_Friedrich_Wilhelm_Heine.jpg)

----

Yggdrasil, by the way, is the World Tree that holds in its boughs the
`nine worlds of Norse mythology <https://en.wikipedia.org/wiki/Norse_cosmology>`_ (Asgard,
Midgard, Vanaheim, Muspelheim, Nifelheim, Alfheim, Svartalfheim,
Jotunheim, and Helheim). It is also the tree from which Odin hanged
himself for nine days, sacrificing himself to himself to learn the
secret of the runes in the poem *Hávamál*, from the *Poetic Edda*.
Stanza 137 tells the tale:

| I know that I hung on a windy tree
| nine long nights, 
| wounded with a spear, dedicated to Odin,
| myself to myself,
| on that tree of which no man knows
| from where its roots run.

For those with a musical bent, the Swedish symphonic metal band Therion
also dedicated a concept album to Yggdrasil and the Nine World entitled
`*Secret of the Runes* <http://www.metal-archives.com/albums/Therion/Secret_of_the_Runes/417>`_.
Readers with Spotify accounts can listen using the widget below.

.. raw :: html

    <iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A22TlRcrCGqRIefG2HRxf3b" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

----

Whoever singlehandedly defeated the Rebel Branch during the Breach of
the Yggdrasil left a hell of a pair of shoes for Arjan to fill. Whether
he'll do so remains to be seen. In the meantime, Obeche is happy to
remind Sadira that since Arjan is still only an initiate, he can always
be erased. While he's correct, I doubt Sadira finds this reminder
helpful; no doubt it would reflect poorly on Sadira if one of her
initiates were to become an unperson, just as any misconduct on her part
would reflect badly on Cedar.

Speaking of Cedar: apparently Sadira had turned to Cedar for comfort
after Cedar's conjunction with Saule. Prior to this conjunction, Sadira
had apparently been close enough to Saule to desperately fear losing her
(p. 89). Afterward, Sadira had latched on to Cedar, who forged such a
strong and comforting emotional bond with Sadira that the younger
alchemist had been the one to make the relationship a more intimate one.

So, she has no doubts about Cedar. Instead, she continues to harbor
doubts about her future with the Council. Like Jaden, Sadira also
resisted the Council's indoctrination at first, and researched
dimensional space in hope of find a way to escape Council dimension. She
had hoped the tunnels beneath the Council grounds would offer a way out,
but they never did.

That old restlessness returned along with the memory, and Sadira deals
with it by finding her Sephrim stash (which she stores using the classic
device of a hollowed out book) and does a hit. As shown on page 91, the
drug doesn't just affect Sadira; it also appears to affect her pendant.
I think this implies that Sephrim isn't just a drug, but somehow boosts
or amplifies the essence held within its user and their pendant.

Either way, it leaves Sadira nice and purry while she waits for Cedar.

The Missing Lapidarian Bees, Part 3: As Below, So Above
-------------------------------------------------------

It's bee time again as Cedar hunts down Amur to ensure she can count on
his support. She found him in the lower archives, poring over a
manuscript. Next to him sat Obeche, flipping through magazines from the
outside world and doing research on colony collapse disorder.

Though Cedar thinks Obeche is just wasting time, no doubt due to their
frequent disagreements, there's a method to his apparent madness. Thus
far, the relationship between Council dimension and the real world could
be described by the formula `"as above, so
below" <http://www.themystica.com/mystica/articles/a/below_above.html>`_:
because the microcosm affects the macrocosm, actions performed in
Council dimension could affect the outside world. Were this not the
case, then the Council's efforts to maintain elemental balance would do
nothing to preserve the outside world.

However, Obeche has come to suspect that the cause of the missing bees
in Lapidarian manuscripts is far more ominous than mere Rebel
shenanigans. He thinks that the real culprit is colony collapse disorder
in the outside world. If he's correct, the disappearance of bees in the
real world is reflected by the disappearance of bees in the manuscripts.

Obeche blames this phenomenon on a second flaw in the Calculus Macula, a
dimensional fissure that allows the outside world, or somebody there, to
affect Council dimension and its protectorates. He hasn't mentioned his
hypothesis to the Azoths yet, nor has he found evidence of the "as
below, so above" phenomenon.

Thus Cedar comes to Sadira without accomplishing her original objective,
talking to Amur about the conjunction. Not that Sadira minds. She has
other uses for Cedar.

A Field Trip to Santa Fe
------------------------

The next day, Jaden gets sidetracked on her way to the North Library by
the sight of Laurel and Sadira chatting by a fountain. Cercis is there,
as well as Arjan and Cedar. They're about to make a jaunt outside, and
were waiting for Jaden to find them.

I'm surprised the Council doesn't keep pages on staff (paid in
Lapidarian honey, naturally) to find members of the Council and tell
them when and where they're wanted, but that might be one of the hats a
Junior Initiate gets stuck wearing for everybody else's benefit. Rank
hath its privileges, even in Council dimension.

In any case, the jaunt isn't just a bit of fun. Cedar will escort the
Initiates to Santa Fe, New Mexico for a test; those who pass will
"progress to the next stage of the venture", according to Sadira (p.
97). She doesn't provide any further detail than that, save that they'll
be using the Salix portal instead of Quercus because they're heading to
North America -- and that they'll need to bring layers to accommodate
temperature changes.

This is the very opportunity Jaden had been hoping to get, time outside
of Lapidarian proximity to recover her memories. However, the fact that
the order came from Cedar makes Jaden nervous. Unlike Sadira, the
younger woman doesn't trust the Novillian Scribe.

[![the San Miguel Chapel in Santa Fe,
NM](\ https://upload.wikimedia.org/wikipedia/commons/4/41/Santa_Fe_San_miguel_chapel.jpg)](\ https://commons.wikimedia.org/wiki/File:Santa_Fe_San_miguel_chapel.jpg#/media/File:Santa_Fe_San_miguel_chapel.jpg)

Searching for Pendants
----------------------

Jaden was first to return outside with Cedar's help, but Cedar left her
standing on a street corner in Santa Fe with instructions to wait. Wait
Jaden did for over thirty minutes, pacing outside the Loretto Chapel and
worrying that she had been abandoned in a foreign city without money or
documentation, until Cedar showed up again with Arjan in tow.

Cedar's explanation? An "impromptu meeting with Obeche" (p. 100). That's
all she told Jaden, and Jaden has little choice but to take the older
woman's explanation at face value, but *I* smell a Lapidarian rat. Given
the antagonistic relationship between Cedar and Obeche, I'm surprised
Obeche would want an impromptu meeting with her.

In any case, after Jaden tells Cedar she's tired of waiting on the
street, Cedar suggests that she and Arjan wait at the cafe in La Fonda.
She then returned to Council dimension, prompting Jaden to wonder if
muggles notice members of the Alchemists council appearing and
disappearing in normal space. "Apparently they don't", according to
Arjan (p. 101).

Since they're alone together, Jaden and Arjan start talking erasure
again. Arjan approached Sadira on the subject, but all he learned was
that the Council doesn't simply *dump* its outcasts back in the real
world. Instead, they are provided the basics they need to make new lives
for themselves: a place to live, an identity, and some cash. The more
Elixir an erased alchemist has ingested prior to being stripped of their
Pendant, the longer they'll survive outside. Meanwhile, the Scribes
manually and alchemically remove references to them from all *known*
manuscripts.

This raises the possibility that a reference to an outcast can survive
in manuscripts unknown to the Alchemists' Council, though it strikes
Jaden and Arjan as somewhat unlikely. However, I doubt it would have
come up if it wasn't somehow relevant. Keep an eye out in subsequent
chapters.

In the meantime, Jaden grows frustrated that the memories haven't
started to come back yet, but Arjan reminds her that it takes time away
from Council dimension. Furthermore, the restoration is temporary;
she'll lose them again when she returns, and different memories might
resurface the *next* time she leaves Lapidarian proximity long enough to
recall the erased. However, it's possible that Jaden had already
*written* about the erased person since she has an old journal in which
she hasn't written for some time.

Once everybody's gathered, Jaden pays for her coffee and Arjan's. Arjan,
in turn, buys her a pen and some paper while chiding her about not
taking advantage of her alchemical skills to create wealth for herself.
They follow Cedar to the restaurant in La Plazuela, where it finally
occurs to Jaden that Cedar has a *history*, and that Santa Fe might be
part of this history. The city might have meant as much to her as
Vancouver means to Jaden.

Once the orders are placed and the drinks served, Cedar makes a toast
with a special meaning to Jaden: "May you find here all that you seek"
(p. 107). She then explains the Initiates' mission in Santa Fe: they are
to find stones suitable for use as interim pendants, so that they can
help the senior alchemists research the missing bee effect in the
protectorate libraries. These stones must be capable of withstanding
Lapidarian infusion, and apparently turquoise works best.

[![an Anasazi freeform turquoise
pendant](\ https://upload.wikimedia.org/wikipedia/commons/2/2e/Chacoan_turquoise_pendant.jpg)](\ https://commons.wikimedia.org/wiki/File:Chacoan_turquoise_pendant.jpg#/media/File:Chacoan_turquoise_pendant.jpg)

Why Turquoise?
--------------

Cedar didn't explain why the Initiates should look specifically for
turquoise, but a bit of research reveals some possibilities.

Turquoise posesses some interesting chemical properties as a hydrated
phosphate of copper and aluminum. It never forms single crystals, but
instead has a triclinic crystalline structure. The vectors are never the
same length, and never intersect at right angles. Furthermore, turquoise
can only be dissolved in heated hydrochloric acid, and despite being
fairly soft compared to other gemstones (6 on the Mohs scale, slightly
harder than window glass), turquoise takes on a good polish.

While our name for the stone comes from *turque*, the French word for
Turk, because turquoise entered the modern European trade via the Silk
Road, humans have used turquoise to decorate objects and make amulets
since the First Dynasty thousands of years ago.

King Tutankhamun's burial mask is inlaid with turquoise. The stone is
also associated with the goddess Hathor, who was the patroness of
Serabit el-Khadim, where ancient Egyptians once mined the stone. In
Chapter 28 of the Book of Exodus, turquoise is one of the stones
inlaying the breastplate worn by the High Priest of the Jews. The
Persians used it to decorate just about *everything*, and the stone also
saw use in Mesopotamia and in China during the Shang dynasty.

[![The iconic gold burial mask of Tutankhamun, inlaid with turquoise,
lapis lazuli, carnelian and coloured
glass.](\ https://upload.wikimedia.org/wikipedia/commons/2/2d/Tutmask.jpg)](\ https://commons.wikimedia.org/wiki/File:Tutmask.jpg#/media/File:Tutmask.jpg)

In the west, pre-Columbian Native Americans used turquoise to make
amulets and for decorative purposes. Though the silver and turquoise
jewelry made by the Navajo and other tribes in the American Southwest is
of a more modern design, and made primarily for trade with outsiders, it
was once as sacred to them as it was to the Aztec and the Maya.

[![Turquoise mosaic mask of Xiuhtecuhtli, the aztec god of
fire.](\ https://upload.wikimedia.org/wikipedia/commons/2/24/Xiuhtecuhtli_%28mask%29.jpg)](\ https://commons.wikimedia.org/wiki/File:Xiuhtecuhtli_(mask).jpg#/media/File:Xiuhtecuhtli_(mask).jpg)

No doubt the author considered many of these factors when choosing the
recommended material for the interim pendants.

Broken Memories of Kalina
-------------------------

Cedar leaves them after lunch with instructions to meet at the San
Miguel Mission at five, but the Initiates talk amongst themselves first.
While discussing the historical precedent for their being granted
interim pendants, Laurel remarks that she hasn't had this much fun since
visiting a spa in Vienna with somebody named Kalina.

There's just one problem: nobody knows who Kalina is, and Laurel has to
explain that she's a Senior initiate. Or, rather, she *was*. Not even
Laurel actually remembers Kalina. She just has a scene from a memory:
her and Kalina at the spa in Vienna.

Jaden writes this intelligence down, noting that the one erased may be
Kalina, and reluctantly begins her search for a pendant. However, when
she returns to Council dimension and cracks open her notebook to review
it, *all of the pages are blank*. This suggests two possibilities:

/ Erasure can effect textual references to erased individuals in
non-Lapidarian writings *after* erasure. / Lapidarian proximity doesn't
simply erase memory; it also affects *perception*. What Jaden wrote in
Santa Fe might still be there, but she can't see it because it mentions
Kalina.

I think the latter is more likely, simply because that's how *I* would
write it, but we don't have enough textual evidence to rule out either
possibility. We will have to see if Jaden sees what she wrote the next
time she leaves Council dimension without a pendant, or if Cedar gets
her hands on the notebook and sees Kalina's name mentioned.

The Missing Lapidarian Bees, Part 4: A Ritual Interrupted
---------------------------------------------------------

Several days after the Junior Initiates' visit to Santa Fe, Cedar
participates in a partial Ritual of Restoration led by the Azoth Magen
himself, Ailanthus. This partial ritual is aimed directly at protecting
and restoring the Lapidarian manuscripts.

Like many rituals, the Ritual of Restoration is structured so that the
Azoth Magen leads the invocations, and those gathered with him offer the
prescribed response when appropriate. Christian readers -- especially
Catholics, Anglicans, and Lutherans -- may note similiarties between the
Ritual and traditional celebrations of the Eucharist in this regard.

The Azoth Magen invokes the elements (Earth, Wind, Sea (water), and
Ember (fire)), and the arboreal essence of plants and trees. He then
invokes the "Lapidarian promise of *Ruach 2103* folio 51 verso -- the
manuscript leaf from which Linden first witnessed the bees disappearing"
(p. 117). In addition to calling upon the apiarian memory of this text,
Ailanthus summoned that of all other Lapidarian manuscripts.

The Elder Council spent over an hour on this ritual intended to restore
the Lapidarian bees to the manuscripts, but something has gone wrong.
The alchemical blue light generated by their work is fading. However,
Cedar is so deep in the flow of the ritual that she doesn't notice she's
the last person still chanting until Ailanthus commands silence.

As the blue light fade, a scarlet light replaced it. With it came
strident laughter in a voice everybody believed had been banished from
Council dimension through erasure. Thoroughly panicked by this voice,
the Elder council scrambles to find evidence in the Lapidarian
manuscripts of an imminent rebellion, or for evidence that the erasure
itself had failed on an elemental level.

They found nothing. Kalina shouldn't have been able to reach back into
Council dimension to laugh at them. All the same, she succeeded in doing
so and offered the Elders a token of her contempt.

![Return of the Erased Commissar](\ http://i.imgur.com/MUyapax.jpg)

Chapter Three
=============

Hot on the heels of the explosive ending of Chapter Two comes an
extended flashback. The entire chapter takes place one year before the
present, and introduces some elements from the Rebel Branch. I'll also
be going heavy on comparisons to real-world esotericism (Kaballah, the
Left-Hand Path, Discordianism, etc.), so buckle up.

Before we can explain why the Council expelled Kalina and subjected her
to erasure, we must first introduce Kalina and the Rebel Branch. We'll
also be meeting the younger Jaden. Finally, if you skipped the *Prima
Materia* (p. vii), now would be a good time to review it. Doing so will
provide valuable context for the topics covered in this chapter (pages
120-174):

-  Lead Into Gold: The "Hello World" of Alchemy
-  The Conjunction of Senior Magistrate Tesu
-  Unintended Consequences of Stalking
-  Sadira's Rebellious Impulses
-  Kalina's Disappearing Act
-  Jaden's Distrust and the SNAFU Principle
-  The Rebel Branch: Walking the Left-Hand Path
-  Sadira's Doubts
-  Jaden Tastes the Dragon's Blood
-  Cedar and Obeche at Cross Purposes
-  Dracaen's Pitch
-  Obeche's Zeal
-  Passing Notes
-  How Kalina Got Made
-  Jaden's Loss

Lead Into Gold: The "Hello World" of Alchemy
--------------------------------------------

We don't get the meet Kalina right away; that would be too easy.
Instead, to drive home the fact that we've gone back to a year ago in
the story, Cynthea Masson opens Chapter 3 with a lab experiment for the
Junior Initiates: the `transmutation of lead into
gold <http://www.scientificamerican.com/article/fact-or-fiction-lead-can-be-turned-into-gold/>`__.
This transmutation was one of the classic goals of Western alchemy,
along with the creation of the Philosopher's Stone, the alkahest
(universal solvent), and an Elixir of immortality.

For Junior Initiates like Jaden, the transmutation of lead into gold is
merely a first step, a trivial task intended to introduce and apply
basic alchemical concepts that will continue to serve Jaden as she
progresses through the ranks of the Alchemists' Council. In that regard,
it's similar to the "hello world" program many first-year computer
science and software development students are taught to write. Here's
one such program, `implemented in
C <http://c.learncodethehardway.org/book/ex1.html>`__.

.. code:: c

    int main(int argc, char *argv[])
    {
        puts("Hello world.");

        return 0;
    }

By itself, "hello world" is a trivial program, but it is also the
smallest *complete* program a programmer can write. If you can't write a
working "hello world", you won't be able to write more complex code.
Likewise, transmuting lead into gold is child's play for the Alchemists'
Council, and thus a suitable learning exercise for Junior Initiates.

However, Jaden builds up this first practical exercise in alchemical
principles in her mind, investing it with additional meaning. To begin,
she hopes to impress Sadira, who she has already marked as her favorite
instructor. Furthermore, Jaden hopes that by actually *doing* alchemy
she'll finally be able to get over her desire to escape Council
dimension and return to her old life -- if only for a little while.

For these reasons, Jaden got to class early and hopes that she would at
least impress Sadira as somebody who has their act together, since as
the current newbie Jaden lacks sufficient knowledge of alchemical
practice to distinguish herself in that regard, and certainly can't
outdo Cercis. Nor can she compete with Laurel on the basis of
appearance, though I suspect this is mainly the negative self-talk
common to many young women. Finally, Ritha has them all beat on
experience as the eldest of the Junior Initiates.

As the eldest, Ritha is eager to progress, but cannot do so until two
alchemists further up undergo conjunction. Fortunately, as she confides
to Jaden in class, the next conjunction should happen soon. According to
her, one of the candidates is Tesu.

The Conjunction of Senior Magistrate Tesu
-----------------------------------------

The scuttlebutt Ritha shares with Jaden concerning Tesu proves true
later on. Azoth Ravenea confirms it at the next Council meeting by her
announcement, though Jaden is too busy trying to work out the meaning of
conjunction by listening to the section of the Law Codes pertaining to
conjunction the other alchemists recite to pay full attention.

It is Rowan Esche who introduces Tesu's partner on conjunction. She's a
young blonde in garnet robes who Jaden had not previously noticed
before, but Cercis, Laurel, and Ritha all recognize her: this is Kalina.

Apparently the pairing of Kalina and Tesu is a shock to many of the
council's members, despite it being approved by the Elder Council, but
Cercis and Ritha share a different reaction: "that could have been me"
(p. 128). Ritha in particular seems to feel like she dodged a bullet,
since she would have been up for consideration if she had become a
Senior Initiate instead of Zelkova.

Unintended Consequences of Stalking
-----------------------------------

Though Jaden had not previously noticed Kalina before, the `Baader
Meinhof
Phenomenon <https://www.damninteresting.com/the-baader-meinhof-phenomenon/>`__
strikes with a vengeance, and now she sees the garnet-robed blonde
everywhere. Though in Jaden's case, it's less a frequency illusion and
more a matter of Jaden constantly looking for Kalina -- and looking for
reasons to talk with her. Jaden even compliments Kalina on her pendant,
a dark green stone set in silver repoussé (metal hammered from the
reverse side to create a low relief), but can't get more than a smile
out of her.

Not that Kalina's cordial aloofness stops Jaden. Instead, it only fuels
her determination to get through to the older woman. After a fight
between two birds outside her window wakes Jaden, she sees Kalina
leaving the residence hall and taking a stone path into the backwoods of
Council dimension.

Naturally, Jaden follows, but soon loses Kalina. Instead, she happens
upon a man and a woman arguing. She can only see one of them, a figure
in blue robes, without revealing her presence. However, she manages to
get close enough to hear their conversation.

It's about Tesu. They're concerned he saw them, but the woman is
confident that they had not yet `been
made <http://english.stackexchange.com/a/188783>`__. This confidence is
misplaced thanks to Jaden's presence, though she has no idea what the
hell is going on.

The conversation soon ends, and the blue-robed man withdraws a red stone
from within his robes. After chanting an incantation, the man soon
disappears into mist, and the woman leaves on foot -- or so Jaden
thinks.

Jaden's too interested in how the blue-robed man left to worry about her
*own* `OPSEC <http://www.opsecprofessionals.org/what.html>`__. This lack
of `tradecraft <http://www.merriam-webster.com/dictionary/tradecraft>`__
is how Kalina manages to find *her* studying the cliff face and
speculating about the possibility of escaping Council dimension.

Note, however, that Cynthea Masson doesn't clearly state that it was
Kalina talking to the blue-robed man. We see Kalina heading toward the
woods, and we see her when she confronts Jaden, but the only evidence we
have of Kalina talking with the blue-robed man are based on inference:

-  the scene didn't introduce any other named characters.
-  the man asks the woman if Tesu saw her.
-  the woman didn't disappear through the mist portal, and thus could
   have circled around Jaden and found her.

However, I'm getting ahead of myself. It isn't until page 135 that we
learn it was Kalina who caught Jaden. Instead, we cut to Sadira.

Sadira's Rebellious Impulses
----------------------------

We find Sadira in an interesting position in this chapter. She's still
raw over the loss of Saule because of her conjunction with Cedar, but
she's taken Cedar as her lover despite the Novillian Scribe being the
person who "for all intents and purposes, had murdered the love of her
[Sadira's] life" (p. 133).

She's stuck on a guilt trip, despising herself because she obeyed the
Council and its dictates instead of rebelling against them. No doubt
that's why we see her voice such sentiments as "chaos is underrated" (p.

#. when Zelkova asks for help with her scribing.

Zelkova's problem is easily solved; her pen just needs a new nib because
the old one was cracked, but if she were working on a real Lapidarian
manuscript, a broken nib could apparently disrupt elemental balance.

Never mind losing the kingdom for want of a nail; try losing the world
for want of a pen.

Zelkova, suffering an acute attack of sanity, says that she doesn't want
such power or responsibility, but Sadira insists that she's duty-bound,
and must accept it. Explaining that Zelkova has years, if not centuries,
of training ahead of her before she becomes a Lapidarian scribe, Sadira
tells the Initiate that all she has to do is heed the Council's lessons
and learn from them.

I wonder if Sadira believes that herself. Regardless, I suspect that if
Jaden were to go Rebel at least Sadira would sympathize.

Kalina's Disappearing Act
-------------------------

I had mentioned earlier that we didn't have direct evidence for Kalina
being the one to talk with the man in the blue robes. We still don't,
but Kalina confronting Jaden at the cliff-face is the first chance Jaden
has gotten to actually talk to the other woman.

Of course, Kalina's a bit more interested in grilling Jaden and finding
out what *she* saw than in conversation. Likewise, Jaden lacks evidence
that would let her think Kalina had been talking to the blue-robed man,
and this lack of evidence shapes her answers to Kalina accordingly -- at
least until Jaden remembers her backbone and demands an explanation.

Rather than provide one, Kalina begins the following exchange (on page
137):

    "Do you trust me?" she asked.

    "Trust you? I barely know you!" Jaden replied. She pulled away from
    Kalina.

    "Not yet," she [Kalina] said.

    "I don't trust you yet?"

    "You don't *know* me yet. And there may come a time when you don't
    know me again."

    "What?"

    "I can't explain right now. I have to go. You will understand soon."

While Jaden doesn't know it at the time, careful readers who have gotten
this far will no doubt realize that Kalina is talking about erasure.
Even if we don't know for sure that Kalina was the one speaking with the
man in the blue robes, this dialogue is suggestive.

The line, "And there may come a time when you don't know me again,"
implies that Kalina knows she's up to something dangerous, something
that could make her an unperson. She doesn't explain herself to Jaden
yet, and I think that's because her initial question, "Do you trust me?"
implies an unspoken question: "Can I trust you?"

Kalina probably isn't sure she can trust Jaden yet. However, her use of
the same portal as the blue-robed man has some interesting implications.

Jaden's Distrust and the SNAFU Principle
----------------------------------------

Jaden certainly doesn't know what to do about Kalina, or about the
arguing pair she saw while following the Senior Initiate. And there's
only one person in the Council she trusts enough to ask for advice.
Getting to talk to Sadira alone, however, proves impossible.

In addition to her pedagogical duties as one of the eight Magistrates
assigned to teaching the Junior and Senior Initiates, Sadira (along with
Linden) is currently busy supervising the other tutors. No doubt Cynthea
Masson's drawing on experience from her day job as a VIU English
professor as she describes Sadira's supervisory duties:

-  showing new tutors the ropes
-  dividing Junior and Senior classes based on the tutors' abilities
-  providing course materials and lesson plans
-  outlines techniques for individualizing lesson plans to account for
   varying ability and experience in initiates

The last one is probably the hardest. Though class sizes are quite small
compared to a typical US classroom (remember, there are only *four*
Junior Initiates and *twelve* Senior Initiates for eight Magistrates to
teach) it probably isn't easy to adapt official Council training
materials to engage an experienced Senior Initiate stuck in the same
class with a fresh Junior Initiate possessed of no knowledge of alchemy
not distorted by popular culture.

It's hard work, and Sadira finds it tiring. Linden, her fellow faculty
supervisor, insists he finds the bureaucratic work accompanying the
start of a new course rotation "invigorating" (p. 139), which only leads
Sadira to wonder why men on the Council -- where conjunction often blurs
gender to the point of making it all but irrelevant -- still insist on
displaying machismo by claiming an inexhaustible capacity for work.

It's a timely question, given that our inability to say no both to more
work and more *stuff* seems to be killing the planet's biosphere, but
that rant is beyond the scope of this commentary.

Getting back to the story, if Sadira had any thought of upbraiding
Linden for being too perky and eager, Jaden precluded the possibility of
acting on them by throwing open the double doors to the library and
rushing to her. Once she catches her breath, Jaden asks to speak to
Sadira privately.

Sadira, reasonably enough, asks if Jaden wants to talk about a personal
matter or Council business. Since Jaden says it might be of importance
to the council, Linden insists on getting involved.

There's just one little problem where Jaden is concerned: she doesn't
know if she can trust Linden. When she asks Linden, "Can I trust you?"
(p. 140), Linden immediately dismisses her concerns by countering, "Are
you suggesting that certain members of the Alchemists' Council are not
to be trusted?"

I think that will prove to be a mistake on Linden's part. While Jaden
didn't come to them intending to edit the truth to avoid implicating
Kalina, his brusque insistence that every member of the Council is
beyond reproach leads her to realize that she has no particular reason
to trust *any* member of the Alchemists' Council, whether it was Linden
or Kalina. After all, the Council had virtually abducted her, and the
alchemical vapors kept her drugged.

Add to this Jaden's status as the Alchemists' Council's newest initiate,
placing her at the *bottom* of the hierarchy, and you have the perfect
set of conditions for the `SNAFU
Principle <http://explorations.chasrmartin.com/2008/01/29/the-snafu-principle/>`__
to come into play. This principle comes from Robert Anton Wilson, who
explains it in *The Illuminatus! Trilogy* with Robert Shea:

    It's what I call the "SNAFU principle." Communication only occurs
    between equals -- real communication, that is -- because when you
    are dealing with people above you in a hierarchy, you learn not to
    tell them anything they don't want to hear. If you tell them
    anything they don't want to hear, the response is, "One more word
    Bumstead and I'll fire you!" Or in the military, "One more word and
    you're court-martialed." It's throughout the whole system.

    So the higher up in the hierarchy you go, the more lies are being
    told to flatter those above them. So those at the top have no idea
    what is going on at all. Those at the bottom have to adjust to the
    rules made by those at the top who don't know what's going on. Those
    at the top can write rules about this, that and the other, while
    those at the bottom have got to adjust reality to fit the rules as
    much as they can.

In any organization where those at the bottom of the hierarchy can be
punished for speaking too freely by those further up the chain, "all
fucked up" becomes "situation normal" because the people in charge have
no idea what the hell's going on. Fortunately the effect of the SNAFU
Principle on the Alchemists' Council is limited because there are only
ten levels of hierarchy and only 101 members, but Jaden still has
incentive to hold back information -- and this reticence buys time for
Kalina.

It won't save her, however. Despite sticking firmly to what she
understands as her minimum duty to the Council, Jaden told Sadira and
Linden enough for them to conclude that Jaden's report needs to go
straight to the Elders, and for Linden to suggest that she had witnessed
Rebel Branch activity, complete with an incursion into Council
dimension.

The Rebel Branch: Walking the Left-Hand Path
--------------------------------------------

Though Jaden may have foiled a Rebel Branch plot despite being only a
Junior Initiate, she has no idea what the Rebel Branch is. Linden
explains them to her in the following passage on page 143:

    "Vigilantes," said Linden. "A self-sustaining branch of the
    Alchemical Tree as old as the Council itself. For thousands of
    years, legions of generations, the Council has maintained control
    over the Tree, its Rebel Branch, and the outside world, but not
    without losses and irreparable damage."

This explanation may be suitable for a Junior Initiate, but I think the
truth is more complicated than that when we consider `Cynthea
Masson's <https://cyntheamasson.com/2016/06/09/reading-the-alchemists-council-chapter-2/>`__
response to my commentary on Chapter II:

    For me the main conflict of the novel revolves around opposing
    philosophies regarding free will and power. Since the era of the
    "primordial myth" with which the book opens, the Alchemists' Council
    and the Rebel Branch have been at war. Thus the conflict is as
    ancient as the dimensions themselves rather than based in particular
    memories that any living alchemist or rebel may have. The goal of
    the Alchemists' Council is to remove the Flaw in the Stone, whereas
    the goal of the Rebel Branch is to increase it. The Flaw in the
    Stone is what permits free will. If the Flaw were to be removed
    completely, the Council believes everyone would be saved in the
    dimensional equivalent of a unified afterlife. The Rebel Branch, on
    the other hand, wants to maintain their current existence as
    individuals with choice (rather than being forced into a collective
    "One" by the alchemists). This main conflict is explored through a
    variety of lenses throughout the book. Since I teach medieval
    literature, much of my inspiration for these conflicts came from
    philosophical debates on free will found in works such as Book IV of
    Chaucer's *Troilus and Criseyde*.

Considering this response by Masson to `Jana Nyman's review at *Fantasy
Literature* <http://www.fantasyliterature.com/reviews/the-alchemists-council/>`__,
we can view the Rebel Branch through several lenses.

For readers familiar with the fantasy works of Michael Moorcock, we can
view the world of *The Alchemists' Council* as one where Law is dominant
and Chaos fights tooth and nail to maintain a foothold. If the Council
succeeded in extirpating the Rebel Branch, it would mean that Law would
reign unchecked, a state as inimical to humanity and free will as that
of absolute Chaos.

While Linden describes the Rebel Branch as a part of the Alchemical Tree
ultimately under the Alchemists' Council's control, I think the Rebel
Branch is a conceit on the Council's part. Readers who reviewed the
*Prima Materia* may recall that the original conflict was between
Aralia, the first being to attain ego and intention, and Osmanthus, the
second to do so. Both combatants gathered followers, who continued the
conflict after Aralia and Osmanthus reconciled with each other and the
Calculus Macula through alchemical conjunction.

If the Alchemists' Council descended from the Aralians, then the Rebel
Branch probably descended from the Osmanthians, and constitutes a
separate and *adverse* Alchemical Tree. The notion of an "adverse tree"
brings to mind some concepts from Kabbalah, Jewish esotericism. In
Kabbalah, the light of God descends to the material world via the tree
of life, the Sephiroth.

However, there's also an "impure" counterpart to the Sephiroth that
distorts divinity, the
`Qliphoth <https://en.wikipedia.org/wiki/Qliphoth>`__ or Sitra Ahra
("the other side"). While most Jewish Kabbalistic practice steered clear
of the Qliphoth, viewing them as shells or remnants of the pure
Sephiroth, Gentile Hermetic systems often treat the other side as a
necessary balance. Consider this illustration from `Newaeon Tarot on
Tumblr <http://newaeontarot.tumblr.com/post/17018540458>`__:

.. figure:: https://s-media-cache-ak0.pinimg.com/736x/0e/d4/ca/0ed4cac367fd8d3f3f6fc1cba4f8b408.jpg
   :alt: Illustration of the Qliphoth as the roots of the Sephiroth

   Illustration of the Qliphoth as the roots of the Sephiroth

Have you noticed that Malkuth and Lilith, the light and dark
corresponding with the mundane/material world, intersect in the
illustration above? I think this is how the Rebel Branch actually works.
Both trees arise out of the Calculus Macula, but in opposing directions.

Both, I think, share a common goal, but their methods and attitudes are
such that I've come to view the Alchemists' Council as a white-light
`right-hand
path <https://en.wikipedia.org/wiki/Left-hand_path_and_right-hand_path>`__
practice, whereas the Rebel Branch uses the `left-hand
path <https://en.wikipedia.org/wiki/Left-hand_path_and_right-hand_path>`__.
Neither side wants the world's elemental balance to fall apart, or for
the outside world to suffer irreparable damage. However, we'll see later
on that the Rebel Branch attempts to *persuade* people, and seems to
honor individuality and volition.

Sadira's Doubts
---------------

Though Sadira shares Linden's opinion that Jaden's report needs to be
passed up the chain, she isn't sure it's wise to go straight to the
Azoths. After all, Jaden might not have seen Rebels. She might have seen
Council members she didn't recognize on legitimate council business. The
cliff face can also be used by Council members to transfer to the
eastern hemisphere of the outside world.

Instead, Sadira wants to consult one of the Novillian Scribes, like Amur
or Cedar. However, she'll have to make do with Obeche. This is a mixed
blessing. While Obeche is willing to listen, and agrees to convene the
Elder Council, in this chapter he seems to pursue Rebel Branch
alchemists as zealously as FBI director `J. Edgar
Hoover <https://en.wikipedia.org/wiki/J._Edgar_Hoover>`__ used to hunt
Communists and other subversives.

With Obeche on the job, Sadira has cause for fear. If the Elders launch
a sufficiently wide investigation, it might uncover Cedar's little
sideline as a Sephrim pusher.

Jaden Tastes the Dragon's Blood
-------------------------------

Getting back to Jaden, we find that Kalina appears to be missing. Jaden
didn't see her at lunch. Linden made no mention of her absence in his
afternoon class, and skipped over her name when checking attendance.
Even Zelkova, from Kalina's quarto of Senior Initiates, didn't realize
she was missing.

Jaden's smart enough to smell a rat. She's also smart enough to stop
asking questions, since she would eventually have to explain her
curiosity.

However, after dinner she notices a flashing red light coming from the
woods of the Council grounds. The light repeats, and settles into a
rhythmic pattern that Jaden likens to a code. So, what does she do? She
sneaks out of the residence hall and follows the light.

Once she gets to the woods, she finds Kalina, who asks Jaden to follow
her and quickly, since they "don't have much time" (p. 150). Jaden
initially resists, and asks Kalina how she knows that:

#. Jaden would have been the one to respond to her signal.
#. Others wouldn't have seen it and come to investigate.

According to Kalina, the light frequencies were alchemically keyed to
Jaden's elemental essence so that only she could see them. This allays
Jaden's concerns enough for curiosity to take over, and Jaden returns
with Kalina to the cliff face where a man in blue robes awaits.

Is it the *same* man in blue Jaden saw earlier this chapter? I think so,
because it doesn't make sense for there to be *two* men in blue robes
appearing at the cliff face unless *The Alchemists' Council* suddenly
became Middle-Earth fanfic and Jaden just met the two lost Blue Wizards
who went east of Mordor and were never heard from again.

I think what's really going on is **Chekhov's Gun**, named for Russian
dramatist Anton Chekhov, who expressed it as follows:

    Remove everything that has no relevance to the story. If you say in
    the first chapter that there's a rifle hanging on the wall, in the
    second or third chapter it absolutely must go off. If it's not going
    to be fired, it shouldn't be hanging there.

Given the extremely low probability of Cynthea Masson being ignorant of
this principle, I think it makes sense to assume despite the continuing
lack of *explicit* textual evidence that the man in blue robes we're
seeing in this scene is the same as in Jaden's last scene. Likewise, I
think Kalina was the one arguing with Blue Robes earlier. But I could be
wrong, and we'll just have to see how things turn out later on. Just
keep in mind that there might be two alchemists in blue.

But let's call *this* blue-robed alchemist by his true name, since
Kalina is kind enough to finally introduce him. His name is Dracaen.
Jaden isn't sure if Dracaen is the same blue-robed man she had seen
disappear last time, but I think it's likely for the reasons I explained
above.

Naturally, Dracaen immediately requests Jaden's trust. When Jaden
protests, Dracaen insists because, "Otherwise, we will repeat this
perpetually."

And he isn't talking about eternal recurrence. Rather, he's referring to
a prior meeting that Jaden doesn't remember. Likewise, without
intervention, Jaden won't remember this meeting later on:

    "We have met in the past and we will meet in the future, but you
    will know me only in the present. I exist here. Elsewhere, I am
    erased." (p. 151)

Seems like Dracaen has been a very naughty alchemist, at least as far as
the Council's concerned. Speaking of the Council, Kalina asks Jaden to
come with her and Dracaen, because it "isn't safe here".

Jaden complies, and finds herself in a cave hung with wooden wind
chimes. Dracaen and Kalina invite her to sit at their table, and Dracaen
serves her a draft of "Dragon's Blood", an alchemical cocktail of spiced
wine infused with essence of the Dragonblood Stone.

And what is the Dragonblood Stone? It is what the Alchemists' Council
calls the "Flaw in the Stone". In Council dimension, the Flaw is almost
impossible to see, but in Dracaen's cave it outshines the Lapis. Though
the Council would love to eradicate the Flaw, it is what allows free
will within Council dimension, the negative space Dracaen and the Rebel
Branch inhabit, and the outside world.

Getting back to the Dragon's Blood: drinking it will allow Jaden to
remember things she had previously forgotten, but the effect will only
last a few hours once she returns to Lapidarian proximity because the
Stone and the Flaw vie for supremacy just as the Council and the Rebel
Branch do. As above, so below.

When Jaden drinks the Dragon's Blood, she does indeed recall having met
Dracaen before, though she doesn't remember him by name. Instead at the
time he was just a man who shared his umbrella with her as she stood in
the rain waiting for a bus. He gave her a small red gemstone as a good
luck charm, warning her to keep it with her at all times.

If you guessed that the stone was a fragment of the Dragonblood Stone,
have yourself a cookie. Dracaen and the other Rebels knew Jaden was to
be the next Initiate. By giving her that fragment, they hoped she would
keep it and thus bring it with her into Council dimension, since keeping
even a splinter of the Dragonblood stone there can counter Lapidarian
memory loss after consuming the Dragon's Blood tonic.

Not that Jaden knew any of this at the time, or that it would have done
her much good if she did. The time wasn't right until now. Now, however,
Dracaen insists that Jaden keep the stone on her at all times lest she
forget about him again, because she must willingly *choose* alchemical
proximity to the Dragonblood Stone. Unlike the Lapis, Dragonblood
proximity cannot be forced on people.

Kalina, however, offers a different warning. If Jaden is caught with the
stone, she'll be branded a Rebel. And that's how the cat got out of the
bag: at Jaden's demand, Dracaen reveals himself as High Azoth of the
Rebel Branch of the Alchemists' Council, a carrier of the Dragonblood
pendant for four hundred and forty-three years, and the restorer of the
Flaw during the Third Rebellion.

His current misson? "The recruitment of Jaden." That's what *I* call
leading from the front.

Cedar and Obeche at Cross Purposes
----------------------------------

We don't get to see Dracaen make his case to Jaden just yet. Instead,
because Kalina is taking advantage of the Elder Council being in a
meeting, we're going to cut to the meeting and look over Cedar's
shoulder. Here's what we learn:

#. Obeche is paranoid, as I mentioned before, and has the same bee in
   his bonnet about the Rebel Branch that Joe McCarthy had about
   Communism, and a similar track record of seeing Rebels where none
   actually existed.
#. The Elder Council meeting was mostly a waste of time, with the Elders
   only unanimously agreeing to one proposal: members of the Senior
   Magistrate would be charged with monitoring the cliff face 24/7.
   Normally the Readers would get stuck with this detail, but they're
   currently busy identifying the next Initiate to fill the opening that
   will result from the conjunction of Kalina and Tesu.
#. Obeche isn't keen on this conjunction, either, but Azoth Ravenea has
   no time for his objections. Not that this stops Obeche. He insists
   that the timing is fishy, and that both Tesu and Kalina should be
   monitors. Ailanthus orders Obeche to do it himself, which ought to
   fix *his* little red wagon.

Cedar, surprisingly enough given what we've seen of her relationship
with Obeche, offers to help. I doubt she offered for his sake, though;
Cedar herself says that, "Conjunction is too great a sacrament to place
at risk," (p. 158).

Afterward, Cedar asks Sadira who *she* suspects Jaden saw at the cliff
face. Instead, Sadira demands that Cedar tell her the truth about the
strangers Jaden saw. Sadira thinks they're Cedar's source for the
Sephrim. However, as Cedar herself points out, it would be
extraordinarily stupid of Cedar to jeopardize their supply and
reputations by inviting her source for an illicit drug into Council
dimension.

Sadira seems to calm down, but a careful reader will notice on page 160
that Cynthea Masson appears to change viewpoint from third person close
with Cedar as the viewpoint character to third person omniscient in
order to get into Sadira's head and show that while Cedar thinks the
matter of where she gets the Sephrim is closed, Sadira still harbors
doubts.

Then again, I might have been misreading the book the whole time, and
Masson's always been using third person omniscient.

Dracaen's Pitch
---------------

Getting back to Jaden, Dracaen, and Kalina: Jaden is surprised that the
Rebel Branch wants to recruit *her*, but the Rebel Branch has their own
Elders, and their own Scribes and Readers. The Dragonian interpretation
of the Lapidarian manuscripts -- with the aid of some masterful
palimpsest revision and a well-placed lacuna or two -- suggests that
Jaden belongs with the Rebel Branch, rather than the Alchemists'
Council.

No doubt Jaden feels a bit like Neo did in *The Matrix* when Morpheus
explained that not only was he right to suspect that something was
terribly wrong with the world, but that the truth was worse than he
dared imagine.

However, like Morpheus and his crew, the Rebel Branch are adepts at
manipulating Lapidarian manuscripts and the alchemical aspects of the
world to their own ends, such as the preservation of volition ensuring
that the Elder Council didn't find cause to block Jaden's initiation.

However, as Kalina explains on page 161, "the people of the outside
world are currently at risk because of archaic Council protocols and
abuse of Azothian power."

Unfortunately, Jaden doesn't know who to believe. She has no reason to
trust the Alchemists' Council when they conscripted her, but she doesn't
know enough yet to decide whether the Rebel Branch is any better. All
she knows for sure is that if destiny can be written or rewritten, then
her *own* destiny might be similarly malleable.

Dracaen, to his credit, states that the Rebel Branch won't force Jaden
to choose immediately. Instead, he promises that as long as the Rebel
Branch exists, Jaden will always have a choice. He is similarly
forthcoming when Jaden asks what exactly the Council is doing to
endanger the outside world:

    "Human beings, with their pervasive pollution and obsession with
    technological advancements, are destroying the balance of the Earth
    beyond basic alchemical repair. To repair the world, the Council may
    opt to eradicate the free will of the people of the outside world."
    (p. 162)

How will the Council do this? The Lapidarian bees are the key. Released
en masse, their wings vibrate at a frequency capable of interfering with
people's ability to think for themselves. (If you're thinking that
people don't seem to think for themselves *now*, just imagine how much
worse it can get.) For the *Neon Genesis Evangelion* fans reading this,
the Alchemists' Council's endgame is similar to SEELE's: a `Human
Instrumentality
Project <http://wiki.evageeks.org/Human_Instrumentality_Project>`__
created not through Third Impact, but by erasing the Flaw in the Stone.

Dracaen isn't giving Jaden the hard sell now because the Rebel Branch
plays the long game. They hope to gain her sympathy, even if she doesn't
join them outright, so that they've got somebody on the inside as Jaden
rises through the ranks of the Alchemists' Council. Their primary aim
isn't to swell their ranks, but to maintain the Flaw in the Stone and
prevent the Council from gaining absolute control over both Council
dimension and the outside world.

As I pointed out earlier, they're advocates for chaos in a world
dominated by order.

However, Jaden and Kalina can't stay any longer. The Elder Council
meeting they used as cover will have ended, and that means the members
of the Elder Council are free to notice that a Senior Initiate and a
Junior Initiate have disappeared from Council dimension.

However, Dracaen has a parting warning for Jaden: she must find her
Dragonblood fragment and keep it on her at all times, otherwise
Lapidarian proximity will blank out her memories again. Dracaen can't
give Jaden a new fragment, because she won't be able to carry it
directly from negative space (Rebel Branch territory) into the positive
space of the Alchemists' Council. Nor can she detour safely through the
outside world without drawing notice.

The situation would be different if Jaden had her pendant, which can be
inlaid with a fragment of the Dragonblood stone that would safely cross
directly into Council dimension, but Jaden hasn't been part of the
Council long enough to get one.

--------------

Before I continue to the next scene, however, it would be a good time to
remark on Cynthea Masson's use of draconic imagery for the Rebel Branch.
The dragon is an interesting symbol for the Rebel Branch because of the
different meanings attributed to it by different cultures.

In Judeo-Christian tradition, the dragon is associated with Satan (or
more properly, *ha-Satan*, the Adversary) who in the Old Testament was
God's enforcer but in the New is God's enemy. In China and Japan,
however, dragons are emblems of the forces of nature: powerful, wise,
and fundamentally benevolent.

Furthermore, there exists in modern occult practice a "draconian
tradition" associated with the Left-Hand Path and the Qliphoth that uses
"dark"/"evil" powers like Leviathan, Lucifer, and Lilith as archetypes.
One such order is the Dragon Rouge, founded by Swedish occultist Thomas
Karlsson in 1989 (who also served as lyricist for the band Therion from
1996, and sings for a band called Shadowseeds).

Was Cynthea Masson aware of these associations? Perhaps not, but the
Ouroboros, the dragon swallowing its own tail, is a major symbol in
alchemy (and probably a distant cousin of Jormungandr, the Midgard
serpent).

Obeche's Zeal
-------------

While Kalina has tried to be mindful of the time, and is careful to
return to Council dimension with Jaden before their absence is noticed,
she wasn't prepared for Obeche. Switching to Cedar's viewpoint for this
scene (starting on page 164), we find her heading back from her visit
with Sadira to her rooms in the Novillian Scribes' wing.

Obeche is in her way. Knowing what we know of his character,
particularly his adversarial relationship with Cedar and his obsession
with rooting out Rebel subversion, I imagine him saying, "I would have
expected you to be in your chambers at this hour," is not an
observation. Rather, it seems like he wants to demand an explanation of
her, but doesn't dare.

Were he a Rowan, an Azoth, or Azoth Magen himself he might pull rank,
but he's Cedar's equal at most and must choose his words with care.
Considering Obeche's officious behavior, Cedar's reply, "Yet once again
I defy expectations," is positively restrained.

Despite Cynthea Masson's economy of words, I think unpacking Cedar's
line reveals additional shades of meaning. I think Cedar is also saying
to Obeche, "I will continue to run roughshod over your expectations,
because there is nothing you can do to enforce them upon me."

If there's an unspoken contest of wills in progress here, Obeche loses
and acknowledges his loss by changing the subject. Kalina is gone. After
the meeting, Obeche came to the Initiates' wing, knocked several times
on Kalina's door, and got no response. So what did he do?

He opened the door anyway. Cedar remonstrates, insisting Obeche had no
right, but Obeche is unrepentant. Azoth Ruis requested he monitor both
Kalina and Tesu, and he suspects Kalina may be in danger (p. 165), so he
has all the justification he needs.

He's just following orders, just as if he were the subject of a Milgram
experiment.

Not even Cedar's accusation concerning his true motives, that he
suspects Kalina of endangering the council, can deter him. As a member
of the Elder Council, he considers himself justified in intruding upon
an initiate whether she appears to present a danger to herself or the
Council.

Worse, he insists that Cedar join him in searching for Kalina. Or, at
least, he demands she wait there in the hallway with him for Kalina to
return to her rooms whereupon he'll grill her over her nocturnal
adventures. Not even Cedar's rational explanation, that Kalina is in one
of the libraries catching up, is enough to allay Obeche's suspicions.

Yet, that is just the explanation Kalina offers when she finally does
return on page 166. Though Obeche tries to catch her in a lie by asking
specific questions concerning her studies, Kalina is evidently prepared,
and answers all of Obeche's questions with ease.

No doubt Obeche wants to accuse her at this point, but Kalina's given
him no cause. Instead, she asks him what's wrong, putting him on the
defensive. Cedar realizes the extent of Obeche's miscalculation, noting
after Kalina slips into her chambers that Obeche just tipped his hand,
warning Kalina to do her rebel work more discreetly.

Obeche's unrepentant, and insists he may have prevented the Fourth
Rebellion. Cedar, perhaps a more astute student of history, or perhaps
less biased by zeal, notes that the Fourth Rebellion cannot be
prevented. It can only be delayed.

I suspect, however, that it's also possible to *precipitate* the Fourth
Rebellion. If so, Obeche's zeal may prove one of the major causes. As
we've seen in preceding scenes and in this one, he expresses his sense
of duty to the Alchemists' Council through constant vigilance. He's on a
constant lookout for any hint of Rebel Branch activity, regardless of
evidence. As a member of the Elder Council, Obeche's self-appointed
mission to root out the Rebel Branch may be one of the abuses Dracaen
spoke of when explaining the Rebel cause to Jaden.

How many alchemists has Obeche *driven* to rebel? It's hard to tell, but
I find Obeche's example applicable to real-world politics. If you're
looking for rebels, you'll find them though they might be rebels of your
own making.

Passing Notes
-------------

The next scene is a short one (pp 167-169) from Jaden's viewpoint.
However, it covers some plot points that will prove important very soon.

First, Jaden found her piece of the Dragonblood stone. It was right
where she left it, in her old jacket. She transfers it to a deep pocket
in her robes, where she believes it will be safe. We'll come back to
this soon.

Next, while Jaden is careful not to approach Kalina, she still wants the
Senior Initiate to know that she found her piece of the stone and
therefore remembers their meeting in Rebel dimension.

However, the class they share together isn't the time or the place. Not
when Obeche interrupts and announces the winners of the winter quarter
award for academic achievement. Laurel and Kalina get the prize: three
days at a spa in Vienna.

Remember how in Chapter II Laurel had a vague memory of spending a few
days at a Viennese spa with Kalina? It happened. I suspect, however,
that it's a ruse to get Kalina out of Council dimension for a few days.
More on *that* later, too.

Jaden never got to talk to Kalina about finding her piece of the stone,
but Kalina managed to pass her a note in the courtyard later that
afternoon. It's nothing explicit, just a reference to *Sapientiae
Aeternae 1818*. No doubt Jaden would find more explicit information
somewhere in that codex.

Kalina's technique is similar to dead drops used by real-world spies,
but suffers from a couple of flaws. Kalina should have told Jaden about
*Sapientiae Aeternae 1818* while in Rebel dimension if she had to tell
the younger woman herself. Dropping a scrap of paper is not only
obvious, but what if an errant breeze had blown it away from Jaden and
into unfriendly hands (like Obeche's)?

Unfortunately for Kalina, she's no spy. We'll find in the next scene
just how badly Kalina's lack of tradecraft betrays her.

How Kalina Got Made
-------------------

While it would have been interesting to see the action of pages 169-172
from Kalina's viewpoint, we instead witness Kalina's return from her
holiday with Laurel through Cedar's eyes.Cedar awaits the young
Initiates in the Hotel Sacher in Vienna, so she can escort them back to
Council dimension.

While Kalina possesses the power to make the return trip on her own, the
Council's trust only extends so far. Give Senior Initiate pendants to
keep them within Lapidarian proximity at all times? No problem. Letting
them travel between Council dimension and the outside world is another
proposition altogether.

Besides, there's another reason for Cedar's presence. The holiday in
Vienna wasn't all chocolate massages and other indulgences. The real
objective, implied by Cedar's observation that Kalina might be
disoriented from getting dosed with Lapidarian Amrita, was to give
Kalina rope and see if she'd hang herself.

Laurel has no idea what's going on, and happily takes the trip at face
value. She talks Cedar's ear off about the fun she had at the spa as
Cedar escorts her and the quiet Kalina back to the Council protectorate,
and from there back to Council dimension through the Quercus gate.

Upon their arrival at the protectorate, a twenty minute walk from the
Hotel Sacher, we learn that Cedar consulted with Linden before she
begins escorting the initiates back to Council dimension. We aren't
privy to the details, but I wouldn't be surprised if Cedar warned Linden
that Kalina might attempt to escape while Cedar's busy escorting Laurel
back to Council dimension.

If so, it's reasonable to wonder why Cedar didn't just escort Kalina
back first. I think there are a couple of reasons:

#. Escorting Laurel back first returns her to the safety of Council
   dimension faster.
#. The setup in Vienna is probably more delicate than it appears. Laurel
   had to go with Kalina, and she had to be ignorant of the real reason
   for the holiday, otherwise Kalina might have smelled a rat. She might
   have taken the opportunity to escape Council control before they
   could strip her of whatever power she had accrued thus far.

In any case, once Laurel is back in Council dimension she thanks Cedar
and then runs off to find Cercis. Cedar then brings Kalina back to
Council dimension, where she finds a most unwelcome welcoming party
awaiting her. Obeche is there, along with both Rowans, Kai and Esche.

And what does Obeche do? Only what he's probably wanted to do for a few
days now: he grabs Kalina by the arm and intones, "By order of the
Alchemists' Council, you are hereby accused of high treason."

Now, one might think Obeche's use of "high treason" instead of "treason"
is just a rhetorical flourish, or for dramatic effect. I don't think
that's the case. English common law once distinguished between high
treason and petty treason.

High treason was treason against the state, the sort of treason
explicitly defined in Article III, Section 3 of the Constitution of the
United States to ensure it was never used as a catch-all charge. Petty
treason, a crime now abolished, denoted treason against one's lawful
superior -- such as a servant murdering their master.

So, if petty treason doesn't exist any longer in countries that
inherited English common law, what does that make high treason? Well,
Canada still distinguishes between treason and high treason. One of the
major differences between treason and high treason in Canada is whether
the crime was allegedly committed while the country was at war.

Of course, I could be reading too much into the fact that Cynthea Masson
is a Canadian author and might be aware of the difference between
treason and high treason under her country's laws. It's also possible
that Obeche is being pompous, *or* that because the Alchemists' Council
is so old and steeped in tradition that it tends toward archaic usage in
its proceedings.

Once Obeche has accused Kalina he, the Rowans, and Cedar forcibly escort
her through the Council's back corridors to the Azothian chambers where
the rest of the Elder Council had assembled. Once everybody was seated,
Obeche makes his case. (p 171)

As Cedar suspected, Obeche managed to slip Kalina some Lapidarian
Amrita. A pastry Kalina ordered in Vienna had been infused with the
stuff. It probably wasn't hard to bribe the hotel cooks, if necessary,
though I have to wonder why Kalina didn't notice her treat had been
tampered with. Is Lapidarian Amrita tasteless and odorless? Did Kalina
lack any alchemical means of detection?

Regardless, the Amrita allowed Obeche to track Kalina's movements and
photograph her as she met with Dracaen. It also let him follow and
photograph her as she snuck into the Vienna protectorate, slipped past
security, and got her hands on Lapidarian ink and some of the
manuscripts in the southeast sector.

Looks like the Readers and Scribes are going to be working overtime.

Most damning of all is the evidence Azoth Ravenea finds in Kalina's
pendant: its memory had been wiped, which she describes as the work of
an alchemist skilled with Dragon's Blood tonic. Ravenea's
recommendation? Complete erasure.

It isn't until Azoth Magen Ailanthus gives the order to gather up all
manuscripts pertaining to Kalina to begin the erasure process that
Kalina speaks. She accuses Cedar of betraying her, an Initiate Cedar
herself had brought over.

Cedar knows better than to show any sympathy, and tells Kalina she
betrayed herself. Obeche, secure in his triumph, can't resist observing
that all Rebels eventually betray themselves.

Another aside before we cover the last scene in the chapter. In previous
chapters we've seen Lapidarian ink and Lapidarian honey, each possessing
alchemical properties granted by the Lapis, but what is Lapidarian
Amrita? All we know is that whoever ingests it can easily be tracked.

However, the original Sanskrit word from which "amrita" is derived
shares etymological origins with the Greek word "ambrosia" because
Sanskrit and Greek are Indo-European languages. Both words share the
same meaning. Both amrita and ambrosia are food for the gods. It first
occurred in the *Rigveda*, whose author used "amrita" as a synonym for
*soma*, the drink that confers immortality upon the gods.

I doubt that Lapidarian Amrita is so potent, but it served its purpose.
It helped Obeche gather the evidence he needed to brand Kalina a rebel
and a traitor.

Jaden's Loss
------------

We close the chapter with Jaden's viewpoint. She's on her way back to
the North Library via a shortcut, where she intends to examine "for the
third time in as many days" (p. 173) folio 16 of *Sapientiae Aeternae
1818*. Whatever message Kalina intended for Jaden to find there remains
hidden, and it will soon be too late.

On her way, she ran into Kalina, Obeche, and Cedar outside the portal
chamber. Obeche demands of Jaden an explanation, and Jaden tells him
part of the truth; she's on her way to the library.She hasn't read the
situation before her, as we see from the following passage:

    ...She then turned to Kalina, "How was your time in Vienna?"

    "Delightful," replied Kalina flatly.

    "Will you be busy later?" Jaden asked Kalina. "I could use some help
    with my lesson review."

    "Yes, she is busy," answered Obeche. "She is headed to the outside
    world."

    "Again?"

At this point, Jaden has no idea that Kalina has been judged a traitor.
Kalina, for her part, remains calmly defiant.

    "No worries, Jaden. I will return."

    Obeche slapped Kalina across the face. "You will not return!"

At this point, Jaden reaches out to defend Kalina, and why wouldn't she?
All Jaden knows is that a man just turned and hit a woman she considered
a friend. Obeche, however, is too angry to think straight and turns his
fury on Jaden, whom he grabs by the collar and wrenches away from
Kalina.

Jaden falls beside one of the corridor benches, her robes snagging on
the bench's wrought iron embellishment. Though Cedar and Kalina help her
up, the damage is done. Jaden has lost her pouch containing the
Dragonblood Stone.

She flees under threat of an official reprimand from Obeche, intending
neither to go far nor to stay away long, but it's too late. Lapidarian
proximity had already begun the work of altering her memories. First,
she forgot her desire to return for the stone. Next, she forgot about
*Sapientiae Aeternae 1818*. Finally, she forgot about Kalina.

But has Kalina forgotten about Jaden? Has Dracaen? We'll see...
