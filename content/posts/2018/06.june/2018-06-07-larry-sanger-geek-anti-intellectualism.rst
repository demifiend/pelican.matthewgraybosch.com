Larry Sanger and Geek Anti-Intellectualism
##########################################

:date: 2018-06-07 11:55
:summary: I had forgotten that Wikipedia co-founder was exploring the possibility of a new wave of anti-intellectualism among geeks back in 2011.
:category: Repeating History
:tags: larry sanger, geeks, anti-intellectualism, 2011

I had forgotten that `Wikipedia`__ founder `Larry Sanger`__ was writing about the possible rise of `anti-intellectualism`__ 
among geeks back in 2011.

.. __: http://wikipedia.org
.. __: http://larrysanger.org
.. __: http://larrysanger.org/2011/06/is-there-a-new-geek-anti-intellectualism/

Looking back seven years later, I suspect the answer to Sanger's question is "hell yes". If you want evidence, 
consider the following:

* The rise of neo-reactionaries, the "dark enlightenment", and the alt-right.
* The seemingly prevalent opinion among techies that schools should only teach `STEM`__ subjects.
* The growing acceptance of pseudoscientific bullshit among geeks like evolutionary psychology and scientific racism (under the guise of "human biodiversity").

.. __: https://www.ed.gov/Stem

What's the answer? I don't know. I'm a geek and a college dropout who codes for a living and writes crappy sci-fi novels, 
so my opinion is probably worth fuck-all.

I would *suggest* that young geeks and techies be given a heavy liberal arts education focusing on history, literature, 
psychology, and sociology. It might give them some perspective to viewpoints they might not otherwise encounter if left on 
their own.
