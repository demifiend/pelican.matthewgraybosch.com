# Life Lessons From Dark Souls

If you're a parent, and you've denied your children the experience of <em>Dark Souls II</em>, you have crippled them for life.
 
Do you even *consider* the lessons your children learn from the video games you buy for them? Have you considered the possibility that your children might learn the *wrong* lessons from their games? These are the lessons your children will miss by not playing <em>Dark Souls II</em>:
 
 * Failure is inevitable.
 * Concentrate on the task at hand.
 * Be patient.
 * Know yourself.
 * Know your enemy.
 * Know your tools.
 * Dodge. Don't block.
 * Face one enemy at a time.
 * Fight dirty.
 * Avoid expectations.
 * Don't be a one-trick pony.
 * Victory isn't the end.
 
## Reasons <em>Dark Souls II</em> Is Not for Kids
 
I will acknowledge a single reason to deny your children the frustration and joy of <em>Dark Souls II</em>. If you're one of those _responsible_ parents who take the time to read the ESRB warnings for games, and only choose "age appropriate" games for your kids, I understand. [Did this warning for <em>Dark Souls II</em> put you off?](http://www.esrb.org/ratings/synopsis.jsp?Certificate=33288)
 
> This is a role-playing game in which players assume the role of an undead fighter in the realm of Drangleic. Players traverse dungeon-like settings and battle a variety of fantastical enemies (e.g., ghouls, zombies, skeletons, giant rats) to gain souls. Players use knives, swords, and arrows to defeat enemies. Combat is highlighted by cries of pain and small splashes of blood. Some locations depict instances of blood and gore: a giant snake boss holding its severed head; a giant boss creature composed of hundreds of corpses; dead ogres near a pool of blood; a torture device with streaks of blood. During the course of the game, a boss creature appears partially topless (e.g. hair barely covering breasts). The words "bastard" and "prick" can be heard in the dialogue.
 
## The Kids Will Be Fine
 
If the above made you wary, I would understand. Here's the rub: if you let your kids watch TV news, or surf the web unsupervised, chances are they've seen worse. Much worse.

Instead, think of the valuable moral instruction From Software and Namco/Bandai is helping you provide, especially if you sit with your children as they play -- which you should be doing anyway.

### Failure is inevitable.

<em>Dark Souls II</em> isn't an easy, forgiving game. If you screw up, you get hurt. If you screw up badly enough, you _die_. Just like real life.

Unlike real life, however, you can take a deep breath, think over what went wrong, and try again from the last bonfire at which you rested. Try not to die again before recovering your souls.

### Concentrate on the task at hand.

If you're thinking about anything except the enemy you're currently trying to fight, that enemy is going to kill you. There's no time-out in <em>Dark Souls II</em>. Hell, there isn't even a pause button.

### Be patient.

You're going to die in <em>Dark Souls II</em>. You're going to die often. You're going to die because you tried to get a quick kill. You're going to die because you dodged the wrong way and rolled off a cliff. You're going to die because somebody invaded your game and was better than you.

You're going to die in real life, too. Accept it, take a deep breath, and remember that your feelings don't matter. Control the things you can control, and let everything else do their worst.

And if you're gonna die, die with your boots on.

[youtube=https://www.youtube.com/watch?v=JhBw80Y54gg]

***

### Know yourself.

Understanding the character you've chosen to play is the first step to success in <em>Dark Souls II</em>. Your character's capabilities will help determine which tactics are most likely to work in the game. Tactics suited to for a character built to wear heavy armor and wield massive weapons aren't ideal for a character built for speed or a character built to wield magic. 

Your own capabilities will help determine your options in life, as well. You must decide what skills and abilities you will cultivate. Will you build on existing strengths, or improve on your weaknesses? Do you even know your strengths and weaknesses? 

### Know your enemy.

Just as you must know your own character in <em>Dark Souls II</em>, you should also understand the enemies you've set out to fight. Every enemy type is different and will fight differently. 

Learn the enemy's attack pattern, and you can exploit it. Learn the enemy's weaknesses and you can strike with weapons capable of inflicting more damage faster.
 
You can't solve a problem you don't understand. You'll just get stuck, flailing away to no avail.

### Know your tools.

You'll likely collect dozens of different weapons in <em>Dark Souls II</em>: swords, daggers, hammers, axes, polearms, bows, catalysts, and other, more exotic implements. Each has its own movements, speed, rhythm, and requirements.

Some weapons are versatile, and usable in a broad range of situations. Others are best used in specific situations. A spear might be effective in a narrow passage where enemies are unlikely to surround you. In a wide open space, however, you might prefer a two-handed sword you can swing in a wide arc.

Remember the wisdom of Sun Tzu, who wrote in _The Art of War_...

> It is said that if you know your enemies and know yourself, you will not be imperiled in a hundred battles; if you do not know your enemies but do know yourself, you will win one and lose one; if you do not know your enemies nor yourself, you will be imperiled in every single battle.

***

### Dodge. Don't block.

Maybe your kids have never been in a fight. Maybe they've never been bullied. If so, good for them. But if they do find themselves in a violent situation, you want them to know how to handle themselves.

While imitating the action in <em>Dark Souls</em> is a bad idea, you can still apply some broad principles. The most important of which is that you should never let the enemy make contact.

Blocking a blow still hurts, and can still damage you. It's better, therefore, to avoid your enemy's attentions altogether. Why block an attack you can dodge?

### Face one enemy at a time.

This really should be self-explanatory. You can't ever count on having allies beside you in a fight. When shit gets real, you're likely to stand alone. You're more likely to survive a one-on-one duel than you are a battle against multiple adversaries -- especially if they've got solid teamwork and a grasp of small-unit tactics.

### Fight dirty.

I know you want your children to learn to be honest and to play fair. They aren't going to be knights of Arthurian romance facing chivalrous adversaries who salute before attacking, and never kick a downed enemy.

The enemies you will face will use every nasty trick available to get the better of you. They'll gang up on you. They'll sneak up on you. They'll play dead and strike from behind.

They'll do anything to avoid a fair fight, and you should emulate their example. If you're in anything resembling a fair fight, rather than a fight where the odds are outrageously in _your_ favor, then you fucked up.

### Avoid expectations.

Here is a principle that applies mainly to fights against other players. Others will invade your game unless you burn a Human Effigy at a bonfire or play offline.

None of these invaders will fight the same way. They may not fight as you expect them to fight based on their armament. They may fight honorably, greeting you with a bow that you might prepare for a duel, or they may strike from ambush.

Appearances deceive. Actions can also deceive. Always be on your guard.

### Don't be a one-trick pony.

It is easy and therefore tempting, to become attached to a favorite weapon and thus a favorite style of gameplay. I am guilty of this myself, especially when playing against others. I tend to favor one-handed slashing weapons, and I like to remain in constant motion.
 
According to Japan's sword saint, Miyamoto Musashi, this is a mistake. 

> You should not have any special fondness for a particular weapon, or anything else, for that matter. Too much is the same as not enough. Without imitating anyone else, you should have as much weaponry as suits you.

Don't depend on a single weapon, or a single spell, or a single tactic. To do so is to lose flexibility and leave you unable to adapt to changing circumstances.

### Victory isn't the end.

When you finally win through to the end, you won't find an ending. You won't find closure. You won't even find an opportunity to begin your journey anew -- at least not until you hit the bonfire at Majula.

But when you do finally start your journey anew, you'll find that it's the same journey, only harder. Life is also like that. It isn't enough to succeed once. Success' reward is the need to redouble one's efforts and succeed again, and again.

## That Isn't So Bad, Is It?

You might think that _Dark Souls II_ is just a gory, violent action game. You might think it's inappropriate for children. You might be right, but to deny your children the experience is to deny them an opportunity to learn lessons that will serve them well in school, and in adult life.