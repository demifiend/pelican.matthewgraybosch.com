WordPress.com? Hell No
######################

:date: 2018-06-13 22:47
:summary: Jesus Christ, Wordpress.com is as clunky as self-hosted WordPress. How the hell is that possible?
:tags: wordpress.com, wordpress.org, ux, mistake


Jesus Christ, WordPress.com is as clunky as self-hosted WordPress.  How the
hell is that possible?  I guess it doesn't matter, since Automattic's
inability to make their own flagship blogging platform look good saved me
from making what would probably be a mistake.  I'll just stick to hosting my
own site and building it with a static site generator.
