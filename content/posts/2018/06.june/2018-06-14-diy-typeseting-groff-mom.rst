DIY Typesetting and Document Processing with groff and mom
##########################################################

:date: 2018-06-14 10:38
:summary: groff and mom might be a good alternative for formatting documents instead of using LaTeX or a word processor.
:category: Just Tech Things
:tags: unix, typesetting, diy, troff, groff, gnu, mom, macros

I want to start typesetting my own novels and stories so I can publish them myself, but 
I don't want to use LaTeX_. I don't mean any disrespect to the people who worked on it, but
LaTeX is probably overkill for my purposes and despite having, reading, and understanding
Leslie Lamport's book I have never managed to produce good output.

.. _LaTeX: https://www.latex-project.org/

Fortunately, there's an alternative. They didn't have TeX or LaTeX at Bell Labs. They had troff_. If it was good enough for Brian Kernighan, Dennis Ritchie, Rob Pike, etc. then there's no reason I shouldn't be able to use it. Of couse, troff doesn't seem to be part of the OpenBSD_ base system.

.. _troff: https://troff.org/
.. _OpenBSD: https://openbsd.org

No matter. There's a port_ and package for the GNU version of troff, groff_. And it includes an interesting macro package called mom_.

.. _port: http://openports.se/textproc/groff
.. _groff: https://www.gnu.org/software/groff/
.. _mom: http://www.schaffter.ca/mom/mom-01.html

The MOM macros for GNU troff (groff) look like they might be straightforward. The online documentation_ certainly makes sense.

.. _documentation: http://www.schaffter.ca/mom/momdoc/toc.html

I'll have to give this a try, and see if I can put together a little anthology: *The Milgram Battery and others*.