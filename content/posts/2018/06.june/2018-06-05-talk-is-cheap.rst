Talk is Cheap
#############

:date: 2018-06-05 12:14
:summary: I trust Nat Friedman and his good intentions as far as I can throw them.
:category: Just Tech Things
:tags: microsoft, github, nat friedman, vayne solidor, final fantasy xii, distrust, cynicism, antitrust, self-hosting

Nat Friedman will be taking over as GitHub's CEO once Microsoft's acquisition goes through. 
He says `he wants to earn developers' trust`__. I've heard that one before, from a `dude with
better hair`__.

.. __: https://natfriedman.github.io/hello/
.. __: https://www.youtube.com/watch?v=QFAl51qtjEM

You could argue that I'm being unfairly cynical in my refusal to trust Nat Friedman and his stated intentions.
However, Mr. Friedman has one problem: he's Microsoft's puppet CEO, and I remember Microsoft under Bill Gates and
Steve Ballmer. They stood trial on `antitrust charges`__ *for a reason*. As such, he is to GitHub what Vayne Solidor 
was to Dalmasca in *Final Fantasy XII*--and I know exactly how nerdy I'm being for making this 
comparison.

.. __: https://en.wikipedia.org/wiki/United_States_v._Microsoft_Corp.

Regardless, I think it's apt, and I will be looking for options that will allow me to migrate
away from silos like GitHub this year. Self-hosting would be a lot easier if residential broadband 
wasn't so restrictive about running servers, but maybe DreamHost will let me rent a VPS that will allow me
to run my own git daemon and other goodies...
