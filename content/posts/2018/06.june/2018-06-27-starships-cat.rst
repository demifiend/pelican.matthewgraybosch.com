Starship's Cat
##############

It wasn't a mouse. Any of the humans aboard the PFS Investigator could have figured that out, even the marines the brass insisted on sending with us in case we ran into intelligent and hostile natives. Regardless, it was about the size of a mouse, with gray fur stippled with black spots that most likely provided active camouflage in its natural habitat, the planet we currently orbited. I kept thinking of it as THX-1138 Gamma, but that isn't its proper designation.

Knowing the planet's name was the crew's job. Keeping vermin from infesting the ship, and keeping Captain Matilda Flinders' lap nice and warm when not killing vermin, was mine. Whatever the creature I stalked was, it was vermin.

It hadn't noticed me yet. Instead, it kept creeping along the ventilation duct, following the row of scarlet LEDs I activated by jacking into the ship's computer. I followed it, inching along behind it even though the conduit was tall enough for me to walk correctly. If I spooked it, it might flee. It might even turn around and fight.

Its movement resembled that of caterpillars; it would lift its body up, extend it forward, and then pull the rest of itself behind it. However, like a slug, it also left a small trail of secretions in its wake. These secretions had a faint chlorine smell, and I soon realized that the little vermin wasn't merely following the ventilation duct. It was following spoor left by another of its kind.

I scrunched myself down, gathering my strength for the pounce as I flattened my ears and whiskers against my head. It seemed vulnerable. Pawing it to death was probably safe, but I wasn't going to take any chances with xenovermin.

I sprang, the sudden release of my tensed muscles driving me forward through the air. My aim was perfect, my paws pinning the creature down so I could sink my teeth into it, and snap its spine with a vicious shake. I'd get the rest later, or perhaps Captain Flinders would get the marines to make themselves useful and sweep the ship clean, but first I had to bring her proof of the infestation.

The Investigator announced any crew member who stepped onto the bridge by name and rank, except me. All I rated was a terse, "Ship's cat underfoot," which was downright contemptuous. As the ship's cat, I outrank everybody.

Captain Flinders ignored me in favor of an animated conversation with the Investigator's chief science officer on the main screen. I can't pronounce their name. I can't pronounce the name for their species, either, but I can tell you they look like trilobites. At least, they would if trilobites grew to two meters in length, were spacefaring and had cut a deal with humans to survey habitable exoplanets together.

Flinders waved me off with a dismissive, "Not now, Trim," so I just dropped my surprise in her lap. She shrieked as she realized I came bearing gifts. "Where did you find that?"

"Mrr. Mrrrrrr. Mrrrraow." I tried to explain, but the Captain only speaks two languages - English, and bad English - and my speech driver had crashed again. "Sorry, Mattie. I found it in the ventilation duct near the galley. Looks like we've got an infestation."