*Avengers: Infinity War* - I Liked it Better as *Final Fantasy IV*
##################################################################

:date: 2018-05-06
:summary: I liked *Avengers: Infinity War* better as *Final Fantasy IV*. This post is full of spoilers, and this is your only warning.
:category: Two Thumbs Down
:tags: disney, marvel, avengers, infinity war, the rapture, thanos, final fantasy iv, golbez, crystals, left behind, rental, hellboy, barney the dinosaur
:summary_only: true
:og_image: /images/golbez-final-fantasy-iv.jpg


Catherine and I went to see the latest Disney comic-book flick today.  Yep,
we followed *Black Panther* with *Avengers: Infinity War*, and quite frankly
*Black Panther* was better.  But it was her idea and she was paying, and all
I had to do was drive the car, keep my opinions to myself, and let her enjoy
the movie, so that's what I did.

However, I'm under no obligation to refrain from inflicting my opinions upon
you, gentle reader, so here's the deal.  I liked *Avengers: Infinity War*
better when it was a SNES JRPG called *Final Fantasy IV*.  You think the
comparison's unfair?  Here are the similarities:

* Both have a guilt-ridden protagonist (Tony Stark vs.  Cecil Harvey) 
* Both involve an epic battle on a moon that can't exist in reality.  
* Both have a villain whose nefarious scheme involves collecting a set of magical crystals.

Only instead of Golbez, who struts about in ornate black armor, we're stuck
with the unholy love child of Hellboy and Barney the Dinosaur: Thanos the
Mad Titan.

And what makes Thanos mad?  Apparently he was the only one out all of the
teeming millions on Titan who realized that their world's population was too
big to be sustainable.  What solution did he propose?  Condoms?  IUDs?  Anal
sex instead of vaginal intercourse?  Compulsory homosexuality?  Nope.  He
suggested a **murder lottery**.  He told everybody that half of the people
should be killed off.

Naturally, nobody listened.  This suggests that the people of Titan were
more advanced than us in at least one respect; they knew better than to try
to solve complex social, economic, and ecological problems with *mass
murder*.  Unfortunately, nobody thought to take Thanos into custody for
psychiatric evaluation, and the Malthusian apocalypse Thanos predicted ended
up coming after all.

Thus vindicated, Thanos gathers himself an army and goes from planet to
planet, culling populations one world at a time and occasionally adopting
girls that he himself had orphaned, until he has an epiphany.  He realizes
that there's *got* to be a faster, easier way to murder half of all of the
people in the universe.

And that's when Thanos the Mad Titanic Love Child of Hellboy and Barney the
Purple Pedosaur decides to get his Golbez on and start his quest for magic
crystals that will let him bring down the Rapture and reduce half the
population to dust in the wind.

Naturally, he has to kill a shitload of people along the way, while laying
on the Incredible Hulk so epic a beatdown that Bruce Banner spends the rest
of the movie unable to hulk out no matter how angry he gets, because the
Hulk is nothing but a big green bully who turns out to be chickenshit as
soon as he takes a hit from somebody his own size. And, because the plot
requires it, we're supposed to believe he gives enough of a shit about
Gamora (one of the orphans he made and then adopted) that throwing her to
her death counts as a sacrifice worthy of one of the crystals? Bullshit.

Then again, the whole movie is bullshit, but Marvel and Disney have
everything going so fast that it's hard to realize they were bullshitting
you the whole time until after the ending credits roll, Samuel L. Jackson as
Nick Fury sees the Rapture happen, and gets caught up in it before he can
finish delivering the only *sensible* response an intelligent person could
have to this bullshit: "Motherf--".

Here's what bugs me: while I can't say I was ever a huge comics fan, I've
read more Marvel comics than I've read DC, and many of the Marvel comics
I've read have been issues of the *Avengers*.  You know what I remember
about those comics?  The quiet times when the individual members were
interacting with each other instead of fighting together to foil some
outlandish plot with unbelievable stakes.

I remember shit like Tony Stark and Thor trying to teach the Vision how to
ask women out for dates and failing miserably.  That's not the sort of
material you see in the movies, except when a bit of comic relief is needed
between set pieces, but it's the sort of material that makes you care about
these characters and their larger-than-life struggles.

But *Avengers: Infinity War* doesn't give you time to give a damn about its
protagonist.  Nor does it give you enough information to care about Thanos,
understand his motives, or consider the possibility that his methods might
be necessary.  Instead, we're supposed to suspend our disbelief in both
Thanos' motives *and* methods, and that's more than even I could bear. 

Despite all of the above, I will probably end up seeing *Avengers: Left
Behind* or whatever they call the damn sequel. My wife will want to know
what happened, and I'll be curious as to whether Disney and Marvel have the
guts to not retcon everybody back to life like Marvel does so often in the
comics.

I mean, they couldn't even make Captain America stay dead after *Civil War*. 
Instead, we get *Captain America Reborn* starting with a paraphrase of Kurt
Vonnegut, Jr.  -- "Listen: Steve Rogers has come unstuck in time." So maybe
it'll turn out that all of the people Thanos raptured away aren't dead, but
stuck on some other plane of the multiverse.  It's how I would handle this
clusterfuck of a plot.
