Party Like It's 1999
####################

:date: 2018-05-21 19:59
:summary: `Make Frontend Shit Again`__ isn't the antidote to modern, anodyte, corporate-friendly web design we need, but the one we deserve.
:summary_only: true
:category: Just Tech Things
:tags: web design, websites, late 1990s, geocities, angelfire, tripod, frontend

.. __: https://makefrontendshitagain.party

`Make Frontend Shit Again`__ isn't the antidote to modern, anodyte,
corporate-friendly web design we need, but the one we deserve.  It's pretty
much a one-page greatest hits album for late 1990s websites hosted on
Geocities, Angelfire, Tripod, etc.  The idea was that the web used to be
fun, and that corporations and capitalism ruined everything.

.. __: https://makefrontendshitagain.party

Isn't that what corporations and capitalism usually do?  And hasn't frontend
always been shitty?

Regardless, I like the idea of saying "fuck it" to the modern web and going
back to hand-coded or copypasta HTML and CSS with JavaScript that does
nothing but animate shit.

Bootstrap, Foundation, React, Angular, Vue.js and the like can fuck right
off.  Ain't nobody gets paid enough for *any* of *that* shit.

If it doesn't work in lynx, it doesn't fuckin' work.  HTTP POST or GTFO!
