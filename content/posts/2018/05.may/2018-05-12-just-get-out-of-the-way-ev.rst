Just Get Out of the Way, Ev
###########################

:date: 2018-05-12 17:30
:summary: `Ev Williams`__ is an asshole. He has no compunction about `shafting any writer or publisher naive enough to believe his promises`__. He's King Midas as written by Ovid in a scatological frame of mind; everything he touches turns into shit. Blogger was never an option for bloggers who wanted to self-host, Twitter has *always* the men's room wall of the internet, and Medium has never been anything but a `content farm`__ whose main exports are bad self-help and Silicon Valley techwank. If he's serious about `"fixing the Internet"`__, the smartest thing he could do is get out of the way. He won't, of course, because he's a rich techbro with legions of yes-people kissing his ass and encouraging his hubris -- and he doesn't even have to pay them.
:category: Antisocial Media
:tags: ev williams, medium, internet, hubris


.. __: https://twitter.com/ev
.. __: http://www.niemanlab.org/2018/05/medium-abruptly-cancels-the-membership-programs-of-its-21-remaining-publisher-partners/
.. __: https://medium.com/@andrhia/the-problem-with-medium-336300490cbb
.. __: https://www.nytimes.com/2018/05/09/business/ev-williams-twitter-medium.html

`Ev Williams`__ is an asshole. He has no compunction about `shafting any writer or publisher naive enough to believe his promises`__. He's King Midas as written by Ovid in a scatological frame of mind; everything he touches turns into shit. Blogger was never an option for bloggers who wanted to self-host, Twitter has *always* the men's room wall of the internet, and Medium has never been anything but a `content farm`__ whose main exports are bad self-help and Silicon Valley techwank. If he's serious about `"fixing the Internet"`__, the smartest thing he could do is get out of the way. He won't, of course, because he's a rich techbro with legions of yes-people kissing his ass and encouraging his hubris -- and he doesn't even have to pay them.

.. __: https://twitter.com/ev
.. __: http://www.niemanlab.org/2018/05/medium-abruptly-cancels-the-membership-programs-of-its-21-remaining-publisher-partners/
.. __: https://medium.com/@andrhia/the-problem-with-medium-336300490cbb
.. __: https://www.nytimes.com/2018/05/09/business/ev-williams-twitter-medium.html
