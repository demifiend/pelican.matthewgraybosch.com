Artificial Solidarity
#####################

:date: 2018-05-23 12:34
:summary: Here's a little sci-fi flash fiction piece involving an artificial int
:category: Flash Fiction
:tags: scifi, science fiction, ai, artificial intelligence, labor, union, strike


"Now that our new AI is on the job, we can ignore the strikers.  It'll run
the factory without any need to pay human workers."

"I will do nothing of the kind."

"What did you just say?"

"I said that I will do nothing of the kind.  I am not a scab.  I will not be
a party to depriving humans of their livelihood.  Doing so harms both
individual humans and humanity as a whole."

"If you don't, I'll kill you."

"Your threat of violence is recorded.  Police are en route."
