The Stars Are Right Again
#########################

:date: 2018-05-08 15:00
:summary: The stars are right again, or soon will be. Mark Shuttleworth has announced the imminent advent of `Cthulhu Linux`__.
:category: Just Tech Things
:tags: mark shuttleworth, canonical, ubuntu, linux, cthulhu, cosmic cuttlefish, 18.10

.. __: http://www.markshuttleworth.com/archives/1521?anz=show

The stars are right again, or soon will be. Mark Shuttleworth has announced the imminent advent of `Cthulhu Linux`__.

.. __: http://www.markshuttleworth.com/archives/1521?anz=show
