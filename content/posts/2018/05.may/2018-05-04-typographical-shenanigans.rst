Typographical Shenanigans
#########################

:date: 2018-05-04 13:56
:summary: I've been monkeying around with the site's typography again. Font support has changed, and I've changed how paragraphs are rendered in articles.
:tags: website, css, fonts, typography, paragraphs, indentation
:summary_only: true


I've been monkeying around with the site's typography again. Font support has changed, and I've changed how paragraphs are rendered in articles. 

If you check out yesterday's *Shattered Guardian* session_, you'll see that all paragraphs after the first are indented until I throw in a header.

.. _session: {filename}/posts/2018/05.may/2018-05-03-shattered-guardian-session-10.rst

The next step is do this for non-blog pages. That'll be more complicated, because structure is different, but I should be able to manage. 

Also, the fancy web fonts I had added to make my site stand out from the pack are now only supported by the most modern browsers. If you're visiting this site on IE, you'll have to make do with whatever fonts you have installed locally (unless you've got Voltaire and Bitter installed).

Also, all absolute URLs should be protocol-relative and work whether you came to my site via HTTP (dangerous!) or HTTPS (safer).

If you don't know what any of this means, don't worry. I'm just trying to make this site better for you.
