I Found Some Mastodon Users' Blogs
##################################

:date: 2018-05-11 12:34
:summmary: I found websites for a couple of people I've seen on Mastodon. If you're on Mastodon and you want to be added to the list, hit me up.
:tags: mastodon, websites



I found websites for a couple of people I've seen on Mastodon.  Even better,
one of them uses my templates. :)

* SinaCutie_
* Mirabilis_

.. _SinaCutie: https://sinacutie.stream/
.. _Mirabilis: https://mirabilis.ca/


If you're on Mastodon and you want to be added to the list, hit me up.
