Shattered Guardian Session 12
#############################

:date: 2018-05-08 13:10
:summary: Josefine lies to Morgan, hoping to impress him, but gets caught. I only had time for short session today because my day job's been demanding lately.
:category: Serials
:tags: shattered guardian, josefine, morgan, conversation, music, albums, prince, purple rain, visual spectrum, lies, superpowers
:summary_only: true
:og_image: /images/starbreakercollage.jpg


Josefine lies to Morgan, hoping to impress him, but gets caught.  I only had
time for short session today because my day job's been demanding lately.

However, Josefine's choice of albums with which to lie to Morgan wasn't
exactly random.  I've been trying to listen to more black musicians lately,
such as `Janelle Monáe`__, `Prince`__, `George Clinton's`__ funk bands
Parliament and Funkadelic, and jazz bassist `Marcus Miller`__.

.. __: https://www.jmonae.com/
.. __: https://prince.org/
.. __: https://georgeclinton.com/
.. __: https://www.marcusmiller.com/

While I could have name-dropped any of these artists, I settled on Prince
and *Purple Rain* because in the album and movie Prince's character is
trying to figure out how to be a better man.  This is similar to Morgan's
work at learning to be human.  Janelle Monáe's *Dirty Computer* might have
worked almost as well, but I might save that for later.

AsgarTech Spire - Josefine Malmgren - 24 Hours Ago (Continued)
==============================================================

"Do you really want to know?" said Josefine, hoping Morgan would admit that
he had only asked for politeness' sake, saving her the embarrassment of
admitting that she had never cared enough about music to buy albums. 
*Though I should be glad he's capable of having a friendly conversation.  It
might make it easier for me to explain his condition if we've got rapport.*

Morgan leaned forward.  "I would not have asked unless I was curious."

*Just make something up.  He's obviously a hipster, so come up with
something old and obscure.* Using her implant, Josefine skimmed through the
last hundred of so songs she had streamed while working until she found
something that dated more than a decade before Nationfall?  "Ever hear of an
album called *Purple Rain*?"

Morgan's eyes widened, "You mean the Prince and the Revolution album from
1984?"

Josefine nodded.  "Yes.  You've heard of it, right?"

Morgan flashed a smile that lit up his face.  "I've got a copy on vinyl at
home," said Morgan.  He leaned forward and lowered his voice, "I had not
figured you as the album-collecting type.  Did you go looking for an album
to name-drop just to impress me?"

"Yeah." Josefine looked at her plate and pushed around the remnants of her
jambalaya.  "Was it so obvious that I was lying?"

Morgan sat back.  "It isn't something I like to talk about because it's just
another quality that sets me apart, but as soon as you used your implant I
could *see* it.  I must concentrate or my vision extends from the resonance
of electrons into the near ultraviolet.  If my concentration had not lapsed,
I might not have seen you reach out to the network with your implant.  If
not for that, I might not have realized you were trying to bullshit me to
keep the conversation going."