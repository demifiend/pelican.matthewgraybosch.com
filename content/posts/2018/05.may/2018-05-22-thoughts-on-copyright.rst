Thoughts on Copyright
#####################

:date: 2018-05-22 12:59
:summary: International law or not, even though I'm an author I think copyright terms are too damn long and should be shortened.
:tags: copyright, extension, reform, public domain, culture


I'm a published author, and I think that the original copyright term of 14
years with the option to renew once for 28 years total was perfectly
reasonable.  At most, 25 years with one renewal for a total of 50 years is
still fair.

I'm borrowing from my culture to create; it's only fair that what I create
should become part of the culture so that others can build upon it.  And if
I can't turn a profit on a novel in 50 years, then to hell with it.

Life of the author is excessive, IMO.  Life + 70-90 years is outrageous,
since the monopoly on a work no longer benefits the creator, but descendents
who had no hand in its creation.
