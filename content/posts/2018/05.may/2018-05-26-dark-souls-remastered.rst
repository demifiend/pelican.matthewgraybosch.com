Dark Souls Remastered
#####################

:date: 2018-05-26 21:43
:summary: I've got a USB stick and a copy of *Dark Souls Remastered*. I'll show you how to git gud by avoiding my mistakes.
:summary_only: true
:category: Hardcore Casual
:tags: dark souls, remastered, gameplay, asylum demon, undead burg, dragonbro, taurus demon, pyromancer, shortsword
:og_image: /images/dark-souls-remastered-elite-knight.jpg


I've got a USB stick and a copy of *Dark Souls Remastered*. I'll show you how to git gud by avoiding my mistakes. You won't see trophy notifications at first because I had already gotten a few initial trophies with another character before I thought to buy a USB stick and record video.



Undead Asylum
=============

Everybody starts here. Most people know that you aren't supposed to try fighting the Asylum Demon before you've gotten your starter gear, but I can't resist trying. You get a special weapon if you manage to defeat him.

.. raw:: html

    <figure>
      <video controls>
        <source src="/videos/dark-souls-remastered_asylum-demon-first-attempt.mp4" type="video/mp4">
        <p>Your browser doesn't support HTML5 video. Here is a <a href="/videos/dark-souls-remastered_asylum-demon-first-attempt.mp4">link to the video</a> instead.</p>
      </video>
      <figcaption>
        Fighting the Asylum Demon with a broken sword in <em>Dark Souls Remastered</em> by From Software and Bandai-Namco Games...
      </figcaption>
    </figure>

Naturally, it didn't work out, though I did take a chunk out of it. Soon enough, it's *payback time*.

.. raw:: html

    <figure>
      <video controls>
        <source src="/videos/dark-souls-remastered_asylum-demon-revenge.mp4" type="video/mp4">
        <p>Your browser doesn't support HTML5 video. Here is a <a href="/videos/dark-souls-remastered_asylum-demon-revenge.mp4">link to the video</a> instead.</p>
      </video>
      <figcaption>
        Payback time with the Asylum Demon in <em>Dark Souls Remastered</em> by From Software and Bandai-Namco Games...
      </figcaption>
    </figure>

With a plunging attack to start things off, all I had to do was stay behind this big-arsed bastard and not get hit. I got him this time, so off we go to Lordran.

Undead Burg
===========

To my surprise, there are already Dragonbros leaving summoning signs for duels. I've always preferred player-vs-player with adherents of the Dragon covenant, because it's strictly one-on-one and most Dragonbros follow a code of etiquette straight out of Eastern martial arts tradition.

It's been a while. Will I win?

.. raw:: html

    <figure>
      <video controls>
        <source src="/videos/dark-souls-remastered_failed-dragonbro-duel.mp4" type="video/mp4">
        <p>Your browser doesn't support HTML5 video. Here is a <a href="/videos/dark-souls-remastered_failed-dragonbro-duel.mp4">link to the video</a> instead.</p>
      </video>
      <figcaption>
        Dueling with a Dragonbro in <em>Dark Souls Remastered</em> by From Software and Bandai-Namco Games...
      </figcaption>
    </figure>

A Bit of Shopping
-----------------

There are a few items you can buy from the merchant in the Undead Burg if you can get past the two spear-wielding hollow soldiers waiting above him. 

* Orange Soapstone for 100 souls
* Residence Key for 1000 souls: lets you access a chest with Golden Pine Resin
* Repair Box for 3000 souls: lets you repair gear at any bonfire
* Bottomless Box for 1000 souls: lets you put away stuff you aren't using.

If you started out as a pyromancer or some other magic user like I did then you'll want to buy a Heater Shield for 1000 souls. It's a small shield with 100% physical damage blocking that lets you parry.

It Isn't the Size That Counts
-----------------------------

After getting my hands on a shortsword, courtesy of a loot drop from a fallen enemy, I decided to take a crack at killing the Taurus Demon. But first, a word about the shortsword in *Dark Souls Remastered*.

You don't see it used much, but the shortsword is actually a good weapon for players who want to play as magic users. It's just like the longsword, but weighs only 2 units instead of 3, and requires only 8 STR and 10 DEX to the longsword's 10 STR and 10 DEX, while getting the same C/C scaling on STR and DEX.

If you want to arm your magic user with a longsword but don't want to spare levels to meet the requirements for effective one-handed use, consider the shortsword instead. Its base attack rating is only 2 less than the longsword's, and only 5 less when maxed out at regular +15.

Titanite Shards!
----------------

The Undead Burg is a surprisingly good place to linger for a while because the enemies here will occasionally drop Titanite Shards. You need nine of these to reinforce a weapon to +5, and getting nine can be a grind, but it's worth doing since upgrading your weapons (and your armor, to a lesser extent) will give you an edge in all sorts of situations.

But you'll need a blacksmith. Fortunately, there's one just outside the New Londo Ruins, which you can access by taking an elevator located under Firelink shrine. Just watch your step along the way.

Taking the Bull by the Horns
----------------------------

With that out of the way, it's time for the Taurus Demon. This is one of those bosses that get demoted to mook much later in the game, but at this stage he's a challenge. It's possible to solo him, though, using many of the same tactics as with the Asylum Demon. It's just a tighter space.

.. raw:: html

    <figure>
      <video controls>
        <source src="/videos/dark-souls-remastered_taurus-demon.mp4" type="video/mp4">
        <p>Your browser doesn't support HTML5 video. Here is a <a href="/videos/dark-souls-remastered_taurus-demon.mp4">link to the video</a> instead.</p>
      </video>
      <figcaption>
        Taking on the Taurus Demon in <em>Dark Souls Remastered</em> by From Software and Bandai-Namco Games...
      </figcaption>
    </figure>

Be Back Soon...
===============

Keep an eye on this website for more videos as I progress further into the unhallowed halls of Lordran in *Dark Souls Remastered*. Don't forget to share this post on your favorite social sites, and feel free to email me or hit me up on Mastodon.

Oh, and if you're playing on the PS4 and see a summoning sign marked **EddieVanHelsing**, that'll be me, ready to help. Have fun, and praise the sun!
