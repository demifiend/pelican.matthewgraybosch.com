Typographical Shenanigans, Part Deux
####################################

:date: 2018-05-10 13:19
:summary: I've been monkeying around with the site's typography yet again.
:tags: website, css, fonts, typography, paragraphs, indentation
:summary_only: true


I've been monkeying around with the site's typography yet again.  I've
adjusted line height, which should help with readability. We've also got
drop caps now, and the first line of the first paragraph on a page or
article now renders in bold. This should also improve readability.
