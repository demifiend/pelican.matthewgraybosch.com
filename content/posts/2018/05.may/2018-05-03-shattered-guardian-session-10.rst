Shattered Guardian Session 10
#############################

:date: 2018-05-03 12:30
:summary: Josefine Malmgren has a tough job ahead of her, and she really isn't getting paid enough for this shit.
:category: Serials
:tags: shattered guardian, josefine, morgan, hospital, awakening, resurrection, humanity
:summary_only: true
:og_image: /images/starbreakercollage.jpg


Time for a new scene in `Shattered Guardian`__. Naomi has just learned that somebody she loved and thought dead is in fact alive.

.. __: /starbreaker/novels/shattered-guardian/

Now I'm cutting to twenty-four-hours ago and showing the immediate aftermath of his post-resurrection awakening, and introducing a character I had only mentioned in `Without Bloodshed`__: Josefine Malmgren.

.. __: /starbreaker/novels/without-bloodshed/

Restoring the dead to life is easier when you can restore from backup, but unlike in *Altered Carbon* this scifi resurrection tech isn't universally available.

AsgarTech Spire - Josefine Malmgren - 24 hours ago
==================================================

*He's finally awake.* Though it meant that Josefine Malmgren's efforts had
proved successful, the thought brought no flush of pride.  Instead of the
triumphant sense of accomplishment that Isaac Magnin and John Desdinova had
promised her, there was only the fear of having made a terrible mistake
churning in the pit of her stomach and the taste of acid creeping up the
back of her throat.  Though the man beyond the door looming before Josefine
had been friendly to her, she could not help but suspect that he would be
less kindly toward her once he learned that her work had been instrumental
in returning him from the dead.

The door swung open at Josefine's touch as if it weighed nothing, revealing
a dark man with a lithe build and long black hair lying in a hospital bed. 
"Excuse me, Adversary," said Josefine after clearing her throat.

He opened his eyes, and appraised her at a glance that reminded Josefine of
a cat sizing up possible prey.  "Good evening, doctor--"

"Malmgren, Josefine Malmgren." Josefine cringed at how easily the formulaic
introduction slipped off her tongue.  "We've met before, though you might
not remember."

Morgan nodded.  "Are you my attending physician, Dr.  Malmgren?"

"I'm not that kind of doctor," said Josefine, her tone as firm as she could
manage.  "Do you know where you are?"

Rather than answer immediately, Morgan got out of bed.  Before she could
protest, he turned his back on her and indulged in a spine-crackling stretch
that rendered every muscle in his back, bottom, and legs in sharp relief. 
"Would you mind telling me where I am, and what sort of doctor you are if
not a physician?  When was I injured, and how long was I out?"

*Here we go.* Josefine braced herself as if she expected to have to take a
punch, and drew in a deep breath.  "You're in the AsgarTech Spire, in the
Asura Emulator Project's R&D Lab.  My doctorate is in biomechanical
engineering, though I've considerable background in artificial psychology."

Morgan glanced at Josefine over his shoulder.  "It seems you know what I
am."

*Here's a delicate situation if ever there was one.* If Josefine's friend
Claire had been here, she would have warned Josefine to phrase her response
with exacting care lest she upset Morgan and risk permanently alienating
him.  Instead, Magnin and Desdinova had made a point of advising her to
handle Morgan's ego gently.  However, Morgan had not been the first asura
emulator for whom Josefine had had to play Whisperer.  *But Polaris had been
more volatile.  At least I know what Morgan wants to hear.*

Forcing herself to wear just the right sort of smile, Josefine answered
Morgan. "I know exactly what you are. You're a man, though human by choice
rather than by origin."
