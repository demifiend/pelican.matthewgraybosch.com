Shattered Guardian Session 11
#############################

:date: 2018-05-05 21:51
:summary: I'm picking up where I left off with Josefine and Morgan, giving Morgan a chance to show that he's human after all. I might have to tighten this scene up to keep the pace from flagging, but I hope I can salvage most of it.
:category: Serials
:tags: josefine, morgan, crowley's thoth, fandom, alcatrazz, no parole from rock 'n roll, memories
:summary_only: true
:og_image: /images/starbreakercollage.jpg


I'm picking up where I left off with Josefine and Morgan, giving Morgan a
chance to show that he's human after all.  I might have to tighten this
scene up to keep the pace from flagging, but I hope I can salvage most of
it, because Josefine is using this conversation to make sure that Morgan
didn't `come back wrong`__

.. __: http://tvtropes.org/pmwiki/pmwiki.php/Main/CameBackWrong

The poor woman knows better, but Claire keeps subjecting her to `Herbert
West, Re-Animator`__ marathons.

.. __: http://tvtropes.org/pmwiki/pmwiki.php/Film/ReAnimator

Like the author of the *Eddie Van Helsing* manga that Josefine mentions, I
too have a habit of mixing references to real-world bands with fictional
bands.  I've got no trouble pairing an Threshold revival band with an act
like Keep Firing, Assholes to support their *Even On Stage Nothing Works*
tour.

However, there actually *was* a band called Alcatrazz.  Founded by English
vocalist Graham Bonnett in the early 1980s, Alcatrazz is basically the band
that gave Yngwie Malmsteen his start, though he only stayed long enough to
record their first album, *No Parole from Rock 'n Roll*, and a live album
from their tour: *Live Sentence*.

Before you groan at the pun, remember that Iron Maiden called one of *their*
live albums *Live After Death*.

Oddly enough, I had a copy of No Parole as a kid.  My father had come home
one night with a big box of tapes, and after he took what he wanted it gave
the box to my brother and me and told us to take what we wanted.  Alcatrazz
was there along with Stryper, House of Lords, Great White, Slaughter,
Whitesnake, Poison, and lots of other 80s cheese.  But it also had King
Diamond, Judas Priest, Def Leppard, Led Zeppelin, Iron Maiden, Queensryche,
Dream Theater, Fates Warning, Black Sabbath, Megadeth, Anthrax, Slayer,
Michael Shenker Group, and Deep Purple.

It all warped my fragile little mind. Maybe I'm being a bit self-indulgent
here, but remember: `this is only the draft`__. All of this will be revised
before an editor gets their grubby hands on it.

.. __: /starbreaker/novels/shattered-guardian/


AsgarTech Spire - Josefine Malmgren - 24 Hours Ago (continued)
==============================================================

Morgan remained silent for a moment before speaking.  "Are you afraid of me,
doctor?"

"No," she lied.  "All right, just a little.  Maybe I watched too many bad
horror movies with Claire, but I keep worrying that you may have come back
wrong."

"I think you had better explain exactly what has happened to me," said
Morgan.  "But first, some clothes.  After that, perhaps we could get
something to eat?  Unless your professional ethics forbid it, of course."

An hour later, Josefine found herself wishing Claire was here to see Morgan
wearing jeans and a polo shirt emblazoned with the AsgarTech Corporation's
rainbow bridge logo and slogan--"Building Bridges to Our Future"--as he
sauntered over to their table carrying a tray laden with food in each hand. 
For Josefine, it just seemed strange to see Morgan dressed as if he worked
here, though she suspected Claire would capitalize on the unfortunate fact
that Morgan's shirt was a size too small and thus clung to him as if
airbrushed onto his skin.

The scent of spices preceded Morgan, making Josefine's stomach rumble as if
reminding her that it had been entirely too long since she last ate.  "They
still had some jambalaya left?"

Morgan placed a tray with two heaping dishes and an oversized mug of coffee
before her.  "The staff seem to know you, and made a point of putting
together a fresh batch as soon as I told them I was with you."

Her eyes watered, and Josefine found herself glad she could blame the
spices.  "I'm lucky they look out for me.  I get so wrapped up in my work
sometimes that I forget to eat.  Is it ever like that for you in the
studio?"

Morgan speared a chunk of sausage and thoughtfully chewed for a long moment
before answering.  "Sometimes, but I think it's fair to say I was built to
withstand a fast better than you can.  That was part of the work your
predecessors here did, was it not?"

"You don't think I did it?"

"Dr.  Malmgren, I am at least thirty years old.  You would have had to have
been an infant prodigy, or far better at looking after yourself than you
would have me believe."

"It seems your reasoning capabilities are undiminished," said Josefine as
Morgan pulled at his collar.  "I'm sorry they didn't have your size.  I
suppose you'd prefer a Crowley's Thoth t-shirt in any event."

"Wear my own band's t-shirt?" To Josefine's surprise, Morgan actually seemed
appalled by the notion.  He leaned forward, gesturing with his spoon.  "It's
obvious you're not a metalhead, doctor.  No musician wears their own band's
t-shirt, especially if they're headlining.  Instead, you wear another
band's--preferably the warm-up act to support 'em."

Now it was Josefine's turn to blush.  "I kinda wasn't until I started
reading *Eddie Van Helsing*."

"Seriously?"  Morgan shook his head.  "That damned manga has been a pain in
my ass ever since its first issue."

"It's fun.  I love the way Charlotte and Natalie keep Eddie in his place. 
And the shows seem so awesome--as long as there aren't any vampires in the
crowd."

That got a smile from him.  "Yeah, there's nothing like a good concert. 
Ever been to one?"

"Not yet.  I was saving my Crowley's Thoth t-shirt for your next tour,
but--" Josefine caught herself.  "Shit.  I'm sorry."

Morgan shook his head, and busied himself with his meal for a while.  When
his plate was half empty he said, "Only the really hardcore fans wear
t-shirts for the band they're going to see."

"Is that another one of those things I don't know because I'm not a real
metalhead?"

"I shouldn't have said that, and I apologize.  We all must start somewhere."

"Where did you start?"

Morgan shifted in his seat.  "This is a little embarrassing.  The first band
I ever got into was Alcatrazz.  I had this worn-out copy of *No Parole from
Rock 'n Roll*--"

"That sounds like something the author of *Eddie Van Helsing* would have
made up."

"I know, but it's the devil's honest truth.  I played that album constantly. 
It was the only one I had, and I didn't know any better."
