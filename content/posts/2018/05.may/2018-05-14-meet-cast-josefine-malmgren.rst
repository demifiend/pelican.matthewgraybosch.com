Meet the Cast: Josefine Malmgren
################################

:date: 2018-05-14 20:00
:summary: As if the cast of Without Bloodshed wasn't big enough, I've added even more characters to Shattered Guardian, such as Dr. Josefine Malmgren.
:category: Meet the Cast
:tags: women, scientists, josefine malmgren, shattered guardian, asgartech corporation, kitty emulator, asura emulator project, project aesir
:summary_only: true
:og_image: /images/sandrinenany-josefinemalmgren-final-credited-1200x1200.png


As if the cast of Without Bloodshed wasn't big enough, I've added even more
characters to Shattered Guardian, such as Dr.  Josefine Malmgren.  Or so I
would say if Malmgren hadn't gotten a passing mention in `Without
Bloodshed`__ by virtue of being Claire Ashecroft's old university friend and
sometime gaming partner.

.. __: /starbreaker/novels/without-bloodshed/

    "Curry and coffee for breakfast?"
    
    "I almost forgot." She retrieved a small container of mint chocolate
    chip gelato from the freezer.  "Thanks, Hal."

    "*Not* what I meant."
    
    "Call it lunch.  It’s late enough in the day for it.  I’ll work it off
    later in the simulator.  I've had a jones for some Ultraviolence ever
    since Mindcrime Interactive released the new expansion patch yesterday. 
    Maybe Josefine will join me, if she's not busy working." She shook her
    head, annoyed by her old university friend's tendency to let obsession
    with work rule her life.

Of course, Josefine has reasons for spending so much time at work.  One of
them weighs about sixteen pounds, has white whiskers and a little pink nose,
and needs near-constant adult supervision.

.. image:: /images/sandrinenany-josefinemalmgren-final-credited-1200x1200.png
  :alt: Josefine Malmgren and Zero. Art by Sandrine NANY, commissioned by Matthew GRAYBOSCH
  :align: left

In the `original Starbreaker`__, one of Josefine's first projects for the
AsgarTech Corporation involved creating suitable bodies for miniaturized and
predatory artificial intelligences.  Her job was to create "kitty
emulators", more realistic and much more useful versions of `mechanical toys
currently on the market`__.

.. __: /starbreaker/novels/original-starbreaker/
.. __: https://joyforall.com/products/companion-cats

Unfortunately for Josefine, she made her prototype a little *too* well. 
It's a little too smart, a little too advanced, and altogether too
talkative.  Zero's favorite song, which he sings when he's flopped onto his
back, goes like this:

| Belly-rub!
| Belly-rub!
| Give a cat a belly-rub!

Naturally, it's a trap.

Not only is she stuck with Zero as her biomechanical purr-baby, but she
attracted the notice of AsgarTech's CEO Isaac Magnin, who tapped her for
Project Aesir's research and development lab but never bothered to tell her
that she was actually working on weapons instead of artificial bodies for
existing artifical intelligences to transfer into for a "hardware upgrade".

What did Josefine do in Project Aesir?  Let's just say she got her
`Frankenstein`__ on and awakened one of the test subjects into the real
world, convinced that VR testing wasn't sufficient.  Polaris, the new model
asura emulator, will also appear in `Shattered Guardian`__.

.. __: https://www.gutenberg.org/ebooks/41445
.. __: /starbreaker/novels/shattered-guardian/

But before I show Josefine's misadventures with Polaris and how she came to
make the acquaintance of Morgan Stormrider and his friends, Josefine must
first help Morgan accept that he has died and been resurrected. 
Unfortunately for her, her studies in artificial psychology weren't quite
enough to prepare her.

Josefine is fundamentally a sweet, innocent young woman.  She's a bit naive
partly because of her focus on work, and partly in reaction to her friend
Claire Ashecroft's cheerful cynicism.  To Claire's chagrin, she has a bit of
a crush on Isaac Magnin, and thinks he doesn't know it and isn't exploiting
it -- and her.

The artwork in this post is by `CakeChoz`_, who did an excellent job with
the commission I gave her.

.. _CakeChoz: https://vavitsara.tumblr.com/