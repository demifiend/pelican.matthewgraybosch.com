Shattered Guardian Session 9
############################

:date: 2018-05-01 12:30
:summary: As if Naomi didn't have enough trouble, a voice from the past haunts her and an old enemy taunts her with a hope she dares not accept.
:category: Serials
:tags: shattered guardian, naomi, annelise, christabel, imaginos, morgan, grief, cemetery gates, hope
:summary_only: true
:og_image: /images/starbreakercollage.jpg


As if Naomi didn't have enough trouble, a voice from the past haunts her and
an old enemy taunts her with a hope she dares not accept. This writing
session is shorter than I'd like, but it's the first in over a week.
Hopefully now that Catherine has had her surgery I can relax and write more.

As always you can read the `full draft`__ of *Shattered Guardian* online.

.. __: /starbreaker/novels/shattered-guardian/

Queens - Naomi Bradleigh (continued)
====================================

"You bastard.  You just *had* to play the hero." Salt burned Naomi's eyes in
a flash flood of tears, and she dropped the roses she had meant to place
before Morgan's grave.  "I kept saying it would get you killed someday."

"That might have been what he wanted," said a once-familiar voice Naomi had
never expected to hear again.  She turned to face a slim brunette in an open
midnight blue wool overcoat over a navy suit.  Poised on spike heels, she
was almost as tall as Naomi, and though she taken on the drawn, haggard
aspect Naomi recognized in herself after too many sleepless nights and too
little to eat, there was no mistaking her gray-eyed arrogance or the band of
orange streaking one of her eyes.  "No doubt that was partially my fault."

Rage set Naomi ablaze, and her hand was on the hilt of her sword before
reason could assert itself and remind Naomi that the other woman was
unarmed.  "Christabel?  Is that you, or another hallucination?"

The other woman took a step forward.  "Of course it's me, Naomi, but please
call me Annelise.  When did you take up the sword?"

"At least a decade too late.  Would that I had had a blade handy when I
first saw you sniffing around Morgan."

"I suppose you're right to detest me," said Annelise, "How much did he
learn?"

"He learned enough," said Naomi, her eyes narrowing as a snow-blond man in a
white double-breasted suit approached. Naomi recognized him, and knew him
for what he was. Though as tall and gracile as a European fashion model, a
cruel and calculating power lay behind the dandy's mask, a power born of a
bitter heart that bides its time and bites.

The dandy glanced at Annelise, "Unfortunately, ladies, I must ask that you
continue your reunion in my limousine."

Though he wore no sword, Naomi drew hers.  Motioning for Annelise to get
behind her, she placed herself squarely in the pale man's path.  "Leave her
alone, Imaginos."

"I have done her no harm, nor do I intend any.  This I would swear by the
Styx, if such oaths carry any weight with you.  All I ask is that you come
with me, Naomi.  Morgan needs you."
