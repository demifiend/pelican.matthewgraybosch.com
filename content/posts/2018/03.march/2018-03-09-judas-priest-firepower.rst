Judas Priest: "Firepower" (2018)
################################

:date: 2018-03-09 16:56
:category: Metal Appreciation
:tags: judas priest, firepower, never the heroes, rising from ruins, heavy metal, classic metal, true metal
:summary: Judas Priest's 18th studio album is a solid effort with a few standout tracks and a flamboyant cover.
:summary_only: true


I'm listening to *Firepower*, the new Judas Priest album, on Spotify since I won't have the CD until I get home from work. It's a solid effort given the band's age and founding axewielder Glenn Tipton's recent and unfortunate Parkinson's diagnosis. "Never the Heroes" and "Rising from Ruins" are standout tracks, but there isn't a bad song on the entire 58 minute album.

Naturally, the album art is flaming...

.. image:: {filename}/images/judaspriestfirepowercd.jpg
    :width: 40em
    :alt: Cover for Judas Priest's 2018 album, Firepower.

If you like old-school metal, you probably already bought this album. If you're new to Judas Priest, this is a reasonable point of entry, but you might get a better impression from *Screaming for Vengeance*, *Defenders of the Faith*, and *Sad Wings of Destiny*.
