How to Enable Grayscale Mode on Android Smartphones
###################################################

:date: 2018-03-08 07:54
:category: Just Tech Things
:tags: android, grayscale, monochromacy, accessibility, developer mode, simulate color space, smartphone, addiction
:summary: I don't think this actually helps with smartphone addiction, but here's a quick rundown on how to enable grayscale mode on your Android smartphone.
:summary_only: true

I don't think this actually helps with smartphone addiction, but here's a quick rundown on how to enable grayscale mode on your Android smartphone. There are actually two ways to go about it.

The Easy Way
============

Depending on your phone model and Android version, you might be able to enable grayscale mode through your device's Accessibility settings. Look for a "monochromacy" or "grayscale" option under "Color Correction". If it's there, you're all set.

The Hard Way
============

Here's how to enable grayscale mode on Android if the option isn't available under Accessibility Settings.

1. Go to Settings.
2. Go to About Phone.
3. Find the Build Number.
4. Keep tapping Build Number until the phone acknowledges you as a developer.
5. Back out to main Settings screen.
6. Go to Developer Options.
7. Find and tap Simulate Color Space.
8. Select "monochromacy" from list.

That's all it takes to set your android ssmartphone to grayscale mode. It's not really *that* hard, but there are more steps involved. Ideally, Android settings should come with a grayscale switch under Display Settings.
