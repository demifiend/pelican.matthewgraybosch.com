Oh, no! Americans have Discovered Refurbished Phones!
#####################################################

:date: 2018-03-01 13:11
:category: As The World Burns
:tags: smartphones, used devices, planned obsolescence, late stage capitalism, samsung, apple, schadenfreude
:summary: Are we supposed to care when planned obsolescence bends CEOs over their mahogany desks and sodomizes them without lube?
:summary_only: true


More USians are finally figuring out that they don't *need* to buy brand-new devices. They've figured out that refurbished phones are often perfectly adequate for their use case. 

According to `a paywalled News Corporation outlet <https://www.wsj.com/articles/your-love-of-your-old-smartphone-is-a-problem-for-apple-and-samsung-1519822801>`_:

    "Smartphones now resemble the car industry very closely," said Sean Cleland, director of mobile at B-Stock Solutions Inc., the world’s largest platform for trade-in and overstock phones, based in Redwood City, Calif. "I still want to drive a Mercedes, but I’ll wait a couple of years to buy the older model. Same mentality."

Here's an `alternate source <https://9to5google.com/2018/03/01/smartphone-upgrade-cycles/>`_ for those who prefer to avoid the *Wall Street Journal*.

Apparently this is a problem for OEMs (original equipment manufacturers) like Apple and Samsung. Corporations keep pushing out slightly new versions of a device to prop up their profits instead of making durable, upgradeable equipment, and we're supposed to care when their strategy of planned obsolescence bends their CEOs over their mahogany desks and sodomizes them without lube?

*Cry me a river while I find my tiny violin.*

Let people with more money than sense buy shiny, brand new gear and replace it every month if they want; at least it puts the money into circulation where it might pass through workers' hands. For everybody else, buying used equipment is a perfectly sensible strategy and one that should be encouraged precisely *because* it fucks over corporations. 

Of course, I'll also be screwed if more people realize they don't need brand-new laptops, either. It'll drive up the price of used T-series ThinkPads.
