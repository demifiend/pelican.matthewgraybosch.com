Shattered Guardian, Episode 1: "Cemetery Gates"
###############################################

:date: 2018-03-03 13:13
:category: Starbreaker
:tags: shattered guardian, screenplay, episode 1, chapter 1, cemetery gates, pantera, naomi bradleigh, christabel crowley, annelise copeland, isaac magnin, morgan stormrider, abram mellech, sabaoth, shaddai, josefine malmgren, desdinova, imaginos, adramelech, death, mourning, resurrection, disbelief, amnesia
:summary: I'm roughing out the first chapter of Shattered Guardian as a screenplay. Naomi Bradleigh learns that her lost love might yet be found, and she is not pleased.
:authors: Matthew Graybosch, Catherine Gatt
:summary_only: true


I'm roughing out the first chapter of Shattered Guardian as a
screenplay. Naomi Bradleigh learns that her lost love might yet be
found, and she is not pleased.

The theme song for this episode is "Cemetery Gates" by Pantera, from
*Cowboys from Hell* (Atlantic, 1992).

.. raw:: html

	<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WyczwqRD2NI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

EXT. 64 YEARS AFTER NATIONFALL
==============================

The Manhattan skyline is visible not because of its lights, but because
of the dark silhouette it casts against the moon and starry sky. Small
red lights like fireflies dot the streets, but the city doesn't blaze
as it once did before everything fell apart. A single light breaks away
from the others.

*Closer:* The lone light is that of a lantern held by a statuesque woman
wearing a burgundy wool overcoat and a sidesword on her hip. This is
**Naomi Bradleigh**, a classically trained soprano, concert pianist, rock
musician, and former Adversary. Her long, snow-blonde hair trails behind
her in the wintry breeze. Her mouth, with lips done in deep carmine to
match her eyes, is set in a hard, determined line. Her stride is swift
and purposeful as she finds her way to a cemetery.

We see in that in her other hand she is carrying two bouquets of
black-tipped scarlet roses as she ignores the majority of the graves on
her way to the **Adversaries' Graveyard**.

EXT. MANHATTAN, ADVERSARIES' GRAVEYARD - NIGHT
==============================================

We can tell this part of the graveyard is different from the complete
absense of all conventional religious symbolism. Every grave is marked
with the swords, serpent, and scales of the Phoenix Society's Civil
Liberties Defense Corps (CLDC), aka the Adversaries. Each grave bears
two sets of dates indicating the lifespan of the deceased and the term
of their service. Most bear as their epitaph the same Latin motto that
is inscribed over the gate: "Mors omnibus tyrannis" - "Death to all
tyrants".

At the center of the graveyard is an eternal flame in the form of a
sculpture in marble of a heroic male figure, nude save for broken
manacles and leg irons, holding a flaming torch in one upraised hand
while crushing an eagle beneath his heel. Though NAOMI BRADLEIGH
recognizes the figure as the titan Prometheus, the pedestal is inscribed
in Latin:

    LUCIFER INVICTUS

Naomi lays one of her bouquets before the pedestal, and kneels as if to
pray.

NAOMI
        For my fallen comrades.

EXT. MANHATTAN, ADVERSARIES' GRAVEYARD - NIGHT
==============================================

One grave stands apart from the rest, between a pair of newly planted
saplings. Its stone is thrice the size of the others, for it bears more
than just the salient facts and a reference to the deceased's true
nature:

    MORGAN STORMRIDER
    2082 - 2112
    2101 - 2112
    WEAPON BY DESIGN, HUMAN BY CHOICE

Below it is a discography:

    * PROMETHEUS UNBOUND
    * GRAVITY MARGIN
    * TO REIGN IN HELL
    * SPARTACUS
    * HOMO EX MACHINA
    * TEAR DOWN THIS WALL

Naomi Bradleigh approaches the grave, her head bowed as if she hopes to
hide the tears welling in her eyes. She stands before the grave, looking
for a place amid other bouquets and gifts to the dead from previous
mourners to place her own offering.

NAOMI
        Damn it, Morgan, why did you have to play the hero?

Naomi creates a place by pushing aside a half-empty bottle of whiskey
that somebody had left after pouring a libation, and places her roses by
Morgan's grave.

NAOMI
        I told you it would get you killed someday.

EXT. MANHATTAN, ADVERSARIES' GRAVEYARD - NIGHT
==============================================

A tall, slim brunette approaches, her heels clicking against the
flagstones. She is dressed at the height of understated winter
fashion in an open blue wool overcoat over a blue suit with a
just-below-knee-length skirt and black stockings and spike heels that
make her almost as tall as Naomi.

*Closer:* We see that her face is drawn and haggard from too many
sleepless nights and too little food. Her hands tremble a little. One
of her blue-grey eyes is streaked with a wide band of orange. This
is **Annelise Copeland**, the artist formerly known as **Christabel
Crowley**.

ANNELISE
        That may have been my fault.

Naomi turns on her heel and grasps the hilt of her sword.

NAOMI
        Christabel?

EXT. MORGAN STORMRIDER'S GRAVE - NIGHT
======================================

ANNELISE
        When did *you* take up the sword?

NAOMI
        At least a decade too late. I should have had a blade handy when I first saw you sniffing around Morgan.

ANNELISE 
        I suppose you're right to hate me. How much did he learn about me?

NAOMI
        He learned enough.

CHRISTABEL?
        (looks away)
        What did he say?

EXT. MANHATTAN, MORGAN STORMRIDER'S GRAVE - NIGHT
=================================================

A man in a white double-breasted suit approaches. He has white hair,
like Naomi, and cold blue eyes. Though he's tall and gracile like a
European fashion model there's a sense of mortal peril about him, that
behind the dandy's appearance is a cruel and calculating power, a bitter
heart that bides its time and bites. This is ISAAC MAGNIN, whom those he
trusts know as the sorcerer and ensof IMAGINOS.

ISAAC MAGNIN
        Unfortunately, Annelise, that will have to wait.

NAOMI
        (drawing her sword)
        Leave her alone, Magnin.

Naomi places herself between Annelise and Isaac Magnin. She raises her
sword, ready to fight.

ISAAC 
        Please come with me. Morgan needs you.

INT. ASGARTECH SPIRE, RECOVERY ROOM - NIGHT
===========================================

*24 hours ago...*
-----------------

Dr. **Josefine Malmgren**, a petite blonde wearing jeans and a cardigan,
approaches a high-tech hospital bed in which a lithe, dark man with
long black hair rests. She is nervous because she knows the man by
reputation.

JOSEFINE
        Adversary Stormrider? Are you awake?

**Morgan Stormrider** opens his eyes. They're green, and have slit
pupils like a cat's. He scans the room, but does not startle the way
many patients do when awakening in a hospital room. Instead, he gets out
of bed. Naked, he turns his back on Josefine and stretches.

MORGAN
        Where am I? Who are you? When was I injured? How long have I been out?

JOSEFINE
        You're in the AsgarTech Spire, in the Asura Emulator Project's R&D lab. I'm Dr. Josefine Malmgren.

MORGAN
        Medical doctor?

JOSEFINE
        Computer science and psychology, actually.

MORGAN
        Asura Emulator... You know what I am?

JOSEFINE
        We haven't met in person, but our mutual friend Claire has told me a great deal about you.

MORGAN
        Which explains why I don't recognize you, but--

JOSEFINE
        I'm afraid this is going to be hard for you.

MORGAN
        Coma?

JOSEFINE
        Worse than that. You were dead.

INT. ASGARTECH SPIRE, CAFETERIA - NIGHT
=======================================

*22 hours ago...*
-----------------

Morgan Stormrider, dressed in jeans and an AsgarTech t-shirt, returns to
a table where Josefine Malmgren is waiting with a tray laden with coffee
and two plates of chicken tikka masala with rice. Giving one plate to
Josefine, he tries the other. He's tentative at first, as if he doesn't
trust the cafeteria staff to make a decent meal, but is soon eating with
gusto. Afterward, he sits back and sips his coffee, which is black like
Sabbath.

MORGAN
        I refuse to believe I was dead.

JOSEFINE ^
        Your body was so thoroughly damaged we had to restore your psyche from a backup into a 200 Series model.

MORGAN
        200 Series? Are you suggesting that this isn't my body.

JOSEFINE
        It is now. I suggest you make yourself at home, because it gets worse. Your last backup was a month prior to your destruction. You will have to reconstruct your personal narrative from external records. Dr. Magnin and Dr. Desdinova asked me to help you.

MORGAN
        No. That can't be true. The last thing I remember was getting in Naomi's car after Christabel's funeral. There must have been a crash. It must have been bad enough to leave me comatose.

Morgan grabs Josefine's hands.

MORGAN
        Where's Naomi? I need to see her. I need to see that she's all right. Please!

EXT. MANHATTAN, CEMETERY - NIGHT
================================

*the present time...*
---------------------

Naomi stares at Isaac Magnin, trembling so badly that she cannot hold her sword steady.

NAOMI
        I saw Morgan die. He came apart in my arms, you bastard.

ISAAC
        I know. I'm sorry you had to go through that.

ANNELISE
        Then why taunt Naomi so?

ISAAC
        I would never taunt my daughter so, Annelise.

NAOMI
        I am not your daughter. Not in any sense that matters.

ISAAC
        For which I've only myself to blame. Please, Naomi. Sheathe your sword and come with me.

NAOMI
        Why should I believe you?

ISAAC
        When have I ever lied to you?

NAOMI
        When have you *not* lied to me? You're a grandmaster in the art of lying by omission.

ANNELISE
        He wouldn't lie about *this*.

Naomi takes a step forward and presses the tip of her sword to ISAAC's throat. The veins in the crystalline gray blade pulse as if it were alive.

NAOMI
        If you want me to come with you, you'd better tell me *everything*.

ISAAC
        Sheathe your sword and I will. Having the Starbreaker's point at my throat makes me nervous. It hungers, you see.

NAOMI
        I know.

Naomi sheathes her sword.

NAOMI
        It whispers in the back of my mind every time I touch it, tempting me to kill.

INT. SOMEWHERE IN EUROPE, RUINED CATHEDRAL - NIGHT
==================================================

*16 hours ago...*
-----------------

The full moon streams through the heavily damaged stained glass windows
of a ruined cathedral. A cadaverous man with close-cropped brown hair
and feline yellow eyes approaches the altar, which appears to have been
desecrated long ago. This is **Abram Mellech**, whose public pose is that of
a televangelist and the founding head pastor of Agape Ministries. His
true identity is that of the ensof **Adramelech**.

*Closer:* An obsidian mirror rests on the altar, its surface roiling with
tenebrous clouds. A voice issues forth, like that of God or of Oz the
Great and Terrible, but it remains disembodied. This is all that **Sabaoth**
-- the ensof that claims to be **Shaddai** (the almighty) -- can manage.

ADRAMELECH
        I trust, Lord, that you have called me here for a reason.

SABAOTH
        I am displeased, Adramelech. You told me that Morgan Stormrider had died for defying Me. I feel his signal on the network strangling this world.

ADRAMELECH
        He did indeed die. Our problem is that he did not *stay* dead.

SABAOTH
        Has IMAGINOS learned to resurrect the dead?

ADRAMELECH
        Asura Emulators are an exception to the general rule. Their psyches can be backed up and placed in external storage.

SABAOTH
        Could IMAGINOS copy a backup of MORGAN to multiple bodies?

ADRAMELECH
        It stands to reason that he could, but I doubt he would profit by doing so. I suspect they would each assert the truth of their identity, and fight amongst themselves. Moreover, there is but one Starbreaker to wield against you, SHADDAI.

SABAOTH
        To think IMAGINOS and his cabal call Me "SABAOTH", the "LORD OF HOSTS", when they've possessed the technology to raise armies of biomechanical abominations against Me and my righteous all this time...

ADRAMELECH
        Lord?

SABAOTH
        Destroy all backups of Morgan Stormrider's psyche. Ensure no new backups can be made. Then kill him.

ADRAMELECH
        (bows)
        Thy will be done, Lord.

SABAOTH
        But first, break him. Strip him of the bonds from which he draws the strength to defy My divine will. Kill everybody he cherishes.

ADRAMELECH
        Everybody?

SABAOTH
        Save Naomi Bradleigh for last.

ADRAMELECH
        Lord, it would be safer to destroy Stormrider first. What he cannot protect, he will avenge. Nor will he be the only one seeking to avenge Naomi.

Lightning arcs out of the clear night sky, transfixing ADRAMELECH.

SABAOTH
        You dare question My will?

ADRAMELECH
        Only that I might see it done and thy kingdom brought forth. 'Tis only your victory that I seek, Lord.

The lightning ceases. ADRAMELECH does not speak immediately, but takes a moment to recover.

SABAOTH
        MORGAN STORMRIDER will fight all the harder to avenge those he loves, but to no avail. It is My will that he die knowing the full extent of his failure.

ADRAMELECH
        I understand now, Lord. Thy will be done, on Earth as it is in Heaven.

ADRAMELECH turns to leave.

SABAOTH
        Do not use Polaris. Their loyalties are divided, and they were no doubt Imaginos' tool from the beginning.

ADRAMELECH
        When we last spoke, they had insinuated themselves into Stormrider's circle. I shall destroy their backup before killing them, as well.

SABAOTH
        Thou art my good and faithful servant. In thee I am well pleased.

ADRAMELECH smiles, as if he had succeeded in bullshitting SHADDAI.

INT. MANHATTAN, LIMOUSINE DRIVING DOWN 5TH AVE - NIGHT
======================================================

*the present time...*
---------------------

Dark streets blur outside the tinted windows of the limousine as Isaac
Magnin pours brandy for Naomi Bradleigh and Annelise Copeland. Annelise
sits beside Isaac Magnin, and he treats her as if she were a former
lover for whom he still retains a strong fondness. Naomi sits across
from Isaac and Annelise, her strange sidesword resting across her lap.
She keeps one hand on the weapon's scabbard as she accepts her brandy.

ISAAC
        I figured you'd want a drink.

NAOMI
        (gently swirls the glass)
        I do, but that doesn't make drinking a good idea.

ANNELISE
        ISAAC, what did you do to NAOMI to earn such distrust?

ISAAC and NAOMI both look at ANNELISE. They answer in unison.

ISAAC
        It's complicated.

NAOMI ^
        It's complicated.

NAOMI glances at ISAAC before continuing.

NAOMI
        He's persuasive enough sober. It's harder to see through his bullshit after a drink or two.

ISAAC
        As if I'd take advantage of my own daughter.

NAOMI
        You would, you have, and you will undoubtedly try to do so again. I'm surprised you didn't offer me to Morgan as a prize, Imaginos.

ISAAC
        Please don't use that name around --

NAOMI
        Around Annelise? Spare me. I've read her letters.

ANNELISE
        Has Morgan read them, too?

ISAAC
        Of course he has. Why else would I have sent him the archive?

ANNELISE
        Those were private. Why would you do such a thing?

ISAAC
        Because it was necessary.

NAOMI ^
        Because he's an arsehole.

ANNELISE and ISAAC both stare at NAOMI. NAOMI finally sips her brandy.

NAOMI
        Don't look at me like that. You're both arseholes, and you richly deserve each other.

ISAAC
        You're being unfair to Ms. Copeland.

NAOMI
        Am I? Morgan is dead because of her.

ISAAC
        Morgan is dead because of his own hubris. He thought himself capable of defeating one of the ensof alone.

ANNELISE
        Failed? I was there. Whatever it was Morgan fought disappeared.

ISAAC
        He managed to destroy SABAOTH's avatar. That isn't the same as killing him.

NAOMI
        And you had some kind of magic that let you resurrect him?

ISAAC
        I had the technology, yes.

NAOMI
        Not that I'd mind seeing him again, but why do you need me?

ISAAC
        His last memory is of getting in your car after CHRISTABEL CROWLEY's sham funeral.

NAOMI
        Are you telling me he thinks he was in a car crash, and blames me?

ISAAC
        No. He's distraught because he thinks you were injured.

INT. ASGARTECH SPIRE, JOSEFINE MALMGREN'S OFFICE - NIGHT
========================================================

*8 hours ago...*
----------------

JOSEFINE paces inside her office, clutching a copy of *The Unix
Programming Environment* as if it were a talisman. A short-haired TUXEDO
CAT slips into the office via a cat door, and jumps up on JOSEFINE's
desk. It has a live mouse clutched in its jaws.

TUXEDO CAT
        (drops the mouse into JOSEFINE's coffee mug)
        Hi, Doc. I brought you a snack.

JOSEFINE
        (pulls a barely alive mouse soaked in stale coffee out of the mug)
        Thanks, ZERO, but I'm not hungry anymore.

ZERO
        Can I have it back, then?

JOSEFINE
        (puts the mouse in a box lined with tissues)
        You should give the poor thing time to rest, so it can play more later.

ZERO
        (purring)
        Oh, yeah. I can't believe I didn't think of that.

JOSEFINE
        You didn't come here just to show off your latest kill, did you?

ZERO
        No, but I've got to look like I do something around here besides lick my own balls and cough up hairballs on ISAAC MAGNIN's chair when he's not around.

JOSEFINE
        (gives ZERO a pointed look)
        Sometime before Ragnarok would be nice.

ZERO
        I checked on your new boyfriend, the dude you brought back from the dead. He's got a cat who looks like me curled up with him.

JOSEFINE
        You're not the only kitty emulator we've got roaming the Spire, ZERO.

ZERO
        *meow!* When did the AsgarTech Corporation start making kitty emulators big enough to replace ponies?

JOSEFINE
        Big enough to replace a pony?

JOSEFINE rushes from her office, and races to the room where MORGAN is being kept. Opening the door, she finds MORGAN scratching a GIANT CAT the size of a golden retriever behind its ears.

JOSEFINE
        How did that cat get here?

MORGAN
        (shrugs)
        Beats me. For all I know, he walks through walls.

The GIANT CAT's purring starts to resemble a biker convention as it rubs its face all over MORGAN.

JOSEFINE
        Is... is it safe to pet him?

MORGAN
        Probably. MORDRED has always been the friendly sort.

MORDRED purrs even louder as MORGAN says his name, and gives a chirping meow. JOSEFINE starts scratching behind MORDRED's ears, and he rolls over to expose a fluffy white belly.

ZERO
        Is that a cat or a cannoli?

MORGAN
        I suppose I shouldn't be surprised that your cat speaks English, DR. MALMGREN.

JOSEFINE
        ZERO was the first project I worked on at AsgarTech. Much of the tech we developed got reused to create asura emulators like you.

MORGAN
        You're overworked and look like shit, but you can't be any older than me. No way you were involved in creating asura emulators like me.

JOSEFINE
        But you're no longer part of the 100 Series. You've been upgraded.

MORGAN
        Bullshit.

JOSEFINE
        I don't get paid enough to bullshit you, Adversary. Not when you seem perfectly capable of doing it to yourself.

MORGAN
        I'm sorry. You must be tired, and I haven't been very cooperative.

JOSEFINE
        In a way, all of this is my fault.

MORGAN stops petting MORDRED, and fixes his gaze on JOSEFINE.

JOSEFINE
        If I hadn't helped CLAIRE develop the firmware patch you used to disable automatic incremental backups, I could have restored you with all of your memories.

MORGAN
        Including the moment of my alleged death. Do you truly think that would have been wise?

JOSEFINE
        What do you think?

MORGAN
        I think it would be as traumatic as remembering one's birth... or being aware of one's conception at the moment it happens.

JOSEFINE
        Only you were neither conceived, nor born. You aren't human. The sooner you accept that...

MORGAN
        That is not a premise you want somebody like me accepting. If I am not human, then of what concern is a human life to me?

JOSEFINE stares at MORGAN in horror as the implications dawn on her exhausted mind.

JOSEFINE
        I've been such an idiot. No wonder you want to see NAOMI. Not to mention CLAIRE, EDDIE, SID, SARAH, and the rest.

MORGAN
        Don't you think they'd want to know that I'm not dead?

JOSEFINE
        Look, you're the first asura emulator we've ever resurrected. We don't know what we're doing. I'll tell DR. MAGNIN and DR. DESDINOVA that you need visitors.

INT. ASGARTECH SPIRE, CONFERENCE ROOM - NIGHT
=============================================

*the present time...*
---------------------

This corporate conference room is set apart from others not only by its
size, but by the gentleness of its lighting and the opulence of its
furnishings. The long table and well-padded chairs exhibit a level of
craftsmanship worthy of Versailles during the reign of the French tyrant
Louis XIV of the Bourbon dynasty.

Two men sit at the far end of the table. One of them, MORGAN STORMRIDER,
is watching video on a wall-mounted display. The other man has blue
eyes, long gray hair, and wears a tailored charcoal gray suit under a
white physician's lab coat, and he's watching MORGAN. This is DESDINOVA,
who goes by no other name despite the risk of exposure he and other
membes of IMAGINOS' cabal face.

EXT. RIO DI JANEIRO, SOCCER FIELD, DAY
======================================

*3 months ago...*
-----------------

MORGAN STORMRIDER has been cut up, beaten up, and burned. His armor is
ruined. His sword is a jagged stump that might only be good for shaving.
He glances around the stadium, sees that the evacuation is still in
progress, and tightens his grip on his broken sword for a moment. It is
the only outward sign of his fear he allows himself to display.

CLOSER, we see resolve, pride, and righteous anger in Morgan's eyes as
he stands tall and defiant before a towering inferno that appeared in
the middle of a soccer game and spoke with a voice out of a whirlwind.

We know that this voice from the whirlwind is the ensof SABAOTH (lord of
hosts), who calls himself SHADDAI (the almighty), but all Morgan knows
is that it has killed innocent people after demanding obedience, and is
thus a tyrant who must be put down.

SABAOTH
        How long do you think you can defy me, little asura? How long before you fall to your knees in fear and trembling and confess that I am the Lord thy God?

MORGAN
        If you want me to kneel, then break my legs. If you are the God you claim to be, it should be easy for you.

An arc of lightning lances at Morgan from the infernal tornado taking up
most of the stadium. Rather than attempt to dodge it, Morgan *catches
the thunderbolt* in one hand as if he were a legendary Japanese sword
saint catching an arrow in midflight.

MORGAN
        That's right. Keep throwing your power at me. Give me everything you've got. I can take it.

SABAOTH
        Such hubris is unbecoming, little asura.

A storm descends upon Morgan, its lightning pounding him like an
A-10's 30mm gatling ripping apart a tank. When it dissipates, Morgan
is still there, though his clothes are in worse condition than before.
Electricity dances along his skin as some of the power he absorbed leaks
out.

Screaming with psychotic fury, Sabaoth redoubles his assault. He drives
much power into his attack on Morgan that the ground on which he stood
is vaporized.

But Morgan is still there at the bottom of the crater, still on his
feet, and still defiant.

SABAOTH
        Impossible...

MORGAN
        (leaping out of the crater)
        You don't get it, do you? This is *my* world. This is where I live. Everything I love is here. Everything that made me what I am is here. What you do to the least of all of the creatures of this world, you do to *me*.

SABAOTH
        You're nobody's savior.

MORGAN
        That's right. I'm not Jesus. *I do not forgive.*

Morgan throws away his broken sword. A sword of deep purple flame blazes
to life before him and he grasps it without fear that it will burn him.
He no longer cares; he is committed to striking Sabaoth down even if it
takes a kamikaze attack to do the job.

MORGAN
        I am an Adversary, sworn not to the service of the Phoenix Society, but by my own choice. This is the sword of my rage, the blade of my hatred. In my own name I wield it and accept its price, and with it I will strike you down for your sins against the world.

Morgan points the weapon forged from his very soul at Sabaoth, who has
taken the form of a crowned beast out of the Book of Revelation.

MORGAN
        This is your last chance, false God. Leave this star system, never to return on pain of death.

INT. ASGARTECH SPIRE, CONFERENCE ROOM - NIGHT
=============================================

*the present time...*
---------------------

MORGAN STORMRIDER stares at the screen, which has gone black because
the feed had cut out. The other man studies Morgan for a moment before
speaking.

DESDINOVA
        Are you sure you don't remember any of this?

MORGAN
        (indignant)
        Of course not.

Morgan extends a short blade of deep purple flame from his fingertip.
Letting it blink out, he points at the screen.

MORGAN
        I've always had certain preternatural abilities, and I've always suppressed them. I've never *flaunted* them as if I were...

DESDINOVA
        (smirking)
        One of your friend Claire's *shonen* manga heroes?

MORGAN
        Exactly.

MORGAN stands, and leans over Desdinova.

MORGAN
        Speaking of which, where is Claire? Where are my friends? Why the *hell* do you have me in a Faraday cage?

INT. ASGARTECH SPIRE, LOBBY - NIGHT
===================================

NAOMI BRADLEIGH leads EDMUND COHEN, SID SCHNEIDER, CLAIRE ASHECROFT,
SARAH KOHLRYNN, and even MUNAKATA TETSUO into the AsgarTech Spire's
lobby. She draws her sword, and the others draw weapons of their own, as
security guards rush to stop them.

NAOMI
        Gentlemen, you aren't getting paid enough to get in our way. Put your weapons away and--

CLAIRE
        (brandishes a rune-covered black cricket bat)
        Bugger off before I bugger the lot of you with Cluebringer.

NAOMI
        (sighs as the guards raise their weapons)
        Dammit, Claire.

A melee ensues as Naomi and the others draw into a tight knot, guarding
each other's backs as they fight security guards outnumbering them two
to one.

CLAIRE
        (already beating up one guard)
        Arioch! Arioch! Hookers and blow for my lord Arioch!

EDMUND
        Goddammit, Claire. Try to take this seriously.

The temperature drops to sub-zero levels. Frost coats every surface as
ISAAC MAGNIN enters the lobby with ANNELISE COPELAND in tow. MAGNIN does
not raise his voice, but he doesn't need to.

ISAAC
        Stand down.

One guard doesn't get the message.

GUARD
        But Dr. Magnin, they came armed.

ISAAC
        They are Adversaries. Of course they would come armed. They are also my honored guests, and are to be afforded every courtesy.

GUARD
        But --

ISAAC MAGNIN stares into the guard's eyes.

ISAAC
        Every. Courtesy.

INT. ASGARTECH SPIRE, CONFERENCE ROOM - NIGHT
=============================================

DESDINOVA attempts to reason with MORGAN STORMRIDER, who isn't
impressed.

DESDINOVA
        We kept you isolated for your own good. We weren't sure what your mental state would be like immediately after resurrection.

MORGAN
        You keep talking about resurrection, but that's impossible. Why not just tell me I was grievously injured, and had just come out of a coma?

DESDINOVA
        How many times have you taken a bullet to the head?

MORGAN
        A couple of times. The round just goes around my skull instead of through it, and I wake up with a headache.

DESDINOVA
        No. The round went *through* your head, and your higher functions shut down while you repaired yourself. After you destroyed Sabaoth's avatar, your body failed to repair itself. It was too heavily damaged.

MORGAN
        Bull--

The doors swing open and strike the wall, as if forced open by a mighty
blow. NAOMI BRADLEIGH does not break stride despite having kicked
the doors open. She stalks across the length of the conference room,
advancing upon Morgan.

MORGAN
        Naomi?

Naomi plunges her hands into Morgan's mane of glossy blue-black hair and
kisses him with the ferocity one normally expects from a male lead in
a bodice-ripper as he ravishes the heroine. Once she's satisfied that
she's kissed Morgan breathless, Naomi draws back and glares at Morgan.

NAOMI
        You bastard. I had to play Chopin's Funeral March in b-flat minor for you. 

Naomi bursts into tears and throws her arms around Morgan, who holds her
close.

ZOOMING IN, we see Naomi add in a softer voice, as if for Morgan's ears
only...

NAOMI
        Do you have any idea how *hard* it is to sight-read when you can't stop crying?

**TO BE CONTINUED...**
