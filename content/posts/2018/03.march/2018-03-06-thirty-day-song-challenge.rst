The 30 Day Song Challenge
#########################

:date: 2018-03-06 18:53
:category: Metal Appreciation
:tags: 30 days, mastodon, social media, silly, list, video
:summary: The 30 Day Song Challenge has been making the rounds on Mastodon. Here are my answers.
:summary_only: true

The 30 Day Song Challenge has been making the rounds on Mastodon. 

.. image:: {filename}/images/30-day-song-challenge.jpg
    :width: 40em
    :alt: a picture of a calendar with a different song challenge for each day

Here are my answers. Videos are subject to removal at any time, so don't be surprised if you can't play them all.

Day 1: A Song You Like with a Color in the Title
================================================

I've got a two-fer for this one.

Black Sabbath - "Black Sabbath"
-------------------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qrVKmTPFYZ8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Judas Priest - "Blood Red Skies"
--------------------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/aORXZiGeIdA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Day 2: A Song You Like with a Number in the Title
=================================================

I've got another two-fer here. :)

Iron Maiden - "2 Minutes to Midnight"
-------------------------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9qbRHY1l0vc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Emerson, Lake & Palmer - "Karn Evil 9"
--------------------------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fLS0Med0s6E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Day 3: A Song That Reminds You of Summertime
============================================

Time for some cock rock...

Edguy - "Trinidad"
------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/IFFS-C0YFjs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Day 4: A Song That Reminds You of Someone You'd Rather Forget
=============================================================

This one's for my first girlfriend.

Type O Negative - "Black No. 1 (Little Miss Scare-All)
------------------------------------------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/vFwYJYl5GUQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Day 5: A Song That Needs to be Played Loud
==========================================

The whole point of heavy metal is that you're supposed to play it loud, so it's hard to pick just one. I know just the thing, however... *Lay down your soul to the gods' rock 'n roll!*

Venom - "Black Metal"
---------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fHmzFVDjVnM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Day 6: A Song That Makes You Want to Dance
==========================================

Isn't this Rammstein's *raison d'etre*?

Rammstein - Moskau
------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WtBsKyndHVQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Day 7: A Song to Drive To
=========================

So *many* to choose from! But let's go with one that isn't actually *about* driving.

The Sisters of Mercy - "Temple of Love 1992 (featuring Ofra Haza)"
------------------------------------------------------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/TMETa77dUrg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

