"Does Anybody Actually Use Desktop Linux?" I Did.
#################################################

:date: 2018-01-17 17:03
:category: Just Tech Things
:tags: coding, desktop, games, linux, music, openbsd, use case, video, writing
:summary: An unnamed user asked Quora: "Does anyone actually use desktop Linux?" The answer is that I do, or did. Now I use OpenBSD. If you want to know why, read on.
:slug: does-anyone-actually-use-desktop-linux-i-did
:summary_only: true


An unnamed user asked Quora: `"Does anyone actually use desktop Linux?" <https://www.quora.com/Does-anyone-actually-use-desktop-Linux>`_. The answer is that I do, or did.

I used desktop GNU/Linux *exclusively* at home, and would only use Windows at my day job. I used multiple distributions, starting with Red Hat and then SuSE way back in 1998, and then trying every distribution that caught my interest. I've still got a copy of Slackware 8.0 on CD-ROM in my box of old software media. I even managed to install Gentoo Linux from stage one *on a dialup connection* back in 2003 (using a US Robotics Model 5610 internal modem, incidentally). When I got married I had allowed myself less time to tinker because I didn't want to screw up the relationship, so I switched to Ubuntu when that first came out, and eventually stared using Debian via Crunchbang once Ubuntu abandoned GNOME in favor of Unity. I've run Arch. The last distro I used was Solus.

But that doesn't answer your question; it merely establishes that I've been using GNU/Linux long enough that I had to have done *something* with it. Here's what I did with desktop GNU/Linux:

* I ripped CDs to Ogg Vorbis (and now FLAC) using tools like gRIP and abcde.
* I rocked out to my ripped CDs with XMMS, cmus, MOC (music on console), and Quod Libet.
* I wrote short stories like "The Milgram Battery" and novels like *Without Bloodshed* using text editors like Vim and Emacs.
* I wrote shell scripts and other little programs for my own use, again with Vim or Emacs.
* I built my own website on Linux, hand-coding HTML and CSS.
* I surfed the web using Mozilla Firefox.
* I revised my writing for publication using LibreOffice.
* I courted my wife for four years (2000-2004) between the northeast US and southeast Australia using AIM protocol with IM clients like Pidgin and email with clients like Sylpheed and Claws Mail (originally sylpheed-claws).
* I edited photos and made lolcats using the GIMP.
* I watched videos compressed using open codecs using mplayer and VLC.
* I played games. Lots of games. Maybe not as many as are available on Windows, but for a couple of years it was enough that I could run Neverwinter Nights. These days there's a respectable selection on Steam. :)

I did it all on used computers like a Lenovo ThinkPad T430s and a ThinkCentre M92p, though I also bought a Pangolin Performance laptop from `System76 <https://system76.com>`_ back in 2012 that still runs just fine. That's all in the past, because I stopped using GNU/Linux in October 2016. Now I use `OpenBSD <https://openbsd.org>`_.

If you're still reading this, you might wonder *why* I use Linux/BSD at home. I have a few reasons.

* I was first exposed to Unix in college. Sitting down in front of a Sun Microsystems SPARCstation running SunOS after dealing with PC clones running DOS and Windows 3.1 felt like a huge step *upward*. It wasn't big iron, but it was bigger iron than what I could buy at `Nobody Beats the Wiz <https://en.wikipedia.org/wiki/The_Wiz_(store)>`_. When my instructor told me it was possible to run a Unix-style OS on a PC clone, I just had to try it and see for myself.
* Using desktop Linux was a challenge, and it was also something of an act of defiance &mdash; a way to raise my middle finger to big corporations who made the use of computers a requirement for functioning in modern society so that they could turn a profit by filling a "need" that never previously existed.
* Tinkering with desktop Linux and trying to customize the interface can be a fun way to waste a rainy day.
* Everything makes *sense* on Unix. If something breaks, chances are it's because I did something stupid and I can probably fix it without having to wipe the drive and reinstall the OS.

All of the above also applies to OpenBSD, only more so because OpenBSD can trace its ancestry back to Bell Labs and Unix via BSD.
