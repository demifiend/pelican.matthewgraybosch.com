Ideas for the Plot of Shattered Guardian
########################################

:date: 2018-01-30 10:25
:category: Starbreaker
:tags: backup, cemetery gates, death, digital media, distrust, ensof, frame story, ideas, isaac magnin, memory loss, morgan stormrider, naomi bradleigh, pantera, plot, resurrection, sabaoth, shattered guardian, without bloodshed
:summary: I finally have a really good idea for how my next novel Shattered Guardian should go, but to explain it I have to provide details from previous works, so this post has lots of spoilers. Don't say I didn't warn you!
:slug: ideas-plot-shattered-guardian
:summary_only: true

This post concerning my ideas for the plot of *Shattered Guardian* (a sequel to *Without Bloodshed*) started out as a `series of toots on Mastodon <https://octodon.social/web/statuses/99434709929770632>`_, so it might be a little disjointed. If you've been reading my stuff, you'll know most of what I explain below to provide context and explain why I want *Shattered Guardian* to go a certain way. Otherwise, consider this your spoiler alert. :)

.. image :: /images/starbreaker-cast-collage.png
	:width: 683px
	:height: 360px
	:alt: Some of the cast of Starbreaker. Art by Harvey Bunda commissioned by the author
	:align: center
	:target: /images/starbreaker-cast-collage.jpg

I've been thinking a lot about how I want to go about writing my next novel, which I been calling *Shattered Guardian*, but there are some problems.

1. *Shattered Guardian* is a sequel to a novel I published in 2013: *Without Bloodshed*.
2. I've changed and learned in the last five years, and where I want to take the overall series has changed as well.
3. I could solve the first two problems by punching the reset button, but that would alienate fans and my wife would kill me.

What Happened Before *Shattered Guardian*?
==========================================

At the end of *Without Bloodshed*, the protagonist Morgan swore to expose Isaac Magnin's wrongdoing and bring him to trial. He knows Isaac is behind the Phoenix Society's corruption, but as an officer of the Phoenix Society Morgan can't publicly accuse Isaac Magnin without an armor-plated case.

The problem is that I want to start with a big bang, something interesting that hits the reader upside the head, not a tedious search for evidence of executive wrongdoing.

Why Not Start With the Investigation?
=====================================

Part of this is that Isaac Magnin himself doesn't have time for Morgan to follow due process. He needs Morgan to confront him ASAP for reasons of his own.

Part of this is my failing. I probably COULD write a nailbitingly tense search for crucial evidence that Isaac Magnin has been using the Phoenix Society for his own ends.

I just don't want to. At least, that's not how I want to *start* the novel. So, how do I want to start *Shattered Guardian*?

Cemetery Gates
==============

I know how I want to begin "Shattered Guardian". I want to begin with the aftermath of Morgan's death. Blame the late, great Dimebag Darrell and Pantera if you like.

.. raw:: html

	<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WyczwqRD2NI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Morgan is one of 666 100-series asura emulators, androids designed by Isaac Magnin and his cabal to kill demons called "ensof". The thing is, Isaac Magnin and most of his cabal are themselves ensof. They just don't want the species from which they sprang subjected to genocide.

The ensof they oppose is imprisoned under Mt. Erebus in Antarctica because it has a nasty habit of trying to commit genocide by appearing before people, claiming to be God, and demanding a holy war. It calls itself Shaddai ("the almighty" or "the destroyer", but its enemies call it Sabaoth ("lord of hosts").

Sealed Evil Never Stays in the Can
==================================

So, what's the deal with Sabaoth? Well, he's the demon Isaac Magnin became a demon in order to fight.

Sabaoth got loose, promptly appeared before a crowd of people attending a sporting event, and promptly demanded that they worship It and prove their devotion by killing on command. When the people refused, it began massacring them. Morgan blames himself for Sabaoth getting loose because his defeating Isaac Magnin in a duel somehow disrupted the spell keeping Sabaoth contained.

Using abilities he had previously suppressed, Morgan attempts to fight Sabaoth while his friends work with local authorities to evacuate the area. He succeeds in destroying Sabaoth's avatar, but the effort kills him. He holds out just long enough for Naomi to see the horror he's made of himself, and it traumatizes her.

Mourners Please Omit Flowers
============================

So we begin with Naomi Bradleigh, Morgan's current girlfriend (and bandmate) visiting his grave. She finds his ex, Christabel Crowley (also their former bandmate) who was supposedly murdered. Crowley admits that she faked her death and had resumed her original identity: Annelise Copeland.

Before Naomi can question Annelise, Isaac Magnin shows up to request their help. He was able to restore Morgan's memories from backup to a new body, but there's a problem.

This is Why You Do Offsite Backups
==================================

The problem is that Morgan refuses to accept that he died. He knows that he's not in his original body. However, because he had disabled automatic incremental memory backups due to his distrust of Isaac Magnin and the Phoenix Society, Morgan was restored with no memory of what had happened after the events of "Without Bloodshed", and refuses to believe any audio, visual, or textual accounts of events.

He'll only trust the accounts of his friends, and his enemies.

Morgan's Ulterior Motive
========================

At least, that's the justification Morgan will give for demanding that Isaac Magnin and his cabal tell their stories of what happened along with his friends.

Morgan has an ulterior motive. He hopes to not only fill in the gap in his own memory, but obtain confessions from Isaac Magnin and his co-conspirators. In addition hopes to force Magnin to accept a bargain: Morgan and his friends will help, but Magnin and his cabal must stop trying to manipulate them, and they must stand trial for everything they've done.

What do you think, if you've followed this far? Is this a workable premise? Is this something you would be interested in reading?
