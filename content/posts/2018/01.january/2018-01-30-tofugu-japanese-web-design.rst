Tofugu on Japanese Web Design
#############################

:date: 2018-01-30 13:42
:category: Just Tech Things
:tags: japan, tofugu, web design, old-school, 1990s, three column layout
:summary: Tofugu has an interesting article on Japanese web design. You should read it, since it describes the aesthetic elements of Japanese web design and why they exist.
:slug: tofugu-japanese-web-design
:summary_only: true

Tofugu has an interesting article about `Japanese Web Design <https://www.tofugu.com/japan/japanese-web-design/>`_ and why it's so different from US and European design. If you've ever been to a .co.jp site, you'll recognize these elements of the Japanese web aesthetic.

* Lots and lots of tightly packed text
* Tiny images
* Three-column layouts (making this not look like utter shit was the `"holy grail" <https://philipwalton.github.io/solved-by-flexbox/demos/holy-grail/>`_ of web design before smartphones ruined the web)
* poor or minimal use of whitespace and padding
* links are usually blue
* it's a fucking mess

You'll want to read the article to understand why. There are legitimate reasons.
