TIL: 1 in 8 new adults in Japan are not Japanese
################################################

:date: 2018-01-14
:category: As The World Burns
:tags: coming of age, culture, customs, japan
:summary: Out of every eight young women celebrating the traditional coming-of-age in Japan, one is not counted as Japanese. They are neither of the tribe nor citizens.
:slug: til-1-8-new-adults-japan-not-japanese
:summary_only: true


I just saw a moderately interesting article in the `Japan Times <https://www.japantimes.co.jp/news/2018/01/10/national/social-issues/coming-age-1-8-new-adults-tokyo-not-japanese-ward-figures-show/>`_ courtesy of `@wintermute@cybre.space <https://octodon.social/web/statuses/99345356969917907>`_. 

Reiji Yoshida reports that in Tokyo, 1 out of every 8 young women celebrating their coming of age in Japanese culture are *not* Japanese citizens. Nor were they born into Japan's dominant Yamato tribe. 

It must feel strange to work hard to fit into a given society and culture, but not be acknowledged as part of that society or culture or be counted as a citizen. 

Not that the USA is consistently better in this regard, but we are long overdue for a national discussion on what it even means to be an American. It won't be pretty.
