The Informed Consent of the Governed
####################################

:date: 2018-01-26 13:48
:category: Starbreaker
:tags: capitalism, claire ashecroft, collapse, corruption, edmund cohen, exposure, informed consent, manufactured consent, morgan stormrider, naomi bradleigh, shit my characters say, sid schneider, social engineering, truth
:summary: This is some dialogue I will eventually work into one of my Starbreaker novels. Morgan and his friends know the truth about the Phoenix Society, and eventually decide that the secret must be revealed so that everybody governed by it can give informed consent.
:og_image: /images/starbreakercollage.jpg
:slug: informed-consent-governed
:summary_only: true


This is some dialogue I will eventually work into one of my Starbreaker novels. Morgan and his friends know the truth about the Phoenix Society, and eventually decide that the secret must be revealed so that everybody governed by it can give informed consent.

**Trigger warning**: *The following touches on race and class issues in a fictional capitalist society, and uses rape as a metaphor for the rich exploiting everybody else. Reader discretion advised.*

.. image:: {filename}/images/starbreakercollage.jpg
	:alt: Some of the cast of Starbreaker. Art by Harvey Bunda commissioned by the author
	:align: left

NAOMI
	I know informed consent is important, but think about the consequences of telling the world that Liebenthal and Mellech were right about the Phoenix Society. The public would panic and think we're trying to start a second Nationfall. There'd be chaos. 

MORGAN
	You think I don't know that?

SID
	Bro, you don't know shit. My father would say you're too *white* to know shit, and maybe he's right, but the real problem is that your whole *life* depends on you not knowing shit.

MORGAN
	You think we don't want to rock the boat because working for the Phoenix Society as an Adversary made our lifestyles possible?

NAOMI
	It's deeper than that. I might not have had my musical career, or it might have followed a different trajectory, but I might still have found a way to study my art and perform. Without the Asura Emulator Project and the Society's under-the-table dealings with Ohrmazd Medical and AsgarTech, you wouldn't exist. Sid doesn't think you can accept that.

SID
	Of course he can't. Just like he can't accept that he isn't human, and that all the rhetoric about individual rights, equality under the law, and justice for all are nothing *but* rhetoric.

MORGAN
	Why do you think I retired?

SID
	Retired, my ass. You were right there beside me when I *finally* got to raid Murdoch Defense Industries and shut it down. Don't get me wrong: I'm glad you were. But you saw how the Phoenix Society fucked you afterward, how they faked the Witness Protocol footage to make it look like *you* killed Victoria Murdoch.

NAOMI
	I'm surprised you didn't say 'murdered'.

SID
	That bitch all but murdered my father. Not even fifty and he's nothing but a broken old man fingerpickin' twelve-bar blues on a guitar I gotta tune for him because he's too goddamn deaf to realize half the strings are half a step flat and the others half an octave.

MORGAN
	You're saying the Phoenix Society used me the way Murdoch used your father.

SID
	It ain't the Phoenix Society. It's the motherfuckers running it. Not just Imaginos. The whole damn XC.

EDDIE
	I resemble that remark. You know they just keep me around because it's cheaper than killing me, right?

SID
	Bullshit. They figured that if they kept you in booze, drugs, and whores you'd eventually take care of yourself.

EDDIE
	Probably would have, too.

CLAIRE (walking in)
	I heard you clowns arguing from the kitchen. I've tried to tell Morgan for years that the Phoenix Society is just how the capitalists go about getting our consent, putting on a condom, and lubing us up before they fuck us.

NAOMI
	Isn't that an improvement over how the powerful treated others before Nationfall?

CLAIRE
	Sure, it's better than how they used to do us: bareback, dry, and without so much as a 'please' or 'thank you'. But I think Sid's asking if we should be getting fucked at all.

SID
	Damn right I am. Morgan, you know damn well the whole damn system is corrupt. I think we should just burn the motherfucker down, but it ain't just my decision. Likewise--

MORGAN
	It's not just Naomi's decision or mine to attempt reform. We all need to have a hand in deciding how our societies should be governed.

NAOMI
	And that means we must expose the Phoenix Society's corruption so that everybody can make an informed decision.

MORGAN
	Regardless, the Executive Council needs to be taken down. They re-engineered our entire civilization to suit their purposes, and that cannot go unpunished.
