The BNR Metal Pages
###################

:date: 2018-01-13 23:30
:tags: community, fandom, heavy metal, reference, bnr
:summary: The `BNR Metal Pages <http://www.bnrmetal.com>`_ was once of the first websites I discovered when I first got online back in 1996, and they're still around over 20 years later. If you've just gotten your first taste of heavy metal, you'll discover lots of new bands here. And if you're long past your golden age of leather, you'll find lots of memories.
:slug: bnr-metal-pages


The `BNR Metal Pages <http://www.bnrmetal.com>`_ was once of the first websites I discovered when I first got online back in 1996, and they're still around over 20 years later. If you've just gotten your first taste of heavy metal, you'll discover lots of new bands here. And if you're long past your golden age of leather, you'll find lots of memories.
