Elder - Reflections of a Floating World
#######################################

:date: 2018-01-24 12:22
:category: Metal Appreciation
:tags: elder, bandcamp, progressive metal, stoner metal, groove, jam
:summary: Here's another excellent album I found on Spotify: Reflections of a Floating World by Elder. Check it out if you like long, heavy progressive rock jams.
:slug: elder-reflections-floating-world
:summary_only: true


Here's another excellent album I found on Bandcamp: *Reflections of a Floating World* by `Elder <https://beholdtheelder.bandcamp.com>`_. Check it out if you like long, heavy progressive rock jams.

This is brilliant stuff if you need a background album at work, for long drives, or can just sit quietly and listen with your full attention on the music.

    Reflections of a Floating World is Elder's fourth full length album and second LP released via Stickman Records (EU) and Armageddon Label (US). Long, undulating and dense tracks float between psychedelic passages and progressive rock without missing a beat; adventurous and unpredictable songs are punctuated by hypnotic jams, all colored by the tendency toward melody and dynamism that has become the band's hallmark. In keeping with their motto of expanding and expanding upon their repertoire, guest musicians Mike Risberg and Michael Samos joined the core three in the studio to add extra guitar, keys and pedal steel, adding vibrancy and lushness to the album. In all regards, Reflections shows a band with a clear vision honing their skills with every year.

See for yourself...

.. raw:: html

    <iframe style="border: 0; width: 350px; height: 654px;" src="https://bandcamp.com/EmbeddedPlayer/album=2796246848/size=large/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="http://beholdtheelder.bandcamp.com/album/reflections-of-a-floating-world">Reflections of a Floating World by Elder</a></iframe>
