Imagining a World Without Jobs
##############################

:date: 2018-01-20 17:56
:category: Agitprop
:tags: antiwork, bullshit jobs, full unemployment, post-work
:summary: Try imagining a world where you weren't expected to find joy, emotional fulfilment, and purpose at jobs consisting of busting your ass to make rich people even richer for at least eight hours a day and at least five days a week.
:slug: imagining-world-without-jobs
:summary_only: true


Try imagining a world where you weren't expected to find joy, emotional fulfilment, and purpose at jobs consisting of busting your ass to make rich people even richer for at least eight hours a day and at least five days a week. 

Try imagining a world where you weren't expected to be grateful to have a job, but were *honored* for your willingness to work for somebody else &mdash; somebody who understood and was grateful for the sacrifice you make by working for them instead of for yourself.

Or just try reading `"Post-work: The Radical Idea of a World Without Jobs" <https://www.theguardian.com/news/2018/jan/19/post-work-the-radical-idea-of-a-world-without-jobs>`_ in *The Guardian*. 

We could do it. We could build a world where nobody needs to do without the basics (food, water, shelter, clothing, access to information and communications), and paid work is only something you do when you want extras. We have the resources. We have the technology.

So, how exactly to we get to **full unemployment**? Most workers are too indoctrinated to buy into the notion of a permanent general strike and simply refusing to ever work for anybody but themselves or their families again.

You could also read David Graeber's `"On the Phenomenon of Bullshit Jobs" <https://strikemag.org/bullshit-jobs/>`_, but his article is flawed in that the notion of "bullshit jobs" implies that there are some jobs that aren't bullshit.

That's right. In a bullshit economy, all jobs are bullshit jobs. Money is bullshit, too. Maybe central planning by the richest among is just as shitty an idea as central planning by government bureaucrats?

It's like a Russian joke that was popular when the Soviet Union was falling apart goes: `"They pretend to pay us. We pretend to work." <http://www.ginandtacos.com/2013/06/20/on-motivation/>`_
