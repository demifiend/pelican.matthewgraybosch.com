Advertisement: "The Milgram Battery"
####################################

:date: 2018-01-16 20:33
:category: Starbreaker
:tags: authority, creative commons, defiance, desdinova, edmund cohen, isaac magnin, karen del rio, milgram battery, morgan stormrider, obedience, ordeal, phoenix society, saul rosenbaum, short story, stanley milgram, test, trial
:summary: Before Morgan Stormrider may take his oath as an Adversary he must prove himself by facing the Milgram Battery, a series of tests that will force him to choose between obeying his conscience and obeying authority.
:slug: milgram-battery-now-free-read-online


Before Morgan Stormrider may take his oath as an Adversary he must prove himself by facing the Milgram Battery, a series of tests that will force him to choose between obeying his conscience and obeying authority.

"The Milgram Battery" is now free to `read online </stories/milgram-battery/>`_.
