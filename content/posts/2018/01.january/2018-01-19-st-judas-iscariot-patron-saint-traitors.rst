St. Judas Iscariot, Patron Saint of Traitors?
#############################################

:date: 2018-01-19 13:33
:category: Why I'm Going to Hell
:tags: betrayal, catholicism, christianity, counterfactual, greater good, judas iscariot, necessary evil, sainthood, theology, treason, worldbuilding
:summary: Why is Judas Iscariot vilified? Should he not be numbered among the first and greatest of saints? It doesn't make sense.
:slug: st-judas-iscariot-patron-saint-traitors
:summary_only: true


Something occurred to me while I was driving home from work last night, something that's always bugged me about Christianity that is just one of the many reasons I don't believe in Christianity (or any other religion): Why is Judas Iscariot vilified? Should he not be numbered among the first and greatest of saints? It doesn't make sense.

I'm not the first to think this. Back in 2001, Graeme Davidson over at Theological Editions made `a case for St. Judas Iscariot <http://www.theologicaleditions.com/Features/previousfeatures/judas.htm>`_:

    But even if the attempt to force Jesus' hand as a warrior messiah was not Judas' motivation, Judas is necessary to bring in the kingdom that Jesus intended. At the last supper, according to John's Gospel (Jn 13:18-35), Jesus appears to collude with Judas as the disciple chosen to fulfil scripture to betray him. Jesus certainly does nothing to dissuade Judas from the action that they both know he is about to perform. As Judas leaves to sell his Master to the authorities, Jesus even implies that what Judas is doing is so that the 'Son of Man may be glorified and God glorified in him'.

Think about it for a moment. Even if Jesus was lying about being the Son of God, he still had teachings he wanted to sell to the people of Israel. To sell them on the teachings, he had to sell them on the notion that he wasn't just Joseph the Carpenter's no-good son Joshua who went around preaching instead of learning his father's trade, but the son of God Almighty.

To sell the people of Israel on his divine nature, Jesus needed to stage a demonstration. Healing sick and disabled people didn't cut it. Casting out demons didn't cut it. But death and resurrection? Now we're talking.

Of course, the problem is how to stage the whole death and resurrection bit. Jesus couldn't just go to Pilate and tell him he wanted to con the Hebrews into thinking he was a demigod so they would buy into his moral philosophy. Instead, he had to sell the Romans occupying Israel on the notion that Jesus wasn't just some rabble-rouser who would go away if ignored long enough, but a genuine threat to their power.

Fortunately, many of the Israelites who had heard Jesus preach and witness his lesser miracles were happy to do most of the work themselves by proclaiming him king and messiah. As Graeme Davidson notes, however:

    If there was no Judas Iscariot to betray the nightly hideaway of Jesus and the disciples, the authorities may have resorted to publicly arresting Jesus. And a public arrest might have been the spark that would incite the rebellion and casualties the Jewish authorities were trying to avoid. More importantly, a public rebellion in support of Jesus could well have confused Jesus' followers as to the true nature of his mission and the kind of kingdom God intended. It was therefore necessary to the completion of Jesus' mission and to the disciples' clear understanding of the nature of that mission that the authorities arrest Jesus surreptitiously. Judas enabled that to happen.

So, why was Judas the fall guy? Why, out of all the other apostles? Peter denied knowing Jesus three times, but Judas is the asshole? I don't buy it.

I think it makes more sense to acknowledge Judas Iscariot as the patron saint of traitors, especially those whose betrayals are a necessary evil that serves a greater good.

Fortunately, in my `Starbreaker </starbreaker/>`_ stories, I can pretty much do as I please. And it pleases me to think that in the Starbreaker setting Catholics doing evil for the greater good might pray to St. Judas Iscariot to intercede on their behalf.

Then again, the Starbreaker setting also features a "Gospel of Judas" that depicts Helel (or Lucifer as he was later known) as a Promethean figure who willingly became *ha-Satan*, the Adversary, to give humanity the flame of defiance out of love for God and humanity alike. Why? He understood that resistance to tyranny is obedience to God.
