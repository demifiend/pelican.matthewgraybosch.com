Dead Women, Angry Men, Toxic Masculinity, and Jung
##################################################

:date: 2018-01-23 8:10
:category: An Unexamined Life
:tags: fiction, jung, psychology, toxic masculinity, anima, animus, shadow, murder, metaphor, allegory
:summary: Every novel about a dead woman and an angry man is a confession of allegorical (or metaphorical) murder at the author's hands.
:slug: dead-women-angry-men-toxic-masculinity-jung
:summary_only: true


If you ever wondered why there's so much fiction about dead women and angry men (probably written almost exclusively by cisgendered heterosexual men), blaming toxic masculinity is probably a safe bet. 

Every novel about a dead woman and an angry man is a confession of allegorical (or metaphorical) murder at the hands of an author who felt forced to murder the feminine aspects of his psyche to be accepted as a man by his society. 

No, I'm not getting Freudian on you. I'm getting Jungian. 

    Every man carries within him the eternal image of woman, not the image of this or that particular woman, but a definite feminine image. This image is fundamentally unconscious, an hereditary factor of primordial origin engraved in the living organic system of the man, an imprint or "archetype" of all the ancestral experiences of the female, a deposit, as it were, of all the impressions ever made by woman-in short, an inherited system of psychic adaptation. Even if no women existed, it would still be possible, at any given time, to deduce from this unconscious image exactly how a woman would have to be constituted psychically. The same is true of the woman: she too has her inborn image of man.

    -- Carl Jung, "Marriage as a Psychological Relationship" (1925) In *CW 17: The Development of the Personality.* P.338

For Jung, the process of individuation and spiritual healing begins with confronting one's shadow, the aspects of an individual's personality that people hide and repress when forming a persona with which to face the world.

    Unfortunately there can be no doubt that man is, on the whole, less good than he imagines himself or wants to be. Everyone carries a shadow, and the less it is embodied in the individual's conscious life, the blacker and denser it is. If an inferiority is conscious, one always has a chance to correct it. Furthermore, it is constantly in contact with other interests, so that it is continually subjected to modifications. But if it is repressed and isolated from consciousness, it never gets corrected.

    -- Carl Jung, "Psychology and Religion" (1938). In *CW 11: Psychology and Religion: West and East.* P.131

But once an individual has faced the darkness within them, there's a further step to individuation. As a person must face and accept their shadow, so must they face their anima or animus, the aspects of their personality that they have not fully developed because they do not suit the gender role assigned to them.

Rather than face and accept the feminine within, boys in Western society are often pressured to purge themselves of any trace of femininity to prove to themselves and society that they are men. Their murder of the anima is a traumatic self-sacrifice that reverberates throughout their lives.

Some do not survive it, and become mass murderers if they manage to get their hands on firearms. Others re-enact the experience in art, hence my characterization of novels about dead women and angry men as allegorical or metaphorical confessions of murder by their authors.

If you want better art, we need to make a better world -- a world that does not demand of men that they disown or outright destroy their feminine aspects.

On a lighter note, "Dead Women and Angry Men" is probably a rejected `Volbeat <http://volbeat.dk>`_ album title. It fits the pattern of previous titles like *Outlaw Gentlemen and Shady Ladies*.
