Metalheads Do Gatekeeping, Too. It Still Sucks.
###############################################

:date: 2018-01-17 20:38
:category: As The World Burns
:tags: casual, elitism, fake fans, fandom, gatekeeping, metalheads, poser, relationships, snobbery, social advice
:summary: Unfortunately, metalheads aren't immune to the tendency toward gatekeeping one finds in too many other fandoms. We even enshrine it in song with lyrics like Manowar's "wimps and posers, leave the hall!"
:slug: metalhead-gatekeeping-still-sucks
:summary_only: true


I saw this on Quora while having a shit: `"How should I deal with a poser metalhead friend? She talks about metal every second just for seeking attention without even knowing the lyrics!" <https://www.quora.com/How-should-I-deal-with-a-poser-metalhead-friend-She-talks-about-metal-every-second-just-for-seeking-attention-without-even-knowing-the-lyrics>`_

The submitter had a bit more to say...

    I'm a metalhead and when she foundout she asked me to be frnds and i accepted but then i foundout that she is a poser and The only bands she knows are linkin park and bvb and she talks about them every freakin second at school and she made everyone hate metal!

Here's my answer...

Is she a poser, or is she just starting out? When I first got into metal the only bands I knew were Black Sabbath and the Blue Öyster Cult, and many would argue that the BÖC isn’t metal but hard rock. I caught a lot of shit from kids like you who thought they knew so much.

Unfortunately, metalheads aren't immune to the tendency toward gatekeeping one finds in too many other fandoms.  We even enshrine it in song with lyrics like Manowar's "wimps and posers, leave the hall!" from "Kings of Metal".

The older I get, the less patience I have for the gatekeeping some other metalheads do to newbies. When obsessive fans insist that they are the only real fans and that everybody else is a "casual" or a "poser" are permitted to dominate a fandom, that fandom becomes as stagnant as a fish tank left untended too long.

We can be better than this. We *must* be better this, unless we want heavy metal to suffer the same fate as European baroque and Romantic music: if it becomes the exclusive province of a snobby elite, it will truly *die*.

Nobody is born a metalhead. No metalhead’s journey is the same. For whatever reason, fewer women than men seem to get into metal on their own. For example, my wife Catherine only knew the bands her younger brother was into, which were mostly mainstream acts like Metallica. Her own taste leaned closer to poppy acts like Savage Garden. I introduced her to Ayreon, Bruce Dickinson, Savatage, Therion, Edguy, Nightwish, and every other band I was into. Whenever I discovered a new band, I'd share it with her. Some she liked, like the Protomen. Others, like Baroness, not so much.

If I had rejected Catherine because she wasn't a "true metalhead", I would have missed out on almost twenty years of friendship, romance, and a marriage that's still going strong. Learn from my example and try to be more patient and open-minded. But if you're already soured on this girl, you aren’t willing to introduce her to other bands, and don’t care that she’s probably as lonely as you might be then at least have the guts to reject her to her face.
