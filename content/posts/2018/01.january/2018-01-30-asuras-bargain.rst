An Asura's Bargain
##################

:date: 2018-01-30 17:10
:category: Starbreaker
:tags: asura, asura emulator, agreement, bargain, crime, faustian, isaac magnin, morgan stormrider, shit my characters say, trial, truth and reconciliation, shattered guardian
:slug: asuras-bargain
:summary: In Starbreaker, asura emulators like Morgan Stormrider are designed to kick demon's asses. They just happen to be good at driving bargains with demons as well.
:summary_only: true

In Starbreaker, asura emulators like Morgan Stormrider are designed to kick demon's asses. They just happen to be good at driving bargains with demons as well.

Something like the following dialogue will probably make its way into *Shattered Guardian*.

MORGAN
	Why couldn't you have told me a decade ago that you needed me for this?

ISAAC
	You would not have believed me. Moreover, I needed you to grow through experience and develop ego-strength through your bonds with others. Will you help me now?

MORGAN
	After everything you admitted to doing to me, Naomi, and Chr- Annelise, I should kill you.

ISAAC
	You tried that once already, which is how we got here.

MORGAN
	I'll help, but afterward you will all stand trial. The Phoenix Society's executive council is going down, and you are going to answer for everything you did starting with Nationfall. We all have a right to know the truth about everything, so we can decide whether the Phoenix Society is worth even trying to reform.

ISAAC
	So, a Truth and Reconciliation Commission [#]_?

MORGAN
	No. A public trial before a tribunal by as impartial a jury as can be managed. What you did, what we're doing&mdash;it needs to be documented lest it be forgotten.

ISAAC
	Very well. I accept, though I feel like *I* am the one making a deal with the Devil.

MORGAN
	You wanted an Adversary, didn't you?

.. [#] Isaac Magnin *is* referring to the `body convened in South Africa in 1996 <https://en.wikipedia.org/wiki/Truth_and_Reconciliation_Commission_(South_Africa)>`_ after the abolition of apartheid.
