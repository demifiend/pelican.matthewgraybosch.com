Leaving the Cult; or, Getting Free of Toxic Tech Culture
########################################################

:date: 2018-01-19 08:07
:category: Agitprop
:tags: antiwork, abuse, culture, silicon valley, tech, time to unionize, toxic, working conditions, strike
:summary: Valerie Aurora's post on leaving a toxic tech culture is absolutely brilliant, and everybody who works in tech should read it. Afterward, they should stage a wildcat strike.
:slug: leaving-the-cult-or-how-to-leave-the-tech-industry
:summary_only: true


Valerie Aurora's `post on leaving a toxic tech culture <https://blog.valerieaurora.org/2018/01/17/getting-free-of-toxic-tech-culture/>`_ is absolutely brilliant, and everybody who works in tech should read it.

That's right, *everybody*. Not just people working in Silicon Valley or for a startup. *Everybody*. I've worked for twenty years as a developer without ever setting foot in the Valley, but the attitudes Ms. Aurora decries are not local to SV. 

They're everywhere.
