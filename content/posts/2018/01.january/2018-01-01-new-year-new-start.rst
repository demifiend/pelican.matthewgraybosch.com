New Start for a New Year. Help Wanted.
######################################

:date: 2018-01-01 00:00
:category: Starbreaker
:tags: update, happy new year, 2018, blackened phoenix, shattered guardian, plans, resolutions
:slug: new-year-new-start
:summary: It's a new year, and time to make a new start. Let's start with an update, an apology for Starbreaker fans, and some explanations.
:summary_only: true


ome of you may have noticed that my website looks different *again*.  No
doubt you're sick of the endless changes. I don't blame you, but there's a
reason I've been messing around with my website instead of just writing
*Shattered Guardian*.

Incidentally, for readers expecting me to write a novel called *Blackened Phoenix*: I've changed the title. The old title kept making me hungry, and
I wanted the novel's title to focus on Morgan's struggles rather than on
organizational issues.

What is My Major Malfunction?
=============================

With that said, one might reasonably ask what's taking so damned long with
*Shattered Guardian*.  After all, I published *Silent Clarion* back in
October 2016, a little over a year ago.  I published *Without Bloodshed* way
back in November 2013.  Am I just dicking around?

Maybe.  It's more complicated than that, though I don't expect anybody to
sympathize.

For starters, I live with a form of depression called
dysthymia_
that basically makes adulting *harder*.  Drugs don't help, and neither does
therapy.  I've tried both.

Despite my dysthymia, I still manage to take care of business.  I still have
my day job as a software developer.  I'm still happily married to my wife of
thirteen years, Catherine Gatt.  We bought a house in June 2017, mere weeks
after I took Catherine to Paris to celebrate her 40th birthday.

It's not like I haven't been writing, either.  I've got almost a dozen
different first chapters for *Shattered Guardian*, but I'm not happy with
any of them.  Worse, I'm not sure how best to continue should I just say
"screw it" and pick one, because I have as many outlines as I do first
chapters and none of *them* are satisfactory, either.

It's like that old song "Flaming Telepaths" goes:

.. raw:: html

    <blockquote>
      <p>Is it any wonder that my mind's on fire<br />Imprisoned by the thought of what to do?</p>
      <p><cite>&mdash; Blue Öyster Cult, <strong>Secret Treaties</strong> (Columbia, 1974)</cite></p>
    </blockquote>

Why is a Sequel Harder? I Managed a Prequel.
============================================

Why is this the case?  I think it's because while you *can* read *Without Bloodshed* 
as a stand-alone work, the events of that novel have
repercussions that must affect the subsequent novels, and I haven't figured
out how to deal with them yet.

However, I think there's a deeper problem at work.  I think the well has run
dry.

The question is what I'm going to do about it.  To start with, I'm shelving
*Shattered Guardian* for now.  Instead, I want to focus on fleshing out the
setting and characters; I suspect that I don't understand them nearly as
well as I thought I did, and certainly not well enough to write the novels I
want to write.

Instead, I'm going to take the winter off and spend it reading.  I can't
take a season of rest away from my day job, but instead of trying to force
*Shattered Guardian* as I have been, I'm going to leave it alone.  It'll be
there when I'm ready, and hopefully a quiet winter spent whittling my
to-be-read pile will help me refill that well.

I'm also going to spend time writing small posts about how the world of
Starbreaker works as well as posts about the characters. I hope you'll read
and share them.

Is That All? Of Course Not.
===========================

I also have some non-Starbreaker projects I'd like to deal with. 

The Rebel Branch Initiate's Guide to the Alchemists' Council
------------------------------------------------------------

I'd like to finish *The Rebel Branch Initiate's Guide to the
Alchemists' Council*, a commentary on Canadian author Cynthea Masson's
literary fantasy, *The Alchemists' Council*.  She has a sequel coming out
called *The Flaw in the Stone*, and it might be nice to catch up with the
first novel before the second one drops if I'm going to keep up the RBIG. 

Unix for Writers
----------------

I think a comprehensive series of *Unix for Writers* articles might
be worth doing.  I've seen quite a few writers struggle because they think
they need a top-of-the-line machine with the latest version of Windows and
the latest version of Microsoft Word, but can't afford to pay for it, and
many of them would be perfectly fine with a refurbished laptop manufactured
5-10 years ago if they knew how to install a BSD or GNU/Linux and use free
software.

Rants Done Right
----------------

I'd like to do something a little more constructive with
my tendency to rant, and contain all of my rants within a collection of
essays called *Everything Considered Harmful*.

Without Bloodshed 2.0
---------------------

The rights to *Without Bloodshed* have reverted to me, and I think I
would like to post the original 2013 text online for all to read.

Time for a Fresh Start
======================

To that end, I'll be redesigning and rebuilding this blog. Some content will move. Some content will go away. I hope you'll stick around. In any case, once I get Pelican working properly I have no intention of breaking it and trying something new just because it's shiny. I wasted enough time on that nonsense, and it's time to get serious.

I could use your help. Since I'm going to commit to using Pelican to manage my web presence for the long haul, I'd love it if you `emailed me </contact/>`_ whenever you have time. If you'd rather not, thanks for reading anyway.

.. _dysthymia: https://www.mayoclinic.org/diseases-conditions/persistent-depressive-disorder/symptoms-causes/syc-20350929
