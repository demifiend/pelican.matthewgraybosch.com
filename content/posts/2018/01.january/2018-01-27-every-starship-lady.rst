Every Starship is a Lady
########################

:date: 2018-01-27 22:08
:category: Starbreaker
:tags: authors, nose art, planets, scientists, ship names, spaceflight, star trek, starship, the culture, women, worldbuilding
:summary: Every ship, whether it plies rivers, oceans, or deep space, is a lady and addressed as she by those sailing them. It might be sexist, but perhaps a sexist tradition can be put to nobler use if we get around to building starships.
:slug: every-starship-lady
:summary_only: true

Spaceflight happens in the world in which my `Starbreaker </starbreaker/>`_ stories are set, and there *is* a starship that will prove important later, but I haven't mentioned much about happenings beyond `Gaia <https://en.wikipedia.org/wiki/Gaia>`_ (the in-story name for Earth) in my stories because most of the action happens on our birthworld.

Ships that leave Earth orbit and fly to `Selene <https://en.wikipedia.org/wiki/Selene>`_ or to other planets carry the IPS `ship prefix <https://en.wikipedia.org/wiki/Ship_prefix>`_ for "interplanetary ship". Based on this designation, space transport firms have gotten ahead of themselves and decided upon the ISS and IGS for interstellar and intergalactic craft respectively.

For example, some of the first ships built by Earth for inner system transport (between the sun Helios and the asteroid belt between Ares and Zeus) were named as follows:

- *IPS Mary Shelley*
- *IPS Catherine Lucille Moore*
- *IPS Leigh Brackett*
- *IPS Ursula K. Le Guin*

I haven't decided how it came to be that the tradition of naming interplanetary spacecraft after women who either made science fiction possible or dramatically changed the genre for the better sprang up. If it ever becomes relevant to plot or characterization I'll think of *something*.

Why Name Starships After Women SF Authors? What About Women Scientists?
=======================================================================

Now that I think of it, I could also name ships after women scientists and mathematicians. One might make the Ares-to-Poseidon run aboard ships with names like:

- *IPS Hypatia*
- *IPS Ada, Countess Lovelace*
- *IPS Margaret Hamilton*
- *IPS Grace Hopper*
- *IPS Hedy Lamarr*

Given her work on the software running on Apollo 11's computers, however, I think it would be more fitting if the *Margaret Hamilton* was dedicated to the transport between Gaia and her moon Selene.

You can be sure that each ship's crew will find at least one or two artists willing to do `extravehicular activity <https://www.nasa.gov/audience/foreducators/topnav/materials/listbytype/Extravehicular_Activity.html>`_ and paint portraits of their respective ship's namesake on the hull. It wouldn't be the first time; `nose art <https://www.airplanenoseart.com/>`_ is a time-honored military tradition, though both the US and UK have mandated gender-neutral imagery. 

This raises a question: how do you paint a hull in space? How do you keep the paint in a liquid state long enough to apply it, and then get it to dry afterward?

One could question the necessity of naming ships after women, or even suggest that doing so is sexist. I won't deny it, but ships need names. Why not give name them after women who helped humanity reach skyward either by providing inspiration through art or making the dream real by their scientific work? Why not keep their memory alive in this fashion? To live on in the memory of others is the only real hope for immortality any of us have, and I'd rather see women like Margaret Hamilton and Leigh Brackett immortalized than businessmen or admirals.

In any case, if I used ship names like *Your Mother Loved It*, *My Pilot Is Drunk*, or *I Got Your Gravitas Right Here, Baby* readers might think I was writing `Culture <https://www.iain-banks.net/books/#culture>`_ fanfic. And names like *Discovery*, *Endeavour*, *Defiance* are mostly too bland and too Trekkie.

Speaking of *Star Trek*, the "USS" prefix applied to Federation ships like the *USS Enterprise* doesn't make sense. "USS" stands for "United States Ship", and has since Theodore Roosevelt's 1907 executive order. If Gene Roddenberry had been British, would we have had the *HMS Enterprise* instead? I suspect at least one of the Trek writers came up with a more fitting prefix like "UFS" ("United Federation Ship) or "FPS" (Federation of Planets Ship), but by the time they did it was too late, and management probably decided it was too picayune a detail to bother with.

Naming Planets
==============

Yes, I know the moon isn't named Selene, and that there are no planets named Ares, Zeus, and Poseidon. Likewise, the Sun's name is "Sol", not "Helios", and that's not because of a Zionist conspiracy. However, I decided that if I was going to name my setting's version of Earth "Gaia" I might as well be consistent and name the sun and other planets after Greek gods rather than their Roman counterparts. Oddly enough, in reality Uranus is the only major planet *not* named after a Roman deity. 

If we had been consistent in our nomenclature, we would call Earth "Terra" and have a planet named `"Caelus" <https://en.wikipedia.org/wiki/Caelus>`_ instead of "Uranus". And then my father wouldn't be able to joke about the similarity between Captain Kirk and toilet paper, which is that both go around Uranus hunting for Klingons. (Yes, I can hear you groaning.)

Orbital Habitats vs Colonizing Planets
======================================

It would be more reasonable to simply build orbital habitats, and those exist as well, but people are still people in my setting. Many of them are irrational enough to want to live on a bigass hunk of rock hurtling through space instead of inside a "tin can".

Speaking of which, here's some more public-domain art from `Don Davis <http://www.donaldedavis.com/>`_. You wouldn't have seen this if you had come directly to my site. 

.. image:: {filename}/images/STORUS1-1024x799.jpg
	:alt: Stanford Torus, by Don Davis. Commissioned by NASA.
	:align: left

Also, `the artist's thoughts on space migration <http://www.donaldedavis.com/2007NEW/SPACEMIGRATION.html>`_ are of interest. I think that learning to build sustainable orbital habitats is a necessary first step toward interplanetary and interstellar spaceflight. Once you have an orbital habit, I think the next logical step is to add propulsion systems and get the habitat moving out of our system at a reasonable fraction of the speed of light.

(We've only got another 4.5 billion years at most, people.)
