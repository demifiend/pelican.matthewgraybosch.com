Advertisement: "Limited Liability"
##################################

:date: 2018-01-23 13:35
:category: Starbreaker
:tags: 	asteroid impact, asteroid mining, ceres, command responsibility, corporate death penalty, corporate responsibility, extinction, malfeasance, medina standard, michael chapman, morgan stormrider, nightmare sequencer, rajiv singh, ron davis, sci-fi, science fantasy, science fiction, short story, three mile island
:summary: "Limited Liability" is the only story I've written that takes place after the events of my main Starbreaker sequence, and features Morgan from Without Bloodshed playing the the heavy. It's now free to read online.
:slug: limited-Liability-now-free-read-online


"Limited Liability" is the only story I've written that takes place after the events of my main Starbreaker sequence, and features Morgan from Without Bloodshed playing the the heavy. 

It's now free to `read online </stories/limited-liability/>`_.
