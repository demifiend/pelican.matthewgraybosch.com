Goodbye, WTPA. Hello, WRTI.
###########################

:date: 2018-02-05 11:20
:modified: 2018-02-26 22:49
:tags: central pa, christians ruin everything, diversity, local, radio, rock, wtpa, wrti
:slug: goodbye-wtpa-hello-wrti
:summary: Pennsylvania recently lost one of the last local radio station worth listening to, WTPA. It had too many ads, but when they played music they played good stuff: classic and recent hard rock and heavy metal.
:summary_only: true

Pennsylvania recently lost one of the last local radio station worth 
listening to, WTPA. It had too many ads, but when they played music 
they played good stuff: classic and recent hard rock and heavy metal.

I don't listen to FM radio often, but when I did this was
my go-to station. At least, it used to be. I found out
that `they've sold out to Educational Media Foundation
<http://www.pennlive.com/entertainment/index.ssf/2018/01/wtpa_sale_chris
tian_radio.html>`_, and have since switched to a "Christian adult
contemporary" format.

Must I Do Everything Myself?!
=============================

As if there aren't enough Christian stations on the air already. First
all the classical music stations seem to go offline, and now I can't
find a decent radio station for metalheads? It's almost as if the world
is daring me to get into pirate radio…

Well, no. I'll be a good metalhead and get my FCC license. That way I
can call my station WPRV: Privateer Radio (pirate radio with a license
to thrill).

Update: 12 February 2018
========================

I was wrong about there not being any classical music or jazz radio
stations in my area. While fiddling with the radio dial in my car I
happened upon WRTI_, a network of stations run by Temple University. On
weekdays WRTI broadcasts classical music from six in the morning to six
at night, whereupon they break out the jazz.

Even better, they have streaming options in the form of Android and iOS
apps as well as streams that, with a bit of fiddling, I can access from
a Unix command line using the mpg123_ tool.

.. _WRTI: https://www.wrti.org/
.. _mpg123: http://mpg123.org/
