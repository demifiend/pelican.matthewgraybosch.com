I've Got My Work Cut Out For Me
###############################

:Date: 2018-02-26 22:26
:tags: migration, jekyll, wordpress, pelican, conversion, typos
:slug: got-my-work-cut-out-for-me
:summary: I've got my work cut out for me, converting over 255 posts dating as far back as 2009 for Pelican.
:summary_only: true


I've got my work cut out for me, converting over 255 posts dating as far
back as 2009 for Pelican_.

I suppose I could keep my posts in their original Markdown_ format, but
their metadata is for several versions of Jekyll_ — assuming they've been
converted to Markdown from WordPress_ HTML.

If I convert everything to reStructuredText_, I can at least check
everything for typos and perhaps do a better job of presenting images.
There's also the matter of theming my Pelican site. Perhaps I should
jazz up the templates using Bulma_ CSS.

And then there's the matter of pulling my content out of `Ghost Town`_.
That should be hilarious.

.. _Pelican: https://blog.getpelican.com/
.. _Markdown: https://daringfireball.net/projects/markdown/
.. _Jekyll: https://jekyllrb.com/
.. _WordPress: https://wordpress.org/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _Bulma: https://bulma.io/
.. _Ghost Town: https://plus.google.com/+MatthewGraybosch
