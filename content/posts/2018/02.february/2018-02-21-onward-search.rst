Recruiter Abuse: Onward Search
##############################

:date: 2018-02-21 20:16
:tags: tech recruiters, abuse, spam, mockery
:slug: recruiter-abuse-onward-search
:summary: I occasionally get spam from clueless tech recruiters. Today's lucky winner is Onward Search.
:summary_only: true

Every once in a while I get unsolicited email from an utterly clueless tech
recruiter. Today's lucky winner apparently works for
`Onward Search`_. Personal names are redacted to
protect the guilty, who are probably just doing their best to make a living
working at a bullshit job.

Now, let the fun begin!

    Hi there Matthew,

That's just a *bit* too friendly, but at least they spelled my name right. You'd
be amazed at how many people don't seem to know how to spell my name.

    I'm <REDACTED> from Onward Search, and we are actively looking for
    individuals with your background in Software. Your skills are very
    marketable, and we are constantly placing candidates similar to you. We
    would love to start a conversation and find out what you are looking
    for.

What kind of software do they mean? I use a _lot_ of software, being an
author *and* a developer (and a generalist/full-stack dev at that).

How similar to me are these other candidates, anyway? Has Onward Search
somehow stumbled on an enclave of metalheads who code for a living
because they haven't figured out how to write bestselling science
fiction? Do these poor bastards resemble me, too?

    If you could send me a copy of your resume, our recruiters would
    like to reach out to discuss things further.

I'm sure they would, but this suggests that the sender not only isn't a
recruiter themselves, but don't actually have a particular position in mind.
They're just fishing for resumes or contacts.

    If you're not interested, but you know someone with a background similar
    to yours who would be, we would love to speak with them as well.

Nope. Not interested. And I'm not about to make enemies of people with whom I
might have to work by giving out *their* contact info.

.. _Onward Search: https://onwardsearch.com
