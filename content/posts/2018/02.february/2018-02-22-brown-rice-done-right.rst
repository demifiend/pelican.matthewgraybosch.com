Cooking: Brown Rice Done Right
##############################

:date: 2018-02-22 12:30
:modified: 2018-02-26 20:52
:tags: rice, brown rice, cooking, boiling, simmering, recipe
:slug: cooking-brown-rice-done-right
:summary: It's hard to cook brown rice and get a palatable result, but I found a way.
:summary_only: true

I can’t believe I went my entire adult life without figuring out how to
properly cook brown rice. You know how the instructions on the packet
say to use 2 cups of water for every cup of rice, and then let it simmer
for 40 minutes after bringing it to a boil? 

That’s complete and utter bullshit. It doesn't work; it just leaves you with a pot of half-cooked and inedible rice.

Here’s what you gotta do instead: `boil brown rice as if it were pasta`_.

I’m serious. Here’s what I did last night:

1. Fill a saucepan with water.
2. Add half a cup of dry brown rice per meal.
3. Add 3 turkey meatballs per meal.
4. Add chicken boullion, `garam masala`_, parsley, and salt.
5. Boil for 40 minutes.
6. Drain without removing rice from saucepan.
7. Add butter and stir through until melted.
8. Cover for ten minutes.

I didn’t eat the rice last night because I had already made dinner, and
my wife was dead set against even trying brown rice again because of
previous failures to cook it in a way that resulted in a palatable meal,
but I had it for lunch today and it was perfect.

`The Cooking Geek <http://thecookinggeek.com>`_ has more information_, and suggests rinsing and soaking brown rice overnight.

.. _boil brown rice as if it were pasta: https://www.saveur.com/perfect-brown-rice-recipe
.. _garam masala: https://en.wikipedia.org/wiki/Curry_powder
.. _information: http://thecookinggeek.com/brown-rice/
