Shattered Guardian Session 7
############################

:date: 2018-04-23 18:25
:summary: Some more magic, and some characterization that should make readers wonder what exactly Isaac Magnin saw in Annelise Copeland.
:category: Serials
:tags: annelise copeland, christabel crowley, isaac magnin, queens, trauma, eyewitness, nightmares
:summary_only: true
:og_image: /images/starbreakercollage.jpg


404 words tonight while Catherine was at the dentist getting a cavity filled. 
For some reason I had "`Broken Wings`__" by Mr. Mister on repeat. Some more
magic, and some characterization that should make readers wonder what 
exactly Isaac Magnin saw in Annelise Copeland.
 
Don't forget you can read the `entire draft`__ whenever you like.

.. __: https://www.youtube.com/watch?v=nKhN1t_7PEY
.. __: /starbreaker/novels/shattered-guardian/

Brooklyn - Annelise Copeland (continued)
========================================

An hour later, Annelise followed Isaac down the paths of a cemetary to which
she had once been denied entry on account of her civilian status.  The
ordinary soldiers' headstones had unsettled her because of their sheer
number, but it was the Adversaries' stones that sent dread worming through
her guts.  Though she lacked any notion of what the mottos enscribed on the
basaltic grave markers meant, she doubted it was anything uplifting about
awaiting one's friends and families in a better world than this one.

Lost in such thoughts, she walked into Isaac's left arm as she raised it to
bar her way.  "What's wrong?"

Without waiting for an answer, Annelise looked ahead to find a basalt statue
of a Greek god holding an eternal flame, and a tall, snow-blonde woman
standing before it with a naked sword in her hand.  "Look.  Naomi's just up
ahead."

At least, Annelise thought it was Naomi.  She knew few other women as tall
as the soprano with whom she had worked in a former life, and she recognized
the cut of the other woman's burgundy wool overcoat as being her own design. 
However, she did not recognize the sword she held.  "That *is* Naomi, isn't
it?"

Isaac nodded.  "Yes, but somebody else is there with her.  Somebody inhuman. 
Stand back, please."

As Annelise complied, Naomi sprang upon the pedestal and thrust her strange
gray sword into the statue's breast. Its jawbone unhinged and fell before
the rest of the statue crumbled into dust. Naomi sprang backward, and Isaac
chuckled.

"What's so funny?"

Isaac glanced at Annelise. "I never cared for that sculpture. I always
thought the artist was pussyfooting, trying for an inoffensive compromise,
but the unconquered lightbringer is not Prometheus. Prometheus submitted
to Zeus and *accepted* his punishment."

Not knowing what Isaac Magnin was going on about was nothing new for
Annelise, so she accepted that this was one of *those* conversations and
tried to go along with it. "Then who is the unconquered lightbringer?"

Rather than answer, Isaac waited for Naomi to leave before snapping his
fingers. The dust whirled as if lifted by a breeze, but instead of
dispersing coalesced into a statue of an angel with broken wings staring
silent defiance at the heavens with eyes of fire. Annelise gazed upon the
statue, and thought she recognized it. "Is that *Morgan*?"

"No, but close enough. Come along. Naomi won't be far off."
