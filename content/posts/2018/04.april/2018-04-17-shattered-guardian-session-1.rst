Shattered Guardian Session 1
############################

:date: 2018-04-17 08:00
:summary: At 362 words, this is a short Shattered Guardian Session, but you might find a surprise if you read it.
:category: Serials
:tags: naomi bradleigh, queens, grief, cemetery gates
:summary_only: true
:og_image: /images/starbreakercollage.jpg


At 362 words, this is a short `Shattered Guardian`__ Session, but you might find a surprise if you read it.

.. __: /novels/shattered-guardian/

If you're a Pantera fan, you know what `song`__ I was listening to while writing this.

.. __: https://www.youtube.com/watch?v=WyczwqRD2NI

Queens - Naomi Bradleigh
========================

Naomi had tuned out the cab driver's chatter soon after settling into the
plush leather seat and specifying her destination. It was a habit she 
normally tried to avoid; once a driver recognized her they tended to ask 
the same questions in the same order. It would have been easy to only 
half-listen and give canned responses to starstruck strangers, but she had
decided to accept fame as a gift rather than a burden, and treat the
people whose interest in her and her work as a musician made her life
possible with actual courtesy.

It was hard to live up to that ideal tonight, she admitted to herself. She
had put off this visit for months, but Claire had finally prevailed upon her
to come to New York, order two bouquets of roses -- one of white and one of 
black-tipped red -- and visit the Adversaries' graveyard. "Even if Morgan
is too dead to give a shit," Claire had said, "This isn't about him. It's
about you. You need to acknowledge his death, pay your respects, mourn
your loss, and move the hell *on*."

The hiss of tires on wet asphalt ceased. The mechanical back-and-forth 
of the windshield wipers halted. The cab's lights were all that held back
the night. "We're here, Ms. Bradleigh," said the cabbie, who remained in his
seat. "Are you sure it's safe to be here alone, in the dark?"

"Thanks for caring, but I brought a light and my sword. I should be fine,"
said Naomi as she counted out money for the fare, and included a generous 
tip. "Will you await my return?"

"Sure. Let me help you out." The driver muttered what was most likely a curse
in Yoruba as his head struck the doorframe. He was still rubbing it as he 
opened the door closest to Naomi and offered her his hand. Taking his
leather-gloved hand in hers, she stepped out of the cab. She stretched, looked skyward, and wished the sky were clear. Selene was nearly full, and her light
combined with that of the stars would have been enough to guide Naomi
now that the city had gone dark until the morning.
