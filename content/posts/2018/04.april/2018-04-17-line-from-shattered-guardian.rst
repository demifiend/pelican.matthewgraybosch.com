A Line from *Shattered Guardian*
################################

:date: 2018-04-17 00:00
:summary: "The lies we tell ourselves are the lies that define us." I don't think this came from a movie I've seen or a book I've read, so I decided to assume it's original and use it in *Shattered Guardian*. It sounds like something Isaac Magnin would say.
:tags: starbreaker, shattered guardian, tagline, isaac magnin
:og_image: /images/starbreakercollage.jpg


"The lies we tell ourselves are the lies that define us." I don't think this came from a movie I've seen or a book I've read, so I decided to assume it's original and use it in *Shattered Guardian*. It sounds like something Isaac Magnin would say.
