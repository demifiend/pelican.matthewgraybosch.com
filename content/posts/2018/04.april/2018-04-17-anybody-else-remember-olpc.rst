Does Anybody Else Remember the OLPC?
####################################

:date: 2018-04-17 14:23
:summary: Does anybody else remember the OLPC? You know, the little green laptop that was supposed to be dirt cheap, durable, and so easy to learn to use that giving them to kids in Third World countries would spark an educational revolution? It didn't work out way. `The Verge digs into what happened and why. <https://www.theverge.com/2018/4/16/17233946/olpcs-100-laptop-education-where-is-it-now>`_
:category: Just Tech Things
:tags: olpc, sugar, miracle tech, didn't pan out, the verge


Does anybody else remember the OLPC? You know, the little green laptop that was supposed to be dirt cheap, durable, and so easy to learn to use that giving them to kids in Third World countries would spark an educational revolution? It didn't work out way. `The Verge digs into what happened and why. <https://www.theverge.com/2018/4/16/17233946/olpcs-100-laptop-education-where-is-it-now>`_
