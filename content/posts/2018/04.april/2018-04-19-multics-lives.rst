Multics Lives!
##############

:date: 2018-04-19 10:31
:category: Just Tech Things
:summary: `trn <https://lobste.rs/u/trn>`_ at `Lobsters <https://lobste.rs>`_ recently `announced <https://lobste.rs/s/4ktahz/ban_ai_s_public_access_multics_system>`_ the existence of `ban.ai public access Multics <https://ban.ai/multics/>`_. Yes, *that* `Multics <http://multicians.org/>`_. I wish this had been around a couple of years ago when I was writing my novel `Silent Clarion </starbreaker/novels/silent-clarion/>`_ in 2015. I had put my protagonist in the position of trying to crack a Multics installation. While Unix was ubiquitous in her setting, she had no experience with Multics. While I had used `http://multicians.org/ <http://multicians.org/>`_ for research, reading a website is no substitute for firsthand experience.
:tags: multics, public assess, timesharing, old-school, retro, vintage, historical


`trn <https://lobste.rs/u/trn>`_ at `Lobsters <https://lobste.rs>`_ recently 
`announced <https://lobste.rs/s/4ktahz/ban_ai_s_public_access_multics_system>`_ 
the existence of `ban.ai public access Multics <https://ban.ai/multics/>`_. Yes, 
*that* `Multics <http://multicians.org/>`_. I wish this had been around a couple 
of years ago when I was writing my novel `Silent Clarion </starbreaker/novels/silent-clarion/>`_ 
in 2015. I had put my protagonist in the position of trying to crack a Multics 
installation. While Unix was ubiquitous in her setting, she had no experience with Multics. 
While I had used `http://multicians.org/ <http://multicians.org/>`_ for research, reading 
a website is no substitute for firsthand experience.
