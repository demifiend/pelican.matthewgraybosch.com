Ghost: "Rats" (2018)
####################

:date: 2018-04-16 18:41
:summary: Ghost's new single "Rats" is as packed with infectious grooves as its namesakes were with disease-ridden fleas back in the day, and the official video reminds me of Michael Jackson's "Thriller". And the cover for their upcoming album *Prequelle* is a delicious riff on Hieronymous Bosch.
:og_image: /images/ghost-prequelle-cover-2018.jpg

Ghost's new single "Rats" is as packed with infectious grooves as its namesakes were with disease-ridden fleas back in the day, and `the official video`__ reminds me of Michael Jackson's "`Thriller`__". And the cover for their upcoming album *Prequelle* is a delicious riff on Hieronymous Bosch.

.. __: https://www.youtube.com/watch?v=C_ijc7A5oAc
.. __: https://www.youtube.com/watch?v=sOnqjkJTMaA

.. image:: {filename}/images/ghost-prequelle-cover-2018.jpg
    :alt: Cover for Ghost's 2018 single, "Rats"
    :align: left

