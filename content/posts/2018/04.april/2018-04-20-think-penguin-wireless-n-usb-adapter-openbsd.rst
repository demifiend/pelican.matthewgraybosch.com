Using Think Penguin's Wireless N USB Adapter on OpenBSD
#######################################################

:date: 2018-04-20 21:00
:summary: I received one of Think Penguin's Wireless N USB Adapters today, and tested it on my main OpenBSD desktop. It took a little setup, but it works.
:category: Just Tech Things
:tags: think penguin, wifi, usb, adapter, openbsd, ar9271, fw_update(1), athn(4), hostname.if(5)
:summary_only: true


I received one of `Think Penguin`__'s `Wireless N USB Adapters`__ today, 
and tested it on my main `OpenBSD`__ desktop, **thagirion**. It took a
little setup, but it works.

.. __: https://www.thinkpenguin.com
.. __: https://www.thinkpenguin.com/gnu-linux/penguin-wireless-n-usb-adapter-w-external-antenna-gnu-linux-tpe-n150usbl
.. __: https://www.openbsd.org

I had to run `fw_update(1)`__ to download firmware for the adapter's Atheros AR9271
chipset while still jacked into my original wired connection (which was only
wired to a Linksys wifi repeater). It was also helpful to RTFM for `athn(4)`__ and `hostname.if(5)`__.

.. __: https://man.openbsd.org/fw_update
.. __: https://man.openbsd.org/athn
.. __: https://man.openbsd.org/hostname.if

Once I downloaded the firmware and restarted **thagirion** I confirmed that
the adapter was working by running ``doas ifconfig athn0 scan``.
``ifconfig`` showed that **thagirion** was talking to my router and seeing
networks.

From here all I had to do was follow the relevant sections of the `OpenBSD
FAQ`__.

.. __: https://www.openbsd.org/faq/faq6.html

Think Penguin's Wireless N USB Adapter is a bit pricy, but worth it if
you've got a desktop system without compatible built-in wifi.
