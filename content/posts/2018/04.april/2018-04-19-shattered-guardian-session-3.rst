Shattered Guardian Session 3
############################

:date: 2018-04-19 12:58
:summary: I'm not really happy with my progress, but 344 words is better than nothing. I might have to rewrite some of this.
:category: Serials
:tags: naomi bradleigh, queens, grief, cemetery gates
:summary_only: true
:og_image: /images/starbreakercollage.jpg


I'm not really happy with my progress, but 344 words is better than nothing. 
I might have to rewrite some of this.  Meanwhile, you can read the `current
state`__ of the novel in its entirety.

.. __: /starbreaker/novels/shattered-guardian/

Queens - Naomi Bradleigh (continued)
====================================

A whir of machinery broke the silence, followed by the clicks of locks
disengaging.  Naomi reached for one of the gates, sure she would have to put
her back into opening.  She was sure the gate would creak and scream for
lack of oil.  Instead, the gate receded at her touch, silently opening
inward as if the hinges had been oiled before the caretakers went home for
the night.

Granite stones in rank and file shaded by trees on the verge of bursting
into leaf stood sentry on either side of the brick-paved path Naomi
followed.  Lesser, narrower paths branched off at intervals, but Naomi
ignored those.  A map appeared in her overlay, showing her current position
and providing guidance to the inner necropolis where Adversaries who had
served honorably were buried regardless of how they died.

Even if she had lacked the technology to follow a map drawn by a tiny
computer implanted in her head, Naomi would have had no trouble finding her
destination.  A lone flame flickered in the distance, visible through the
tree branches.  Passing a breach in a stone wall that separated Nationfall's
dead from the much smaller plot dedicated to the repose of the Phoenix
Society's sworn swords, Naomi stood before a massive granite pedestal.

A heroically proportioned man reclined against a mountain crag, nude save
for broken manacles and leg irons.  He crushed a dead eagle underfoot, and
in the palm of his upraised left hand an eternal flame burned.  Naomi had
not needed to read the inscription or translate it from Latin to recognize
the figure or the allegory in marble before her.  Here was Prometheus, the
unconquered lightbringer, watching over those who had given at least a part
of their lives to the liberation of their fellows.

Approaching the pedestal, Naomi placed one of the bouquets before the
massive foot Prometheus was not using to trample symbolic raptors,
retreated, and kneeled as if to pray.  "For my fallen comrades," she
whispered.  She remained kneeling for several minutes, but held the humanist
titan's stony, unseeing gaze.
