An Open Letter to All Incel Assholes
####################################

:date: 2018-04-25 13:30
:modified: 2018-04-26 20:33
:summary: Twenty years ago I could have been one of you, only I still had my pride. As such, I'm about to lay some harsh truth on you: *if you make your inability to get laid the basis of your identity, you deserve to be incel*.
:category: As The World Burns
:tags: incel, self-pity, tough love, anger, rape culture, rant, manosphere, red pill, useful idiot, alt-right
:summary_only: true
:og_image: /images/our-gang-little-rascals-he-man-woman-haters.jpg

Twenty years ago I could have been one of you, only I still had my pride.  I
certainly wasn't going to `plow a van into a bunch of women because I
couldn't get laid`__.  As such, I'm about to lay some harsh truth on you:
*if you make your inability to get laid the basis of your identity, you
deserve to be incel*.

.. __: https://www.reuters.com/article/us-canada-van/toronto-van-attack-suspect-expected-in-court-on-tuesday-idUSKBN1HV1AY

It's not *that* hard to find a willing partner.  I'm a fat geek who writes
about swashbuckling soprano catgirls, and I managed to find partners. 
Christ, I've been married for *thirteen fucking years*.  What the *hell* is
your excuse?

.. contents:: **Table of Contents**
	:depth: 1
	:backlinks: top

.. image:: {filename}/images/our-gang-little-rascals-he-man-woman-haters.jpg
	:alt: Our Gang/Little Rascals: a pre-internet incel rally
	:align: left

Seriously, what is your major malfunction?  You're long past the age when
your misogyny could be dismissed as innocent, cute, or just "boys will be
boys".

"But dating is hard..."
=======================

You think it was easy twenty years ago?  It wasn't, but whatever.

Don't want to date?  Go to one of Nevada's legal brothels.  Go to Amsterdam. 
Go to Austria.  Go to Australia.  Go somewhere where adult sex workers can
ply their trade without fear of state violence and get your itch scratched
by a professional who -- to a point -- will put up with your bullshit and
your toxic attitudes because they're getting paid for it.

Oh, right.  It doesn't "count" if you had to pay for it because you can't
brag about it to your macho buddies.  So don't brag about it.  What happens
in Vegas stays in Vegas, remember?

Oh, but you have a moral objection to hiring a sex worker?  You're concerned
about human trafficking?  You think sex work is inherently exploitative to
those who engage in it?  That's commendable, but what's stopping you from
approaching women and talking to them like a functioning human being?

"But I'm depressed/autistic..."
===============================

To quote a movie that was old when I was a kid: you and about five million
other guys.

You think depression or being on the autistic spectrum make it impossible to
reach out to other people and form relationships?  I'll admit it makes
things *harder*, but the gap between harder and impossible is so big that
God could disappear inside of it.

I manage it.  Millions of other depressed and/or autistic people manage it. 
Stop bullshitting yourself and get your act together.

"But I'm ugly..."
=================

How ugly?  So ugly that even your mothers can't deny it?  Or just ugly as in
not handsome enough for Hollywood?  There's a big difference.

But let's take a look at that loser you venerate as the "Supreme Gentleman". 
Sure, he was half-Asian and Asian men often face stigma when dating, but his
face wasn't deformed or disfigured.  It's not like he had outrageously bad
skin or an untreated harelip.  His was the sort of ugly you can't fix at the
gym or by seeing a dermatologist or a plastic surgeon, and chances are so is
yours.

Your kind of ugly comes from within.  It's not your looks; it's your
personality, mindset, and beliefs.  You've forgotten, or never learned, that
women are people and that *other people do not exist for your benefit*.

You believe that you *deserve* love and attention from women you find
attractive.  You aren't willing or able to challenge your shallow,
manufactured notions of what makes a woman attractive.  Nor are you willing
to fix your own flaws and make yourself attractive to the sort of women
whose esteem you crave.  You certainly aren't willing to broaden your
horizons and consider other young men who are as lonely and skin-hungry as
yourselves as potential partners, because you're afraid of what Chad will
think.

Here's a clue for you: if Chad spares a moment's thought for you, he has
probably already drawn his own conclusions regarding your sexual
preferences.  You might as well stock up on condoms and lube, and have
yourselves a gay old time.

Nobody owes you love.  Nobody owes you sex.  You aren't entitled to another
person's time, affection, or touch.  Believing otherwise is *exactly* what
people mean when they talk about "rape culture".

Instead of accepting that you have a shitty attitude and fixing yourselves,
you wallow in your self-pity with dicks in hand, fantasizing about "incel
revolutions" and "beta uprisings".

"But women only want alphas for sex, and use betas for resources..."
====================================================================

First off, that whole alpha/beta thing is pseudoscientific bullshit that has
no basis in reality.  My guess is that it got cripped from Aldous Huxley's
*Brave New World*, a dystopian novel in which people genetically engineered
to fit into particular tasks, the alphas ran everything, and the betas did
the the thankless work of keeping civilization running.

You aren't doing that work.  You aren't betas.  You aren't even omegas. 
You're nothing but overgrown children with no experience of true adversity. 
You probably never learned how to handle rejection because your parents
forgot how to say 'no' before they finished doing a half-assed job of
toilet-training you.

You haven't accepted that the world doesn't revolve around you.  You haven't
internalized the reality of the utter irrelevance of your feelings to the
rest of the human race.  And before one of you shitfountains decide to get
cute and say "a cat is fine too," your cat probably doesn't give a damn
about your feelings either.

"But there's nothing I can do..."
=================================

**The lies you tell yourself are the lies that define you.** Right now, the
lie you're telling yourself is that your misery isn't of your own making. 
Maybe you got dealt a shitty hand.  Maybe you got dealt a decent hand, but
never learned how best to play it.  Either way, you've got to learn to play
the hand you were dealt.

But you don't want to accept that because doing so would require that you
look at yourselves in a goddamn mirror.  You're the author of your own
misery, and you're doing everything in your power to avoid the truth that
you're the only one who can fix your life.

"`I am the captain of my soul`__," is another lie some of us tell ourselves,
but people who believe that lie and act as if it were true seem to get more
out of life than people who tell themselves that there's nothing they can do
to improve their own lives.

.. __: https://www.poetryfoundation.org/poems/51642/invictus

I'd like to say that the truth of the matter is that both free will and
determinism are bullshit, but even that isn't necessarily the truth.  It
might just be *less wrong* than believing in one or the other.

Instead of assuming that it's all under your control or that it's all fate,
treat life as a game of poker.  Take the hand you've been dealt and play it
for everything it's worth.  Go all in on every hand.  Never fold.  Never
back down.

"But there's no point in fixing myself without a GF..."
=======================================================

If you think a relationship will make everything better, I've got a clue for
you: having a girlfriend (or a wife) doesn't magically make everything
better.  Hell, it often makes things *harder*, because now you can't just
put yourself first and to hell with everybody else.  Now you've got to
consider another person's needs and desires.

Instead of thinking, "Man, I don't feel like going to work today.  I'm just
gonna call in sick," and doing so, you find yourself thinking, "Man, I
really don't want to go to work today, but if I lose my job Stacy will have
to explain to all her friends why she's dating a broke scrub with no job."

It gets even harder when you're married, because now you and the other
person are bound into a personal and economic partnership by law, and
neither of you can just walk away because you're not happy, despite the lies
MRAs might have told you about no-fault divorce.  Instead, you're together
pretty much all the time, and see each other when you're not on your best
behavior and trying to impress each other.

You'll get sick.  You'll argue over money.  You'll think the other person's
parents are assholes.  You'll think you married an asshole and wonder what
the hell you were thinking before you remember that you weren't thinking,
but feeling, and goddammit you still feel that way even though they've
pissed you off.

And if you have kids?  *It only gets harder.*

It ain't just coed naked fun and games, people.  Relationships are a
responsibility.  Ideally the journey is worthwhile, but this is real life
and there are no guarantees.  But you can't accept that.  You aren't willing
to step up and do the work necessary to become functioning human beings who
are capable of having adult relationships.

"But feminism has ruined women..."
==================================

Do you even know what feminism is?  Yes, women aren't compliant and
subservient like they "used to be" back in the "good old days".  They don't
just lie back, close their eyes, and think of England.

This is a *good* thing.  Women today can be more interesting, more
challenging, and more worthwhile than they ever were before.  They can
challenge, inspire, and delight like never before, if we let them.

If you believe otherwise then those "red pills" you've been taking are made
of snake oil.  You need to stop reading and listening to the shit the
alt-right has been selling you via the "manosphere".  I know they tell you
that it's not *your* fault, that you're worthy just the way you are and that
women who can't accept you as you are to blame.

**Don't you realize they're lying to you?**

Are you so fucking ignorant and naive that you haven't realized that these
assholes don't give a single little fucking shit about you?  Wake up and
smell the napalm.  To them, you're nothing but a bunch of useful idiots, and
they will discard you the second you cease to be useful.  They threw Milo
Yiannopoulos under the bus, and they'll do the same to you.

"What do you expect me to do?"
==============================

I wrote this as a warning to young men who feel unfuckable but haven't yet
embraced "incel" as some kind of sociopolitical identity.  I don't expect
*you* to do *anything*.  After all, it's not *your* fault that your life
sucks, right?

Look: it's not that I've written you off, but that you've written yourselves
off.  If you're going to write youselves off as worthless, why shouldn't the
rest of us take you at your word?

You're welcome to prove me wrong if you can.  Here's how to start: ask
yourself what you want the meaning of your life to be.  Do you want it to be
about nothing but your inability to connect with other people?  Don't you
want more for yourself?

If so, here's what to do.  Get off the internet, turn off the screens, go
outside, and get some sun.  Try talking to other human beings in person. 
Try learning how to make and keep friends.  Learn how to be a decent human
being and make a life for yourself where your happiness doesn't depend on
whether somebody else is helping you get your rocks off.  Broaden your
horizons and learn to see beauty where it isn't obvious.  Try dating
somebody because you enjoy their company and not because looking at them
makes your dick hard.

There's no guarantee it'll happen, but you might get laid along the way. 
You'll be nervous.  Things won't work like they do in porn, but porn is
bullshit and you know it.  Just be honest, admit that it's your first time,
and if they give you shit about it then they're the asshole.

Accept that `nobody's keeping score but you`__.  Nobody cares if you're a
virgin or not.  They care if you're `good, giving, and game`__ in bed.  You
can be that way your first time.  You can have had a hundred partners and
still be a lousy lay.

.. __: http://www.albinoblacksheep.com/flash/bunny
.. __: https://www.eastbayexpress.com/oakland/good-giving-and-game/Content?oid=3056340

It's up to you.

Postscript
==========

Since an `earlier version`__ of this rant has gotten a bit of traction on a
social site most dismiss as a "ghost town", a few people have questioned the
need for this rant.

.. __: https://plus.google.com/u/0/+MatthewGraybosch/posts/W3zQ1LyUF7g

I wrote this because I was having a shitty day at work, felt like ranting,
and needed an acceptable target.  The `Harold Lauders`__ of the world, the
unattractive losers who not only blame others for their own issues but
resort to violence against innocent people, are fair game.  So are those who
cheer them on.

.. __: http://stephenking.wikia.com/wiki/Harold_Lauder
