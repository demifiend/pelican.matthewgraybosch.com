Mastectomy
##########

:date: 2018-04-27 10:00
:summary: My wife's in the hospital getting a skin-sparing mastectomy because her first mammogram turned up a tumor in her left breast and follow-ups turned up a second tumor in the same breast. She'll get reconstruction later, but it won't be the same. It's up to me to help her feel good about herself despite losing one of her breasts, though. But first, the surgery, and while I can help her get ready and wait with her until it's time to get prepped and go into the OR, there's nothing more I can do but take Edmond Dantes' advice: wait and hope.
:tags: surgery, breast cancer, mastectomy, count of monte cristo, support, marriage, self-esteem


My wife's in the hospital getting a "skin-sparing mastectomy" because her first mammogram turned up a tumor in her left breast and follow-ups turned up a second tumor in the same breast. She'll get reconstruction later, but it won't be the same. It's up to me to help her feel good about herself despite losing one of her breasts, though. But first, the surgery, and while I can help her get ready and wait with her until it's time to get prepped and go into the OR, there's nothing more I can do but take Edmond Dantes' advice: wait and hope.
