The Eye of Sauron is Real
#########################

:date: 2018-04-20 17:34
:summary: No discussion of surveillance capitalism is complete without a mention of Peter Thiel's data mining/analysis corporation `Palantir <https://www.bloomberg.com/features/2018-palantir-peter-thiel/>`_. I suspect he wanted to call it "Sauron", but somebody took him aside and told him that was too obvious. If you think Facebook and Cambridge Analytica are bad, you have no idea how creepy Silicon Valley can get.
:category: As The World Burns
:tags: peter thiel, palantir, sauron, surveillance, capitalism, creepy
:og_image: /images/ceiling-cat-watching-you.jpg


No discussion of surveillance capitalism is complete without a mention of Peter Thiel's data mining/analysis corporation `Palantir <https://www.bloomberg.com/features/2018-palantir-peter-thiel/>`_. I suspect he wanted to call it "Sauron", but somebody took him aside and told him that was too obvious. If you think Facebook and Cambridge Analytica are bad, you have no idea how creepy Silicon Valley can get.

.. image:: {filename}/images/ceiling-cat-watching-you.jpg
  :alt: Ceiling Cat is watching you masturbate from his office at Palantir.
  :align: left
