The Shape of Water
##################

:date: 2018-04-29 20:00
:summary: Been watching *The Shape of Water* with Catherine tonight. Guillermo Del Toro should do a sequel called *The Innsmouth Look*.
:tags: guillermo del toro, sally hawkins, hellboy, abe sapien, h. p. lovecraft
:summary_only: true
:og_image: /images/the-shape-of-water-poster.jpg
 

Been watching *The Shape of Water* with Catherine tonight. Even though this has no official connection with the *Hellboy* film Guillermo Del Toro directed, I have a hard time not associating the fishman in *The Shape of Water* with Abe Sapien. The thing is, Ape Sapien can speak, and there are no reference to the BPRD.

.. figure:: {filename}/images/the-shape-of-water-poster.jpg
  :alt: movie poster for "The Shape of Water"
  :align: left
  
  Theatrical release poster for *The Shape of Water*

Michael Shannon is a creepy bastard as Richard Strickland, and Sally Hawkins is brilliant as Elisa Esposito. Octavia Spencer deserves major respect for her supporting role as Zelda Fuller.

The sets were gorgeous, and Guillermo Del Toro both recreates 1962 Baltimore while at the same time making the setting seem fantastical. Though Del Toro was supposedly inspired by *The Creature from the Black Lagoon*, which had no *official* connection to H. P. Lovecraft, I still think there's some secondhand Lovecraftian goodness to be had here.

Maybe there'll be a sequel called *The Innsmouth Look*?
