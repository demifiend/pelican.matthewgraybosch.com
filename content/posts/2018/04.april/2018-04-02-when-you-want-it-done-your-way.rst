When You Want It Done Your Way
##############################

:date: 2018-04-02 12:15
:category: Just Tech Things
:summary: Open Source isn't Burger King. You can't always expect to have it your way. Sometimes you need to do it yourself.
:tags: web development, front end, web design, oedipus, diy, custom theme, pelican, starter kit, static site generator, python
:summary_only: true


I like static site generators. I like the idea of taking some components, adding my own code and markup, and having the result crank out a website from content files.

Unfortunately, Jekyll is a pain in the ass to get working on OpenBSD, whereas Pelican is as easy as ``doas pkg_add -iv pelican py3-pip`` and then ``pip3.6 install jinja`` (you need Jinja 2.10, but AFAIK the OpenBSD port instals 2.8).

There's just one problem with Pelican: the themes_ are terrible. At least, *I* think so.

However, open source isn't Burger King. You can't always expect to have it your way. Sometimes you need to do it yourself.

So that's what I did. I built my own theme for Pelican, inspired by `this website <http://motherfuckingwebsite.com>`_ and this `other website <http://bettermotherfuckingwebsite.com>`_. I also drew upon `this guy's take <https://codepen.io/dredmorbius/pen/KpMqqB>`_, because why not?

You're reading this post on the result of my work and pillaging of others' designs. Better still, because I wanted something I could use to spin up other websites relatively quickly, `you can use it yourself <https://github.com/matthewgraybosch/oedipus-pelican-starter>`_.

That's right. Anybody who wants to is welcome to use my "Oedipus" theme. Clone or fork the repository, customize it, and have fun. Just keep in mind that the documentation's a bit rough because it's one of those "scratch your own itch" projects. Your best bet is to go through the files yourself, open them in a text editor, and figure out how how everything works.

However, if you have any questions you can ask me on Mastodon_ or file an issue.

.. _themes: https://github.com/getpelican/pelican-themes
.. _Mastodon: https://octodon.social/@starbreaker
