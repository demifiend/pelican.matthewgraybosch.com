Shattered Guardian Session 8
############################

:date: 2018-04-24 12:30
:summary: Back to Naomi, though this session didn't go as well or last as long as I had hoped. I'll just have to do better tomorrow. I was tempted to start earlier and bring in Cerberus again, but he didn't really have anything relevant to say despite being a *good* dog.
:category: Serials
:tags: annelise copeland, christabel crowley, isaac magnin, queens, trauma, eyewitness, nightmares
:summary_only: true
:og_image: /images/starbreakercollage.jpg


Back to Naomi, though this session didn't go as well or last as long as I had hoped. I'll just have to do better tomorrow. I was tempted to start earlier and bring in Cerberus again, but he didn't really have anything relevant to say despite being a *good* dog.

Don't forget that you can read the `full draft`__ if you haven't seen what already happened.

.. __: /starbreaker/novels/shattered-guardian/

Queens - Naomi Bradleigh
========================

After enduring a lecture on the respect due the dead from Cerberus, Naomi
followed the daemon's guidance deeper into the Adversaries' graveyard to
find a plot set apart from the others by a circle of carpet roses. Two newly
planted saplings flanked a hunk of basalt thrice the size of other
Adversaries' grave markers, and bore the following inscription.

| Morgan Stormrider
| 2082 - 2113
| 2100 - 2112
| Weapon by design. Human by choice.

Below this, the memorialist responsible for this stone had carved the
majority of the Crowley's Thoth discography.  It began with the first album
Morgan and Naomi had written together, *Prometheus Unbound*, and ended with
their most recent rock opera *The Stars My Destination*.

The grass in front of the grave was littered with bouquets of flowers whose
scent fought a desperate rearguard action against the reek of cheap whiskey
somebody had poured out on the ground.  Somebody had left an autographed
copy of the long out-of-print *Prometheus Hits the Road* double live album,
as if Morgan might have needed it.

Salt burned Naomi's eyes in a flash flood of tears, and she dropped the
roses she had meant to place before Morgan's grave.  "You bastard.  You just
*had* to play the hero." 
