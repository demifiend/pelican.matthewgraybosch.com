Nick Macario of dock.io is a sexist asshole.
############################################ 

:date: 2018-04-27 13:38
:summary: Who's Nick Macario? Some sexist asshole whose startup with an all-male development team picked the wrong day to spam me.
:category: Sillycon Valley
:tags: nick macario, growth hack, spam, asshole, ceo, dock.io, fuck your startup, sexism, sexist hiring practices
:summary_only: true
:og_image: /images/nick-macario-dock-io-sexist-asshole.jpg


Who's `Nick Macario`__? He's `just another reason`__ Silicon Valley should be forcibly evacuated and repurposed for aboveground nuclear testing. 

Here's what the asshole looks like, so there's no confusing him for some other poor bastard named "Nick Macario".

.. image:: {filename}/images/nick-macario-dock-io-sexist-asshole.jpg
  :alt: Photo of dock.io founder Nick Macario taken from his Birdsite profile
  :align: left

He's currently acting as CEO of `dock.io`__, yet another worthless fucking social media startup that growth hacks by harvesting new users' contact data and *fucking spamming everybody by email*.

.. __: https://twitter.com/nickmacario
.. __: https://www.linkedin.com/in/nickmacario/
.. __: https://dock.io

How do I know dock.io is fucking worthless? I had to hear about it by spam sent because some dumb fuck of a programmer from Brunei had me in his LockedIn contacts. Never mind that I haven't had a profile on *that* shithole in over a year.

Also, take a look at dock.io's `about page`__. See what the women do? Sure, there's a woman co-founder. Whoop-de-fucking-do. How many women does this startup have on the *development team*?

.. __: https://dock.io/about

**Not a single goddamn one. The development team is a sausage fest.**

What do the women working for dock.io do? They're in the "customer success" department. They're fucking *tech support*. Are there any guys in that role? You might as well ask if the President is lying about having played hide the salami with Stormy Daniels.

It's 2018, and I'm willing to bet that if an Adversary from my `Starbreaker saga`__ put a sword to Nick Macario's throat and demanded that he explain why he couldn't find at least three female developers, the first sentence out of his fucking mouth would contain the phrase "culture fit".

.. __: /starbreaker/

This motherfucker's startup picked the *wrong* day to spam me. I'm scared for my wife, and would love nothing better than to channel that fear into anger and lash out at an acceptable target. And since I've already pimp-slapped `the incels`__, this putz will do.

.. __: {filename}/posts/2018/04.april/2018-04-25-open-letter-incel-assholes.rst

Why am I calling out Nick Macario for this bullshit on my website? Because I damn well can. What's he gonna do, not hire me? As CEO he is responsible for the assholes working under him, but he's too chickenshit to put his email address on the website, and I'm not about to join Birdsite or LockedIn just so I can tell this filth what I think of him, his latest venture, and his hiring practices.
