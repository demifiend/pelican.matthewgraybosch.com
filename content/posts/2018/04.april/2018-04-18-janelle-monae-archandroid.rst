Janelle Monáe: *The ArchAndroid* (2010)
#######################################

:date: 2018-04-18 11:49
:summary: I'm late to the party as usual, checking out Janelle Monáe concept album *The ArchAndroid* eight years after everybody else. I'm used to getting sci-fi concept albums in rock, but you know what? This *works*. I love how she's reinterpreting Fritz Lang's *Metropolis* and making it her own.
:category: Metal Appreciation
:tags: janelle monáe, pop, sci-fi, afrofuturism, concept album
:og_image: /images/janelle-monae-archandroid.jpg


I'm late to the party as usual, checking out Janelle Monáe concept album *The ArchAndroid* eight years after everybody else. I'm used to getting sci-fi concept albums in rock, but you know what? This *works*. I love how she's reinterpreting Fritz Lang's *Metropolis* and making it her own.

.. image:: {filename}/images/janelle-monae-archandroid.jpg
  :alt: Covert art for "The Archandroid" (2010) by Janelle Monáe
