Kobra and the Lotus: "Let Me Love You" (2018)
#############################################

:date: 2018-04-16 19:02
:summary: The adorably cute cover screams J-Pop, but "Let Me Love You" by Kobra and the Lotus is a surprisingly solid power ballad. Not as catchy as Ghost's "Rats", though.
:slug: kobra-lotus-let-me-love-you
:og_image: /images/kobra-lotus-let-me-love-you-single-cover-cute.jpg


The adorably cute cover screams J-Pop, but "`Let Me Love You <https://www.youtube.com/watch?v=DoMMHn_bAJw>`_" by Kobra and the Lotus is a surprisingly solid power ballad. Not as catchy as Ghost's "Rats", though.

.. image:: {filename}/images/kobra-lotus-let-me-love-you-single-cover-cute.jpg
    :alt: Cover for Kobra and the Lotus's 2018 single, "Let Me Love You"
    :align: left
