VuuR - *In This Moment We Are Free - Cities* (2017)
###################################################

:date: 2018-04-24 10:32
:summary: Anneke van Giersbergen is at it again, showing her heavy side with a new progressive metal project called VuuR named after the Dutch word for "fire". Each song describes a different city that has left its mark on Ms. van Giersbergen in her travels as a touring musician. Each reveals just how underrated a vocalist she is.
:category: Metal Appreciation
:tags: vuur, anneke van giersbergen, progressive metal, concept album
:og_image: /images/vuur-in-this-moment-we-are-free-cities-2017.jpg


Anneke van Giersbergen is at it again, showing her heavy side with
a new progressive metal project called VuuR named after the Dutch word for
"fire". Each song describes a different city that has left its mark on Ms. 
van Giersbergen in her travels as a touring musician. Each reveals just how 
underrated a vocalist she is.

.. image:: {filename}/images/vuur-in-this-moment-we-are-free-cities-2017.jpg
	:alt: Cover art for "In This Moment We Are Free - Cities" (2017) by VuuR
	:align: left
