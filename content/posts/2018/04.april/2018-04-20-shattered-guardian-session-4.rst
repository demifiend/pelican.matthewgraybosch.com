Shattered Guardian Session 4
############################

:date: 2018-04-20 13:01
:summary: I was never happy with what I did in the last session, so I had to go back and fix it before I continued. This session went better.
:category: Serials
:tags: naomi bradleigh, queens, grief, cemetery gates
:summary_only: true
:og_image: /images/starbreakercollage.jpg


I was never happy with what I did in the last session, so I had to go back
and fix it before I continued.  This session went better.  As always, you
can read the `current state`__ of the novel in its entirety.

.. __: /starbreaker/novels/shattered-guardian/

Queens - Naomi Bradleigh (continued)
====================================

A whir of machinery broke the silence, followed by the clicks of locks
disengaging.  Naomi reached for one of the gates, sure she would have to put
her back into opening.  She was sure the gate would creak and scream for
lack of oil.  Instead, the gate receded at her touch, silently opening
inward as if the hinges had been oiled before the caretakers went home for
the night.

Granite stones in rank and file shaded by trees on the verge of bursting
into leaf stood sentry on either side of the brick-paved path Naomi
followed.  Lesser, narrower paths branched off at intervals, but Naomi
ignored those.  A map appeared in her overlay, showing her current position
and providing guidance to the inner necropolis where Adversaries who had
served honorably were buried regardless of how they died.

Even if she had lacked the technology to follow a map drawn by a tiny
computer implanted in her head, Naomi would have had no trouble finding her
destination.  A lone flame flickered in the distance, visible through the
tree branches.  Passing a breach in a stone wall that separated Nationfall's
dead from the much smaller plot dedicated to the repose of the Phoenix
Society's sworn swords, Naomi stood before another gate.

This one bore a shorter but more ominous Latin motto: *Mors Omnibus
Tyrannis*.  As before, Naomi's implant provided a translation in the
overlay, but she didn't need it.  Despite the emphasis on due process,
"death to all tyrants" was the Adversaries' purpose.

Naomi passed the gate as it opened before her and checked her surroundings. 
She was definitely in the Adversaries' graveyard now; the headstones were
all basalt rather than granite, and devoid of any hint of religious
symbolism.  Whatever faith they had held in life was irrelevant; whether
they had fallen in the line of duty or not they had dedicated themselves not
to their chosen gods, but to the ideals of liberty, equity, and justice for
all.  As such, every stone bore the sword, serpent, and scales emblematic of
the Civil Liberties Defense Corps.  In place of an epitaph, every grave bore
the same Latin motto Naomi had seen over the gate.

Naomi turned off the miniature map in her overlay.  The flame was closer
now, her path to it direct.  Within minutes she stood before a massive
granite pedestal.  A heroically proportioned male figure hewn from basalt
reclined against a mountain crag, nude save for broken manacles and leg
irons.  He crushed a dead eagle underfoot, and in the palm of his upraised
left hand an eternal flame burned.  Naomi had not needed to read the
inscription or translate it from Latin to recognize the figure or the
allegory in marble before her.  Here stood Prometheus, the unconquered
lightbringer, watching over those who had given at least a part of their
lives to the liberation of their fellows.

Approaching the pedestal, Naomi placed one of the bouquets before the
massive foot Prometheus was not using to trample symbolic raptors,
retreated, and kneeled as if to pray.  "For my fallen comrades," she
whispered.  She remained kneeling for several minutes, but held the humanist
titan's stony, unseeing gaze.

It was one thing to kneel out of respect for her fellow Adversaries, but
Naomi would not do so with eyes downcast as if the sculpture erected in
their honor represented a god.  *What would Morgan think if he saw me
cowering before a mere statue of a god?*

The statue winked at Naomi.  Before she could dismiss it as her imagination
or a trick of the light, its igneus jaw opened to speak.  "You have some
nerve, little demifiend, to glare at me with such defiance in your eyes."

Half sure she had finally gone round the bend, Naomi stood and answered.  "I
was just thinking that if you really were Prometheus you'd appreciate a bit
of sass.  But you're probably just a figment of my imagination."

"Sorry to disappoint you, but I'm as real as the stone over your toyboy's
grave.  He died for nothing, you know."

"He saved tens of thousands of lives."
