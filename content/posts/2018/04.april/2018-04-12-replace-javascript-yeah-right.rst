Replace JavaScript? Yeah, right.
################################

:date: 2018-04-12 19:23 -0400
:category: Just Tech Things
:summary: "Let's replace JavaScript with something better," says John Ankarström. "It'll be easy", he says.
:tags: javascript, malware, adtech, replacement, opt-in, cookies, rant
:summary_only: true


`"Let's replace JavaScript with something better," says John Ankarström.`__ "It'll be easy", he says.

.. __: https://john.ankarstrom.se/english/texts/replacing-javascript/

I'd love to see that happen, but I don't think Mr. Ankarström adequately explains what should be done 
with all of the JS currently in the wild. We can't just nuke it all from orbit, as much as I would 
like to stick it to the adtech industry as a whole (especially Google and Facebook) by doing so.

I'd honestly settle for browsers handling JS the way they do cookies. Let me decide whether to 
allow all JS, allow only self-hosted JS, or disable JS entirely -- and let me blacklist/whitelist 
particular domains.

Is that so fucking hard?

It must be really fucking hard, because even Mozilla -- whose PR is all about how they alone care 
about users -- hasn't done so. They don't even implement the option but hide it in ``about:config``
so that only people who know what they're doing mess with it.

Why? Maybe the developers think that ad-blocking extensions are good enough. However, they aren't.

Do ad blockers kill Google Analytics? Do they kill all of the godawful shit Facebook uses to track
people across websites? I don't know for sure, but probably not. However, being able to *blacklist* 
JavaScript from Google and Facebook, or refusing to run any third-party JS at all, would offer better 
protection.

"B-b-but disabling JavaScript will break the Web!" I can hear other developers saying.

I think that's bullshit. I think that disabling JavaScript won't break the web. It will only expose
the extent to which the web is already broken *because of JavaScript*.

Most websites are defective because most web developers don't get the fundamental principle of progressive enhancement:

1. Use clean, semantic HTML5 to deliver core functionality.
2. Implement all client/server communication using HTML5 forms and HTTP GET and POST operations.
3. Use *just enough* CSS to make the page look good.
4. Check browser capabilities after the basic page has rendered and *offer* enhanced functionality using JS to clients with current equipment and fast connections.

It shouldn't be that hard to grasp. I manage it, and I'm just some long-haired 
metalhead who rants about the internet when he should be writing.
