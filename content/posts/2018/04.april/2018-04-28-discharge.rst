Discharge
#########

:date: 2018-04-28 22:00
:summary: Catherine is back from the hospital, and doing better than I expected.
:tags: wife, surgery, care, gelato fiasco, jackie chan, the foreigner
:summary_only: true


Catherine is back from the hospital, and doing better than I expected. She slept most of the day after joining me on a quick supermarket run, but was up to letting me spoil her with beef stroganoff for dinner and watching Jackie Chan's `The Foreigner`__ with me while we ate `gelato fiasco`__.

.. __: http://www.vulture.com/2017/10/jackie-chans-the-foreigner-is-a-weighty-revenge-drama.html
.. __: https://gelatofiasco.com
