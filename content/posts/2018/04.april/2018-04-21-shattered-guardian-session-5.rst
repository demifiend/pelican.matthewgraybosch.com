Shattered Guardian Session 5
############################

:date: 2018-04-21 15:22
:summary: I wasn't expecting to have Sabaoth playing silly buggers with a statue of Prometheus in the Adversaries' Graveyard, but it works.
:category: Serials
:tags: naomi bradleigh, queens, grief, cemetery gates
:summary_only: true
:og_image: /images/starbreakercollage.jpg


Picking up where I left off yesterday, with only a little bit of
backtracking.  I wasn't expecting to have Sabaoth playing silly buggers
with a statue of Prometheus in the Adversaries' Graveyard.  

I think it
works, though. In my last two books I had tried to keep things reasonably well-grounded, but it's time to cut loose and get weird.

As always you can `read the current draft`__.

.. __: /starbreaker/novels/shattered-guardian/

Queens - Naomi Bradleigh (continued)
====================================

The statue winked at Naomi.  Before she could dismiss it as her imagination
or a trick of the light, its igneus jaw opened to speak.  "Such a brave
little demifiend to glare at me with such defiance in your eyes."

Half sure she had finally gone round the bend, Naomi left her roses on the
ground as she rose to her feet.  "I was just thinking that if you really
were Prometheus and not a figment of my imagination you'd appreciate a bit
of sass."

"Sorry to disappoint you, but I'm as real as the stone over your toyboy's
grave.  He died for nothing, you know."

*Maybe he did, but I'll be damned if I'll forgive this arsehole for saying
so.* A distorted bass hum set Naomi's teeth on edge as she drew her sword. 
A crystalline gray blade whose black veins pulsed with tenebrous light
caught the firelight and drank it in.  She raised the blade so that its tip
pointed toward the statue's face despite her sudden and urgent longing to
drop the loathsome weapon and flee.  "Why don't you come down here so I can
kill you?"

"What's this?  Another false Starbreaker?" The blade itself snarled as if
insulted, but the statue continued.  "No, not a false one this time.  You
know, little demifiend, Morgan might actually have killed me if you had
trusted him with your little secret."

Naomi's vision blurred as the realization struck home, and despite herself
she struggled to find the words to explain what had really happened.  Before
she could, the statue added, "Perhaps you never truly loved him.  Ah!  Could
that be the truth, that rather than loving him you loved the idea of him
loving *you*?"

"That is *quite* enough," said Naomi.  Springing onto the pedestal, she
thrust her sword into the basaltic breast of the idol through which the
presence taunting her spoke.  Cracks radiated through the stone from the
point of impact as the weapon's bass hum intensified.

"Go and pay your respects," said the statue, though it had lost its lower
jaw.  "I will reunite you with your lover soon."

"Wait.  What do you mean?" Naomi suspected the statue's last words were a
threat, but craved confirmation.  Before she could withdraw the weapon, the
statue crumbled into glittering dust.  Covering her nose and mouth to avoid
inhaling the dust that had once been a statue hewn from volcanic rock, Naomi
sprang back while remaining on guard.  The voice that had spoke through the
statue spoke no more.

Instead, Cerberus cut in on a restored connection whose interruption Naomi
had not noticed.  «I didn't let you in here so you could wreck priceless
statuary and pick fights!  Bad human!»
