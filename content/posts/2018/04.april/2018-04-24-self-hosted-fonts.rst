Self-Hosted Fonts
#################

:date: 2018-04-24 00:00
:summary: Rather than settling for system standard fonts, I've chosen a set of fonts that I think would best display my site's contents and `self-hosted`_ them. Initial loads might be a bit slower, but I think you'll find that the initial performance hit is worth it. I'm using Voltaire for headings, Bitter for body text, and Cutive Mono for code samples and pre-formatted text. All are available from Google Fonts.
:category: Just Tech Things
:tags: website, fonts, self-hosted fonts, web fonts


Rather than settling for system standard fonts, I've chosen a set of fonts that I think would best display my site's contents and `self-hosted`_ them. Initial loads might be a bit slower, but I think you'll find that the initial performance hit is worth it. I'm using Voltaire for headings, Bitter for body text, and Cutive Mono for code samples and pre-formatted text. All are available from Google Fonts.

.. _self-hosted: https://google-webfonts-helper.herokuapp.com/
