Mentioning Tikal leads to Accidental Worldbuilding
##################################################

:date: 2018-04-23 19:00
:summary: Mentioning the ancient Mayan city of Tikal in my last writing `session`_ raised some interesting questions about the setting. They weren't relevant before, but they might be now.
:category: Mapping the Territory
:tags: tikal, yax mutal, maya, mayan, native americans, conquest, genocide, revival, algonquin, lenape, unami
:summary_only: true
:og_image: /images/raymond-ostertag-tikal-temple1-2006-08-11.jpg


In my last writing `session`_ the character whose viewpoint I was writing was still having nightmares related to a `futbol`_ match in `Tikal`_.

Tikal is the modern name for the ruins of a Mayan city that had probably been called Yax Mutal.

.. image:: {filename}/images/raymond-ostertag-tikal-temple1-2006-08-11.jpg
  :alt: Photo of Tikal Temple 1 by Raymond Ostertag
  :align: left

So, what happened? Either the Native Americans were never conquered and subjected to genocide and cultural erasure, or after Nationfall surviving Native Americans revived their indigenous culture and reclaimed their old cities.

Practical implications of Native Americans never being conquered and genocided would include traditions and culture intersecting and merging with European culture to a greater extent. Bare minimum you'd see New York City street signs and official documents rendered in both English and Unami (an Algonquin language spoken by the `Lenape`_ people who lived in the area).

But this assumes that the Lenape were or became literate, and as far as I can tell that's not necessarily the case. More research might be necessary, especially if the continued existence or revival of Native American culture and traditions alongside European culture in my setting's version of the Americas becomes more relevant to plot or characterization.

.. _session: /posts/2018/04.april/2018-04-23-shattered-guardian-session-6.rst
.. _Tikal: https://en.wikipedia.org/wiki/Tikal
.. _futbol: https://en.wikipedia.org/wiki/Association_football
.. _Lenape: http://www.talk-lenape.org/
