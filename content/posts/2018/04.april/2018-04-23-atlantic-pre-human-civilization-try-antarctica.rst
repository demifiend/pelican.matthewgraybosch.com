Maybe They Should Try Looking in Antarctica?
############################################

:date: 2018-04-23 20:00
:summary: There's an article in *The Atlantic* that discusesses the possibility of pre-human civilizations and how we might find evidence of their existence, but it makes no mention of Lovecraft. What about the shoggoths?
:tags: pre-human civilization, quaternary period, h. p. lovecraft, at the mountains of madness, antarctica, shoggoths


There's an article in `The Atlantic`__ that discusesses the possibility of pre-human civilizations and how we might find evidence of their existence, but it makes no mention of `H. P. Lovecraft`__, who considered the possibility in `At the Mountains of Madness`__ almost a century ago. Yes, he was a racist, an anti-Semite, and a misogynist, but *what about the shoggoths*?


.. __: https://www.theatlantic.com/science/archive/2018/04/are-we-earths-only-civilization/557180/
.. __: http://www.hplovecraft.com
.. __: http://www.hplovecraft.com/writings/fiction/mm.aspx
