The Web Sucks Because of Web Developers
#######################################

:date: 2018-04-06 12:00
:category: Just Tech Things
:summary: The World Wide Web sucks nowadays because too many web developers are ignorant techbros who fail to consider human factors and insist on using trendy tech.
:tags: web development, front end development, progressive enhancement, accessibility, javascript, cancerscript
:summary_only: true


I just read a transcript of a brilliant talk called `Dear Developer, The Web Isn't About You`__.

.. __: https://sonniesedge.co.uk/talks/dear-developer

Here's the short version: The World Wide Web sucks nowadays because too many web developers are ignorant techbros who fail to consider human factors and insist on using trendy tech. These people didn't grow up accessing the network on dialup. They never had to make do with browsers like `lynx`__ or `Mosaic`__. They're spoiled, arrogant, and think that if a website works for them and their coworkers then they've done their job.

.. __: http://lynx.invisible-island.net/
.. __: http://www.ncsa.illinois.edu/enabling/mosaic

It doesn't work that way, kids. 

If your site doesn't render in Lynx on a connection throttled to 28.8Kbps within 2 seconds, then your site is defective. 

If your single page app doesn't at least provide a "This app requires JavaScript and a browser like Google Chrome or Mozilla Firefox" inside a set of ``<noscript>`` tags, then your app is defective.

And if you built a personal blog using fucking `React`__, then you deserve to be doxxed and exposed to public ridicule for grotesquely overengineering something people on `sdf.org`__ manage to do serving up plain text files with fucking `Gopher`__.

.. __: https://www.gatsbyjs.org/
.. __: http://sdf.org
.. __: http://gopher.floodgap.com/overbite/

And if you don't know what Gopher is, you need to get off my lawn *post* fucking *haste*.

Who am I to say this? I'm the guy who might end up maintaining your code after you move on to more interesting things. I'm the guy who will clean up your messes and make your shit work for real people.

Don't make my job harder. If you do, and I find out who you are, I'll put you in my next novel, give you misshapen genitalia and embarrassing sexual proclivities, and inflict upon your literary counterpart a gruesome death.

I've done it before. I will do it again. All I need is a good excuse.
