A Quick Advertisement
#####################

:date: 2018-04-16 21:40
:summary: I know you hate ads as much as I do. Please bear with me. You might not be aware that I'm an author, but I've written a couple of science fantasy novels called `Without Bloodshed <https://www.amazon.com/Without-Bloodshed-Starbreaker-Book-1-ebook/dp/B00GQ0BJOO/>`_ and `Silent Clarion <https://www.amazon.com/Silent-Clarion-Collection-Matthew-Graybosch-ebook/dp/B01MCWCSPA/>`_. I'd love it if you were to buy them, read them, leave reviews, and tell your friends about them. Thanks!
:og_image: /images/starbreakercollage.jpg


I know you hate ads as much as I do. Please bear with me. You might not be aware that I'm an author, but I've written a couple of science fantasy novels called `Without Bloodshed <https://www.amazon.com/Without-Bloodshed-Starbreaker-Book-1-ebook/dp/B00GQ0BJOO/>`_ and `Silent Clarion <https://www.amazon.com/Silent-Clarion-Collection-Matthew-Graybosch-ebook/dp/B01MCWCSPA/>`_. I'd love it if you were to buy them, read them, leave reviews, and tell your friends about them. Thanks!

