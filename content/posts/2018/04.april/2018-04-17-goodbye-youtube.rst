Goodbye, YouTube
################

:date: 2018-04-16 19:29
:summary: I'm glad I nuked my YouTube account. Being able to view videos without logging in is nice, and without an account I'm not tempted to wrestle with pigs by posting comments on videos. I'd love to do the same with Google+ and Gmail, but getting free of those disservices will be much harder.


I'm glad I nuked my YouTube account. Being able to view videos without logging in is nice, and without an account I'm not tempted to wrestle with pigs by posting comments on videos. I'd love to do the same with Google+ and Gmail, but getting free of those disservices will be much harder.
