Shattered Guardian Session 6
############################

:date: 2018-04-23 12:01
:summary: Time for a viewpoint shift. My plans for this scene called for Naomi to meet somebody presumed dead in *Without Bloodshed*, so it's time to bring her onto the stage.
:category: Serials
:tags: annelise copeland, christabel crowley, isaac magnin, queens, trauma, eyewitness, nightmares
:summary_only: true
:og_image: /images/starbreakercollage.jpg


Time for a viewpoint shift.  My plans for this scene called for Naomi to
meet somebody presumed dead in *Without Bloodshed*, so it's time to bring
her onto the stage.  She's an eyewitness so the events immediately preceding
Morgan's death, so Imaginos needs her.

Queens - Annelise Copeland
==========================

Life had been easy for Annelise Copeland since giving up her stage name and
her career in music.  While Isaac no longer came to her and played her body
with greater virtuosity than she ever brought to the violin -- or gave her
that drug by the name of World Without End that heightened her sensitivity
until the slightest breath upon her skin threatened to set her off -- he had
delivered on everything he had promised Annelise when he first asked her to
leave her life behind and become an actor in history.

She had never had to worry about money.  People bought the clothes she
designed and wore them in public.  Her little boutique in Brooklyn enjoyed
exactly the sort of discreet prosperity Annelise had come to crave, and she
no longer missed being a wizard's lover and spy.  She had been glad to leave
all of the weirdness behind.

*Naturally*, thought Annelise as she woke from a nightmare of flames and
voices in the sky that still occasionally recurred months after that futbol
match in Tikal, *the weirdness just had to come and find me again.*

Giving up on sleep, Annelise wrapped herself in a cotton robe.  Navigating
mostly by feel, she padded into the kitchen before turning on a light to
make coffee.

The screen mounted by the stove flashed an "incoming call" message, and
Annelise considered telling the building's daemon to dismiss it.  Instead,
she asked, "Who would *dare* call me at one in the morning?"

The daemon replied over the speaker in a soft, masculine voice because
Annelise had gotten her implant removed as soon as she had left Isaac
Magnin's service.  "Ms.  Copeland, it's Isaac Magnin from the AsgarTech
Corp--"

"I know who Isaac Magnin is.  Put him on."

"I would wish bid you a good evening, Annelise, but I suspect it has not
been for you.  Did I wake you, my dear?"

*He ignores me for months, but now it's 'my dear'?  Not likely.* A glance at
the screen showed Magnin in his usual double-breasted white suit, wearing a
pair of sapphire cufflinks she had given him as a Winter Solstice gift. 
"You didn't wake me, but I'm not in the mood for diamonds and rust right
now."

Magnin shook his head, and a small, sad smile Annelise could not recall ever
having seen before curved his lips.  "You resent my absence, then?  I'm
sorry; I had thought that you'd want some time on your own to remember how
to be yourself again and happy."

"I needed the time," said Annelise, and her voice slipped the leash, "But I
missed you too."

"We can't be what we were before."

"But I have nobody else with whom I can talk about the last decade of my
life."

Now Magnin's smile took a wry turn.  "I might be able to fix that, but you
should be careful what you wish for.  I need you to accompany me on a late
night graveyard visit.  I suspect Naomi will be there soon, finally paying
her respects at Morgan's grave, and I need to speak with her.  Having you
there with me might keep her from drawing her sword long enough to actually
listen to me."

"What if I'd prefer not to?"

"Then I can bring your understudy," Annelise shuddered at the mention of the
artificial being who had taken on her identity so she could escape the life
she built in Magnin's service, "but it might be better for everybody
involved if you came."

After a long moment, Annelise made her decision.  "Can you pick me up?"

"A limousine awaits.  Come down when you're ready."
