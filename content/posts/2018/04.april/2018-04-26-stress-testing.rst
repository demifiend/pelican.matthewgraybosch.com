Stress Testing
##############

:date: 2018-04-26 18:30
:summary: I feel like a jerk when I get frustrated because an application I'm supposed to stress test falls over and dies as soon as I poke at it, especially when the poor bastards who developed it had been working 60-80 hours a week for the last couple of months just to get it this far. It's still their fault the app's a piece of shit, but could I have done better? Maybe not.
:category: Just Tech Things
:tags: testing, software, bugs, overtime, empathy, frustration, Windows


I feel like a jerk when I get frustrated because an Windows desktop application I'm supposed to stress test falls over and dies as soon as I poke at it, especially when the poor bastards who developed it had been working 60-80 hours a week for the last couple of months just to get it this far. It's still their fault the app's a piece of shit, but could I have done better? Maybe not.
