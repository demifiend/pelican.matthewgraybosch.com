No Rest for the Wicked
######################

:date: 2018-04-19 01:17
:summary: I should be in bed with my wife, but instead I'm working on this website and adding more content for *you*. I hope you appreciate it.
:tags: website, starbreaker, free fiction, content
:summary_only: true

I should be in bed with my wife, but instead I'm working on this website and 
adding more content for *you*. I hope you appreciate it. I've got lots of free 
fiction for you: short stories and a couple of novels.

Some of it is still in rough shape, and there are some missing images. I know 
about them, and will fix them as time permits.

I've also got a shitload of old posts from 2016 and earlier that need to be 
converted.

And I'll eventually want to dump everything I've posted to Google+ and put it 
up here so that it isn't lost when Google finally does to their failed attempt 
at killing Facebook what they did to Google Reader.

And I've got to continue writing *Shattered Guardian*. you can `read the draft 
</starbreaker/novels/shattered-guardian/>`_ as I add to it.

Most importantly, though, I've got to take care of my wife. She's got breast 
cancer and needs me to keep my shit together.

Yeah, there `ain't no rest for the wicked 
<https://www.youtube.com/watch?v=HKtsdZs9LJo>`_.
