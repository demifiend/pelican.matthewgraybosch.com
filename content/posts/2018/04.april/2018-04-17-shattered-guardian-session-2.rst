Shattered Guardian Session 2
############################

:date: 2018-04-17 18:01
:summary: This session was a little better than the first, with 378 words down. Naomi's cemetery visit has run into a little snag.
:category: Serials
:tags: naomi bradleigh, queens, grief, cemetery gates
:summary_only: true
:og_image: /images/starbreakercollage.jpg


This session was a little better than the first, with 378 words down. Naomi's 
cemetery visit has run into a little snag in the form of a doggy daemon who 
takes his duty of guarding the dead rather seriously.

Readers of previous Starbreaker stories might wonder at my use of "daemon".
In *Shattered Guardian*, I use the word to refer to all AIs who inhabit
mainframes. I'll probably retcon this usage into new editions of *Without
Bloodshed* and *Silent Clarion*.

You can `read the entire draft`__, if you like.

.. __: /novels/shattered-guardian/

Queens - Naomi Bradleigh (continued)
====================================

Selene would have been full tonight, and her light combined with that of the 
stars would have been enough to guide Naomi now that New York had gone dark 
until the morning. Instead, the flashlight she had purchased for this visit
would have to suffice.

Remembering the roses, Naomi turned back to the cab and found the driver 
waiting with a bouquet cradled in each arm. Taking one, she allowed him to 
place the other in her grasp. "Thanks again. I shan't be long."

The driver shrugged and pulled a cigarette case from his pocket. Naomi 
caught a whiff of cannabis as he selected a joint and fired it up. He 
wouldn't be able to drive for at least an hour; the daemon monitoring him
and the cab would refuse to so much as let the motor start. "Take as long 
as you like."

*I'd rather not be here at all.* Naomi kept that to herself as she squared
her shoulders and approached the entrance. Massive wrought iron gates three 
meters tall barred her way, the arch inscribed with a motto that her implant
automatically translated from Latin to overlay in English: "You who prosper 
in peace and plenty, disturb not our well-earned rest."

Unsure of how to request entry, Naomi thrust her flashlight into one of the
pockets of her burgundy wool overcoat and reached for the latch.

A text message from the daemon overseeing the cemetery appeared in Naomi's 
overlay. «Cemetery visiting hours are 08:00 to 20:00. Return tomorrow morning.»

Naomi bared her teeth, even though the daemon was not there to see her display 
of wrath. «I am Naomi Bradleigh, Adversary Emeritus and still sworn to the 
Phoenix Society's service. Who are *you* to order me about?»

«You may call me Cerberus, Adversary Emeritus.»

That figured. It seemed to Naomi that every daemon set to manage a cemetery took
the name of a god or monster associated with the various underworlds. «I came to
pay my respects to fallen comrades. Must I disturb Malkuth to tell him you're 
being a bad dog?»

«But I'm a good dog. A *good* dog.»

«Prove it, please. Let me in, so that I can visit my dead discreetly and not have
paparazzi baying at my heels.»
