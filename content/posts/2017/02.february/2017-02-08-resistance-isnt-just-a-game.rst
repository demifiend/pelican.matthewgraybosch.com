Resistance Is Not a Game, Dammit
################################

:date: 2017-02-08 14:03
:category: As The World Burns
:tags: protest, resistance, antifa, tech, techbros
:summary: I might not be out in the streets protesting the Republican regime, but I won't tell the people who are that they're wasting their time. That sort of talk only helps the bastards keep us down.
:slug: resistance-not-a-game
:summary_only: true


Feminist and activist Ijeoma Oluo recently wrote an interesting article on [Ev Williams' new content farm](https://medium.com) called `"Fuck This White Dude Game Theory" <https://medium.com/@IjeomaOluo/fuck-this-white-dude-game-theory-ae59b5bed21#.pk66xg73a>`_, and this bit got me thinking:

    I have worked in the tech industry. I live in a tech city. Nowhere else in America will you find a culture more steeped in the supposed brilliance and ingenuity of white men. And when articles start coming out from white tech dudes stating that the resistance headed by women and POC is just playing into the hands of brilliant white dudes, I'm gonna need y'all to pause.

    When articles are coming out from people with absolutely no political experience other than the fact that when they are playing computer games there's always some hella complicated strategy and everything happens for some pre-planned reason, and suddenly that becomes the defining narrative, I'm gonna need y'all to pause.


She's right. The Republican regime isn't a cabal of diabolical masterminds. They're a bunch of stooges for rich reactionaries to whom concepts like *noblesse oblige* and *civic duty* are utterly alien. But there's ore.

    And when those articles are making people doubt the effectiveness of getting out in the streets and marching (as if the fuck-all nothing everybody's been doing before this was so damn effective) and makes people give up before they even start, I'm gonna need y'all to pause.

    Why aren't we questioning the narrative that we are fighting brilliant white dudes and only other brilliant white dudes can save us? How have we allowed people who have the least to lose in this battle shake our resolve?

Again, she's right. If real life *were* a computer game or a fantasy novel where a few evil geniuses are fucking everything up, the world would be too simple to be worth saving. White techies discouraging direct action need to find themselves a mirror and figure out whose side they're really on.

Me? I'm on *my* side, and that just happens to be the same side as the protesters. I've got less to lose under a fascist regime than most people, but that doesn't mean I want to see Republicans finish the job of dismantling the republic.

As one of the white techies with little to lose, I *applaud* the protests. If I were younger and had the balls, I might be on the streets myself. Unlike some of my "fellow" white techies, I know the resistance isn't just a game. I know the stakes are life-and-death for those out front. I know the protesters aren't just trying to be annoying. `I know that you aren't trying to persuade me, but to show those who share in your oppression <https://humaniterations.net/2012/02/29/you-are-not-the-target-audience/>`_ that it is *possible* to fight back and that the system is not yet invincible.

Actions vs Words
================

As one of the white techies with little to lose, I *applaud* the protests, but I don't *participate*. You don't see me there with you because despite being a slightly queer working-class atheist with black ancestors, I can pass as one of the normal middle-class cisgender heterosexual WASP dudes. That's why I tell my friends and acquaintances that it's *long past time* to organize and go on strike. They won't listen to you, but because they think I'm "one of them" they might hear me out before dismissing me as "just another SJW".

Trump and his alt-reich neo-Nazi scumbag cronies can't do much without the modern surveillance state. If we techies were to down tools and refuse to cooperate, the system would grind to a halt. If we escalated beyond passive resistance into sabotage or outright electronic warfare against the government (albeit at dire risk to our own lives, liberty, and property) we could accelerate the system's collapse.

Unfortunately, most of the other white techies I know love the taste of their rugged-individualist Flavor-Aid. They think that as long as `they keep their heads down and do what they're told like good Germans <http://www.thirdworldtraveler.com/Fascism/Good_German_Syndrome.html>`_, nothing bad will happen to them or anybody they care about.

So I'm stuck donating to legal defense funds while I keep trying to persuade them. I tell them that even if we don't take to the streets, we still have the right and the responsibility to resist.

Why I'm Not Out There
=====================

Not that you'd care, but I have my reasons. I won't pretend to be "woke". I'm probably not a good ally, and my work is problematic as hell. Despite my efforts I still harbor racist, sexist, and ableist attitudes (I just try not to make them everybody else's problem). 

However, when my wife Catherine agreed to have me as her husband, she didn't insist that I emigrate to Australia, though I had offered to do so. Instead, she chose to leave her old life in Australia and live with me here in America. She looks white and has her green card, but she's still an immigrant and half `Maltese <http://www.khazaria.com/genetics/maltese.html>`_. 

Trump's focus might currently be on brown immigrants from certain majority-Muslim countries (the ones where he doesn't have business interests), but history shows that tyrants always find new scapegoats.

If for no other reason than for *my wife's* sake, I have a responsibility to resist in any capacity I can. Perhaps I could do more than donate to the ACLU and various legal defense funds while playing the subversive and sowing sedition among my social circle, but I also owe it to my wife to resist *carefully*. 

I don't want to see her arrested, imprisoned, deported, or otherwise harmed because of me. This might be my struggle because I'm a US citizen who still believes in liberty and equal justice under law for all, but it shouldn't be *her* fight unless she chooses for herself to take up the cause.
