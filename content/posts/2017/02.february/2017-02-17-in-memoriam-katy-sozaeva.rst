In Memoriam: Katy Sozaeva (1970-2016)
#####################################

:date: 2017-02-17 12:38
:category: From the Fandom
:tags: RIP, in memoriam, tribute, katy sozaeva, belated, fuck cancer
:summary: I was recently reminded of the passing of Katy Sozaeva, one of my first fans and rave reviewers.
:slug: in-memoriam-katy-sozaeva
:summary_only: true


I'm probably a jerk for not saying something sooner, but I'm sorry to know that `indie book blogger Katy Sozaeva <http://katysozaeva.blogspot.com/>`_ lost her struggle against cancer. 2016 claimed far too many musicians that I grew up listening to with my father, but Ms. Sozaeva's death strikes a little closer to home.

I never met Ms. Sozaeva in person, and thus never had the opportunity to properly thank her for her kindness toward me. She was `one of the first to review my first novel <http://katysozaeva.blogspot.com/2013/12/mgraybosch-curiosityquills-review.html>`_ *Without Bloodshed* way back in 2013. She was one of the first to publicly rave about it, and therefore one of my first fans.

I probably should have been more supportive toward her in her fight beyond offering her kind words when I saw her on social media, but it's too late now.

To Katy's friends and family, I offer my condolences. I'm honored that Katy not only read my first novel, but enjoyed it so.

.. {% include image.html src="/assets/images/original/katy-sozaeva-facebook.jpg" alt="photo of Katy Sozaeva" caption="[Photo of Katy Sozaeva downloaded from Facebook](https://www.facebook.com/photo.php?fbid=922363451112553&set=t.100000167216324&type=3&theater)" %}

Links To/About Katy Sozaeva
===========================

- `Katy Sozaeva's blog: "Now is Gone" <http://katysozaeva.blogspot.com/>`_
- `Katy Sozaeva on Goodreads <https://www.goodreads.com/user/show/5552109-katy>`_
- `Katy Sozaeva's review of Without Bloodshed <http://katysozaeva.blogspot.com/2013/12/mgraybosch-curiosityquills-review.html>`_

Tribute to Katy Sozaeva
=======================

According to `Weston Kincade <http://kincadefiction.blogspot.com/>`_ (author of *A Life of Death*, *The Priors*, and *Strange Circumstances*), the owner of `Cabin Goddess <https://cabingoddess.com>`_ is compiling material from everybody who knew Ms. Sozaeva to celebrate her life and ensure her memory lives on. You can find details on Kincade's blog at `"Celebrating Her Life, Katy Sozaeva" <http://kincadefiction.blogspot.com/2016/08/celebrating-her-life-katy-sozaeva.html>`_.
