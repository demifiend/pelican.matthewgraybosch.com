Carol Berg's Transformation: Two Emotional Journeys in One
##########################################################

:date: 2017-02-28 14:12
:category: Hiding Behind Books
:tags: carol berg, transformation, rai-kirah, fantasy, seyonne, aleksander, slave, master, dreams, demons, demon hunter, exorcism, reform, male emotion
:summary: Never mind the cover. If you haven't read *Transformation*, Carol Berg's debut and the first Rai-Kirah novel, you're missing out.
:slug: carol-berg-transformation-two-emotional-journeys-one
:summary_only: true


I just finished reading Carol Berg's *Transformation* last night, and it was just what I wanted in a fantasy novel: just enough worldbuilding, deep and complex characterization for not only Seyonne and Aleksander but most of the supporting cast, a tight plot that kept me guessing, and an ending that allows me to recommend the novel to my wife with a clear conscience.

Never mind the cover, which is so terrible that it's probably now iconic. Carol Berg's debut novel and the first of her Rai-Kirah trilogy is two emotional journeys in one:

1. The slave Seyonne's reclamation of his old identity as a Warden, a warrior-mage capable of battling demons called Rai-Kirah within a possessed person's soul.
2. The prince Aleksander's struggle to remain himself and awaken the light within him as corrupt courtiers plot to make him a demon lord's host.

These entwined character arcs are especially interesting because Ms. Berg points to them with references to prophecies handed down by Seyonne's people predicting a struggle between a master of demons and a "warrior with two souls".

Once Seyonne realizes that despite his latest master's casual cruelty and arrogance he has the potential to be a far better man than he currently is locked within him, he suspects and later comes to fear that Aleksander is this prophecied warrior. However, the truth is far stranger, and far more interesting.

I won't tell you what happens; you really should see for yourself.

The only problem I had was that the antagonists weren't very meaty. They were effective catalysts for the protagonists' emotional journeys as Seyonne reclaimed his old self and Aleksander became a more human person, but were otherwise forgettable.

Despite this quibble, I've already bought Carol Berg's other Rai-Kirah novels: *Revelation* and *Restoration*. You should do the same.
