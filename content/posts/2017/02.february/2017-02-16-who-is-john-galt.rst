Who Is John Galt?
#################

:date: 2017-02-16 08:09
:category: Stranger Than Fiction
:tags: john galt, lord byron, guelph, canada, history
:summary: Fans of Ayn Rand think they know the answer. They don't know much about history.
:slug: who-is-john-galt
:summary_only: true


Who Is John Galt? Fans of Ayn Rand think they know the answer. They're mistaken.

If you aren't a Canadian or familiar with the life and work of Lord Byron or the early literature of the Industrial Revolution, the answer *probably* isn't what you expected. :)

It turns out there actually *was* a guy named `John Galt <https://en.wikipedia.org/wiki/John_Galt_(novelist)>`_, and he isn't a porn star. 

Now that I think of it, though, there *was* a male porn star who used that name as a pseudonym. I recall Lindsay Perigo of the New Zealand right-libertarian group "Libertarianz" and the "Sense of Life Objectivists" made a small fuss over him when I was involved with the latter group between 2001 and the beginning of 2005. (I drifted away soon after getting married. Funny how that works, isn't it?)

The John Galt I'm talking about lived and worked in the early 19th century, and he was Scottish. He was Lord Byron's friend and later biographer, as well as the author of the first biography of artist Benjamin West. He was a businessman, and the founder of Guelph, Canada. 

He was also a novelist and a travel writer, and did time in debtor's prison after the Canada Company (to which he was appointed Secretary) a corporation chartered to help colonize the Huron Tract in Upper Canada (now part of the province of Ontario).

He died in 1839, and his son Alexander was Canada's first Minister of Finance.

PS
==

For those interested, the "Sense of Life Objectivists" still seem to be around at `solopassion.com <https://www.solopassion.com>`_, even if the domain name brings to mind the sound of one hand clapping. Their old site is now located at `rebirthofreason.com <https://www.rebirthofreason.com>`_, which still has some `old posts of mine <https://rebirthofreason.com/Articles/Author_15.shtml>`_ if you're bored and looking for a laugh.
