Starbreaker Next, Chapter 1: "World of Lines" - First Attempt
#############################################################

:date: 2017-05-04 12:59
:category: Starbreaker
:tags: outtakes, claire ashecroft, secret history, cover-up, suppressed truth, penetration testing, world of lines, coheed and cambria, outtakes
:summary: This is my first attempt at opening Chapter 1 of the next Starbreaker novel, where Claire is documenting real (to her) history on the down-low.
:slug: starbreaker-next-chapter-1-world-lines-first-attempt
:summary_only: true


As promised, you can follow as I write the next Starbreaker novel.  

Here is a first attempt at opening the first chapter that I bashed out on my lunch break. I don't know if I'll continue from this angle, though, because it deviates substantially from `the ideas in this post <{filename}/posts/2017/04.april/2017-04-25-starbreaker-next-an-idea-for-an-opening-scene.rst>`_.

The chapter title on this version comes from `a song <https://open.spotify.com/track/190pQLAFPrl8XShdmc7vFV>`_ on Coheed and Cambria's 2010 album, *Year of the Black Rainbow*.

Chapter 1: "World of Lines"
===========================

Have you ever read a newspaper or watched a news broadcast on the network and suspected that you weren't getting the whole story? Have you ever looked at the world around you and asked if this was all there is, if the life you were living was all you would ever get? Have you ever been sure that there was a hidden truth, a world between the lines? That's probably why you're reading this account. It's also the reason I'm sitting in a back room and writing this bloody thing.

The bartender probably thinks I'm using this old laptop to do some penetration testing for a corporation. At least, that's what I told him I was doing. It's a cover I've used before to justify renting tables at cafes or pubs to work away from home when I didn't want something traced back to me over the network.

Sometimes, though, the cover is the real story. I actually would switch from my usual Mewnix installation to Kali Linux and crack a system. Penetration testing is a good way to make money if you know what you're doing -- and I'm as good at penetrating systems as I am at penetrating men.

That's one thing Matthew Graybosch got right about me when he wrote *Without Bloodshed* and *Silent Clarion*. Never mind that he hadn't been there and made up all sorts of shit. 

For example, he had *no* idea what Morgan Stormrider's real name was, or how he got saddled with that ridiculous stage name. His real name is Morgan Sullivan, and the stage name was my fault. While Christabel suggested Morgan take on a stage name because she didn't want people who had a beef with the Adversaries shooting up Crowley's Thoth concerts, the actual name was my idea.

To this day I still can't believe he put up with it, but he insists that he didn't mind because "the name sounded metal".

Incidentally, Morgan knows I'm writing about him. I've got a secure relay chat going on with him, Naomi, and the rest of the crew. Isaac Magnin is here, too, and so are the rest of *those* assholes. They're all here, so we *could* just talk, but there's just one problem.

Actually, there's two, but one follows from the other. You see, you aren't supposed to be reading this. While I published this as a novel, and I'm writing this as if it *were* a work of fiction, every word is true. 

On the Vernal Equinox of 21XX, an entity claiming to be God did in fact appear and demand the worship and obedience of the entire human race on pain of extinction, and this entity's first command was that we turn upon the asuras living among us -- people we thought were just ordinary people with a condition called congenital pseudofeline morphological disorder or CPMD&mdash;and kill them all. That actually happened, and so did the Great Defiance where pretty much the entire human race told this false God to go bugger Himself with a rusty bazooka. 

He called himself Shaddai, the almighty. We called him Sabaoth, the Lord of Armies, because he had always been too chickenshit to do his own genocidal dirty work. Lord of Armies? More like Lord of the War Pigs, the god of all those generals gathered in their masses.

Regardless, you've probably seen one of the angels he would send to kill at a distance once the war was on. Hell, maybe you even took up one of the devil-killer rifles that Isaac Magnin eventually got busted for distributing to 'terrorist factions' and fought them.

The Phoenix Society couldn't cover *everything* up; they had to admit that Sabaoth and his angels were real, and that "a courageous band of Adversaries led personally by the Phoenix Society's executive council" mounted a direct assault against Sabaoth on Luna after driving him off of Earth.

I was there, too. The Phoenix Society won't admit that, any more than they will admit that the XC are mostly a bunch of immortal witches and wizards, that Morgan and the rest of the asura emulators are basically artificial superheroes, or that not only is magic real, but it's possible for humans to use it too.

How do I know magic is real? It's pretty simple: I reasoned that since Morgan is an android that can do magic, it should be possible to make programmable machines capable of creating magical effects. My bestie Josefine helped me build one, and we ended up writing a shitload of code to put together a standard magic library.

You aren't supposed to be reading this, because I'm not supposed to write it. I'm writing it anyway, just as I distributed my magitech schematics and source code on the network, because the Sephiroth can go fuck themselves. Especially that asshole Kether. I'm surprised he hasn't turned himself blue and taken to walking about starkers on the surface of Mars.

If they think I'm going to let them bury the truth, let them bury *my* truth, then they're in for the mother of all rude awakenings. Even if I must pretend this is just a fantasy novel, even if you believe what really happened is just a story, it doesn't matter.

People need to know what really happened, even I must disguise the truth as the sort of fantasy novel I used to read as a little girl, the sort of story I used to wish I could be part of.

Well, I got my wish, and I got it good and hard. Though it's obvious that I survived, since I'm writing this after the fact, you don't know what happened. To be honest, I don't fully understand what happened either, or why.

Let's figure it out together. It'll be more fun that way, I promise. And we'll get to stick it to the Phoenix Society in the process -- which is *always* good for a laugh.
