Starbreaker Next, Chapter 1: "Take the Day" (Second Attempt)
############################################################

:date: 2017-05-05 18:38
:category: Starbreaker
:tags: claire ashecroft, preparation, space, spaceflight, raspberry jam delta-v, nerves, fear, jitters, outtakes, turisas, take the day
:summary: This is my second attempt at opening Chapter 1 of the next Starbreaker novel.
:slug: starbreaker-next-chapter-1-take-day-second-attempt
:summary_only: true


I took a late lunch break today, and ended up building upon `ideas in this post <{filename}/posts/2017/04.april/2017-04-25-starbreaker-next-an-idea-for-an-opening-scene.rst>`_. Unlike the `last scene <{filename}/posts/2017/05.may/2017-05-04-starbreaker-next-chapter-1-first-attempt.rst>`_, this one's about 1700 words long, and the chapter title comes from a power metal song called `"Take the Day!" <https://open.spotify.com/track/70I9GGgU962T4Wox6KgDcK>`_ by Turisas, from their 2012 album *Stand Up and Fight!*.

The song's about a soldier in the Varangian Guard, an elite unit of Viking warriors in service to the Byzantine Empire, and his anticipation of the battle to come.

Chapter One: "Take the Day"
===========================

Morgan and the rest of the crew used to tell me that the time spent waiting before the start of a mission is the worst part. Once they were armed, armored, and briefed on the objective and the rules of engagement, they had little to do but hurry up and wait. It was easy to scoff at them; I was usually well away from the action, safe in my London flat curled up on my couch behind a huge screen that afforded me no end of antidotes to the mixture of boredom and creeping fear that my friends described as an Adversary's lot.

It was different now. It was worse. This time, I wasn't simply cheering the others on, or gathering intelligence, or providing electronic warfare and countermeasures support. This time I would step up to the firing line.

I was going to war aboard something that was either a dragon that had eaten a starship, or a starship designed to resemble a dragon, and I was scared shitless in the most literal possible sense. I had spent the last two hours in one of the ship's null-gravity toilets, and had only recently emerged.

«Want a hand suiting up? You're the last one to do so, and you're going to be fucked if something punches a hole in Koschei.» Josefine made the offer without speaking or putting down her Unix programming manual, a book so battered from constant rereading that I could no longer tell whether it was duct tape holding it together or my bestie's willpower. The book was her security blanket; reading it allowed her to pretend that the world made sense.

She was already suited up, helmet and all, and had her devil-killer rifle slung across her back. Though I would never understand what she saw in that sleaze Eddie Cohen, I was glad she would be taking a sniper's position along with him. It meant she'd be a little safer.

Rather than answer her question, I gently propelled myself across the cabin toward my own gear. Because this was my first flight to Luna, I still wasn't used to getting around in null-gravity and it took longer than I would have liked.

Like Josefine's, my armor was a skintight black suit of artificial muscle under ceramic plate. I should have stuffed myself into it already, but its second-skin fit wasn't the only reason I found it unpleasant to wear. The fit was harder on Josefine and Naomi, who are more modest than I am.

The worst part was the catheters. I don't want to know what Nakajima was thinking when she decided that these suits should grow tubes that penetrated me so that it instead of going inside the suit, the suit would absorb bodily waste extract useful material for reuse. Let's just say that kink had an unexpected upside.

Naturally, it meant being naked underneath. Not that I minded, since it was funny to imagine the guys squirming as their suits wormed their way up their cocks and arseholes. In all honesty it was rather distracting, but not without peacetime applications in the fetish scene.

I wonder if Nakajima had thought about *that*. I'll have to get Chihiro drunk and ask her once this is all over--assuming I live through this.

«Your hands are shaking. Let me help.» This time Josse put her book aside, and thrust herself toward me with a gentle kick. If not for the suit covering her mouth and nose it would have been tempting to steal a kiss as I caught her, but we had been friends too long for me to do more than entertain the thought.

Besides, Polaris and I had promised each other that if one of us played with somebody else, the other would know ahead of time and have the opportunity to join in. It was only fair.

«Aren't you scared, too?» I couldn't believe Josse's hands were so steady as she slipped behind me and began working the fastenings from the bottom up, as if she were helping me into a ball gown--which will probably be good practice for her if she decides to marry Eddie and sticks me with the maid of honor job.

«Scared? I'm bloody terrified. Just hold still and let me finish, all right.» She gave her head an impatient shake. «I'm glad Nakajima didn't consult you on the design, by the way. Otherwise we'd probably all have different colored suits, and I've gotten stuck with the pink one.»

«There's just one problem with that.» I counted the members of our merry, motley crew. «Between you, me, Morgan, Naomi, Eddie, Sid, Sarah, Polaris, Tetsuo, and Desdinova there's at least ten of us. A proper super sentai team only has five members, with an occasional sixth. You know that as well as I do.»

«I kinda wish Polaris and Tetsuo were with us. Don't you?»

Josse cursed as I shrugged and pulled a set of fastenings from her fingers. «It was my idea for Polaris to stay on Earth. Tetsuo and the other asura emulators also have roles to play.»

What I didn't tell Josse was that I wished it wasn't just Tetsuo and the others who could simply jack themselves in and transfer copies of their minds to meat puppets grown to order on Mars, Venus Orbital, the Belt habitats, and the Jovian moons. Sabaoth could materialize angels anywhere in the solar system, and do a hell of a lot of damage before we took him out, so we had to prepare for the possibility he might attempt diversionary attacks.

Sure, Morgan could have used a meat puppet himself, but he's still fucked up from dying and being resurrected--and *somebody* had to carry the bloody Starbreaker. I wasn't about to do it, assuming it would even let me.

«Take a deep breath. I'm almost done.» I complied, and Josse made quick work of the last few bindings. Once she finished, I put on my helmet. It was completely seamless; instead of a visor it must have had an array of cameras. If I wanted to, I could even see behind me having to turn my head and look over my shoulder.

Once I had it on and sealed the gasket, the suit booted up and interfaced with my implant--and the rest of me. As it injected a chemical cocktail into my bloodstream, I understood why her hands didn't so much as tremble.

Artificial calm coated my nerves, and while I was still scared out of my demon-ridden mind, I could look at the distant Earth through the viewscreen without freaking out over how my last sight of home might be that pale blue dot. «Thanks, Josse.»

"We're currently 100,000 kilometers from Earth." Thanks to the drugs, I didn't jump out of my skin at the sudden announcement from the ship, or the dragon, or whatever the hell it was. I was still wrapping my head around the notion that my friends and I had climbed up a dragon's arsehole and found a bloody starship inside. "We will reach Lunar orbit and begin our approach to Armstrong City in 17 hours and 59 minutes."

«Not that I'm ungrateful, Koschei, but can't we go faster?»

"Do you like raspberry jam? I could make you some."

It was Josse who answered. «I *love* raspberry jam. Can you really make some? Do you have any scones, too?»

"No scones, Dr. Malmgren, but the raspberry jam is easy. I need only go faster as Claire wishes, and make a sudden deceleration once I reach Lunar orbit."

«That won't be necessary, Koschei.»

"You sure, Claire? You seem impatient."

Patience isn't one of my virtues, but it's coming to me at lot more easily now. «Don't mind me. I'm just nervous, and Sabaoth could cause a lot of trouble in eighteen hours.»

"The others seem nervous as well. I would suggest that you comfort each other."

«How the hell are we supposed to do that?» A group hug seemed unlikely, and an orgy was right out.

"You're an intelligent young woman. You should have no trouble figuring something out."

«Thanks for the vote of confidence.» Though I was being sarcastic, tone wasn't something secure relay chat could easily convey. Still, Koschei was pretty smart for a biomechanical space dragon, so I wasted no time worrying about it as I grabbed my weapons. «Come on. Let's find the others.»

I found Morgan and Naomi first. They wore the same armor I did, but wore swords in addition to the rifles slung across their backs. To my surprise, they had all but shaved their heads. It was one thing to see Morgan with close-cropped black hair; men can look good with long hair or short hair. Seeing Naomi's drift of snow-blonde locks reduced to a dusting was a bigger shock; without the hair to frame her face, the bone structure seemed harsher, and her scarlet eyes more alien.

Regardless of their new looks, anybody would mistake Morgan and Naomi for the real heroes: the war machine who chose to be a man and the devil's daughter. They looked bigger than life and twice as sexy with their armor fitting every contour of their bodies as closely as mine fit me.

Josse must have felt the same way. «Do they even need us?»

«Every superhero has sidekicks.» Sure, Morgan was closer to fitting the definition than Naomi, but was hard to think of either of them as larger-than-life heroes. I might not be their valet, but I've listened to them shagging in an adjoining hotel room with my hands in my knickers.

Yes, they know. I'm no more discreet than they are. Even now they seemed ready to try stealing a kiss, but settled for bumping helmets like the cats their eyes and ears made them slightly resemble. «Oi, Nims. I love the new look. It's very Ellen Ripley.»

Her eyes crinkled as if she would laugh if she could. «Too bad we don't have it as easy as she did. How are you and Josefine feeling?»

Josse and I answered in unison: «Scared.»

Naomi nodded. «I haven't been this scared since Clarion.» She gave Morgan's shoulder a playful punch. «I'd bet that even Morgan's scared, but won't admit it. Any odds you like.»

Morgan looped an arm around her shoulder. «Don't tempt Claire like that when you're going to lose, Nims. I'm the only person on this ship who has no right or reason to be scared, but knowing that doesn't help at all.»

Character Notes
===============

I'm sure you're sick of reading, but you might find the following character notes useful. My books tend to have large casts, and some people have trouble keeping track of who's who.

Claire Ashecroft
----------------

As I said I would, I'm writing from Claire Ashecroft's viewpoint. She's a gray hat hacker from London who isn't used to taking as an active role in events as she has lately, but despite her fears and reservations I hope to show her strength as she rises to the occasion and proves herself a worthy companion to friends she would regard as heroes if she didn't know them so well.

I first introduced her in *Without Bloodshed*, where she played an integral role in Morgan Stormrider's mission to Boston after helping Naomi Bradleigh fight her way free of Alan Thistlewood at MEPOL in London. However, a younger Claire also made appearances in *Silent Clarion*.

In both books, Claire is a fun-loving, playful hipster with a bawdy sense of humor and a possibly excessive fondness for sci-fi, fantasy, comics, and video games. She's also proof that one should be wary of programmers who carry screwdrivers.

.. {% include image.html src="/assets/images/original/starbreaker-cast-claireashecroft.jpg" alt="Claire Ashecroft" caption="Claire Ashecroft at home. Artwork by Harvey Bunda." %}

Josefine Malmgren
-----------------

Claire Ashecroft's best friend is Josefine Malmgren, whom Claire calls Josse. Readers of *Without Bloodshed* may recall that I mentioned Dr. Malmgren in Chapter Four. If I were writing the Starbreaker saga in order, you would have met Josefine as Claire rescues her from the prototype asura emulator Polaris with Morgan's help.

They grew up together, and were roommates while studying at university together. While Claire has always wanted to have Josefine as a lover, she is too respectful of her friend's boundaries to do more than occasionally flirt or fantasize.

While Josefine took a more respectable career path by accepting Isaac Magnin's patronage and working for the AsgarTech Corporation on Project Aesir, the last phase of the Asura Emulator Project, her skills are complementary to Claire's. This will prove critical as the story progresses.

I don't currently have commissioned art for Josefine Malmgren, but she's a petite, bespectacled blonde that I associate with Apollo program software engineer Margaret Hamilton.

.. {% include image.html src="/assets/images/original/margaret-hamilton-mit-nasa-1969.jpg" alt="Margaret Hamilton, MIT/NASA, 1969" caption="Margaret Hamilton with the Apollo program source code, 1969 (public domain)" %}

Koschei
-------

Koschei, the dragon starship in which Claire and the others are currently flying to Armstrong City on the Moon, was the "guardian" I mentioned in a post from a couple of weeks ago.

I have more material on him concerning his background and origins in Slavic myth and fantastic literature, but that's material for another post.

Punctuation Notes
=================

You might have noticed the angle brackets that I've been using instead of quotation marks. This isn't a mistake, but a stylistic choice.

I use double angle brackets called "guillemets" when quoting dialogue conveyed over the network using protocols based on SMS, various IM protocols, and IRC. For example, this is text dialogue:

    «Whoa.» If my mind wasn't blown by how weird my life had gotten before, it was now. «This whole deal is so MegaTen it isn't even funny.»

And this is the usual spoken dialogue.

    Taking my eyes from Morgan's swaying hips, I elbowed Naomi in the ribs. "He's got a great arse. Want to borrow my strap-on?"

You're welcome to do the same in your own work -- not that I could stop you. :)
