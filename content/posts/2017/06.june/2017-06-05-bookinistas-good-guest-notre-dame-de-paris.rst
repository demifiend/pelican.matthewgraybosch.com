Travel: Bookinistas, Being a Good Guest, and Notre Dame de Paris
################################################################

:date: 2017-06-05 20:01
:slug: bookinistas-good-guest-notre-dame-de-paris
:tags: alexandre dumas, bookinistas, books, memories, notre dame, paris, france, philip k. dick, seine, street vendors, vacation, voltaire
:summary: Catherine left me alone outside the cathedral of Notre Dame de Paris so she could take photos, so I looked around and did some thinking. This is the result.
:summary_only: true


After riding the Metro from Pyrenees to St. Michel via Chatelet Station, Catherine and I headed toward Notre Dame de Paris on foot along the Seine after a stop a the fountain of St. Michel. This fountain depicts the archangel Michael grinding Satan beneath his heel while holding a wave-bladed sword that may have been intended to resemble the flaming sword that barred Adam and Eve from returning to Eden. Two great bronze dragons flank Michael as secondary fountains.

.. image:: {filename}/images/paris_st-michel_fountain.jpg
	:alt: a photo of the fountain at St. Michael's plaza in Paris, France
	:align: left

Incidentally, there's something I've always wondered about St. Michael, since I was never Catholic enough to know. He's supposed to be both an archangel and a saint, and saints usually end up that way because they were martyrs for Christianity, but in standard Christian mythology there's no way to *kill* an angel. I'm sure there's some abstruse theological explanation for why an archangel is venerated as a saint somewhere...

Bookinistas on the River Seine
==============================

Walking along the river Seine one finds the bookinistas, street vendors selling books, artwork, and souvenirs such as coaster sets, fridge magnets, and tin plates emblazoned with Parisian art from the last two centuries. You'll also find modern originals mainly depicting scenes from Paris on unframed canvas; presumably the vendor would roll it up and tie a bit of string around it for the customer, but Catherine and I spent the last of the cash we had on hand on other things and these new artworks cost at least 30€ each.

Some of this art, being Parisian, is just a bit risqué. Not that there's anything wrong that. Indeed, it's rather fun to see fridge magnets features photos of two nude women facing away from the camera with their hands on each other's bottoms with the slogan "J'aime la Paris" (I love Paris).

Most of the books are in French, but the elderly woman managing the first few stalls Catherine and I examined was kind enough to point me toward a stall containing books in English. To her surprise, however, I select two books in French as well: a paperback of Philip K. Dick' *Ubik* and a handsome little hardcover of Voltaire's *L'Affaire Calas*. While I've read most of Dick's work (an obligatory endeavor for those who would write their own science fiction) the only Voltaire I can honestly claim to have read was *Candide*. In English, I picked up copies of A. E. Waite's *The Pictorial Key to the Tarot* and William Camden's *History of the Most Renowned and Victorious Princess Elizabeth Late Queen of England*. The latter is a scholarly text printed by the University of Chicago Press in 1970 as part of series of "Classics of British Historical Literature", and is in fact a set of selections from Camden's *Annals of Queen Elizabeth*, a primary source on the Virgin Queen's reign written by one of her ministers.

.. image:: {filename}/images/paris_eiffel-tower-across-the-seine.jpg
	:alt: a photo of the Eiffel Tower across the River Seine in Paris, France
	:align: left

Buying Books I Can't Read Yet
=============================

To satisfy the vendor's curiosity as to why I chose both French and English books, I explained that while she had correctly identified Catherine and me as tourists from abroad I wanted to pick up a couple of books in French to add to my library and read to improve my French. Fortunately, I had just enough French to tell her so, and to make our purchases.

I'm not even close to proficiency in French, let alone fluency, but I think it's important to at least try to learn to make oneself understood in the language of the country I'm visiting. I don't like to think of myself as a tourist while visiting France, but a guest. If I am to be a guest, I would prefer to be a good guest so that a second visit would be welcome rather than dreaded. That means not only obeying the local laws, but observing local customs and making an effort to speak and read the language.

Sitting Outside Notre Dame de Paris
===================================

The bells of Notre Dame de Paris rang for a minute at 11:00am, and for a few seconds at 11:15, 11:30, and 11:45. They then began to ring again at 11:50, and continued for four minutes. They rang again to greet the noonday sun and continued for five minutes. The famous cathedral might have crumbled if not for the revived interest aroused by Victor Hugo's epic novel, but that is unlikely to happen again if the lines to pass the cathedral doors are any indication.

Catherine and I had not chosen to brave the queue. Instead, she wandered around what I think is called the Place du Notre Dame and photographing the exterior. I sat near at great bronze monument to Charlemagne.

.. image:: {filename}/images/paris_notre-dame_charlemagne.jpg
	:alt: a photo of the monument to Charlemagne outside Notre Dame in Paris, France
	:align: left

The place is broken up by raised garden beds fenced off with wrought iron painted green. The stone containing these beds is solid, and wide enough to serve as a bench for those content to sit outside and admire the detailed facades. Of course, one must do so beneath the gazes of saints stationed above Notre Dame's three sets of massive entrance doors.

One cannot tell merely by looking, but the saints' heads are reproductions. Much like King Louis XVI and his maligned queen Marie-Antoinette, the saints over Notre Dame lost their heads during the French Revolution of 1789 as zealous citizens of the nascent Republic sought to eradicate any vestige of the old regime in which the people suffered beneath the twin tyrannies of the king and the Church.

However, the original heads of the saints were merely removed rather than destroyed. I understand one might see the originals at the Museum of Architecture in Paris.

.. image:: {filename}/images/paris_notre-dame_saints.jpg
	:alt: a photo of some of the worn statues of saints at Notre Dame in Paris, France
	:align: left
