Your Dog Will Never Be This Cute
################################

:date: 2017-09-20 22:28
:tags: cat, gif, imgur
:summary: If Virgil or Smudge did this to me, I'd get nothing done either.
:slug: your-dog-will-never-be-this-cute
:summary_only: true


If Virgil or Smudge did this to me, I'd get nothing done either.

.. image:: {filename}/images/nNF3QcM.gif
    :height: 728px
    :width: 728px
    :alt: An animated GIF of a piebald cat showing affection toward its pet male human
    :align: center
    :target: https://imgur.com/nNF3QcM


You've got to admit it. That's a *good* cat. Even if he is just claiming 
that guy as his person. That's the sort of cat that justifies saying **"A cat is fine, too,"** when somebody suggests adopting a dog for companionship.

Image credit: `danielraysir on Imgur <https://imgur.com/nNF3QcM>`_
