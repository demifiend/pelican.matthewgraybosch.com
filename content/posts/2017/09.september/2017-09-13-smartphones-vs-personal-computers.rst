Smartphones vs Personal Computers
#################################

:date: 2017-09-13 10:29
:tags: hardware, smartphone, personal computer, raspberry pi
:slug: smartphones-vs-personal-computers
:summary: A random thought on smartphones vs personal computers that I might expand later.
:summary_only: true


I think that expanding the capabilities of mobile phones until they
became handheld computers was a mistake. Instead, I think we should
have worked on miniaturizing general purpose PCs until we had handheld
computers with touch screens that ran VOIP apps on our choice of
operating system (Windows, Linux, BSD, or OSX) and could be plugged
into a docking station via USB for desktop work.

There's no technological reason we can't. And there's no shortage
of `blog posts explaining how to use a smartphone as a desktop
computer <http://www.makeuseof.com/tag/ditch-your-desktop-turn-your-smar
tphone-into-a-desktop-replacement/>`_. However, they all suffer from some
crippling limitations:

- You must use a wireless mouse and keyboard.
- You're stuck using Android or iOS, both of which are locked down.
- Unless you can root/jailbreak your device, you're limited to software available from your device's app store.
- There isn't a unified standard for connectivity to external devices such as monitors, keyboards, mice/trackballs, and external storage.
- You cannot access external storage.

The `Raspberry Pi <https://www.raspberrypi.org>`_ solves the problems
I listed above, but has its own drawbacks. While touchscreens for the
Pi are available, they come in sizes that make the Pi unsuitable for
use as a smartphone. Nor does the Pi connect to mobile phone networks
by default; its wireless connectivity is based on the 802.11 wifi
standards.

(`content liberated from Google+ <https://plus.google.com/+MatthewGraybosch/posts/hb7PjwMRSeX>`_)
