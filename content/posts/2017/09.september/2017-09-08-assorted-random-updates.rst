Assorted Random Updates
#######################

:date: 2017-09-08 07:34
:tags: life, kitten, adoption, music, netflix, mortgage
:slug: assorted-random-updates
:summary: A few slices of my life as of today.
:summary_only: true


Here's a little update on what's going on in my life. 
I might do make this a weekly thing. I might not.

And Then There Were Three
=========================

My wife and I got stuck with a kitten. Some asshole just dumped him in my parents' front yard, and they couldn't keep him. He's a little gray tabby fur baby with white paws, a white belly, neck and chin, and a white-tipped tail. He's completely fearless, and you can hear him purring from across the house if it's quiet, so we call him Purrseus (or sometimes just Percy).

Unfortunately, our older cats -- Smudge (5) and Virgil (8) -- aren't especially pleased to have a new bitty buddy chasing them around and trying to play with them, though Smudge is closer to accepting Percy than Virgil. Then again, Virgil's just a big pussy; you'd think a 20 pound barn cat wouldn't be afraid of a kitten who's barely six weeks old, but whatever.

If they don't chill the hell out and learn to get along with the little guy by the end of the month, we'll take Purrseus to a local no-kill shelter with a check for a couple hundred bucks to help pay for him to get neutered and vaccinated.

Free Agency
===========

I've also gotten confirmation that all rights to my first novel, *Without Bloodshed* and the mainline Starbreaker saga have reverted back to me. The novel's still available from `Curiosity Quills Press <https://curiosityquills.com>`_ on Amazon, as is *Silent Clarion*, but I've got to decide what I want to do with my first novel. I'm thinking of making it available online as a web serial and revising it.

It's too bad I've got a face made for radio and a voice made for print, because otherwise it might be worth my while to record a series of video readings.

I'm also thinking of writing subsequent Starbreaker novels as web serials, though I won't serialize *Silent Clarion* online because that novel's still under contract.

Light Reading
=============

I went on a bit of a binge this week, reading the first five **Monster Hunter** novels by Larry Correia at a pace of about one a day. I'm a little ambivalent about giving the guy my money because of his involvement with Sad Puppies, but at least he's not Vox Day or Orson Scott Card.

Now I'm back to R. Scott Bakker's *The Warrior Prophet* and E. R. Eddison's *The Worm Ourobouros*. I really should just focus on one of them and power through.

Devil-Killer Music
==================

I bought copies of *Echoes of the Aftermath* by `The Murder of My Sweet <https://www.themurderofmysweet.com>`_ and *Apex* by `Unleash the Archers <http://www.unleashthearchers.com>`_. Of the two, Echoes has gotten more play; I've been in the mood for more melodic music this week rather than power metal, but Apex is an excellent album in its own right.

Here are videos of "Personal Hell" by TMoMS and "The General of the Dark Army" by UtA.

Personal Hell
-------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/PCLiCifcuI8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The General of the Dark Army
----------------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WVi0d7oqDvs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Netflix and Chill
=================

If you're looking for a solid sci-fi B movie, you could do far worse than *What Happened to Monday* starring Noomi Rapace. Ms. Rapace plays seven fraternal twin sisters who must share a common identity in a dystopian future Europe that enforces a One Child policy with greater zeal than China ever did. She'd make a great Elisabeth Bathory in a Starbreaker adaptation.

Also look for Willem Dafoe in a supporting role and Glenn Close as a `Well-Intentioned Extremist <http://tvtropes.org/pmwiki/pmwiki.php/Main/WellIntentionedExtremist>`_.

Adulting
========

I also finally got automatic mortgage payments set up; I wasn't able to do it at first because the mortgage broker wasn't equipped to take payments by any means other than a check in the mail, but they sold the mortgage to a corporation that realizes that it's the 21st century. So now, on the seventh of every month, I make the required payment of a little over a grand plus an extra hundred to pay down the principal faster.
