Mid-September Update (Small Dark Lines)
#######################################

:date: 2017-09-15 19:15
:tags: update, starbreaker, threshold, single, video, small dark Lines
:slug: mid-september-update-small-dark-Lines
:summary: Here's what's up with me this month: I'm working on Starbreaker and loving the new Threshold single.
:summary_only: true


Here's what's up with me this month: I'm working on Starbreaker and 
loving the new Threshold single, `"Small Dark Lines" <https://www.youtube.com/watch?v=e9NCuOe4zJk>`_. 
It's an infectious groove, like "Ashes" from their 2012 album *March of Progress*, 
so I'm definitely going to have to buy *Legends of the Shires* when it drops.

It's such a perfect song for my Starbreaker setting.

.. raw:: html

    <blockquote>
        <p>There are small dark lines on my heart tonight<br />
        From all those times that I crossed the line<br />
        Hallmark scars that were left behind<br />
        Memories never fade<br />
        I know I should never be forgiven<br />
        Given all the little lies I'm living<br />
        So I suffer those lines as a warning sign<br />
        That never goes away</p>
        <p><cite>&mdash; Threshold, <em>Legends of the Shires</em> (Nuclear Blast, 2017)</cite></p>
    </blockquote>


Here's the video. The imagery's as powerful as the lyrics.

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/e9NCuOe4zJk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

My wife is going to Washington, DC with her girlfriends this weekend, so 
I've got the house to myself. I think I'll take the time to rip some albums and get a dedicated Starbreaker website going at starbreakersaga.com. 

Once that's up, I can begin serializing *Without Bloodshed Remastered*, 
a second edition that I started yesterday in preparation for writing the sequel. 

Here's a taste...

    The house reflected its owner, mostly white with crimson accents. 
    The door opened, revealing its mistress. She held a sword in one hand, 
    and clasped a black woolen overcoat closed over her nightgown with 
    the other. Though sleep still tinged her voice, her displeasure was 
    evident as she glared at him with eyes that betrayed her demonic 
    ancestry to those who recognized the scarlet gaze of a demifiend. 
    "You. What have you done to Christabel?"

I still haven't decided on a title for the next *Starbreaker* novel, but I'm thinking one of the following:

- Blackened Phoenix
- Mastema Whispered
- Witnesses for the Persecution
- Resurgent Demiurge
- Shattered Guardian

If you have a preference, I'd love to hear it `by email <mailto:public@matthewgraybosch.com>`_. Thanks!
