Yet Another Opening For Shattered Guardian
##########################################

:date: 2017-12-09 15:20
:tags: shattered guardian, opening scene, outtake, morgan stormrider, badass, action, sci-fi, fantasy, science fantasy
:category: Starbreaker
:slug: yet-another-opening-shattered-guardian
:summary: This is yet another beginning for Shattered Guardian that I originally wrote on Reddit a couple weeks ago.
:summary_only: true


Morgan studied the security guards arrayed before him.  Thirty-six men
tightened their grips on their rifles, but none of them fired.  "What the
hell is wrong with you people?  Is this your first day on the job or
something?"

One of the guards raised his head from his rifle's chin rest.  "Maybe it's
*your* first day, Adversary, but if you don't turn your ass around it's
gonna be your last."

Morgan shook his head.  "I'd be doing Victoria Murdoch a favor by putting
you all to the sword.  You really are a bunch of demon-ridden idiots."

Another guard spoke up.  "Permission to waste this idiot?"

"Are you nuts?" said the first guard.  "He's an Adversary.  We can't kill
him.  He's the guy the Phoenix Society sends when other Adversaries get
killed.  Who will they send if we kill *him*?"

"Believe it or not," said Morgan, "my employers aren't worried about you
killing me.  Therein lies your problem.  You should have seen me coming,
thanks to the security systems Victoria Murdoch placed throughout the
complex instead of paying her workers time and a half for overtime."

"If we started shooting at you while you were out there," said the guards'
leader.  "You would have dodged every shot."

*Enough of this.  Time for a demonstration.* Holding his place in time,
Morgan slipped past the guards.  Standing behind the leader, he pressed the
tip of his sword into the nape of his neck before letting the moment go. 
"Move, and I'll run you through."

"H-how the fuck did you --"

"Doesn't matter," said Morgan.  "All that matters if that if I wanted to
dodge bullets from rifles firing supersonic rounds I could do so whether I'm
two hundred yards away or close enough to stab you.  All I need is to see a
finger tighten on a trigger, which is why you're better off shooting
*before* get close."

A shot grazed Morgan's forehead, and he turned to find a young woman
struggling against shaking hands to line up another shot.  "Congratulations,
kid.  Since you had the guts to take a shot and managed to hit me despite
being so scared I can smell it, you get to leave this lobby unharmed.  Just
drop the weapon and run."

"But&mdash;"

Morgan drove his sword through the leader's shoulder.  "Last chance, kid. 
And while you're running, I suggest you ask yourself why I came here alone
and brought a sword to a gunfight."
