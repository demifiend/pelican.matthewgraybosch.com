A Question for Starbreaker Readers and Fans
###########################################

:date: 2017-04-19 13:32 
:category: Starbreaker
:tags: future plans, direction, change, claire ashecroft, protagonist
:slug: question-starbreaker-readers-fans
:summary: If you've read Without Bloodshed or Silent Clarion, or even if you haven't, I have a question for you about the future of my Starbreaker saga.
:summary_only: true


Here's a question for anybody who has been reading my Starbreaker novels
(*Without Bloodshed* and *Silent Clarion*), or anybody who wants to
weigh in.

Background
==========

I've been thinking of reworking/reimagining my Starbreaker saga. While
*Without Bloodshed* hasn't been a complete bomb sales-wise and has
gotten respectable reviews, it's also an extremely dense book that
demands more of most readers than they're willing to give.

I've come to suspect that writing subsequent Starbreaker novels in the
same style as *Without Bloodshed* won't work because doing so would
make Starbreaker Morgan's story and reduce the rest of what could be a
diverse and interesting ensemble cast to supporting roles.

I have this character named Claire Ashecroft. You might remember her;
she's the brash, kinky, gray hat hacker from London who helped Morgan
and the team with computer stuff in *Without Bloodshed* and mistook
Naomi for a character from one of her favorite video games in *Silent
Clarion*.

She's smart, sassy, and she's probably my wife's favorite character out
of the entire cast.

She's also the closest thing my cast has to an "Only Sane Person". Her
comparison of her friends' situation to anime and video games (such as
saying she has never trusted Isaac Magnin because he's "a white-haired
bishounen") is her way of acknowledging the ridiculous nature of the
reality they experience.

If I am going to re-imagine Starbreaker as a series of shorter, more
tightly focused adventures featuring various characters as they
gradually become aware of the true nature of the world and come together
to face a common threat, it might still be helpful to readers if I were
to tell my characters's stories with a single, consistent voice.

The Big Question
================

Those of you familiar with Sherlock Holmes will recall that all of his
adventures were recounted by his friend, partner in detection, and
biographer Dr. John H. Watson, who wrote in the first person.

Therefore, here is my question to you: **Would Claire make a good Watson
in a rebooted Starbreaker saga?**

Why Claire?
===========

I think Claire would work as Watson because while she wasn't directly
involved in everything, she knows all of the key players.

Furthermore, she has a reason to tell the story. As a hacker, she
believes that information should be available to everybody.

However, after the Great Defiance and the investigation that followed,
the AIs now running the Phoenix Society (the Sephiroth) ordered that all
information about Claire and the others, all video evidence, and all
testimony and depositions at the trial of Isaac Magnin and the rest of
the Phoenix Society's executive council for crimes against humanity be
sealed. They decided that the truth is so weird that most people would
willfully refuse to believe it because the implications would shatter
their understanding of the world.

However, Claire refuses to accept this and insists on telling the story
of how she helped fought a demon who pretended to be God alongside a
killer android with a soul, a swashbuckling soprano, a pro wrestler
turned watchdog agent, a sleazy old sniper with a heart of gold, a
giant Maine Coon Cat who understood English and could play poker, a
cabal of witches and wizards who became demons to fight them, and other
outlandish characters as if it were fiction.

Basically, I want to do with the Starbreaker saga what Michael Moorcock
did with Elric when he wrote *Stormbringer*. I want to start with the
big crisis, the highest stakes, and kick readers' asses so hard they
land on the other side of the fucking planet wondering what the hell
just happened to them.

This will be the new Starbreaker, Claire's fictionalized history of the
Great Defiance and what really happened.

After that, Claire can dig back into the past and write further accounts
of the events that led up to Starbreaker, such as the Liebenthal Crisis
I originally wrote about in *Without Bloodshed* or the Clarion murders
Naomi Bradleigh solved in *Silent Clarion*.

What About the Books You Already Wrote?
=======================================

Don't worry. *Without Bloodshed* and *Silent Clarion* aren't going to go
away immediately. You will still be able to buy copies on Amazon.

While the Kindle editions of these novels may eventually go away and
Curiosity Quills Press might cease print publication of *Without
Bloodshed* eventually, I am not going to disavow these works.

While I can't guarantee that removing the Kindle editions from Amazon
*won't* remove them from your Kindle apps and devices, I will do my best
to ensure that if you already bought it, you can keep it. Likewise,
those of you with paperback editions of *Without Bloodshed* may end up
with a collector's item on your shelves &mdash; especially if I signed
it.

Punching the Reset Button
=========================

If I write Starbreaker and subsequent stories leading to it from
Claire's point of view, I will account for the existence of these novels
in-universe. If nothing else, Claire might mention them as "unauthorized
accounts by some hack who wasn't involved and had no bloody idea what
the 'ell was going on".

It may seem premature to reboot Starbreaker when I spent so many years
talking about sequels to *Without Bloodshed* and even have titles for
them, but I've got to be honest with myself and with all of you readers
and fans.

I Had No Idea What I Was Doing, and I Screwed Up
================================================

While I did the best I could at the time, you deserve better. The
Starbreaker saga in its current form just isn't working, and that's my
fault. I thought I could expand a 289,000 word draft of a first novel
into four-volume epic, and I was wrong.

It's time I swallowed my pride and admitted it.

I'm sorry. I made a lot of mistakes. I want to learn from them so that
my next novel is my best yet, and so that the Starbreaker saga is
everything I as the author and you as the readers hope it can be.

I'll be copying this post or sharing links to it on various social media
platforms because I want to hear from *you*. If you have any advice or
just want to wish me luck, please comment and share.

Thank you for all of your support over the years. If you'd like to
provide feedback directly to me instead of replying on social media
channels, please `email me`_.

.. _email me: mailto:public@matthewgraybosch.com?subject=Starbreaker series feedback
