Claire as FemShep from Mass Effect 2: An Idea for Starbreaker
#############################################################

:date: 2017-04-20 12:05
:category: Starbreaker
:tags: claire ashecroft, femshep, mass effect 2, ideas, plot, character development, idiot ball, AI, uranus, bad jokes, shit my characters say
:summary: Or maybe I should just give up and call the next Starbreaker novel "Claire Ashecroft and the Workshop of the Telescopes"...
:slug: claire-femshep-mass-effect-2-idea-starbreaker
:summary_only: true


Here's an idea for the next Starbreaker book where Claire tells the story of how she, Morgan, Naomi and the rest of the crew saved the planet from annihilation at the hands of a demon bent on genocide.

Suppose everybody's pissed at Morgan for various reasons and Claire gets stuck with the job of helping them reconcile? Basically, I'm imagining Claire as FemShep (Commander Shephard as voiced by Jennifer Hale) in *Mass Effect 2*. 

Of course, Claire wouldn't be `Renegade FemShep <https://www.youtube.com/watch?v=F3YCDGSs9rU>`_ *or* `Paragon FemShep <https://www.youtube.com/watch?v=aen2kgDr1ZI>`_ (videos probably contain ME2 spoilers). Renegade FemShep is a bloodthirsty asshole, and Paragon FemShep is more of an officer and a lady than Claire will ever be no matter how much character development she gets. 

Instead, Claire will still be her brash self, but she'll have to grow as a person and develop some leadership skills so that she can get all of her old friends to settle their differences, forgive themselves and each other, and fight together one last time.

First she's got to convince Morgan that unleashing the Starbreaker is the only way to deal with the threat posed by the ensof Sabaoth. That's going to be a tall order, because Morgan now knows exactly how the Starbreaker works.

Now, if I were another author I might write Morgan as knowing how the Starbreaker works and what it will cost him to use its true power, but also show him refusing to tell Claire because reasons. That would force her to go talk to Thagirion or Imaginos or one of the other immortal witches and wizards who became demons to fight demons and get the info from *them*. Of course, this would mean sticking Morgan with the `Idiot Ball <http://tvtropes.org/pmwiki/pmwiki.php/Main/IdiotBall>`_, and it would result in the sort of `plot induced stupidity <http://tvtropes.org/pmwiki/pmwiki.php/Main/ForgotAboutHisPowers?from=Main.PlotInducedStupidity>`_ that ensues when `people don't talk to each other <http://tvtropes.org/pmwiki/pmwiki.php/Main/PoorCommunicationKills>`_.

I've got a *better* idea. At least, *I* think it's a better idea. Morgan's going to tell Claire. Claire, in turn, is going to go find Imaginos, deck him in front of the rest of the Phoenix Society's executive council, and then demand confirmation of everything Morgan told her while verbally ripping Imaginos and the rest of the XC new assholes.

Including Thagirion. I can see it now...

Chapter ??: We're Off to Deck the Wizard
========================================

TAMARA GELLION
  Young woman, you will remember your place when you speak to me. Do you know who I am, and what I might do to you should you offend me?

CLAIRE ASHECROFT
  First off, the last idiot to tell me to remember my place ended up chewing a hole in one of my pillows while I showed him just how well I understood my place. Second, I know exactly who you are. You're Little Miss Bloodbath's older sister, and your candy-ass boyfriend orchestrated this clusterfuck just to impress *you*. Not that I blame him. You don't look a day over ten million. How do you do it, anyway?

TAMARA
  Can you not hear the ice cracking beneath your feet?

CLAIRE
  Take your best shot, asshole. 

ABRAM MELLECH
  Pride goeth before a fall, Miss Ashecroft.

CLAIRE
  Save the sermons for church, you old fraud. (turns back to Tamara) I'm willing to bet that if you kill me, Morgan won't care if unleashing the Starbreaker means he has to be killed before it can be sealed again. Morgan won't care how many civilizations hundreds of light-years away he'll condemn to freeze to death in the dark, either.

ISAAC MAGNIN
  Are you so sure, Claire? He doesn't love you.

CLAIRE
  You're so full of shit your eyes have turned brown. You know damn well Morgan thinks of me as a sister. He said as much. You've got it on Witness Protocol along with every other private moment of his life, you depraved bastard.

TAMARA
  Isaac, the obnoxious little trollop is right. Harming her *will* bring Morgan down on our heads, and he most likely will not care that killing you entails killing the Sun.

CLAIRE
  W-Wait. Did you just say that if Morgan whacks Isaac Magnin, the whole planet is fucked?

MALKUTH
  Think bigger, Claire. The entire *solar system* would be fucked, even if by some miracle these clowns managed to smash Jupiter, Saturn, and Neptune into Uranus&mdash;

CLAIRE
  Oi, Malkuth. There's no way you're fitting anything *that* big up there.

TAMARA
  (accusing) Whose bright idea was it to give AIs the capacity for humor?

ISAAC
  It's a spandrel. We needed to develop human-equivalent artificial intelligence for the Asura Emulator Project. Unfortunately, any software capable of emulating the human mind ends up with a human's sense of humor. Unfortunately, Malkuth developed a penchant for low comedy and toilet humor that I was never able to correct.

MALKUTH
  (raises his middle finger) Correct *this*, asshole. It's too bad you latched onto the sun, because we'd all be better off if Morgan unleashed the Starbreaker and killed you all.

CLAIRE
  Hey, Malkuth, is there a way we could smash the solar system's four gas giants together and fire up a replacement sun?

MALKUTH
  Any entity capable of doing so would be a greater threat to human civilization than the demons in this room or Sabaoth.

CLAIRE
  Bollocks. (glares at Tamara and the others) Look, I'm sorry I barged in here, decked Imaginos, and insulted the lot of you. I'm only human, and we don't always think straight when we're angry, especially if we're angry because somebody we care about is suffering. The truth is, I need your help. So does Morgan, even though he doesn't trust any of you enough to ask it.

TAMARA
  He has cause for distrust, as do you. And yet here you are, first challenging us despite having no hope of meeting any of us as an equal, and then humbling yourself before us. Why?

CLAIRE
  We all know CPMD is bullshit. 

DESDINOVA
  (coughs)

CLAIRE
  Don't worry, doc. I'll keep it to myself, but I know Morgan, Naomi, and everybody like them aren't actually human. You might be demons now, but you used to be like Morgan and Naomi.

ISAAC
  Do you think we watch over our people like guardian deities?

CLAIRE
  You must have had some reason to hang around and keep Sabaoth on ice. It doesn't actually matter why, but you're here because you still give a shit about what happens to the devas or asuras or whatever the hell you call yourselves.

ELISABETH BATHORY
  Maybe we're just here because it amuses us. Have you considered that possibility, Claire? (caressing Claire's cheek) Maybe we simply enjoy toying with you.

CLAIRE
  (shivering) How much fun do you think you'll have if we're all dead? Because that's what Sabaoth wants. Even if my species were to all bow down and lick his taint, we'd just be buying time. So help me help Morgan. Tell me how he can unleash the Starbreaker and kill Sabaoth without being possessed by it and needing to be killed and restored from a fucking backup again.

ISAAC
  Was that truly so traumatic an experience?

CLAIRE
  I don't know, but after Polaris killed Ahuramazda with the unbound Starbreaker, and Morgan killed Polaris, Josefine spat in Morgan's face and told him that she would never forgive him for consigning Polaris to the same hell she helped him escape.

DESDINOVA
  Dr. Malmgren did spend hundreds of hours in what Miss Ashecroft insists on calling the nightmare sequencer before Morgan was able to accept his new body. She never told me what the work entailed, but it must have taken a toll on her, Isaac. Resurrection in a new body for an asura emulator most likely as traumatic experience as having your avatar destroyed.

ISAAC
  I see. (turns to Claire) You may recall that Dr. Malmgren worked for me for several years. The behavior you described is uncharacteristic of her.

CLAIRE
  No shit, Sherlock. We've been besties since bloody kindergarten. She would never, *ever* spit in somebody's face like that. It's hard enough to get her to raise her voice. She's a *mouse*. I love her to bits but that's what she is.

TAMARA
  A mouse who found it in herself to defy a lion? Interesting.

CLAIRE
  I'm glad you think so, but I didn't tell you this for lulz. This is Morgan we're talking about here. He once let a suspect empty a Kalashnikov into his chest, smile, and ask the guy if he'd like a second to reload. He did it without wearing armor, just to show the suspect and his flunkies what they were fucking with. 

SAMUEL TERELL
  I doubt Naomi was amused.

CLAIRE
  No, she wasn't, but that's what Morgan was like. But it wasn't just showing off. He would endanger himself like that because it didn't cost him as much as it would cost the rest of us. That's why, when Sabaoth first showed up in the middle of Asgard and started killing people left and right, Morgan said, "Don't worry. I've got this. Help with the evacuation."

ISAAC
  It was foolhardy of him to face Sabaoth as he did. He should have simply used the bound Starbreaker to destroy Sabaoth's avatar.

CLAIRE
  So he could rematerialize somewhere else and start another massacre? If you hadn't been hellbent on making him manifest his talent by fucking with us until he got angry enough to break his limiters and kill your avatars, maybe you could have helped him seal the evil back in its can.

ISAAC
  Killing my avatar was what let the evil out of the can in the first place.

CLAIRE
  I know that. More importantly, Morgan knew that too. He realized his mistake, he stepped up, and he dealt with it. He was a man staring down a god until it backed down, and it killed him.

DESDINOVA
  We brought him back.

CLAIRE
  And I'm sure it cost you a shitload of money, doc, but it cost Morgan everything that mattered to him. He spent his whole life trying to be a man instead of a weapon, and you stole that from him. He was ready to die if that was what it took to set things right, because that's what soldiers and men do. You might have given him a new life, but you stole his identity. You stole his pride. You stole his love. You stole most of his friends.

DESDINOVA
  (shrugging) He's paying the price all immortals pay, and he would have had to pay it eventually.

CLAIRE
  (slaps Desdinova and smiles as his lip starts to bleed) You're not as immortal as you think, but you're immortal enough to know it doesn't work that way. Naomi, Eddie, Sid, Josse, me: we're the ones who die, and he's the one who goes on and carries our memories nestled within his heart next to his secret. That's how it's supposed to work. We weren't supposed to see him reduced to a charred skeleton that still somehow had the strength to reach out to us and try to speak.

TAMARA
  (gasps) Is that what happened to him?

CLAIRE
  Yeah, that's what happened. Naomi took his hand, and it crumbled to ash at her touch. She's got to live with that horror the rest of her life. We all do, and we all have to somehow square it with seeing Morgan alive again. We have to keep telling ourselves that the man we trusted isn't some kind of monster.

ISAAC
  Morgan is a weapon, not a man. Weapons do not need friends. Weapons do not need to be loved.

CLAIRE
  Weapons don't hide in the crapper and cry from loneliness and heartbreak, either, but what the hell. Maybe you don't owe him. You sure as shit owe *me*. You owe me, and Eddie, and Sid, and Naomi, and Sarah, and every other person who has ever been part of his life. You *all* do, because we showed him how to be human.

TAMARA
  She's right. You said it yourself; the original asuras lacked sufficient ego strength to resist domination by the ensof because their personalities were shallow, artificial constructs that did not grow organically over years of social interaction with others. You designed the 100 series asura emulators to grow from infancy through childhood and adolescence precisely so they would have the opportunity to become people.

ISAAC
  Fine. She's right. You're right. That doesn't change the plain fact that we can't help her. Thagirion, even you don't fully understand how the Starbreaker works. You don't know how to safely release it, or how to bind it again without killing the wielder. Ahuramazda might have known, but the knowledge most likely died with him. I'm sorry, Claire. I'm sorry for everything. I didn't want to admit the truth because it was easier to be reviled as demons than admit that for all our power we are still profoundly limited.

CLAIRE
  Yeah, you're sorry all right. You're as sorry a bunch of excuses for people as you are gods. For fuck's sake, why couldn't you have talked to any of us? Why couldn't you have done the work necessary to convince us and *asked* us to help? I can't speak for the others, but when I was a little girl I would have dropped *everything* for a chance to go on a quest, and help wizards and witches save the world.

ISAAC
  (chuckles) Well, then, perhaps I can grant your wish. We even have a wizard in gray to serve as your guide. Ahuramazda had a hideaway somewhere in the Himalayas. It is protected by a guardian whose nature I cannot describe because I have never seen it. No ensof other than Ahuramazda may approach, but I am given to understand that the guardian will grant entrance to any mortal descendant of his.

CLAIRE
  Then this guardian should let us in if Naomi comes along, right?

DESDINOVA
  Naomi doesn't know the way, or the language in which one must address the dragon.

CLAIRE
  Did you just say there's a fuckin' dragon in the fuckin' Himalayas?

DESDINOVA
  You wanted a quest straight out of a fantasy novel, didn't you? Come on. Let's go talk to the others. We will need them to prove to the guardian that Morgan's life is worth saving. It will no doubt insist that it would be better to kill him than to give him the knowledge of how to release and bind the Starbreaker at will.

CLAIRE
  And how will having the crew back together prove Morgan's worth saving? Are you saying we have to plead for his life even though you're the only one who speaks his language?

DESDINOVA
  I will do what I can to teach you, but if you value Morgan's life -- or your own -- you'll learn the guardian's tongue and you will learn it fast.

I Might As Well Just Make a Movie
=================================

Well, shit. I didn't mean to, but I think I've got enough dialogue, plot, and exposition for a whole chapter. And a substantial portion of the novel's plot. Funny how that works, isn't it?

I suppose I could call this book *Claire Ashecroft and the Workshop of the Telescopes* (which is what I might call Ahuramazda's little Himalayan fortress of solitude, because why not throw in *another* Blue Oyster Cult reference?). It's got that pulpy Indiana Jones feel, and Claire probably already has a whip. She just needs to steal a fedora off some dudebro who thinks the "friend zone" is real. :cat:

There's just one problem: writing the dialogue first like this and fleshing things out later works OK for my own use, but I can't keep sharing such material formatted as I did in this post.

Then again, I'm practically writing a screenplay anyway. Maybe I should just go whole hog and write an honest-to-goddess screenplay: *Starbreaker: the Movie*. 

Don't forget the merchandising. We'll put the name on everything, because that's where the *real* money gets made. We've got it all -- *Starbreaker: the T-Shirt*, *Starbreaker: the Coloring Book*, *Starbreaker: the Lunch Box*, *Starbreaker: the Breakfast Cereal*, and *Starbreaker: the Flamethrower* `(the kids love this one) <https://www.youtube.com/watch?v=oNZove4OTtI>`_.

And, last but not least, *Starbreaker: the Doll*, featuring Smudge!

.. image:: {filename}/images/smudge-on-kitchen-bookshelf-stereo-speaker.png
  :width: 422px
  :height: 454px
  :alt: a long-haired brown-and-cream tabby cat perched up high
  :align: center
  :target: {filename}/images/smudge-on-kitchen-bookshelf-stereo-speaker.jpg

Adorable, isn't he?

Oh, come on. You should have expected this. My setting has a band called `Keep Firing, Assholes <https://www.youtube.com/watch?v=PNcDI_uBGUo>`_, doesn't it?

Despite the gratuitous riffing on Mel Brooks, it might be worth my while to first write the new Starbreaker as a screenplay, or one screenplay per chapter (the better to adapt for Netflix, my dear). I can use `Fountain <https://fountain.io/>`_ markup, and `Ben Scott <https://benscott.org>` has a repository of HTML partials and Coffeescript called `Inkwell <http://bescott.org/inkwell/>`_ that I could incorporate into my Jekyll build for this website to render my Fountain scripts into the standard screenplay format.

I just have to copy the license from the `Inkwell repository <https://github.com/evan-erdos/inkwell>`_ into my own repository's license file to comply with the terms of the MIT License.

But that's not your problem. :)

If you want more, follow me on your favorite platform. Hopefully that's `Mastodon <https://octadon.social/@starbreaker>`_, but Google+ will do. 

Oh, and one more thing: the "dragon" you saw mentioned above? You're gonna *love* it. 

If you don't, Claire will summon Satan all over your hard drive.
