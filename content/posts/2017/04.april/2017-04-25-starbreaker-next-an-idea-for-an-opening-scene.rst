Starbreaker Next: An Idea for an Opening Scene
##############################################

:date: 2017-04-25 21:32
:category: Starbreaker
:tags: claire ashecroft, shit my characters say, script, screenplay, idea, cold open
:summary: Nobody is going to mistake me for John Carpenter any time soon...
:slug: starbreaker-next-idea-opening-scene
:summary_only: true


Here I've got a cold open for a story that begins *in media res*, since I'll be skipping over a lot of stuff I'd rather not write about right now. I haven't decided if I'll do this exactly like this, but it's an idea.

Of course, nobody is going to mistake me for David Mamet or John Carpenter any time soon, but who cares? The screenplay-like format is mainly for roughing out scenes.

INT. FUTURISTIC STARSHIP - NIGHT
================================

A young, well-built, auburn-haired woman in form-fitting futuristic armor smiles at a camera.

CLAIRE ASHECROFT
  I used to think reality was dull. I used to wish I could take part in an epic adventure.

Claire checks her rifle, which resembles a matte-black collaboration between Nikola Tesla and Mikhail Kalashnikov.

CLAIRE
  I got what I wanted, good and hard. I might not survive it.

Claire slings her rifle over her shoulder and sticks out her pierced tongue.

CLAIRE
  I've got no regrets, though. I asked for every moment.

A tall man with long black hair and an equally tall woman with snow-white hair walk behind Claire. Both wear similar armor to Claire's, and carry similar rifles, but the man has a longsword strapped to his back while the woman wears a rapier on her hip.

CLAIRE
  If anybody sees this, they'll think Morgan and Naomi are the real heroes. The weapon who chose to be a man and the devil's daughter. Bigger than life and twice as sexy, right?

Naomi adjusts a strap on Morgan's armor. Morgan steals a kiss.

CLAIRE
  It's hard to think of Morgan and Naomi as larger-than-life heroes when you've listened them shagging in an adjoining hotel room with your hands in your knickers.

SHIP'S VOICE (O.S.)
  We're currently 100,000 kilometers from Earth. We will reach Lunar orbit and begin our approach to Armstrong City in 17 hours and 59 minutes.

CLAIRE
  Plenty of time to explain why we're flying to the moon in the belly of a dragon that used to sail between the stars using its wings as photon sails, right? I hope so, because it's liable to be a long, strange mother of a story.
