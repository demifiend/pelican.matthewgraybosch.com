Paper Books vs Ebooks: Both Have Their Uses
###########################################

:date: 2017-03-31 12:26
:category: Hiding Behind Books
:tags: ebooks, print, use cases, neutrality, false dichotomy
:summary: In the paper vs. electronic books debate, my position is one of armed neutrality.
:slug: paper-books-vs-ebooks-both-useful
:summary_only: true


In the paper vs. electronic books debate, my position is one of armed neutrality. I like both for different reasons, and if you don't like it I'll summon Satan all over your hard drive.

Before ebooks, I bought a shitload of paperbacks. If I found myself rereading a book and getting more out of it each time, I'd get a hardcover and give away the paperback.

Once ebooks caught on and devices got cheap, I started doing a lot of reading on screens, starting with a Nook that I've since given to my parents. However, they don't use it; when they read, they prefer paper.

Getting back to my own preferences: I currently have a Kindle paperwhite and the Kindle app on my phone. I buy a lot of ebooks, mainly because they *don't* take up shelf space or nightstand space. They're convenient in that regard, though the space issue isn't as pressing a concern for me as it had been now that I've moved from a cramped two-bedroom apartment into a three-bedroom house with a living room, a family room, and an attic just begging to be finished and remodeled into a loft master bedroom with an *en suite* bathroom.

Ebooks are also *discreet*. If I'm stuck in a meeting, there's no way I could get away with whipping out a hardcover or a paperback and reading while the suit up front drones on and on and drip-feeds us information that could have been conveyed by email. However, in meetings like that just about *everybody* is dicking around with the phones so I'm less likely to get caught reading.

Likewise, if I walk into the crapper with a hardcover it's obvious I'm hiding in there to slack off. But walking into the crapper with my phone in my pocket doesn't raise any questions.

Let's not forget that when you read on your phone, the only way to tell if somebody is reading erotica or pornography is to look over their shoulder and risking a well-deserved ass-kicking. Incidentally, this is a pet peeve of mine; people who look over a reader's shoulder to figure out what they're reading should mind their own business.

Despite the advantages of ebooks, I still buy books in hardcover for various reasons. My most recent hardcover purchases include...

- Neil Gaiman: *Norse Mythology*
- Jacqueline Carey: *Miranda and Caliban*
- Lev Grossman:
  - *The Magicians*
  - *The Magician King*
  - *The Magician's Land*
- Alexandre Dumas (translated by Robin Buss) *The Count of Monte Cristo*

Granted, I had to buy the hardcover Penguin Classics edition of Dumas secondhand, but it came in near-pristine condition and looks gorgeous on my shelf, so it was worth it.

Why did I buy these books in hardcover?
=======================================

In Jacqueline Carey's case, I have her first two Kushiel trilogies in hardcover and she's one of my favorite authors. *Kushiel's Dart* in particular was one of the books my wife and I shared before we got married.

In Lev Grossman's case, my wife and I had watched *The Magicians* on Netflix, and I simply didn't feel like dicking around with paperbacks or ebooks since I was already into the story. 

I already had the Buss translation of *The Count of Monte Cristo* in trade paperback, but I figured it could only take one more re-reads from my wife and me before it started falling apart, and we wanted an attractive and durable copy for our shelf. 

Finally, I grabbed Neil Gaiman's *Norse Mythology* simply because I wanted to read his take on those old stories outside the context of *American Gods*.

In general, I still prefer to have hardcover copies of books that I will re-read several times over the years for the same reason I'll buy my favorite albums on CD or even vinyl. I want to *own* them, not merely buy a license to read them. I want to be able to touch them. I want guests to see that they're important parts of my life.

Most importantly, hardcover books are touchstones. Catherine and I can take them down off the shelf and remember the way we were when we were younger and learning to love each other.
