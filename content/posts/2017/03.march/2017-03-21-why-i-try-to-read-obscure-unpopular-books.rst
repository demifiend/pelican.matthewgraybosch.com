Why I Try to Read Obscure or Unpopular Books
############################################

:date: 2017-03-21 10:06
:category: Hiding Behind Books
:tags: goodreads, obscure books, unpopular books, book choices
:summary: Haruki Murakami is right. If you only read the books that everyone else is reading, you can only think what everyone else is thinking.
:slug: why-read-obscure-unpopular-books
:summary_only: true


I saw a Reddit post on r/fantasy this morning that asked: `"What is your goodreads cut off when choosing books?" <https://www.reddit.com/r/Fantasy/comments/60mztu/what_is_your_goodreads_cut_off_when_choosing_books/>`_. In it, the author added the following:

    There is really two parts to this question:
    
    1. Would you read a book which gets below 3.5 stars?
    2. Would you read a book which has under 1,000 ratings?

Normally I would begin answering questions concerning Goodreads with two words, the first of which you aren't supposed to say on the radio because children might be listening. 

But I felt like making a point this morning so I hope the following is a more substantive/constructive response: **I would read a book that had a single one-star rating on Goodreads if the jacket copy caught my attention and the first 25-50 pages or so caught my interest.**

If you want to know why, here's the short version: *I'm a bit of a hipster.* I don't rock the aesthetic (because I don't have the figure for it) but I like to read obscure books for the same reason I like to listen to obscure heavy metal bands that don't sell millions of albums or pack arenas: *variety makes life more interesting*.

There's more, if you want to keep reading and don't mind a bit of a rant.

The Long Version
================

I'm not `other-directed <http://www.artofmanliness.com/2012/06/11/becoming-an-autonomous-man-in-an-other-directed-world/>`_ -- at least, I prefer to see myself as a more autonomous man -- so I tend not to choose books based on others' opinions. 

You can't count on me to read your book just because all my friends love it. You certainly can't count on me to read it just because it's popular.

I know that asking people to be less dependent on what other people think is an exercise in spitting into a headwind, but I think somebody should. By all means, ask friends, acquaintances, or even strangers on the net if they can recommend good books. But don't go by their opinions or reviews. They can't tell you if you'll enjoy a book. All they can tell you is whether *they* did, and why.

You've still got to decide for yourself. I think you should make your decision based on the book itself, not stars and ratings on Goodreads or reviews on Amazon. Get the Kindle app or preview the sample in your browser, and see for yourself if a book grabs you.

If you only read books with at least 3.5 stars or 1,000 ratings, you might miss out on obscure books that you would have loved if only you gave them the 5-30 minutes it might take to read the preview on Amazon. While no author is entitled to an audience, limiting yourself to books with at least 3.5 stars or 1,000 ratings on Goodreads makes it harder for new and independent authors to gain traction.

I freely admit that I'm posting this out of `self-interest <https://www.goodreads.com/author/show/7144002.Matthew_Graybosch>`_, but I'm not the only indie author who frequents r/fantasy. I won't presume to speak for them, especially those who do a better job of marketing themselves than I do, but the following comes to mind:

    If you only read the books that everyone else is reading, you can only think what everyone else is thinking.

    -- Haruki Murukami, *Norwegian Wood*

Never mind old Apple advertisements. If you want to think different, *read different*.
