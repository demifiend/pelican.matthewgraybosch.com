The Parable of the Blind Swordfighter
#####################################

:date: 2017-03-01 15:55
:category: An Unexamined Life
:tags: anger, control, mindfulness, metaphor, restraint, consequences, zatoichi, blindness
:summary: Whenever you feel angry, imagine that you're a blind person in a crowded room with a sword that doesn't have a handle. It's a metaphor for anger and the need for control I've found useful, even if I don't consistently apply it.
:slug: parable-blind-swordfighter
:summary_only: true


Whenever you feel angry, imagine that you're a blind person in a crowded room with a sword that doesn't have a handle. It's a metaphor for anger and the need for control I've found useful, even if I don't consistently apply it.

That sword is your anger, and if you draw that sword in the crowded room by lashing out with harsh words or violent actions, you have no way of knowing who you might wound. You might cut the person who angered you. You might cut an innocent bystander. You won't be able to tell until it's too late.

It gets worse. When you strike down your enemy, you do the same to their friends and family. No matter how richly they might deserve the bite of your steel, remember one thing: even Adolf Hitler had a girlfriend and a dog who loved him.

Finally, and worst of all, you're still hurting yourself even if by some miracle your stroke slices only air instead of flesh. The sword has no hilt, and it cuts your hands as you grasp it.

For all of these reasons, you should careful about when you yield to your anger. Don't draw that sword unless you're absolutely sure you're justified in doing so.

However, when the cause *is* truly just, you should let nothing stop you from drawing the sword of your rage and striking with such force and fury that even God stands defenseless before you.

Too Many Samurai Movies?
========================

If it sounds like I've seen too many samurai movies, it's possible that I might have. The image of the blind swordsman in a crowded room was probably inspired by `Zatoichi <https://en.wikipedia.org/wiki/Zatoichi>`_, a long-running and beloved series of novels and movies created by `Kan Shimozawa <https://en.wikipedia.org/wiki/Kan_Shimozawa>`_. 

.. image:: {filename}/images/zatoichi-blind-samurai.png
	:width: 400px
	:height: 300px
	:alt: A still from one of the old Zatoichi films
	:align: center
	:target: {filename}/images/zatoichi-blind-samurai.jpg

Zatoichi is a wandering masseur and gambler living in early 19th century Japan (Edo period) who carries a sword-cane because only samurai may legally possess proper katanas. He regularly draws his sword on behalf of the downtrodden, which makes him a heroic figure similar to pulp legend `Zorro <https://en.wikipedia.org/wiki/Zorro>`_ and Marvel comics hero `Daredevil <https://en.wikipedia.org/wiki/Daredevil_(Marvel_Comics_character)>`_.

I've only seen the 2003 movie directed by and starring `"Beat" Takeshi Kitano <https://en.wikipedia.org/wiki/Takeshi_Kitano>`_. Netflix might still offer it for streaming under *The Blind Swordsman: Zatoichi*. The Criterion Collection offers the `original films from the 1960s and 1970s as a 9 disc boxed set <https://www.criterion.com/boxsets/1012-zatoichi-the-blind-swordsman>`_ if you've got $160.00 to spare.
