I Always See Her
################

:date: 2017-08-22 20:52
:slug: i-always-see-her
:tags: love, romance, marriage, Catherine Gatt, Sarah Brightman, Paris, memories
:summary: Ripping Sara Brightman's LA LUNA album had me listening to an old song and thinking about my wife.
:summary_only: true


I haven't listened to Sara Brightman's *La Luna* album in years; it was packed away with the rest of my CD collection since 2009, but one of my little projects after moving in to my new house and creating a home office was ripping all of my old CDs.

One of those songs is called "He Doesn't See Me", a lament from a woman who loves a man who loves somebody else.

.. raw:: html

	<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/lB7pYi3NaLI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

I often suspect my wife Catherine might have felt this way once or twice before we met, but if that's the case she's never told me so. In any case, I always see her. How could I not when she's done and given so much to help me build the life we share together?

.. catherine-paris-army-museum-angelinas.jpg

.. Catherine in Paris, at Angelina's in the Army Museum (June 2017)
