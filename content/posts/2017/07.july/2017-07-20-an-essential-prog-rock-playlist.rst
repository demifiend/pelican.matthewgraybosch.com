An Essential Prog Rock Playlist
###############################

:date: 2017-07-20 12:20
:category: Metal Appreciation
:tags: progressive rock, david weigel, the show that never ends, playlist, history, npr, pretentious wank
:summary: I grew up listening to prog, and I still listen to it, so naturally I'm interested in David Weigel's new book, The Show That Never Ends.
:slug: essential-prog-rock-playlist
:summary_only: true


I'm not sure where I found it, but yesterday I found this `NPR article about David Weigel's new book <http://www.npr.org/2017/07/18/534577902/can-t-prog-rock-get-any-respect-around-here>`_, *The Show That Never Ends: The Rise and Fall of Prog Rock*.

If you're not familiar with the term, prog rock is short for progressive rock, rock fused with jazz, European baroque and Romantic music, and other influences to create songs and other pieces far more complex than traditional blues-based rock.

Some people, like rock critic Lester Bangs, think it's nothing but pretentious wank. I happen to *like* pretentious wank, and I grew up listening to prog before I knew anybody thought it was for pretentious wankers thanks to my father.

I haven't read the book yet, but I just ordered it because why the hell not? In the meantime, David Weigel's put together a sweet playlist. It's got some classic tracks by the usual suspects like King Crimson's "21st Century Schizoid Man", "Close to the Edge" by Yes, and Emerson, Lake & Palmer's "Tarkus", but it's also got some material I haven't heard before.

.. raw:: html

  <iframe src="https://open.spotify.com/embed/user/npr_music/playlist/3scpkgvcoSUeNSDrMCIBHH" width="100%" height="380" frameborder="0" allowtransparency="true"></iframe>

Hell, I'm not sure if my father's heard some of this stuff, like the "Valentyne Suite" by Colosseum. There's some sweet Hammond organ work in that piece, paired with saxophone. Check it out, and don't forget to email me. Do you know any bands I should check out that aren't on this list?
