Corporate Monopolies Considered Harmful
#######################################

:date: 2017-10-24 22:07
:category: Everything Considered Harmful
:tags: antitrust, break them up, corporate death penalty, corporations, economics, joseph stiglitz, monopoly
:summary: We have tolerated the existence of corporate monopolies like Google, Facebook, Microsoft, Apple, Comcast, Verizon, Exxon-Mobil, and Con-Agra for too long, but there's a solution: break them up.
:slug: corporate-monopolies-considered-harmful
:summary_only: true


First, a history lesson. You should probably read `Dr. Stiglitz's article <https://www.thenation.com/article/america-has-a-monopoly-problem-and-its-huge/>`_ in *The Nation* first.

    Some century and a quarter ago, America was, in some ways, at a similar juncture: Political and economic power seemed concentrated in a few hands, in ways that were inconsonant with our democratic ideals. We passed the Sherman Anti-Trust Act in 1890, followed in the next quarter-century by other legislation trying to ensure competition in the market place. Importantly, these laws were based on the belief that concentrations of economic power inevitably would lead to concentrations in political power. Antitrust policy was not based on a finely honed economic analysis, resting on concurrent advances in economics. It was really about the nature of our society and democracy. But somehow, in the ensuing decades, antitrust was taken over by an army of economists and lawyers. They redefined and narrowed the scope, to focus on consumer harm, with strong presumptions that the market was in fact naturally competitive, placing the burden of proof on those who contended otherwise. On this basis, it became almost impossible to successfully bring a predatory pricing case: Any attempt to raise prices above costs would instantaneously be met by an onslaught of new firm entry (so it was claimed). Chicago economists would argue—with little backing in either theory or evidence—that one shouldn’t even worry about monopoly: In an innovative economy, monopoly power would only be temporary, and the ensuing contest to become the monopolist maximized innovation and consumer welfare.

Did everybody get that? No? Here's the deal.

1. All of this shit has happened before, in the 1890s.
2. We dealt with it by breaking up businesses that got too big.
3. We didn't break them up because it was "good for the economy" or to promote "consumer welfare", but because it was good for the republic and its citizens.
4. Economists from the Chicago school have conned our leaders into thinking vigorous antitrust enforcement is unnecessary because in a free market monopolies are only temporary.
5. The problem is that free markets only exist in the imaginations of neoliberals and Ayn Rand fans.

So, what do we do about corporations that have become too large and too powerful? We do the same thing we did over a century ago. We break them up. Or, because it's the 21st century and we got Twitter instead of life in space: **#BreakThemUp**.

Break up Alphabet (Google's parent company).

Break up Microsoft.

Break up Amazon.

Break up Apple.

Break up Disney.

Break up Facebook.

Break up Comcast.

Break up Verizon.

Break up Con-Agra.

Break up Exxon-Mobil.

Break up AT&T *again*, because those assclowns didn't learn their lesson last time.

Break up every multinational corporation on Earth, or at least bar them from operating on American soil.

No corporation should be permitted to operate in more than one state of the Union, in more than one industry, or exist for more than 10 years. 

No corporation should be permitted to exist unless it provides a specific public benefit that government is not legally authorized by its constitution or charter to provide. 

No corporation should be construed as having rights, and no corporation should be able to buy other corporations.

It should be possible to dissolve a corporation and shut it down at the first evidence of malfeasance on the corporation's part.

A corporation's employees should have as great a say in the corporation's governance as its stockholders.

No, this isn't communism. Communism would involve nationalizing the corporations and putting the stockholders in the fucking gulag where they belong. This isn't `"hipster antitrust" <https://developingworldantitrust.com/2017/09/13/hipster-antitrust/>`_, either. This is how we *used* to do shit in the United States, back when government of the people was *by* the people and **for** the people.

More of this bullshit later...
