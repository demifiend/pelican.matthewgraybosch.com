White Identity Considered Harmful
#################################

:date: 2017-10-19 22:17
:category: Everything Considered Harmful
:tags: antifa, fascists, identity politics, nazis, punching nazis, race, racism, white nationalism, white supremacy, whiteness
:summary: Let me make something crystal fucking clear: if you're of European ancestry and you think being white is the core of your identity, then you probably don't actually have an identity or anything resembling a life.
:slug: white-identity-considered-harmful
:summary_only: true


While I'm glad that I'm glad that no *humans* were harmed during `Richard Spencer's appearance at the University of Florida <https://www.washingtonpost.com/news/grade-point/wp/2017/10/18/uf/>`_, that ignorant shitfountain is a mere symptom of the real disease.

Whiteness iself is the real problem. Let me make something crystal fucking clear: if you're of European ancestry and you think being white is the core of your identity, then you probably don't actually have an identity or anything resembling a life. If that's the case, you should try getting one of each, preferably a life and identity that doesn't involve being Nazi filth.

If notions like "white solidarity" and "white pride" are anything to you but contemptible, then you yourself are *beneath* my contempt. How dare you take pride or seek solidarity in a quality that you did nothing to earn? You're no better than a gambler who acts like he's king of the world because he had a good night at the blackjack table.

Being born white is like being dealt aces at a poker game with two aces on the damn table. You've already got a damn good hand just by showing up, and you did fuck-all to earn it.

But it's always the assholes who never bothered to play their hands to advantage who make such an inordinately big *deal* about having been dealt aces. You know what getting dealt a pair of aces will get you when the other player builds a better hand from their own hole cards and whatever's on the table? **Jack shit**, which is all most white supremacists have to offer--and they damned well know it.

That's right. Most white supremacists, or white nationalists, or whatever the hell you want to call them, know they're worthless. They know God wasted perfectly good skin on them. They know the world's better off without them because they've made nothing of their lives. They know that white genocide is the only genocide that comes close to being justifiable.

Does saying this make me a "self-hating white person"? Only to people who take notions like "white genocide" seriously. I don't even think of myself as white. "White" is a false identity created to con poor colonists of European ancestry into a false sense of solidarity with rich white slaveowners. It's based on the notion that even poor white trash are "better than" black people.

What am I, if not "white"? What is the core of my identity? My identity is mine to decide for myself. The core of my identity is whatever I damned well *say* it is, and any Nazi who thinks otherwise can go straight to Hel with the rest of the cowards and weaklings.

This is where I could talk big about punching Nazis, and it's certainly fair to say that Nazis like Richard Spencer deserve to get punched. They know it. They're psyched up for it. They *want* to get punched. That's why they're out on the streets instead of talking shit on Stormfront and 4chan while jerking off.

Why do they want to get punched? Because it makes martyrs of them as surely as if I showed up at one of their rallies with a rifle and bagged my limit. These dumb fucks must think they'll go to Valhalla if they get to die fighting for their race or something, so why let them have that illusory glory?

Let them die of old age in their beds. Let their deaths go unnoticed except by those who must bury them. Let them die the *straw death*. Indeed, let Hel's embrace be their fate.

Don't mourn their passing. Don't give them funerals. Just put them in a landfill with the rest of the garbage once their time on earth has run its natural course, and let their very names be forgotten.

And if you can't afford such patience, why not laugh at them instead like antifascists do in Europe? Fanatics can't stand mockery. They can't handle being laughed at. They can't handle seeing their ideals ridiculed. They're like Jehovah's Witnesses in that respect.

More about this bullshit later...
