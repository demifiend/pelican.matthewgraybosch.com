title: SMBC Explains Why Office Life Sucks
date: 2016-03-08 09:54
tags: automation, basic income, bullshit jobs, capitalism, webcomic, pretending to work, revolution, smbc, work ethic
summary: SMBC explains why we can't have nice things, and I go off on a rant as usual.
summary_only: true


Saturday Morning Breakfast Cereal posted a comic yesterday that [pretty much explains why office life in modern America sucks balls](http://www.smbc-comics.com/?id=4042). Not to mention big floppy donkey dicks.

Basically, this is why we can't have nice things. We're still stuck on the notion that it's good to work, so we should force people to do so by threatening them with poverty if they don't.

But we could make a better world for ourselves. We have the technology. We could create a sustainable economy where people aren't working (or pretending to work) for 8-12 hours a day just to "earn a living".

Why should any of us have to "earn a living", anyway? Because the Bible said so? Screw the Bible. Screw the rich, too; they need us more than we need them. Who's going to buy all the shit that gets made and keep the long con of capitalism going?

It won't be the rich. It won't be the robots with which they're replacing us workers, either. So the rich have two choices: basic income, or being treated to a rousing chorus of "Up Against the Wall, Motherfucker!" when everybody else realizes that the people in charge are the real enemy.

We can use the tune to ["Uncle Fucker" (NSFW)](https://www.youtube.com/watch?v=IaEfs1HBJq8). 🙂