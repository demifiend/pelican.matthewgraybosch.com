title: Movie Night on the Graveyard Shift
date: 2016-03-20 04:20
summary: I've spent a couple of quiet Saturday nights watching movies my wife rented while working the graveyard shift.
summary_only: true
og_image: /images/king-crimson-the-night-watch.jpg
category: Two Thumbs Down
tags: graveyard shift, movies, ant-man, hitman, agent 47, crimson peak, the last witch huner


I'm working the graveyard shift on a Saturday night, so my wife slipped the movies she had rented to watch while I was sleeping into my bag to take with me. I don't mind watching movies on DVD while working on weekends since I've got dual screens, and after one in the morning I'm just babysitting a server farm that's idle save for a few hourly and semi-hourly jobs.

I just wish my brain would get its arse into gear so I could work on _Starbreaker_ at night instead. But the need to keep an eye on the server monitor makes getting into flow and writing difficult.

Enough bitching. Tonight, we've got _Ant-Man_ and _Hitman: Agent 47_ on tap. A couple of weeks ago it was _Crimson Peak_ and _The Last Witch Hunter_. I've got things to say about each, so expect spoilers.

## Ant-Man

_Ant-Man_ was a decent heist flick, though it wouldn't have been worth seeing in the cinema. Paul Rudd as ex-con Scott Lang really sold it, and Michael Douglas as Hank Pym and Evangeline Lilly as Hope van Dyne were excellent supporting characters. Unfortunately, Corey Stoll as Darren Cross in the Yellowjacket suit was just a bit too reminiscent of Jeff Bridges as Obadiah Stane in _Iron Man_ (2008) for my liking.

The whole "going subatomic" thing seemed a bit overplayed, once Scott had his little shrink/enlarge throwing stars it was obvious he'd use one of the enlargers to counteract the disabled regulator. He might have thought to use it sooner if Pym hadn't scared the shit out of him, though we did get some visuals reminiscent of _2001: A Space Odyssey_ &#8211; and the possibility that Mrs. Pym might still be alive.

Well, maybe not. But Peyton Reed isn't Stanley Kubrick. And he was working on the quantum level, where Kubrick was on the cosmic level &#8212; and possibly acid. But it was the 1960s. You know who wasn't 
on acid? The Establishment and anybody who wanted to join 'em.

_Ant-Man_ is recommended for _Marvel fans and heist movie fans_.

## Hitman: Agent 47

My wife rented _Hitman: Agent 47_ mainly because she wasn't expecting much, and doesn't share my antipathy for movies based on video games. She figured it would just be a fun action movie.

I really don't have high hopes for this, but I'm not heavily invested either. I never got into the _Hitman_ franchise of stealth games. So, let's give it a shot.

Not even ten minutes and I'm already bored. Jesus wept. This is the sort of dialogue you can expect:

> Dude: "I work for a corporation called Syndicate International. I'm here to protect you."
>
> Katya: "From who?"
>
> Dude: "His name is 47, and he's an agent. Right now the only thing standing between him and you is me."

I mean, seriously? Syndicate International? And Katya, who the movie has shown to have some kind of low-grade ESP, is going to trust this guy? Fuck this. I'm done.

_Hitman: Agent 47_ is recommended for people who want their balls stomped into jelly but can't afford to hire a dominatrix, or even to buy a copy of *Dark Souls*.

## Crimson Peak

So, we've got a classic gothic horror story directed by Guillermo Del Toro starring Mia Wasikowska as Edith, an American heiress with literary ambitions who sees dead people, Tom Hiddleston as Sir Thomas Sharpe (yet another Byronic character), and Jessica Chastain as Lady Lucille Sharpe, Thomas' sister.

Though Hiddleston is solid as Thomas, don't expect him to chew the scenery like he did as Loki. According to [Bustle](http://bustle.com), _Crimson Peak_ is [all about the women](http://www.bustle.com/articles/117278-is-crimson-peak-a-feminist-film-tom-hiddlestons-lead-doesnt-wield-the-real-power).

> The gothic romance horror film Crimson Peak takes place in the late 1800s early 1900s, and, as such, implemented a delightful amount of Victorian-era feminism. Although I originally went to the movie for Tom Hiddleston, I left feeling thrilled by the gender equality in the Guillermo Del Toro film. That's right, Crimson Peak is definitely a feminist film, and that's what attracted Jessica Chastain (who plays Hiddleston's onscreen sister Lucille) to the story. Chastain told reporters at the New York premiere on Wednesday that, as a feminist, she was so excited to see how strong the Crimson Peak women were were written to be. "It was really important [to me]," she says. "When I read the script that was the first thing I noticed, that there were two amazing female characters. Most of the time I get a script and I'm usually reading the only female character in the movie."

The sets and costumes are gorgeous, Ms. Chastain is deliciously evil, and Del Toro's penchant for the grotesque gets free rein with his ghosts, but it's Mia Wasikowska who makes the movie work. She _nails_ Edith's character, showing us her growth from innocence to experience through resolve. Instead of being broken by her experiences, one suspects that they only tempered her.

Throughout the movie, however, I was reminded of classic King Diamond albums like _Abigail_ and _Them_. It must have been the tea. You'll see.

_Crimson Peak_ is recommended for _fans of gothic stories with well-written and richly portrayed female characters_

## The Last Witch Hunter

This is pretty much a competent urban fantasy romp with Vin Diesel being Vin Diesel. If you've seen him in his various Riddick movies, the _Fast and Furious_ franchise, or _XXX_, you have a good idea of what to expect.

This time around, he plays Kaulder, a soldier of a witch-hunting religious order called the Axe and Cross. 800 years ago he and a party his fellow warriors mounted an assault against the Witch Queen. This battle ended with most of the witches and witch hunters dead. The Witch Queen herself cursed Kaulder with immortality before her tree fortress burned down around them.

When the Axe and Cross found a burned Kaulder in the ashes, they _also_ found the Witch Queen's still-beating heart. Before their eyes, Kaulder began to heal, but began dying as soon as one soldier held his sword to the Witch Queen's heart.

The connection's obvious, and will prove plot-relevant later on. We cut to the present day, where Kaulder doesn't just kill witches any longer. As we see when he sits next to a young witch on a flight caught in the middle of a nasty thunderstorm caused by magic she unleashed by mistake, Kaulder tries to save them.

It's this change in characterization that salvages _The Last Witch Hunter_, and allows it to be a decent rental. We don't get to see what Kaulder experienced over 800 years of life to make him a more humane person, but creates a character that suits Vin Diesel perfectly.

Rose Leslie is solid as Chloe, and Elijah Wood competently plays the treacherous sidekick as Dolan 37, but I suspect Michael Caine gets a certain wry pleasure out of playing the Alfred as Dolan 36, and he gets one of the movie's best lines as he swats a lingering plague wasp with his journal: "Try doing _that_ with an iPad."

On the villains' side, Ólafur Darri Ólafsson chews the scenery for a while as Belial, but Julie Engelbrecht's Witch Queen feels more like an obligatory plot device than a character. It's almost as if Ms. Engelbrecht's makeup technicians put more effort into her character than the screenwriters.

_The Last Witch Hunter_ is recommended for _urban fantasy fans and people who liked Keanu Reeves in Constantine_.
