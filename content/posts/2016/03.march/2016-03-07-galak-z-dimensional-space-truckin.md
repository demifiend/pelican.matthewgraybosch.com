title: Galak-Z: Dimensional Space Truckin'
summary: Let's go space truckin' in 17-BIT's roguelike 80s sci-fi anime shoot-'em-up *Galak-Z: the Dimensional*.
summary_only: true
category: Hardcore Casual
tags:, 17-bit, 1980s, anime, galak-z, macross, mecha, nostalgia, ps4, robotech, roguelike, space opera, wintendo, review


I decided to check out [17-BIT's](http://17-bit.com/) new roguelike sci-fi shooter [_Galak-Z: The Dimensional_](http://17-bit.com/galakz) this weekend. Good times, but I guess Deep Purple's "Space Truckin'" wouldn't fit the 80s anime aesthetic.

## Update: 7 March 2016

The PS4 port of _Galak-Z: The Dimensional_ is free for PlayStation Plus subscribers until 5 April 2016. Grab it and give it a shot.

## About _Galak-Z: The Dimensional_

So, what is _Galak-Z_? It's basically a 2D space shooter for the PS4 and Windows with a storyline, characters, and general aesthetic drawn from 70s and 80s sci-fi anime like _Battle of the Planets_, _Robotech_, _Voltron_, and other series that never got [Macekred](http://tvtropes.org/pmwiki/pmwiki.php/Main/Macekre) for an English-speaking audience.

The game is divided into five "Seasons", both a throwback to the anime that inspired the game and a mechanic reminiscent of DOS shareware games like _Doom_, _Wolfenstein 3D_, and _Commander Keen_. To advance from one Season to the next, you have to complete five consecutive procedurally generated missions without dying. Easier said that done, at least in my case.

You play as "A-Tak", an Earth Navy pilot who probably _isn't_ qualified to fly. He's escaped the destruction of his ship aboard a Galak-S fighter that turns out to be something special. He soon gets in over his head, but receives help from an officer named "Beam" aboard the science vessel _USS Axelios_. With help from "Crash", a reggae-loving salvager, they patch up their ships, gather supplies, and begin to fight back against the Empire, Void Raiders, giant space-dwelling bugs, and other threats to humanity.

A classic space shooter would give you attempts before you're sent back to the beginning of the level with your score reset to zero and your ship stripped of all upgrades. 17-BIT did something a bit different. A-Tak's fighter has a shield that can take a couple of hits, and armor capable of taking up to four hits. The shield regenerates, but the armor must be repaired--usually at considerable expense. If your armor is destroyed, there goes your ship and all progress within the Season.

No doubt this would give _Galak-Z_ a frustration potential worthy of other roguelike games, but 17-BIT throws players a bone in the form of Crash Coins. Collect five, and you can redeem them to restart the current mission with all your upgrades locked inside a "Recovery Crate". Find the crate, blow it up, and retrieve the contents to continue on your way. Otherwise, you can redeem your Crash Coins for Salvage on your next playthrough &mdash; and use that Salvage to buy upgrades before your first mission.

What kind of upgrades? The usual: thruster upgrades, booster upgrades, a coat of paint that stuns bugs when they touch you, missile capacity boosts, a missile targeting computer speed boost, incendiary missiles, and lots of fun laser upgrades.

Speaking of the laser, 17-BIT did an interesting job with the Galak-Z's main weapon. It comes with moderately rapid fire while holding down the X button by default, and the gun fires in a straight line at first. However, the default laser loses accuracy as you hold down the trigger, so that you can fire at up to a 30&deg; an angle from your ship's nose. One upgrade can decrease the spread to a maximum of 20&deg;. Another can eliminate the spread entirely, and make each shot more powerful at the expense of a reduced rate of fire.

That's just the beginning. Want shots that bounce off walls at an angle? Crash has you covered. Want an incendiary laser? You can have one of _those_, too. With the right upgrades, you can even have a laser that resembles a shotgun.

You'll need every upgrade for your fighter you can get, because the enemies in _Galak-Z_ are vicious. They'll patrol, flee to retrieve backup, and use AI subroutines provided by the [Cyntient](http://www.cyntient.com) engine to learn about their environment and adapt to _your_ play style.

## The Verdict

The last time I bothered to review a game (several years ago), I didn't deal in stars or a numeric score. I don't get paid enough for such nonsense. Instead, I would give one of the following recommendations:

  * Buy
  * Rent/Try the Demo First
  * Ignore
  * Nuke the Developers from Orbit

Because _Galak-Z_ is a downloadable title, presumably unavailable on physical media, renting the game to see if you'll like it isn't an option. Likewise, demos for the game aren't available on the PlayStation Network or Steam.

Because you pretty much have to buy the game to play it, and it can cost $20, you should think carefully before pulling the trigger. Do you like games with procedural generation? Do you like games with moderately realistic space physics (inertia, knockback from explosions, etc)? Do you feel nostalgic for 80s space opera anime like _Robotech_? Do you have a high tolerance for having to start over from zero after repeated ass-kickings?

I don't regret buying _Galak-Z: The Dimensional_ despite my usual fare including RPGs like _Bloodborne_ and _Shin Megami Tensei IV_. This isn't a game that requires twitch reflexes, which sets it apart from classics like _Gradius_ and _R-Type_. Instead, it's a deeply tactical game that rewards the careful exploitation of both your ship's capabilities, the environment, and the AI's tendency to turn on enemies of different factions when you're not around.

Trust me, there's nothing like leading a Sword Sentinel toward an incredibly huge space bug for a game of "Let's you and him fight." If you can spare $20 and the time necessary to download the updates, give it a shot. The only downside I've seen is that it can be hard to keep track of especially frenetic dogfights on the PS4 with my TV. Members of the PC Master Race might get better results.

_Recommendation: a qualified **BUY** for fans of roguelikes, space shooters, and 80s anime space opera._
