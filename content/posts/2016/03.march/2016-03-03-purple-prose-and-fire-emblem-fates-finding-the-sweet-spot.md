title: Purple Prose and Fire Emblem Fates &mdash; Finding the Sweet Spot
date: 2016-03-03 08:05
summary: What if purple prose, used in moderation, wasn't the aesthetic atrocity that style fascists and writers even more pretentious than I am make it out to be?
summary_only: true
category: Hardcore Casual
tags: Fire Emblem Fates, gamers, Garon, localization, purple prose, style, sweet spot, translation, birdsite


If you're a writer, you've probably been accused of writing purple prose, especially if you recently binged on writers like H. P. Lovecraft. Or, if you're a _snob_ like I am, you've probably accused others of writing it. Maybe, like me, you've suggested that a writer you dislike writes prose so purple it can play "Smoke on the Water".

But what if purple prose itself wasn't a terrible thing? What if it was OK to use in moderation. What if we dealt with [style fascists](http://www.writers-village.org/writing-award-blog/writer-alert-how-safe-is-your-story-from-the-style-fascists-) [the way we used to deal with real fascists?](https://commons.wikimedia.org/wiki/File%3AMussolini_e_Petacci_a_Piazzale_Loreto%2C_1945.jpg) (Photo may be NSFW.)

Wait. That might just be a good way to get arrested. Don't do it unless you can afford to fight a murder rap. 🙂

## An Example from _Fire Emblem Fates_

Here's a case in point. Twitter user [@Xythar](https://twitter.com/Xythar) was making a point about how many gamers and anime fans insist on a direct translation from Japanese to English, rather than a localization that attempts to capture the spirit and flavor of the original Japanese text and render it in English. To illustrate the point, he shared the following from _Fire Emblem Fates_ by Nintendo:

<blockquote class="twitter-tweet" data-width="550">
  <p lang="en" dir="ltr">
    case in point: this image <a href="https://t.co/fVqWeYAGLY">pic.twitter.com/fVqWeYAGLY</a>
  </p>
  
  <p>
    &mdash; Xythar (@Xythar) <a href="https://twitter.com/Xythar/status/705243114361913344">March 3, 2016</a>
  </p>
</blockquote>

Read both versions, and never mind that somebody had to render dialogue originally written in Japanese into English. Some gamers might argue that they both convey the same information, but the translation is "cleaner". If they thought to do so, they might argue that Garon's speech in the localization is florid, or call the prose purple.

I disagree with them. I think they're _wrong_. I might not know jack about translation vs localization, but I _am_ a [published author](http://www.amazon.com/Matthew-Graybosch/e/B00FPJDDNC) so I know just a _bit_ about writing, plotting, and characterization.

The translation isn't clean. It's dry. It's probably as literal as possible. It's what you might get if Ernest Hemingway localized video games. And it _doesn't_ convey as much information as the localization.

## Localization vs Translation: First Panel

Consider the first panel. The translation goes like this:

> The Divine Dragon, our ancient god&#8230; 

For the love of Kefka, that isn't even a complete English sentence! I could argue for the localization's superiority on grammar alone, but it just gets worse. If you looked at the translation without any context, you wouldn't know who the hell Garon is.

> We of the royal family are descendants of the ancient gods, the First Dragons. 

With the localization, you see Garon identified as royalty. He's either addressing a relative, or using the royal we; I can't tell without additional context.

The translation implies that Garon venerates a single god, the "Divine Dragon". The localization, however, not only mentions ancient _gods_, but claims that Garon's lineage sprang from a group of gods called the First Dragons.

With the localization we see the Garon and his family are _polytheistic_, acknowledging the existence of multiple gods. Like in many other monarchies and empires they claim the right to rule by virtue of being descended from gods.

See how much more detail the localization conveys?

## Localization vs Translation: Second Panel

I don't think the translation fared too well in the first panel, but we've got two remaining. Here's the second panel of the translation.

> Its blood has been passed down from our ancestors to our royal family. 

At least this is a complete sentence, but notice the passive voice? Passed down by whom? Did the Divine Dragon get bored and decide to try _schtupping_ a human woman? Did he at least get her consent first?

Even worse, the phrase "from our ancestors to our royal family" strikes me as awkward. Are Garon's ancestors not part of the royal family? Or was Garon's family not always royal?

While you're chewing on that, here's the second panel of the localization:

> As inheritors of that divine strength, we conquer those who oppose us with ease. 

See that? Garon's actually making a point. Never mind that Garon's line allegedly descended from the Divine Dragon or the First Dragons; they inherited _power_ from these gods, and use it to conquer those who oppose them. The translation doesn't even mention this, but I'm supposed to consider the translation superior?

Not likely. Not when the localization continues the trend of conveying more detail than the translation.

## Localization vs Translation: Third Panel

The translation has lost in two panels out of three against the localization. That ought to be sufficient to call the match, but the last panel of the translation strikes me as especially egregious.

> We can destroy the enemy with its power. 

That's it? All this blithering about the Divine Dragon and inheriting its blood, and that's all Garon has to say? Who is the enemy? Is it just one guy?

If so, I am _not_ impressed, because I don't need to be descended from a divine dragon to take out one guy. I just need a gun, a good cause, and guaranteed immunity from prosecution. But let's have a look at the localization.

> One who learns to wield that power can destroy an entire army of common troops. 

See that? Now we know _exactly_ what it means to be part of Garon's royal family. It means inheriting divine power from the First Dragons that, should you succeed in learning to harness it, allows you to defeat entire armies of ordinary soldiers.

## Final Score: Translation 0 &#8211; Localization 3 &#8230; FLAWLESS VICTORY!

As far as I'm concerned, the "localization" of Garon's monologue from _Fire Emblem Fates_ is better than the "translation", even though the translation is [measurably simpler](http://wordcounttools.com/). I'll explain why, but first let me present both versions as wholes with some readability stats.

### Garon's Monologue Translated

> The Divine Dragon, our ancient god&#8230; Its blood has been passed down from our ancestors to our royal family. We can destroy the enemy with its power. 

| Word Count | Monosyllabic Words | Polysyllabic Words | Difficult Words | Readability level |
| ---------- | ------------------ | ------------------ | --------------- | ----------------- |
| 27         | 17                 | 4                  | 5               | Middle School     |

### Garon's Monologue Localized

> We of the royal family are descendants of the ancient gods, the First Dragons. As inheritors of that divine strength, we conquer those who oppose us with ease. One who learns to wield that power can destroy an entire army of common troops. 

| Word Count | Monosyllabic Words | Polysyllabic Words | Difficult Words | Readability level |
| ---------- | ------------------ | ------------------ | --------------- | ----------------- |
| 43         | 25                 | 6                  | 15              | College           |

## Accessibility vs Complexity

You're probably wondering why I insist the localization is superior to the translation despite the localization being almost three grade levels higher than the translation. That's a fair question.

The translation is more _accessible_. The stats prove it. It isn't as verbose. It contains a lower percentage of polysyllabic words _and_ has fewer ["difficult" words](http://wordcounttools.com/#definition_difficult_word). It should be readable to middle school students, whereas the tools I used for analysis suggest the localization requires a college student's reading level. Many people will seize on the expected reading level and insist that the translation is better because you don't have to be a college student to read it.

Despite these facts I think the localization is better than the translation. It's more complex, and some might call it purple prose, but consider what you get with that intricacy:

The localization's language is richer. I especially like the phrase "inheritors of that divine strength"; it sounds the way good chocolate tastes. It would also make a great title for a power metal album. 🙂

The language fits the character better, as well. [Garon](http://fireemblem.wikia.com/wiki/Garon) is a king, and his character portrait shows he isn't a young man. Because he is a king, and thus should be reasonably well-educated, it's in-character for him to speak as he does in the localization.

The localization also conveys more information about Garon's royal ancestry, what his family got from their divine ancestors, and what they can do with their heritage. The localization even shows that Garon had to _learn_ to use his power. None of that is in the translation.

## So, Where's the Sweet Spot?

What should writers take away from this post &#8212; aside from the obvious fact that I'm a pretentious twit with a serious complexity jones? There's a sweet spot between complexity and accessibility that's a Goldilocks zone for writers and readers alike. I don't know about you, but I want to write compelling stories as much as readers want to read them. Here's the problem: the sweet spot is a moving target. No matter how hard you try, you can't please everybody.

My recommendation? Please yourself first. Write something that makes you proud to call yourself a writer. If some readers don't like it, fuck 'em. (But get their consent first.)
