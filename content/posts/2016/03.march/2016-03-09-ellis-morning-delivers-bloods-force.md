title: Ellis Morning Delivers with *Blood's Force*
date: 2016-03-09
category: Hiding Behind Books
tags: ellis morning, blood's force, sword and starship, science fiction, science fantasy, space opera, indie, ebook
:summary: An acquaintance of mine, Ellis Morning, published her first SF novel, *Blood's Force*.


I just bought _Blood's Force_ by [_The Daily WTF_](http://thedailywtf.com/) contributor [Ellis Morning](http://thedailywtf.com/authors/ellis-morning), because the blurb had a kind of _Connecticut Yankee in King Arthur's Court_ vibe to it.

More details on [Google+](https://plus.google.com/+EllisMorning/posts/89h1voZvJ6p). Or just buy it on Amazon for [six bucks](www.amazon.com/Bloods-Force-Sword-Starship-Book-ebook/dp/B015TUD5BG/).

## Update: 9 March 2015

This is an excellent little sci-fi tale with capable protagonists and constant tension throughout. My only disappointment was that Ms. Morning ended the story where she did; I want moar.
