title: The Night Flight Orchestra is a Blast from the Past
summary: Björn Strid of Soilwork and bassist Sharlee D'Angelo of Arch Enemy recorded an album. Then they recorded another.
summary_only:true
og_image: /images/night-flight-orchestra.jpg
category: Metal Appreciation
tags: AOR, Arch Enemy, Björn Strid, early 1980s, Internal Affairs, late 1970s, revival band, Sharlee D'Angelo, Skyline Whispers, Soilwork, Spin̈al Tap
date: 2016-03-02

It seems vocalist Björn Strid of Soilwork and bassist Sharlee D'Angelo of Arch Enemy have a jones for classic AOR (album oriented rock), because they got together and started a side project called [The Night Flight Orchestra](https://www.facebook.com/The-Night-Flight-Orchestra-210664785613576/).

This ain't no shit, folks. Two veteran members of melodic death metal bands started a side project that sounds like Deep Purple, Styx, Journey, Night Ranger, REO Speedwagon, and other classic bands from the late 1970s and early 1980s.

If you need a TL;DR, the Night Flight Orchestra is a revival band of the sort of music parodied in the 1984 mocumentary, _This Is Spin̈al Tap_.

<iframe width="563" height="337" src="https://www.youtube.com/embed/N63XSUpe-0o" frameborder="0" allowfullscreen></iframe>

Since NFO is still a "side project", they've only got two albums, _Internal Affairs_ and _Skyline Whispers_.

## Night Flight Orchestra: _Internal Affairs_ (2012)

![Night Flight Orchestra: Internal Affairs (2012)]({{ base_path }}/images/night-flight-orchestra-internal-affairs.jpg)

I'm honestly not sure what to say about this album. It's a _good_ album, in that it sounds exactly like the sort of rock that got airplay back in the day. The band's tight, and the production's clean, but this album is really just good background music. You could put on headphones, set this album to repeat, and just let it run while you do a solid day's work.

"West Ruth Ave" has just the right amount of bluesy swagger to stand out, but it sounds like the Protomen covering a song by a more famous band. Not that there's anything wrong with that.

Give _Internal Affairs_ a listen and see for yourself.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A0SqFLkhUPdnfOUIc9xGGjA" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## Night Flight Orchestra: _Skyline Whispers_ (2015)

![Night Flight Orchestra: Skyline Whispers (2015)]({{ base_path }}/images/night-flight-orchestra-skyline-whispers.jpg)

Here's the deal. If you could only afford to buy _one_ Night Flight Orchestra album, _Skyline Whispers_ is the one you should choose. Everything the band got right in _Internal Affairs_ is refined and cranked up to 11 in their 2015 follow-up.

Of course, they now sound a hell of a lot like Deep Purple Mark II (Ian Gillan on vocals, Jon Lord on organ, Roger Glover on bass, Ian Paice on drums, and Ritchie Blackmore on guitar), especially on the album's opening track, "Sail On". But there are worse bands to sound like than Deep Purple at their most successful.

"Living for the Nighttime" is one of those songs where, if you're driving, you get a lead foot as soon as it starts playing. "Stiletto" is a hilarious ode to high heels, and another track worthy of Deep Purple. Then we've got songs like "Lady Jade", in which Björn evokes all those classic songs where some philandering rock musician declares his love for some woman.

If you're feeling nostalgic for the 1980s, this is the album for you. Hell, it's worth playing even if you don't have any fond memories of Ronnie Raygun's Reign of Terror.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A1Dj7Zh5R4VTZQn5hejYlfN" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## The Night Flight Orchestra is pretty good live, too.

See for yourself.

<iframe width="563" height="337" src="https://www.youtube.com/embed/DmCpB0-dmVM" frameborder="0" allowfullscreen></iframe>

#### Image Credits

Main image: [Night Flight Orchestra performing live in Helsingborg, Sweden.](https://www.youtube.com/watch?v=DmCpB0-dmVM)

Album Covers by Coroner Records and the Night Flight Orchestra.
