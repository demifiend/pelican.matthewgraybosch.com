title: I Played *The Legend of Legacy*. Avoid My Mistake.
date: 2016-02-28 08:00
category: Hardcore Casual
tags: 3DS, atlus, demo, fantasy, final fantasy ii, furyu, grind fest, jrpg, masashi hamauzu, minimal plot, nintendo, non-linear, old school, rngesus, rpg, SaGa, skill-based leveling


I recently played the demo for a 3DS JRPG with a charmingly generic title, [_The Legend of Legacy_](https://en.wikipedia.org/wiki/The_Legend_of_Legacy). I won't be buying it, and I don't recommend that you buy it either unless you can get it for at least a 50% discount off its usual forty-dollar retail price.

## More Context Than the Game Provides

Earlier this week I got it into my head to charge up my old 3DS, which I originally bought so I could play _Shin Megami Tensei IV_, and see if the console had any new JRPGs available that I could play while working the graveyard shift to stay awake between 2am and 7am when practically _nothing_ happens.

Why not just play some more MegaTen? Simple: I had already beaten SMT IV several times, and seen all of its four possible outcomes: Law, Chaos, Neutral (best ending), and Nihilism.

So, it was time for something new. Since [ATLUS](http://atlus.com/legendoflegacy/) had been kind enough to release a playable demo for _The Legend of Legacy_, I decided to give it a go.

I hadn't heard of its developer, FuRyu, but LOL was developed with the help of several Square Enix and Level 5 personnel, including composer Masashi Hamauzu (_SaGa Frontier II_, _Unlimited SaGa_, _Final Fantasy XIII_). Given its pedigree, I figured it would be halfway decent, so I downloaded it from Nintendo's eShop and gave it a shot.

Keep reading for information about the game and my experience playing the demo.

## The Lowdown on Legend of Legacy

_The Legend of Legacy_ appears to have been intended to serve as a spiritual successor to games like [_Romancing SaGa_](https://en.wikipedia.org/wiki/Romancing_SaGa) and [_Final Fantasy II_](https://en.wikipedia.org/wiki/Final_Fantasy_II), and this legacy (sorry!) is obvious as soon as you start the game and are asked to choose a primary character.

### A Cast of Seven

_The Legend of Legacy_ has seven possible protagonists, each with their own backstory:

* Owen the Berserk fanboy (seriously, that sword is at least 1.5x his height)
* Eloise the outrageously busty alchemist
* Filmia the singing frog prince (who's probably searching for the Masamune, in addition to the rest of his tribe)
* Meurs the elementalist whose name nobody bothered to try pronouncing
* Bianca the silver-haired amnesiac
* Liber the treasure hunter (Locke's distant cousin?)
* Garnet the faithful and steadfast templar

The player can select one of these protagonists as a main character. Others join as supporting characters. Any character can use any weapon, though some characters may have weapons with which they're especially proficient; this is not clarified for the player.

### Character Development is _Such_ a Grind

Characters have HP and SP, as well as separate levels for Attack, Guard, and Support roles/stances. In addition, each weapon has its own attack level, and each weapon technique and elemental charm also has their own levels, all of which can increase with use in battle.

HP determines how much abuse they can take before being KO'ed. If a character is KO'ed, healing them will bring them back, but with reduced max HP. Reduced HP limits can be restored to normal by resting at an inn. Losing HP in battle is a good way to get HP increases after a fight.

SP governs the use of weapon techniques and elemental charms. Characters recover SP at a rate of at least 1 SP per round in battle, can use items to recover in the field, or recover to max at an inn. Just as with HP, using up SP in battle seems to make a character more likely to get SP growth.

Weapon techniques work as one might expect in a SaGa game. Using them can improve them (and the Attack level in general). In addition, using weapon techs against powerful enemies (like bosses) can spark _new_ techniques.

Elemental charms appear to work along similar principles: repeated use increases power. However, to use an elemental charm the player must first be able to create a contract with the appropriate elementals of water, wind, or fire. Furthermore, some monsters can steal an elemental contract for their side. Of course, the player can steal it back.

### Journey Before Destination

The story is pretty bare bones. All seven characters came to the once-submerged island of Avalon for their own reasons, but how their individual quests play out depends on the player. The player has to explore areas and uncover the story and history of Avalon on their own in a manner similar to the lore and story of _Dark Souls_.

Battles can be unforgiving to the unprepared, but can often be avoided. In addition, the game provides a quicksave feature after the initial dungeon, allowing players to minimize risk while exploring. This is a good thing, because not only is _The Legend of Legacy_ is an old-school Japanese grind-fest, but it's easy to find yourself fighting impossible battles where even running away (and ending up at the entrance of the current dungeon) isn't an option.

## Spoilers Ahoy! Bianca in the Forest Ruins 

I did my playthrough of the _Legend of Legacy_ demo as Bianca just because I like cute girls with swords. Also, I want to see what ridiculous past the developers concocted for the poor girl since she has amnesia.

The game starts with Bianca waking up on a beach at dawn. She walks alone, and keeps walking, until she reaches the only town on the island of Avalon, a settlement called Initium. From there, she joins an expedition to some ruins in the forest sponsored by the King of Adventures, an exiled nobleman who first discovered the island of Avalon and encouraged other adventuresome folks to come, colonize, and explore.

Joining Bianca are Eloise the alchemist and Filmia the frog prince.

After exploring the clearing, Bianca and the others entered an area called the Singing Grove, where they eventually happened upon an ancient statue radiating song. Eloise and Filmia approach the statue to investigate, but a beast called the Dreadwing attacks the party.

Fighting together, the team overcome the Dreadwing. Bianca led the offense, improving her Cheap Shot technique and sparking the Crossover attack. Eloise acted as medic, using her Medicine Box to keep the others on their feet, and Filmia used his buckler to defend the others from the Dreadwing's onslaught.

Afterward, the statue reaches out to the three and offers a message and a vision from the past. They all hear the following:

> Thou art our link to the elementals.  
> Thou art our master.  
> We shall be gone, yet thou must remain here longer.  
> To our children, inheritors of this land, please pass on our memories.  
> Thou art our link to the elementals.  
> Thou art our king.

Bianca receives an item called the Singing Shard. Unable to understand their experience, the three return to Initium to consult the King of Adventurers and report their findings.

The King of Adventurers explains that the Singing Shard is special, and reveals its ability to forge contracts with nearby water elementals, enabling the use of water-based elemental charms in battle. He then gives Bianca the elemental scale, allowing her to gauge to concentration of elemental forces in the vicinity. After explaining its use, he also presents her with an encyclopedia so she can record her discoveries, and mentions several ancient mysteries of Avalon: the old gods, the Star Graal (Grail?), the City of the Unseen, and the forgotten light.

> "This island is full of legends, yet it's quite lacking in evidence! Are these stories made up&#8230; or are they real?"
  
> <cite>The King of Adventurers</cite> 

Looks like it's up to Bianca and her friends to root out the truth. To start with, Bianca feels a sense of _nostalgia_ from the Singing Shard, as if it might be connected with her memories. Filmia expects to find his kin somewhere on Avalon, and Eloise wants to get her hands on the Star Graal.

The Adventurer King commands them to rest and resupply in town before venturing out into the ruins and wilderness of Avalon to find their answers, and once you leave Initium, the opening credits roll&#8230;

If you forgot to save at the inn, you'll get a chance after the credits.

## Verdict: The Legend of Legacy is a Little _Too_ Old-School.

Look, I grew up playing old-school JRPGs. If you sat me down with a NES and a copy of the original _Dragon Quest_ or _The Legend of Zelda_, I could get through those games without punching up GameFAQs. While _The Legend of Legacy_ has a lot going for it, and could be FuRyu's flagship series with a few iterations, the first game has some real problems that limit its appeal.

### What _The Legend of Legacy_ Gets Right

* Character models are cute, and easily distinguished.
* Environments are gorgeous.
* Masashi Hamauzu's score sounds good on the 3DS hardware.
* Selling completed maps provides impetus to thoroughly explore areas.
* Limited voice-overs.
* Translation of in-game text to English is clean.
* The save point kitty in town is adorable.
* Being able to create a reusable quicksave everywhere encourages experimentation and risk-taking.
* Most selections in battle are retained from turn to turn.
* Holding down the A button enables auto-battle.

### What _The Legend of Legacy_ Gets Wrong

* Character advancement depends almost entirely on random chance.
* It's possible to spend half an hour on random encounters without getting stat gains or sparking new techniques.
* In battles, the game remembers the attack/spell/item you selected, but not the target chosen.
* Effect of stances on character advancement is not adequately documented.

### What I Would Have Done Differently

I don't mind a grind in a JRPG, but I never cared for the randomness of character advancement in _SaGa_-type games. At least with _Final Fantasy II_, you could measure a character's progress toward improvement with a given weapon type or magic spell. While gaining new elemental charms by exploring and finding statues works, but the system for learning new weapon techniques is unnecessarily obtuse in a decade where RPGs have skill trees and clearly defined paths toward advancement.

### Is _The Legend of Legacy_ Worth Buying?

It all comes down to whether you're willing to spend forty dollars on an old-school Japanese grind fest that will have you praying (and preparing burnt offerings) to [RNGesus](http://knowyourmeme.com/memes/rngesus) in the hope that your characters will improve every time you get into one of the game's many random encounters.

I don't think the game's worth buying at full retail. If you have an Android or iOS device, you'd be better off dropping eight bucks for a copy of [_Final Fantasy II_](https://play.google.com/store/apps/details?id=com.square_enix.android_googleplay.finalfantasy2).