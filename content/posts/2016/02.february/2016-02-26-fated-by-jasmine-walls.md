---
title: Fated by Jasmine Walls and Amy Philips
date: 2016-02-26 07:27
tags: amy phillips, comic, critical failure, critical hit, fabulously wrong, gay, human, jasmine walls, orc, role-playing, romance, tabletop

---
I saw [_Fated_ by Jasmine Walls](https://mythjae.wordpress.com/2016/01/28/fated-3/) on Reddit's r/fantasy subreddit. It's a hilarious short comic about a D&D session gone _fabulously_ wrong. Here's the first panel.

<figure>
  <img src="http://i0.wp.com/mythjae.files.wordpress.com/2016/01/fated-0012.png" 
  alt="Fated by Jasmine Walls and Amy Phillips, First Panel" class data-recalc-dims="1" />
  <figcaption>
    <p>Fated by Jasmine Walls and Amy Phillips, First Panel</p>
  </figcaption>
</figure> 

It just gets worse. [Check out the rest](https://mythjae.wordpress.com/2016/01/28/fated-3/). It's good stuff.