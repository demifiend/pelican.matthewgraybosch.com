title: Hedy Lamarr &mdash; Actress, Inventor... and Indie Band?
summary: Actress and inventor Hedy Lamarr was never just a pretty face, and neither is the namesake Euro-pop band from Rome.
og_image: /images/Hedy-Lamarr-Band-Facebook.jpg
category: Metal Appreciation
tags: amplitudeness, cabaret, euro-pop, hedy lamarr, indie, italy, noir pop, rock, solange mattioli, umberto duca


You might have heard of Hedy Lamarr, the actress who starred in the first non-pornographic film to depict intercourse and female orgasm ([_Ecstasy_](https://en.wikipedia.org/wiki/Ecstasy_(film)), 1933). You may also know that the frequency-hopping spread-spectrum technology she helped invent during WWII is the basis for modern networking tech like wifi and Bluetooth.

If not, maybe this 1944 photograph will jog your memory.

![Hedy Lamarr in The Heavenly Body (1944)](/images/Hedy_Lamarr_in_The_Heavenly_Body_1944.jpg)

## More Than a Pretty Face

Hedy Lamarr was a hell of a lot more than just a pretty face in life, but this isn't about the lady. Tonight, I'd like to introduce you to an Italian indie pop/rock duo that takes its name from the legendary Austrian-American actress.

![Solange Mattioli (left) and Umberto Duca (right) are Hedy Lamarr](/images/Hedy-Lamarr-Band-Facebook.jpg)

That's right. Umberto Duca (bass, guitar, programming, keyboards) and Solange Mattioli (keyboards, vocals) _are_ Hedy Lamarr, the band. I heard about them after they (or whoever handles social media for them) added me to their circles on Google+.

It's a good thing they did, because I might otherwise have missed out on a good album. You might have missed out as well. 😼

[![Hedy Lamarr, Amplitudeness (2016)](/images/Hedy-Lamarr-Band-Amplitudeness.jpg)](https://soundcloud.com/hedylamarrtheband)

## A Review of Hedy Lamarr's _Amplitudeness_

Naturally, the real question is what does Hedy Lamarr sound like? I'm tempted to suggest after four playthroughs of their 2016 debut album _Amplitudeness_ that Hedy Lamarr is more pop than rock, but they don't do the sort of vacuous bubblegum pop that gets foisted upon unsuspecting teenagers. Instead, Hedy Lamarr is intelligent pop with real musicianship behind it.

Imagine if [Edith Piaf](https://en.wikipedia.org/wiki/%C3%89dith_Piaf) had been born forty years later, and were to collaborate with [Peter Gabriel](https://en.wikipedia.org/wiki/Peter_Gabriel) or [Thomas Dolby](https://en.wikipedia.org/wiki/Thomas_Dolby). The result might resemble Hedy Lamarr's music.

The eight songs comprising _Amplitudeness_' 37 minute runtime are subtle affairs, with Umberto Duca's instrumentation and programming supporting Solange Mattioli's vocals. Ms. Mattioli's voice is clear and commands the listener's attention whether she sings in English or in French (as on "Formes Rudes" and "Larves"). Whoever produced the album is to be commended; they don't appear to have bought into the [Loudness War](https://en.wikipedia.org/wiki/Loudness_war), and _Amplitudeness_ is all the better for its soft, lush sound and generous dynamic range.

Hedy Lamarr's promotional efforts appear to focus on their single, but I think "Slow" is the stand-out track. It's a sensual, jazz-influenced number that just drips _film noir_. Now that I think of it, pop/rock doesn't accurately describe the Hedy Lamarr aesthetic. Just about every song on _Amplitudeness_ would fit somewhere on a modern noir film's soundtrack, or in a 1940s cabaret anachronistically equipped with modern tech, so perhaps "noir pop" is a better term. Regardless, it's an album I recommend for quiet, rainy nights spent alone in the city.

## Listen to _Amplitudeness_ on Spotify

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A445QdtpPkSs1nf6QMkDQeg" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## "I'm Over" official video clip

This _may_ be NSFW. I'm at work so I haven't played it yet. 🙂

<iframe width="563" height="337" src="https://www.youtube.com/embed/ypVo6p478vc" frameborder="0" allowfullscreen></iframe>

## Following Hedy Lamarr

If you want to follow the band, use the following services:

  * [Facebook](https://www.facebook.com/hedylamarrtheband)
  * [Twitter](https://twitter.com/hedylamarrband)
  * [Instagram](https://www.instagram.com/hedylamarrband/)
  * [Google+](https://plus.google.com/113763888295082222043)

#### Photo Credit

Top image taken from the [Facebook page](https://www.facebook.com/hedylamarrtheband/) for [Hedy Lamarr, the band](https://www.facebook.com/hedylamarrtheband/photos/pb.402048579822520.-2207520000.1456468004./1223617280998975/?type=3&theater).
