title: Playing Paul Kantner's *Blows Against the Empire*
summary: To mark the passing of Jefferson Airplane founder Paul Kantner, I've been playing this classic album.
summary_only: true
date: 2016-01-29
modified: 2016-01-31
category: Metal Appreciation
tags: classic rock, concept album, grace slick, hippies, jefferson airplane, jefferson starship, jerry garcia, paul kantner, protest music, robert heinlein, rock, signe toly anderson
og_image: /images/paulkantner-blowsagainsttheempire.jpg


Paul Kantner is dead at the age of 74. It hasn't hit me the way [Lemmy's](http://www.rollingstone.com/music/news/lemmy-kilmister-motorhead-singer-and-heavy-metal-legend-dead-at-70-20151228) passage did, but I suspect it'll be worse for my father. He grew up with Paul Kantner, Grace Slick, Jefferson Airplane, and Jefferson Starship.

In fact, Kantner and company were part of the musical inheritance my father passed to me along with King Crimson, ELP, Genesis, Mountain, Hot Tuna, the Jimi Hendrix Experience, and many other classic bands from the late 1960s and 1970s.

I've even seen Kantner perform live in New York City. It was the summer of 1992, and we had driven from the middle of Long Island into Central Park to attend an outdoor rock festival headlined by Jefferson Starship.

If I were to get my father on the phone, he'd talk about that show and others he'd seen. And I suspect he would talk about a 1970s concept album called *Blows Against the Empire* that Kantner recorded with Grace Slick, other members of Jefferson Airplane, Jerry Garcia of the Grateful Dead, and members of Crosby, Stills, Nash, and Young.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A4auMzhj0E7BHPNLgTHoJjA" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

*Blows Against the Empire* is very much a hippie album, and my father used to play "The Baby Tree" for my mother all the time. Yes, you should take the title literally; it's a folk-style song that's just Paul Kantner on vocals and Jerry Garcia on banjo, and it's about an island where babies grow on trees and happy, grateful parents pick them like fruit. But they only take the smiling, happy babies.

Yeah, that's pretty fucked up when you think about it. The parents in "The Baby Tree" only want the easy, pleasant babies. What happens to the others? Well, Grace Slick suggested in "White Rabbit" (_Surrealistic Pillow_, 1967) that we should go ask Alice&#8230;

---

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A732KDQdZHJIXbOxqg1pgVM" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A4vpeKl0vMGdAXpZiQB2Dtd" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

---

And now I want a production of *Alice in Wonderland* with Alice Cooper as the Cheshire Cat. When Alice first meets him, he can sing "Welcome to my Nightmare."

![Original illustration by John Tenniel](/images/alicecooper-cheshirecat.jpg)

Sorry, that was a bad joke and I couldn't resist.

Getting back to Kantner and _Blows_: the first track, "Mau Mau (Amerikon)", is both a protest song and an introduction to the album's story. The counterculture is sick of life on Earth, and has heard of a starship that's currently under construction. They're gonna hijack it and search for a new home somewhere out in space.

The next few songs are from the viewpoint of a hippie couple. After doing acid ("The Baby Tree"), the couple hear tell of the starship under construction and dream of leaving Earth and America behind ("Let's Go Together"). The dream takes on urgency in "A Child is Coming"; neither of the hippies want Uncle Samuel getting is hands on their child and sending him or her off to war.

"Hijack" continues the story, describing a counterculture living in a dystopian mid-1980s America. A starship has been under orbital construction since 1980, and it will be ready to launch in 1990. The hippies want to hijack it and leave Earth behind. "Have You Seen the Stars Tonight" is mostly instrumental. After another interlude ("X-M"), the album reaches its climax with "Starship". Humanity has left its birthworld behind and is now free.

The funny thing is that I didn't know Paul Kantner was dead at first. Earlier tonight I saw this [Google+ post](https://plus.google.com/+RichardBensam/posts/TRihtpsZFxp) by Richard Bensam where he showed an excerpt from a letter Robert Heinlein wrote to Paul Kantner. Apparently Kantner wanted to used ideas and imagery from _Methuselah's Children_. All Heinlein wanted was a signed copy.

It was after that that I saw that [Paul Kantner](https://plus.google.com/u/0/+SusanStone/posts/XfSvUeKQczC) was dead.

If you aren't familiar with the man, or with Grace Slick and the rest of Jefferson Airplane/Jefferson Starship, now's as good a time as any to feed your head. And if you have any memories, you're welcome to share them in the comments.

![Cover for "Blows Against the Empire"]({{ base_path }}/images/paulkantner-blowsagainsttheempire.jpg)

## Update: 31 January 2016

It seems that [Signe Toly Anderson](https://classicrock.teamrock.com/news/2016-01-31/jefferson-airplane-founder-anderson-dead-at-74), a vocalist and founding member of Jefferson Airplane succeeded by Grace Slick after the band's first album, died in hospice the same day as Paul Kantner.
