*Blackened Phoenix*, Chapter 2: Between Knowledge and Belief (Scene 2)
######################################################################

:date: 2016-01-08 16:43
:summary: In this flashback, we see from Morgan's eyes the end of his relationship with Christabel. He knew, but did not believe.
:category: Serials
:tags: morgan, christabel, flashback, hotel, drunkenness, attempted murder, suicide, suicide by cop, desperation, escape
:summary_only: true
:og_image: /images/starbreaker-adversary-symbol.jpg


In this flashback, we see from Morgan's eyes the end of his relationship with Christabel. He knew, but did not believe.

Winter Solstice 2111, London, the Hellfire Club…
================================================

Morgan Stormrider narrowed his eyes as Christabel staggered into his hotel room, and checked his surroundings for weapons. The only one was a short thrusting weapon intended for use as an Adversary's service blade. Morgan wore it as a dress sword, but as it was the only weapon in the room he regretted leaving in the closet with his coat and boots.

It was too late to retrieve it now. Doing so would require that he get past Christabel, whose drunken sway and hateful glare lent her the aspect of a malevolent willow bending to winds that blew out of one direction and then another. "You just couldn't wait to get away from me at the post-performance party. Hell, I'm surprised Naomi isn't here in bed with you--or is she hiding in the loo?"

The accusation was too much for Morgan. "Funny how the cheater accuses their partner of cheating on them, isn't it?"

Christabel's glare grew more malefic. "What are you crapping on about?"

Morgan shrugged. If Christabel wanted to be with Isaac Magnin, he was happy to let her go. It wasn't like he owned her, and an end to their now utterly dysfunctional relationship would simplify both their lives. *Besides, why should I fight to keep Christabel? It just isn't worth it to me any longer, and maybe it never was.* "Just turn around and walk out, Christabel. Go back to Isaac Magnin. Be happier with him than you were with me."

"Ohhhh, that's *so* generous of you. You're such a bloody martyr, you know that?" Christabel advanced on Morgan, and slapped him across the face. "Does *that* hurt? Does *that* make you angry? Jesus Christ, Morgan, I did everything short of deep-throating Magnin at that party but you don't care. You *can't* care, just like you can't feel pain when I bitch-slap you, because you're just a fucking robot."

"I'm just a robot? Is that what you really think of me?" Morgan regretted the question as soon as it escaped his lips. *Eddie and Claire and Sid kept telling me I should get away from Christabel and stop sticking my dick in crazy. And trying to make sense of crazy is just as pointless.*

"Magnin *made* you, did you know that? He made you to be a demon-ridden meat puppet. He told me everything when he hired me and trained me."

Morgan kept an eye on Christabel as he opened the wet bar and retrieved a small bottle of gin for which the hotel would charge him double the price of a full-sized bottle. "You aren't drunk enough to talk like that. Gin and tonic?"

Christabel blinked at the offer. "Yes, please."

Though Morgan suspected he'd regret helping Christabel *stay* drunk, he hoped the offer of a drink might allow them to continue the discussion in something resembling a rational manner. "Here."

He sipped his own, despising the bitterness on his lips, as he waited for Christabel to finish hers. He did not want to ask the question her last statement raised without first making her his guest, since they no longer seemed bound by any other relationship. "Christabel, you said Isaac Magnin hired you, and trained you. When did he hire you, and for what purpose?"

"He hired me to seduce you. I was supposed to become part of your life, part of your inner circle, and report my observations of your behavior. I was supposed to gather intelligence that couldn't be gleaned from Witness Protocol data. He wanted to know what you were thinking, what you were feeling. He wanted to be sure you had at least one relationship in your life that he could use to control you."

"And why would Isaac Magnin, who has stakes in the AsgarTech Corporation and Ohrmazd Medical Group and is probably either a member of the Phoenix Society's executive council or an XC member's golfing buddy, need to control *me*?" The very notion of somebody like Magnin using Christabel as a spy seemed laughable to Morgan, but he didn't want to laugh at her. It might provoke her, and his dress sword wasn't where he had left it. *Had she gotten her hands on it while I was making drinks? And why has she taken off her coat and draped it over her lap like that?*

"I don't know. He wouldn't tell me. But I trusted him because he offered me a chance to be more than I was. He told me I could be an actor in history, but all I am is second fiddle to you and that ghost bitch Naomi."

Here was the perennial complaint, and it took a herculean effort on Morgan's part to keep from rolling his eyes. "Oh, come off it. You've been complaining that Nims and I overshadowed your contribution to the band you named after your alleged ancestor, that old fraud Aleister, ever since we put out Prometheus Unbound. Never mind that it was *your* idea to turn Shelley's poem into a rock opera."

"But the audience doesn't care about *me*!"

"Why the hell should they? Do you write any of the music? Do you write any of the lyrics? Do you give interviews, or interact with fans."

"I design the clothes we wear on stage. I come up with the visual imagery."

"Naomi and I explain that to the media, but coming from us it sounds like we're just trying to explain why we haven't said ‘fuck it' and replaced you with a fiddler who can be bothered to *show up on time*."

"I—"

Rather than let Christabel make excuses for herself, Morgan pressed on. "Speaking of showing up, there's a reason people who take music seriously think Naomi and I sound good together, and why they think we cover for you. We actually get together and *rehearse*.

"Oh, so you bring your instruments to bed?" Christabel was in her feet now, her coat held before her.

Before Morgan could put his drink aside, Christabel threw her coat at him. He swiped it aside in time to see the point of his sword speeding toward his chest with just under fifty kilos of angry, drunk, untrained woman behind it.

Because it was a smallsword, an edgeless weapon designed exclusively for thrusting, Morgan had no trouble sidestepping the point, grasping the blade at its base, and wrenching the weapon from Christabel's hand. He flipped it, caught the hilt in midair, and pressed the tip to Christabel's throat as she turned to face him again. "Is this what you wanted?"

"I want out. I'm tired of being Christabel Crowley. What you saw at the party, me with him, is part of the act too. I want to go back to being myself. But Magnin won't let me go."

Morgan considered the coat he had batted aside. Christabel had thrown in his face to give herself a decent chance of a successful thrust with the sword he now held on her. That she thought to do so suggested that this was hardly an impulsive attempt at suicide by Adversary. "Are you so desperate for escape you believe your only hope is death by my hand?"

Christabel's smile was as bitter as her tone. "Everybody knows the reputation you cultivated. If I attacked you and you put me down, I'd be free of Magnin." She looked away, her eyes welling. "Please. I need to get free of him. You're the only one who can help me."

Morgan shrugged. "You should have thought of that before you came at me with my own sword."

"If you love me—"

Morgan hardened himself. "I don't. Maybe I never did. Am I not a mere robot, incapable of emotion?"

"You bastard. Don't you realize that Magnin is seeing everything through my own eyes? He'll know I've destroyed our relationship. He'll have no further use for me."

"That's not my problem, Christabel. We're through. You're tired of pretending to love me, and I'm tired of trying to love somebody who thinks I'm just a machine who fakes it to fit in." He smiled at her rising panic as he spoke. "Don't worry. I'm not leaving Crowley's Thoth until we've finished the upcoming tour for Glass Earth Falling."

"Always the trouper. Why can't you at least hate me?"

For the first time since Christabel's arrival, Morgan smiled in genuine pleasure. "If you weren't shitfaced you'd realize I think you're beneath contempt. But I'm human enough to want revenge for the time you've stolen from me, and here's how I'll go about it. Every cruel word, every blow, every slight--it's all forgiven, *because* I despise you."

13 February 2113, West 96th St., Manhattan…
===========================================

Morgan sat back down on the sofa he had been sharing with Naomi earlier, and met the eyes of Eddie, Claire, Sid, Sarah, and Astarte in turn before continuing. "I hadn't believed a word of what Christabel was saying, but if she kept diaries going back to before I had even met her, that alone would be strong evidence that she wasn't living out an elaborate delusion."

Sarah snorted. "Maybe, but I think you're dressing it up just a bit. Don't you think some of the shit you said to Christabel was just a *bit* over the top?"

"I don't." Claire chuckled. "I love Morgan almost as much as Naomi does, but he *can* be a drama queen."

Sid reached over and stole the last handful of popcorn from Claire's bowl. "So, where does that leave us? It seems obvious that Magnin offed Christabel because she had become a liability to him. He just waited before doing it right away would have been too obvious."

"It seems a reasonable inference, Sid, but the last entries in her diary suggest that he was pleased with her work, and that Christabel *wasn't* afraid of him." Naomi held up the slim, leather-bound notebook she had been reading as Morgan related his story. "Instead, she seemed to think that Magnin had some elaborate escape planned."

"Shit." Edmund coughed around his pipe. Taking it from his mouth, he pointed it at Morgan. "Sorry to have to say this, bro, but what if Magnin staged Christabel's death to give her an out?"

Author's Notes
==============

I had showed the breakup of Crowley's Thoth, the neo-Romantic heavy metal power trio that had Christabel Crowley on violin and viola, Naomi Bradleigh on lead vocals and keyboards, and Morgan Stormrider on guitar and backing vocals (with unnamed guest musicians handling bass and drums) in *Without Bloodshed*, but I never showed how the romantic relationship between Morgan Stormrider and Christabel Crowley fell apart a year before the band's dissolution.

My wife Catherine is concerned that by revealing that Christabel had confessed to Morgan that Isaac Magnin had hired and trained her to play Mata Hari I would not only undermine Morgan's motivation for going after Isaac Magnin, but undermine Isaac Magnin's motivation for removing Christabel from play in the manner that he did.

However, I think she (and you, the reader) will find that I've only given Morgan additional motivation. Might he have saved a life if he had put his own pain and anger aside and *listened*?
