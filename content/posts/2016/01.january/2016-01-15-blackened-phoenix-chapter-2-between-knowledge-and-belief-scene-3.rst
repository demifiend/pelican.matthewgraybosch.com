*Blackened Phoenix*, Chapter 2: Between Knowledge and Belief (Scene 3)
######################################################################

:date: 2016-01-08 16:43
:summary: Morgan and Naomi clear the air, but Claire interrupts their reconciliation with news of a friend in danger.
:category: Serials
:tags: morgan, naomi, christabel, claire, josefine, polaris, witness exraction, consent, rough sex, bdsm, safeword
:summary_only: true
:og_image: /images/starbreaker-adversary-symbol.jpg

Naomi snuggled close to Morgan as he settled into bed beside her. Since he was on his side, facing away from her, she spooned with him. She did not mind; he spooned with her often enough and turnabout was fair play, though he usually responded to her caress. *His mind was elsewhere while we made love, too.*

Kissing his shoulder, she grazed her teeth against his skin in manner that usually made him shiver with pleasure even if he was recently spent. His only response was a sigh. Dissatisfied, Naomi gently turned Morgan onto his back and draped her body over his so that she could gaze into his green eyes. "Oi. What are you thinking?"

"Am I that transparent?" Despite the mild exasperation in Morgan's tone, his hands on her back were gentle.

Naomi settled against Morgan, her head resting on his shoulder. "I was worried you might be upset with me. I didn't exactly give you the benefit of the doubt today."

"Did it feel like I was still upset with you earlier?"

"It wasn't your attentions I found lacking, but rather your response to my efforts. It's a bit early for lovemaking to become a chore, don't you think?"

Morgan kissed her, one hand plunging into her hair while the other slid down her back and slipped into her knickers to mold itself to the curve of her bottom. He did not let up until she was breathless. "Is that more like it?"

Naomi shifted so that his thigh was between hers, and sighed as Morgan lifted his leg to add pressure. "That's much better. Now get out of your own head and talk to me. If things are good between us, what's eating you?"

"Telling you all how I ended things with Christabel brought back a lot of bad memories. All the times she hurt me, and the times I'd let my control slip and hurt her in return."

"Like the time you bent her over the vanity in her dressing room and fucked her until she begged for mercy?"

Morgan froze beneath her. "She told you about that?"

"No. I saw you with her." The memories flooded her imagination, and with them came the wish Morgan would manhandle *her* in similar fashion. "She isn't exactly discreet when she lays into you, so I heard her provoke you before I saw you grab her and kiss her harder than you've ever kissed me. I heard you tell her her safe word was 'coda' before asking if she was sure she wanted to go farther. It wasn't like you forced yourself on her; you asked for her consent and she gave it. If she had said 'coda' I think you would have stopped immediately, but she didn't."

"She was bleeding when I was done with her."

"Your fingernails were digging into her skin, and it wasn't like you didn't take care of her afterward." Naomi kissed him, and held his gaze. "I checked up on her afterward, and trust me when I say she was fine, albeit shocked that you had it in you. If I had *any* doubts about Christabel's consent, I would have confronted you with sword in hand. I would have testified against you, and I wouldn't have rested until you were convicted and sentenced."

"Once an Adversary, always an Adversary?"

"Damn right." Naomi nipped Morgan's shoulder. "Now, are you worried that you might get too rough with me, and hurt me?"

Morgan shrugged beneath her. "Well, we've never had angry sex before."

"I'm not suggesting we do. But I *liked* what I saw, and you might have noticed that despite my complexion I'm not made of porcelain."

"What?"

She nipped Morgan again, her teeth grazing his throat, and lifted herself off him. She slipped out of bed, grabbed Morgan's shirt off the floor, and began buttoning it around her. "Here's something to keep in mind next time we're sparring. I might just overpower you and *take* what I want. You're welcome to do the same—if you can."

Before she could say anything else, her hands were braced against Morgan's desk and she was gazing at the sculpture of Cecilia Harvey from *Last Reverie IV* she had given Morgan as a Winter Solstice gift a few years ago. The gift had been a silly whim, a way for her to watch over Morgan as he slept because people used to say she resembled the character.

Morgan's hands were rough on her, and his voice rougher. "What's *your* safe word, Nims?"

"Shield." The word escaped her lips in a whisper barely audible over the rustle of her panties sliding down over her thighs. Despite the aggression with which Morgan drove himself into her, she did not repeat the word for it would have halted their play.  

A knock at the door broke the silence with which Morgan used her. "Fuck off. We're busy."

Rather than obey, whoever sought to interrupt them redoubled their efforts. "Dammit, Morgan, it's important. We need to talk."

Moved by the frantic tone with which Claire pleaded outside, Naomi fought free of the haze that had settled over her mind as Morgan's relentless pursuit of his own pleasure drove her to similar efforts. She reached back, touching the hand with which Morgan grasped a fistful of her hair. "Shield."

True to his word, Morgan immediately stopped. "Shit. Claire actually sounds upset."

"I know." An ache of frustration radiated outward from her lower belly as she lifted her panties back into place; she had been so close. "This better be damned good."

Morgan only put on his shorts before opening the door, and made no effort to hide the fact that he had been left hanging. "What the fuck is it, Claire?"

Claire began babbling, and out of the stream of words Naomi plucked a name. "Slow down. Take a breath. Tell us about Josefine. Is she one of your lovers?"

"I'll be back in a moment." Morgan ducked out as Claire sat on the edge of his bed.

Naomi sat beside her, and put an arm around the younger woman's shoulders. "You can tell us what happened, and we'll take care of it."

"But Morgan just scarpered."

"And now I'm back." Morgan, no longer straining against his boxer briefs, offered Claire a bottle of Iron Rider IPA. "Have a beer and tell us what's going on. You mentioned a Josefine. Girlfriend?"

Claire drank half her beer in one go. "I wish. Astarte? Could you please pull and display Josefine Malmgren's public profile? We want the one who lives in Asgard and works for the AsgarTech Corporation."

"AsgarTech?" Morgan's voice sharpened with interest. "Claire, you have a friend who works for AsgarTech? How come you never told me?"

"I had hoped to leave her out of this." Claire shook her head as a profile with photos appeared on the wall-mounted screen. The photos were all of a petite, bespectacled European woman with mousy brown hair and a powder-blue cardigan at least a size too large for her. The profile listed an impressive list of credentials and honors, and identified Isaac Magnin as her patron.

*Bloody hell.* Naomi caught Morgan's glance, and saw the same understanding in Morgan's eyes. They had campaigned to ban the patronage system that had cropped up after Nationfall, under which wealthy individuals or organizations identified gifted and talented children and sponsored their accelerated education. The Phoenix Society itself had served as patron to both Morgan and Naomi, sponsoring their education in exchange for four years of training to become an Adversary and a minimum of two years active service in the IRD corps.

In practice, having a patron all too often meant having an owner, and thus was too close to chattel slavery to be tolerated under the Universal Declaration of Individual Rights. However, individuals who had existing patrons were allowed to keep them if they wished. "Why is Dr. Malmgren willing to keep Isaac Magnin as her patron?"

Claire flashed Morgan a bitter smile. "I've asked her that a few times myself. I think she's at least a little bit in love with that white-haired, rent-seeking, rat bastard bishounen. You'll probably get to ask her yourself."

"What do you mean? Is Dr. Malmgren in danger?" Naomi suspected as much; she doubted that a committed hedonist like Claire would dare interrupt others' pleasure for any other reason.

Claire sighed. "She might be. Listen, Morgan. You know how Desdinova said you're some kind of replicant or android called an asura emulator, and that you and six hundred and sixty-five others were made as some kind of fucked up experiment to make a better Adversary?"

Morgan nodded. "I remember, but I still don't like to believe it."

"Well, the AsgarTech Corporation got their hands on the designs, and are improving on them. Isaac Magnin is working on creating a new set of asura emulators." Claire paused, as if for effect. "Josse's the lead the developer on the OS driving the AI. These new asura emulators don't start out as baby replicants, but as adults with artificial personality constructs. The prototype was activated while we were in Boston. Polaris knows he isn't human."

Morgan began selecting fresh clothes and dressing. "And you want me to protect Dr. Malmgren from the monster she made?"

"That's a bit harsh." Naomi turned to Claire. "Did Josefine ask you to arrange protection?"

Claire shook her head. "No. She just told me she was worried that she no longer had a hold on Polaris."

"Given that I'm a walking security hole outside my own home, Claire, why would you have me travel to Asgard to rescue this person?"

"She's my friend, Morgan." Claire met his gaze, and held it, her voice hardening. "That aside, I think she could be a huge help. Josse has access to the original asura emulator specs and firmware source code. With her help, we can patch your operating system and get Isaac Magnin's eyes out of your head."

Morgan remained silent as he continued to dress, and studied Claire with a cold, calculating expression that disconcerted Naomi. "Morgan, Claire's right. Dr. Malmgren could be a valuable witness, especially since she's highly placed within the AsgarTech Corporation."

"Don't worry, Naomi. I'm going. I'm just waiting for Claire to tell me the rest. There has to be some crucial detail she's holding back, the way she never bothered to mention that she had a close friend that Isaac Magnin could use to get leverage on us."

Claire reddened at this, and while Naomi sympathized she also grasped the justice in Morgan's remark. Claire, however, immediately struck back. "You know, if you'd learn to forgive yourself you might have an easier time forgiving your friends when they make an honest mistake. Yes, I should have mentioned Josefine to you. I didn't because I feared doing so might have given Isaac Magnin a hint of our own strategy."

"And if Magnin knew we were interested in Malmgren, he might move against her himself." Morgan's expression softened, and he drew Claire into a brief hug. "You're right. That's a reasonable concern. Now, how am I supposed to get to Malmgren without leading Magnin to her?"

Claire broke into one of her classic sunny smiles, and pulled a pair of small devices the pockets of her bib overalls. The one she tossed to Naomi had a crude picture of a submarine diving drawn on it, and a name: U-Stealth v0.0.2.

Once Claire finished her beer, she took a deep breath and started talking in the rapid staccato cadence she used when explaining tech to non-specialists. "Astarte helped me with these. When you fire up the U-Stealth, it will emit a sphere of EM interference one meter in diameter on the 60Ghz band to keep you off the public net. While it's doing that, it will create a personal area network for your implant on the 2.4Ghz band so that Witness Protocol doesn't switch to caching mode, and provide a proxy for secure talk and secure relay chat. Don't worry about batteries; it draws power off your body's bioelectromagnetic field."

Naomi turned her U-Stealth over in her hands. "If Morgan and I are the only ones who have these, won't Magnin be able to track us by listening on the 2.4Ghz band?"

Claire smiled at Morgan, and punched him in the arm. "You have a delightful tendency to surround yourself with women smarter than you. Don't worry. I'm just taking the piss."

She pulled out another U-Stealth, and giggled. "I got one, too. So do Eddie, Sid, and Sarah. I shared the designs and source code on Port Royal so anybody who wants one can nip down to the local makehaus and rent time on a fabricator. If the the activity on my announcement thread is any indication, Isaac Magnin is going to have a metric fuckton of network outages to track."

Morgan slipped his device into a pocket. "So, the Age of Sousveillance is over. I suppose Adversaries will have to do actual detective work now."

"Not quite." Claire's smile turned impish. "I thought of that. U-Stealth won't work for public officials, corporate executives, clergy, or active-duty Adversaries. You can also retrieve Witness Protocol video from your U-Stealth with your implant. Review the footage, transmit what's relevant, and delete the rest. The device can only be accessed from your implant once it's paired."

Morgan flashed a predatory smile that wouldn't have been out of place on a pirate's visage. "Excellent work, Claire. I knew you'd come through for me eventually. I owe you."

"Nims, could you break up with Morgan for like ten minutes so he can repay me properly? I'll give him back. I promise."

"Ten minutes? Don't insult him like that." Unsure of whether it was because of the hacker's audacity, or her own, Naomi laughed. "Morgan, do you want me to come to Asgard with you?"

He answered her with a searing kiss that left her hoping he would say yes, hoping he would get a private compartment on the maglev to Asgard, and suspecting he would insist on going alone. "It's tempting, but even with U-Stealth we might be noticed if we go together. So I'll go alone, and incognito. Besides, I have something else in mind for you and Sid, if you two are willing."

Author's Note
=============

So, this has been an *interesting* scene, since we go from Fifty Shades to James Bond meets Cryptonomicon. However, it sets the stage for me to do some interesting shit plot-wise by splitting up the cast and getting them out of Morgan's brownstone.

Also, I've broken the 10,000 word milestone. We're currently at 11,533 words. Still a long way to go, but now I've got some momentum going.

There's going to be a whole lot of shit happening at roughly the same time in multiple time zones for the next few chapters. Here be spoilers:

* Morgan finds Josefine Malmgren, fights off Polaris, and escorts Josefine to a safe location.
* Naomi and Sid Schneider hit the headquarters of Murdoch Defense Industries and find out what's up with those devil-killer rifles Alexander Liebenthal was selling in *Without Bloodshed*.
* Munakata Tetsuo seeks the truth about Alexander Liebenthal's fate, and finds it -- along with Thagirion, who has a job for him.
* Edmund Cohen begs Desdinova to give Morgan a means of finding out the truth for himself and possibly unlocking his full potential as a flowseeker.
* Sathariel sneak into the Antarctic subterrane while following Adramelech, and find that not only does Polaris now have the Starbreaker, but he's sworn himself to Sabaoth's service.
* Ashtoreth, armed with the intelligence Sathariel gathered, warns Imaginos that his plan of goading Morgan and testing him to near-destruction could cost their best shot at getting the Starbreaker back and keeping Sabaoth contained until Morgan is ready to unleash the weapon and kill the ensof.

After all this shit happens, we have Josefine telling Morgan and the others her story with scenes from Polaris' viewpoint interspersed to set up the story's climactic showdown. It's gonna be *heavy*.
