Queensrÿche: *Condition Hüman* (2015)
#####################################

:date: 2016-01-19 00:00
:summary: I wanted to like Queensrÿche's *Condition Hüman* but this album named for the human condition lacks humanity.
:category: Metal Appreciation
:tags: queensrÿche, condition hüman, seattle, usa, progressive metal, 2015, meh
:summary_only: true
:og_image: /images/queensryche-condition-human.jpg

I really wanted to like *Condition Hüman*, the 2015 release by veteran Seattle progressive metal act Queensrÿche, but I can't. I can't hate it, either. I can't manage much of a reaction to the album at all. That's not good for an album whose title is a play on the phrase `human condition`__.

.. __: https://en.wikipedia.org/wiki/Human_condition

The music is competently performed, and the production is faultless. It's solid progressive metal, but every song sounds like the band's just meeting contractual obligations. It feels like there's no soul or humanity in this music.

I think the problem is that I keep hoping that each new Queensrÿche album will hit me the way *Rage for Order* (1986) and *Operation: Mindcrime* (1988) did when I was a teenager, and it keeps not happening. It isn't even another *Empire* (1990).

Hell, it sounds almost like a Queensrÿche revival band, and not like the actual Queensrÿche. But maybe I just need to keep listening and let it grow on me.
