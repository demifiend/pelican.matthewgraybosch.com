*Blackened Phoenix*, Chapter 3: Walk in the Shadows (Scene 1)
#############################################################

:modified: 2016-01-26 04:37
:summary: Before Morgan can get Dr. Josefine Malmgren out of Asgard and escort her to safety, he has to earn her trust. That might be the easy part.
:tags: morgan, josefine, polaris, witness, extraction
:category: Serial
:date: 2016-01-22 00:00:00


Morgan crouched at the edge of the rooftop over AsgarTech Plaza, scanning the crowd through his binoculars. His head and the back of his neck burned beneath the klieg array that served as the domed city of Asgard’s artificial sun, and Morgan regretted letting Claire and Naomi talk him into shearing off his mane to alter his appearance.  The array had been calibrated to approximate the warmth and brightness of a temperate sun from street level. At half a kilometer from the street, the heat was anything but temperate.

Not that Mordred minded. The cat sprawled on his back, purring as his belly soaked up the light and heat. Morgan shook his head and rubbed Mordred’s belly. “Didn’t I leave you in New York?”

Mordred flopped onto his side, and slowly blinked at his human. “Mmrrrraow?”

“Fine. Just don’t get underfoot.” Morgan sighted on the crowd below again and texted Claire. «Mordred and I are in position.»

«I’ll tell Naomi. She was wondering where the cat had gone. It's like he walks through walls or something.»

Rather than dwell on the times Mordred cock-blocked him, Morgan stuck to business. «Does Dr. Malmgren know to expect me?»

«Yeah, but there’s something you’ll have to say so she knows I’ve sent you.»

Morgan braced himself. Knowing Claire, her authentication phrase was bound to be egregious. «This better not be an another Aleister Crowley quote.»

«Nope. Just gotta say, “Come with me if you want to live.”»

Morgan shook his head, and forced himself to muster a shred of gratitude that Claire had at least chosen a line appropriate to the situation. «Do you have updated intel on what Dr. Malmgren’s up against?»

A series of photos appeared in Morgan’s view. All were of a man of similar height and build to himself. In some of the photos he wore a flowing black mane. In others, his hair seemed iridescent, as if each strand was a fiber-optic cable. Claire’s explanation soon followed. «Josefine can tell you more, but apparently Polaris’ personality template is based on you. Any knowledge you possessed when the template was generated is also his.»

«So, I had to cut my hair because the new model might be impersonating me?»

«The technicolor hair is a new twist. It’s almost as if he’s embracing his inhuman nature.»

Removing the photos from view, Morgan returned his attention to the crowd.  Another pass showed nothing, which did nothing to ease Morgan’s annoyance as a new photo sprang into view. This one was of a pallid young blonde wrapped in a powder blue cardigan that threatened to swallow her up. A message from Claire soon followed. «Looks like Josse stopped dying her hair. She’s outside Memison’s.»

Immediately sighting on the patio seating outside the restaurant Claire specified, Morgan soon found a blonde sitting alone. Though she seemed to hide behind her book, her deep blue eyes darted from behind the pages to catch glimpses of the crowd around her. «I found Dr. Malmgren.»

Zooming out, he also sighted upon a swordsman in black with technicolor hair approaching Memison’s. Without a directional microphone Morgan had no way to discern what he said to the maitre d’, but he suspected he had just requested a table for one with a clear line-of-sight to Dr. Malmgren. «I think I’ve found Polaris. He’s trying to set up an approach on Dr. Malmgren. I’m going in.»

Morgan stood, stretched, and considered his options. The AsgarTech plaza was half a kilometer wide. Getting a running start and leaping from the rooftop would, if he was lucky, let him land in the middle of the plaza. *That’ll just announce my presence to Polaris. Cutting through the crowd won’t do, either.*

Sighting on the building next to his, Morgan took a running start and flung himself across the twenty-meter gap. Catching the edge of the next rooftop, he pulled himself up. Several buildings later, he stood on the rooftop of the building housing Memison’s, which was apparently famous for its fish dinners. A quick look down showed, Dr. Malmgren taking dainty bites of her lemon sole on rice. She watched the swordsman with technicolor hair, and ate with the wariness of a gazelle sharing a watering hole with a lioness.

Satisfied she was safe for now, Morgan checked the alleyway below. Taking the stairs would require getting through security, and using the fire escape would make too much noise. The quickest way down was to jump, since the alley was clear. «Polaris hasn’t been able to approach yet. I’m going in.»

Before Claire could caution him, Morgan jumped. As he fell, he focused on the ground below and visualized the air thickening beneath him to cushion his fall. The air pressure around him increased as he fell toward terminal velocity. However, Morgan never reached the maximum speed at which it should have been possible for him to fall, for the air pressure around him continued to increase he no longer fell through gas.

Unknown to those dining nearby, Morgan plunged through a column of liquified air and landed on his feet with the grace of an alley cat. He opened his eyes, unable to believe he had gotten away with the fall he had taken. As he looked around to get his bearings, he caught the barest glimpse of amber eyes. There was a rustle of silk, a woman’s soft laughter, and then nothing. *Was that Bathory? No. That doesn’t make any sense.*

Shaking off such speculations, Morgan strode into the plaza. He vaulted the velvet rope cordoning off the the patio seating outside Memison’s and sat down at Dr. Malmgren’s table before she or the waiter attending her could protest. “Dr. Malmgren is to be my guest. Bring a pot of coffee and the check, please.”

“Of course, sir.”

Dr. Malmgren stared at Morgan as the waiter left her alone with him. “I’m sorry, but I don’t think we’ve been introduced.”

Morgan rolled his eyes, and took a deep breath. *I can’t believe Claire’s making me say this.* “Come with me if you want to live.”

“Oh. Claire sent you.” Dr. Malmgren’s wariness melted, leaving nothing but exasperated sympathy. “I am *so* sorry.”

“It sounds like you know her reasonably well.”

That earned a titter from Dr. Malmgren. “Sir, we roomed together for four years. She’s the best friend you could ask for, as long as your nerves are up to the task of being *her* friend.” Malmgren looked around, as if scanning for hostile eyes. “Now, what should I call you? I know who you are, but if you took the trouble of getting a haircut and color contacts I doubt you want me to use your usual names.”

“Call me Drew Burton.” It was the name on Morgan’s current set of fake ID, with a photo matching his current short-haired, blue-eyed guise.

Dr. Malmgren nodded. “So, you’re *the* Drew Burton? Claire told me a bit about you. You grew up on Mars in a city called Bradbury, got rich mining, and retired to Earth. You’re looking for a wife, and for some reason *I’m* your most likely candidate.”

Morgan nodded. “Exactly.”

Malmgren rolled her eyes. “How much has Claire told you about me?” A secure text message soon followed. «Best switch to secure talk, Adversary Stormrider.»

«Isaac Magnin financed your education in computer science, cognitive science, and mathematics from an early age. You’re now a highly-placed and valuable employee of the AsgarTech Corporation, you’re involved with Project Æsir, and your prototype has given you cause to fear for your life. Claire has asked me to get you out of Asgard and protect you from Polaris.» Morgan spread his hands as if to fan invisible cards across the table. «Have I left anything out?»

«That’s everything I’ve told Claire, but if you can get me out of Asgard and take me somewhere safe, I can tell you the rest.» Dr. Malmgren had lowered her voice as she spoke, her eyes darting to and fro. «I think Polaris is here.»

«He is. That’s why I interrupted your meal and instructed the waiter to bring me the check.»

Despite the fear making her hands tremble, her eyes took on an impish glint for a moment. «Were you serious about paying?»

«Yes. From now on, you don’t spend so much as a milligram. Your purchases can be tracked.»

«And yours can’t?»

Morgan shrugged. «When I create an identity, I ensure it can withstand scrutiny. Drew Burton is a Martian who grew up in Bradbury, near Olympus Mons. He made a small fortune in mining and decided to retire to Earth after striking up a correspondence with an attractive young computer scientist. If anybody looks, that’s all they’ll find. The relevant blockchains are doctored; Claire sees to that for me. Anybody searching your home AI would even find emails that you sent Drew Burton, suggesting that the two of you meet at Memison’s to see if there was any chemistry.»

Dr. Malmgren reddened at this last. «I had hoped she’d turn her skills to more ethical uses.»

«Such as saving your life?» Morgan leaned forward. «Look, doctor, I could have come here as Morgan Stormrider. Doing so might have saved me the trouble of creating a fake identity, it would save you some embarrassment, and it sure as hell would have saved me a haircut. However, Polaris is not the only one who should concern you. Did it occur to you that having me escort you into hiding outside of Asgard might also interest your patron, Isaac Magnin?»

«He- he wouldn’t hurt me.»

«You possess information that makes you a valuable witness against Isaac Magnin. I think it’s reasonable to suspect he might seek to prevent you from testifying against him.»

«So, we pretend to be two lonely people courting over the network who have finally met.» Dr. Malmgren’s blush renewed itself. «I suppose you’ll need to kiss me at some point. Since we’re out in public.»

Morgan gave Dr. Malmgren an appraising look as the waiter approached with. *We should have sent Eddie to do the extraction. She’s his type, not mine.* «Your hand in mine should be sufficient. Are you ready to go, or would you like a cup of coffee first?»

Author's Notes
==============

This was supposed to go up on Friday the 22nd of January, or Saturday the 23rd at the latest, but I got stuck on an earlier attempt at this scene that began before Morgan was even *in* the city of Asgard.

I wasn't able to get through the block until yesterday afternoon. Sorry about that. You can read the first attempt below if you like.

The First Attempt
=================

The maglev’s slow deceleration woke Morgan, who sat facing a young family of three. Their daughter, an excitable toddler, sat in her mother’s lap and pointed out the windows, as if expecting to see something of the city in which they would soon arrive before the train finally stopped. Her parents spoke to her in soft, affectionate Mandarin. “…has a huge crystal dome overhead, and at night you can see *all* the stars…”

Though Morgan understood, he refrained from intruding upon the conversation. Had his temporary companions wanted to include him, they would have spoken to him in English. Instead, they had left him alone. It was just as well; their conversation made it plain that they had never been to the antarctic city of Asgard before.

There was no need for Morgan to explain that the dome was mere concrete. They would see for themselves that a gigantic array of high-powered lights mounted inside created a false sun that lit the city for twelve hours a day. And they would eventually learn that after ‘sunset’, another system of lights mounted in the ceiling to simulate the night sky bathed the city in artificial starlight. If they stayed long enough, they would seven see the city bathed in false moonlight.

The novelty was already long gone for Morgan. He visited Asgard many times while touring with Crowley’s Thoth, and while the fans always welcomed the band, he often entertained a cruel suspicion while walking beneath the concrete skies among spires of steel and crystal. *Of course the locals are friendly and cheerful. Nobody wants to admit they live in a dystopia.*

Chasing that thought was another. *Dr. Malmgren willingly moved here from a city where you could walk beneath an open sky in green spaces. What does that say about her?*

“Have you been to Asgard before?” The father, who had been glancing at Morgan throughout the journey, finally spoke.

Since the other man used English, Morgan did the same. “A few times. I never stay. All those tons of concrete overhead make me nervous.”

“I just got a job with them, working security. You look like a cop or an Adversary who’s gone private-sector, so I figured-“

“I’m not private-sector.”

Theme Song
==========

Like the first chapter, chapter 3 of *Blackened Phoenix* has a theme song to go with its title, "Walk in the Shadows". This one's by Queensrÿche, from their 1985 *Rage for Order* album.

.. raw:: html

  <iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A27Wxcom55Yau5sRB0Ed9kw" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
