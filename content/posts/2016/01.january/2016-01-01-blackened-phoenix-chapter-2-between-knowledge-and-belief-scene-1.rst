*Blackened Phoenix*, Chapter 2: Between Knowledge and Belief (Scene 1)
######################################################################

:date: 2016-01-01
:summary: Morgan's refusal to hate Christabel for what she did to him and Naomi is going to bite him in the ass. Naomi isn't at all amused.
:category: Serials
:tags: morgan, naomi, claire, astarte, sarah, anger, forgiveness, revenge, cruelty
:summary_only: true
:og_image: /images/starbreaker-adversary-symbol.jpg


The journey by maglev back to New York from London had been a tense one, with Naomi refusing all of Morgan's attempts to draw her out of the sullen mood that had clouded her expression since they left MEPOL with a promise from Windsor that the diaries and letters found in Christabel Crowley's house would be at a pre-arranged location.

The box Morgan balanced on his left shoulder as he opened the door to his Upper West Side brownstone was ample proof that it wasn't the retrieval operation that had angered Naomi. *She's been upset ever since I said I couldn't condemn Christabel for what she had done. Shit. I had hoped we'd manage a bit longer before our first fight*.

Naomi cleared her throat behind him. "Can you please be lost in thought inside? I'd like to use the loo."

"Sorry." Morgan stepped inside, got out of the way, and put down the box. His hands free, he turned to take Naomi's coat. "Hi, Astarte. We're home."

The front hall had no screen for the household AI to show herself, but her voice came over the living room speakers. "Welcome back, Morgan. Hi, Naomi. How was London?"

"It was fine." Naomi said nothing more as she thrust her coat into Morgan's hands and strode through the living room. Once Morgan had finished with the coats, he was alone with Claire, who lay sprawled across a couch. An open manga compilation lay facedown on her belly, riding the waves of her snoring.

He would have left her that way if Mordred had not nuzzled her awake. "Sorry, Claire."

"No worries." Claire sat up, gave Mordred a quick petting, and retrieved the manga she had been reading. "You ought to check out this new manga, though. It's called *Eddie Van Helsing*, but it's really all about you."

Morgan shook his head. "Another? How bad is this one?"

"Oh, it's not *that* bad." Claire paused as Naomi returned. "But you have a black sword named Black Sunshine, and one of your deadliest techniques is called Black Sabbath."

Despite the foul mood still evident in her expression, Naomi chuckled. "Please tell me I'm not also involved this time."

"Sorry, Nims. Your character's tits are bigger, you wear glasses all the time, and you've got a pair of swords called Sweetness and Light."

"Sweetness and *Light*?" Naomi shook her head, her exasperation evident in her tone and expression. "Why can't they give *Morgan* a sword with a ridiculous name, like Bunnyhug or Fluffy Kitten Cuddles? I suppose I wear pink all the bloody time, too."

"I got a black sword called Black Sunshine and a technique called Black Sabbath." Morgan took the manga from Claire to show Naomi. "I'm dressed all in black, I seem to have the emotional range of a turnip, and why the hell are we arguing over ridiculous portrayals at the hands of hack writers instead of the real issue? Why are you *really* angry with me?"

Naomi glanced at Claire, and her voice was a low growl. "Do you *really* want to have that discussion here and now where others can hear what I think of you?"

"You think I'm an asshole. That much is fairly obvious, and even if Claire left Astarte would still be here." The wall-screen flared to life, and a bespectacled redhead with silver eyes leaned on the bottom of the display, glaring at Morgan and Naomi. *Speak of the devil*.

Claire was first to remark on Astarte's expression. "You're annoyed with Morgan, too?"

"*And* Naomi. Your stand-ins are parodies of how you two are *usually* portrayed in manga. I honestly thought you two would appreciate the joke." Astarte brandished a pencil as she spoke, and stood aside to reveal an easel on which she appeared to be drawing Morgan standing in the shower with most of his body obscured by soapsuds. "Be grateful you both don't have tails, fluffy kitty ears—"

Astarte lowered her tone before adding, "—and *barbed penises*. You really don't want to see how some bara(fn) artists depict you two. *rust me*."

"Fine." Naomi huffed, and advanced upon Morgan. She jabbed his chest with a fingertip, and continued jabbing as she spoke. "I seem to be the only one who gives a damn that that bitch Christabel played us better than she ever played her violin the whole time we both knew her. How do you think I feel, knowing that you can't condemn her because she just wanted a better life? I wanted a better life for myself, but I didn't fuck other people over to get it."

Now that Morgan understood why Naomi had been glaring daggers at him all afternoon, it was a herculean effort on his part to keep from laughing at her. *Was it really that simple? She just doesn't understand, and whose fault is that? My own, really*. "The reason I'm not as broken up over knowing Christabel had been working for Isaac Magnin the entire time is the same reason I was willing to go to London despite the knowledge that I am a walking security breach: Christabel told me everything. I just wasn't willing to believe her."

"Ohhhh, this ought to be *good*." Claire sprang from the couch. "I'm gonna make popcorn. You want some, Nims?"

"Right. Popcorn sounds good, Claire. Better yet, why don't you go out and get a case of beer?" Naomi's tone was dismissive, and she only looked at Morgan as she spoke. "You know what? I could believe that. It's totally you. What I can't believe is that you'd forgive everything she did to you -- and everything she did to *me*. What gave you the right?"

Morgan did not immediately answer. Something about Naomi's body language was off, and it wasn't just because of the new distance between them. When he finally spoke, it was intuition that chose his words rather than reason. "What if I told you that forgiving Christabel was my revenge on her, the cruelest blow I could strike?"

"I could almost believe you. It's better than hearing you decided to be a better person and stop hating Christabel." Naomi paused for moment, and might have continued if not for Astarte's reappearance on the living room screen. "What is it?"

"I can show you what happened. Morgan recorded everything, in case something happened to Christabel and he was accused." She turned her gaze toward Morgan, her voice compassionate. "Should I play it, and let her see for herself?"

"No." Naomi shook her head. "I'd like a copy to see later, but I want to hear from you first, Morgan, and in your own words."

The smell of popcorn bombarded with radiation permeated the room as Claire carried out her threat in the kitchen. Though Naomi had suggested that the younger woman leave on an errand, Morgan did not mind her hearing the story. *Maybe everybody should hear. It's not like Christabel's presence in my life hasn't affected them*. "Astarte, where are Eddie and Sid?"

"Eddie's down in the basement with Sarah, working out. Sid's up on the third floor with his family, settling in."

"Is there really room enough for everybody?" Naomi looked around, temporarily distracted. "Won't everybody be getting in each other's way?"

Astarte laughed at the notion. "Not at all, Naomi. Even with all of Sid's family, there's still room for another four people." She turned toward Morgan. "You bought too much house, as I mentioned when that realtor Lovelace showed you the place. She thought it was too much for you, too."

Morgan shrugged. It *had* been too much house, but he had no regrets. "She didn't mind the commission, though. Besides, you were lonely."

"You're right. I was." Astarte's wistful expression brightened as she returned her attention to Naomi. "It's actually quite empty with just the boss and Mordred around. Speaking of whom, Sid's girls have already discovered him, and are busy playing dress-up."

It wasn't the first time Sid's five daughters, none older than ten, had subjected Mordred to their attentions, but Morgan still winced at the notion. The cat always looked pathetic when he came back to Morgan begging to be extricated from the bows and other adornments inflicted on him. "You're keeping an eye on Mordred and those girls, right?"

Astarte nodded. "Of course. And I've already asked Sid to come down. Eddie and Sarah, too."

Morgan shot a glance Naomi's way. "I suppose you wanted to hear this first, but I'd rather not have to tell this story twice."

"I can understand Eddie, Sid, and even Claire." The grudging tone in Naomi's voice suggested to Morgan that her understanding came with an effort for which he should express gratitude in the form of a lavish candlelit dinner for two at one of Manhattan's most exclusive restaurants. "But Sarah? Why—"

"Because I took two bullets with your name on them." Sarah limped into the living room, still toweling herself down, and flopped into an armchair. Her scars, where the rounds from Alexander Liebenthal's devil-killer rifle tore through her legs and crippled them, were livid from her recent exertion. "So, yeah, Naomi. I think I've earned the right to hear from your boyfriend about how he's the biggest schmuck in New York."
