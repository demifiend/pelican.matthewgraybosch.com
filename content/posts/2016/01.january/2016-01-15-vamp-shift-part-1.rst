Vamp Shift
##########

:date: 2016-01-15 08:18
:summary: The vamp shift begins after full dark, and ends before the first hint of morning twilight. One need not be undead to work such hours, but it helps.
:category: Fragments and Outtakes
:tags: first-person, climate change, horror, shift work, philadelphia, taxi, poverty, serial, undead, underemployment, urban fantasy, vamp shift, graveyard shift, night shift, vampire, working class
:summary_only: true
:og_image: /images/night-taxi.jpg


vamp shift (noun)

  The vamp shift begins after full dark, and ends before the first hint 
  of morning twilight. One need not be undead to work such hours, but 
  it helps.

I honestly thought getting the Kiss would change my life. I figured I wouldn't just be Dave, that guy who plays bass in an indie band nobody listens to and delivers pizza when not playing freelance taxi driver for Ryde. I used to call my schedule the vamp shift as a joke, but it got old fast once I turned.

Driving at night for Ryde or delivering pizzas using the Hiro app stopped being a way to earn a few bucks without having to deal with the bullshit of working a nine-to-five like one of the normals, and became a necessity. I literally *couldn't* work during the day any longer, because what worked for Dracula in late nineteenth century London didn't work in the early twenty-first century. Goddamn climate change.

At least I know what to do if I decide life on the vamp shift sucks. I don't need a gun or pills if I want to check out; I can just go work on my tan. That's how a lot of young vampires go out, actually. It's not like we get help adjusting to undeath; the people giving us the Kiss figure we'll either adapt on our own, or we don't deserve the gift of eternal life. Care to guess how they tend to vote?

And then there's the blood thing. Sure, actual fresh blood fresh from the vein of a consenting human partner is the best, but good luck getting it if you aren't a rich hottie vampire. The lady who turned me, Victoria, was seeing twenty-eight different people. They all knew each other, and she'd drink from a different person each day so that nobody donated more than once a month. A couple of them, Jack and Diane, got married a month ago. Nice couple, met at one of Victoria's parties. I hear she helped ‘em put a down payment on a ranch out in West Chester.

If you're just an ordinary guy like me, you've got a few choices when it comes to blood. None of them are particularly palatable. There's stray pets. Yeah, you heard me. If your dog or cat goes missing, don't blame the local Asian restaurant, you racist pig. And collar your damn animals.

If you absolutely must have human blood, and you aren't really worried about consent, you can get it. You can prey on the homeless, but that's an even better way to get shot at by police than driving while black.

I'm not really into either. My alternative is manager's specials at supermarket meat departments. Buy beef that's about to get thrown out because it's almost at its sell-by date, but only if it isn't frozen. Throw the meat into a juicer and liquify it. Then choke it down while asking yourself, "What the fuck was I was thinking?"

I suppose if you're a vegetarian or a vegan you'll think I'm disgusting for doing that, but think of it this way: the animal's already dead, and if not for me the meat would just end up in a dumpster.

But last night, I found another way. I had just finished delivering a pizza when Ryde pinged my phone. The location was familiar; I knew ten different ways to get there because most nights I'd pick up a couple of car loads worth of drunk yuppies and get them home.

The fare last night was different. He was sober. He had a duffel bag clutched in one hand and a pistol in the other. Before I could ask him where he wanted to go he was grinding the muzzle of his gun into the back of my head. "Get on a highway heading south and drive. Keep right and stick to the speed limit, or so help me God I'll blow your fuckin' head off."

Author's Notes
==============

Blame my wife for this one. I told her that next week I wouldn't be working the vampire shift by mistake instead of the graveyard shift. She immediately suggested "vamp shift". We bounced the idea around a bit, and I ended up writing this.

Image Credit
============

`NYC Taxi by Kenny Louie`__

.. __: https://www.flickr.com/photos/kwl/7038011669
