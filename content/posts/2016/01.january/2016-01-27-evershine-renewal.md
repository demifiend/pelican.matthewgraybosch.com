Title: Evershine: *Renewal* (2012)
Summary: An Italian band tried doing Japanese-style power metal. It worked surprisingly well.
category: Metal Appreciation
Tags: 2012, AOR, Evershine, indie, Italy, J-metal, melodic metal, power metal, Renewal, Rome
summary_only: true
og_image: /images/evershine-renewal.jpg


I found _Renewal_, the 2012 album by a Roman band called [Evershine](http://www.evershine.it/), while dicking around on Spotify last night. If you don't mind using [Facebook](https://www.facebook.com/evershineband/), you can follow them there.

_Renewal_ is a rock-solid album with excellent guitar and keyboard work. However, the vocals and lyrics feel a bit like generic Japanese metal, like these guys OD'ed on Loudness, X-Japan, or post-2008 [Galneryus](/2016/01/why-galneryus-is-the-best-power-metal-band-youve-probably-never-listened-to/). They may be hit-or-miss depending on how much cheese you like in your metal. Fortunately, they don't hammer the dreaded phrase "so far awayyyy" like some power metal bands (this means _you_, Dragonforce).

The album cover's kinda cool, too, though my first thought when I saw it was, "The Painkiller has landed". Speaking of Judas Priest and their _Painkiller_ album, the last track on _Renewal_ ("Where Heroes Lie") reminds me a bit of what Priest might sound like if they did "Battle Hymn" and "One Shot at Glory" with an orchestra.

![Evershine: Renewal (2012) &mdash; THE PAINKILLER HAS LANDED](/images/evershine-renewal.jpg)

The production is quite clean for an indie band, and the keyboards and guitar solos make for fun listening. _Renewal_ might not get you out of your seat and banging your head, but they'll brighten a dull workday without making you want to invoke the powers of Hell and put your coworkers and customers to the sword. In fact, I had this on repeat while working on a scene for [_Blackened Phoenix_](/starbreaker/novels/blackened-phoenix/).

That's not a bad thing, by the way. If you like power metal and melodic metal in the vein of Rhapsody of Fire, Stratovarius, Gamma Ray, and Helloween you might want to give this album a shot. Likewise if you're a fan of J-metal.

## Spotify Embed

You can check out _Renewal_ right here.

<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A2vlvYcl5ViQYoeRev5L6st" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

## New Single: "Crimson Dawn"

Evershine also seem to have a single from 2015 called "Crimson Dawn". Maybe they'll drop another album in 2016.

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A2m5twb5kJUO71VA2z4SVUK" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
