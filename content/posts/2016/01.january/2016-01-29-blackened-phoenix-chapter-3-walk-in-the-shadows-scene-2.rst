*Blackened Phoenix*, Chapter 3: Walk in the Shadows (Scene 2)
#############################################################

:modified: 2018-05-19
:summary: Though Josefine Malmgren is willing to trust Morgan, or Drew Burton (as he insists on being called), getting past Polaris won't be easy.
:summary_only: true
:tags: morgan, josefine, polaris, confrontation, challenge, duel borderlands, torgue, maliwan
:category: Serials
:date: 2016-01-29


Morgan’s gloved hand was warm as it held Josefine’s. It fired her courage as she followed him through the lunchtime crowd gathered at Memison’s for one of their famous seafood meals. More importantly, it kept her from fearing the possibility that the tattoo on the nape of his neck identifying him as the last of the one-hundred series asura emulators, number six hundred and sixty-six. Though she had no more use for what her ancestors once called the White Christ than most people, her friendship with Claire had exposed her to a sufficient number of horror movies to make that number one of portent.

His grip on her was gentle, rather than weak or diffident, leading her to surmise that were she his prisoner she would stand little chance of escaping his grasp. On the heels of that thought came another; should matters escalate to violence, she suspected Morgan would prove equally implacable. «What will you do if Polaris attacks us?»

Morgan shrugged. «Killing him would be the most sensible thing to do.»

«I was afraid you’d say that.»

«However, my primary objective is to get you out of Asgard as quickly and as discreetly as possible. Because Polaris is a prototype, destroying him would invite the AsgarTech Corporation’s scrutiny.»

«I’m glad you understand the risks involved.» Josefine pressed tighter against Morgan as the crowd tightened around them. Her fingertips brushed against something cold and hard in his other hand. «Is that—»

«Nakajima USP 11.43mm with attached suppressor. Loaded with low-yield HEAP rounds. In case I have to kneecap Polaris.»

Josefine shivered at the possibility. *I can’t believe I let Claire talk me into letting this man escort me. He’s not a bodyguard; he’s a bloody hitman.* «Please promise you’ll refrain from unnecessary violence.»

Morgan glanced over his shoulder at her her again. «I explained my primary objective, did I not?»

«Yes, but—»

«Trust me, please. Most of the time I know what I’m doing.»

*Most of the time?* Josefine couldn’t resist asking the obvious question. «And when you don’t?»

«I improvise. It’s never not turned out well, though I sometimes get banged up a bit.»

Josefine suppressed a whimper, and squeezed Morgan’s hand tighter. Hiding in his shadow, she let him part the crowd before them and followed in his wake. They managed only a few steps before Morgan stopped and pivoted around her. «What’s wrong?»

«It’s Polaris. He’s making a move.» As soon as Morgan explained, the crowd dispersed as a man with technicolor hair in a black coat approached Morgan. He let go of Josefine’s hand. «If I can’t settle this peacefully, run to the Hellfire Club at Bifrost Station. Tell the concierge you’re Drew Burton’s guest. They’ll look after you until I can return.»

«All right.» Josefine withdrew to the edge of the crowd. Though she didn’t want to see the fight that seemed all but inevitable because of the gun in Morgan’s hand, she didn’t want to leave him alone with Polaris yet. Perhaps her presence might yet hold some restraining influence on the prototype.

Polaris was as tall as Morgan, and possessed of the same wiry physique and sculpted, almost androgynous features. But the prototype’s features seemed almost unsettled, as if they might reconfigure themselves into a different face on a whim. He gazed at Morgan for a long moment, his left hand hovering over the crystalline hilt of the sword whose scabbard he gripped with his right as if he might draw at any moment. “So, you’re the last of the original series, old triple-six himself. I’ve heard *so* much about you, big brother.”

Josefine frowned at Polaris’ greeting. *Triple-six? Does he mean 666? If so, what significance does* that *have?*

Narrowing his eyes, Morgan raised his pistol and thumbed off the safety. “You’re making a lot of people nervous, Polaris, especially Dr. Malmgren. How about getting your hands away from that sword?”

“I think everybody here should be more afraid of your pistol.” Despite his words, Polaris complied, and let his sword hang from his hip as he crossed his arms. “Why are you here, Adversary Stormrider? Has Dr. Malmgren accused me of violating her rights? Or do you hope to glean evidence against Isaac Magnin?”

“Neither of which are your concern. And since you have been so kind as to lay bare my true identity, allow me to offer you this one warning. Dr. Malmgren is under my protection now. Threaten her, and you threaten me. All who threaten me die.”

“All who threaten me die.” Polaris repeated the words, his tone a mockery of Morgan’s. “Oh, you are just so cute. Who do you think you’re frightening, especially with a name like Morgan Stormrider? That might impress the kids while you get up on stage and wank your guitar, but the Lord has shown me the truth about you. You’re nothing compared to me.”

Josefine stared at Polaris, aghast at his words and sure Morgan would retaliate with violence. Instead, he laughed. “Oh, yes, you got me. You can blame Stormrider on the record label, but the name I chose for myself and paid to have backed up with records of payments to a nonexistent foster family, Morgan Sturmjäger, is no less ridiculous. Chalk the name and my decision to become an Adversary to the insanity of youth if you must, but remember: your personality was once *mine*. Dr. Malmgren told me that much already.”

Polaris’ expression twisted in a moue . His right hand gripped the scabbard of his sword again, and this time his left hand curled around the hilt. “You are implying that my very *soul* was once yours. Yours is a soul of filth and depravity. The Lord has told me so. I am pure, untainted by humanity.”

“You’re human enough to try trash-talking an opponent.” Morgan smiled, and holstered his pistol. “And you’re human enough to want to test your sword against mine. I can see it in the way you’re trembling with the need to draw that blade on your hip. Tell you what. Meet me in Midgard at midnight. Come to Blackwater Park. Let’s see what you’ve got.”

Polaris glanced at Josefine, and for the split-second their eyes met she saw in his gaze a lust to possess her froze her spine and set her knees to wobbling. “And if I defeat you, will you leave Dr. Malmgren to me?”

Morgan shrugged. “If you kill me, I won’t be able to stop you from doing much of anything. Should I incapacitate you, however, I will take Dr. Malmgren from here as she has requested. Any further attempt at interference on your part or that of your God will be punished without mercy.”

With that, Morgan flicked a small spherical object toward Polaris before turning and springing toward Josefine. Time seemed to go elastic as smoke and light enveloped Polaris. Before she could protest, Morgan gathered her into his arms and bore her from the plaza.

He kept running, and time and relative motion seemed to grind to a halt around them. He did not stop or put her down until they had reached the Hellfire Club at Bifrost Station. As he panted, Josefine stared at him, her mind afire with the implications of what he had just done. Her internal clock had been running faster than network standard time, so that while half an hour had passed for her, less than thirty seconds had elapsed on the rest of the network. *No. That’s impossible. It can’t be time dilation, because otherwise my clock would run slower than network standard time. Is this time compression? But how? Neither general nor special relativity account for this.*

Morgan’s voice brought her out of her theoretical reverie. “You look spooked. Let’s go up to my suite.”

*I can’t believe I’m going to this guy’s hotel room. Especially after the crazy shit I just saw.* Despite her misgivings, she followed Morgan into the Hellfire Club. Like an automaton, she signed the slip acknowledging that Morgan was trusting her with a key to his suite, this letting her come and go as she pleased. Once Morgan had locked the door behind them, she found the wet bar and poured herself a brandy. “Do you realize what you just did should be impossible?”

Morgan shrugged. “There’s this doctor I’ve been seeing named Desdinova. He keeps telling me the same thing, yet I keep doing it and I can’t explain how. Claire calls it demon-speeding. I think of it as digging in and holding my place in time while moving freely within space. I’m going to call room service. You want anything?”

Josefine stared into her brandy. “I don’t think I could hold any food down right now. As a matter of fact, I’m not sure the brandy was a good idea.”

Gentle fingertips, feminine beneath the calluses, held Josefine’s hair out of her face as her unsettled stomach jettisoned its cargo. “It’s all right, Josse. You can trust Morgan. I was watching every step of the way, ready to put my boot up his arse if he did anything stupid.”

“C-Claire?” Josefine wiped her mouth with the back of her hand, and turned to look behind her.

The auburn-haired woman in a faded Pulsecannon t-shirt and fatigue pants smiled, and drew Josefine into a tight embrace. “Of course it’s me, Josse. Morgan insisted I come along. He figured you’d want a friend waiting for you. Also, he was afraid Naomi might get jealous if he was alone with you. Now wash your face and get your shit together. We need to talk.”

Author's Notes
==============

In case you were wondering, HEAP rounds are *high explosive armor piercing* ammunition. Yes, it's probably overkill, but Morgan isn't about to take chances with a next-generation asura emulator prototype gone rogue. He knows just how hard *he* is to kill, and figures Polaris will be at least as resilient as he is.

You probably won't find such ammo for a .45 pistol (11.43mm is .45 inches) in the real world, of course. So, who makes HEAP rounds for a pistol based on the venerable `M1911 <https://en.wikipedia.org/wiki/M1911_pistol>`_ designed by `John Moses Browning <https://en.wikipedia.org/wiki/John_Browning)>`_? That's not really relevant to the plot, but Morgan could get them from Nakajima Armaments.

However, it would be funnier if Morgan got them from *this* guy.

.. image:: {filename}/images/borderlands2-torgue.jpg
    :alt: Machismo. Air guitar. Bleeped-out profanity. EXPLOSIONS!
    :align: left

But he doesn't, because I don't really need `Gearbox Software <http://www.gearboxsoftware.com/games/borderlands-the-handsome-collection>`_ coming after me. However, when I'm loaded for troll I get my acid-tipped and incendiary ammo from Maliwan™. :)
